﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Foxxlegacy.Web.Startup))]
namespace Foxxlegacy.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}