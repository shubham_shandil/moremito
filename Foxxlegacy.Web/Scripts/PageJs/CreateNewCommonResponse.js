﻿$(document).ready(function () {

    $(document).on("click", "#btnEdit", function () {

        var ID = $(this).attr("data-ID");
        var Name = $(this).attr("data-Name");
        var ReplyText = $(this).attr("data-ReplyText");
        var DisplayOrder = $(this).attr("data-DisplayOrder");

        $("#ID").val(ID);
        $("#Name").val(Name);
        $("#ReplyText").val(ReplyText);
        $("#DisplayOrder").val(DisplayOrder);
    });


});