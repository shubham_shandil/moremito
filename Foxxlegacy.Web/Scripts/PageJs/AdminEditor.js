﻿$(document).on("click", "#btnPreviewSendDates", function () {

    $("#DuplicateMessagedatatable").html("");
    var txtQuantity = $("#txtQuantity").val();
    var txtOnceEvery = $("#txtOnceEvery").val();
    var ddlFreqPeriod = $("#ddlFreqPeriod").val();
    var txtStartingOn = $("#txtStartingOn").val();
    var TimeId = $("#TimeId").val();
    var dataObj = {};

    dataObj.Quantity = txtQuantity;
    dataObj.OnceEvery = txtOnceEvery;
    dataObj.ddlFreqPeriod = ddlFreqPeriod;
    dataObj.StartingOn = txtStartingOn;
    dataObj.Time = TimeId;
    $.ajax({
        type: "GET",
        url: "/AdminEditor/PopulateFreqTable",
        data: dataObj,
        success: function (res) {

            if (res.FreqTableList.length > 0) {
                for (var i = 0; i < res.FreqTableList.length; i++) {

                    var Ordinal = res.FreqTableList[i].Ordinal;
                    var OrderDate = res.FreqTableList[i].OrderDate;


                    $("#DuplicateMessagedatatable").append("<tr><td>" + Ordinal + "</td><td>" + OrderDate + "</td></tr>");


                }
            }



        }
    });

});


$("body").on("click", "#btnDuplicateMessageSave", function () {

    var SmsId = $("#SmsId").val();


    //Loop through the Table rows and build a JSON array.
    var FreqTableList = new Array();
    $("#tblPreviewSendDates TBODY TR").each(function () {
        var row = $(this);
        var FreqTabledata = {};
        FreqTabledata.ID = SmsId;
        FreqTabledata.Ordinal = row.find("TD").eq(0).html();
        FreqTabledata.OrderDate = row.find("TD").eq(1).html();
        FreqTableList.push(FreqTabledata);
    });

    $.ajax({
        type: "POST",
        url: "/AdminEditor/DuplicateMessageInsert",
        data: '{model:' + JSON.stringify(FreqTableList) + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });

});