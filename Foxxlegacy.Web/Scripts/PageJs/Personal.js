﻿$(document).on("click", "#personalinfo", function () {
    var Id = $(this).attr("data-Id");
    showLoader('genDiv')
    $.ajax({
        type: "GET",
        url: "/Members/GetPersonalInformation?InfoId=" + Id,
        dataType: "html",
        success: function (data) {
            $('#myModalMyInfo').html('');
            $('#myModalMyInfo').html(data);
            $('#infofield').modal('show');
            hideLoader('genDiv')
        },
        error: function () {
            hideLoader('genDiv')
            alert("No Data to view");

        }
    });
});

$(document).on("click", "#personaltree", function () {
    var Id = $(this).attr("data-Id");
    $('#userTree').modal('show');
    $.ajax({
        type: "GET",
        // url: "/Members/GetPersonalInformation?InfoId=" + Id,
        url: "/members/fetchreferralnetworkuserdata?id=" + Id,
        // dataType: "html",
        success: function (data) {
            debugger
            $('#userTreeInfo').html('');
            $('#userTreeInfo').html(data);
            $('#userTree').modal('show');
        },
        error: function () {
            alert("No Data to view");
        }
    });
});
$(document).on("click", "#personalCurrenttree", function () {
    var Id = $(this).attr("data-Id");
    $('#userTree').modal('show');
    $.ajax({
        type: "GET",
        // url: "/Members/GetPersonalInformation?InfoId=" + Id,
        url: "/members/FetchReferralNetworkCurrentUserData?id=" + Id,
        // dataType: "html",
        success: function (data) {
            debugger
            $('#userTreeInfo').html('');
            $('#userTreeInfo').html(data);
            $('#userTree').modal('show');
        },
        error: function () {
            alert("No Data to view");
        }
    });
});

$(document).on("click", "#tabMenu", function () {
    var Id = $(this).attr("data-Id");
    window.location.href = "/Members/Resources?Id=" + Id;
});

$(document).ready(function () {

    $(document).on("click", "#btnQTEUpdatedRankHistory", function () {

        var UserId = $(this).attr("data-UserId");
        var TabName = $(this).attr("data-TabName");
        QTEUpdated(UserId, TabName);
    });
    $(document).on("click", "#btnQTEUpdatedUserInfoDetails", function () {

        var UserId = $(this).attr("data-UserId");
        var TabName = $(this).attr("data-TabName");
        QTEUpdated(UserId, TabName);
    });
    function QTEUpdated(UserId, TabName) {
        $.ajax({
            type: "POST",
            url: "/Admin/QTEUpdated?UserId=" + UserId + "&TabName=" + TabName,
            dataType: "json",
            success: function (data) {
                if (TabName == 'RankHistoryFromTree') {
                    $('#RankHistorymsgQTEUpdatedId').show();
                    $("#RankHistorymsgQTEUpdatedId").html(data);
                }
                else if (TabName == 'UserInfoDetailsFromTree') {
                    $('#UserInfoDetailsmsgQTEUpdatedId').show();
                    $("#UserInfoDetailsmsgQTEUpdatedId").html(data);
                }
            },

        });
    }

    $(document).on("click", "button#btnSearchUser", function (e) {
        var _this = $(e.currentTarget),
            _searchErrorTxt = $("p#searchErrorTxt"),
            _txtBxUsrNm = $("input#txtBxUsrname"),
            _tblDivSection = $("div#PlacementUserPanel"),
            _tblObject = $("#placementUserTblBody");

        var userName = _txtBxUsrNm.val(),
            UserId = _this.data("id");

        _tblObject.html('');
        _tblDivSection.hide();
        _searchErrorTxt.hide();

        if (userName != null && userName.length > 0) {
            var moveUserName = $('#MoveUserId').text();
            $.ajax({
                url: "/members/getuserdetails?u=" + userName + "&MoveUserId=" + UserId,
                type: "GET",
                dataType: "JSON",
                success: function (response) {
                    var trArray = [];
                    if (typeof response === "object" && response.length > 0) {
                        $.each(response, function (i, user) {
                            console.log('user', user)
                            trArray.push('<tr>');
                            trArray.push('<td>' + user.UserName + '</td>');
                            trArray.push('<td>' + user.FirstName + '</td>');
                            trArray.push('<td>' + user.LastName + '</td>');
                            trArray.push('<td>' + user.Phone + '</td>');
                            trArray.push('<td><button type="button" class="btn btn-primary placementmap" data-sid="' + user.ID + '" data-cid="' + user.CustomerID + '" data-uid="' + UserId + '">Place User ' + moveUserName + ' here</button></td>');

                            trArray.push('</tr>');
                        });
                        _tblObject.html(trArray.join(''));
                        if (trArray.length > 0) {
                            _tblDivSection.show();
                        }
                    } else {
                        _searchErrorTxt.show();
                    }
                },
            });
        }

    });

    $(document).on("click", "button.placementmap", function (e) {
        var _this = $(e.currentTarget);
        if (confirm("Are you sure to place that user ?")) {
            var _UserId = _this.data('uid'),
                _SponsorId = _this.data('sid');

            if (parseInt(_UserId) > 0 && parseInt(_SponsorId) > 0) {
                showLoader('infofield')
                $.ajax({
                    url: "/members/updateuserplacement",
                    data: { "SponsorUserId": _SponsorId, "UserId": _UserId },
                    type: "POST",
                    beforeSend: function () {

                    },
                    success: function (response) {
                        debugger
                        hideLoader('infofield')
                        alert(response);
                        $('#infofield').modal('hide');
                    }
                });
            }
        }
    });

    $(document).on("click", "button.treebutton", function (e) {
        debugger
        var _this = $(e.currentTarget),
            _liObj = _this.closest("li"),
            _childNode = _liObj.find("ul.child-tree-node");


        var id = parseInt(_this.data("id")),
            _sign = $.trim(_this.html());
        if (id > 0 && _sign == "+") {
            $.ajax({
                url: "/members/fetchreferralnetworkuserdata?id=" + id,
                type: "GET",
                success: function (response) {
                    if (typeof response == "string" && response.length > 0) {
                        _liObj.append(response);
                        _this.html("-");
                    }
                }
            });
        } else {
            _childNode.remove();
            _this.html("+");
        }


    });

    $('.user-info-span').bind("contextmenu", function (event) {
        return
        $('.move-user').show();
        var currentRow = $(this).closest('li');

        //if (currentRow != undefined) {
        //    $("#creditCardList tr").each(function () {
        //        $(this).removeClass('selectedCreditCard');
        //    })
        //    $(currentRow).addClass('selectedCreditCard');
        //    var type = currentRow.find('td:eq(2) input').val();

        //    if (type == 5 || type == 3) {
        //        $('.void-trans').hide();
        //        $('.capture-trans').hide();
        //        $('.credit-trans').hide();
        //    } else if (type == 0) {
        //        $('.void-trans').hide();
        //        $('.capture-trans').hide();
        //        $('.credit-trans').show();
        //    } else {
        //        $('.void-trans').show();
        //        $('.capture-trans').show();
        //        $('.credit-trans').hide();
        //    }
        //}

        if (currentRow != undefined) {
            $(".user-tree-row").each(function () {
                $(this).find('span.user-info-span').removeClass('selectedCreditCard');
            })
            $(currentRow).find('span.user-info-span').addClass('selectedCreditCard');

        }

        // Avoid the real one
        event.preventDefault();

        var top = event.pageY - 35;
        var left = event.pageX;
        // Show contextmenu
        $(".custom-menu").finish().toggle(100).

            // In the right position (the mouse)
            css({
                top: top + "px",
                left: left + "px"
            });
    });

    $(document).bind("mousedown", function (e) {
        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {

            // Hide it
            $(".custom-menu").hide(100);
        }
    });

});