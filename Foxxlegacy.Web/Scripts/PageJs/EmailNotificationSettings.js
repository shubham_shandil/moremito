﻿


$(document).on("click", "#EmailNotificationSettingsOptInOutbtn", function () {
    var href = window.location.href;
    var Id = $(this).attr("data-OptOutCatID");
    var IsOptedIn = $(this).attr("data-IsOptedIn");

    var dataObj = {};
    if (Id != undefined && Id != null) {
        dataObj.OptOutCatID = Id;
        dataObj.IsOptedIn = IsOptedIn;
        $.ajax({
            type: "GET",
            url: "/Members/EmailNotificationSettingsOptOutInsertDelete",
            data: dataObj,
            success: function (res) {
                window.location.href = href;


            }
        });
    }
});






