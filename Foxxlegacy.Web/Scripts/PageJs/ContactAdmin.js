﻿

$(document).on("click", "#viewId", function () {

    $("#InvalidDataMsg").html("");
    var Id = $(this).attr("data-ID");
    GetDialoglist(Id);
    $.ajax({
        type: "GET",
        url: "/Contacts/GetContactUsById?ID=" + Id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            result = JSON.stringify(response);
            $('#lblDate').html(response.contactUsListViewModel[0].EventDate);
            $('#lblSubject').html(response.contactUsListViewModel[0].Subject);
            $('#lblDetails').html(response.contactUsListViewModel[0].Details);
            $('#ContactsId').val(response.contactUsListViewModel[0].ID);
        },

    });
});

$(document).on("click", "#btnAddDialogue", function () {
    $("#InvalidDataMsg").html("");
    $("#AddyourComments").html("");
    var Id = $("#ContactsId").val();
    var Message = $("#DialogueId").val();


    var dataObj = {};
    if (Id != undefined && Id != null) {
        dataObj.ID = Id;
        dataObj.Message = Message;
        $.ajax({
            type: "POST",
            url: "/Contacts/AddyourComments",
            data: dataObj,
            success: function (res) {

                for (var i = 0; i < res.contactUsListViewModel.length; i++) {
                    var Dialogue = res.contactUsListViewModel[i].Dialogue;
                    var CreateDate = res.contactUsListViewModel[i].CreateDate;
                    var IsAdmin = res.contactUsListViewModel[i].IsAdmin;
                    if (IsAdmin == true) {
                        $("#AddyourComments").append("<div class='admin-reply'>Admin:&nbsp;" + Dialogue + "<br /><div style='position:relative;float:right;'>" + CreateDate + "</div></div><div class='clear'></div>")
                    }
                    else {
                        $("#AddyourComments").append("<div class='member-reply'>" + Dialogue + "<br><div style='position:relative;float:right;'>" + CreateDate + "</div></div></div><div class='clear'></div>");
                    }


                }


            }
        });
    }
});





function GetDialoglist(Id) {



    var dataObj = {};
    if (Id != undefined && Id != null) {
        dataObj.ID = Id;

        $.ajax({
            type: "GET",
            url: "/Contacts/GetyourComments",
            data: dataObj,
            success: function (res) {

                for (var i = 0; i < res.contactUsListViewModel.length; i++) {
                    var Dialogue = res.contactUsListViewModel[i].Dialogue;
                    var CreateDate = res.contactUsListViewModel[i].CreateDate;
                    var IsAdmin = res.contactUsListViewModel[i].IsAdmin;
                    if (IsAdmin == true) {
                        $("#AddyourComments").append("<div class='admin-reply'>Admin:&nbsp;" + Dialogue + "<br /><div style='position:relative;float:right;'>" + CreateDate + "</div></div><div class='clear'></div>")
                    }
                    else {
                        $("#AddyourComments").append("<div class='member-reply'>" + Dialogue + "<br><div style='position:relative;float:right;'>" + CreateDate + "</div></div></div><div class='clear'></div>");

                    }


                }


            }
        });
    }
}


$(document).on("click", "#btnArchive", function () {

    var Id = $("#ContactsId").val();



    var dataObj = {};
    if (Id != undefined && Id != null) {
        dataObj.ID = Id;
        $.ajax({
            type: "POST",
            url: "/Contacts/ClosethisContact",
            data: dataObj,
            success: function (res) {


                $("#InvalidDataMsg").html("This contact has been closed. Please click to the Close Page button to continue.");

            }
        });
    }
});

$(document).on("click", "#btnCxl", function () {

    window.location.reload();
});