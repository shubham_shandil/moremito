﻿


$(document).on("click", "#FirstbtnDetailsId", function () {
    $("#secondTabledatalisId").html("");
    $("#thirdTabledatalisId").html();
    $("#thirdTabledatalistdiv").hide();
    $("#fourthTabledatalistdiv").hide();
    $("#fifthTabledatalistdiv").hide();
    $("#dispayHeadingForSecondTabledatalist").html("");

    var Year = $(this).attr("data-Year");

    var DetailsList = "secondTabledatalist";

    var dataObj = {};

    dataObj.Year = Year;
    dataObj.DetailsList = DetailsList;
    $.ajax({
        type: "GET",
        url: "/FoxxCash/GetFoxxCashDetailsListByYearMonth",
        data: dataObj,
        success: function (res) {
            $("#secondTabledatalistdiv").show();
            $("#dispayHeadingForSecondTabledatalist").append("<b>Summary by Month " + Year + "</b>");
            if (res.foxxCashDetailsListViewModel2.length > 0) {
                for (var i = 0; i < res.foxxCashDetailsListViewModel2.length; i++) {

                    var TheMonth = res.foxxCashDetailsListViewModel2[i].TheMonth;
                    var TheYear = res.foxxCashDetailsListViewModel2[i].TheYear;
                    var Total = res.foxxCashDetailsListViewModel2[i].Total;
                    var TheMonthINT = res.foxxCashDetailsListViewModel2[i].TheMonthINT;

                    $("#secondTabledatalisId").append("<tr><td><a class='outbtn' href='javascript: void (0)' id='SecondbtnDetailsId' data-Year=" + TheYear + " data-Month=" + TheMonth + " data-TheMonthINT=" + TheMonthINT + ">Details</a></td>"
                        + "<td>" + TheMonth + "</td><td>" + Total + "</td></tr>");


                }
            }
            else {
                $("#secondTabledatalisId").append("<tr><td colspan='3' style='text-align:center;'>No data to display.</td></tr>");
            }


        }
    });

});

$(document).on("click", "#SecondbtnDetailsId", function () {

    $("#dispayHeadingForthirdTabledatalist").html("");
    $("#thirdTabledatalisId").html("");
    $("#dispayHeadingForfourthTabledatalist").html("");
    $("#fourthTabledatalisId").html("");

    $("#dispayHeadingForfifthTabledatalist").html("");
    $("#fifthTabledatalisId").html("");
    var Year = $(this).attr("data-Year");
    var MonthName = $(this).attr("data-Month");
    var Month = $(this).attr("data-TheMonthINT");
    var DetailsList = "thirdTabledatalist";

    var dataObj = {};

    dataObj.Year = Year;
    dataObj.Month = Month;
    dataObj.DetailsList = DetailsList;
    $.ajax({
        type: "GET",
        url: "/FoxxCash/GetFoxxCashDetailsListByYearMonth",
        data: dataObj,
        success: function (res) {

            //---------third table dtata list sction---------
            $("#dispayHeadingForthirdTabledatalist").append("<b>Summary for " + MonthName + " " + Year + "</b>");
            $("#thirdTabledatalistdiv").show();
            if (res.foxxCashDetailsListViewModel3.length > 0) {
                for (var i = 0; i < res.foxxCashDetailsListViewModel3.length; i++) {

                    var CommissionName = res.foxxCashDetailsListViewModel3[i].CommissionName;

                    var Total = res.foxxCashDetailsListViewModel3[i].Total;


                    $("#thirdTabledatalisId").append("<tr>"
                        + "<td>" + CommissionName + "</td><td>" + Total + "</td></tr>");
                }
            }
            else {
                $("#thirdTabledatalisId").append("<tr><td colspan='2' style='text-align:center;'>No data to display.</td></tr>");
            }
            //---------third table dtata list section---------

            //---------fourth table dtata list sction---------
            $("#dispayHeadingForfourthTabledatalist").append("<b>Foxx Cash Detail for Orders " + MonthName + " " + Year + "</b>");
            $("#fourthTabledatalistdiv").show();
            if (res.foxxCashDetailsListViewModel4.length > 0) {
                for (var i = 0; i < res.foxxCashDetailsListViewModel4.length; i++) {

                    var CommissionsOrderID = res.foxxCashDetailsListViewModel4[i].CommissionsOrderID;

                    var SumTotal = res.foxxCashDetailsListViewModel4[i].SumTotal;

                    var CommissionsCreateDate = res.foxxCashDetailsListViewModel4[i].CommissionsCreateDate;
                    var Description = res.foxxCashDetailsListViewModel4[i].Description;

                    var FromLevel = res.foxxCashDetailsListViewModel4[i].FromLevel;


                    $("#fourthTabledatalisId").append("<tr>"
                        + "<td>" + CommissionsOrderID + "</td><td>" + CommissionsCreateDate + "</td><td>" + SumTotal + "</td><td>" + Description + "</td><td>" + FromLevel + "</td></tr>");
                }
            }
            else {
                $("#fourthTabledatalisId").append("<tr><td colspan='5' style='text-align:center;'>No data to display.</td></tr>");
            }

            //---------fourth table dtata list section---------


            //---------5th table dtata list sction---------
            $("#dispayHeadingForfifthTabledatalist").append("<b>Foxx Cash Detail from Transfers " + MonthName + " " + Year + "</b>");
            $("#fifthTabledatalistdiv").show();
            if (res.foxxCashDetailsListViewModel5.length > 0) {
                for (var i = 0; i < res.foxxCashDetailsListViewModel5.length; i++) {

                    var ID = res.foxxCashDetailsListViewModel5[i].ID;

                    var Amount = res.foxxCashDetailsListViewModel5[i].Amount;

                    var CreateDate = res.foxxCashDetailsListViewModel5[i].CreateDate;
                    var Description = res.foxxCashDetailsListViewModel5[i].Description;

                    var FromLevel = res.foxxCashDetailsListViewModel5[i].FromLevel;


                    $("#fifthTabledatalisId").append("<tr>"
                        + "<td>" + CreateDate + "</td><td>" + Amount + "</td><td>" + Description + "</td><td>" + FromLevel + "</td></tr>");
                }
            }
            else {
                $("#fifthTabledatalisId").append("<tr><td colspan='4' style='text-align:center;'>No data to display.</td></tr>");
            }
            //---------5th table dtata list section---------

        }
    });

});




















