﻿$(document).on("click", "button.treebutton", function (e) {
    var _this = $(e.currentTarget),
        _liObj = _this.closest("li"),
        _childNode = _liObj.find("ul.child-tree-node");

    var id = parseInt(_this.data("id")),
        _sign = $.trim(_this.html());
    if (id > 0 && _sign == "+") {
        $.ajax({
            url: "/admin/fetchsubmaintree?id=" + id,
            type: "GET",
            success: function (response) {
                if (typeof response == "string" && response.length > 0) {
                    _liObj.append(response);
                    _this.html("-");
                }
            }
        });
    } else {
        _childNode.remove();
        _this.html("+");
    }
});

$(document).on("click", ".EditMyPersonalInfo", function () {
    var UserId = $(this).attr("data-UserId");
    var MyInfoURL = '/Admin/GetUserPersonalInfo?UserId=' + UserId;

    var options = { "backdrop": "static", keyboard: true };
    $.ajax({
        type: "GET",
        url: MyInfoURL,
        contentType: "application/json; charset=utf-8",

        datatype: "json",
        success: function (data) {

            $('#myModalEditPersonalInfo').html(data);
            $('#editfieldPersonalInfo').modal(options);
            $('#editfieldPersonalInfo').modal('show');

        },
        error: function () {
            alert("No Data to view");
        }
    });

});




$(document).on("click", ".EditMyAddressInfo", function () {
    var UserId = $(this).attr("data-UserId");
    var MyInfoURL = '/Admin/GetUserAddressInfo?UserId=' + UserId;

    var options = { "backdrop": "static", keyboard: true };
    $.ajax({
        type: "GET",
        url: MyInfoURL,
        contentType: "application/json; charset=utf-8",

        datatype: "json",
        success: function (data) {

            $('#myModalEditAddressInfo').html(data);
            $('#editfieldAddressInfo').modal(options);
            $('#editfieldAddressInfo').modal('show');

        },
        error: function () {
            alert("No Data to view");
        }
    });

});

$(document).on("click", "#btnAdd", function () {
    debugger
    $("#GrandFathersdiv").show();
    //$("#GrandFathersdiv").html("");
    
    var UserId = $(this).attr("data-UserId");
    var RankID = $("#RankID").val();
    var ExpireDate = $("#ExpireDateId").val();
    var Reason = $("#ReasonId").val();
    var dataObj = {};

    dataObj.UsersID = UserId;
    dataObj.RankID = RankID;
    dataObj.ExpireDate = ExpireDate;
    dataObj.Reason = Reason;

    if ($.isEmptyObject(ExpireDate)) {

        $("#ExpireDateIdErrormsg").show();
        GetAllGrandFathering();
    }
    else {
        $("#ExpireDateIdErrormsg").hide();
        $.ajax({
            type: "POST",
            url: "/Admin/AddGrandFathering",
            data: dataObj,
            success: function (res) {
                if (res != undefined) {
                    if (res.status == true) {
                        alert('Record saved successfully!');
                        $("#GrandFathersTabledatalisId").html("");
                        GetAllGrandFathering();
                    } else if (res.statusCode==0) {
                        alert(res.msg)
                        return
                    }
                    else if (res.statusCode == -1) {
                        alert('Error occurred :' + res.msg)
                        return
                    }
                }
                
             
                return
                result = JSON.stringify(res);
                if (res.userInformationViewModel.grandFathersListViewModel.length > 0) {
                    for (var i = 0; i < res.userInformationViewModel.grandFathersListViewModel.length; i++) {
                        var ID = res.userInformationViewModel.grandFathersListViewModel[i].Id;
                        var UsersID = res.userInformationViewModel.grandFathersListViewModel[i].UsersID;
                        var RankName = res.userInformationViewModel.grandFathersListViewModel[i].RankName;

                        var ExpireDate = res.userInformationViewModel.grandFathersListViewModel[i].ExpireDate;

                        var Reason = res.userInformationViewModel.grandFathersListViewModel[i].Reason;

                        $("#GrandFathersTabledatalisId").append("<tr>"
                            + "<td>" + RankName + "</td><td>" + ExpireDate + "</td><td>" + Reason + "</td><td><a class='admin_btn' href='javascript:void(0)' id='btnDeleteGrandFather' data-GrandFathersId=" + ID + " data-UsersID=" + UsersID + ">Delete</a></td></tr>");
                    }
                }
                else {
                    $("#GrandFathersdiv").html('');
                    $("#GrandFathersdiv").append("<p align='center'> No records found.</p>");
                }
            },

        });
    }
});

$(document).ready(function () {

    GetAllGrandFathering();
    let activeTab = getUrlParameter('ActiveTab');
    if (activeTab == 'CSJTab') {
        onCsjTabClick();
    } else if (activeTab == 'CommissionsTab') {
        onCommissionTabClick();
    } else if (activeTab == 'MemberInfonTab') {
        onMemberInfoTabClick();
    }
    

});

$(document).on("click", "#btnDeleteGrandFather", function () {


    var GrandFatheringID = $(this).attr("data-GrandFathersId");
    var UsersID = $(this).attr("data-UsersID");

    if (confirm("Are you sure you want to delete this?")) {

        var dataObj = {};

        dataObj.UsersID = UsersID;
        dataObj.GrandFatheringID = GrandFatheringID;

        $.ajax({
            type: "POST",
            url: "/Admin/DeleteGrandFathering",
            data: dataObj,
            success: function (response) {

                GetAllGrandFathering();
            }

        });
    }
});





$(document).on("change", "#ExpireDateId", function () {
    var ExpireDate = $("#ExpireDateId").val();
    if ($.isEmptyObject(ExpireDate)) {

        $("#ExpireDateIdErrormsg").show();
    }
    else {
        $("#ExpireDateIdErrormsg").hide();
    }

});


function GetAllGrandFathering() {
    $("#GrandFathersdiv").show();
    $("#GrandFathersTabledatalisId").html("");

    var hdnUserID = $("#hdnUserID").val();



    $.ajax({
        type: "GET",
        url: "/Admin/GetAllGrandFathering?UserId=" + hdnUserID,
        data: "json",
        success: function (res) {
            debugger
            console.log('GF', res)
            result = JSON.stringify(res);
            if (res.userInformationViewModel.grandFathersListViewModel.length > 0) {
                for (var i = 0; i < res.userInformationViewModel.grandFathersListViewModel.length; i++) {
                    var ID = res.userInformationViewModel.grandFathersListViewModel[i].Id;
                    var UsersID = res.userInformationViewModel.grandFathersListViewModel[i].UsersID;
                    var RankName = res.userInformationViewModel.grandFathersListViewModel[i].RankName;

                    var ExpireDate = res.userInformationViewModel.grandFathersListViewModel[i].ExpireDate;

                    var Reason = res.userInformationViewModel.grandFathersListViewModel[i].Reason;

                    $("#GrandFathersTabledatalisId").append("<tr>"
                        + "<td>" + RankName + "</td><td>" + ExpireDate + "</td><td>" + Reason + "</td><td><a class='admin_btn' href='javascript:void(0)' id='btnDeleteGrandFather' data-GrandFathersId=" + ID + " data-UsersID=" + UsersID + ">Delete</a></td></tr>");
                }
            }
            else {
                $("#GrandFathersTabledatalisId").html('');
                $("#GrandFathersTabledatalisId").append("<p align='center'> No records found.</p>");
            }
        },

    });
}






$(document).on("change", "#DialogueId", function () {

    var ReplyText = $(this).val();
    $("#DialogueTextAreaFor").text(ReplyText);

});



$(document).on("click", "#personalinfo1", function () {
    var Id = $(this).attr("data-Id");
    $.ajax({
        type: "GET",
        url: "/Admin/GetPersonalInformation?InfoId=" + Id,
        dataType: "html",
        success: function (data) {
            debugger;
            $('#myModalMyInfo01').html(data);
            $('#infofield01').modal('show');
        },
        error: function () {
            alert("No Data to view");
        }
    });
});
$(document).on("click", "#personalinfo2", function () {
    var Id = $(this).attr("data-Id");
    $.ajax({
        type: "GET",
        url: "/Admin/GetPersonalInformation?InfoId=" + Id,
        dataType: "html",
        success: function (data) {
            $('#myModalMyInfo02').html(data);
            $('#infofield02').modal('show');
        },
        error: function () {
            alert("No Data to view");
        }
    });
});

$(document).ready(function () {

    function Accept() {
        //  __doPostBack('<%=GridView1.UniqueID%>', 'Placement$');
    }
    function Reject() {
        //  hdnMovingUsername.value = -1;
        var modal = document.getElementById("myModalPlacementToolPlaceHere");
        modal.style.display = "none";
        return false;
    }
    function PlaceUser(el) {

        var rowData = el.parentNode.parentNode;
        var rowIndex = rowData.rowIndex;
        var ri = parseInt(rowIndex);
        //var hdnMovingUsername = document.getElementById("hdnUserName");

        //hdnMovingUsername.value = GridView1.rows[ri].cells[0].innerText;


        var modal = document.getElementById("myModalPlacementToolPlaceHere");

        var span = document.getElementsByClassName("close")[0];
        modal.style.display = "block";
        span.onclick = function () { modal.style.display = "none"; }
        return false;
    }


    $(document).on("click", "#btnQTEUpdatedRankHistory", function () {

        var UserId = $(this).attr("data-UserId");
        var TabName = $(this).attr("data-TabName");
        QTEUpdated(UserId, TabName);
    });
    $(document).on("click", "#btnQTEUpdatedUserInfoDetails", function () {

        var UserId = $(this).attr("data-UserId");
        var TabName = $(this).attr("data-TabName");
        QTEUpdated(UserId, TabName);
    });
    function QTEUpdated(UserId, TabName) {
        $.ajax({
            type: "POST",
            url: "/Admin/QTEUpdated?UserId=" + UserId + "&TabName=" + TabName,
            dataType: "json",
            success: function (data) {
                if (TabName == 'RankHistoryFromTree') {
                    $('#RankHistorymsgQTEUpdatedId').show();
                    $("#RankHistorymsgQTEUpdatedId").html(data);
                }
                else if (TabName == 'UserInfoDetailsFromTree') {
                    $('#UserInfoDetailsmsgQTEUpdatedId').show();
                    $("#UserInfoDetailsmsgQTEUpdatedId").html(data);
                }
            },

        });
    }
});

function onCommissionTabClick() {
    let UserId = IntParse(getUrlParameter('UserId'));

    showLoader('comm_payment');
    showLoader('CommDetTable');
    showLoader('pendingCommissions')
    showLoader('paidCommissionTable')
    $.ajax({
        url: '/Admin/HasW9?UserId=' + UserId,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            if (data.status) {
                $('#w9Text').text('Yes');
            } else {
                $('#w9Text').text('No');
            }
            $('#total_fc_earned').text(' Total Amount Earned: $ ' + data.amount);

            $('#comListBody').html('');
            $('#comListBody').append(data.comList);

            $('#pendingCommissionsBody').html('');
            if (data != undefined && data.pendingCommissions != undefined) {
                $('#pendingCommissionsBody').append(data.pendingCommissions);
            }
            $('#pendingSpanTotal').text('Total Pending Amount: $ ' + data.pendingAmount);

            $('#paidCommissionTableBody').html('');
            if (data != undefined && data.paidCommissionsContent != undefined) {
                $('#paidCommissionTableBody').append(data.paidCommissionsContent);
            }
            $('#paidSpanTotal').text('Total Amount Paid: $ ' + data.paidAmount);

            hideLoader('comm_payment');
            hideLoader('CommDetTable');
            hideLoader('pendingCommissions')
            hideLoader('paidCommissionTable')
        },
        error: function () {
            hideLoader('comm_payment');
            hideLoader('CommDetTable');
            hideLoader('pendingCommissions')
            hideLoader('paidCommissionTable')
        }
    });

}
function commissionByMonth(year, type, theMonth, monthName) {
    var userName = $('#selectedUserName').val();
    let UserId = IntParse(getUrlParameter('UserId'));
    if (type == 'Year') {
        var theYear = IntParse(year)
        $('#commissionInfoByYear').modal('show');
        $('#year_header').text('Summary of the year ' + theYear + ' for user :' + userName);
        showLoader('commissionInfoByYear')
        $.ajax({
            url: '/Admin/GetCommissionByYear?userId=' + UserId + '&Year=' + theYear,
            type: "GET",
            success: function (data) {
                debugger
                console.log('res', data);
                $('#comListMonthBody').html('');
                if (data != undefined && data.content != undefined) {
                    $('#comListMonthBody').append(data.content);
                }

                hideLoader('commissionInfoByYear');

            },
            error: function () {
                hideLoader('commissionInfoByYear');
            }
        });
    } else if (type == 'Month') {
        var theYear = IntParse(year)
        $('#commissionInfoByMonth').modal('show');
        $('#month_header').text('Commissions Summary for ' + monthName + ', ' + theYear + ' for user :' + userName);
        showLoader('commissionInfoByMonth')
        $.ajax({
            url: '/Admin/GetCommissionByMonth?userId=' + UserId + '&Year=' + theYear + '&Month=' + theMonth,
            type: "GET",
            success: function (data) {
                debugger
                console.log('res', data);
                $('#comListMonthCommBody').html('');
                if (data != undefined && data.content != undefined) {
                    $('#comListMonthCommBody').append(data.content);
                }
                $('#comListMonthCommDetailBody').html('');
                if (data != undefined && data.detailsContent != undefined) {
                    $('#comListMonthCommDetailBody').append(data.detailsContent);
                }
               
                hideLoader('commissionInfoByMonth');

            },
            error: function () {
                hideLoader('commissionInfoByMonth');
            }
        });
    }

}
function getCommissionByOrderId(orderId) {
    showLoader('commBodyByOrderId');
    let UserId = IntParse(getUrlParameter('UserId'));
    $('#commissionByOrderId').modal('show');
    $('#commBodyByOrderId').html('');
    $.ajax({
        url: '/Admin/GetCommisionsByOrderId?UserId=' + UserId + '&OrderId=' + orderId ,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            $('#order_header').text('Commission details for Order ID :' + orderId)
            $('#commBodyByOrderId').html('');
            if (data != undefined && data.content != undefined) {
                $('#commBodyByOrderId').append(data.content);
            }
            hideLoader('commBodyByOrderId');
        },
        error: function () {
            hideLoader('commBodyByOrderId');
        }
    });
}
function onMemberInfoTabClick() {
    getUserOptMessage();
}
function onCsjTabClick() {
    getCsjList();
}

function getUserOptMessage() {
    showLoader('OptUsersMsg');
    $('#btnOptEmailIn').hide();
    $('#btnOptEmailOut').hide();
    $('#btnOptEmailInGen').hide();
    $('#btnOptEmailOutGen').hide();
    $('#btnSmsOptIn').hide();
    $('#btnSmsOptOut').hide();
    $('#btnSmsOptInGen').hide();
    $('#btnSmsOptOutGen').hide();
    let UserId = IntParse(getUrlParameter('UserId'));   
    $.ajax({
        url: '/Admin/OptUser?UserId=' + UserId,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            if (data.status) {
                if (data.CheckEmailOptIn != true ) {
                    $('#emailOpt').text('Opted Out');
                    $('#emailOpt').css('color', 'red');
                    $('#btnOptEmailIn').show();
                   
                } else {
                    $('#emailOpt').text('Opted In');
                    $('#emailOpt').css('color', 'green')
                    $('#btnOptEmailOut').show();
                }
                if (data.emailCountGeneral > 0) {
                    $('#emailOptGeneral').text('Opted Out');
                    $('#emailOptGeneral').css('color', 'red');
                    $('#btnOptEmailInGen').show();
                } else {
                    $('#emailOptGeneral').text('Opted In');
                    $('#emailOptGeneral').css('color', 'green');
                    $('#btnOptEmailOutGen').show();
                }

                if (data.CheckSMSOptIn != true) {
                    $('#smsOpt').text('Opted Out');
                    $('#smsOpt').css('color', 'red');
                    $('#btnSmsOptIn').show();
                } else {
                    $('#smsOpt').text('Opted In');
                    $('#smsOpt').css('color', 'green');
                    $('#btnSmsOptOut').show();
                }
                if (data.SmsCountGeneral > 0) {
                    $('#smsOptGeneral').text('Opted Out');
                    $('#smsOptGeneral').css('color', 'red');
                    $('#btnSmsOptInGen').show();
                } else {
                    $('#smsOptGeneral').text('Opted In');
                    $('#smsOptGeneral').css('color', 'green');
                    $('#btnSmsOptOutGen').show();
                }

                if (data.MobileVerified != true) {
                    $('#smsOpt').text('Mobile Not Verify Yet.');
                    $('#smsOpt').css('color', 'red');

                    $('#smsOptGeneral').text('Mobile Not Verify Yet.');
                    $('#smsOptGeneral').css('color', 'red');

                    $('#btnSmsOptIn').hide();
                    $('#btnSmsOptOut').hide();
                    $('#btnSmsOptInGen').hide();
                    $('#btnSmsOptOutGen').hide();
                }
                
            }
           
            hideLoader('OptUsersMsg');
        },
        error: function () {
            hideLoader('OptUsersMsg');
        }
    });
}
function optUserChange(type) {
    debugger
    var msg = '';
    var opType = '';
    if (type == 'emailOut') {
        msg = 'Are you sure you want to remove this user from all system emails?';
        opType = 'email';
    } else if (type == 'emailIn') {
        msg = 'Are you sure you want to opt-in this user for all system emails?';
        opType = 'email';
    } else if (type == 'emailOutGen') {
        msg = 'Are you sure you want to remove this user from general system emails?';
        opType = 'email';
    } else if (type == 'emailInGen') {
        msg = 'Are you sure you want to opt-in this user for general system emails?';
        opType = 'email';
    }
    else if (type == 'smsOut') {
        msg = 'Are you sure you want to remove this user from all system SMS messages?';
        opType = 'sms';
    } else if (type == 'smsIn') {
        msg = 'Are you sure you want to opt-in this user for all system SMS messages?';
        opType = 'sms';
    } else if (type == 'smsOutGen') {
        msg = 'Are you sure you want to remove this user from general system SMS messages?';
        opType = 'sms';
    } else if (type == 'smsInGen') {
        msg = 'Are you sure you want to opt-in this user for general system SMS messages?';
        opType = 'sms';
    }
    if (confirm(msg)) {
        if (type == 'smsInGen' || type == 'smsIn' || type == 'emailInGen' || type == 'emailIn') {
            OptUserIn(opType,type);
        } else {
            OptUserOut(opType, type);
        }
    } else {

    }
}

function OptUserOut(type,emailSMS) {
    let UserId = IntParse(getUrlParameter('UserId'));
    $.ajax({
        url: '/Admin/OptOutUser?UserId=' + UserId + '&EmailSms=' + type + '&OptType=' + emailSMS,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            hideLoader('OptUsersMsg');
            if (data.status) {
                getUserOptMessage();
            }            
        },
        error: function () {
            hideLoader('OptUsersMsg');
        }
    });
}
function OptUserIn(type, emailSMS) {
    let UserId = IntParse(getUrlParameter('UserId'));
    $.ajax({
        url: '/Admin/OptInUser?UserId=' + UserId + '&EmailSms=' + type + '&OptType=' + emailSMS,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            hideLoader('OptUsersMsg');
            if (data.status) {
                getUserOptMessage();
            }
        },
        error: function () {
            hideLoader('OptUsersMsg');
        }
    });
}
function OptOutofNonSystem() {
    let phone = $('#optOutPhone').val().trim();
    if (phone == undefined || phone == '') {
        alert('Phone no. required.')
        return
    }
    if (phone.length <= 5) {
        return
    }
    $.ajax({
        url: '/Admin/OptOutofNonSystem?Phone=' + phone ,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            hideLoader('OptUsersMsg');
            if (data.status) {
                alert(phone + ' is out of non-system sms');
            }
        },
        error: function () {
            hideLoader('OptUsersMsg');
        }
    });
}

function getCsjList() {
    let UserId = IntParse(getUrlParameter('UserId'));
    showLoader('csjTable')
    $.ajax({
        url: '/Admin/GetCsjComments?UserId=' + UserId,
        type: "GET",
        success: function (data) {
            debugger
            console.log('res', data);
            hideLoader('csjTable');
            if (data.status) {
                $('#csjTableBody').html('');
                $('#csjTableBody').append(data.Content);
            }
        },
        error: function () {
            hideLoader('csjTable');
        }
    });
}

function addCsj() {
    let UserId = IntParse(getUrlParameter('UserId'));
    var Comment = $('#csjInput').val();
    if (Comment == undefined || Comment == '') {
        alert('My record is required.')
        return
    }
    Comment = Comment.trim();
    var obj = {
        UserId: UserId,
        AdminId: 0,
        Comment: Comment
    }
    showLoader('csjDiv')
    $.ajax({
        url: '/Admin/AddCsj',
        type: "POST",
        data: obj,
        success: function (data) {
            debugger
            console.log('res', data);
            hideLoader('csjDiv');
            if (data.status) {
                $('#csjInput').val('');
                getCsjList();
            }
        },
        error: function () {
            hideLoader('csjDiv');
        }
    });
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

