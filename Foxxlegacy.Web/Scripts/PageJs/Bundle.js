﻿

$(document).ready(function () {
    $('.btn-toggle').click(function (e) {
        e.preventDefault();
        $(this).find('.btn').toggleClass('active');
        if ($(this).find('.btn-primary').size() > 0) {
            $(this).find('.btn').toggleClass('btn-primary');
        }


        $(this).find('.btn').toggleClass('btn-default');

    });
    checkAutoShip();
    calculateOrderTotal();
    //$('input[type="number"].qty').keyup(function () {
    //    calculateOrderTotal();        
    //});
    $("input[type=number]").bind('keyup input', function () {
        calculateOrderTotal();
    });
    $('#bundleTable').on('click', '.autoShipBtn', function (e) {
        e.preventDefault();
        checkAutoShip();
        calculateOrderTotal();
    });
});

function calculateOrderTotal() {
    var orderTotal = 0;
    var totalSaved = 0;
    $('#bundleTable > tbody  > tr.item-tr').each(function (index, tr) {
        var price = floatParse($(this).find("td:eq(1) span.priceTd").text());
        var qty = floatParse($(this).find("td:eq(2) input").val());
        var total = floatParse((price * qty).toFixed(2));

        var saved = 0;
        if (qty > 0) {
            checkAutoShip(); // check for autoship discount
        }
        if ($('#congratsMsg').is(":visible")) {
            saved = qty * 10;
        }
        if (qty > 0) {
            total = total - saved;
        }

        $(this).find("td:eq(3) span.totalTd").text('$' + total);
        $(this).find("td:eq(4) span.currency").text('$');
        $(this).find("td:eq(4) span.youSaved").text(saved);
        $(this).find('td:eq(4)').removeClass('highlight_text')
        if (saved > 0) {
            $(this).find('td:eq(4)').addClass('highlight_text')
        }
        totalSaved = totalSaved + saved;
        orderTotal = orderTotal + total;
    });
    $('#orderTotal').text('$' + orderTotal.toFixed(2));
    $('#discountTotal').text('$' + totalSaved.toFixed(2));
}
function onQtyChange() {
    calculateOrderTotal();
}
function orderNow() {

    var orderTotal = 0;
    var items = [];
    $('#bundleTable > tbody  > tr.item-tr').each(function (index, tr) {
        var ItemId = IntParse($(this).find("td:eq(0) input.itemId").val());
        var price = floatParse($(this).find("td:eq(1) span.priceTd").text());
        var qty = floatParse($(this).find("td:eq(2) input").val());
        var total = floatParse((price * qty).toFixed(2));
        var saved = 0;

        if ($('#congratsMsg').is(":visible")) {
            saved = qty * 10;
        }
        if (qty > 0) {
            total = total - saved;
        }
        $(this).find("td:eq(3) span.totalTd").text('$' + total);

        var autoShipText = $(this).find("td:eq(5) button.active").text();
        var autoShip = autoShipText == 'No' ? false : true;
        orderTotal = orderTotal + total;
        if (qty > 0) {
            var obj = {
                ItemId: ItemId,
                Price: price,
                Qty: qty,
                Total: total,
                AutoShip: autoShip
            }
            items.push(obj);
        }

    });
    var model = {
        OrderTotal: orderTotal,
        Items: items
    }
    debugger
    var urlName = '/' + $('#controllerName').val() + '/' +'OrderNow';
    console.log('model', model)
    $.ajax({
        type: "POST",
        url: urlName,
        data: { 'model': model },
        success: function (data) {
            debugger
            console.log('d', data)
            if (data != undefined) {
                if (data.status == true) {
                    window.location.href = data.RedirectUrl;
                }
            }

        },
        error: function (err) {
            console.log('err', err)
            alert("Error occurred!");
        }
    });
}

function checkAutoShip() {
    var oldOrderAutoShip = IntParse($('#autoShipId').val());
    if (oldOrderAutoShip > 0) {
        $('#congratsMsg').show();
        $('#congratsMsg2').show();
    } else {
        var found = false;
        $('#bundleTable > tbody  > tr.item-tr').each(function (index, tr) {
            var autoShipText = $(this).find("td:eq(5) button.active").text();
            var qty = floatParse($(this).find("td:eq(2) input").val());
            if (qty > 0) {
                var autoShip = autoShipText == 'No' ? false : true;
                if (autoShip == true) {
                    found = true;
                    $('#congratsMsg').show();
                    $('#congratsMsg2').show();
                    return
                }
            }
        });
        if (found == false) {
            $('#congratsMsg').hide();
            $('#congratsMsg2').hide();
        }

    }

}