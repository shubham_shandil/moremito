﻿$(document).ready(function () {


    $(document).on("click", "#btnGetPassword", function () {

        var UserName = $('#UserName').val();
        GetPasswordByName(UserName);
    });


    function GetPasswordByName(UserName) {
      
       
        var Password = '';
        $.ajax({
            type: "GET",
            url: "/Admin/GetPasswordByName?UserName=" + UserName,
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            cache: false,
            async: true,
            success: function (result) {
               
                $("#lblPassword").html("");
                Password = result.data.Password;
                $("#lblPassword").html(Password);


            }
        });
    };

});