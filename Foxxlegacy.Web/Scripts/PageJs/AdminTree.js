﻿$(document).on("click", "#personalinfo", function () {
    var Id = $(this).attr("data-Id");
    showLoader('genDiv')
    $.ajax({
        type: "GET",
        url: "/AdminPermission/GetPersonalInformation?InfoId=" + Id,
        dataType: "html",
        success: function (data) {
            $('#myModalMyInfo').html('');
            $('#myModalMyInfo').html(data);
            $('#infofield').modal('show');
            hideLoader('genDiv')
        },
        error: function () {
            hideLoader('genDiv')
            alert("No Data to view");

        }
    });
});

$(document).on("click", "#personaltree", function () {
    var Id = $(this).attr("data-Id");
    $('#userTree').modal('show');
    $.ajax({
        type: "GET",
        // url: "/Members/GetPersonalInformation?InfoId=" + Id,
        url: "/members/fetchreferralnetworkuserdata?id=" + Id,
        // dataType: "html",
        success: function (data) {
            debugger
            $('#userTreeInfo').html('');
            $('#userTreeInfo').html(data);
            $('#userTree').modal('show');
        },
        error: function () {
            alert("No Data to view");
        }
    });
});
$(document).on("click", "#personalCurrenttree", function () {
    var Id = $(this).attr("data-Id");
    $('#userTree').modal('show');
    $.ajax({
        type: "GET",
        // url: "/Members/GetPersonalInformation?InfoId=" + Id,
        url: "/AdminPermission/FetchReferralNetworkCurrentUserData?id=" + Id,
        // dataType: "html",
        success: function (data) {
            debugger
            $('#userTreeInfo').html('');
            $('#userTreeInfo').html(data);
            $('#userTree').modal('show');
        },
        error: function () {
            alert("No Data to view");
        }
    });
});
//$(document).on("click", "button.treebutton", function (e) {
//    debugger
//    return
//    var _this = $(e.currentTarget),
//        _liObj = _this.closest("li"),
//        _childNode = _liObj.find("ul.child-tree-node");


//    var id = parseInt(_this.data("id")),
//        _sign = $.trim(_this.html());
//    if (id > 0 && _sign == "+") {
//        console.log('idd', id)
//        $.ajax({
//            url: "/members/fetchreferralnetworkuserdata?id=" + id,
//            type: "GET",
//            success: function (response) {
//                if (typeof response == "string" && response.length > 0) {
//                    _liObj.append(response);
//                    _this.html("-");
//                }
//            }
//        });
//    } else {
//        _childNode.remove();
//        _this.html("+");
//    }


//});
$('#adminRefferalNetwork').on('click', 'button.treebutton', function (e) {
    var _this = $(e.currentTarget),
        _liObj = _this.closest("li"),
        _childNode = _liObj.find("ul.child-tree-node");


    var id = parseInt(_this.data("id")),
        _sign = $.trim(_this.html());
    if (id > 0 && _sign == "+") {
        console.log('idddd', id)
        $.ajax({
            url: "/members/fetchreferralnetworkuserdata?id=" + id,
            type: "GET",
            success: function (response) {
                if (typeof response == "string" && response.length > 0) {
                    _liObj.append(response);
                    _this.html("-");
                }
            }
        });
    } else {
        _childNode.remove();
        _this.html("+");
    }
});
$(document).on("click", "button#btnSearchUser", function (e) {
    var _this = $(e.currentTarget),
        _searchErrorTxt = $("p#searchErrorTxt"),
        _txtBxUsrNm = $("input#txtBxUsrname"),
        _tblDivSection = $("div#PlacementUserPanel"),
        _tblObject = $("#placementUserTblBody");

    var userName = _txtBxUsrNm.val(),
        UserId = _this.data("id");

    _tblObject.html('');
    _tblDivSection.hide();
    _searchErrorTxt.hide();

    if (userName != null && userName.length > 0) {
        var moveUserName = $('#MoveUserId').text();
        $.ajax({
            url: "/members/getuserdetails?u=" + userName + "&MoveUserId=" + UserId,
            type: "GET",
            dataType: "JSON",
            success: function (response) {
                var trArray = [];
                if (typeof response === "object" && response.length > 0) {
                    $.each(response, function (i, user) {
                        console.log('user', user)
                        trArray.push('<tr>');
                        trArray.push('<td>' + user.UserName + '</td>');
                        trArray.push('<td>' + user.FirstName + '</td>');
                        trArray.push('<td>' + user.LastName + '</td>');
                        trArray.push('<td>' + user.Phone + '</td>');
                        trArray.push('<td><button type="button" class="btn btn-primary placementmap" data-sid="' + user.ID + '" data-cid="' + user.CustomerID + '" data-uid="' + UserId + '">Place User ' + moveUserName + ' here</button></td>');

                        trArray.push('</tr>');
                    });
                    _tblObject.html(trArray.join(''));
                    if (trArray.length > 0) {
                        _tblDivSection.show();
                    }
                } else {
                    _searchErrorTxt.show();
                }
            },
        });
    }

});
$(document).on("click", "button.placementmap", function (e) {
    var _this = $(e.currentTarget);
    if (confirm("Are you sure to place that user ?")) {
        var _UserId = _this.data('uid'),
            _SponsorId = _this.data('sid');

        if (parseInt(_UserId) > 0 && parseInt(_SponsorId) > 0) {
            showLoader('infofield')
            $.ajax({
                url: "/members/updateuserplacement",
                data: { "SponsorUserId": _SponsorId, "UserId": _UserId },
                type: "POST",
                beforeSend: function () {

                },
                success: function (response) {
                    debugger
                    hideLoader('infofield')
                    alert(response);
                    $('#infofield').modal('hide');
                    loadAdminTree();
                }
            });
        }
    }
});




