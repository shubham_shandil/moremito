﻿$(document).on("click", "#btnSearch", function () {

    $("#searchdiv").show();
    $("#PanelSweeperAnyone").hide();

    $("#PanelSweeper").hide();
    var Search = $("#SearchId").val();


    $("#searchdatatable").html("");
    var dataObj = {};

    dataObj.Search = Search;

    $.ajax({
        type: "GET",
        url: "/FoxxCash/Search",
        data: dataObj,
        success: function (res) {

            if (res.transferFCListViewModel.length > 0) {
                for (var i = 0; i < res.transferFCListViewModel.length; i++) {

                    var theUsersID = res.transferFCListViewModel[i].theUsersID;
                    var UserName = res.transferFCListViewModel[i].UserName;
                    var FirstName = res.transferFCListViewModel[i].FirstName;
                    var LastName = res.transferFCListViewModel[i].LastName;
                    var Email = res.transferFCListViewModel[i].Email;
                    var Phone = res.transferFCListViewModel[i].Phone;
                    var InMyLevel = res.transferFCListViewModel[i].InMyLevel;

                    $("#searchdatatable").append("<tr><td style='white-space:nowrap'><a class='outbtn' href='javascript: void (0)' id='SendFoxxCashId' data-RecipientUserID=" + theUsersID + ">Send Foxx Cash</a></td>"
                        + "<td style='white-space:nowrap'>" + theUsersID + "</td><td style='white-space:nowrap'>" + UserName + "</td><td style='white-space:nowrap'>" + FirstName + "</td><td style='white-space:nowrap'>" + LastName + "</td><td style='white-space:nowrap'>" + Email + "</td><td style='white-space:nowrap'>" + Phone + "</td><td style='white-space:nowrap'>" + InMyLevel + "</td></tr>");


                }
            }
            else {
                $("#searchdatatable").append("<tr><td colspan='7' style='text-align:center;'>Your search did not return any results.</td></tr>");
            }


        }
    });

});

$(document).on("click", "#SendFoxxCashId", function () {

    $("#PanelSweeper").show();
    var RecipientUserID = $(this).attr("data-RecipientUserID");
    $("#RecipientUserID").val(RecipientUserID);
    var dataObj = {};

    dataObj.UserId = RecipientUserID;
    $.ajax({
        type: "GET",
        url: "/FoxxCash/GetUserDetailsbyUsersID",
        data: dataObj,
        success: function (response) {
            result = JSON.stringify(response);
            $("#PanelSweeper").show();
            $("#AmounttoTransferdiv").show();
            $("#btnChangeRecipient").show();
            $('#Recipientdiv').html(response.RecipientsUsername);
            $('#RecipientNamediv').html(response.RecipientName);
            $('#RecipientEmaildiv').html(response.RecipientEmail);
            $('#RecipientPhonediv').val(response.RecipientPhone);
            $("#RecipientsUsernameId").val(response.RecipientsUsername);
        },

    });
});

$(document).on("click", "#btnChangeRecipient", function () {

    $("#PanelSweeper").hide();
});

$(document).on("click", "#btnValidateTransfer", function () {

    $("#ValidUserNameErrorMessagediv").html("");
    $("#PanelSweeper").hide();
    var RecipientsUsername = $("#txtTransferUserName").val();

    if ($.isEmptyObject(RecipientsUsername)) {
        $("#ValidUserNameErrorMessagediv").html("<p>User Name is Required</p>");
        $("#PanelSweeper").hide();
    }
    else if (CheckValidName(RecipientsUsername) == false) {
        $("#ValidUserNameErrorMessagediv").html("<p>No spaces or special characters. Only dash, underscore, alphabetical and numeric characters</p>");
        $("#PanelSweeper").hide();
    }
    else {
        var dataObj = {};

        dataObj.UserName = RecipientsUsername;
        $.ajax({
            type: "GET",
            url: "/FoxxCash/GetUserDetailsbyUsersID",
            data: dataObj,
            success: function (response) {
                result = JSON.stringify(response);

                if ($.isEmptyObject(response.RecipientName)) {
                    $("#ValidUserNameErrorMessagediv").html("<p>User Name does not exist, please try again.</p>");
                    $("#PanelSweeper").hide();
                }
                else if (response.SelfUser == true) {
                    $("#ValidUserNameErrorMessagediv").html("<p> You cannot send Foxx Cash to yourself</p>");
                    $("#PanelSweeper").hide();
                }
                else {
                    $("#PanelSweeper").show();
                    $("#AmounttoTransferdiv").show();
                    $('#RecipientNamediv').html(response.RecipientName);
                    $('#RecipientEmaildiv').html(response.RecipientEmail);
                    $('#RecipientPhonediv').val(response.RecipientPhone);
                }

            },

        });
    }



});

$(document).on("click", "#btnSendFCAnyone", function () {

    $("#PanelSweeperAnyone").show();
    $("#searchdiv").hide();
    $("#PanelSweeper").show();
    $("#PanelSweeper").hide();
});

function CheckValidName(name) {
    var response = false;
    var regexp = "^([a-zA-Z])[a-zA-Z0-9-_]*$";
    var check = name;
    if (check.search(regexp) === -1) {
        response = false;
    }
    else {
        response = true;
    }

    return response;
}

function DoAmountCheck() {

    var AmountToSweep = $("#txtAmountToSweep").val();
    var Balance = $("#BalanceId").val();
    var RecipientsUsername = $("#RecipientsUsernameId").val();
    $("#lblSweeperLabel").html("");
    $("#AmounttoTransferErrorMessagediv").html("");
    if (parseFloat(AmountToSweep) <= parseFloat(Balance)) {
        $("#lblSweeperLabel").html("");

        if (parseFloat(AmountToSweep) < parseFloat(5)) {
            $("#AmounttoTransferErrorMessagediv").append("<p>You must send at least 5 dollars.</p>");
            $("#lblSweeperLabel").html("");
        }
        else {
            $("#pnlConfirm").show();
            $("#AmounttoTransferErrorMessagediv").append("You are about to transfer " + AmountToSweep + " Foxx Cash to " + RecipientsUsername.toUpperCase() + ".  Do you wish to continue?");
        }
    }
    else {

        $("#pnlConfirm").hide();
        $("#lblSweeperLabel").append("<p>* You must select an amount that is less than or equal to your Total Available to Transfer.</p>");
    }
}

$(document).on("click", "#btnAmounttoTransfer", function () {

    DoAmountCheck();
});

$(document).on("click", "#btnConfirmYes", function () {
    $("#lblSweeperLabel").html("");
    var AmountToSweep = $("#txtAmountToSweep").val();
    var Balance = $("#BalanceId").val();
    var RecipientsUsername = "";

    if (!$.isEmptyObject($("#txtTransferUserName").val())) {
        RecipientsUsername = $("#txtTransferUserName").val();
    }
    else {
        RecipientsUsername = $("#RecipientsUsernameId").val();
    }

    var RecipientUserID = $("#RecipientUserID").val();
    if (parseFloat(AmountToSweep) <= parseFloat(Balance)) {


        var dataObj = {};

        dataObj.Amount = parseFloat(AmountToSweep);
        dataObj.CommissionsSweptDescription = "Request for " + parseFloat(AmountToSweep, 2) + " to be transferred to downline user " + RecipientsUsername;
        dataObj.RecipientUserID = RecipientUserID;
        dataObj.CommissionsDescription = "" + parseFloat(AmountToSweep, 2) + " Foxx Cash transferred from";
        $.ajax({
            type: "POST",
            url: "/FoxxCash/ConfirmYes",
            data: dataObj,
            success: function (response) {
                result = JSON.stringify(response);

                $("#lblSweeperLabel").append("<p>Your transfer amount of <font color='red'>" + parseFloat(AmountToSweep) + " is recorded.</font> Please check Payment History for the status of this request.</p>");

                alert("" + parseFloat(AmountToSweep) + "  Foxx Cash was sent to  " + RecipientsUsername);
                window.location.reload();
            },

        });



    }
    else {
        $("#PanelSweeper").show();

        $("#lblSweeperLabel").append("You must select an amount that is less than or equal to your Total Available to Transfer.");
    }
});
$(document).on("click", "#btnConfirmNo", function () {
    $("#pnlConfirm").hide();
    $("#PanelSweeper").show();
    $("#AmounttoTransferErrorMessagediv").html("");


});