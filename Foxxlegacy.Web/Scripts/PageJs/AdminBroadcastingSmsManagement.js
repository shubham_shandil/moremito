﻿

$(document).ready(function () {
    var UserCountDisplay = function () {
        var _sendingTypeObj = $("input.SendingType:checked"),
            _SpecifiedUserNameObj = $("input#SpecificUserName"),
            _DownlineUserNameObj = $("input#DownlineUserName"),
            _UserRankDdlObj = $("select#UserRankId"),
            _UsernameTablePanel = $("div#UsernameTablePanel"),
            _UsernameTbody = new Object(),
            _DownlineUserTablePanel = $("div#DownlineUserTablePanel"),
            _DownlineUserTbody = new Object(),
            _countField = $("span#BroadcastEmailRecipientCountField");

        if (_UsernameTablePanel.length > 0) {
            var _table = _UsernameTablePanel.children("table");
            if (_table.length > 0) {
                _UsernameTbody = _table.children("tbody");
            }
        }

        if (_DownlineUserTablePanel.length > 0) {
            var _table = _DownlineUserTablePanel.children("table");
            if (_table.length > 0) {
                _DownlineUserTbody = _table.children("tbody");
            }
        }


        var TypeId = _sendingTypeObj.length > 0 ? parseInt(_sendingTypeObj.val()) : 0,
            _specifiedUserName = _SpecifiedUserNameObj.length && _SpecifiedUserNameObj.val() != null > 0 ? "&u=" + _SpecifiedUserNameObj.val() : "",
            _downlineUserName = _DownlineUserNameObj.length && _DownlineUserNameObj.val() != null > 0 ? "&u=" + _DownlineUserNameObj.val() : "",
            _rankid = _UserRankDdlObj.length && _UserRankDdlObj.val() != null > 0 ? "&r=" + _UserRankDdlObj.val() : "";


        if (TypeId > 0) {
            $.ajax({
                url: "/admin/getusercount?id=" + TypeId + _specifiedUserName + _downlineUserName + _rankid,
                type: "GET",
                dataType: "JSON",
                async: true,
                success: function (response) {
                    if ("count" in response)
                        _countField.html(response.count);

                    if ("list" in response) {
                        var _tblStringArray = [];
                        if (response.list.length > 0) {
                            $.each(response.list, function (i, e) {
                                _tblStringArray.push('<tr>');
                                _tblStringArray.push('<td>' + e.ID + '</td>');
                                _tblStringArray.push('<td>' + e.UserName + '</td>');
                                _tblStringArray.push('<td>' + e.FirstName + '</td>');
                                _tblStringArray.push('<td>' + e.LastName + '</td>');
                                _tblStringArray.push('<td>' + e.Email + '</td>');
                                _tblStringArray.push('<td></td>');
                                _tblStringArray.push('</tr>');
                            });

                            if (TypeId == 2) {
                                _UsernameTbody.html(_tblStringArray.join(''));
                                _UsernameTablePanel.show();
                                _DownlineUserTbody.html('');
                                _DownlineUserTablePanel.hide();
                            } else if (TypeId == 3) {
                                _UsernameTbody.html('');
                                _UsernameTablePanel.hide();
                                _DownlineUserTbody.html(_tblStringArray.join(''));
                                _DownlineUserTablePanel.show();
                            } else {
                                _UsernameTbody.html('');
                                _UsernameTablePanel.hide();
                                _DownlineUserTbody.html('');
                                _DownlineUserTablePanel.hide();
                            }
                        }
                    }
                }
            });
        }

    };

    $(document).on("change", "input.SendingType", function (e) {
        var _this = $(e.currentTarget),
            _countField = $("span#BroadcastEmailRecipientCountField"),
            _UsernamePanel = $("div#UsernamePanel"),
            _DownlineUserPanel = $("div#DownlineUserPanel"),
            _UserRankPanel = $("div#UserRankPanel");
        if (_this.is(":checked")) {
            var TypeId = parseInt(_this.val());
            if (TypeId > 0) {
                switch (TypeId) {
                    case 2: {
                        _countField.html(0);
                        _UsernamePanel.show();
                        _DownlineUserPanel.hide();
                        _UserRankPanel.hide();
                    } break;
                    case 3: {
                        _countField.html(0);
                        _UsernamePanel.hide();
                        _DownlineUserPanel.show();
                        _UserRankPanel.hide();
                    } break;
                    case 4: case 5: {
                        _countField.html(0);
                        _UsernamePanel.hide();
                        _DownlineUserPanel.hide();
                        _UserRankPanel.show();
                    } break;
                    default: {
                        UserCountDisplay();
                        _UsernamePanel.hide();
                        _DownlineUserPanel.hide();
                        _UserRankPanel.hide();
                    } break;
                }
            }
        }

    });

    $(document).on("change", "select#SmsTemplateId", function (e) {
        var _ddlObj = $(e.currentTarget),
            _templateId = parseInt(_ddlObj.val()),
            _smsBodyObj = $("#SmsBody");

        if (_templateId > 0) {
            $.ajax({
                type: "GET",
                url: "/admin/getsmsbody?id=" + _templateId,
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                cache: false,
                async: true,
                success: function (result) {
                    _smsBodyObj.val(result);
                }
            });
        } else {
            _smsBodyObj.val("");
        }

    });

    $(document).on("click", "input#btnGetUserNameCount", function (e) {
        var _elem = $("input#SpecificUserName");
        if (_elem.val().length > 0) {
            UserCountDisplay();
        }
    });

    $(document).on("click", "input#btnGetDownlineUserNameCount", function (e) {
        var _elem = $("input#DownlineUserName");
        if (_elem.val().length > 0) {
            UserCountDisplay();
        }
    });

    $(document).on("click", "input#btnGetRankUserCount", function (e) {
        var _elem = $("select#UserRankId");
        if (parseInt(_elem.val()) > 0) {
            UserCountDisplay();
        }
    });



    $("#ckbCheckAll").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });
    

    $('#datetimepicker').datetimepicker();


    $(document).on("click", "#ViewEmailDetailsSentEmailReport", function () {

        var html = $(this).closest('tr').find('.email-body').text();
        $('#SentEmailReportModalInfo').html(html);
        $('#SentEmailReportModal').modal('show');
    });


});