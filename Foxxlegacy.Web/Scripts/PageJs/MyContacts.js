﻿$(document).on("click", "#MyContactsSelectId", function () {


    var Id = $(this).attr("data-ID");

    $.ajax({
        type: "GET",
        url: "/Contacts/GetMyContactsById?ID=" + Id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            result = JSON.stringify(response);
            $('#Origin').val(response.contactUsListViewModel[0].Origin);
            $('#EventDate').val(response.contactUsListViewModel[0].EventDate);
            $('#Name').val(response.contactUsListViewModel[0].Name);
            $('#EmailId').val(response.contactUsListViewModel[0].EmailId);
            $('#Phone').val(response.contactUsListViewModel[0].Phone);

            $('#Subject').val(response.contactUsListViewModel[0].Subject);
            $('#Details').val(response.contactUsListViewModel[0].Details);
            $('#ContactsId').val(response.contactUsListViewModel[0].ID);
            var Notes = response.contactUsListViewModel[0].Notes;
            if ($.isEmptyObject(Notes)) {
                $("#NotesdivTextAreaFor").show();
                $("#NotesdivTextBoxFor").hide();
                $("#btnMyContactsUpdate").text("Update");
            }
            else {
                $('#NotesTextbox').val(response.contactUsListViewModel[0].Notes);
                $("#NotesdivTextBoxFor").show();
                $("#NotesdivTextAreaFor").hide();
                $("#btnMyContactsUpdate").text("Edit");
            }
        },

    });
});


$(document).on("click", "#btnMyContactsUpdate", function () {
    var btnType = $("#btnMyContactsUpdate").text();
    if (btnType == "Edit") {
        var Notes = $('#NotesTextbox').val();
        $("#NotesTextAreaFor").val(Notes);
        $("#NotesdivTextAreaFor").show();
        $("#NotesdivTextBoxFor").hide();
        $("#btnMyContactsUpdate").text("Update");
    }
    else {

        var Id = $("#ContactsId").val();

        var Notes = $("#NotesTextAreaFor").val();

        var dataObj = {};
        if (Id != undefined && Id != null) {
            dataObj.ID = Id;
            dataObj.Notes = Notes;
            $.ajax({
                type: "POST",
                url: "/Contacts/UpdateMyContacts",
                data: dataObj,
                success: function (response) {

                    $("#NotesdivTextBoxFor").show();
                    $("#NotesdivTextAreaFor").hide();
                    $('#NotesTextbox').val(response.contactUsListViewModel[0].Notes);
                    $("#btnMyContactsUpdate").text("Edit");
                }
            });
        }
    }
});