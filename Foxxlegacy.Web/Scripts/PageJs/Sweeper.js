﻿

$(document).ready(function () {
   
});
function loadUserCurrentPaymentMethods() {

    showLoader('addNewPaymentMethodContent');
    $.ajax({
        url: '/Members/LoadUserPaymentMethods?MethodId='+0,
        type: "GET",
        success: function (res) {
            console.log('res', res);
            if (res.status) {

            }
            hideLoader('addNewPaymentMethodContent');
        },
        error: function () {
            hideLoader('addNewPaymentMethodContent');
        }
    });
}

function addNewUserPaymentMethod() {
    $('#addNewPaymentMethod').modal('show');
    showLoader('addNewPaymentMethodContent');
    $.ajax({
        url: '/Members/LoadUserPaymentMethods',
        type: "GET",
        success: function (res) {
            console.log('res', res);
            if (res.status) {

                $('#paymentBody').html('');
                $('#paymentBody').append(res.content);
            }
            hideLoader('addNewPaymentMethodContent');
        },
        error: function () {
            hideLoader('addNewPaymentMethodContent');
        }
    });
}
function paymentDetails(id, name) {
    showLoader('methodDetails');
    $.ajax({
        url: '/Members/LoadPaymentMethods?Id=' + id,
        type: "GET",
        success: function (res) {
            console.log('res', res);
            if (res.status) {
                debugger
                var d = res.data;
                $('#methodDetails').modal('show');
                $('#method_header').text('Details for ' + name);
                $('#paymentName').text(name);
                $('#lbl_field1_val').text(d.tb_field1);
                $('#lbl_field2_val').text(d.tb_field2);
                $('#lbl_field3_val').text(d.tb_field3);
                $('#lbl_field4_val').text(d.tb_field4);
                $('#lbl_field5_val').text(d.tb_field5);
                $('#lbl_field6_val').text(d.tb_field6);

            }
            hideLoader('methodDetails');
        },
        error: function () {
            hideLoader('methodDetails');
        }
    });
}
function addThisMethod(id, name) {
    showLoader('addMethodModal');
    $.ajax({
        url: '/Members/LoadPaymentMethods?Id=' + id,
        type: "GET",
        success: function (res) {
            console.log('res', res);
            if (res.status) {
                $('#paymentMethodId').val(id);
                var d = res.data;
                $('#addMethodModal').modal('show');
                $('#method_header_add').text('Enter details for ' + name);

                $('#lbl_field1').text(d.tb_field1);
                $('#lbl_field2').text(d.tb_field2);
                $('#lbl_field3').text(d.tb_field3);
                $('#lbl_field4').text(d.tb_field4);
                $('#lbl_field5').text(d.tb_field5);
                $('#lbl_field6').text(d.tb_field6);

                $('#tb_field1_val').attr('required', d.field1_req);
                $('#tb_field2_val').attr('required', d.field2_req);
                $('#tb_field3_val').attr('required', d.field3_req);
                $('#tb_field4_val').attr('required', d.field4_req);
                $('#tb_field5_val').attr('required', d.field5_req);
                $('#tb_field6_val').attr('required', d.field6_req);

            }
            hideLoader('addMethodModal');
        },
        error: function () {
            hideLoader('addMethodModal');
        }
    });
}
function savePaymentMethod() {
    debugger
    var UserProfileId = IntParse($('#paymentMethodId').val());
    var tb1 = '#tb_field1_val';
    var tb2 = '#tb_field2_val';
    var tb3 = '#tb_field3_val';
    var tb4 = '#tb_field4_val';
    var tb5 = '#tb_field5_val';
    var tb6 = '#tb_field6_val';

    var f1 = $(tb1).val();
    var f2 = $(tb2).val();
    var f3 = $(tb3).val();
    var f4 = $(tb4).val();
    var f5 = $(tb5).val();
    var f6 = $(tb6).val();

    var f1_req = validateInput(f1, tb1, 'lbl_field1');
    if (!f1_req) {
        return
    }
    var f2_req = validateInput(f2, tb2, 'lbl_field2');
    if (!f2_req) {
        return
    }
    var f3_req = validateInput(f3, tb3, 'lbl_field3');
    if (!f3_req) {
        return
    }
    var f4_req = validateInput(f4, tb4, 'lbl_field4');
    if (!f4_req) {
        return
    }
    var f5_req = validateInput(f5, tb5, 'lbl_field5');
    if (!f5_req) {
        return
    }
    var f6_req = validateInput(f6, tb6, 'lbl_field6');
    if (!f6_req) {
        return
    }

    var model = {
        UserProfileId: UserProfileId,
        tb_field1: f1,
        tb_field2: f2,
        tb_field3: f3,
        tb_field4: f4,
        tb_field5: f5,
        tb_field6: f6
    }
    showLoader('addPaymentMethodForm');
    $.ajax({
        url: '/Members/SavePaymentMethod',
        type: "POST",
        data: {
            method: model
        },
        success: function (res) {
            console.log('res', res);
            if (res.status) {
                alert('Record added successfully!');
                $('#paymentMethodId').val(0);
                $('#addPaymentMethodForm')[0].reset();
            }
            hideLoader('addPaymentMethodForm');
        },
        error: function () {
            hideLoader('addPaymentMethodForm');
        }
    });
}
function validateInput(f1, tb, lbl_field1) {
    if ($(tb).prop('required')) {
        if (f1 == undefined || f1 == '') {
            alert($('#' + lbl_field1).text() + ' is required !');
            $(tb).focus();
            return false;
        }
    }
    return true;
}