﻿$(document).ready(function () {


    function isNumber(evt) {
        evt = evt ? evt : window.event;
        var charCode = evt.which ? evt.which : evt.keyCode;
        if (charCode >= 48 && charCode <= 57) {
            return true;
        }
        return false;
    }

    function isDecimalNumber(evt) {
        evt = evt ? evt : window.event;
        var charCode = evt.which ? evt.which : evt.keyCode;
        if ((charCode >= 48 && charCode <= 57) || charCode == 46) {
            //var _this = $(evt.currentTarget);
            //var value = _this.val();
            //if (value.length > 0) {
            //    if (Number.isInteger(value))
            //        _this.val(parseFloat(value));
            //    else
            //        _this.val(parseFloat(value).toFixed(2));
            //}
            return true;
        }
        return false;
    }
    function isChar(evt) {
        evt = evt ? evt : window.event;
        var charCode = evt.which ? evt.which : evt.keyCode;
        if ((charCode >= 65 && charCode <= 122) || charCode == 32 || charCode == 0) {
            return true;
        }
        return false;
    }

    $(document).on("keypress", ".validnumeric", function (e) {
        return isNumber(e);
    });

    $(document).on("keypress", ".validChar", function (e) {
        return isChar(e);
    });

});

