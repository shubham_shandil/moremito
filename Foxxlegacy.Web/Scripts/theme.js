// JavaScript Document




//Product Carousel
var cc = $('#banner');
cc.owlCarousel({
    autoplay: true,
    loop: true,
    nav: true,
    dots: true,
    //stagePadding: 150,
    animateOut: 'fadeOut',
    //slideSpeed: 200,
    autoplayTimeout: 10000,
    items: 1,
    //margin: 25,
    navText: ['<i class="fa fa-arrow-left" aria-hidden="true"></i>', '<i class="fa fa-arrow-right" aria-hidden="true"></i>'],

    responsive: {
        // breakpoint from 0 up
        0: {
            items: 1,
            nav: true,
            //stagePadding: 50,

        },
        // breakpoint from 480 up
        480: {
            items: 1,
            nav: true,
            //stagePadding: 50,

        },
        // breakpoint from 768 up
        768: {
            items: 1,
            nav: true,

        }
    }
});

