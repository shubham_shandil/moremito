﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Foxxlegacy.Web.Filters
{
    public class FoxxExceptionFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                var exceptionMessage = filterContext.Exception.InnerException == null ? filterContext.Exception.Message : filterContext.Exception.InnerException.Message;
                var stackTrace = filterContext.Exception.StackTrace.Substring(0, 1000); ;
                var controllerName = filterContext.RouteData.Values["controller"].ToString();
                var actionName = filterContext.RouteData.Values["action"].ToString();

                string Message = "Controller: " + controllerName + ", Action:" + actionName +
                                 " Error Message : " + exceptionMessage;
                
                filterContext.ExceptionHandled = true;
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary{{ "controller", "Error" },
                                          { "action", "Index" },
                                          {"ExceptionMsg",Message }

                                         });
            }
        }
    }
}