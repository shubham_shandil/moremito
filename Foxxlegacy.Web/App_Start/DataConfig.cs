﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Web.Script.Serialization;

namespace Foxxlegacy
{
    public class DataConfig
    {
    }

    public static class Base64
    {
        public static string Encode(object data)
        {
            string encoding = string.Empty;
            string ENCRYPTION_SALT = "@#^%#$&*^&*^$#%%&^@#@#*^$&%$%(*&$*%_";
            try
            {
                if (data is null) return string.Empty;
                byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(string.Concat(ENCRYPTION_SALT, data));
                encoding = Convert.ToBase64String(plainTextBytes);
            }
            catch (Exception ex)
            {
                _ = ex.Message;
            }
            return encoding;

        }

        public static string Decode(string encodeData)
        {
            string ENCRYPTION_SALT = "@#^%#$&*^&*^$#%%&^@#@#*^$&%$%(*&$*%_";
            string decoding = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(encodeData))
                {
                    var base64EncodedBytes = Convert.FromBase64String(encodeData);
                    decoding = System.Text.Encoding.UTF8.GetString(base64EncodedBytes).Replace(ENCRYPTION_SALT, "");
                }
            }
            catch (Exception ex)
            {
                _ = ex.Message;
            }
            return decoding;
        }

        public static string FileToEncode(string path)
        {
            string encoding = string.Empty;
            try
            {
                if (string.IsNullOrEmpty(path)) return string.Empty;
                if (File.Exists(path))
                {
                    byte[] plainTextBytes = File.ReadAllBytes(path);
                    encoding = Convert.ToBase64String(plainTextBytes);
                }
            }
            catch (Exception ex)
            {
                _ = ex.Message;
            }
            return encoding;

        }

        public static string FileToEncode(byte[] byteArray)
        {
            string encoding = string.Empty;
            try
            {
                if (byteArray.Length == 0) return string.Empty;
                else
                    encoding = Convert.ToBase64String(byteArray);
            }
            catch (Exception ex)
            {
                _ = ex.Message;
            }
            return encoding;

        }

        public static bool DecodeToFile(string path, string encodeData)
        {
            bool decoding = false;
            try
            {
                if (!string.IsNullOrEmpty(path) && !string.IsNullOrEmpty(encodeData))
                {
                    var base64EncodedBytes = Convert.FromBase64String(encodeData);
                    File.WriteAllBytes(path, base64EncodedBytes);

                    if (File.Exists(path))
                        decoding = true;
                }
            }
            catch (Exception ex)
            {
                _ = ex.Message;
            }
            return decoding;
        }



    }

    public static partial class Data
    {
        private static readonly Random random = new Random();
        private static readonly JavaScriptSerializer jsObject = new JavaScriptSerializer();

        public static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 5)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string ObjectToString(object obj)
        {
            string _data = string.Empty;
            if (!Equals(obj, null))
                _data = jsObject.Serialize(obj);
            return _data;
        }

        public static T StringToObject<T>(string str)
        {
            return jsObject.Deserialize<T>(str);
        }

        public static object StringToObject(string str, Type type)
        {
            return jsObject.Deserialize(str, type);
        }

    }

    
}