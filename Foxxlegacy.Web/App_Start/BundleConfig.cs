﻿using System.Web;
using System.Web.Optimization;

namespace Foxxlegacy.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css"));



            bundles.Add(new ScriptBundle("~/bundles/Js/EmailNotificationSettings").Include(
                "~/Scripts/PageJs/EmailNotificationSettings.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/Js/TextNotificationSettings").Include(
               "~/Scripts/PageJs/TextNotificationSettings.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/Js/ContactAdmin").Include(
              "~/Scripts/PageJs/ContactAdmin.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Js/Personal").Include(
              "~/Scripts/PageJs/Personal.js"
              ));


            bundles.Add(new ScriptBundle("~/bundles/Js/MyContacts").Include(
              "~/Scripts/PageJs/MyContacts.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Js/FAQ").Include(
              "~/Scripts/PageJs/FAQ.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Js/FoxxCashDetails").Include(
              "~/Scripts/PageJs/FoxxCashDetails.js"
              ));

            bundles.Add(new ScriptBundle("~/bundles/Js/TransferFC").Include(
              "~/Scripts/PageJs/TransferFC.js"
              ));


            bundles.Add(new ScriptBundle("~/bundles/Js/UserInfo").Include(
             "~/Scripts/PageJs/UserInfo.js"
             ));

            bundles.Add(new ScriptBundle("~/bundles/Js/CreateNewCommonResponse").Include(
            "~/Scripts/PageJs/CreateNewCommonResponse.js"
            ));
            bundles.Add(new ScriptBundle("~/bundles/Js/AdminMember").Include(
           "~/Scripts/PageJs/AdminMember.js"
           ));
            bundles.Add(new ScriptBundle("~/bundles/Js/AdminEditor").Include(
        "~/Scripts/PageJs/AdminEditor.js"
        ));

            bundles.Add(new ScriptBundle("~/bundles/Js/AdminMEditor").Include(
       "~/Scripts/PageJs/AdminMEditor.js"
       ));

            bundles.Add(new ScriptBundle("~/bundles/Js/Utilities").Include(
      "~/Scripts/PageJs/Utilities.js"
      ));

            bundles.Add(new ScriptBundle("~/bundles/Js/Join").Include(
      "~/Scripts/PageJs/Join.js"
      ));

            bundles.Add(new ScriptBundle("~/bundles/Js/AdminBroadcastingEmailManagement").Include(
                "~/Scripts/PageJs/AdminBroadcastingEmailManagement.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/Js/AdminBroadcastingSmsManagement").Include(
                "~/Scripts/PageJs/AdminBroadcastingSmsManagement.js"
            ));

        }
    }
}
