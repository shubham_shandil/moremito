﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Foxxlegacy
{
    public class SessionExistAttribute : ActionFilterAttribute
    {
    }

    public class SessionData
    {
        private static HttpSessionState _sessionContext;

        public static long GetUserId()
        {
            long _userId = 0;
            _sessionContext = HttpContext.Current.Session;
            if (_sessionContext.Count > 3)
            {
                if (!object.Equals(_sessionContext["USER_ID"], null))
                {
                    if (_sessionContext["USER_ID"] is long)
                    {
                        _userId = Convert.ToInt32(_sessionContext["USER_ID"]);
                    }
                    else
                    {
                        throw new Exception("Session User Id should be Integer.");
                    }
                }
                else
                {
                    throw new Exception("Session User Id Missing.");
                }
            }
            return _userId;
        }

        public static long GetGuestId()
        {
            long _Id = 0;
            _sessionContext = HttpContext.Current.Session;
            if (!object.Equals(_sessionContext["GUEST_ID"], null))
            {
                if (_sessionContext["GUEST_ID"] is long)
                {
                    _Id = Convert.ToInt32(_sessionContext["GUEST_ID"]);
                }
                else
                {
                    throw new Exception("Session Guest Id should be Integer.");
                }
            }
            return _Id;
        }

        public static LoggedinUserDetails GetUserDetails()
        {
            LoggedinUserDetails userDetails = new LoggedinUserDetails();
            _sessionContext = HttpContext.Current.Session;
            if (_sessionContext.Count > 3)
            {

                if (SessionData.ContainKey("UserName"))
                    userDetails.UserName = Convert.ToString(_sessionContext["UserName"]);
                if (SessionData.ContainKey("Password"))
                    userDetails.Password = Convert.ToString(_sessionContext["Password"]);
                if (SessionData.ContainKey("Address"))
                    userDetails.Address = Convert.ToString(_sessionContext["Address"]);
                if (SessionData.ContainKey("FirstName"))
                    userDetails.FirstName = Convert.ToString(_sessionContext["FirstName"]);
                if (SessionData.ContainKey("LastName"))
                    userDetails.LastName = Convert.ToString(_sessionContext["LastName"]);
                if (SessionData.ContainKey("City"))
                    userDetails.City = Convert.ToString(_sessionContext["City"]);
                if (SessionData.ContainKey("Email"))
                    userDetails.Email = Convert.ToString(_sessionContext["Email"]);
                if (SessionData.ContainKey("State"))
                    userDetails.State = Convert.ToString(_sessionContext["State"]);
                if (SessionData.ContainKey("Zip"))
                    userDetails.Zip = Convert.ToString(_sessionContext["Zip"]);
                if (SessionData.ContainKey("SignupType"))
                    userDetails.SignupType = Convert.ToString(_sessionContext["SignupType"]);
                


            }
            else
            {
                throw new Exception("Session Missing.");
            }
            return userDetails;
        }

        public static bool ContainKey(string key)
        {
            _sessionContext = HttpContext.Current.Session;
            bool _isAvail = false;
            if (!Equals(_sessionContext[key], null))
                _isAvail = true;
            return _isAvail;
        }
    }

    public class UserDetails : IUserDetails
    {
        public string CurrentLoginType { get; set; }
        public int? UserId { get; set; }
        public int? RoleId { get; set; }
        public int? ProviderId { get; set; }
        public int? UserTypeId { get; set; }
        public string FirstName { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsFirstLogin { get; set; }
        public int? CurrencyId { get; set; }
        public int? CountryId { get; set; }
        public int? CityId { get; set; }
        public int? LanguageId { get; set; }
        public string CustomerType { get; set; }
        public string Uuid { get; set; }
        public string EmiritsId { get; set; }
    }

    interface IUserDetails
    {
        int? UserId { get; set; }
        int? RoleId { get; set; }
        int? ProviderId { get; set; }
        int? UserTypeId { get; set; }
        string FirstName { get; set; }
        string Role { get; set; }
        int? CurrencyId { get; set; }
        int? CountryId { get; set; }
        int? CityId { get; set; }
        int? LanguageId { get; set; }
      
    }
}