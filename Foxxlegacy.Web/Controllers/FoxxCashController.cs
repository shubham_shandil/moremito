﻿
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Foxxlegacy.Services.FoxxCash;
using Foxxlegacy.Services.Home;
using Foxxlegacy.Services.User;
using Foxxlegacy.Services.Commission;
using Foxxlegacy.Web.Helpers;

namespace Foxxlegacy.Web.Controllers
{
    [FoxxAuthentication]
    public class FoxxCashController : BaseController
    {
        #region Fields
        private readonly IFoxxCashService _foxxcashService;
        private readonly IHomeService _homeService;
        private readonly IUserService _userService;
        private readonly ICommissionService _commissionService;

        #endregion

        #region CTor
        public FoxxCashController
            (
            IFoxxCashService foxxcashService, IHomeService homeService, IUserService userService, ICommissionService commissionService

            )
        {
            this._foxxcashService = foxxcashService;
            this._homeService = homeService;
            this._userService = userService;
            this._commissionService = commissionService;
        }
        #endregion
        // GET: FoxxCash
        public ActionResult MyFoxxCashRC()
        {
            MyFoxxCashViewModel model = new MyFoxxCashViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.myFoxxCashViewModelList = _foxxcashService.GetTotals(1, UserId);

                if (model.myFoxxCashViewModelList.Count > 0)
                {
                    if (model.myFoxxCashViewModelList[0].RetVal1 > 0)
                    {
                        model.Earned = model.myFoxxCashViewModelList[0].RetVal1;
                    }
                    if (model.myFoxxCashViewModelList[0].RetVal2 > 0)
                    {
                        model.Swept = model.myFoxxCashViewModelList[0].RetVal2;

                    }
                    model.Balance = (model.Earned - model.Swept);
                }

              //  var data = _commissionService.GetCommision(model);
               // response.data = ConvertPartialViewToString("_commissionView", data);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            //return View(model);
            return View("index");
        }

        public ActionResult GetMyCommissonReport(GetCommissionModel model)
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetCommisions(GetCommissionModel model)
        {
            AjaxResponseViewModel response = new Models.AjaxResponseViewModel();
            try
            {
                long UserId = GetCacheUserInfo().UserId;
                model.UserId = UserId;
                var data = _commissionService.GetCommision(model);
                response.data = ConvertPartialViewToString("_myCommissionView", data);
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return new JsonResult { Data = response, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            
        }
        public ActionResult SweeperHistory()
        {

            SweeperHistoryListViewModel model = new SweeperHistoryListViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.sweeperHistoryListViewModel = _foxxcashService.GetSweeperHistory(1, UserId);



            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }

        public ActionResult TransferFCHistory()
        {
            TransferFCHistoryListViewModel model = new TransferFCHistoryListViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.transferFCHistoryListViewModel = _foxxcashService.GetTransferFCHistory(1, UserId);
                model.eventsViewModel.WebContentName = "Members_Content_FoxxCash_TransferHistory";
                model.eventsListViewModel = _homeService.GetWebContent(1, model.eventsViewModel);
                if (model.eventsListViewModel != null)
                {
                    model.Content = model.eventsListViewModel.Content;
                }
                model.foxxCashReceivedListViewModel = _foxxcashService.GetTotalFoxxCashReceived(2, UserId);
                if (model.foxxCashReceivedListViewModel.Count > 0)
                {
                    model.TotalReceived = model.foxxCashReceivedListViewModel[0].TotalReceived;
                }
                model.transferFCHistoryListViewModel2 = _foxxcashService.GetTransferFCHistory2(3, UserId);//for 2nd table data list

                model.foxxCashSentListViewModel = _foxxcashService.GetTotalFoxxCashSent(4, UserId);
                if (model.foxxCashSentListViewModel.Count > 0)
                {
                    model.TotalSent = model.foxxCashSentListViewModel[0].TotalSent;
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }

        public ActionResult FoxxCashDetails()
        {


            FoxxCashDetailsViewModel model = new FoxxCashDetailsViewModel();


            try
            {

                long UserId = GetCacheUserInfo().UserId;
                model.UsersID = UserId;
                model.foxxCashDetailsListViewModel = _foxxcashService.FoxxCashDetails1(1, model);
                model.eventsViewModel.WebContentName = "Members_Content_FoxxCashDetails_RC";
                model.eventsListViewModel = _homeService.GetWebContent(1, model.eventsViewModel);
                if (model.eventsListViewModel != null)
                {
                    model.Content = model.eventsListViewModel.Content;
                }
                if (model.foxxCashDetailsListViewModel.Count > 0)
                {
                    for (int i = 0; i < model.foxxCashDetailsListViewModel.Count; i++)
                    {
                        model.TotalEarned = model.TotalEarned + model.foxxCashDetailsListViewModel[i].Total;
                    }
                    model.Year = model.foxxCashDetailsListViewModel[0].TheYear;
                }




            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }


        public JsonResult GetFoxxCashDetailsListByYearMonth(FoxxCashDetailsViewModel model)
        {


            try
            {
                long UserId = GetCacheUserInfo().UserId;
                model.UsersID = UserId;
                if (model.DetailsList == "secondTabledatalist")
                {
                    model.foxxCashDetailsListViewModel2 = _foxxcashService.FoxxCashDetails2(2, model);//bind 2nd Data table list
                }
                if (model.DetailsList == "thirdTabledatalist")
                {
                    model.foxxCashDetailsListViewModel3 = _foxxcashService.FoxxCashDetails3(3, model);//bind 3rd Data table list

                    model.foxxCashDetailsListViewModel4 = _foxxcashService.FoxxCashDetails4(4, model);//bind 4th Data table list
                    model.foxxCashDetailsListViewModel5 = _foxxcashService.FoxxCashDetails5(5, model);//bind 5th Data table list
                }




            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return Json(model, JsonRequestBehavior.AllowGet);


        }

        public ActionResult TransferFC()
        {

            TransferFCViewModel model = new TransferFCViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.userViewModel.UserId = UserId;

                model.userDetailsViewModel = _userService.GetUserDetailsbyUsersID(7, model.userViewModel);

                if (model.userDetailsViewModel != null)
                {
                    model.HasW9 = model.userDetailsViewModel.HasW9 > 0 ? true : false;
                    model.AdminBlockSweep = model.userDetailsViewModel.AdminBlockSweep;

                }

                model.myFoxxCashViewModelList = _foxxcashService.GetTotals(1, UserId);


                if (model.myFoxxCashViewModelList.Count > 0)
                {
                    if (model.myFoxxCashViewModelList[0].RetVal1 > 0)
                    {
                        model.Earned = model.myFoxxCashViewModelList[0].RetVal1;
                    }
                    if (model.myFoxxCashViewModelList[0].RetVal2 > 0)
                    {
                        model.Swept = model.myFoxxCashViewModelList[0].RetVal2;

                    }
                    model.Balance = (model.Earned - model.Swept);
                    if (model.myFoxxCashViewModelList[0].RetVal3 > 0)
                    {
                        model.HasCommissions = true;
                        if (model.HasCommissions == true)//'has commission check
                        {
                            if (model.HasW9 == false)
                            {
                                // return RedirectToAction("FormW9");
                            }
                            else if (model.HasW9 == true)
                            {
                                if (model.Balance > 0)
                                {
                                    if (model.AdminBlockSweep == false)
                                    {
                                        ViewBag.PanelNoSweeper = false;

                                    }
                                    else
                                    {
                                        ViewBag.PanelNoSweeper = true;
                                        ViewBag.NoSweeperNote = "Unable to send Foxx Cash at this time. Please contact customer service about this.";
                                    }
                                }
                                else
                                {
                                    ViewBag.PanelNoSweeper = true;
                                    ViewBag.NoSweeperNote = "You are not able to send Foxx Cash at this time because you have no Foxx Cash to send. Please review Pending Commissions or Payment History.";
                                }
                            }
                        }
                        else//does not have commissions
                        {
                            ViewBag.PanelNoSweeper = true;
                            ViewBag.NoSweeperNote = "You are not able to send Foxx Cash at this time because you have no Foxx Cash to send. Please review Pending Commissions or Payment History.";
                        }

                    }

                }
                model.eventsViewModel.WebContentName = "Members_Content_TransferFoxxCash";
                model.eventsListViewModel = _homeService.GetWebContent(1, model.eventsViewModel);
                if (model.eventsListViewModel != null)
                {
                    model.Content = model.eventsListViewModel.Content;
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }

        [HttpGet]
        public JsonResult Search(TransferFCViewModel model)
        {

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                long UserId = GetCacheUserInfo().UserId;
                //model.UsersID = UserId;
                model.UsersID = 26;
                model.transferFCListViewModel = _foxxcashService.TransferFC(1, model);
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "success";


                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetUserDetailsbyUsersID(UserViewModel modelobj)
        {

            TransferFCViewModel model = new TransferFCViewModel();
            try
            {
                long LogginUserId = GetCacheUserInfo().UserId;

                model.userDetailsViewModel = _userService.GetUserDetailsbyUsersID(7, modelobj);
                if (model.userDetailsViewModel != null)
                {
                    model.RecipientsUsername = model.userDetailsViewModel.UserName;

                    model.RecipientName = model.userDetailsViewModel.FirstName + ' ' + model.userDetailsViewModel.LastName;
                    model.RecipientEmail = model.userDetailsViewModel.Email;
                    model.RecipientPhone = model.userDetailsViewModel.Phone;
                    model.RecipientUserID = model.userDetailsViewModel.RecipientUserID;

                    if (model.RecipientUserID == LogginUserId)
                    {
                        model.SelfUser = true;
                    }
                    else
                    {
                        model.SelfUser = false;
                    }

                }


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ConfirmYes(TransferFCViewModel model)
        {

            SqlResponseBaseModel _baseModel1 = new SqlResponseBaseModel();
            SqlResponseBaseModel _baseModel2 = new SqlResponseBaseModel();
            SqlResponseBaseModel _baseModel3 = new SqlResponseBaseModel();

            try
            {
                long UserId = GetCacheUserInfo().UserId;
                model.UsersID = UserId;
                //model.UsersID = 26;

                // NewCommissionsSweptRecord section
                model.commissionsSweptViewModel.UsersID = UserId;
                model.commissionsSweptViewModel.Amount = model.Amount;
                model.commissionsSweptViewModel.Description = model.CommissionsSweptDescription;
                _baseModel1 = _foxxcashService.NewCommissionsSweptRecord(1, model.commissionsSweptViewModel);
                // NewCommissionsSweptRecord section

                //NewCommissionsRecord section

                model.commissionsViewModel.CommissionTypesID = 34;
                model.commissionsViewModel.Amount = model.Amount;
                model.commissionsViewModel.Description = model.CommissionsDescription + " " + GetCacheUserInfo().UserName;
                model.commissionsListViewModel = _foxxcashService.setCommissionCodeAndName(2, model.commissionsViewModel);
                if (model.commissionsListViewModel != null)
                {
                    model.commissionsViewModel.Type = model.commissionsListViewModel.CommissionCode;

                }

                // GetDownlineUsersLevel Start Section
                //model.commissionsViewModel.UsersID = model.UsersID;
                model.commissionsViewModel.UsersID = 24;//Pending Need to Done
                model.commissionsViewModel.RecipientUserID = model.RecipientUserID;
                //SP_Commissions ActionId=5 Pending Need to Done
                model.FromLevel = _foxxcashService.GetDownlineUsersLevel(5, model.commissionsViewModel);
                // GetDownlineUsersLevel End Section

                model.commissionsViewModel.FromLevel = model.FromLevel;
                model.commissionsViewModel.UsersID = model.RecipientUserID;

                _baseModel2 = _foxxcashService.NewCommissionsRecord(1, model.commissionsViewModel);
                //NewCommissionsRecord section



                // NewFoxxCashTransferLog sction
                model.RecipientUserID = _foxxcashService.GetRecipientCommissionID(3, model.commissionsViewModel);
                model.commissionsViewModel.UsersID = model.UsersID;
                model.commissionsViewModel.RecipientUserID = model.RecipientUserID;
                model.commissionsViewModel.Amount = model.Amount;
                model.commissionsViewModel.FromCommissionsSweptID = (long)_baseModel1.InsertedId;
                model.commissionsViewModel.RecipientCommissionRecordID = (long)_baseModel2.InsertedId;
                _baseModel3 = _foxxcashService.NewFoxxCashTransferLog(4, model.commissionsViewModel);
                // NewFoxxCashTransferLog sction

                if (_baseModel3.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "success";


                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult FormW9()
        {
            return View();
        }
    }
}