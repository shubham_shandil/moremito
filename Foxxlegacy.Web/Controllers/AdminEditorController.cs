﻿using Foxxlegacy.Services.Admin;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Controllers
{
    [FoxxAuthentication]
    public class AdminEditorController : Controller
    {

        #region Fields
        private readonly IAdminService _adminService;
        private readonly IMembersService _membersService;
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;

        #endregion

        #region CTor
        public AdminEditorController
            (
            IAdminService adminService, IMembersService membersService, IUserService userService, ICommonService commonService

            )
        {
            this._adminService = adminService;
            this._membersService = membersService;
            this._userService = userService;
            this._commonService = commonService;

        }
        #endregion
        // GET: AdminEditor
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult InsertDeeplinkDetails(AdminEditorViewModel adminEditorModel)
        {
            int ActionId = 0;
            if (adminEditorModel.DeeplinkId == 0)
            {
                ActionId = 2;
            }
            else if (adminEditorModel.DeeplinkId != 0)
            {
                ActionId = 3;
            }

            var UpdateStatus = _adminService.InsertUpdateDeeplinkDetails(ActionId, adminEditorModel);
            if (UpdateStatus != null)
            {
                if (UpdateStatus.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your  data has been inserted successfully.";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                }

            }
            long ActiveTab = 5;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
            //return View("AdminEditor");
        }

        public ActionResult DeleteDeeplinkDetails(int DeeplinkId)
        {

            AdminEditorViewModel adminEditorModel = new AdminEditorViewModel();
            adminEditorModel.DeeplinkId = DeeplinkId;
            var UpdateStatus = _adminService.InsertUpdateDeeplinkDetails(4, adminEditorModel);
            if (UpdateStatus != null)
            {
                if (UpdateStatus.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your  data has been deleted successfully.";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                }

            }
            long ActiveTab = 5;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
            //return View("AdminEditor");
        }

        public JsonResult GetDeeplinkData(int DeeplinkId)
        {
            AdminEditorViewModel adminEditorModel = new AdminEditorViewModel();
            DeeplinkDetails Deeplinkmodel = new DeeplinkDetails();
            adminEditorModel.DeeplinkId = DeeplinkId;
            var DeeplinkDetailsData = _adminService.GetDeeplinkDetails(5, adminEditorModel);
            if (DeeplinkDetailsData != null)
            {
                Deeplinkmodel = DeeplinkDetailsData;

            }
            return Json(new { data = Deeplinkmodel }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmailTemplateData(int Id)
        {
            var model = new EmailTemplateDetails();
            var ContentData = _adminService.GetEmailTaskNameList(4, Id);
            if (ContentData != null)
            {
                model = ContentData.FirstOrDefault();

            }
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult InsertUpdateEmailTemplateDetails(AdminEditorViewModel adminEditorModel)
        {
            int ActionId = 0;
            if (adminEditorModel.Task != null)
            {
                ActionId = 1;
            }
            else if (adminEditorModel.emailTemplateDetails.EmailId != 0)
            {
                ActionId = 2;
            }

            var UpdateStatus = _adminService.InsertUpdateEmailTemplateDetails(ActionId, adminEditorModel.emailTemplateDetails);
            if (UpdateStatus != null)
            {
                if (UpdateStatus.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your  data has been inserted successfully.";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                }

            }
            long ActiveTab = 2;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
            //return View("AdminEditor");
        }

        [HttpGet]
        public ActionResult CheckUpdateDeeplinkDetails(int DeeplinkId, Boolean IsCheck)
        {

            AdminEditorViewModel adminEditorModel = new AdminEditorViewModel();
            adminEditorModel.DeeplinkId = DeeplinkId;
            adminEditorModel.SelectForQuickLinks = IsCheck;
            var UpdateStatus = _adminService.InsertUpdateDeeplinkDetails(6, adminEditorModel);
            if (UpdateStatus != null)
            {
                if (UpdateStatus.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your  data has been updated successfully.";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                }

            }
            long ActiveTab = 8;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
            //return View("AdminEditor");
        }

        [HttpPost]
        public ActionResult UpdateCats(CatsDetails cats)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _adminService.InsertUpdateCatsDetails(2, cats);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been updated successfully.";
            }
            long ActiveTab = 10;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }
        [HttpPost]
        public ActionResult DeleteCats(CatsDetails cats)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _adminService.InsertUpdateCatsDetails(3, cats);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been deleted successfully.";
            }
            return new EmptyResult();
            //return RedirectToAction("AdLinks", "Members", new { Id = Convert.ToInt64(customer.CampaignId) });
        }
        [HttpPost]
        public ActionResult InsertCatsData(AdminEditorViewModel model)
        {
            CatsDetails cats = new CatsDetails();
            cats.Name = model.CatsName;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _adminService.InsertUpdateCatsDetails(4, cats);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            long ActiveTab = 10;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        [HttpPost]
        public ActionResult FaqUpdateCats(CatsDetails cats)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _adminService.InsertUpdateCatsDetails(7, cats);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been updated successfully.";
            }
            long ActiveTab = 12;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }
        [HttpPost]
        public ActionResult FaqDeleteCats(CatsDetails cats)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _adminService.InsertUpdateCatsDetails(6, cats);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been deleted successfully.";
            }
            return new EmptyResult();
            //return RedirectToAction("AdLinks", "Members", new { Id = Convert.ToInt64(customer.CampaignId) });
        }
        [HttpPost]
        public ActionResult FaqInsertCatsData(AdminEditorViewModel model)
        {
            CatsDetails cats = new CatsDetails();
            cats.Name = model.CatsName;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _adminService.InsertUpdateCatsDetails(5, cats);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            long ActiveTab = 12;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        [HttpPost]
        public ActionResult InsertResourcesItems(AdminEditorViewModel model)
        {
            ResourcesItems item = new ResourcesItems();
            item = model.resourcesItems;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (item.Id > 0)
            {
                _baseModel = _adminService.InsertResourcesItems(3, item);
            }
            else
            {
                _baseModel = _adminService.InsertResourcesItems(2, item);
            }

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            long ActiveTab = 11;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        public ActionResult DeleteResourcesItems(int Id)
        {
            ResourcesItems item = new ResourcesItems();
            item.Id = Id;

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (item.Id > 0)
            {
                _baseModel = _adminService.InsertResourcesItems(4, item);
            }


            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been deleted successfully.";
            }
            long ActiveTab = 11;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        [HttpPost]
        public ActionResult InsertFaqItems(AdminEditorViewModel model)
        {
            Faqs item = new Faqs();
            item = model.FaqsItems;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (item.Id > 0)
            {
                _baseModel = _adminService.InsertFaqItems(4, item);
            }
            else
            {
                _baseModel = _adminService.InsertFaqItems(3, item);
            }

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            long ActiveTab = 13;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        public ActionResult DeleteFaqItems(int Id)
        {

            Faqs item = new Faqs();
            item.Id = Id;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (item.Id > 0)
            {
                _baseModel = _adminService.InsertFaqItems(5, item);
            }

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been deleted successfully.";
            }
            long ActiveTab = 13;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        [HttpPost]
        public ActionResult UpdateFaqItems(AdminEditorViewModel model)
        {
            Faqs item = new Faqs();
            item = model.FaqsItems;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (item.SelectedTeaIds.Length > 0)
            {
                foreach (int Id in item.SelectedTeaIds)
                {
                    item.Id = Id;
                    _baseModel = _adminService.InsertFaqItems(6, item);
                }

            }

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            long ActiveTab = 13;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
        }

        [HttpPost]
        public ActionResult UpdateSmsEventDetails(AdminEditorViewModel model)
        {

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();


            _baseModel = _adminService.InsertUpdateSmsEvent(5, model.SmsEventDetails);

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            int Id = model.SmsEventDetails.Id;
            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = Id });
        }

        [HttpPost]
        public ActionResult InsertSmsEventDetails(AdminEditorViewModel model)
        {
            int Id = 0;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (model.SmsEventDetails.NewEventName != null)
            {
                model.SmsEventDetails.EventName = model.SmsEventDetails.NewEventName;
                model.SmsEventDetails.Keyword = "keyword needed";
                model.SmsEventDetails.ConfirmOptIn = "add text";
                model.SmsEventDetails.ConfirmOptOut = "add text";

            }

            _baseModel = _adminService.InsertUpdateSmsEvent(4, model.SmsEventDetails);

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
                Id = Convert.ToInt32(_baseModel.InsertedId);
            }

            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = Id });
        }

        [HttpPost]
        public ActionResult UpdateSmsEventMessageDetails(SmsEventMessage model)
        {

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();


            _baseModel = _adminService.InsertUpdateSmsEventMessage(7, model);

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been updated successfully.";
            }
            //int Id = model.SmsEventDetails.Id;
            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = 0 });
        }


        public ActionResult DeleteSmsEventDetails(Int32 Id)
        {
            SmsEvent model = new SmsEvent();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            model.Id = Id;

            _baseModel = _adminService.InsertUpdateSmsEvent(8, model);

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been deleted successfully.";
            }

            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = Id });
        }


        public ActionResult DeleteSmsEventMsgDetails(Int32 Id, Int32 SmsId)
        {
            SmsEvent model = new SmsEvent();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            model.Id = Convert.ToInt32(Id);

            _baseModel = _adminService.InsertUpdateSmsEvent(9, model);

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been deleted successfully.";
            }

            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = SmsId });
        }

        public ActionResult InsertSmsEventMessageDetails(Int32 Id)
        {
            SmsEventMessage model = new SmsEventMessage();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

            model.Id = Convert.ToInt32(Id);
            model.Msg = "Select Text";

            _baseModel = _adminService.InsertUpdateSmsEventMessage(6, model);

            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
            }
            //int Id = model.SmsEventDetails.Id;
            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = Id });
        }

        [HttpPost]
        public ActionResult DuplicateMessageInsert(List<FreqTableList> model)
        {
            SmsEventMessage modelobj = new SmsEventMessage();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

            if (model.Count > 0)
            {

                for (int i = 0; i < model.Count; i++)
                {


                    modelobj.Id = Convert.ToInt32(model[i].ID);
                    modelobj.Msg = "Select Text";

                    modelobj.SendTime = model[i].OrderDate;


                    _baseModel = _adminService.InsertUpdateSmsEventMessage(6, modelobj);


                }

                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Data has been inserted successfully.";
                }
            }
            long ActiveTab = 6;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin", new { Id = 0, CategoriesId = 0, SmsId = modelobj.Id });
        }

        public JsonResult PopulateFreqTable(AdminEditorViewModel model)
        {

            AdminEditorViewModel ModelObj = new AdminEditorViewModel();
            // Dim FreqInt As New DateInterval
            var FreqInt = DateTime.Now.Day;
            //FreqInt = DateInterval.Day
            switch (model.ddlFreqPeriod)
            {
                case "1": // 'days


                    FreqInt = DateTime.Now.Day;
                    break;

                case "2":// 'weeks
                    FreqInt = DateTime.Now.Day;
                    break;
                case "3":// 'months
                    FreqInt = DateTime.Now.Month;
                    break;
                default:
                    break;
            }
            DateTime originalDate = Convert.ToDateTime(model.StartingOn);
            long Qty = model.Quantity;
            if (model.ddlFreqPeriod == "2")
            {
                //Qty = (Qty * 7);
                FreqInt = 7;
            }
            else if (model.ddlFreqPeriod == "3")
            {
                //Qty = (Qty * 7);
                FreqInt = DateTime.DaysInMonth(DateTime.Now.Year, originalDate.Month);
            }

            //DateTime NextDate  = FormatDateTime(txtFreqStartDate.Text & " " & SetTimeFromOriginalMessage(GridView2.SelectedValue), DateFormat.GeneralDate)

            string dt = Convert.ToDateTime(model.StartingOn).ToString("MM-dd-yyyy");
            var NextDate = dt.Replace("-", "/") + ' ' + model.Time;

            //For i As Integer = 0 To CInt(txtQty.Text) -1
            //    Dim dr As DataRow = DT.NewRow()
            //    dr("ID") = i
            //    dr("Ordinal") = i + 1
            //    dr("OrderDate") = Date;
            //DT.Rows.Add(dr)
            //    NextDate = DateAdd(FreqInt, Qty, NextDate)
            //Next
            List<FreqTableList> ListViewFreqChart = new List<FreqTableList>();

            for (int i = 0; i < model.Quantity; i++)
            {
                FreqTableList Freqmodel = new FreqTableList();
                Freqmodel.ID = i;
                Freqmodel.Ordinal = i + 1;
                Freqmodel.OrderDate = NextDate;
                ListViewFreqChart.Add(Freqmodel);
                //DateAdd(FreqInt, Qty, NextDate)


                NextDate = originalDate.AddDays(Convert.ToDouble(FreqInt)).ToString("MM-dd-yyyy").Replace("-", "/") + ' ' + model.Time;
                originalDate = originalDate.AddDays(Convert.ToDouble(FreqInt));

                if (model.ddlFreqPeriod == "3")
                {
                    //Qty = (Qty * 7);
                   
                    FreqInt = DateTime.DaysInMonth(DateTime.Now.Year, originalDate.Month);
                }
            }
            ModelObj.FreqTableList = ListViewFreqChart;

            return Json(ModelObj, JsonRequestBehavior.AllowGet);


        }
    }
}