﻿using Foxxlegacy.Services.Admin;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.User;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Contacts;
using System.Globalization;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Data;
using System.Text;
using System.Reflection.Metadata;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text;
using Document = iTextSharp.text.Document;
using static Foxxlegacy.Web.Controllers.MembersController;
using Foxxlegacy.Web.Models;
using OfficeOpenXml;
using System.Configuration;
using Foxxlegacy.Core;
using Foxxlegacy.Services.Commission;
using Foxxlegacy.Web.Helpers;
using Foxxlegacy.Services.FoxxCash;
using AuthorizeNet.Api.Contracts.V1;
using Plivo;

namespace Foxxlegacy.Web.Controllers
{
    [FoxxAuthentication]
    [FoxxAuthorization("admin")]
    public class AdminController : BaseController
    {
        #region Fields
        private readonly IAdminService _adminService;
        private readonly IMembersService _membersService;
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly IContactsService _contactsService;
        private readonly ICommissionService _commissionService;
        private readonly IFoxxCashService _foxxCashService;
        public static List<string> lst = null;
        long itemID = Convert.ToInt32(ConfigurationManager.AppSettings["ItemId"]);
        #endregion

        #region CTor
        public AdminController
            (
            IAdminService adminService, IMembersService membersService, IUserService userService, ICommonService commonService, IContactsService contactsService, ICommissionService commissionService, IFoxxCashService foxxCashService

            )
        {
            this._adminService = adminService;
            this._membersService = membersService;
            this._userService = userService;
            this._commonService = commonService;
            this._contactsService = contactsService;
            this._commissionService = commissionService;
            this._foxxCashService = foxxCashService;
        }
        #endregion

        // GET: Admin
        [HttpGet]
        public ActionResult Home()
        {
            AdminHomeViewModel adminHomeModel = new AdminHomeViewModel();
            var content = _adminService.GetWebContentById(1, "Admin_HomeContent", 0);
            adminHomeModel.AdminHomeContent = content != null ? content.HelpContent : "";
            var GetUserCount = _userService.GetUserDetails(12, "", 0);
            adminHomeModel.UserCount = GetUserCount != null ? GetUserCount.UserCount : 0;

            var GetEmailCount = _userService.GetUserDetails(13, "", 0);
            adminHomeModel.EmailCount = GetEmailCount != null ? GetEmailCount.EmailCount : 0;

            var GetLinkErrors = _adminService.GetLinkErrors(1);
            adminHomeModel.LinkErrorsList = GetLinkErrors != null ? GetLinkErrors : null;
            if (Session["ActiveTab"] != null)
            {
                adminHomeModel.ActiveTab = Convert.ToInt64(Session["ActiveTab"]);
            }
            else
            {
                adminHomeModel.ActiveTab = 0;
            }
            if (TempData.Count == 0)
            {
                TempData["ActiveTab"] = "AdminHomeTab";
            }
            //-------------Admin Home tab Section------------------
            _adminService.CleanupPlivo(3);
            // LongLatForOrders()Pending
            // LongLatForUsers()Pending
            adminHomeModel.MysteryNumber = _adminService.GetMysteryNumber(4);
            //-------------Admin Home tab Section------------------

            return View(adminHomeModel);
        }

        [HttpPost]
        public ActionResult Home(AdminHomeViewModel adminHomeModel)
        {
            if (adminHomeModel.OrderNo != null)
            {
                adminHomeModel.MemberInfo = _adminService.GetMemberInformation(4, adminHomeModel.UserName, adminHomeModel.SearchBy, adminHomeModel.OrderNo).ToList();
            }
            if (adminHomeModel.UserName != null)
            {
                adminHomeModel.MemberInfo = _adminService.GetMemberInformation(3, adminHomeModel.UserName, adminHomeModel.SearchBy, adminHomeModel.OrderNo).ToList();
            }


            adminHomeModel.ActiveTab = 1;

            return View(adminHomeModel);
        }


        public JsonResult GetPasswordByName(string UserName)
        {
            var model = new UserNamePassword();
            var UserInfo = _adminService.GetMemberInformation(5, UserName, null, null).ToList();
            if (UserInfo != null)
            {
                model.UserName = UserInfo[0].UserName;
                model.Password = UserInfo[0].Password;
            }
            TempData["ActiveTab"] = "PasswordsTab";
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeUserName(AdminHomeViewModel adminHomeModel)
        {
            long ActiveTab = 3;
            var isDuplicateUserName = _commonService.CheckDuplicateUserName(adminHomeModel.NewUserName);
            if (isDuplicateUserName == true)
            {
                TempData["ERROR_MESSAGE"] = "New username already exist.";
                Session["ActiveTab"] = ActiveTab;


                TempData["ActiveTab"] = "ChangeUserNameTab";
                return RedirectToAction("AdminMember");

            }
            var userId = _commonService.GetUsersIDbyUserName(9, adminHomeModel.OldUserName);
            if (userId == 0)
            {
                TempData["ERROR_MESSAGE"] = "Old username does not exist.";
                Session["ActiveTab"] = ActiveTab;


                TempData["ActiveTab"] = "ChangeUserNameTab";
                return RedirectToAction("AdminMember");
            }

            var model = new UserNamePassword();
            long UserId = GetCacheUserInfo().UserId;
            var UserInfo = _adminService.ChangeUserName(6, UserId, adminHomeModel.OldUserName, null, null, adminHomeModel.NewUserName);
            if (UserInfo != null)
            {
                model.UserName = adminHomeModel.NewUserName;
                TempData["SUCCESS_MESSAGE"] = "Username is modified successfully.";
            }


            Session["ActiveTab"] = ActiveTab;
            if (adminHomeModel.TabName == "ChangeUserNameTab")
            {

                TempData["ActiveTab"] = "ChangeUserNameTab";
                return RedirectToAction("AdminMember");
            }
            return RedirectToAction("Home");
        }


        [HttpPost]
        public ActionResult ChangeSponsor(AdminHomeViewModel adminHomeModel)
        {
            var model = new UserNamePassword();
            long UserId = GetCacheUserInfo().UserId;
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();

            long UserID = _commonService.GetUsersIDbyUserName(9, adminHomeModel.UserName.Trim());
            long OldSponsor = _commonService.GetUsersIDbyUserName(9, adminHomeModel.OldSponsor.Trim());
            long NewSponsor = _commonService.GetUsersIDbyUserName(9, adminHomeModel.NewSponsor.Trim());
            if (UserID > 0)
            {
                if (OldSponsor != 0)
                {
                    if (NewSponsor != 0)
                    {
                        if (_adminService.GetSponsorsUsersID(1, Convert.ToInt64(UserID), false) == OldSponsor)
                        {
                            baseModel = _adminService.ChangeSponsor(2, UserID, NewSponsor);
                            DownlineCountsViewModel dcmodel = new DownlineCountsViewModel();
                            dcmodel.UsersID = UserID;
                            _commonService.RemoveDownlineCounts(2, dcmodel);
                            AddDownlineCounts(UserID);
                            ZeroCodes(UserID);
                            //Core.SetInitialPlatinumCodes(UserID, NewSponsor)
                            //Core.SetInitialGoldCodes(UserID)
                            //NewCSJRecord(UserID)
                            //lblComplete.Text = "Sponsor Change from " & txtOldSponsor.Text & " to " & txtNewSponsor.Text & " is complete"


                        }
                    }
                }
            }


            if (baseModel != null)
            {
                model.UserName = adminHomeModel.NewUserName;
                TempData["SUCCESS_MESSAGE"] = "Sponsor  Modification successfully.";
            }

            long ActiveTab = 3;
            Session["ActiveTab"] = ActiveTab;
            if (adminHomeModel.TabName == "ChangeSponsorTab")
            {

                TempData["ActiveTab"] = "ChangeSponsorTab";
                return RedirectToAction("AdminMember");
            }
            return RedirectToAction("Home");
        }
        public ActionResult AdminMember()
        {
            AdminHomeViewModel model = new AdminHomeViewModel();
            model.OrderCallListViewModel = _adminService.GetOrderCallList(1);

            model.DeclineCallListViewModel = _adminService.GetOrderCallList(4);
            model.PinCertificateListViewModel = _adminService.GetPinCertificateList(1);
            model.PinsToSendCount = _commonService.PinsToSendCount(2);
            model.CallListViewModel = _adminService.GetCallList(1);
            model.CallToMakeCount = _adminService.CallToMakeCount(2);
            if (TempData.Count == 0)
            {
                TempData["ActiveTab"] = "MemberInfoTab";//Default Tab
            }
            else if (TempData.ContainsKey("ActiveTab"))
            {
                if (TempData["ActiveTab"].ToString() == "ChangeUserNameTab")
                {
                    TempData["ActiveTab"] = "ChangeUserNameTab";
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult AdminMember(AdminHomeViewModel adminHomeModel)
        {
            if (adminHomeModel.OrderNo != null)
            {
                adminHomeModel.MemberInfo = _adminService.GetMemberInformation(4, adminHomeModel.UserName, adminHomeModel.SearchBy, adminHomeModel.OrderNo).ToList();
            }
            if (adminHomeModel.UserName != null)
            {
                adminHomeModel.MemberInfo = _adminService.GetMemberInformation(3, adminHomeModel.UserName, adminHomeModel.SearchBy, adminHomeModel.OrderNo).ToList();
            }

            TempData["ActiveTab"] = "MemberInfoTab";
            //adminHomeModel.ActiveTab = 0;

            return View(adminHomeModel);
        }

        public ActionResult OrderCallList(long OrderID, long theUsersID, string CommandName, long OrderNo)
        {
            var loggedInUser = GetCacheUserInfo();
            if (CommandName == "Called")
            {


                _adminService.UpdateOrderCallList(2, OrderID);

                NewCSJRecord(theUsersID, "Order Call List: complimentary call for order no. " + OrderNo);
            }
            else if (CommandName == "MyCall")
            {

                _adminService.ClaimOrder(3, loggedInUser.UserId, OrderID);


            }



            TempData["ActiveTab"] = "OrderCallListTab";




            return RedirectToAction("AdminMember");
        }

        [HttpPost]
        public ActionResult OrderCallListNoCall(long OrderID, long theUsersID, long OrderNo)
        {
            _adminService.UpdateOrderCallList(2, OrderID);
            NewCSJRecord(theUsersID, "Complimentary call for order no. " + OrderNo + " was Not placed.");
            TempData["ActiveTab"] = "OrderCallListTab";
            return RedirectToAction("AdminMember");
        }


        public ActionResult DeclineCallList(long OrderID, long theUsersID, string CommandName, long OrderNo)
        {
            var loggedInUser = GetCacheUserInfo();
            if (CommandName == "Called")
            {


                _adminService.UpdateOrderCallList(2, OrderID);

                NewCSJRecord(theUsersID, "Order decline call for order no. " + OrderNo);
            }
            else if (CommandName == "MyCall")
            {

                _adminService.ClaimOrder(3, loggedInUser.UserId, OrderID);


            }



            TempData["ActiveTab"] = "DeclineCallListTab";




            return RedirectToAction("AdminMember");
        }


        [HttpPost]
        public ActionResult DeclineCallListNoCall(long OrderID, long theUsersID, long OrderNo)
        {

            _adminService.UpdateOrderCallList(2, OrderID);
            NewCSJRecord(theUsersID, "Order decline call for order no. " + OrderNo + " was Not placed.");
            TempData["ActiveTab"] = "DeclineCallListTab";

            return RedirectToAction("AdminMember");
        }
        public ActionResult Shipping()
        {
            return View();
        }
        public ActionResult Autoship()
        {
            return View();
        }
        public ActionResult Utilities()
        {
            return View();
        }

        public ActionResult Commissions()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetCommisions(GetCommissionModel model)
        {
            string content = string.Empty;
            AjaxResponseViewModel response = new Models.AjaxResponseViewModel();
            try
            {
                //var data = _commissionService.GetCommision(model);
                //response.data = ConvertPartialViewToString("_commissionView", data);
                var data = _commissionService.GetMyCommision(model);
                response.data = ConvertPartialViewToString("_myAdminCommisions", data);
                response.Status = true;
                //  return Json(new { status=true , content }, JsonRequestBehavior.AllowGet);

                return new JsonResult { Data = response, MaxJsonLength = Int32.MaxValue };

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return Json(new { response }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetAllCommisionsByOrderId(int OrderId)
        {
            string msg = string.Empty;
            string content = string.Empty;
            GetCommissionModel model = new GetCommissionModel();

            bool status;
            try
            {
                AdminCommissionViewModel m = new AdminCommissionViewModel();
                long UserId = GetCacheUserInfo().UserId;
                //  model.UserId = UserId;
                model.OrderNo = OrderId;
                // model.UserId = UserId;
                model.SearchType = 1;//_myAdminCommisions
                m = _commissionService.GetOrderMetaData(model);
                //  m._commissionList = data;
                content = ConvertPartialViewToString("_myAdminCommissionInfo", m);
                status = true;
                return Json(new { status, msg, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetUserWiseCommissions(int id)
        {
            decimal TotalCommissionAmount = 0;
            AjaxResponseViewModel response = new Models.AjaxResponseViewModel();
            try
            {
                List<UserWiseCommissionModel> commissions = _commissionService.GetUserWiseCommissions(id);
                if (commissions != null)
                {
                    string content = string.Empty;
                    if (id == 0)
                    {
                        content = ConvertPartialViewToString("_commissionUserWiseView", commissions);
                    }
                    else
                    {
                        content = ConvertPartialViewToString("_commissionAdminUserWiseView", commissions);

                    }
                    TotalCommissionAmount = (decimal)commissions.Sum(x => x.Amount);
                    response.Status = true;
                    response.data = new
                    {
                        content,
                        TotalCommissionAmount
                    };
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
            }
            return new JsonResult { Data = response, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Contact()
        {

            ArchiveCommonResponseViewModel model = new ArchiveCommonResponseViewModel();
            model.archiveCommonResponseListViewModel = _contactsService.GetArchiveCommonResponseList(6, model);
            return View(model);
        }
        public ActionResult CreateNewCommonResponse()
        {
            ArchiveCommonResponseViewModel model = new ArchiveCommonResponseViewModel();
            model.commonResponseList = _contactsService.GetcommonResponseList(7, model);
            return View(model);

        }

        [HttpPost]
        public ActionResult DeleteNewCommonResponse(long? ID)
        {
            ArchiveCommonResponseViewModel model = new ArchiveCommonResponseViewModel();
            model.ID = Convert.ToInt64(ID);
            model.commonResponseList = _contactsService.GetcommonResponseList(8, model);//For Delete

            return RedirectToAction("CreateNewCommonResponse");

        }

        [HttpPost]
        public ActionResult CreateNewCommonResponse(ArchiveCommonResponseViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {



                    SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

                    _baseModel = _contactsService.CreateNewCommonResponseNow(4, model);

                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Successfully created new response.";
                    }
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops! Something went wrong";
            }

            return RedirectToAction("CreateNewCommonResponse");
        }
        public ActionResult Contact_inner(long? ContactsID, long? UserId)
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();


            long SponsorID = 0;

            userInformation = _membersService.GetUserInformation(8, Convert.ToInt64(UserId));
            if (userInformation == null)
            {
                userInformation = new UserInformationViewModel();
            }
            userInformation.promotionRanksViewModel = _userService.GetAllPromotionRanks(1, userInformation == null ? 0 : userInformation.RankID);
            userInformation.stateListViewModel = _commonService.GetStateList(1);
            //userInformation.RankDropDownList = _userService.GetAllPromotionRanks(2, userInformation != null ? userInformation.RankID : 0);
            userInformation.RankDropDownList = _userService.GetAllPromotionRanks(1, 1);
            ViewBag.UserId = UserId;
            ViewBag.UserName = userInformation != null ? userInformation.UserName : "";
            TempData["TempDataContactsID"] = ContactsID;
            TempData["TempDataUserId"] = UserId;
            Session["AutoshipUserId"] = UserId;
            userInformation.userRoleDetailsListViewModel = GetAllRoleName(userInformation.UserName != null ? userInformation.UserName : "Na");
            userInformation.personalsListViewModel = _userService.GetPersonalsList(1, userInformation != null ? userInformation.UserName : "");

            userInformation.messagesListViewModel = _userService.GetMessagesList(1, Convert.ToInt64(UserId));

            //------------------------History of contacts---------------------------
            userInformation.historyofContactsViewModel.historyofContactsListViewModel = _userService.GetHistoryofContactsList(2, Convert.ToInt64(ContactsID));


            if (userInformation.historyofContactsViewModel.historyofContactsListViewModel.Count > 0)
            {
                foreach (var item in userInformation.historyofContactsViewModel.historyofContactsListViewModel)
                {
                    userInformation.historyofContactsViewModel.dialogueList = _userService.GetHistoryofContactsDialogueList(3, item.ID);
                }
            }

            //------------------------History of contacts---------------------------
            //------------------------Contact Details---------------------------
            ContactDetailsViewModel model = new ContactDetailsViewModel();
            model.ContactID = Convert.ToInt64(ContactsID);
            userInformation.contactDetailsListViewModel = _contactsService.ContactDetails(4, model);
            userInformation.contactDetailsViewModel.DialogueList = _commonService.GetAllDropdownlist(1);
            userInformation.contactDetailsViewModel.contactNotesListViewModel = _contactsService.GetContactNotesList(5, model);
            if (ContactsID > 0)
            {
                userInformation.ActiveTab = "ContactDetailsTab";
            }

            //------------------------Contact Details---------------------------
            SponsorID = _adminService.GetSponsorsUsersID(1, Convert.ToInt64(UserId), true);

            if (SponsorID != 0)
            {
                SponsorContactDetailsViewModel SponsorContactDetailsViewModel = PopulateDataSponsorContactDetails(SponsorID);
                userInformation.sponsorContactDetailsViewModel = SponsorContactDetailsViewModel;
                userInformation.UplineContactDetailsViewModelList = _userService.GetUplineUsers(Convert.ToInt32(UserId));
                //if (userInformation.UserName != null)
                //{                    
                //    userInformation.UplineContactDetailsViewModelList = _userService.GetUplineUsers(Convert.ToInt32(UserId));// PopulateDataUplineContactDetailsViewModel(SponsorID, Convert.ToInt64(UserId), userInformation.UserName);
                //}
            }


            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;

            //ViewBag.GetSweepBlockStatus = _adminService.GetSweepBlockStatus(1, Convert.ToInt64(UserId));
            //ViewBag.GetCodeGrandFatheredStatus = _adminService.GetCodeGrandFatheredStatus(1, Convert.ToInt64(UserId));
            //ViewBag.IsUserOnAutoship = _adminService.IsUserOnAutoship(1, Convert.ToInt64(UserId));

            var content = _adminService.GetWebContentById(1, "IBA_Backoffice_TreeView", 0);
            userInformation.TreeContentOrganicPlacement = content != null ? content.HelpContent : "";
            //  userInformation.treeOrganicViewModel.mainList = _adminService.GetMainTree(1, loggedInUser.UserId, 0);
            //  userInformation.treeOrganicViewModel.subMainList = _adminService.GetSubMainTree(2, loggedInUser.UserId, 0);
            userInformation.treePlacementViewModel.mainList = _adminService.GetMainTree(1, UserId ?? 0, 1);
            userInformation.treePlacementViewModel.subMainList = _adminService.GetSubMainTree(2, UserId ?? 0, 1);
            //  userInformation.treeOrganicPlacementViewModel.childMainList = _adminService.GetChildTree(3, UserId??0);

            //userInformation.cSJDetailsViewModel = _adminService.GetCSJDetails(1, Convert.ToInt64(UserId));
            //var JournalHistoricalRecords = _adminService.GetCSJJournalHistoricalRecords(3, Convert.ToInt64(UserId));
            //if (JournalHistoricalRecords != null)
            //{
            //    userInformation.cSJDetailsViewModel.JournalHistoricalRecords = HttpUtility.HtmlEncode(JournalHistoricalRecords);
            //}

            if (lst != null && lst.Count > 0)
            {
                if (lst[0] != null)
                {
                    userInformation.placementTooListlViewModel = _adminService.GetPlacementToolList(1, lst[0], Convert.ToInt64(lst[1]), lst[2], lst[3]).ToList();
                }
                userInformation.ActiveTab = "SearchPlacementToolTab";
            }

            //-------------------Rank History Section----------------------------


            userInformation.rankPromotion = _membersService.GetRankPromotion(2, Convert.ToInt64(UserId));


            if (_userService.IsUserInRole(1, UserName, "member") == true)
            {
                userInformation.MembershipType = "Paid Representative";
            }
            else if (_userService.IsUserInRole(1, UserName, "referringcustomer") == true)
            {
                userInformation.MembershipType = "Referring Customer";
            }
            else if (_userService.IsUserInRole(1, UserName, "friendoffoxx") == true)
            {
                userInformation.MembershipType = "Customer";
            }
            userInformation.HighestRankAchieved = _commonService.HighestRank(Convert.ToInt32(UserId));//  _commonService.RankName(2, _commonService.GetRankID(1, Convert.ToInt64(UserId)));
            userInformation.QTE = getQteNowForMBO(Convert.ToInt64(UserId));

            var GetCurrentRankmodel = _membersService.GetRankPromotion(3, UserId ?? 0);
            if (GetCurrentRankmodel.Count > 0)
            {
                userInformation.GetCurrentRank = _userService.GetCurrentRankByUserId(Convert.ToInt32(UserId));// GetCurrentRankmodel[0].GetCurrentRank;
            }

            //-------------------Rank History Section----------------------------



            //-------------------Orders Start Section-----------------------
            userInformation.adminOrders = _membersService.GetAdminOrders(1, Convert.ToInt64(UserId));
            //-------------------Orders End Section-----------------------

            //-------------------Commission Summary Section-----------------------
            decimal Pending = _foxxCashService.GetCommissionSummaryData(6, Convert.ToInt64(UserId));
            userInformation.commissionSummary.Pending = Pending;

            decimal Earned = _foxxCashService.GetCommissionSummaryData(7, Convert.ToInt64(UserId));
            userInformation.commissionSummary.Earned = Earned;

            decimal Swept = _foxxCashService.GetCommissionSummaryData(8, Convert.ToInt64(UserId));
            userInformation.commissionSummary.Swept = Swept;
            decimal SummarySweepNow = Earned - Pending - Swept;
            decimal Summarybalance = Earned - Swept;
            userInformation.commissionSummary.SummarySweepNow = SummarySweepNow;
            userInformation.commissionSummary.Summarybalance = Summarybalance;

            RankManagementViewModel _rankMgmtReport = _membersService.RankManagementReport(Convert.ToInt32(UserId));
            if (_rankMgmtReport != null)
            {
                userInformation.RankManagement = _rankMgmtReport;
            }


            //---------------- Autoship ---------
            userInformation.AutoshipList = _membersService.GetAutoshipList(1, Convert.ToInt32(UserId));
            userInformation.RecurringOrderHistoryList = _membersService.GetRecurringOrderHistory(2, Convert.ToInt32(Convert.ToInt32(UserId)));
            //For logdin user
            //userInformation.AutoshipList = _membersService.GetAutoshipList(1, Convert.ToInt32(loggedInUser.UserId));
            //userInformation.RecurringOrderHistoryList = _membersService.GetRecurringOrderHistory(2, Convert.ToInt32(loggedInUser.UserId));


            //-----------------Setting All Tab Active Section-----------------------
            if (Request.QueryString["ActiveTab"] != null)
            {
                if (Request.QueryString["ActiveTab"] == "CSJTab")
                {
                    TempData["ActiveTab"] = "CSJTab";

                }
                else if (Request.QueryString["ActiveTab"] == "MemberInfonTab")
                {
                    TempData["ActiveTab"] = "MemberInfonTab";

                }
                else if (Request.QueryString["ActiveTab"] == "PersonalsTab")
                {
                    userInformation.ActiveTab = "PersonalsTab";
                }
                else if (Request.QueryString["ActiveTab"] == "TreeTab")
                {
                    userInformation.ActiveTab = "TreeTab";
                }
                else if (Request.QueryString["ActiveTab"] == "UplineTab")
                {
                    userInformation.ActiveTab = "UplineTab";
                }
                else if (Request.QueryString["ActiveTab"] == "TeamCustomersCountTab")
                {
                    userInformation.ActiveTab = "TeamCustomersCountTab";
                }
                else if (Request.QueryString["ActiveTab"] == "CommissionsTab")
                {
                    userInformation.ActiveTab = "CommissionsTab";
                }
                else if (Request.QueryString["ActiveTab"] == "OrdersTab")
                {
                    userInformation.ActiveTab = "OrdersTab";
                }
                else if (Request.QueryString["ActiveTab"] == "RankTab")
                {
                    TempData["ActiveTab"] = "RankTab";
                }
                else if (Request.QueryString["ActiveTab"] == "MessagesTab")
                {
                    userInformation.ActiveTab = "MessagesTab";
                }



            }
            //-----------------Setting All Tab Active Section-----------------------

            var NopUserId = _userService.GetNopUserId(1, (int)UserId, "");
            var res = _commonService.GetUserAddress("", "", NopUserId);
            if (res != null)
            {
                userInformation.State = res.CountryName + "/" + res.StateName;
            }
            return View(userInformation);
        }

        [HttpGet]
        public PartialViewResult FetchSubMainTree(int id)
        {
            //var model = new List<SubMainList>();
            var model = new GenealogyViewModel();
            if (Request.IsAjaxRequest() && id > 0)
            {
                //model = _adminService.GetSubMainTree(2, loggedInUser.UserId, (int)uId);
                model.ReferralDataList = _membersService.GetReferralNetworkTree(2, id);
            }
            return PartialView("~/Views/Admin/_OrganicPlacementTree.cshtml", model.ReferralDataList);
        }


        public ActionResult Help(string KeyName, string Return)
        {
            //KeyName = "Members_Help_Genealogy";
            string KeyName2 = "Members_Help_Salutation";
            HelpViewModel helpModel = new HelpViewModel();
            if (Return != null)
            {
                helpModel.Return = Return;
            }
            var ContentData = _membersService.GetWebContent(1, KeyName);
            if (ContentData != null)
            {
                helpModel.HelpContent = ContentData.HelpContent;
            }
            var ContentData2 = _membersService.GetWebContent(1, KeyName2);
            if (ContentData2 != null)
            {
                helpModel.HelpContent2 = ContentData2.HelpContent;
            }

            return View(helpModel);
        }

        [HttpGet]
        public ActionResult GetUserPersonalInfo(long UserId)
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();
            try
            {

                userInformation = _membersService.GetUserInformation(8, UserId);
                userInformation.promotionRanksViewModel = _userService.GetAllPromotionRanks(1, userInformation.RankID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_UserInfoEditView", userInformation);
        }

        [HttpPost]
        public ActionResult UpdateMyInfo(UserInformationViewModel userInformation)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            TempData["ActiveTab"] = "MemberInfonTab";
            _baseModel = _membersService.InsertUpdateUserInformation(5, userInformation);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Your information has been updated successfully.";

            }
            else
            {

            }
            return RedirectToAction("Contact_inner", new { UserId = userInformation.UserId });
        }

        [HttpGet]
        public ActionResult GetUserAddressInfo(long UserId)
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();
            try
            {

                userInformation = _membersService.GetUserInformation(8, UserId);
                userInformation.promotionRanksViewModel = _userService.GetAllPromotionRanks(1, userInformation.RankID);
                userInformation.stateListViewModel = _commonService.GetStateList(1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_EditUserAddressView", userInformation);
        }

        [HttpPost]
        public ActionResult UpdateAddressInfo(UserInformationViewModel userInformation)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            TempData["ActiveTab"] = "MemberInfonTab";
            _baseModel = _membersService.InsertUpdateUserInformation(6, userInformation);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE_UpdateAddressInfo"] = "Your information has been updated successfully.";

            }
            else
            {

            }
            return RedirectToAction("Contact_inner", new { UserId = userInformation.UserId });
        }
        public ActionResult ChangeCallStatus(Boolean CallStatus, long UserId)
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();
            try
            {
                TempData["ActiveTab"] = "MemberInfonTab";
                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                userInformation.CourtesyCalls = CallStatus;
                userInformation.UserId = UserId;
                _baseModel = _membersService.InsertUpdateUserInformation(2, userInformation);
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your Courtesy voice calls status has been changed successfully.";

                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops! Something went wrong";
            }
            return RedirectToAction("Contact_inner", new { UserId = userInformation.UserId });

        }


        [HttpPost]
        public JsonResult AddGrandFathering(GrandFathersViewModel model)
        {
            string msg = string.Empty;
            bool status = false;
            int statusCode = -1;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            TempData["ActiveTab"] = "MemberInfonTab";
            long SelectedRank = 0;

            long RankValue = 0;
            long PreviousRank = 0;
            try
            {
                if (model.ExpireDate != null && model.RankID > 0)
                {
                    var loggedInUser = GetCacheUserInfo();
                    model.UserName = loggedInUser.UserName;

                    model.userInformationViewModel = _membersService.GetUserInformation(8, model.UsersID);
                    var GfRank = _membersService.GetGrandFatherRank((int)model.UsersID);
                    RankValue = model.userInformationViewModel.RankID;
                    SelectedRank = model.RankID;
                    PreviousRank = RankValue;
                    model.RankID = SelectedRank;
                    if (RankValue >= SelectedRank)
                    {
                        statusCode = 0;
                        msg = "User has already a higher rank.";
                        return Json(new { status, msg, model, statusCode }, JsonRequestBehavior.AllowGet);
                        //  _baseModel = _adminService.UpdateUserRank(2, model);

                    }
                    if (GfRank > 0)
                    {
                        statusCode = 0;
                        msg = "User has already an active Grand father rank.";
                        return Json(new { status, msg, model, statusCode }, JsonRequestBehavior.AllowGet);
                    }
                    //else
                    {
                        model.PreviousRank = PreviousRank.ToString();
                        model.Comments = "Updated The Grandfathering to rank " + SelectedRank + " for the following reason " + model.Reason + " till " + model.ExpireDate + ".";
                        _baseModel = _adminService.GrandFathersInsertUpdate(1, model);
                        model.userInformationViewModel.grandFathersListViewModel = _adminService.GetGrandFathersList(4, model);
                        status = true;
                        statusCode = 1;
                    }

                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg, model, statusCode }, JsonRequestBehavior.AllowGet);
            //return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllGrandFathering(long? UserId)
        {
            GrandFathersViewModel model = new GrandFathersViewModel();
            model.UsersID = Convert.ToInt64(UserId);

            model.userInformationViewModel.grandFathersListViewModel = _adminService.GetGrandFathersList(4, model);

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteGrandFathering(GrandFathersViewModel model)
        {

            var loggedInUser = GetCacheUserInfo();
            model.UserName = loggedInUser.UserName;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            TempData["ActiveTab"] = "MemberInfonTab";
            model.userInformationViewModel = _membersService.GetUserInformation(8, model.UsersID);
            //model.UserName = model.userInformationViewModel.UserName;
            _baseModel = _adminService.GrandFathersInsertUpdate(5, model);
            // model.userInformationViewModel.grandFathersListViewModel = _adminService.GetGrandFathersList(4, model);

            return Json(model, JsonRequestBehavior.AllowGet);
        }


        private UserRoleDetailsListViewModel GetAllRoleName(string UserName)
        {

            UserRoleDetailsListViewModel model = new UserRoleDetailsListViewModel();
            bool SelectedRole = false;
            model.userRoleListViewModel = _userService.GetAllRoles(2, UserName);

            {

                // var RoleNameList = new List<string> { "Member", "FriendOfFoxx", "ReferringCustomer", "Email_TestGroup" };


                var name = new[] { "Member", "FriendOfFoxx", "ReferringCustomer", "Email_TestGroup" };
                model.userRoleListViewModel = (from e in model.userRoleListViewModel
                                               where e.RoleName == name[0] || e.RoleName == name[1]
                                               || e.RoleName == name[2] || e.RoleName == name[3]
                                               select e).Distinct().ToList();

                //var listforCurrentUser = model.userRoleListViewModel.Where(x => x.UserName == UserName).ToList();
                //foreach (var item in model.userRoleListViewModel)
                //{
                //    item.Check = true;                    
                //}

                //  string NotAvilabeRoleName = string.Empty;


                //model.userRoleListViewModel.Except(listforCurrentUser.Where(x => x.UserName == UserName).ToList());

                //foreach (var item in model.userRoleListViewModel)
                //{
                //    NotAvilabeRoleName = listforCurrentUser.Where(x => x.RoleName != item.RoleName).FirstOrDefault().RoleName;
                //}


                //model.userRoleListViewModel = model.userRoleListViewModel.Where(x => x.RoleName == NotAvilabeRoleName).ToList();


                //foreach (var item in RoleNameList)
                //{
                //    model.RoleNameList.Add(item);
                //    model.IsUserInRoleList.Add(false);
                //    model.RoleIdList.Add("Na");
                //    model.UserIdList.Add(model.userRoleListViewModel[0].UserId);
                //}

                foreach (var item in model.userRoleListViewModel)
                {
                    SelectedRole = _userService.IsUserInRole(1, UserName, item.RoleName);


                    switch (item.RoleName)
                    {
                        case "Member":

                            //model.IsUserInRoleList[0] = (SelectedRole);
                            //model.RoleIdList[0] = (item.RoleId);
                            //model.UserIdList[0] = (item.UserId);

                            model.IsUserInRoleList.Add(SelectedRole);
                            model.RoleNameList.Add("Member");
                            model.RoleIdList.Add(item.RoleId);
                            model.UserIdList.Add(item.UserId);
                            model.UserNameList.Add(item.UserName);

                            break;
                        case "FriendOfFoxx":
                            //model.IsUserInRoleList[1] = (SelectedRole);
                            //model.RoleIdList[1] = (item.RoleId);
                            //model.UserIdList[1] = (item.UserId);
                            model.IsUserInRoleList.Add(SelectedRole);
                            model.RoleNameList.Add("FriendOfFoxx");
                            model.RoleIdList.Add(item.RoleId);
                            model.UserIdList.Add(item.UserId);
                            model.UserNameList.Add(item.UserName);
                            break;
                        case "ReferringCustomer":
                            //model.IsUserInRoleList[2] = (SelectedRole);
                            //model.RoleIdList[2] = (item.RoleId);
                            //model.UserIdList[2] = (item.UserId);
                            model.IsUserInRoleList.Add(SelectedRole);
                            model.RoleNameList.Add("ReferringCustomer");
                            model.RoleIdList.Add(item.RoleId);
                            model.UserIdList.Add(item.UserId);
                            model.UserNameList.Add(item.UserName);
                            break;

                        case "Email_TestGroup":
                            //model.IsUserInRoleList[3] = (SelectedRole);
                            //model.RoleIdList[3] = (item.RoleId);
                            //model.UserIdList[3] = (item.UserId);
                            model.IsUserInRoleList.Add(SelectedRole);
                            model.RoleNameList.Add("Email_TestGroup");
                            model.RoleIdList.Add(item.RoleId);
                            model.UserIdList.Add(item.UserId);
                            model.UserNameList.Add(item.UserName);
                            break;
                            //default:

                            //    model.IsUserInRoleList.Add(SelectedRole);
                            //    model.RoleNameList.Add(item.RoleName);
                            //    model.RoleIdList.Add(item.RoleId);
                            //    model.UserIdList.Add(item.UserId);
                            //    model.UserNameList.Add(item.UserName);
                            //    break;
                    }


                }
                //  foreach (var item in model.RoleNameList)
                //  {
                //      var ob = model.userRoleListViewModel.Where(x =>x.UserName .Contains(UserName)).ToList();
                //  }

                //model.userRoleListViewModel = model.UserNameList=(UserName).ToList().ToList();

            }

            return model;



        }

        [HttpPost]
        public ActionResult ModifyMembershipType(UserInformationViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            string UserId = string.Empty;
            string RoleId = string.Empty;
            bool IsUserInRole = false;
            string RoleName = string.Empty;
            bool enableRole = false;


            try
            {
                for (int i = 0; i < model.userRoleDetailsListViewModel.RoleNameList.Count; i++)
                {
                    UserId = model.userRoleDetailsListViewModel.UserIdList[i];
                    RoleId = model.userRoleDetailsListViewModel.RoleIdList[i];
                    RoleName = model.userRoleDetailsListViewModel.RoleNameList[i];
                    enableRole = model.userRoleDetailsListViewModel.IsUserInRoleList[i];




                    IsUserInRole = _userService.IsUserInRole(1, model.UserName, RoleName);
                    if (enableRole == true && IsUserInRole == false)
                    {
                        _baseModel = _userService.AddRemoveUserRole(3, UserId, RoleId);
                    }
                    else if (enableRole == false && IsUserInRole == true)
                    {
                        _baseModel = _userService.AddRemoveUserRole(4, UserId, RoleId);
                    }

                }
                if (_baseModel != null)
                {
                    TempData["ActiveTab"] = "MemberInfonTab";
                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Permissions changed For User: " + model.UserName;

                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Oops! Something went wrong!";

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Contact_inner", new { UserId = model.ID });
        }


        [HttpPost]
        public ActionResult InsertRecordDialogue(UserInformationViewModel userInformation, string submitRecordDialogue)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();


            switch (submitRecordDialogue)
            {
                case "Record Dialogue":


                    _baseModel = _contactsService.RecordDialogueContactDetails(1, userInformation.contactDetailsViewModel);
                    break;
                case "Archive":
                    _baseModel = _contactsService.RecordDialogueContactDetails(2, userInformation.contactDetailsViewModel);
                    return RedirectToAction("Contact");
                    break;

                case "Admin Private Notes":
                    return RedirectToAction("AdminPrivateNotes", new { ContactsID = userInformation.contactDetailsViewModel.ContactID, UserId = userInformation.contactDetailsViewModel.UsersID });
                    break;

            }

            if (_baseModel.RowAffected > 0)
            {
                //  Core.QMailToMember(GetFromUsersID(ContactID), 0, 31)//Need to Modify
                TempData["SUCCESS_MESSAGE"] = "Your Record has been inserted successfully.";
            }
            else
            {

            }
            return RedirectToAction("Contact_inner", new { ContactsID = userInformation.contactDetailsViewModel.ContactID, UserId = userInformation.contactDetailsViewModel.UsersID });
        }
        [HttpGet]
        public ActionResult AdminPrivateNotes(long? ContactsID, long? UserId)
        {
            ContactDetailsViewModel model = new ContactDetailsViewModel();
            model.ContactID = Convert.ToInt64(ContactsID);
            model.UsersID = Convert.ToInt64(UserId);
            return View(model);
        }
        [HttpPost]
        public ActionResult AdminPrivateNotes(ContactDetailsViewModel model)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

                _baseModel = _contactsService.RecordDialogueContactDetails(3, model);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops! Something went wrong!";
            }

            return RedirectToAction("Contact_inner", new { ContactsID = model.ContactID, UserId = model.UsersID });
        }
        public ActionResult AdminReport()
        {
            return View();
        }
        public ActionResult AdminEmails()
        {
            DropDown.GetEmailSendingProfileTypes();
            return View();
        }

        [HttpGet]
        public ActionResult BroadcastEmail(string TabName)
        {
            var model = new AdminBroadcastEmailDataValidation();
            TempData["ActiveTab"] = "broadcastEmailTab";
            if (TabName == null)
            {
                TabName = "broadcastemail";
            }
            try
            {
                model.EmailSendingTypeList = DropDown.GetEmailSendingProfileTypes();

                var datalist = _adminService.GetEmailTaskNameList(4, 0);
                model.EmailTemplateTypeList.Add(new SelectListItem { Value = Convert.ToString(0), Text = "Please select a template" });
                foreach (var item in datalist)
                {
                    model.EmailTemplateTypeList.Add(new SelectListItem { Value = Convert.ToString(item.EmailId), Text = item.Task });
                }

                var RankList = _userService.GetUserRankList(3);
                foreach (var item in RankList)
                {
                    model.UserRankList.Add(new SelectListItem { Value = Convert.ToString(item.Id), Text = item.RankName });
                }

                var scheduleList = _adminService.GetAdminScheduleBroadcastList(1);
                model.ScheduleList = scheduleList;

                ViewBag.BroadcastEmailmessagesCount = _adminService.BroadcastEmailmessagesCount(3, "");
                if (TempData.ContainsKey("GetBroadcastEmailSearchText") && TabName == "broadcastemail")
                {



                    string SearchText = (string)TempData["GetBroadcastEmailSearchText"];
                    model.GetBroadcastEmailList = _adminService.GetBroadcastEmailGridList(4, SearchText);

                    TempData["GetBroadcastEmailSearchText"] = SearchText;

                    TempData["ActiveTab"] = "broadcastEmailTab";

                }

                //------------------------Broadcast Reports----------------
                if (TempData.ContainsKey("BroadcastReports") && TabName == "BroadcastReports")
                {

                    model.BroadcastReportsViewModel = _adminService.GetBroadcastReportsPendingEmailSmsData(1);
                    TempData["ActiveTab"] = "BroadcastReportsTab";

                }
                //------------------------Broadcast Reports----------------

                //------------------------Sent Email Report----------------
                if (TempData.ContainsKey("SentEmailReport") && TabName == "SentEmailReport")
                {

                    model.SentEmailReport = _adminService.SentEmailReportList(1);
                    TempData["ActiveTab"] = "SentEmailReportTab";

                }

                //------------------------Sent Email Report----------------


                //---------------------EmailReports Broadcast Reports----------------------------

                if (TempData.ContainsKey("EmailReports") && TabName == "EmailReports")
                {
                    string qtype = (string)TempData["EmailReportsqtype"];
                    model.SentEmailReport = _adminService.BroadcastReportsEmailSmsReportList(4, qtype);
                    TempData["ActiveTab"] = "EmailSmsReportsTab";

                }
                //---------------------EmailReports Broadcast Reports----------------------------


                //---------------------SmsReports Broadcast Reports----------------------------

                if (TempData.ContainsKey("SmsReports") && TabName == "SmsReports")
                {
                    string qtype = (string)TempData["SmsReportsqtype"];
                    model.SentEmailReport = _adminService.BroadcastReportsEmailSmsReportList(5, qtype);
                    TempData["ActiveTab"] = "EmailSmsReportsTab";

                }
                //---------------------SmsReports Broadcast Reports----------------------------

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return View(model);
        }

        [HttpPost]
        public RedirectToRouteResult BroadcastEmail(AdminBroadcastEmailDataValidation request)
        {
            try
            {
                string _fromEmail = string.Empty,
                    _emailBody = string.Empty,
                    _emailSubject = string.Empty,
                    _loggedUserName = string.Empty,
                    _groupType = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("ToEmail", typeof(string));
                dt.Columns.Add("FromEmail", typeof(string));
                dt.Columns.Add("EmailBody", typeof(string));
                dt.Columns.Add("EmailSubject", typeof(string));
                dt.Columns.Add("UserID", typeof(int));
                dt.Columns.Add("CustomerID", typeof(int));
                dt.Columns.Add("SendByAdmin", typeof(string));
                dt.Columns.Add("GroupType", typeof(string));


                var list = new List<UserPersonalInformationViewModel>();
                var model = new BroadcastEmailUserRequest()
                {
                    CategoryId = 0,
                    UserName = request.DownlineUserName ?? request.SpecificUserName,
                    RankId = (int)request.UserRankId
                };

                switch (request.EmailSendingTypeId)
                {
                    case 2: { list = _userService.GetUserDetailsList(1, model); } break;
                    case 3: { list = _userService.GetUserDetailsList(5, model); } break;
                    case 4: { list = _userService.GetUserDetailsList(6, model); } break;
                    case 5: { list = _userService.GetUserDetailsList(7, model); } break;
                    default: { list = _userService.GetUserDetailsList(1, model); } break;
                }

                _fromEmail = "admin@moremito.com";
                _emailBody = request.EmailBody;
                _emailSubject = request.EmailSubject;
                _loggedUserName = Session["UserName"] != null ? Convert.ToString(Session["UserName"]) : string.Empty;
                _groupType = DropDown.GetEmailSendingProfileTypes().Where(c => c.Value == request.EmailSendingTypeId.ToString()).Select(c => c.Text).FirstOrDefault();
                foreach (var user in list)
                {
                    DataRow row = dt.NewRow();
                    row["ToEmail"] = user.Email;
                    row["FromEmail"] = _fromEmail;
                    row["EmailBody"] = _emailBody;
                    row["EmailSubject"] = _emailSubject;
                    row["UserID"] = user.ID;
                    row["CustomerID"] = user.CustomerID;
                    row["SendByAdmin"] = _loggedUserName;
                    row["GroupType"] = _groupType;
                    dt.Rows.Add(row);
                }

                var Res = _adminService.InsertAdminBroadcastEmail(1, dt);

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RedirectToAction("broadcastemail", "admin");
        }



        [HttpGet]
        public ActionResult GetBroadcastEmailGridList(string SearchUserNameOrPhone)
        {

            try
            {
                TempData["GetBroadcastEmailSearchText"] = SearchUserNameOrPhone;
                TempData["TabName"] = "broadcastEmailTab";
                TempData["GetBroadcastEmailGridList"] = "GetBroadcastEmailGridList";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("BroadcastEmail");
        }


        [HttpPost]
        public ActionResult SearchDeleteBroadcastEmail(AdminBroadcastEmailDataValidation model, string Search, string delete, string ScheduleEmail, string SendEmailNow)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            GetBroadcastEmailList modelobj = new GetBroadcastEmailList();
            bool Checked = false;
            try
            {
                if (Search != null)
                {
                    TempData["GetBroadcastEmailSearchText"] = model.SearchUserNameOrPhone;

                    TempData["GetBroadcastEmailGridList"] = "GetBroadcastEmailGridList";
                }
                else if (delete != null)
                {

                    if (model.GetBroadcastEmailList.Count > 0)
                    {
                        for (int i = 0; i < model.GetBroadcastEmailList.Count; i++)
                        {
                            if (model.GetBroadcastEmailList[i].CheckId == true)
                            {
                                Checked = true;
                                modelobj.Id = model.GetBroadcastEmailList[i].Id;
                                _baseModel = _adminService.DeleteAdminBroadcastEmail(5, modelobj.Id, "");
                            }
                        }
                        if (TempData.ContainsKey("GetBroadcastEmailSearchText"))
                        {
                            model.SearchUserNameOrPhone = (string)TempData["GetBroadcastEmailSearchText"];
                        }
                        TempData["GetBroadcastEmailSearchText"] = model.SearchUserNameOrPhone;

                        TempData["GetBroadcastEmailGridList"] = "GetBroadcastEmailGridList";

                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                    if (Checked == false)
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                }


                else if (ScheduleEmail != null)
                {
                    if (model.GetBroadcastEmailList.Count > 0)
                    {
                        for (int i = 0; i < model.GetBroadcastEmailList.Count; i++)
                        {
                            if (model.GetBroadcastEmailList[i].CheckId == true)
                            {
                                Checked = true;
                                modelobj.Id = model.GetBroadcastEmailList[i].Id;
                                _baseModel = _adminService.DeleteAdminBroadcastEmail(6, modelobj.Id, model.ScheduleTime);
                            }
                        }

                        if (TempData.ContainsKey("GetBroadcastEmailSearchText"))
                        {
                            model.SearchUserNameOrPhone = (string)TempData["GetBroadcastEmailSearchText"];
                        }
                        TempData["GetBroadcastEmailSearchText"] = model.SearchUserNameOrPhone;

                        TempData["GetBroadcastEmailGridList"] = "GetBroadcastEmailGridList";
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Please select atleast 1 user!!!";
                    }
                    if (Checked == false)
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                }


                else if (SendEmailNow != null)
                {
                    if (model.GetBroadcastEmailList.Count > 0)
                    {
                        for (int i = 0; i < model.GetBroadcastEmailList.Count; i++)
                        {
                            if (model.GetBroadcastEmailList[i].CheckId == true)
                            {
                                Checked = true;
                                long Id = model.GetBroadcastEmailList[i].Id;
                                modelobj = _adminService.GetBroadcastEmailList(7, Id);

                                _baseModel = _adminService.SendBroadcastEmail(8, modelobj);
                            }
                        }

                        if (TempData.ContainsKey("GetBroadcastEmailSearchText"))
                        {
                            model.SearchUserNameOrPhone = (string)TempData["GetBroadcastEmailSearchText"];
                        }
                        TempData["GetBroadcastEmailSearchText"] = model.SearchUserNameOrPhone;

                        TempData["GetBroadcastEmailGridList"] = "GetBroadcastEmailGridList";
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Please select atleast 1 user!!!";
                    }

                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "SMS Sent Succesfully On Background.";

                    }
                    if (Checked == false)
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                }


                TempData["TabName"] = "broadcastEmailTab";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("BroadcastEmail");

        }


        [HttpGet]
        public ActionResult BroadcastSms(string TabName)
        {
            var model = new AdminBroadcastSmsDataValidation();
            TempData["ActiveTab"] = "broadcastsmsTab";
            if (TabName == null)
            {
                TabName = "broadcastsms";
            }
            try
            {
                model.SmsSendingTypeList = DropDown.GetSmsSendingProfileTypes();

                var datalist = _adminService.GetSmsTempletes(1, 0);
                model.SmsTemplateTypeList.Add(new SelectListItem { Value = Convert.ToString(0), Text = "Please select a template" });
                foreach (var item in datalist)
                {
                    model.SmsTemplateTypeList.Add(new SelectListItem { Value = Convert.ToString(item.ID), Text = item.Task });
                }

                var RankList = _userService.GetUserRankList(3);
                foreach (var item in RankList)
                {
                    model.UserRankList.Add(new SelectListItem { Value = Convert.ToString(item.Id), Text = item.RankName });
                }

                var scheduleList = _adminService.GetAdminScheduleBroadcastList(2);
                model.ScheduleList = scheduleList;



                ViewBag.BroadcastSMSmessagesCount = _adminService.BroadcastSMSmessagesCount(3, "");
                if (TempData.ContainsKey("GetBroadcastSMSSearchText") && TabName == "broadcastsms")
                {



                    string SearchText = (string)TempData["GetBroadcastSMSSearchText"];
                    model.GetBroadcastSMSList = _adminService.GetBroadcastSMSGridList(4, SearchText);

                    TempData["GetBroadcastSMSSearchText"] = SearchText;

                    TempData["ActiveTab"] = "broadcastsmsTab";

                }

                //------------------------Broadcast Reports----------------
                if (TempData.ContainsKey("BroadcastReports") && TabName == "BroadcastReports")
                {

                    model.BroadcastReportsViewModel = _adminService.GetBroadcastReportsPendingEmailSmsData(1);
                    TempData["ActiveTab"] = "BroadcastReportsTab";

                }
                //------------------------Broadcast Reports----------------

                //------------------------Sent Email Report----------------
                if (TempData.ContainsKey("SentEmailReport") && TabName == "SentEmailReport")
                {

                    model.SentEmailReport = _adminService.SentEmailReportList(1);
                    TempData["ActiveTab"] = "SentEmailReportTab";

                }
                //------------------------Sent Email Report----------------


                //---------------------EmailReports Broadcast Reports----------------------------

                if (TempData.ContainsKey("EmailReports") && TabName == "EmailReports")
                {
                    string qtype = (string)TempData["EmailReportsqtype"];
                    model.SentEmailReport = _adminService.BroadcastReportsEmailSmsReportList(4, qtype);
                    TempData["ActiveTab"] = "EmailSmsReportsTab";

                }
                //---------------------EmailReports Broadcast Reports----------------------------


                //---------------------SmsReports Broadcast Reports----------------------------

                if (TempData.ContainsKey("SmsReports") && TabName == "SmsReports")
                {
                    string qtype = (string)TempData["SmsReportsqtype"];
                    model.SentEmailReport = _adminService.BroadcastReportsEmailSmsReportList(5, qtype);
                    TempData["ActiveTab"] = "EmailSmsReportsTab";

                }
                //---------------------SmsReports Broadcast Reports----------------------------
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return View(model);
        }

        [HttpPost]
        public RedirectToRouteResult BroadcastSms(AdminBroadcastSmsDataValidation request)
        {
            try
            {
                long UserCount = 0;
                string _fromEmail = string.Empty,
                    _emailBody = string.Empty,
                    _emailSubject = string.Empty,
                    _loggedUserName = string.Empty,
                    _groupType = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("ToNumber", typeof(string));
                dt.Columns.Add("FromNumber", typeof(string));
                dt.Columns.Add("SmsText", typeof(string));
                dt.Columns.Add("UserID", typeof(string));


                var list = new List<UserPersonalInformationViewModel>();
                var model = new BroadcastEmailUserRequest()
                {
                    CategoryId = 0,
                    UserName = request.DownlineUserName ?? request.SpecificUserName,
                    RankId = (int)request.UserRankId
                };

                switch (request.SmsSendingTypeId)
                {
                    case 2: { list = _userService.GetUserDetailsList(1, model); } break;
                    case 3: { list = _userService.GetUserDetailsList(5, model); } break;
                    case 4: { list = _userService.GetUserDetailsList(6, model); } break;
                    case 5: { list = _userService.GetUserDetailsList(7, model); } break;
                    default: { list = _userService.GetUserDetailsList(1, model); } break;
                }

                _fromEmail = "admin@moremito.com";

                _loggedUserName = Session["UserName"] != null ? Convert.ToString(Session["UserName"]) : string.Empty;
                _groupType = DropDown.GetSmsSendingProfileTypes().Where(c => c.Value == request.SmsSendingTypeId.ToString()).Select(c => c.Text).FirstOrDefault();
                foreach (var user in list)
                {
                    DataRow row = dt.NewRow();
                    row["ToNumber"] = user.Phone;
                    row["FromNumber"] = "7023429839";
                    row["SmsText"] = request.SmsBody;
                    row["UserID"] = user.ID;
                    dt.Rows.Add(row);

                    long ExistingCount = _adminService.BroadcastSMSUserCount(2, request.SmsBody, user.Phone);
                    if (ExistingCount == 0)
                    {
                        UserCount = UserCount + 1;
                    }
                }

                var Res = _adminService.InsertAdminBroadcastSms(1, dt);


                if (UserCount > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Set broadcast SMS succesfully to " + UserCount + " user(s).";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "BroadCast SMS not set for any user.";
                }


            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RedirectToAction("broadcastsms", "admin");
        }



        [HttpGet]
        public ActionResult GetBroadcastSMSGridList(string SearchUserNameOrPhone)
        {

            try
            {


                TempData["GetBroadcastSMSSearchText"] = SearchUserNameOrPhone;
                TempData["TabName"] = "broadcastsmsTab";
                TempData["GetBroadcastSMSGridList"] = "GetBroadcastSMSGridList";


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("BroadcastSms");
        }

        [HttpPost]
        public ActionResult SearchDeleteBroadcastSms(AdminBroadcastSmsDataValidation model, string Search, string delete, string ScheduleSMS, string SendSMSNow)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            GetBroadcastSMSList modelobj = new GetBroadcastSMSList();
            bool Checked = false;
            try
            {
                if (Search != null)
                {
                    TempData["GetBroadcastSMSSearchText"] = model.SearchUserNameOrPhone;

                    TempData["GetBroadcastSMSGridList"] = "GetBroadcastSMSGridList";
                }
                else if (delete != null)
                {

                    if (model.GetBroadcastSMSList.Count > 0)
                    {
                        for (int i = 0; i < model.GetBroadcastSMSList.Count; i++)
                        {
                            if (model.GetBroadcastSMSList[i].CheckId == true)
                            {
                                Checked = true;
                                modelobj.Id = model.GetBroadcastSMSList[i].Id;
                                _baseModel = _adminService.DeleteAdminBroadcastSms(5, modelobj.Id, "");
                            }
                        }
                        if (TempData.ContainsKey("GetBroadcastSMSSearchText"))
                        {
                            model.SearchUserNameOrPhone = (string)TempData["GetBroadcastSMSSearchText"];
                        }
                        TempData["GetBroadcastSMSSearchText"] = model.SearchUserNameOrPhone;

                        TempData["GetBroadcastSMSGridList"] = "GetBroadcastSMSGridList";

                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                    if (Checked == false)
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                }


                else if (ScheduleSMS != null)
                {
                    if (model.GetBroadcastSMSList.Count > 0)
                    {
                        for (int i = 0; i < model.GetBroadcastSMSList.Count; i++)
                        {
                            if (model.GetBroadcastSMSList[i].CheckId == true)
                            {
                                Checked = true;
                                modelobj.Id = model.GetBroadcastSMSList[i].Id;
                                _baseModel = _adminService.DeleteAdminBroadcastSms(6, modelobj.Id, model.ScheduleTime);
                            }
                        }

                        if (TempData.ContainsKey("GetBroadcastSMSSearchText"))
                        {
                            model.SearchUserNameOrPhone = (string)TempData["GetBroadcastSMSSearchText"];
                        }
                        TempData["GetBroadcastSMSSearchText"] = model.SearchUserNameOrPhone;

                        TempData["GetBroadcastSMSGridList"] = "GetBroadcastSMSGridList";
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Please select atleast 1 user!!!";
                    }
                    if (Checked == false)
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                }


                else if (SendSMSNow != null)
                {
                    if (model.GetBroadcastSMSList.Count > 0)
                    {
                        for (int i = 0; i < model.GetBroadcastSMSList.Count; i++)
                        {
                            if (model.GetBroadcastSMSList[i].CheckId == true)
                            {
                                Checked = true;
                                long Id = model.GetBroadcastSMSList[i].Id;
                                modelobj = _adminService.GetBroadcastSMSList(7, Id);
                                modelobj.ToNumber = CommonViewModel.GetPhoneWithCountryCode(modelobj.ToNumber);
                                _baseModel = _adminService.SendBroadcastSMS(8, modelobj);
                            }
                        }

                        if (TempData.ContainsKey("GetBroadcastSMSSearchText"))
                        {
                            model.SearchUserNameOrPhone = (string)TempData["GetBroadcastSMSSearchText"];
                        }
                        TempData["GetBroadcastSMSSearchText"] = model.SearchUserNameOrPhone;

                        TempData["GetBroadcastSMSGridList"] = "GetBroadcastSMSGridList";
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Please select atleast 1 user!!!";
                    }

                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "SMS Sent Succesfully On Background.";

                    }
                    if (Checked == false)
                    {
                        TempData["ERROR_MESSAGE"] = "Please Select Some Value.";
                    }
                }


                TempData["TabName"] = "broadcastsmsTab";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("BroadcastSms");

        }

        [HttpGet]
        public JsonResult GetUserCount(int id, string u = null, int r = 0)
        {
            var dictionary = new Dictionary<string, object>();
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var userList = new List<UserPersonalInformationViewModel>();
                    var model = new BroadcastEmailUserRequest()
                    {
                        CategoryId = 0,
                        UserName = u,
                        RankId = r
                    };

                    switch (id)
                    {
                        case 2: { userList = _userService.GetUserDetailsList(1, model); dictionary.Add("list", userList); } break;
                        case 3: { userList = _userService.GetUserDetailsList(5, model); dictionary.Add("list", userList); } break;
                        case 4: { userList = _userService.GetUserDetailsList(6, model); dictionary.Add("list", userList); } break;
                        case 5: { userList = _userService.GetUserDetailsList(7, model); dictionary.Add("list", userList); } break;
                        default: { userList = _userService.GetUserDetailsList(1, model); } break;
                    }

                    dictionary.Add("count", userList.Count);
                }
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Json(dictionary, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSmsBody(int id)
        {
            var _body = string.Empty;
            try
            {
                if (Request.IsAjaxRequest())
                {
                    var datalist = _adminService.GetSmsTempletes(1, id);
                    _body = datalist.Count > 0 ? datalist.FirstOrDefault().Body : string.Empty;
                }
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Json(_body, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AdminEditor(Int32? Id, Int32? CategoriesId, Int32? SmsId)
        {
            if (Id > 0)
            {
                Session["ActiveTab"] = 11;
            }
            else
            {
                Id = 0;
            }
            if (CategoriesId > 0)
            {
                Session["ActiveTab"] = 13;
            }
            else
            {
                CategoriesId = 0;
            }
            if (SmsId > 0)
            {
                Session["ActiveTab"] = 6;
            }
            else
            {
                SmsId = 0;
            }

            AdminEditorViewModel adminEditorModel = new AdminEditorViewModel();
            if (Session["ActiveTab"] != null)
            {
                adminEditorModel.ActiveTab = Convert.ToInt64(Session["ActiveTab"]);
            }
            else
            {
                adminEditorModel.ActiveTab = 0;
            }
            var ContentKeyListData = _adminService.GetWebContentKeyName(2);
            if (ContentKeyListData != null)
            {
                adminEditorModel.ContentKeyList = ContentKeyListData;
            }
            var FAQCategoriesListData = _adminService.GetFAQCategoriesList(1);
            if (FAQCategoriesListData != null)
            {
                adminEditorModel.FAQCategoriesList = FAQCategoriesListData;
            }

            var FAQListData = _adminService.GetFaqsItemsList(2, Convert.ToInt32(CategoriesId));
            if (FAQListData != null)
            {
                adminEditorModel.FaqsItemsList = FAQListData;
            }

            var DeeplinkDetailsListData = _adminService.GetDeeplinkDetailsList(1, adminEditorModel);
            if (DeeplinkDetailsListData != null)
            {
                adminEditorModel.DeeplinkDetailsList = DeeplinkDetailsListData;
            }
            var EmailTaskNameListData = _adminService.GetEmailTaskNameList(3, 0);
            if (EmailTaskNameListData != null)
            {
                adminEditorModel.EmailTaskList = EmailTaskNameListData;
            }
            var CatsListData = _adminService.GetCatsDetailsList(1, 0);
            if (CatsListData != null)
            {
                adminEditorModel.CatsDetailsList = CatsListData;
            }
            var FaqCatsListData = _adminService.GetCatsDetailsList(8, 0);
            if (FaqCatsListData != null)
            {
                adminEditorModel.FaqCatsDetailsList = FaqCatsListData;
            }
            var ResourcesItems = _adminService.GetResourcesItemsList(1, Convert.ToInt32(Id));
            if (ResourcesItems != null)
            {
                adminEditorModel.resourcesItemsList = ResourcesItems;
            }

            var SmsEventListData = _adminService.GetSmsEventList(1, 0);
            if (SmsEventListData != null)
            {
                adminEditorModel.SmsEventList = SmsEventListData;
            }

            var SmsEventData = _adminService.GetSmsEventList(2, Convert.ToInt32(SmsId));
            if (SmsEventData.Count > 0)
            {
                adminEditorModel.SmsEventDetails = SmsEventData[0];
            }

            var SmsEventMsgListData = _adminService.GetSmsEventMsgList(3, Convert.ToInt32(SmsId));
            if (SmsEventMsgListData != null)
            {
                adminEditorModel.SmsEventMsgList = SmsEventMsgListData;
            }
            return View(adminEditorModel);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateEditorData(AdminEditorViewModel adminEditorModel)
        {


            var UpdateStatus = _adminService.UpdateWebContentById(4, null, adminEditorModel.Id, adminEditorModel.EditorContent);
            if (UpdateStatus != null)
            {
                if (UpdateStatus.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your content data has been updated successfully.";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Oops! Something went wrong!";
                }

            }
            long ActiveTab = 3;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
            //return View("AdminEditor");
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult UpdateEditorData2(AdminEditorViewModel adminEditorModel)
        {
            int ActionId = 0;
            if (adminEditorModel.ContentKeyName != null)
            {
                ActionId = 5;
            }
            else if (adminEditorModel.Id != 0)
            {
                ActionId = 4;
            }
            var UpdateStatus = _adminService.UpdateWebContentById(ActionId, adminEditorModel.ContentKeyName, adminEditorModel.Id, adminEditorModel.EditorContent);
            if (UpdateStatus != null)
            {
                if (UpdateStatus.RowAffected > 0)
                {
                    if (ActionId == 4)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Your content data has been updated successfully.";
                    }
                    else if (ActionId == 5)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Your new web content name has been saved successfully.";
                    }

                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Oops! Something went wrong!";
                }

            }
            long ActiveTab = 4;
            Session["ActiveTab"] = ActiveTab;
            return RedirectToAction("AdminEditor", "Admin");
            //return View("AdminEditor");
        }

        public JsonResult GetContentData(string Id)
        {
            var model = new HelpViewModel();
            var ContentData = _adminService.GetWebContentById(3, null, Convert.ToInt32(Id));
            if (ContentData != null)
            {
                model.HelpContent = ContentData.HelpContent;

            }
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AdminMEditor()
        {
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();

            try
            {
                //----------------- Brands Tab Section Start------------
                model.BrandsListViewModel = _adminService.GetBrandList(2);

                if (TempData.Count == 0)
                {
                    TempData["ActiveTab"] = "BrandsTab";//Default Tab
                    TempData["EditBranddiv"] = "hidediv";
                }




                if (TempData.ContainsKey("TabName"))
                {
                    BrandsViewModel Brandmodel = new BrandsViewModel();
                    if (TempData["TabName"].ToString() == "Brand")
                    {
                        if (TempData.ContainsKey("GetBrandListByIdData"))
                        {
                            if (TempData.ContainsKey("EditBrandId"))
                            {
                                Brandmodel.ID = (long)TempData["EditBrandId"];

                                model.BrandsViewModel = _adminService.GetBrandListById(4, Brandmodel.ID);
                                model.BrandsViewModel.EditName = model.BrandsViewModel != null ? model.BrandsViewModel.Name : "";

                                TempData["EditBranddiv"] = "showdiv";
                            }
                            else

                            {
                                TempData["EditBranddiv"] = "hidediv";
                            }

                        }
                        else
                        {
                            TempData["EditBranddiv"] = "hidediv";
                        }
                        TempData["ActiveTab"] = "BrandsTab";
                    }
                }




                //----------------- Brands Tab Section End------------




                //----------------- Product Tab Section Start------------

                if (TempData.ContainsKey("TabName"))
                {
                    if (TempData["TabName"].ToString() == "Product")
                    {
                        if (TempData.ContainsKey("ActiveTab") && TempData["ActiveTab"].ToString() == "ProductsTab")
                        {

                            TempData["ActiveTab"] = "ProductsTab";
                        }
                        if (TempData.ContainsKey("GetProductsListByIdData") && TempData.ContainsKey("EditProductsId"))
                        {

                            TempData["ActiveTab"] = "ProductsTab";
                            long Id = Convert.ToInt64(TempData["EditProductsId"]);
                            model.ProductsListViewModel = _adminService.GetProductsList(4, Id);
                            model.ProductsViewModel.MBrandID = Id;


                        }
                        if (TempData.ContainsKey("EditProductData"))
                        {
                            model.ProductsViewModel = (ProductsViewModel)TempData["EditProductData"];
                            TempData["ActiveTab"] = "ProductsTab";
                            TempData["DropDownListForBrandList"] = "Disabled";
                            TempData["buttonName"] = "Update New Product Now";
                        }
                        if (TempData.ContainsKey("ChangeBrandsForthisProductData"))
                        {
                            model.ProductsViewModel = (ProductsViewModel)TempData["ChangeBrandsForthisProductData"];
                            TempData["Changebrandsforthisproductdiv"] = "Show";
                            TempData["ActiveTab"] = "ProductsTab";

                        }
                    }
                }


                model.BrandList = _commonService.GetAllDropdownlist(2);
                model.BrandsforthisProductdList = _commonService.GetAllDropdownlist(2);

                //----------------- Product Tab Section End------------




                //----------------- Site Tab Section Start------------
                model.ProductList = _commonService.GetAllDropdownlist(3);
                model.BrandsforthisSiteList = _commonService.GetAllDropdownlist(3);

                if (TempData.ContainsKey("TabName"))
                {
                    if (TempData["TabName"].ToString() == "Sites")
                    {
                        if (TempData.ContainsKey("ActiveTab") && TempData["ActiveTab"].ToString() == "SitesTab")
                        {

                            TempData["ActiveTab"] = "SitesTab";
                        }

                        if (TempData.ContainsKey("GetSiteListByIdData") && TempData.ContainsKey("EditSiteId"))
                        {

                            TempData["ActiveTab"] = "SitesTab";
                            long Id = Convert.ToInt64(TempData["EditSiteId"]);
                            model.SiteListViewModel = _adminService.GetSiteList(4, Id);

                            model.SiteViewModel.ProductID = Id;

                        }
                        if (TempData.ContainsKey("EditSiteData"))
                        {
                            model.SiteViewModel = (SiteViewModel)TempData["EditSiteData"];
                            TempData["ActiveTab"] = "SitesTab";
                            TempData["DropDownListForProductList"] = "Disabled";
                            model.SiteViewModel.ProductID2 = Convert.ToInt64(TempData["EditSiteID"]);

                            TempData["buttonName"] = "Update New Site Now"
        ;
                        }
                        if (TempData.ContainsKey("ChangeBrandsForthisSiteData"))
                        {
                            model.SiteViewModel = (SiteViewModel)TempData["ChangeBrandsForthisSiteData"];
                            TempData["ChangebrandsforthisSitediv"] = "Show";
                            TempData["ActiveTab"] = "SitesTab";
                        }
                    }
                }




                //----------------- Site Tab Section End------------


                //----------------- Methods Tab Section Start------------
                TempData["EditMethodsdiv"] = "hidediv";

                model.MethodsListViewModel = _adminService.GetMethodsList(2);

                if (TempData.ContainsKey("TabName"))
                {
                    MethodsViewModel Methodsmodel = new MethodsViewModel();
                    if (TempData["TabName"].ToString() == "Methods")
                    {
                        if (TempData.ContainsKey("GetMethodsListByIdData"))
                        {
                            if (TempData.ContainsKey("EditMethodsId"))
                            {
                                Methodsmodel.ID = (long)TempData["EditMethodsId"];


                                model.MethodsViewModel = (MethodsViewModel)TempData["GetMethodsListByIdData"];
                                TempData["EditMethodsdiv"] = "showdiv";
                            }
                            else

                            {
                                TempData["EditMethodsdiv"] = "hidediv";
                            }

                        }
                        else
                        {
                            TempData["EditMethodsdiv"] = "hidediv";
                        }
                        TempData["ActiveTab"] = "MethodsTab";
                    }
                }
                //----------------- Methods Tab Section End------------


                //----------------- Resources Tab Section Start------------
                model.SiteList = _commonService.GetAllDropdownlist(4);
                model.MethodList = _commonService.GetAllDropdownlist(5);

                if (TempData.ContainsKey("TabName"))
                {
                    if (TempData["TabName"].ToString() == "Resources")
                    {
                        if (TempData.ContainsKey("ActiveTab") && TempData["ActiveTab"].ToString() == "ResourcesTab")
                        {

                            TempData["ActiveTab"] = "ResourcesTab";
                        }
                        if (TempData.ContainsKey("GetResourcesListByIdData") && TempData.ContainsKey("EditResourcesId"))
                        {

                            TempData["ActiveTab"] = "ResourcesTab";
                            long Id = Convert.ToInt64(TempData["EditResourcesId"]);
                            model.ResourcesListViewModel = _adminService.GetResourcesList(4, Id);
                            model.ResourcesViewModel.SitesID = Id;

                        }
                        if (TempData.ContainsKey("EditResourcesData"))
                        {
                            model.ResourcesViewModel = (MarketingResourcesViewModel)TempData["EditResourcesData"];
                            TempData["ActiveTab"] = "ResourcesTab";


                            TempData["buttonName"] = "Update New Resource Now";
                            model.QRDataForResource.SourcesID = model.ResourcesViewModel.ID;
                        }
                    }
                }
                //----------------- Resources Tab Section End------------



                //----------------- ResourcesQrCode Tab Section Start------------

                if (TempData.ContainsKey("TabName"))
                {
                    TempData["EditResourcesQrCodediv"] = "hidediv";
                    if (TempData["TabName"].ToString() == "ResourcesQrCode")
                    {
                        if (TempData.ContainsKey("ActiveTab") && TempData["ActiveTab"].ToString() == "ResourcesQrCodeTab")
                        {

                            TempData["ActiveTab"] = "ResourcesQrCodeTab";
                        }

                        if (TempData.ContainsKey("GetResourcesQrCodeByIdData") && TempData.ContainsKey("EditResourcesQrCodeSourcesID"))
                        {

                            long SourcesID = (long)TempData["EditResourcesQrCodeSourcesID"];
                            TempData["ActiveTab"] = "ResourcesQrCodeTab";
                            model.QRDataListForResource = _adminService.GetResourcesQrCodeList(2, SourcesID);
                            model.QRDataForResource.SourcesID = SourcesID;


                        }
                        if (TempData.ContainsKey("EditResourcesQrCodeData") && TempData.ContainsKey("EditResourcesQrCodeId"))
                        {
                            model.QRDataForResource = (QRDataForMarketingResource)TempData["EditResourcesQrCodeData"];
                            TempData["ActiveTab"] = "ResourcesQrCodeTab";
                            TempData["EditResourcesQrCodediv"] = "showdiv";

                        }

                    }
                }

                //----------------- ResourcesQrCode Tab Section End------------




                //----------------- Product Video Pages Tab Section Start------------
                #region Product Video Pages


                model.CategoryList = _commonService.GetAllDropdownlist(6);

                if (TempData.ContainsKey("TabName"))
                {
                    //TempData["EditResourcesQrCodediv"] = "hidediv";
                    if (TempData["TabName"].ToString() == "ProductVideoPages")
                    {
                        if (TempData.ContainsKey("ActiveTab") && TempData["ActiveTab"].ToString() == "ProductVideoPagesTab")
                        {

                            TempData["ActiveTab"] = "ProductVideoPagesTab";
                        }

                        if (TempData.ContainsKey("GetProductVideoPagesListByIdData") && TempData.ContainsKey("EditProductVideoPagesCategory"))
                        {

                            string category = (string)TempData["EditProductVideoPagesCategory"];
                            TempData["ActiveTab"] = "ProductVideoPagesTab";
                            model.MarketingResourceListVideoPages = _adminService.GetProductVideoPagesList(4, category);
                            model.MarketingResourceVideoPages.CategoryDropdown = category;


                        }
                        if (TempData.ContainsKey("EditProductVideoPagesData"))
                        {
                            model.MarketingResourceVideoPages = (MarketingResourceVideoPages)TempData["EditProductVideoPagesData"];
                            TempData["ActiveTab"] = "ProductVideoPagesTab";

                            model.MarketingResourceVideoPages.CategoryDropdown = Convert.ToString(TempData["EditProductVideoPagesCategory"]);

                            TempData["buttonName"] = "Update Resource Now"
        ;
                        }

                    }
                }
                #endregion

                //----------------- Product Video Pages Tab Section End------------


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult CancelAdminMEditor(string TabName)
        {
            if (TabName == "Methods")
            {
                TempData["ActiveTab"] = "MethodsTab";
                TempData["TabName"] = "Methods";
            }
            else if (TabName == "Resources")
            {
                TempData["ActiveTab"] = "ResourcesTab";
                TempData["TabName"] = "Resources";
            }
            else if (TabName == "ProductVideoPages")
            {
                TempData["ActiveTab"] = "ProductVideoPagesTab";
                TempData["TabName"] = "ProductVideoPages";
            }
            else
            {
                TempData.Clear();
            }


            return RedirectToAction("AdminMEditor");
        }

        #region Brands

        [HttpPost]
        public ActionResult CreateNewBrand(BrandsViewModel BrandsViewModel)
        {

            try
            {
                if (ModelState.IsValid == false && BrandsViewModel.ID > 0)
                {
                    //var msg = "alert('Please Enter All Fields.');";
                    //var msg = "alert('Are you sure want to Continue?');";
                    //return new JavaScriptResult() { Script = msg };

                    TempData["Message"] = "Please Enter All Fields.";
                    return RedirectToAction("AdminMEditor", BrandsViewModel);
                }

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                if (BrandsViewModel.ID > 0)
                {
                    BrandsViewModel.Name = BrandsViewModel.EditName;
                }
                _baseModel = _adminService.CreateUpdateDeleteBrand(1, BrandsViewModel);
                TempData["ActiveTab"] = "BrandsTab";
                TempData["TabName"] = "Brand";
                if (_baseModel.RowAffected > 0)
                {
                    //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                    //TempData["ActiveTab"] = "BrandsTab";
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops! Something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }

        [HttpPost]
        public ActionResult DeleteBrand(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                BrandsViewModel model = new BrandsViewModel();
                model.ID = Id;
                _baseModel = _adminService.CreateUpdateDeleteBrand(3, model);
                TempData["ActiveTab"] = "BrandsTab";
                TempData["TabName"] = "Brand";
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }

        [HttpGet]
        public ActionResult EditBrand(long Id)
        {
            BrandsViewModel Brandsmodel = new BrandsViewModel();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Brandsmodel = _adminService.GetBrandListById(4, Id);
                TempData["EditBranddiv"] = "showdiv";
                TempData["TabName"] = "Brand";
                if (Brandsmodel != null)
                {
                    Brandsmodel.ID = Id;
                    Brandsmodel.EditName = Brandsmodel.Name;
                    TempData["GetBrandListByIdData"] = "GetBrandListById";
                    TempData["EditBrandId"] = Id;

                }
                model.BrandsViewModel = Brandsmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }

        #endregion


        #region Products

        [HttpGet]
        public ActionResult GetProductsListById(long? Id)
        {
            List<ProductsListViewModel> Productsmodel = new List<ProductsListViewModel>();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Productsmodel = _adminService.GetProductsList(7, Convert.ToInt64(Id));



                TempData["GetProductsListByIdData"] = "GetProductsListById";
                TempData["EditProductsId"] = Id;
                TempData["TabName"] = "Product";


                model.ProductsListViewModel = Productsmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }

        [HttpPost]
        public ActionResult CreateNewProduct(ProductsViewModel ProductsViewModel)
        {

            try
            {
                if (ModelState.IsValid == false && ProductsViewModel.MBrandID == 0)
                {
                    // ProductsViewModel model = new ProductsViewModel();
                    ProductsViewModel.MBrandID = ProductsViewModel.MBrandID2;
                    SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

                    _baseModel = _adminService.CreateUpdateDeleteProduct(1, ProductsViewModel);

                    if (_baseModel.RowAffected > 0)
                    {
                        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                        //TempData["ActiveTab"] = "BrandsTab";
                    }
                }
                TempData["ActiveTab"] = "ProductsTab";
                TempData["TabName"] = "Product";
                TempData["DropDownListForProductList"] = "Enabled";

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }


        [HttpPost]
        public ActionResult DeleteProduct(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                ProductsViewModel model = new ProductsViewModel();
                model.ID = Id;
                _baseModel = _adminService.CreateUpdateDeleteProduct(6, model);
                TempData["ActiveTab"] = "ProductsTab";
                TempData["TabName"] = "Product";
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }

        [HttpGet]
        public ActionResult EditProduct(long MBrandID, long Id)
        {
            ProductsViewModel Brandsmodel = new ProductsViewModel();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Brandsmodel = _adminService.GetProductSingleListById(9, Id);

                TempData["TabName"] = "Product";
                if (Brandsmodel != null)
                {

                    TempData["EditProductData"] = Brandsmodel;
                    TempData["GetProductsListByIdData"] = "GetProductsListById";
                    TempData["EditProductsId"] = MBrandID;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }


        [HttpGet]
        public ActionResult ChangeBrandsForthisProduct(string ProductName, long BrandId, long ProductId)
        {
            ProductsViewModel model = new ProductsViewModel();
            model.ProductName = ProductName;
            model.MBrandID = BrandId;
            model.ID = ProductId;
            TempData["ChangeBrandsForthisProductData"] = model;
            TempData["TabName"] = "Product";

            TempData["GetProductsListByIdData"] = "GetProductsListById";

            TempData["EditProductsId"] = BrandId;


            return RedirectToAction("AdminMEditor");
        }


        [HttpPost]
        public ActionResult Updatebrandsforthisproduct(ProductsViewModel ProductsViewModel)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                ProductsViewModel model = new ProductsViewModel();
                //bool IsProductInBrand = false;
                for (int i = 0; i < ProductsViewModel.SelectedMBrandID.Length; i++)
                {
                    long MBrandID = ProductsViewModel.SelectedMBrandID[i];

                    model.ID = ProductsViewModel.ID;
                    // IsProductInBrand = _adminService.IsProductInBrand(3, model.ID, MBrandID);
                    if (i == 0)
                    {


                        model.MBrandID = ProductsViewModel.SelectedMBrandID[0];
                        _baseModel = _adminService.CreateUpdateDeleteProduct(8, model);
                    }
                    else if (i >= 1)
                    {


                        model = _adminService.GetProductSingleListById(9, model.ID);
                        model.ID = 0;
                        model.MBrandID = MBrandID;
                        _baseModel = _adminService.CreateUpdateDeleteProduct(1, model);
                    }
                }


                TempData["ActiveTab"] = "ProductsTab";
                TempData["TabName"] = "Product";

                TempData["GetProductsListByIdData"] = "GetProductsListById";
                TempData["EditProductsId"] = 0;
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }
        #endregion


        #region Site

        [HttpPost]
        public ActionResult CreateNewSite(SiteViewModel SiteViewModel)
        {

            try
            {
                if (ModelState.IsValid)
                {


                    SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                    SiteViewModel.ProductID = SiteViewModel.ProductID2;
                    _baseModel = _adminService.CreateUpdateDeleteSite(1, SiteViewModel);

                    if (_baseModel.RowAffected > 0)
                    {
                        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                        //TempData["ActiveTab"] = "BrandsTab";
                    }
                }
                TempData["ActiveTab"] = "SitesTab";
                TempData["TabName"] = "Sites";


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }


        [HttpGet]
        public ActionResult GetSiteListById(long? Id)
        {
            List<SiteListViewModel> Sitemodel = new List<SiteListViewModel>();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Sitemodel = _adminService.GetSiteList(4, Convert.ToInt64(Id));




                TempData["EditSiteId"] = Id;
                TempData["TabName"] = "Sites";
                TempData["GetSiteListByIdData"] = Sitemodel;


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("AdminMEditor");
        }


        [HttpPost]
        public ActionResult DeleteSite(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                SiteViewModel model = new SiteViewModel();
                model.ID = Id;
                _baseModel = _adminService.CreateUpdateDeleteSite(6, model);
                TempData["ActiveTab"] = "SitesTab";
                TempData["TabName"] = "Sites";
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }

        [HttpGet]
        public ActionResult EditSite(long SiteID)
        {
            SiteViewModel Sitemodel = new SiteViewModel();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Sitemodel = _adminService.GetSiteListById(8, SiteID);

                TempData["TabName"] = "Sites";
                if (Sitemodel != null)
                {

                    TempData["EditSiteData"] = Sitemodel;

                    TempData["EditSiteID"] = Sitemodel.ProductID;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }

        [HttpGet]
        public ActionResult ChangeBrandsForthisSite(string SiteName, long SiteId, long ProductID)
        {
            SiteViewModel model = new SiteViewModel();
            model.SiteName = SiteName;
            model.ID = SiteId;
            model.ProductID = ProductID;
            TempData["ChangeBrandsForthisSiteData"] = model;
            TempData["TabName"] = "Sites";

            TempData["GetSiteListByIdData"] = "GetSiteListById";

            TempData["EditSiteID"] = ProductID;




            return RedirectToAction("AdminMEditor");
        }


        [HttpPost]
        public ActionResult UpdatebrandsforthisSite(SiteViewModel SiteViewModel)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                SiteViewModel model = new SiteViewModel();
                model.ProductID = SiteViewModel.SelectedProductID[0];
                model.ID = SiteViewModel.ID;
                _baseModel = _adminService.CreateUpdateDeleteSite(7, model);
                TempData["ActiveTab"] = "SitesTab";
                TempData["TabName"] = "Sites";

                TempData["GetSiteListByIdData"] = "GetSiteListById";
                TempData["EditSiteId"] = 0;
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }



        #endregion



        #region Methods

        [HttpPost]
        public ActionResult CreateNewMethods(MethodsViewModel MethodsViewModel)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                //if (ModelState.IsValid == false && MethodsViewModel.EditName == null && MethodsViewModel.Description == null)
                //{
                if (ModelState.IsValid)
                {
                    if (MethodsViewModel.ID > 0)
                    {
                        MethodsViewModel.Name = MethodsViewModel.EditName;
                    }
                    else
                    {
                        MethodsViewModel.Description = MethodsViewModel.Name;
                    }
                    _baseModel = _adminService.CreateUpdateDeleteMethods(1, MethodsViewModel);
                }
                TempData["ActiveTab"] = "MethodsTab";
                TempData["TabName"] = "Methods";
                if (_baseModel.RowAffected > 0)
                {
                    //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                    //TempData["ActiveTab"] = "BrandsTab";
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }

        [HttpGet]
        public ActionResult EditMethods(long Id)
        {
            MethodsViewModel Methodsmodel = new MethodsViewModel();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Methodsmodel = _adminService.GetMethodsListById(4, Id);
                TempData["EditMethodsdiv"] = "showdiv";
                TempData["TabName"] = "Methods";
                if (Methodsmodel != null)
                {
                    Methodsmodel.ID = Id;
                    Methodsmodel.EditName = Methodsmodel.Name;
                    Methodsmodel.Description = Methodsmodel.Description;
                    TempData["GetMethodsListByIdData"] = Methodsmodel;
                    TempData["EditMethodsId"] = Id;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }

        [HttpPost]
        public ActionResult DeleteMethods(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                MethodsViewModel model = new MethodsViewModel();
                model.ID = Id;
                _baseModel = _adminService.CreateUpdateDeleteMethods(3, model);
                TempData["ActiveTab"] = "MethodsTab";
                TempData["TabName"] = "Methods";
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }


        #endregion


        #region Resources

        [HttpPost]
        public ActionResult CreateNewResources(MarketingResourcesViewModel ResourcesViewModel)
        {

            try
            {
                if (ModelState.IsValid)
                {


                    SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

                    _baseModel = _adminService.CreateUpdateDeleteResources(1, ResourcesViewModel);

                    if (_baseModel.RowAffected > 0)
                    {
                        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                        //TempData["ActiveTab"] = "BrandsTab";
                    }
                }
                TempData["ActiveTab"] = "ResourcesTab";
                TempData["TabName"] = "Resources";
                //TempData["DropDownListForProductList"] = "Enabled";

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }



        [HttpGet]
        public ActionResult GetResourcesListById(long? Id)
        {
            List<MarketingResourcesListViewModel> Resourcesmodel = new List<MarketingResourcesListViewModel>();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Resourcesmodel = _adminService.GetResourcesList(4, Convert.ToInt64(Id));

                TempData["EditResourcesId"] = Id;
                TempData["TabName"] = "Resources";
                TempData["GetResourcesListByIdData"] = Resourcesmodel;


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("AdminMEditor");
        }


        [HttpGet]
        public ActionResult ModifyResources(long ResourcesId, long SitesID)
        {
            MarketingResourcesViewModel Resourcesmodel = new MarketingResourcesViewModel();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Resourcesmodel = _adminService.GetResourcesListById(2, ResourcesId);

                TempData["TabName"] = "Resources";
                if (Resourcesmodel != null)
                {

                    TempData["EditResourcesData"] = Resourcesmodel;
                    TempData["EditResourcesId"] = SitesID;
                    TempData["TabName"] = "Resources";
                    TempData["GetResourcesListByIdData"] = Resourcesmodel;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }

        [HttpPost]
        public ActionResult DeleteResources(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                MarketingResourcesViewModel model = new MarketingResourcesViewModel();
                model.ID = Id;
                _baseModel = _adminService.CreateUpdateDeleteResources(3, model);
                TempData["ActiveTab"] = "ResourcesTab";
                TempData["TabName"] = "Resources";
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }
        #endregion


        #region MarketingResources_QrData

        [HttpPost]
        public ActionResult CreateNewResourcesQrCode(QRDataForMarketingResource QRDataForResource)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                if (ModelState.IsValid == false && QRDataForResource.URL == null)
                {



                    if (QRDataForResource.QRDataForResourceId > 0)
                    {
                        TempData["QRDataForResourceId"] = QRDataForResource.QRDataForResourceId;
                        QRDataForResource.URL = QRDataForResource.EditURL;
                    }
                    _baseModel = _adminService.CreateUpdateDeleteResourcesQrCode(1, QRDataForResource);

                    if (_baseModel.RowAffected > 0)
                    {
                        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                        //TempData["ActiveTab"] = "BrandsTab";
                    }
                }
                else

                {
                    _baseModel = _adminService.CreateUpdateDeleteResourcesQrCode(1, QRDataForResource);

                    if (_baseModel.RowAffected > 0)
                    {
                        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                        //TempData["ActiveTab"] = "BrandsTab";
                    }
                }
                TempData["ActiveTab"] = "ResourcesQrCodeTab";
                TempData["TabName"] = "ResourcesQrCode";

                TempData["EditResourcesQrCodeSourcesID"] = QRDataForResource.SourcesID;

                TempData["GetResourcesQrCodeByIdData"] = "";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }



        [HttpGet]
        public ActionResult GetResourcesQrCodeList(long SourcesID)
        {
            List<QRDataListForMarketingResource> Resourcesmodel = new List<QRDataListForMarketingResource>();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                Resourcesmodel = _adminService.GetResourcesQrCodeList(2, SourcesID);

                TempData["EditResourcesQrCodeSourcesID"] = SourcesID;
                TempData["TabName"] = "ResourcesQrCode";
                TempData["GetResourcesQrCodeByIdData"] = Resourcesmodel;


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("AdminMEditor");
        }

        [HttpGet]
        public ActionResult EditResourcesQrCode(long Id)
        {
            QRDataForMarketingResource QRDataForMarketingmodel = new QRDataForMarketingResource();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                QRDataForMarketingmodel = _adminService.GetResourcesQrCodeListById(4, Id);
                TempData["EditResourcesQrCodediv"] = "showdiv";
                TempData["TabName"] = "ResourcesQrCode";
                if (QRDataForMarketingmodel != null)
                {
                    QRDataForMarketingmodel.QRDataForResourceId = Id;
                    QRDataForMarketingmodel.EditURL = QRDataForMarketingmodel.URL;


                    TempData["EditResourcesQrCodeData"] = QRDataForMarketingmodel;
                    TempData["EditResourcesQrCodeId"] = Id;

                    TempData["GetResourcesQrCodeListByIdData"] = "";

                    TempData["EditResourcesQrCodeSourcesID"] = QRDataForMarketingmodel.SourcesID;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }


        [HttpPost]
        public ActionResult DeleteResourcesQrCode(long Id, long SourcesID)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                QRDataForMarketingResource model = new QRDataForMarketingResource();
                model.QRDataForResourceId = Id;
                _baseModel = _adminService.CreateUpdateDeleteResourcesQrCode(3, model);
                TempData["ActiveTab"] = "ResourcesQrCodeTab";
                TempData["TabName"] = "ResourcesQrCode";
                if (_baseModel.RowAffected > 0)
                {

                }

                TempData["GetResourcesQrCodeListByIdData"] = "";

                TempData["EditResourcesQrCodeSourcesID"] = SourcesID;
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }


        //Try Now functionality is Pending


        #endregion




        #region MarketingResources_VideoPages
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateNewProductVideoPages(MarketingResourceVideoPages MarketingResourceVideoPages)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                if (ModelState.IsValid)
                {



                    //if (QRDataForResource.QRDataForResourceId > 0)
                    //{
                    //    TempData["QRDataForResourceId"] = QRDataForResource.QRDataForResourceId;
                    //    QRDataForResource.URL = QRDataForResource.EditURL;
                    //}
                    _baseModel = _adminService.CreateUpdateDeleteMarketingResourcesVideoPages(1, MarketingResourceVideoPages);

                    if (_baseModel.RowAffected > 0)
                    {
                        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                        //TempData["ActiveTab"] = "BrandsTab";
                    }
                }
                //else

                //{
                //    _baseModel = _adminService.CreateUpdateDeleteResourcesQrCode(1, QRDataForResource);

                //    if (_baseModel.RowAffected > 0)
                //    {
                //        //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                //        //TempData["ActiveTab"] = "BrandsTab";
                //    }
                //}
                TempData["ActiveTab"] = "ProductVideoPagesTab";
                TempData["TabName"] = "ProductVideoPages";

                // TempData["EditResourcesQrCodeSourcesID"] = QRDataForResource.SourcesID;

                // TempData["GetResourcesQrCodeByIdData"] = "";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }


        [HttpGet]
        public ActionResult GetProductVideoPagesListById(string Category)
        {
            List<MarketingResourceListVideoPages> ProductVideoPagesmodel = new List<MarketingResourceListVideoPages>();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {


                TempData["EditProductVideoPagesCategory"] = Category;
                TempData["TabName"] = "ProductVideoPages";
                TempData["GetProductVideoPagesListByIdData"] = ProductVideoPagesmodel;


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("AdminMEditor");
        }


        [HttpGet]
        public ActionResult ModifyProductVideoPages(long Id, string Category)
        {
            MarketingResourceVideoPages VideoPagesmodel = new MarketingResourceVideoPages();
            AdminMEditorsViewModel model = new AdminMEditorsViewModel();
            try
            {
                VideoPagesmodel = _adminService.GetProductVideoPagesListById(2, Id);

                TempData["TabName"] = "ProductVideoPages";
                if (VideoPagesmodel != null)
                {

                    TempData["EditProductVideoPagesData"] = VideoPagesmodel;
                    TempData["EditProductVideoPagesCategory"] = Category;
                    TempData["GetProductVideoPagesListByIdData"] = VideoPagesmodel;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("AdminMEditor");
        }

        [HttpPost]
        public ActionResult DeleteProductVideoPages(long Id, string Category)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                MarketingResourceVideoPages model = new MarketingResourceVideoPages();
                model.ID = Id;
                _baseModel = _adminService.CreateUpdateDeleteMarketingResourcesVideoPages(3, model);
                TempData["ActiveTab"] = "ProductVideoPagesTab";
                TempData["TabName"] = "ProductVideoPages";
                if (_baseModel.RowAffected > 0)
                {

                }

                TempData["GetProductVideoPagesListByIdData"] = "";

                TempData["EditProductVideoPagesCategory"] = Category;
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMEditor");

        }


        #endregion

        public ActionResult AdminPermissions()
        {
            return View();
        }

        public ActionResult AdminCK()
        {
            return View();
        }

        public ActionResult AdminCK1()
        {
            return View();
        }

        public ActionResult AdminCK2()
        {
            return View();
        }


        private SponsorContactDetailsViewModel PopulateDataSponsorContactDetails(long SponsorID)
        {
            SponsorContactDetailsViewModel model = new SponsorContactDetailsViewModel();
            UserInformationViewModel userInformation = new UserInformationViewModel();
            userInformation = _membersService.GetUserInformation(8, Convert.ToInt64(SponsorID));
            if (userInformation != null)
            {
                model.UserName = userInformation.UserName;
                model.Name = userInformation.FirstName + ' ' + userInformation.LastName;
                model.Phone = userInformation.Phone;
                model.Email = userInformation.Email;
            }
            return model;
        }



        public List<UplineContactDetailsViewModel> PopulateDataUplineContactDetailsViewModel(long SponsorID, long SelectedUsersID, string Username)
        {
            List<UplineContactDetailsViewModel> modelList = new List<UplineContactDetailsViewModel>();
            long Sponser = SponsorID;
            //long SponSpon = _membersService.GetRepresentativeInformation(1, SponsorID, false).SponsorUsersID;
            long SponSpon = 0;
            var SponSponData = _membersService.GetRepresentativeInformation(1, SponsorID, false);
            if (SponSponData != null)
            {
                SponSpon = SponSponData.SponsorUsersID;
            }
            long SponSponSpon = 0;
            if (SponSpon > 0)
            {
                SponSponSpon = _membersService.GetRepresentativeInformation(1, SponSpon, false).SponsorUsersID;
            }

            List<long> UserIDList = new List<long>();
            if (Sponser > 25)
            {
                UserIDList.Add(Sponser);
            }
            if (Sponser > 25)
            {
                UserIDList.Add(SponSpon);
            }
            if (Sponser > 25)
            {
                UserIDList.Add(SponSponSpon);
            }

            long nextSilver = getNextUplineSilver(SelectedUsersID);

            bool NeedToFindSilver2 = true;
            bool NeedToFindSilver3 = true;

            if (nextSilver >= 25)
            {
                switch (GetRankID(nextSilver))
                {
                    case 4:
                        {
                            if (nextSilver > 25)
                                UserIDList.Add(nextSilver);
                            break;
                        }

                    case 5:
                        {
                            if (nextSilver > 25)
                                UserIDList.Add(nextSilver);
                            NeedToFindSilver2 = false;
                            break;
                        }

                    case object _ when GetRankID(nextSilver) > 5:
                        {
                            if (nextSilver > 25)
                                UserIDList.Add(nextSilver);
                            NeedToFindSilver2 = false;
                            NeedToFindSilver3 = false;
                            break;
                        }
                }
                while (NeedToFindSilver2 == true)
                {
                    nextSilver = getNextUplineSilver(nextSilver);
                    switch (GetRankID(nextSilver))
                    {
                        case 5:
                            {
                                if (nextSilver > 25)
                                    UserIDList.Add(nextSilver);
                                NeedToFindSilver2 = false;
                                break;
                            }

                        case object _ when GetRankID(nextSilver) > 5:
                            {
                                if (nextSilver > 25)
                                    UserIDList.Add(nextSilver);
                                NeedToFindSilver2 = false;
                                NeedToFindSilver3 = false;
                                break;
                            }
                    }
                }
                while (NeedToFindSilver3 == true)
                {
                    nextSilver = getNextUplineSilver(nextSilver);
                    switch (GetRankID(nextSilver))
                    {
                        case object _ when GetRankID(nextSilver) > 5:
                            {
                                if (nextSilver > 25)
                                    UserIDList.Add(nextSilver);
                                NeedToFindSilver3 = false;
                                break;
                            }
                    }
                }
            }

            long CustomerID = _membersService.GetRepresentativeInformation(2, SelectedUsersID, false).CustomerID;
            long G1 = 0;
            long G2 = 0;
            long G3 = 0;
            CustomerDetailsViewModel _custModel = new CustomerDetailsViewModel();
            _custModel = _membersService.GetCustomerDetails(1, CustomerID);
            if (_custModel.G1ID > 25)
                UserIDList.Add(G1);
            if (_custModel.G2ID > 25)
                UserIDList.Add(G2);
            if (_custModel.G3ID > 25)
                UserIDList.Add(G3);
            int P1 = 0;
            int P2 = 0;
            int P3 = 0;
            if (_custModel.P1ID > 25)
                UserIDList.Add(P1);
            if (_custModel.P2ID > 25)
                UserIDList.Add(G2);
            if (G3 > 25)
                UserIDList.Add(G3);
            // deduplicate list
            if (UserIDList.Count > 1)
                UserIDList = UserIDList.Distinct().ToList();

            if (UserIDList.Count > 0)

                foreach (var item in UserIDList)
                {
                    UplineContactDetailsViewModel model = UserData(item, SelectedUsersID, Username);
                    modelList.Add(model);
                }

            return modelList;
        }


        public long getNextUplineSilver(long UserID)
        {
            long getNextUplineSilver = 26;
            long EvalGuy = 0;
            var EvalGuyData = _membersService.GetRepresentativeInformation(1, UserID, false);
            if (EvalGuyData != null)
            {
                EvalGuy = EvalGuyData.SponsorUsersID;
            }
            while (EvalGuy > 25)
            {
                if (GetRankID(EvalGuy) >= 4)
                {
                    getNextUplineSilver = EvalGuy;
                    break;
                }
                else
                    return EvalGuy = _adminService.GetSponsorsUsersID(1, UserID, true);
            }
            return getNextUplineSilver;
        }
        public long GetRankID(long UsersID)
        {
            long GetRankID = 0;
            var GetRankIDData = _membersService.GetRepresentativeInformation(2, UsersID, false);
            if (GetRankIDData != null)
            {
                GetRankID = GetRankIDData.RankID;
            }
            return GetRankID;
        }

        public UplineContactDetailsViewModel UserData(long UsersID, long SelectedUserID, string Username)
        {



            UplineContactDetailsViewModel model = GetWelcomeTagLiterals(Username, UsersID, SelectedUserID);
            long RankId = _commonService.GetRankID(1, UsersID);
            model.Rank = _commonService.RankName(2, RankId);
            model.Level = GetWelcomeTagLiterals(Username, UsersID, SelectedUserID).Level;
            return model;
        }

        public UplineContactDetailsViewModel GetWelcomeTagLiterals(string Username, long UsersID, long SelectedUserID)
        {
            UplineContactDetailsViewModel model = new UplineContactDetailsViewModel();
            if (_userService.IsUserInRole(1, Username, "Member") == true || _userService.IsUserInRole(1, Username, "FriendOfFoxx") == true || _userService.IsUserInRole(1, Username, "ReferringCustomer") == true)
            {
                model = _adminService.GetWelcomeTagLiterals(1, Username, UsersID, SelectedUserID);
            }

            return model;
        }




        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateCSJ(UserInformationViewModel userInformation)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

            var comment = DateTime.Now.ToString() + ' ' + userInformation.UserName + ' ' + "wrote:" + Environment.NewLine.ToString() + " " + userInformation.cSJDetailsViewModel.MyRecord + Environment.NewLine.ToString() + " " + userInformation.cSJDetailsViewModel.JournalHistoricalRecords;
            userInformation.cSJDetailsViewModel.Comments = comment;
            userInformation.cSJDetailsViewModel.UserName = userInformation.UserName;
            userInformation.cSJDetailsViewModel.UserId = userInformation.UserId;
            _baseModel = _adminService.UpdateCSJ(2, userInformation.cSJDetailsViewModel);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";
                TempData["ActiveTab"] = "CSJTab";
            }
            else
            {

            }
            return RedirectToAction("Contact_inner", new { UserId = userInformation.UserId });
        }
        [HttpPost]
        public ActionResult AddCsj(AddNewCsj model)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                CSJDetailsViewModel csj = new CSJDetailsViewModel();
                csj.AdminId = Convert.ToInt32(loggedInUser.UserId);
                csj.UserId = model.UserId;
                csj.Comments = model.Comment;
                var _baseModel = _adminService.UpdateCSJ(2, csj);
                status = true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendFeedbackEmail()
        {
            //Need to Modify
            return Json(JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetPersonalInformation(long InfoId)
        {
            MiscInfoViewModel misc_model = new MiscInfoViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                misc_model._personalInfo = _membersService.GetPersonalDetails(4, InfoId);
                long Level = _membersService.GetLevelInformation(2, InfoId, loggedInUser.UserId);
                misc_model._questionAnswer = _membersService.GetMQDetails(3, InfoId);
                misc_model._recurringOrder = _membersService.GetRecurringOrderDetails(5, InfoId);
                misc_model._processOrder = _membersService.GetProcessOrderDetails(6, InfoId);
                misc_model._advertisment = _membersService.GetAdvertismentDetails(7, InfoId);


                //-------------------Rank History Section----------------------------


                UserName = _commonService.GetUserNamebyUsersID(15, Convert.ToInt64(InfoId));
                TempData["TempDataUserId"] = InfoId;

                if (_userService.IsUserInRole(1, UserName, "member") == true)
                {
                    misc_model.MembershipType = "Paid Representative";
                }
                else if (_userService.IsUserInRole(1, UserName, "referringcustomer") == true)
                {
                    misc_model.MembershipType = "Referring Customer";
                }
                else if (_userService.IsUserInRole(1, UserName, "friendoffoxx") == true)
                {
                    misc_model.MembershipType = "Customer";
                }
                misc_model.HighestRankAchieved = _commonService.RankName(2, _commonService.GetRankID(1, Convert.ToInt64(InfoId)));
                misc_model.QTE = getQteNowForMBO(Convert.ToInt64(InfoId));

                var GetJoinDate = _membersService.GetUserInformation(14, Convert.ToInt64(InfoId));
                if (GetJoinDate != null)
                {
                    misc_model.JoinDate = GetJoinDate.JoinDate;
                }
                misc_model._rankHistory = _membersService.GetRankHistoryDetails(8, InfoId);
                //-------------------Rank History Section----------------------------






                misc_model._marketingActivity = _membersService.GetMarketingActivityDetails(9, InfoId);

                misc_model._personalInfo.SponserName = UserName;
                misc_model._personalInfo.Level = Level;


                misc_model._mobileAppActivity.RecipientsLast72Hours = RecipientsOrActivityCountInTime(InfoId, "72H", "recipient").ToString();
                misc_model._mobileAppActivity.RecipientsLast7Days = RecipientsOrActivityCountInTime(InfoId, "7day", "recipient").ToString();
                misc_model._mobileAppActivity.Recipients8to14day = RecipientsOrActivityCountInTime(InfoId, "8to14day", "recipient").ToString();
                misc_model._mobileAppActivity.Recipinets15to21day = RecipientsOrActivityCountInTime(InfoId, "15to21day", "recipient").ToString();
                misc_model._mobileAppActivity.Recipients22to28day = RecipientsOrActivityCountInTime(InfoId, "22to28day", "recipient").ToString();
                misc_model._mobileAppActivity.RecipientsLife = RecipientsOrActivityCountInTime(InfoId, "", "recipient").ToString();

                misc_model._mobileAppActivity.Activity72h = RecipientsOrActivityCountInTime(InfoId, "72H", "activity").ToString();
                misc_model._mobileAppActivity.Activity7day = RecipientsOrActivityCountInTime(InfoId, "7day", "activity").ToString();
                misc_model._mobileAppActivity.Activity8to14day = RecipientsOrActivityCountInTime(InfoId, "8to14day", "activity").ToString();
                misc_model._mobileAppActivity.Activity15to21day = RecipientsOrActivityCountInTime(InfoId, "15to21day", "activity").ToString();
                misc_model._mobileAppActivity.Activity22to28day = RecipientsOrActivityCountInTime(InfoId, "22to28day", "activity").ToString();
                misc_model._mobileAppActivity.ActivityLife = RecipientsOrActivityCountInTime(InfoId, "", "activity").ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_TreeOrganicPlacement", misc_model);
        }

        [HttpPost]
        public ActionResult SearchPlacementTool(UserInformationViewModel model)
        {
            lst = new List<string>();
            lst.Add(model.UserName);
            lst.Add(model.UserId.ToString());
            lst.Add(model.SearchBy);
            lst.Add(model.Value);




            return RedirectToAction("Contact_inner", new { UserId = model.UserId });

        }


        public ActionResult PlacementToolPlaceHere(long? PlacementUserID, long? UserId)
        {
            TempData["SUCCESS_MESSAGE_ContentPlacementTool"] = _adminService.GetWebContentById(1, "ConfirmBox_Content_MyPersonalPage", 0);
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

            PlacementToolViewModel model = new PlacementToolViewModel();
            model.PlacementUserID = Convert.ToInt64(PlacementUserID);
            model.UserId = Convert.ToInt64(UserId);
            _baseModel = _adminService.InsertUpdatePlacementTool(2, model);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE_PlaceHere"] = "Place Here Successfully";
                AddDownlineCounts(Convert.ToInt64(UserId));
                getQteNowForMBO(Convert.ToInt64(PlacementUserID));
                getQteNowForMBO(Convert.ToInt64(UserId));
            }
            else
            {

            }
            return RedirectToAction("Contact_inner", new { UserId = UserId });
        }

        public void AddDownlineCounts(long UserId)
        {

            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            DownlineCountsViewModel model = new DownlineCountsViewModel();
            if (UserId > 1)
            {
                long LevelCount = 1;
                long FocusGuy = UserId;
                long FocusGuysSponsor = _adminService.GetSponsorsUsersID(1, FocusGuy, true);
                while (FocusGuysSponsor > 0)
                {
                    model.UsersID = UserId;
                    model.UplineUsersID = FocusGuysSponsor;
                    model.UserIsInUplinesLevelX = LevelCount;
                    baseModel = _commonService.InsertDownlineCounts(1, model);
                    FocusGuy = FocusGuysSponsor;
                    FocusGuysSponsor = _adminService.GetSponsorsUsersID(1, FocusGuy, true);
                    LevelCount += 1;
                }
            }
        }

        public string getQteNowForMBO(long PlacementUserID)
        {
            string getQteNowForMBO = "NA";
            UsersQTENowforMBOViewModel modelDetails = new UsersQTENowforMBOViewModel();

            var model = _commonService.GetQteNowForMBODetails(2, PlacementUserID);

            if (model != null)
            {
                if (model.HasQteForNowRecord == false)
                {
                    // SetQteNowForMBO(UsersID)//Need to Modify
                }
            }
            modelDetails = _commonService.GetQteNowForMBODetails(1, PlacementUserID);
            if (modelDetails != null)
            {

                if (modelDetails.QteRankID > 0)
                {


                    getQteNowForMBO = _commonService.RankName(2, modelDetails.QteRankID) + "as of " + modelDetails.AsOfDate;
                }

            }

            return getQteNowForMBO;
        }


        public ActionResult QTEUpdated(long? UserId)
        {

            var Date = "";
            //6/10/2021 8:34 AM
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            //Public MyCulture As IFormatProvider = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
            var time = DateTime.Now.ToString("hh:mm tt");
            // var startDay = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.;
            // var dd = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            // var dddd = dd.DateTimeFormat.d;
            // Now.ToLongTimeString
            string dt = DateTime.Now.Date.ToString("MM-dd-yyyy");
            Date = dt.Replace("-", "/") + ' ' + time;


            //    Core.SetQteNowForMBO(SelectedUsersID)
            //lblQTERank.Text = Core.getQteNowForMBO(SelectedUsersID)

            //if (_baseModel.RowAffected > 0)
            //{

            TempData["SUCCESS_MESSAGE"] = "QTE Updated" + ' ' + Date;

            TempData["ActiveTab"] = "RankTab";
            //}
            //else
            //{

            //}


            return RedirectToAction("Contact_inner", new { UserId = UserId });
        }

        public long GetTeamCustomersCount(long UsersID)
        {

            long GetTeamCustomersCount = 0;
            List<long> UsersId = _commonService.GetTeamCustomersCount(2, UsersID);
            if (UsersId.Count > 0)
            {


                while (GetTeamCustomersCount < UsersId.Count)
                {
                    GetTeamCustomersCount += 1;
                }
            }
            return GetTeamCustomersCount;
        }


        public void ZeroCodes(long UserId)
        {
            long CustomerID = _commonService.GetCustomerIDByUserID(2, UserId);
            _membersService.GetCustomerDetails(3, CustomerID);
        }

        public void SetInitialPlatinumCodes(long NewUsersID, long SponsorID)
        {
            PlatinumCodesListViewModel model = new PlatinumCodesListViewModel();



            //    SetQTEPromoData(SponsorsUsersPlatCode(0), 10, 0)
            //    SetQTEPromoData(SponsorsUsersPlatCode(1), 11, 0)
            //    SetQTEPromoData(SponsorsUsersPlatCode(2), 12, 0)
            //    If IsUserQTEAtThisLevel(SponsorsUsersPlatCode(0), 10, 0) = True Then 'this is the guy
            //        NewPlatCodes(0) = SponsorsUsersPlatCode(0)
            //    Else 'search uplines of FoundP1's Users.QteFail_P1ID to find qte@p1 guy
            //        NewPlatCodes(0) = FindNextPlatQTE(SponsorsUsersPlatCode(0), 10)
            //    End If
            //    If IsUserQTEAtThisLevel(SponsorsUsersPlatCode(1), 11, 0) = True Then 'this is the guy
            //        NewPlatCodes(1) = SponsorsUsersPlatCode(1)
            //    Else 'search uplines of FoundP2's Customer.(pay)P2ID to find qte @p2 guy
            //        NewPlatCodes(1) = FindNextPlatQTE(SponsorsUsersPlatCode(1), 11)
            //    End If
            //    If IsUserQTEAtThisLevel(SponsorsUsersPlatCode(2), 12, 0) = True Then 'this is the guy
            //        NewPlatCodes(2) = SponsorsUsersPlatCode(2)
            //    Else 'search uplines of FoundP3's Customers.(pay)P3ID to find qte @p3 guy
            //        NewPlatCodes(2) = FindNextPlatQTE(SponsorsUsersPlatCode(2), 12)
            //    End If
            //    UpdateUsersPlatinumCodes(NewUsersID, NewPlatCodes(0), NewPlatCodes(1), NewPlatCodes(2))
            //    UpdateCustomersPlatinumCodes(NewUsersID, NewPlatCodes(0), NewPlatCodes(1), NewPlatCodes(2))
            //End If
            if (NewUsersID > 0 && SponsorID > 0)
            {

                model = _commonService.GetUsersPlatinumCodes(10, SponsorID);
                if (model != null)
                {


                    List<long> SponsorsUsersPlatCode = new List<long>();
                    SponsorsUsersPlatCode[0] = model.P1;
                    SponsorsUsersPlatCode[1] = model.P1;
                    SponsorsUsersPlatCode[2] = model.P1;
                    List<long> NewPlatCodes = new List<long>();
                    _commonService.CleanUpOrder0QtePromoData(11, SponsorsUsersPlatCode[0]);
                    _commonService.CleanUpOrder0QtePromoData(11, SponsorsUsersPlatCode[1]);
                    _commonService.CleanUpOrder0QtePromoData(11, SponsorsUsersPlatCode[2]);
                    //SetQTEPromoData(SponsorsUsersPlatCode(0), 10, 0)


                }
            }
        }


        public void SetQTEPromoData(long UsersID, long RankID, long OrderID)
        {
            PopulateCV(UsersID, OrderID);
            switch (RankID)
            {
                case 2:
                    // 'no additional requirements';
                    break;

                case 3:
                    //'+ legs (4)
                    PopulateLegs(UsersID, OrderID);
                    break;
                default:
                    break;
            }

        }

        public void PopulateCV(long UsersID, long OrderID)
        {
            long QteDataRecord = _commonService.GetQteDataRecord(1, UsersID, OrderID);//core

            long GetQteMetaValue = _commonService.GetQteMetaValue(2, QteDataRecord, "CV");//core
            if (GetQteMetaValue < 20)
            {
                long RetVal = _commonService.GetQteDataRecord(3, UsersID, 0);//core
                if (RetVal > 0)
                {
                    _commonService.SetQteMetaValue(4, QteDataRecord, "CV", RetVal);//core

                }
            }
        }

        public void PopulateLegs(long UsersID, long OrderID)
        {
            long TempLegs = 0;
            long QteDataRecord = _commonService.GetQteDataRecord(1, UsersID, OrderID);//core

            long GetQteMetaValue = _commonService.GetQteMetaValue(2, QteDataRecord, "Legs");//core
            if (GetQteMetaValue < 21)
            {
                var result = _membersService.GetLegsDetails(3, UsersID);


                foreach (var item in result)
                {


                    if (item.Id > 0)
                    {
                        TempLegs += 1;
                    }
                }

                //'check for your order...
                long MyTV = _membersService.GetMyTV(4, UsersID);
            }
        }


        public void NewCSJRecord(long UserId, string NewComment)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            CSJDetailsViewModel model = new CSJDetailsViewModel();
            string CurrentRecord = string.Empty;
            var loggedInUser = GetCacheUserInfo();

            var CSJDetails = _adminService.GetCSJDetails(4, Convert.ToInt64(UserId));
            if (CSJDetails != null)
            {
                CurrentRecord = CSJDetails.Comments;
            }



            var CommentsString = DateTime.Now.ToString() + ' ' + loggedInUser.UserName + ' ' + "wrote:" + Environment.NewLine.ToString() + " " + NewComment + Environment.NewLine.ToString() + " " + CurrentRecord;
            model.Comments = CommentsString;
            model.UserId = UserId;
            model.UserName = loggedInUser.UserName;
            baseModel = _adminService.UpdateCSJ(2, model);
        }

        public ActionResult SentPin(long Id, long theUsersID, string PinRankName)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            string PinMsg = "";

            long LogRankID = Id;

            PinMsg = PinRankName + " pin sent";
            _baseModel = _commonService.SentPin(3, LogRankID);

            //'create new csj record of when pin was sent
            NewCSJRecord(theUsersID, PinMsg);
            //'todo send out email notifying user their pin was sent

            if (_baseModel.RowAffected > 0)
            {
                //TempData["SUCCESS_MESSAGE_NotesUpdated"] = "Notes Updated";

            }
            TempData["ActiveTab"] = "PinCertificateListTab";
            return RedirectToAction("AdminMember");
        }


        [HttpPost]
        public ActionResult PinCertificateListNoCall(long LogRankID, long theUsersID, string PinRankName)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            string PinMsg = "";

            try
            {

                PinMsg = PinRankName + " pin Not sent";
                baseModel = _commonService.SentPin(3, LogRankID);
                NewCSJRecord(theUsersID, PinMsg);


                TempData["ActiveTab"] = "PinCertificateListTab";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("AdminMember");
        }


        public void ExportAllList()
        {

        }


        public ActionResult CallList(long LogID, long theUsersID, string CommandName, string PinRankName)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string CalledMsg = "";
                if (CommandName == "Called")
                {

                    CalledMsg = "Called and congratulated for their " + PinRankName + " rank achievement";

                    baseModel = _adminService.UpdateCalled(3, LogID);
                    NewCSJRecord(theUsersID, CalledMsg);

                }
                else if (CommandName == "MyCall")
                {
                    baseModel = _adminService.ClaimThisCall(4, LogID, loggedInUser.UserId);


                }
                TempData["ActiveTab"] = "CallListTab";


            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction("AdminMember");
        }

        [HttpPost]
        public ActionResult CallListNoCall(long LogID, long theUsersID, string PinRankName)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            string CalledMsg = "";
            try
            {

                CalledMsg = "Did not call or congratulate for their " + PinRankName + " rank achievement";

                baseModel = _adminService.UpdateCalled(3, LogID);
                //'create new csj record of when pin was sent
                NewCSJRecord(theUsersID, CalledMsg);
                //'todo send out email notifying user their pin was sent
                TempData["ActiveTab"] = "CallListTab";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("AdminMember");
        }



        public ActionResult ExportToExcel()
        {

            var GetPinCertificateList = _adminService.GetPinCertificateList(4);

            DataTable dt = CommonViewModel.ToDataTable<PinCertificateListViewModel>(GetPinCertificateList);
            dt.Columns.RemoveAt(0);//Remove column1
            dt.Columns.RemoveAt(0);//Remove column2
                                   //gv.DataSource = dt;
                                   //gv.DataBind();



            //string mydate = DateTime.Now.Date.ToString("MMddyyyy");
            //Response.ClearContent();
            //Response.Buffer = true;

            //Response.Charset = "";

            //Response.AddHeader("content-disposition", "attachment;filename=Foxx_RankPinReport_" + mydate + ".csv");
            //// Response.ContentType = "application/ms-excel";
            //Response.ContentType = "application/text";

            //Response.Charset = "";
            //StringWriter objStringWriter = new StringWriter();
            //HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);

            //gv.RenderControl(objHtmlTextWriter);

            //Response.Output.Write(objStringWriter.ToString());
            //Response.Flush();
            //Response.End();

            ExportToCSV(dt, "Foxx_RankPinReport_");

            return RedirectToAction("AdminMember");
        }

        protected void ExportToCSV(DataTable dt, string ReportName)
        {
            string mydate = DateTime.Now.Date.ToString("MMddyyyy");
            //Get the data from database into datatable
            Response.Clear();
            Response.Buffer = true;

            // Response.AddHeader("content-disposition", "attachment;filename=" + ReportName + "_" + DateTime.Now + ".csv");
            Response.AddHeader("content-disposition", "attachment;filename=Foxx_RankPinReport_" + mydate + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            StringBuilder sb = new StringBuilder();

            for (int k = 0; k < dt.Columns.Count; k++)

            {
                //add separator
                sb.Append(dt.Columns[k].ColumnName + ',');
            }

            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();

        }

        //protected void ExportToPDF(DataTable dt, string ReportName)
        //{
        //    //Create a dummy GridView

        //    GridView GridView1 = new GridView();

        //    GridView1.AllowPaging = false;

        //    GridView1.DataSource = dt;

        //    GridView1.DataBind();

        //    Response.ContentType = "application/pdf";

        //    Response.AddHeader("content-disposition", "attachment;filename=" + ReportName + "_" + DateTime.Now + ".pdf");

        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

        //    StringWriter sw = new StringWriter();

        //    HtmlTextWriter hw = new HtmlTextWriter(sw);

        //    GridView1.RenderControl(hw);

        //    StringReader sr = new StringReader(sw.ToString());

        //    Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);

        //    HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

        //    PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        //    pdfDoc.Open();

        //    htmlparser.Parse(sr);

        //    pdfDoc.Close();

        //    Response.Write(pdfDoc);

        //    Response.End();
        //}


        [HttpPost]
        public ActionResult ClearLog()
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();

            try
            {
                baseModel = _adminService.ClearLog(2);
                if (baseModel != null)
                {


                }

                TempData["ActiveTab"] = "AdminHomeTab";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction("Home");
        }

        public long RecipientsOrActivityCountInTime(long UsersID, string TimeFrame, string RecipientOrActivity)
        {
            long TheCount = 0;
            string TimeClause = "";
            string TableName = "MobileAppRecipient";
            TableName = RecipientOrActivity.ToLower() == "activity" ? "MobileAppRecipientActivity" : "MobileAppRecipient";
            switch (TimeFrame.ToLower())
            {


                case "72h":
                    TimeClause = "AND (" + TableName + ".CreateDate >= DATEADD(hh, - 72, GETDATE()))";
                    break;
                case "7day":
                    TimeClause = "AND (" + TableName + ".CreateDate >= DATEADD(d, - 7, GETDATE()))";
                    break;
                case "8to14day":
                    TimeClause = "AND (" + TableName + ".CreateDate between DATEADD(d, - 8, GETDATE()) AND  DATEADD(d, - 14, GETDATE()))";
                    break;
                case "15to21day":
                    TimeClause = "AND (" + TableName + ".CreateDate between DATEADD(d, - 15, GETDATE()) AND  DATEADD(d, - 21, GETDATE()))";
                    break;
                case "22to28day":
                    TimeClause = "AND (" + TableName + ".CreateDate between DATEADD(d, - 22, GETDATE()) AND  DATEADD(d, - 28, GETDATE()))";
                    break;
                default:
                    TimeClause = "";
                    break;


            }

            if (RecipientOrActivity.ToLower() == "activity")
            {
                TheCount = _membersService.GetMobileAppActivityDetails(11, UsersID, TimeClause);
            }
            else
            {
                TheCount = _membersService.GetMobileAppActivityDetails(10, UsersID, TimeClause);
            }

            return TheCount;
        }

        [HttpPost]
        public JsonResult QTEUpdated(long? UserId, string TabName)
        {

            var Date = "";
            //6/10/2021 8:34 AM
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            //Public MyCulture As IFormatProvider = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")
            var time = DateTime.Now.ToString("hh:mm tt");
            // var startDay = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.;
            // var dd = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            // var dddd = dd.DateTimeFormat.d;
            // Now.ToLongTimeString
            string dt = DateTime.Now.Date.ToString("MM-dd-yyyy");
            Date = dt.Replace("-", "/") + ' ' + time;


            //    Core.SetQteNowForMBO(SelectedUsersID)
            //lblQTERank.Text = Core.getQteNowForMBO(SelectedUsersID)

            //if (_baseModel.RowAffected > 0)
            //{

            string Message = "QTE Updated" + ' ' + Date;


            //}
            //else
            //{

            //}

            if (TabName == "UserInfoDetailsFromTree")
            {


            }
            else if (TabName == "RankHistoryFromTree")
            {


            }
            return Json(Message, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Orders(string SearchBy)
        {
            Orders model = new Orders();
            if (string.IsNullOrEmpty(SearchBy))
            {
                SearchBy = "";
            }
            model.myOrders = _membersService.GetOrdersList(3, 0, SearchBy);
            return View(model);
        }

        [HttpGet]
        public ActionResult OrdersAsPerCustomer(long CustomerId)
        {
            Orders model = new Orders();
            model.myOrders = _membersService.GetMyOrdersList(4, CustomerId).Where(x => x.CustomerId == CustomerId).ToList();
            foreach (var order in model.myOrders)
            {
                order.Status = Enum.GetName(typeof(OrderStatus), order.OrderStatusId);
            }

            model.GrandOrderTotal = model.myOrders.Count > 0 ? model.myOrders.Sum(x => x.OrderTotal).ToString() : "0";
            return View(model);
        }
        [HttpGet]
        public ActionResult OrdersDetails(long OrderId, long CustomerId)
        {
            OrderDetailsModel model = new OrderDetailsModel();

            model.myOrders = _membersService.GetMyOrdersList(4, CustomerId).Where(x => x.OrderId == Convert.ToString(OrderId)).FirstOrDefault();
            model.Items = _membersService.GetMyOrdersDetailsList(2, OrderId);
            model.OrderStatus = Enum.GetName(typeof(OrderStatus), model.myOrders.OrderStatusId);
            model.ShippingStatus = Enum.GetName(typeof(ShippingStatus), model.myOrders.ShippingStatusId);
            model.PaymentMethodStatus = Enum.GetName(typeof(PaymentStatus), model.myOrders.PaymentStatusId);
            return View(model);
        }

        [HttpGet]
        public ActionResult AutoshipOrdersDetails(long OrderId, long CustomerId)
        {
            OrderDetailsModel model = new OrderDetailsModel();

            model.myOrders = _membersService.GetMyOrdersList(4, CustomerId).Where(x => x.OrderId == Convert.ToString(OrderId)).FirstOrDefault();
            model.Items = _membersService.GetMyOrdersDetailsList(2, OrderId);
            model.OrderStatus = Enum.GetName(typeof(OrderStatus), model.myOrders.OrderStatusId);
            model.ShippingStatus = Enum.GetName(typeof(ShippingStatus), model.myOrders.ShippingStatusId);
            model.PaymentMethodStatus = Enum.GetName(typeof(PaymentStatus), model.myOrders.PaymentStatusId);
            return View(model);
        }


        [HttpPost]
        public ActionResult VIEImport(ImportExcelViewModel importExcel)
        {
            if (ModelState.IsValid)
            {
                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                var ModelList = new List<VIEImportViewModel>();
                if ((importExcel.file != null) && (importExcel.file.ContentLength > 0) && !string.IsNullOrEmpty(importExcel.file.FileName))
                {
                    string fileName = importExcel.file.FileName;
                    string fileContentType = importExcel.file.ContentType;
                    byte[] fileBytes = new byte[importExcel.file.ContentLength];
                    var data = importExcel.file.InputStream.Read(fileBytes, 0, Convert.ToInt32(importExcel.file.ContentLength));



                    using (var package = new ExcelPackage(importExcel.file.InputStream))
                    {
                        var currentSheet = package.Workbook.Worksheets;
                        var workSheet = currentSheet.First();

                        var noOfCol = workSheet.Dimension.End.Column;
                        var noOfRow = workSheet.Dimension.End.Row;
                        long count = 1;
                        var excelcolName = "";
                        string OriginalColumnName = "";
                        int index = 1;
                        if (noOfCol == 14)
                        {
                            foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])  //.Cells[Row start, Column Start, Row end, Column End]
                            {
                                OriginalColumnName = Enum.GetName(typeof(VIEImportColumnList), index);
                                excelcolName = firstRowCell.Text;

                                if (OriginalColumnName != null)
                                {
                                    if (excelcolName != OriginalColumnName)
                                    {
                                        //TempData["SUCCESS_MESSAGE"] = "Error in excel File. Please check the Column Name properly";
                                        TempData["SUCCESS_MESSAGE"] = "Invalid Column " + excelcolName + "! Column Name Shoud be " + OriginalColumnName + "";
                                        TempData["ActiveTab"] = "VIEImportTab";
                                        return RedirectToAction("Utilities", "AdminPermission");
                                    }
                                    else
                                    {
                                        TempData["SUCCESS_MESSAGE"] = "";
                                    }
                                }
                                else
                                {
                                    TempData["SUCCESS_MESSAGE"] = "Error in excel File. Please check the file format properly";
                                    TempData["ActiveTab"] = "VIEImportTab";
                                    return RedirectToAction("Utilities", "AdminPermission");
                                }

                                index++;

                            }
                        }
                        else
                        {
                            TempData["SUCCESS_MESSAGE"] = "Error in excel File. Please check the file format properly";
                            TempData["ActiveTab"] = "VIEImportTab";
                            return RedirectToAction("Utilities", "AdminPermission");
                        }

                        for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                        {
                            var model = new VIEImportViewModel();
                            model.IndexNo = count;
                            model.EnrollmentDate = Convert.ToString(workSheet.Cells[rowIterator, 1].Value);
                            model.RecordLocatorNumber = Convert.ToString(workSheet.Cells[rowIterator, 2].Value);
                            model.FirstName = Convert.ToString(workSheet.Cells[rowIterator, 3].Value);
                            model.LastName = Convert.ToString(workSheet.Cells[rowIterator, 4].Value);
                            model.CreditScoreClass = Convert.ToString(workSheet.Cells[rowIterator, 5].Value);
                            model.ProductCode = Convert.ToString(workSheet.Cells[rowIterator, 6].Value);
                            model.ProductTerm = Convert.ToString(workSheet.Cells[rowIterator, 7].Value);
                            model.MemberStatus = Convert.ToString(workSheet.Cells[rowIterator, 8].Value);
                            model.SponsorStatus = Convert.ToString(workSheet.Cells[rowIterator, 9].Value);
                            model.StreetAddress = Convert.ToString(workSheet.Cells[rowIterator, 10].Value);
                            model.City = Convert.ToString(workSheet.Cells[rowIterator, 11].Value);
                            model.State = Convert.ToString(workSheet.Cells[rowIterator, 12].Value);
                            model.AddressPostalCode = Convert.ToString(workSheet.Cells[rowIterator, 13].Value);
                            model.OVCPStatus = Convert.ToString(workSheet.Cells[rowIterator, 14].Value);
                            count++;

                            ModelList.Add(model);
                        }
                    }

                    DataTable dt = CommonViewModel.ToDataTable<VIEImportViewModel>(ModelList);




                    _baseModel = _adminService.VIEImportExcel(1, dt);

                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Data Inserted Sucessfully";



                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
                    }

                }
            }

            TempData["ActiveTab"] = "VIEImportTab";
            return RedirectToAction("Utilities", "AdminPermission");
        }




        [HttpPost]
        public ActionResult VIEProcessCommission()
        {
            string cartid = CommonViewModel.getrandseq();
            long OrderID = 0;
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            var CustomerIdList = _adminService.GetCustomerIdList(2);
            //using (var dataContext = new FoxxlegacyAppContext())
            //{
            //    using (System.Data.Entity.DbContextTransaction dbTran = dataContext.Database.BeginTransaction())
            //    {
            try
            {
                foreach (var model in CustomerIdList)
                {
                    ItemModel Item = _adminService.GetItemDetails(5, itemID).FirstOrDefault();
                    model.Customerid = model.SponsorID;
                    model.Price = Item.Price;
                    model.Cartid = cartid;
                    model.OrderNo = _adminService.GetNextOrderNo(4);
                    _baseModel = _adminService.CreateOrder(1, model);
                    if (_baseModel.RowAffected > 0)
                    {
                        OrderID = Convert.ToInt64(_baseModel.InsertedId);
                        if (_adminService.IsOrderOvcp(3, OrderID) == false)
                        {


                            //  Core.OrderVerifyCompPlan(OrderID)///Pending

                            TempData["SUCCESS_MESSAGE"] = "Process Complete for " + OrderID + "";
                        }
                        else

                        {
                            TempData["ERROR_MESSAGE"] = OrderID + "was already OVCP'd. No need to do it again";
                        }

                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Oops! Something went wrong";
                    }
                }
                //dbTran.Commit();
            }
            catch (Exception ex)
            {
                //Rollback transaction if exception occurs    
                // dbTran.Rollback();
            }

            TempData["ActiveTab"] = "VIEProcessCommissionTab";
            // }
            //}
            return RedirectToAction("Utilities", "AdminPermission");


        }


        public ActionResult BroadcastReports(string TabName, string MenuName)
        {
            // TempData["ActiveTab"] = "BroadcastReportsTab";
            TempData["BroadcastReports"] = "BroadcastReports";
            return RedirectToAction(MenuName, "admin", new { TabName = "BroadcastReports" });
        }

        [HttpPost]
        public ActionResult ClearEmailData(string TabName)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                _baseModel = _adminService.ClearBroadcastReportsEmailSms(2);
                TempData["ActiveTab"] = "BroadcastReportsTab";
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "EmailData Clear successfully.";
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction(TabName, "admin", new { TabName = "BroadcastReports" });

        }

        [HttpPost]
        public ActionResult ClearSMSData(string TabName)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                _baseModel = _adminService.ClearBroadcastReportsEmailSms(3);
                TempData["ActiveTab"] = "BroadcastReportsTab";
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "SMSData Clear successfully.";
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }

            return RedirectToAction(TabName, "admin", new { TabName = "BroadcastReports" });

        }


        [HttpGet]
        public ActionResult SentEmailReport(string TabName, string MenuName)
        {

            try
            {

                TempData["SentEmailReport"] = "SentEmailReport";
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction(MenuName, "admin", new { TabName = "SentEmailReport" });
        }


        public ActionResult EmailReports(string qtype, string TabName, string MenuName)
        {
            try
            {

                TempData["EmailReports"] = "EmailReports";
                TempData["EmailReportsqtype"] = qtype;

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction(MenuName, "admin", new { TabName = "EmailReports" });
        }
        public ActionResult SmsReports(string qtype, string TabName, string MenuName)
        {
            try
            {

                TempData["SmsReports"] = "SmsReports";
                TempData["SmsReportsqtype"] = qtype;
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Oops something went wrong!";
            }
            return RedirectToAction(MenuName, "admin", new { TabName = "SmsReports" });
        }

        public ActionResult WelcomeEmail(long? UserId, string UserName)
        {
            TempData["ActiveTab"] = "MemberInfonTab";
            string Password = "";
            Password = _userService.GetPassword(1, (long)UserId);

            if (!string.IsNullOrEmpty(Password))
            {
                //QMailToMember_Welcome(UserId, UserId, 1, Password);
                ViewBag.Message = "Email Sent";
            }
            return RedirectToAction("contact_inner", "admin", new { UserId = UserId, ActiveTab = TempData["ActiveTab"] });
        }
        public string GetPassword(long UserId, string UserName)
        {
            string Password = "";
            if (_userService.IsUserInRole(1, UserName, "admin") == false)
            {
                try
                {
                    Password = _userService.GetPassword(1, UserId);
                }
                catch (Exception ex)
                {
                    Password = "";
                }
            }
            else if (UserName.ToLower() == "cking")
            {
                if (UserName.ToLower() != "cking" & UserName.ToLower() != "master" & UserName.ToLower() != "rbaker")
                {
                    Password = _userService.GetPassword(1, UserId);
                    try
                    {
                        Password = _userService.GetPassword(1, UserId);
                    }
                    catch (Exception ex)
                    {
                        Password = "";
                    }
                }
                else
                    Password = "";
            }
            return Password;
        }

        //public bool IsUserOptOut(int UsersID, int OptOutCatID) // related: isSmsOptIn
        //{
        //    IsUserOptOut = false;
        //    SqlConnection Conn = new SqlConnection(Application("ConnStrDotNet"));
        //    Conn.Open();
        //    string Sql = "Select Count(ID) as TheCount from OptOut where (UsersID = " + UsersID + ") AND (OptOut_CatsID = " + OptOutCatID + ")";
        //    SqlCommand Command = new SqlCommand(Sql, Conn);
        //    object RetVal = Command.ExecuteScalar;
        //    Command.Dispose();
        //    Sql = null;
        //    Conn.Close();
        //    Conn.Dispose();
        //    if (Information.IsNothing(RetVal) == false)
        //    {
        //        if (Information.IsDBNull(RetVal) == false)
        //        {
        //            if (Information.IsNumeric(RetVal) == true)
        //            {
        //                if (RetVal > 0)
        //                    IsUserOptOut = true;
        //            }
        //        }
        //    }
        //    return IsUserOptOut;
        //}

        //public void QMailToMember_Welcome(long RecipientID, long TriggerID, long EmailID, string strPassword)
        //{
        //    if (_userService.IsUserOptOut(RecipientID, 0) == false)
        //    {
        //        string TheSubject = "";
        //        string TheBody = "";

        //        SetEmailSubjectAndBody(EmailID, TheSubject, TheBody);
        //        if (TheSubject == "" | TheBody == "")
        //            return;
        //        string R_U = "";
        //        string R_SU = "";
        //        string R_F = "";
        //        string R_L = "";
        //        string R_P = "";
        //        string R_E = "";
        //        string R_RANK = "";
        //        string R_OLDRANK = "";

        //        SetUSUFLEROr(RecipientID, R_U, R_SU, R_F, R_L, R_P, R_E, R_RANK, R_OLDRANK);

        //        var LabelValue = "%Name:" + R_F + " " + R_L + "%Phone:" + R_P + "%Email:" + R_E + "%";
        //        if (!(string.IsNullOrEmpty(LabelValue) | string.IsNullOrWhiteSpace(LabelValue)))
        //        {
        //            byte[] byt = System.Text.Encoding.UTF8.GetBytes(LabelValue);
        //            LabelValue = Convert.ToBase64String(byt);
        //        }
        //        if (R_E == "")
        //            return;
        //        string TG_U = "";
        //        string TG_SU = "";
        //        string TG_F = "";
        //        string TG_L = "";
        //        string TG_P = "";
        //        string TG_E = "";
        //        string TG_RANK = "";
        //        string TG_OLDRANK = "";
        //        SetUSUFLEROr(TriggerID, ref TG_U, ref TG_SU, ref TG_F, ref TG_L, ref TG_P, ref TG_E, ref TG_RANK, ref TG_OLDRANK);

        //        TheSubject = TheSubject.Replace("@@R_U@@", R_U);
        //        TheSubject = TheSubject.Replace("@@URL@@", R_U + "&Label=" + LabelValue);
        //        TheSubject = TheSubject.Replace("@@R_SU@@", R_SU);
        //        TheSubject = TheSubject.Replace("@@R_F@@", R_F);
        //        TheSubject = TheSubject.Replace("@@R_L@@", R_L);
        //        TheSubject = TheSubject.Replace("@@R_E@@", R_E);
        //        TheSubject = TheSubject.Replace("@@R_RANK@@", R_RANK);
        //        TheSubject = TheSubject.Replace("@@R_OLDRANK@@", R_OLDRANK);
        //        TheSubject = TheSubject.Replace("@@C_NAME@@", R_F + " " + R_L);

        //        TheSubject = TheSubject.Replace("@@TG_U@@", TG_U);
        //        TheSubject = TheSubject.Replace("@@TG_SU@@", TG_SU);
        //        TheSubject = TheSubject.Replace("@@TG_F@@", TG_F);
        //        TheSubject = TheSubject.Replace("@@TG_L@@", TG_L);
        //        TheSubject = TheSubject.Replace("@@TG_E@@", TG_E);
        //        TheSubject = TheSubject.Replace("@@TG_RANK@@", TG_RANK);
        //        TheSubject = TheSubject.Replace("@@TG_OLDRANK@@", TG_OLDRANK);

        //        TheBody = TheSubject.Replace("@@R_U@@", R_U);
        //        TheBody = TheSubject.Replace("@@R_Password@@", strPassword);
        //        TheBody = TheSubject.Replace("@@URL@@", R_U + "&Label=" + LabelValue);
        //        TheBody = TheSubject.Replace("@@R_SU@@", R_SU);
        //        TheBody = TheSubject.Replace("@@R_F@@", R_F);
        //        TheBody = TheSubject.Replace("@@R_L@@", R_L);
        //        TheBody = TheSubject.Replace("@@R_E@@", R_E);
        //        TheBody = TheSubject.Replace("@@R_RANK@@", R_RANK);
        //        TheBody = TheSubject.Replace("@@R_OLDRANK@@", R_OLDRANK);

        //        TheBody = TheSubject.Replace("@@TG_U@@", TG_U);
        //        TheBody = TheSubject.Replace("@@TG_SU@@", TG_SU);
        //        TheBody = TheSubject.Replace("@@TG_F@@", TG_F);
        //        TheBody = TheSubject.Replace("@@TG_L@@", TG_L);
        //        TheBody = TheSubject.Replace("@@TG_E@@", TG_E);
        //        TheBody = TheSubject.Replace("@@TG_RANK@@", TG_RANK);
        //        TheBody = TheSubject.Replace("@@TG_OLDRANK@@", TG_OLDRANK);
        //        TheBody = TheSubject.Replace("@@C_NAME@@", R_F + " " + R_L);

        //        var QueID = InsertToEmailQue(ConfigurationManager.AppSettings["AdminFromEmail"], R_E, TheSubject, TheBody);
        //    }
        //}
        //public void SetEmailSubjectAndBody(int EmailID, ref string Subject, ref string Body)
        //{
        //    string SqlString = "SELECT Subject, Body FROM Email WHERE (ID=" + EmailID + ")";
        //    Subject = Reader("Subject");
        //    Body = Replace(Replace(Replace(Replace(Reader("Body"), "DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"", ""), "    ", ""), Environment.NewLine, ""), "<!---->", "");
        //}

        //public void SetUSUFLEROr(long UsersID, ref string Username, ref string UsersSponsor, ref string FirstName, ref string LastName, ref string Phone, ref string Email, ref string Rank, ref string OldRank)
        //{
        //    SqlConnection Conn = new SqlConnection(Application("ConnStrDotNet"));
        //    Conn.Open();
        //    string Sql = "SELECT Users.UserName, Users_1.UserName AS UsersSponsor, Users.FirstName, Users.LastName, Users.Phone, Users.Email, Ranks.RankName, Ranks_1.RankName AS OldRankName FROM Users LEFT OUTER JOIN Users AS Users_1 ON Users.SponsorUsersID = Users_1.ID INNER JOIN Ranks ON Users.RankID = Ranks.ID  LEFT OUTER JOIN Ranks AS Ranks_1 ON Users.RankID - 1 = Ranks_1.ID WHERE (Users.ID = " + UsersID + ")";
        //    SqlCommand Command = new SqlCommand(Sql, Conn);
        //    SqlDataReader Reader = Command.ExecuteReader;
        //    if (Reader.Read)
        //    {
        //        if (Reader("UserName") != null & Reader("UserName").ToString != "")
        //            Username = Reader("UserName");
        //        if (Reader("UsersSponsor") != null & Reader("UsersSponsor").ToString != "")
        //            UsersSponsor = Reader("UsersSponsor");
        //        if (Reader("FirstName") != null & Reader("FirstName").ToString != "")
        //            FirstName = Reader("FirstName");
        //        if (Reader("LastName") != null & Reader("LastName").ToString != "")
        //            LastName = Reader("LastName");
        //        if (Reader("Phone") != null & Reader("Phone").ToString != "")
        //            Phone = Reader("Phone");
        //        if (Reader("Email") != null & Reader("Email").ToString != "")
        //            Email = Reader("Email");
        //        if (Reader("RankName") != null & Reader("RankName").ToString != "")
        //            Rank = Reader("RankName");
        //        if (Reader("OldRankName") != null & Reader("OldRankName").ToString != "")
        //            OldRank = Reader("OldRankName");
        //    }
        //    Reader.Close();
        //    Command.Dispose();
        //    Conn.Close();
        //    Conn.Dispose();
        //}

        public ActionResult HasW9(int UserId)
        {
            bool status = false;
            string msg = string.Empty;
            string pendingCommissions = string.Empty;
            string paidCommissionsContent = string.Empty;
            decimal amount = 0;
            decimal pendingAmount = 0;
            decimal paidAmount = 0;
            string comList = string.Empty;
            try
            {

                status = _userService.HasW9(UserId);
                var commission = _userService.GetTotalEarned(UserId);
                var pending = _userService.GetPendingCommissions(UserId);
                var paidCommissions = _userService.GetPaidCommissions(UserId);
                if (commission != null)
                {
                    amount = commission.amount;
                    comList = ConvertPartialViewToString("_commissionDetails", commission.commissionList);
                }
                if (pending != null)
                {
                    pendingAmount = pending.Sum(x => x.SumTotal);
                    pendingCommissions = ConvertPartialViewToString("_pendingCommissions", pending);
                }
                if (paidCommissions != null)
                {
                    paidAmount = paidCommissions.Sum(x => x.Amount);
                    paidCommissionsContent = ConvertPartialViewToString("_paidCommissions", paidCommissions);
                }
                return Json(new { status, msg, amount, comList, pendingCommissions, pendingAmount, paidAmount, paidCommissionsContent }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg, pendingAmount }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCommissionByYear(int userId, int Year)
        {
            bool status = false;
            string msg = string.Empty;
            string content = string.Empty;
            try
            {
                var data = _userService.GetCommissionByYear(userId, Year);
                content = ConvertPartialViewToString("_commissionDetails", data);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg, content }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCommissionByMonth(int userId, int Year, int Month)
        {
            bool status = false;
            string msg = string.Empty;
            string content = string.Empty;
            string detailsContent = string.Empty;
            try
            {
                var data = _userService.GetCommissionByMonth(userId, Year, Month);
                var details = _userService.GetCommissionDetailsByMonthYear(userId, Year, Month);
                content = ConvertPartialViewToString("_commissionDetails", data);//_commissionDescription
                detailsContent = ConvertPartialViewToString("_commissionDescription", details);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg, content, detailsContent }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCommisionsByOrderId(int UserId, int OrderId)
        {
            string msg = string.Empty;
            string content = string.Empty;
            GetCommissionModel model = new GetCommissionModel();

            bool status;
            try
            {
                model.OrderNo = OrderId;
                // model.UserId = UserId;
                model.SearchType = 1;
                model.UserId = 0;
                var data = _commissionService.GetCommision(model);
                content = ConvertPartialViewToString("_commissionsBOrderID", data);
                //var data = _commissionService.GetMyCommision(model);
                //content = ConvertPartialViewToString("_myPersonalCommissions", data);
                status = true;
                return Json(new { status, msg, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptUser(int UserId)
        {
            bool status = false;
            string msg = string.Empty;
            string content = string.Empty;
            try
            {
                var smsCount = _userService.CheckUserSMSOptOut(UserId);
                var SmsCountGeneral = _userService.CheckUserSMSOptOutGeneralCount(UserId);

                var CheckEmailOptIn = _userService.CheckEmailOptIn(UserId);
                var emailCount = _userService.CheckUserEmailOptOut(UserId);
                var emailCountGeneral = _userService.CheckUserEmailOptOutGeneralCount(UserId);

                (bool CheckSMSOptIn, bool MobileVerified) = _userService.CheckSMSOptIn(UserId);
                status = true;
                return Json(new { status, smsCount, SmsCountGeneral, CheckEmailOptIn, emailCount, emailCountGeneral, CheckSMSOptIn, MobileVerified }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

            }

            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptOutUser(int UserId, string EmailSms, string OptType)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                if (OptType == "emailOutGen" || OptType == "smsOutGen")
                {
                    var data = _userService.OptUserOut(UserId, EmailSms, 0);
                }
                else
                {
                    var opt = _userService.ToggleEmailSmSOptOutIn(UserId, EmailSms, 0);
                    var data = _userService.OptOutFromEmail(UserId, EmailSms);
                }

                status = true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptInUser(int UserId, string EmailSms, string OptType)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                if (OptType == "emailInGen" || OptType == "smsInGen")
                {
                    var data = _userService.KillOptOut(UserId, EmailSms);
                }
                else
                {
                    var data = _userService.KillOptOut(UserId, EmailSms);
                    var opt = _userService.ToggleEmailSmSOptOutIn(UserId, EmailSms, 1);
                }

                status = true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptOutofNonSystem(string Phone)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var data = _userService.OptOutofNonSystem(Phone);
                status = true;
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCsjComments(int UserId)
        {
            bool status = false;
            string msg = string.Empty;
            string Content = string.Empty;
            try
            {
                List<CSJDetailsViewModel> data = _adminService.GetNewCsj(1, Convert.ToInt64(UserId));
                Content = ConvertPartialViewToString("_csjCommentList", data);
                status = true;
                return Json(new { status, msg, Content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Get404Logs()
        {
            bool status = false;
            string msg = string.Empty;
            string Content = string.Empty;
            try
            {
                var data = _adminService.GetNotFoundLogs();
                Content = ConvertPartialViewToString("_notFoundLogs", data);
                status = true;
                return Json(new { status, msg, Content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult NewAutoship(int AdId, string CustomerPaymentID, string CustomerAddressId)
        {
            RecurringOrderViewModel recurringOrderViewModel = new RecurringOrderViewModel();
            CreateCustomerProfile ccp = new CreateCustomerProfile();
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            int UserId = 0;
            //need to block
            UserId = (Int32)loggedInUser.UserId;
            if (Session["AutoshipUserId"] != null)
            {
                string SessionUserId = Session["AutoshipUserId"].ToString();
                UserId = Convert.ToInt32(SessionUserId);
            }

            if (AdId == 0)
            {
                recurringOrderViewModel.UserId = UserId;
                recurringOrderViewModel.Status = true;

                _model = _membersService.InsertUpdateAutoship(3, recurringOrderViewModel);
                if (_model.InsertedId > 0)
                {
                    AdId = (Int32)_model.InsertedId;
                    return RedirectToAction("NewAutoship", new { AdId = AdId });
                }
            }
            if (CustomerPaymentID == null)
            {
                if (Session["CustomerPaymentID"] != null)
                {
                    CustomerPaymentID = Session["CustomerPaymentID"].ToString();
                }
            }
            if (CustomerAddressId == null)
            {
                if (Session["CustomerAddressId"] != null)
                {
                    CustomerAddressId = Session["CustomerAddressId"].ToString();
                }
            }
            recurringOrderViewModel.AdId = AdId;
            recurringOrderViewModel = _membersService.GetRecurringOrder(6, recurringOrderViewModel);
            recurringOrderViewModel.ProductListFromNopCom = _membersService.GetProductListFromNopCom(8);
            recurringOrderViewModel.ProductListFoxx = _membersService.GetProductListFoxx(2, AdId);
            if (recurringOrderViewModel.ProductListFoxx.Count > 0)
            {
                ViewBag.Total = recurringOrderViewModel.ProductListFoxx.Sum(x => x.Price);
            }
            else
            {
                ViewBag.Total = 0;
            }

            recurringOrderViewModel.UserId = UserId;
            recurringOrderViewModel.StateList = _commonService.GetAllDropdownlist(8);
            recurringOrderViewModel.AutoshipPeriodList = _commonService.GetAllDropdownlist(9);
            var CustomerProfile = _membersService.GetCustomerProfileId(9, recurringOrderViewModel);
            if (CustomerProfile != null)
            {

                recurringOrderViewModel.CustomerProfileId = CustomerProfile.CustomerProfileId;
            }
            else
            {
                recurringOrderViewModel.CustomerProfileId = "0";
            }

            recurringOrderViewModel.NickNamePaymentMethodList = _membersService.GetAllDropdownlistForPaymentAddressById(4, recurringOrderViewModel.CustomerProfileId);
            recurringOrderViewModel.NickNameShipingAddressList = _membersService.GetAllDropdownlistForPaymentAddressById(5, recurringOrderViewModel.CustomerProfileId);
            if (CustomerPaymentID != null)
            {
                var rr = ccp.GetCustomerProfile(recurringOrderViewModel.CustomerProfileId);
                var response2 = ccp.GetCustomerPaymentProfile(recurringOrderViewModel.CustomerProfileId, CustomerPaymentID);
                if (response2 != null)
                {
                    recurringOrderViewModel.PaymentMethod = response2;
                    recurringOrderViewModel.CustomerPaymentID = CustomerPaymentID;
                    var Name = recurringOrderViewModel.NickNamePaymentMethodList.Where(m => m.Value == CustomerPaymentID).First();
                    {
                        recurringOrderViewModel.PaymentMethod.NickName = Name.Text;
                    }
                }
            }
            if (CustomerAddressId != null)
            {
                var response = ccp.GetCustomerShippingAddressRun(recurringOrderViewModel.CustomerProfileId, CustomerAddressId);
                if (response != null)
                {
                    recurringOrderViewModel.ShippingAddress = response;
                    recurringOrderViewModel.CustomerAddressId = CustomerAddressId;
                    var Name = recurringOrderViewModel.NickNameShipingAddressList.Where(m => m.Value == CustomerAddressId).First();
                    {
                        recurringOrderViewModel.ShippingAddress.NickName = Name.Text;
                    }
                }
            }
            CreateCustomerProfile mm = new CreateCustomerProfile();
            RecurringPaymentMethod model = new RecurringPaymentMethod();
            // mm.CreateNewCustomerProfile("4Pr3w2FE", "45S8QyZ44R96x5md", "chandan.mandal1@met-technologies.com", model);
            //  mm.GetCustomerProfile("4Pr3w2FE", "45S8QyZ44R96x5md", "900622555");
            return View(recurringOrderViewModel);
        }

        public ActionResult ChangeAutoshipStatus(int AdId, Boolean Status)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            RecurringOrderViewModel recurringOrderModel = new RecurringOrderViewModel();
            recurringOrderModel.AdId = AdId;
            recurringOrderModel.Status = Status;
            _model = _membersService.InsertUpdateAutoship(4, recurringOrderModel);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAutoship", new { AdId = AdId });
        }

        public ActionResult ChangeUseFoxxCash(int AdId, Boolean Status)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            RecurringOrderViewModel recurringOrderModel = new RecurringOrderViewModel();
            recurringOrderModel.AdId = AdId;
            recurringOrderModel.UseFoxxCash = Status;
            recurringOrderModel.Status = Status;
            _model = _membersService.InsertUpdateAutoship(7, recurringOrderModel);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAutoship", new { AdId = AdId });
        }

        [HttpPost]
        public ActionResult AddProductsToAutoship(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            ProductListFromNopCom model1 = new ProductListFromNopCom();
            foreach (var Item in model.ProductListFromNopCom)
            {
                if (Item.IsCheck)
                {
                    model1 = Item;
                    model1.Qty = 1;
                    _model = _membersService.InsertUpdateAutoshipDirectivesItems(1, model.AdId, model1);
                }
            }

            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult EditPaymentMethod(RecurringOrderViewModel model)
        {
            CreateCustomerProfile ccp = new CreateCustomerProfile();
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            string CustomerId = "CustomerId-" + model.AdId.ToString();
            string customerProfileId = model.CustomerProfileId;
            string PaymentProfileId = model.CustomerPaymentID;
            string ShippingAddressId = null;
            if (model.CustomerProfileId != "0")
            {
                var ResponseData = _membersService.InsertUpdateAutoshipPaymentMethod(3, model.AdId, model.PaymentMethod);
                var response = ccp.UpdateCustomerPaymentProfile(customerProfileId, PaymentProfileId, model.PaymentMethod);
                if (response != null)
                {
                    if (response.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response.messages.message != null)
                        {

                        }
                    }
                }
            }

            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult AddAddressDetails(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            CreateCustomerProfile ccp = new CreateCustomerProfile();
            string customerProfileId = model.CustomerProfileId;// Session["customerProfileId"].ToString(); //"900622555";
            string customerAddressId = null;
            string StripeCustomerId = null;
            string StripeAddressId = null;
            var response = ccp.CreateCustomerShippingAddress(customerProfileId, model.ShippingAddress);
            var response1 = _membersService.InsertUpdateAutoshipShipingAddress(2, model.AdId, model.ShippingAddress);
            if (response.customerAddressId != null)
            {
                customerAddressId = response.customerAddressId.ToString();
                _model = _membersService.InsertUpdateAnetPaymentShipping(2, 0, customerProfileId, null, model.ShippingAddress.NickName, false, customerAddressId, false, model.UserId, StripeCustomerId, StripeAddressId);
            }
            //_model = _membersService.InsertUpdateAutoshipPaymentMethod(1, model.AdId, model1);
            //if (_model.RowAffected > 0)
            //{
            //    // AdId = (Int32)_model.InsertedId;
            //}



            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult EditAddressDetails(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            CreateCustomerProfile ccp = new CreateCustomerProfile();
            string customerProfileId = model.CustomerProfileId;// Session["customerProfileId"].ToString(); //"900622555";
            string customerAddressId = model.CustomerAddressId;
            _membersService.InsertUpdateAutoshipShipingAddress(4, model.AdId, model.ShippingAddress);
            var response = ccp.UpdateCustomerShippingAddress(customerProfileId, customerAddressId, model.ShippingAddress);
            if (response != null)
            {

            }
            //_model = _membersService.InsertUpdateAutoshipPaymentMethod(1, model.AdId, model1);
            //if (_model.RowAffected > 0)
            //{
            //    // AdId = (Int32)_model.InsertedId;
            //}



            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        public ActionResult ChangePaymentNickName(string CustomerPaymentID, int AdId)
        {
            Session["CustomerPaymentID"] = CustomerPaymentID;
            return RedirectToAction("NewAutoship", new { AdId = AdId, CustomerPaymentID = CustomerPaymentID });
        }

        public ActionResult ChangeAddressNickName(string CustomerAddressID, int AdId)
        {
            string CustomerPaymentID = null;
            if (Session["CustomerPaymentID"] != null)
            {
                CustomerPaymentID = Session["CustomerPaymentID"].ToString();
            }
            Session["CustomerAddressID"] = CustomerAddressID;
            return RedirectToAction("NewAutoship", new { AdId = AdId, CustomerPaymentID = CustomerPaymentID, CustomerAddressID = CustomerAddressID });
        }

        [HttpPost]
        public ActionResult AddAutoshipFrequency(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            //************************
            string ddlFreqPeriod = model.AutoshipPeriodId.ToString();
            string StartingOn = model.StartingOn;// "20/07/2020";
            int Quantity = 1;
            var FreqInt = model.OnceEvery;// DateTime.Now.Day; 
            AdminEditorViewModel ModelObj = new AdminEditorViewModel();
            // Dim FreqInt As New DateInterval

            DateTime originalDate = Convert.ToDateTime(StartingOn);
            long Qty = Quantity;
            if (ddlFreqPeriod == "2")
            {
                FreqInt = (FreqInt * 7);
            }
            string dt = Convert.ToDateTime(StartingOn).ToString("MM-dd-yyyy");
            var NextDate = dt.Replace("-", "/");
            List<FreqTableList> ListViewFreqChart = new List<FreqTableList>();

            for (int i = 0; i < Quantity; i++)
            {
                FreqTableList Freqmodel = new FreqTableList();
                Freqmodel.ID = i;
                Freqmodel.Ordinal = i + 1;
                Freqmodel.OrderDate = NextDate;
                ListViewFreqChart.Add(Freqmodel);
                if (ddlFreqPeriod == "2" || ddlFreqPeriod == "1")
                {
                    NextDate = originalDate.AddDays(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddDays(Convert.ToInt32(FreqInt));
                }
                else if (ddlFreqPeriod == "3")
                {
                    NextDate = originalDate.AddMonths(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddMonths(Convert.ToInt32(FreqInt));
                }
            }
            ModelObj.FreqTableList = ListViewFreqChart;


            //*******************************************************
            model.NextOrderDate = NextDate;
            _model = _membersService.InsertUpdateAutoship(10, model);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult ChangeProductQty(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            ProductListFromNopCom model1 = new ProductListFromNopCom();
            model1.Qty = model.Qty;
            model1.Id = model.Id;
            _model = _membersService.InsertUpdateAutoshipDirectivesItems(4, model.AdId, model1);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult AddPaymentMethod(RecurringOrderViewModel model)
        {
            CreateCustomerProfile ccp = new CreateCustomerProfile();
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            StripePaymentGateway spg = new StripePaymentGateway();

            createCustomerProfileResponse response = new createCustomerProfileResponse();
            string CustomerId = "CustomerId-" + model.AdId.ToString();
            string customerProfileId = null;
            string PaymentProfileId = null;
            string ShippingAddressId = null;
            string StripeCustomerId = null;
            string StripeAddressId = null;
            string StripePaymentMethodId = null;


            var CustomerResp = spg.CreateCustomer(model.PaymentMethod);
            if (CustomerResp != null)
            {
                StripeCustomerId = CustomerResp.CustomerId;
                StripePaymentMethodId = CustomerResp.PaymentMethodId;
            }
            var ResponseData = _membersService.InsertUpdateAutoshipPaymentMethod(1, model.AdId, model.PaymentMethod);
            //if(ResponseData!=null)
            //{
            //    StripeCustomerId=ResponseData.I
            //}
            //customerProfileId = CustomerResp.Id;
            //PaymentProfileId = response.customerPaymentProfileIdList[0];
            //ShippingAddressId = response.customerShippingAddressIdList[0];
            // Session["customerProfileId"] = customerProfileId;
            // _model = _membersService.InsertUpdateAnetPaymentShipping(3, 0, customerProfileId, PaymentProfileId, model.PaymentMethod.NickName, false, null, false, model.UserId);


            //var val = spg.CreateNewCustomerProfile(CustomerId, model.PaymentMethod);

            if (model.CustomerProfileId == "0")
            {
                response = ccp.CreateNewCustomerProfile(CustomerId, model.PaymentMethod);
                if (response != null)
                {
                    if (response.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response.messages.message != null)
                        {
                            customerProfileId = response.customerProfileId;
                            PaymentProfileId = response.customerPaymentProfileIdList[0];
                            ShippingAddressId = response.customerShippingAddressIdList[0];
                            Session["customerProfileId"] = customerProfileId;

                            _model = _membersService.InsertUpdateAnetPaymentShipping(3, 0, customerProfileId, PaymentProfileId, model.PaymentMethod.NickName, false, null, false, model.UserId, StripeCustomerId, StripeAddressId);
                        }
                    }
                }
            }
            else
            {
                customerProfileId = model.CustomerProfileId;
                var response1 = ccp.CreateCustomerPaymentProfile(customerProfileId, model.PaymentMethod);
                if (response1 != null)
                {
                    if (response1.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response1.messages.message != null)
                        {
                            PaymentProfileId = response1.customerPaymentProfileId;
                            Session["customerProfileId"] = customerProfileId;

                            _model = _membersService.InsertUpdateAnetPaymentShipping(1, 0, customerProfileId, PaymentProfileId, model.PaymentMethod.NickName, false, null, false, model.UserId, StripeCustomerId, StripeAddressId);

                        }
                    }
                }
            }



            //if (_model.RowAffected > 0)
            //{
            //    // AdId = (Int32)_model.InsertedId;
            //}



            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }

        [HttpPost]
        public JsonResult PopulateAutoshipFrequencyTable(string OnceEvery, string AutoshipPeriodId, string StartingOn)
        {
            string ddlFreqPeriod = AutoshipPeriodId.ToString();
            //string StartingOn = StartingOn;// "20/07/2020";
            int Quantity = 10;
            var FreqInt = Convert.ToInt32(OnceEvery);// DateTime.Now.Day; 
            AdminEditorViewModel ModelObj = new AdminEditorViewModel();
            // Dim FreqInt As New DateInterval

            DateTime originalDate = Convert.ToDateTime(StartingOn);
            long Qty = Quantity;
            if (ddlFreqPeriod == "2")
            {
                FreqInt = (FreqInt * 7);
            }
            string dt = Convert.ToDateTime(StartingOn).ToString("MM-dd-yyyy");
            var NextDate = dt.Replace("-", "/");
            List<FreqTableList> ListViewFreqChart = new List<FreqTableList>();

            for (int i = 0; i < Quantity; i++)
            {
                FreqTableList Freqmodel = new FreqTableList();
                Freqmodel.ID = i;
                Freqmodel.Ordinal = i + 1;
                Freqmodel.OrderDate = NextDate;
                ListViewFreqChart.Add(Freqmodel);
                if (ddlFreqPeriod == "2" || ddlFreqPeriod == "1")
                {
                    NextDate = originalDate.AddDays(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddDays(Convert.ToInt32(FreqInt));
                }
                else if (ddlFreqPeriod == "3")
                {
                    NextDate = originalDate.AddMonths(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddMonths(Convert.ToInt32(FreqInt));
                }
            }
            ModelObj.FreqTableList = ListViewFreqChart;


            //*******************************************************
            return Json(ListViewFreqChart);
            // return Json(new { data = ListViewFreqChart }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeNickName(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            _model = _membersService.InsertUpdateAutoship(5, model);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAutoship", new { AdId = model.AdId });
        }
        public ActionResult RemoveProductItem(int Id, int AdId)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            ProductListFromNopCom model1 = new ProductListFromNopCom();
            model1.Id = Id;
            _model = _membersService.InsertUpdateAutoshipDirectivesItems(3, 0, model1);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAutoship", new { AdId = AdId });
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                model.ModifiedBy = (int)UserId;
                var result = _adminService.ChangePassword(model);
                status = true;
                return Json(new { status, result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SMS()
        {
            return View();
        }
        public ActionResult SendSMS(SendSmsModel model)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                List<string> list = new List<string>();
                list.Add(model.Phone);
                //list.Add("9817099899");
                //list.Add("981709989h");
                //list.Add("+918219455915");
                SendSMSToUsers(list, model.Content);
                status = true;
                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status }, JsonRequestBehavior.AllowGet);
        }
        private string GetPlivoDefaultNumber(PhoneVerificationViewModel model)
        {
            string number = string.Empty;
            var data = _userService.SetTempOptInCode(4, model);
            if (data != null)
            {
                number = Convert.ToString(data.InsertedId);
                if (!string.IsNullOrEmpty(number))
                {
                    IncrementPlivoDefaultNumber(number);
                }

            }
            return number;
        }
        private void IncrementPlivoDefaultNumber(string PlivoDefaultNumber)
        {
            PhoneVerificationViewModel model = new PhoneVerificationViewModel();
            model.PlivoDefaultNumber = PlivoDefaultNumber;
            var data = _userService.SetTempOptInCode(5, model);
        }
        private void SendSMSToUsers(List<string> ToNumberList, string SmsText, string PlivoNumber = null,bool? IsBroadCast=null,int? maxStatus=null)
        {
           
            var BaseUrl = ConfigurationManager.AppSettings["WebSitePath"];
            var callBack = BaseUrl + "/Home/DeliveryReport";
            PhoneVerificationViewModel m = new PhoneVerificationViewModel();
            var FromNumber = PlivoNumber == null ? GetPlivoDefaultNumber(m) : PlivoNumber;
            string AuthId = Convert.ToString(ConfigurationManager.AppSettings["PlivoKeyID"]);
            string AuthToken = Convert.ToString(ConfigurationManager.AppSettings["PlivoKeyValue"]);
            PlivoApi api = new PlivoApi(AuthId, AuthToken);
            try
            {
               
                string UUID = string.Empty;
                Plivo.Resource.Message.MessageCreateResponse response = api.Message.Create(
              src: FromNumber,
               dst: ToNumberList,
              text: SmsText,
              url: callBack
              );
                if (response != null && response.MessageUuid != null && response.MessageUuid.Count > 0)
                {
                    PlivoResponseModel r = new PlivoResponseModel();
                    r.ApiId = response.ApiId;
                    r.Message = response.Message;
                    r.StatusCode = Convert.ToString(response.StatusCode);
                    r.Username = response.Username;
                    r.SmsContent = SmsText;
                    r.TotalCount =Convert.ToString( ToNumberList.Count());
                    r.IsBroadcast = IsBroadCast;
                    r.MaxStatus = maxStatus;
                    var dt = new DataTable();
                    dt.Columns.Add("ToNumber", typeof(string));
                    dt.Columns.Add("FromNumber", typeof(string));
                    dt.Columns.Add("SmsText", typeof(string));
                    dt.Columns.Add("UserID", typeof(string));
                    var invalidDt = new DataTable();
                    invalidDt.Columns.Add("ToNumber", typeof(string));
                    invalidDt.Columns.Add("FromNumber", typeof(string));
                    invalidDt.Columns.Add("SmsText", typeof(string));
                    invalidDt.Columns.Add("UserID", typeof(string));
                    if (response.MessageUuid != null && response.MessageUuid.Count > 0)
                    {
                        foreach (var item in response.MessageUuid)
                        {
                            DataRow row = dt.NewRow();
                            row["ToNumber"] = "moremito";
                            row["FromNumber"] = "moremito";
                            row["SmsText"] =Convert.ToString( item);
                            row["UserID"] =0;
                            dt.Rows.Add(row);
                        }
                        r.MessageUuid = string.Join(",", response.MessageUuid);
                    }
                    if (response.invalid_number != null && response.invalid_number.Count > 0)
                    {
                        foreach (var item in response.invalid_number)
                        {
                            DataRow row = invalidDt.NewRow();
                            row["ToNumber"] = "moremito";
                            row["FromNumber"] = "moremito";
                            row["SmsText"] = Convert.ToString(item);
                            row["UserID"] = 0;
                            invalidDt.Rows.Add(row);
                        }
                        r.InvalidNumber = string.Join(",", response.invalid_number);
                    }
                    r.InputJson = string.Join(",", ToNumberList);
                    _membersService.SavePlivoSMSResponse(r,dt,invalidDt);

                }
            }
            catch (Exception ex)
            {
                _membersService.LogException(0, ex);
            }


        }

        [HttpPost]
        public ActionResult GetSmsBroadCastList(PlivoBroadCastInputModel model)
        {
            BroadCastResponseViewModel response = new Models.BroadCastResponseViewModel();
            bool status = false;
            string msg = string.Empty;
            try
            {
                var m = _membersService.GetUsersForSMSBroadcasting(model);
                if (m != null && m.userList != null)
                {
                    response.Count = m.userList.Count();
                }
                var content = ConvertPartialViewToString("_broadcastUserList", m);
                response.Status = true;
                response.data = content;
                return new JsonResult { Data = response, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SmsBroadcast(PlivoBroadCastInputModel model)
        {
            BroadCastResponseViewModel response = new Models.BroadCastResponseViewModel();
            bool status = false;
            string msg = string.Empty;
            try
            {
                //var m = _membersService.GetUsersForSMSBroadcasting(model);
                if (model != null && model.phoneList != null)
                {
                    var plivoNumbers = _membersService.GetAllPlivoNumbers();
                    response.Count = model.phoneList.Count();
                    var MaxStatus = _membersService.MaxBroadCastNo();
                    List<List<BroadCastUsersModel>> chunkList = model.phoneList.ChunkBy<BroadCastUsersModel>(250);
                    if (chunkList != null && chunkList.Count > 0 && plivoNumbers.Count > 0)
                    {
                        string PlivoNumber = plivoNumbers[0];
                        int i = 0;
                        foreach (var item in chunkList)
                        {
                            if (plivoNumbers.Count > i)
                            {
                                PlivoNumber = plivoNumbers[i];
                            }
                            i++;
                            List<string> PhoneList = item.Select(x => x.phone).ToList();
                           SendSMSToUsers(PhoneList, model.Content, PlivoNumber, true, MaxStatus);
                        }
                    }
                    status = true;
                }

                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
                _membersService.LogException(0, ex);
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSmsApiId()
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var list = _membersService.GetSmsApiI();
                status = true;
                return Json(new { status, list }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSmsReport(int id)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var report = _membersService.GetSmsReport(id);
                status = true;
                return Json(new { status, report }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOptReport(int id)
        {
            BroadCastResponseViewModel response = new Models.BroadCastResponseViewModel();
            bool status = false;
            string msg = string.Empty;
            try
            {
                string content = string.Empty;
                //_plivoReceiveSMS
                var report = _membersService.GetOptSmsReport(id);
                if (id == 1)
                {
                    content = ConvertPartialViewToString("_optInOutReport", report);
                }
                else if(id==2)
                {
                    content = ConvertPartialViewToString("_plivoReceiveSMS", report);
                }
                 
                response.Status = true;
                response.data = content;
                return new JsonResult { Data = response, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptOutInUser(string Phone,string OptType, string Text)
        {
            bool status = false;
            string msg = string.Empty;
            int statusCode = 0;
            try
            {
                PlivoOptInOutModel m = new PlivoOptInOutModel();
                m.From = Phone;
                m.To = "moremito";
                m.MessageUUid = "moremito" ;
                m.MessageIntent = OptType;
                m.SmsText = Text;
                m.OptType = "MOREMITO";

               

                var res = _membersService.IsOptInOut(Phone, OptType);
                if (res == true)
                {
                    statusCode = 500;
                }
                else
                {
                    _membersService.SavePlivoOptInOut(m);
                    statusCode = 200;
                }
                status = true;
                return Json(new { status, statusCode }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUsersFromPhone(string Phone)
        {
            bool status = false;
            string msg = string.Empty;
            
            try
            {

                var content = _membersService.GetUsersFromPhone(Phone);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}
