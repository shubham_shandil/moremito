﻿using Foxxlegacy.Services.Rankup;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Controllers
{
    public class RankupController :Controller
    {
        #region Fields
        private readonly IRankupService _rankupService;


        #endregion

        #region CTor
        public RankupController
            (
            IRankupService rankupService

            )
        {
            this._rankupService = rankupService;
        }

        #endregion

        public string RankAdvanceOrderOwner(int UserID, int OrderID, int isSponsorship)
        {
            string Response = null;
            String RankCheckStatusID = "0", SendingLevel = "0";
            int CheckForLevel = 0;
            int CompareCountLegs = 0, PerLegCappedCustomerCount = 0, CappedCustomerCount = 0, LegsWithRank3Rank4 = 0,
                      CheckRankLegTotal = 0, NoOfRankingLeg = 0;
            RanksDataViewModel RanksData = new RanksDataViewModel();
            int RankId = _rankupService.GetRankByUser(1, UserID);
            if (RankId == 12)
            {
                return Response;
            }
            if (UserID == 1)
            {
                return Response;
            }
            if (RankId == 1)
            {
                insertUsersQualifyingOrders(UserID, OrderID, RankId);
            }
            CheckForLevel = RankId;
            CheckForLevel = CheckForLevel + 1;
            SendingLevel = CheckForLevel.ToString();
            if (CheckForLevel == 4)
            {
                SendingLevel = "4A";
            }
            if (SendingLevel == "1")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "2" || RankCheckStatusID == "2")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "3" || RankCheckStatusID == "3")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "4a" || RankCheckStatusID == "4")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    SendingLevel = "4b";
                    RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                    if (RankCheckStatusID == "0")
                    {
                        return Response;
                    }

                }
            }
            if (SendingLevel == "5" || RankCheckStatusID == "5")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "6" || RankCheckStatusID == "6")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }

            if (SendingLevel == "7" || RankCheckStatusID == "7")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "8" || RankCheckStatusID == "8")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "9" || RankCheckStatusID == "9")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "10" || RankCheckStatusID == "10")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "11" || RankCheckStatusID == "11")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }

            if (isSponsorship == 0)
            {
                AssignGold_On_OrderOwner(OrderID);
            }



            return Response;
        }

        public long insertUsersQualifyingOrders(int UserID, int OrderID, int RankID)
        {
            int response = 0;
            response = _rankupService.InsertUsersQualifyingOrders(2, UserID, OrderID, RankID);

            return response;
        }

        public string RankCheckPeople(int UserId, int OrderID, string CurrLevel)
        {
            int ActiveLegCount = 0, RankID = 0, CompareCountLegs = 0;
            string response = "0";
            RanksDataViewModel RanksData = new RanksDataViewModel();
            if (CurrLevel == "1" || CurrLevel == "2" || CurrLevel == "3" || CurrLevel == "4a")
            {
                ActiveLegCount = _rankupService.InsertUsersQualifyingOrders(3, UserId, OrderID, RankID);
            }
            else if (CurrLevel == "4b")
            {
                ActiveLegCount = _rankupService.InsertUsersQualifyingOrders(4, UserId, OrderID, RankID);
            }

            if (CurrLevel == "1")
            {
                RanksData = _rankupService.GetRanksData(5, 1);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount > CompareCountLegs)
                {
                    ActiveLegCount = Convert.ToInt32(CurrLevel) + 1;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "2")
            {
                RanksData = _rankupService.GetRanksData(5, 2);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount > CompareCountLegs)
                {
                    ActiveLegCount = Convert.ToInt32(CurrLevel) + 1;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "3")
            {
                RanksData = _rankupService.GetRanksData(5, 3);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount > CompareCountLegs)
                {
                    ActiveLegCount = Convert.ToInt32(CurrLevel) + 1;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "4a")
            {
                //CompareCountLegs = _rankupService.GetRanksData(5, 2);
                if (ActiveLegCount > 11)
                {
                    ActiveLegCount = 4;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "4b")
            {
                RanksData = _rankupService.GetRanksData(5, 4);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount >= CompareCountLegs)
                {
                    ActiveLegCount = 4;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }

            DateTime QTE_ExpDate = System.DateTime.Now,
         QTE_FailDate = System.DateTime.Now,
         FailureMessagedDate = System.DateTime.Now,
         QTE_DemotionDate = System.DateTime.Now,
         SetQualTillDate = System.DateTime.Now;

            if (ActiveLegCount == 0)
            {
                _rankupService.InsertUsersQTERank(1, UserId, Convert.ToInt32(CurrLevel), QTE_ExpDate, QTE_FailDate, FailureMessagedDate, QTE_DemotionDate);
            }
            else
            {
                if (CurrLevel == "4a" || ActiveLegCount == 4)
                {
                    CurrLevel = "5";
                }

                else if (CurrLevel == "4b" || ActiveLegCount == 4)
                {
                    CurrLevel = "5";
                }

                insertUsersQualifyingOrders(UserId, OrderID, Convert.ToInt32(CurrLevel));
            }


            return response;
        }

        public int CheckStep_Five_To_Eleven(int UserID, int OrderID, int PerLegCappedCustomerCount, int CappedCustomerCount, int LegsWithRank3Rank4, int CheckRankLegTotal, int NoOfRankingLeg, int SendingLevel)
        {
            bool IsClearRank = false;
            var model = new CheckStepFiveToElevenRequestViewModel
            {
                UserId = UserID,
                PerLegCappedCustomerCount = PerLegCappedCustomerCount,
                LegsWithRank3Rank4 = LegsWithRank3Rank4,
                CheckRankLegTotal = CheckRankLegTotal
            };
            var CountData = _rankupService.CheckStepFiveToEleven(1, model);

            if (CountData.Count > 0)
            {
                foreach (var userCount in CountData)
                {
                    if (userCount.CappedCustomerCount >= CappedCustomerCount && userCount.UsersInLegCount >= NoOfRankingLeg)
                    {
                        IsClearRank = true;
                    }
                }
            }

            if (IsClearRank)
            {
                int insertedId = _rankupService.InsertUsersQualifyingOrders(2, UserID, OrderID, SendingLevel);
                return SendingLevel + 1;
            }
            else
            {
                return 0;
            }
        }

        public void AssignGold_On_OrderOwner(int OrderId)
        {
            if (OrderId > 0)
            {
                var OrderRequestModel = new OrderUserIdRequestViewModel { OrderId = OrderId };
                int UserId = _rankupService.GetOrderUserId(OrderRequestModel);

                if (UserId > 0)
                {
                    int UnilevelSponsorIDFirst = SetGoldOne_On_UsersID(UserId, 6);
                    if (UnilevelSponsorIDFirst > 0)
                    {
                        Update_User_Codes(UserId, "Gold1", UnilevelSponsorIDFirst);
                        if (UserId > 0)
                        {
                            int UnilevelSponsorID2A = SetGoldOne_On_UsersID(UserId, 7);
                            if (UnilevelSponsorID2A > 0)
                            {
                                Update_User_Codes(UserId, "Gold2A", UnilevelSponsorID2A);
                                if (UnilevelSponsorID2A > 0)
                                {
                                    int UnilevelSponsorID2B = SetGoldOne_On_UsersID(UnilevelSponsorID2A, 7);
                                    Update_User_Codes(UserId, "Gold2B", UnilevelSponsorID2B);
                                }
                            }
                        }
                    }
                }


            }
        }

        public int SetGoldOne_On_UsersID(int UserId, int CheckRankID)
        {
            int ReturnUniLevelSponsorID = 0;
            try
            {
                var model = new GoldRankupSponsorIdReuestViewModel { UserId = UserId, CheckRankId = CheckRankID };
                ReturnUniLevelSponsorID = _rankupService.GetGoldRankupSponsorId(model);
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return ReturnUniLevelSponsorID;
        }

        public void Update_User_Codes(int UsersID, string GoldLevel, int UnilevelSponsorID)
        {
            var model = new UpdateGoldRankUserReuestViewModel { UserId = UsersID, GoldLevel = GoldLevel, SponsorID = UnilevelSponsorID };
            _rankupService.UpdateGoldUserRankup(model);
        }
    }
}