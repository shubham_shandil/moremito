﻿using Foxxlegacy.Services.Admin;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Helpers;
using Foxxlegacy.Web.Models;
using Foxxlegacy.Web.Services;
using Newtonsoft.Json;
using Plivo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Controllers
{
    public class JoinController : BaseController
    {

        #region Fields
        private readonly IMembersService _membersService;
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;
        private readonly IAdminService _adminService;

        #endregion

        #region CTor
        public JoinController
            (
            IMembersService memberService, ICommonService commonService, IUserService userService, IAdminService adminService

            )
        {
            this._membersService = memberService;
            this._commonService = commonService;
            this._userService = userService;
            this._adminService = adminService;

        }
        #endregion

        string pathValue = ConfigurationManager.AppSettings["nopLogUrl"];
        
        // GET: Join

        public ActionResult Join(string SignupType)
        {
            UserViewModel model = new UserViewModel();
            HelpViewModel hmodel = new HelpViewModel();
            if (SignupType == null)
            {
                // SignupType = "0";

                if (TempData.ContainsKey("SignupType"))
                {
                    SignupType = TempData["SignupType"].ToString();
                }
            }
            TempData["MESSAGE"] = SignupType;
            TempData["SignupType"] = SignupType;
            List<CountryStateModel> countries = _commonService.GetCountriesAndStates(1, 0);
            if (countries != null)
            {
                var countryList = countries.Select(x => new SelectListItem
                {
                    Text=x.Name,
                    Value=x.Id.ToString()
                }).ToList();

                model.StateList = countryList.AsEnumerable();
             
            }
            List<SelectListItem> EmptyList = new List<SelectListItem>();
            model.StateNewList = EmptyList.AsEnumerable();
            //   model.StateList = _commonService.GetAllDropdownlist(7);
            model.SignupType = Convert.ToInt32(SignupType);
            var userDetails = (UserDetailsViewModel)Session["userDetails"];
            if (userDetails == null)
            {
                return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Join/Join" });

            }
            else
            {
                TempData["SPONSER_NAME"] = userDetails.Name;
            }

            model.FullName = userDetails.Name;
            switch (SignupType)
            {
                case "2":
                    model.Content = _adminService.GetWebContentById(1, "Join_ReferringCustomer", 0) != null ? _adminService.GetWebContentById(1, "Join_ReferringCustomer", 0).HelpContent : "";
                    model.SignUpBelowURL = _adminService.GetWebContentById(1, "Join_ReferringCustomer_BelowURL", 0) != null ? _adminService.GetWebContentById(1, "Join_ReferringCustomer_BelowURL", 0).HelpContent : "";
                    model.cbAgree = _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_RC", 0) != null ? _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_RC", 0).HelpContent : "";
                    break;
                case "3":

                    model.Content = _adminService.GetWebContentById(1, "Join_FriendOfFoxx", 0) != null ? _adminService.GetWebContentById(1, "Join_FriendOfFoxx", 0).HelpContent : "";
                    model.SignUpBelowURL = _adminService.GetWebContentById(1, "Join_FriendOfFoxx_BelowURL", 0) != null ? _adminService.GetWebContentById(1, "Join_FriendOfFoxx_BelowURL", 0).HelpContent : "";
                    model.cbAgree = _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_FOF", 0) != null ? _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_FOF", 0).HelpContent : "";
                    break;
                default:
                    break;

            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Join(UserViewModel model)
        {
            try
            {



                //  model.StateList = _commonService.GetAllDropdownlist(7);
                List<CountryStateModel> countries = _commonService.GetCountriesAndStates(1, 0);
                if (countries != null)
                {
                    var countryList = countries.Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }).ToList();

                    model.StateList = countryList.AsEnumerable();

                }

                List<CountryStateModel> states = _commonService.GetCountriesAndStates(2, model.CountryId);

                if (states != null)
                {
                    var statesList = states.Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Id.ToString()
                    }).ToList();

                    model.StateNewList = statesList.AsEnumerable();

                }
                if (ModelState.IsValid)
                {

                    model.GuiId = Guid.NewGuid();
                    string userName = Session["SponsorName"] != null ? Session["SponsorName"].ToString() : "";
                    var userId = _commonService.GetUsersIDbyUserName(9, userName.Trim());
                    model.UserId = userId;
                    model.SponsorID = Convert.ToInt32(userId);

                    if (userId == 0)
                    {
                        TempData["ERROR_MESSAGE"] = "Unable to find sponsor user.Try again by providing your sponsor website details.";
                        return View(model);
                    }
                    var isDuplicateUserName = _commonService.CheckDuplicateUserName(model.UserName);
                    if (isDuplicateUserName == true)
                    {
                        TempData["UserExits"] = "UserName Already Exists!";
                        return View(model);
                    }
                    var isDuplicateEmail = _commonService.CheckDuplicateEmail(model.Email);
                    if (isDuplicateEmail == true)
                    {
                        TempData["EmailExits"] = "Email address already exists!";
                        return View(model);
                    }





                    switch (model.SignupType)
                    {
                        case 0: //fullmember
                            model.IsCustomerAUser = 1;
                            model.SponsorID = Convert.ToInt32(_commonService.GetUsersIDbyUserName(9, userName.Trim()));// 'Since user is a member, his CustomerSponsorID is himself
                            break;
                        case 3: //'FriendOfFoxx
                            model.IsCustomerAUser = 0;
                            break;
                        case 2:// 'ReferringCustomer
                            model.IsCustomerAUser = 1;
                            model.SponsorID = Convert.ToInt32(_commonService.GetUsersIDbyUserName(9, userName.Trim()));// 'Since user is a member, his CustomerSponsorID is himself
                            break;
                        default:
                            break;
                    }


                    SqlResponseBaseModel response = new SqlResponseBaseModel();
                    if (model.CheckboxTermCondition == true)
                    {
                        response = _userService.InsertUpdateUsersSignUpData(1, model);
                        if (response.InsertedId > 0)
                        {
                            //AddDownlineCounts(model.UserId);
                            AddDownlineCounts(response.InsertedId ?? 0); // pass userid of currently registered user instead of his sponsor user
                            try
                            {
                                SetSMSCode(response.InsertedId ?? 0);
                                await SendEmails(Convert.ToInt32(response.InsertedId ?? 0), model.Password);
                            }
                            catch (Exception ex)
                            {

                            }

                           // TempData["SUCCESS_MESSAGE"] = "User joined successfully";
                            TempData["UserId"] = response.InsertedId;
                            TempData["personalusername"] = model.UserName;
                            TempData["PhoneVerificationData"] = model;


                        }
                        else
                        {
                            TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                        }
                        if (response.ErrorCode == 2601)
                        {
                            TempData["UserExits"] = "UserName Already Exists!";
                            TempData["MESSAGE"] = model.SignupType;
                            var userDetails = (UserDetailsViewModel)Session["userDetails"];
                            if (userDetails != null)
                            {
                                model.FullName = userDetails.Name;
                            }


                            switch (model.SignupType.ToString())
                            {
                                case "2":
                                    model.Content = _adminService.GetWebContentById(1, "Join_ReferringCustomer", 0) != null ? _adminService.GetWebContentById(1, "Join_ReferringCustomer", 0).HelpContent : "";
                                    model.SignUpBelowURL = _adminService.GetWebContentById(1, "Join_ReferringCustomer_BelowURL", 0) != null ? _adminService.GetWebContentById(1, "Join_ReferringCustomer_BelowURL", 0).HelpContent : "";
                                    model.cbAgree = _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_RC", 0) != null ? _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_RC", 0).HelpContent : "";

                                    break;
                                case "3":

                                    model.Content = _adminService.GetWebContentById(1, "Join_FriendOfFoxx", 0) != null ? _adminService.GetWebContentById(1, "Join_FriendOfFoxx", 0).HelpContent : "";
                                    model.SignUpBelowURL = _adminService.GetWebContentById(1, "Join_FriendOfFoxx_BelowURL", 0) != null ? _adminService.GetWebContentById(1, "Join_FriendOfFoxx_BelowURL", 0).HelpContent : "";
                                    model.cbAgree = _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_FOF", 0) != null ? _adminService.GetWebContentById(1, "Join_Agree_PnP_TOS_FOF", 0).HelpContent : "";
                                    break;
                                default:
                                    break;

                            }

                            return View(model);
                        }
                        else
                        {
                            return RedirectToAction("PhoneVerification", "Join");
                        }

                    }
                }


                return RedirectToAction("Join", new
                {
                    SignupType = model.SignupType
                });
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] ="Error occurred : "+ ex.Message;
            }
            return View(model);
        }
        #region Set Sms Code
        private void SetSMSCode(long userId)
        {
            var smsCode = _userService.SetSmsCode(1, userId, string.Empty, string.Empty);
            if (smsCode != null)
            {
                var firstLetter = smsCode.SmsCode;
                var secondLetter = smsCode.SmsCode1;

                string nextSmsCode = GetNextSmsCode(firstLetter, secondLetter);
                if (!string.IsNullOrEmpty(nextSmsCode))
                {
                    var up = _userService.SetSmsCode(5, userId, string.Empty, nextSmsCode);
                }
            }
        }
        private string GetNextSmsCode(string Letter1, string Letter2)
        {
            string GetNextSmsCode = string.Empty;
            bool FoundAFreeCode = false;
            int TheCount = 1;
            while (FoundAFreeCode == false)
            {
                if (IsUserNameAvailable(Letter1 + Letter2 + TheCount.ToString()) == true)
                {
                    GetNextSmsCode = (Letter1 + Letter2 + TheCount.ToString()).ToUpper();
                    FoundAFreeCode = true;
                }
                else
                {
                    TheCount += 1;
                }
            }
            return GetNextSmsCode;
        }
        private bool IsUserNameAvailable(string UserName)
        {
            bool IsUserNameAvailable = false;

            //var data = _userService.SetSmsCode(4, 0, string.Empty, UserName);
            var data = _userService.SetSmsCode(4, 0, UserName, UserName);
            if (data != null)
            {
                if (data.ValCount == 0)
                {
                    if (IsUserNameABadWord(UserName) == false)
                    {
                        if (isUserNameAnSmsCode(UserName) == false)
                        {
                            IsUserNameAvailable = true;
                        }
                    }
                }
            }

            return IsUserNameAvailable;
        }
        private bool IsUserNameABadWord(string UserName)
        {
            bool IsUserNameABadWord = true;
            var data = _userService.SetSmsCode(3, 0, string.Empty, UserName);
            if (data != null)
            {
                if (data.ValCount == 0)
                {
                    IsUserNameABadWord = false;
                }
            }
            return IsUserNameABadWord;
        }
        private bool isUserNameAnSmsCode(string smsCode)
        {
            bool isUserNameAnSmsCode = true;
            var data = _userService.SetSmsCode(2, 0, string.Empty, smsCode);
            if (data != null)
            {
                if (data.ValCount == 0)
                {
                    isUserNameAnSmsCode = false;
                }
            }
            return isUserNameAnSmsCode;
        }

        #endregion


        #region Send Emails
        private async Task<int> SendEmails(int Userid, string password)
        {
            await QMailToMember_Welcome(Userid, Userid, 3, password);//send the new guy email

            //sponsor and 9 upline sponsors
            await GetSponsorsUsersID(Userid);
            return 1;
        }
        private async Task<int> SendEmailsByKey(int Userid, string password,string TaskKey)
        {
            await QMailToMember_Welcome(Userid, Userid, 3, password,true,TaskKey);//send the new guy email

            //sponsor and 9 upline sponsors
            await GetSponsorsUsersID(Userid);
            return 1;
        }
        private async Task<int> QMailToMember_Welcome(int RecipientID, int TriggerID, int EmailID, string strPassword,bool GetEmailByKey=false,string TaskKey=null)
        {
            var IsUserOptOutVal = IsUserOptOut(RecipientID, 0);
            if (IsUserOptOutVal == false)
            {
                string TheSubject = string.Empty;
                string TheBody = string.Empty;
                if (GetEmailByKey == true)
                {
                    SetEmailSubjectAndBodyByTaskKey(TaskKey, ref TheSubject, ref TheBody);
                }
                else
                {
                    SetEmailSubjectAndBody(EmailID, ref TheSubject, ref TheBody);
                }
               
                if (string.IsNullOrEmpty(TheSubject) || string.IsNullOrEmpty(TheBody))
                {
                    return 0;
                }
                else
                {
                    string R_U = string.Empty;
                    string R_SU = string.Empty;
                    string R_F = string.Empty;
                    string R_L = string.Empty;
                    string R_P = string.Empty;
                    string R_E = string.Empty;
                    string R_RANK = string.Empty;
                    string R_OLDRANK = string.Empty;
                    var data = _userService.SetUSUFLEROr(RecipientID);
                    GetStringValues(ref R_U, ref R_SU, ref R_F, ref R_L, ref R_P, ref R_E, ref R_RANK, ref R_OLDRANK, data);

                    string LabelValue = "%Name:" + R_F + " " + R_L + "%Phone:" + R_P + "%Email:" + R_E + "%";
                    if (!string.IsNullOrEmpty(LabelValue))
                    {
                        var byt = System.Text.Encoding.UTF8.GetBytes(LabelValue);
                        LabelValue = Convert.ToBase64String(byt);
                    }
                    if (string.IsNullOrEmpty(R_E))
                    {
                        return 0;
                    }

                    string TG_U = string.Empty;
                    string TG_SU = string.Empty;
                    string TG_F = string.Empty;
                    string TG_L = string.Empty;
                    string TG_P = string.Empty;
                    string TG_E = string.Empty;
                    string TG_RANK = string.Empty;
                    string TG_OLDRANK = string.Empty;

                    var trigger = _userService.SetUSUFLEROr(TriggerID);
                    GetStringValues(ref TG_U, ref TG_SU, ref TG_F, ref TG_L, ref TG_P, ref TG_E, ref TG_RANK, ref TG_OLDRANK, trigger);

                    TheSubject = TheSubject.Replace("@@R_U@@", R_U);
                    TheSubject = TheSubject.Replace("@@URL@@", R_U + "&Label=" + LabelValue);
                    TheSubject = TheSubject.Replace("@@R_SU@@", R_SU);
                    TheSubject = TheSubject.Replace("@@R_F@@", R_F);
                    TheSubject = TheSubject.Replace("@@R_L@@", R_L);
                    TheSubject = TheSubject.Replace("@@R_E@@", R_E);
                    TheSubject = TheSubject.Replace("@@R_RANK@@", R_RANK);
                    TheSubject = TheSubject.Replace("@@R_OLDRANK@@", R_OLDRANK);
                    TheSubject = TheSubject.Replace("@@C_NAME@@", R_F + " " + R_L);

                    TheSubject = TheSubject.Replace("@@TG_U@@", TG_U);
                    TheSubject = TheSubject.Replace("@@TG_SU@@", TG_SU);
                    TheSubject = TheSubject.Replace("@@TG_F@@", TG_F);
                    TheSubject = TheSubject.Replace("@@TG_L@@", TG_L);
                    TheSubject = TheSubject.Replace("@@TG_E@@", TG_E);
                    TheSubject = TheSubject.Replace("@@TG_RANK@@", TG_RANK);
                    TheSubject = TheSubject.Replace("@@TG_OLDRANK@@", TG_OLDRANK);

                    TheBody = TheBody.Replace("@@R_U@@", R_U);
                    TheBody = TheBody.Replace("@@R_Password@@", strPassword);
                    TheBody = TheBody.Replace("@@URL@@", R_U + "&Label=" + LabelValue);
                    TheBody = TheBody.Replace("@@R_SU@@", R_SU);
                    TheBody = TheBody.Replace("@@R_F@@", R_F);
                    TheBody = TheBody.Replace("@@R_L@@", R_L);
                    TheBody = TheBody.Replace("@@R_E@@", R_E);
                    TheBody = TheBody.Replace("@@R_RANK@@", R_RANK);
                    TheBody = TheBody.Replace("@@R_OLDRANK@@", R_OLDRANK);

                    TheBody = TheBody.Replace("@@TG_U@@", TG_U);
                    TheBody = TheBody.Replace("@@TG_SU@@", TG_SU);
                    TheBody = TheBody.Replace("@@TG_F@@", TG_F);
                    TheBody = TheBody.Replace("@@TG_L@@", TG_L);
                    TheBody = TheBody.Replace("@@TG_E@@", TG_E);
                    TheBody = TheBody.Replace("@@TG_RANK@@", TG_RANK);
                    TheBody = TheBody.Replace("@@TG_OLDRANK@@", TG_OLDRANK);
                    TheBody = TheBody.Replace("@@C_NAME@@", R_F + " " + R_L);

                    var adminEmail = Convert.ToString(ConfigurationManager.AppSettings["AdminFromEmail"]);
                    var QueId = _userService.InsertToEmailQue(adminEmail, R_E, TheSubject, TheBody,true);
                    EmailSendViewModel m = new EmailSendViewModel();
                    m.To = new List<string>();
                    m.EmailBody = TheBody;
                    m.To.Add(R_E);
                    m.Subject = TheSubject;
                    m.From = adminEmail;
                    FoxxEmailService foxx = new FoxxEmailService();
                    await foxx.SendEmail(m);
                }
            }
            return 1;
        }
        private async Task<int> SendGuestUserMailToAdmin(GuestJoinModel model)
        {
            string TheSubject = string.Empty;
            string TheBody = string.Empty;
            SetEmailSubjectAndBodyByTaskKey("GuestUser", ref TheSubject, ref TheBody);
            if (string.IsNullOrEmpty(TheSubject) || string.IsNullOrEmpty(TheBody))
            {
                return 0;
            }

            TheBody = TheBody.Replace("@@R_Name@@", model.FirstName+" "+model.LastName);
            TheBody = TheBody.Replace("@@R_Email@@", model.Email);
            TheBody = TheBody.Replace("@@R_Phone@@", model.Phone);
            TheBody = TheBody.Replace("@@R_U@@", model.Username);
            TheBody = TheBody.Replace("@@R_address@@", model.Address);
            TheBody = TheBody.Replace("@@R_city@@", model.City);
            TheBody = TheBody.Replace("@@R_zip@@", model.Zip);
            TheBody = TheBody.Replace("@@R_country@@", model.CountryStateName);
            TheBody = TheBody.Replace("@@R_sponsor@@", model.SponsorName);

           
            var adminEmail = Convert.ToString(ConfigurationManager.AppSettings["AdminFromEmail"]);
            var SupportEmailId =   Convert.ToString(ConfigurationManager.AppSettings["SupportEmailId"]);
            var QueId = _userService.InsertToEmailQue(adminEmail, SupportEmailId, TheSubject, TheBody, true);
            EmailSendViewModel m = new EmailSendViewModel();
            m.To = new List<string>();
            m.EmailBody = TheBody;
            m.To.Add(SupportEmailId);
            m.Subject = TheSubject;
            m.From = adminEmail;
            FoxxEmailService foxx = new FoxxEmailService();
            await foxx.SendEmail(m);
            return 1;
        }
        private async Task<int> QMailToMember(int RecipientID, int TriggerID, int EmailID,string TaskName=null)
        {
            var IsUserOptOutVal = IsUserOptOut(RecipientID, 0);
            if (IsUserOptOutVal == false)
            {
                string TheSubject = string.Empty;
                string TheBody = string.Empty;
                if(EmailID==0 && !string.IsNullOrEmpty(TaskName))
                {
                    SetEmailSubjectAndBodyByTaskKey(TaskName, ref TheSubject, ref TheBody);
                }
                else
                {
                    SetEmailSubjectAndBody(EmailID, ref TheSubject, ref TheBody);
                }
              
                if (string.IsNullOrEmpty(TheSubject) || string.IsNullOrEmpty(TheBody))
                {
                    return 0;
                }
                else
                {
                    string R_U = string.Empty;
                    string R_SU = string.Empty;
                    string R_F = string.Empty;
                    string R_L = string.Empty;
                    string R_P = string.Empty;
                    string R_E = string.Empty;
                    string R_RANK = string.Empty;
                    string R_OLDRANK = string.Empty;
                    var data = _userService.SetUSUFLEROr(RecipientID);
                    GetStringValues(ref R_U, ref R_SU, ref R_F, ref R_L, ref R_P, ref R_E, ref R_RANK, ref R_OLDRANK, data);

                    string LabelValue = "%Name:" + R_F + " " + R_L + "%Phone:" + R_P + "%Email:" + R_E + "%";
                    if (!string.IsNullOrEmpty(LabelValue))
                    {
                        var byt = System.Text.Encoding.UTF8.GetBytes(LabelValue);
                        LabelValue = Convert.ToBase64String(byt);
                    }
                    if (string.IsNullOrEmpty(R_E))
                    {
                        return 0;
                    }

                    string TG_U = string.Empty;
                    string TG_SU = string.Empty;
                    string TG_F = string.Empty;
                    string TG_L = string.Empty;
                    string TG_P = string.Empty;
                    string TG_E = string.Empty;
                    string TG_RANK = string.Empty;
                    string TG_OLDRANK = string.Empty;

                    var trigger = _userService.SetUSUFLEROr(TriggerID);
                    GetStringValues(ref TG_U, ref TG_SU, ref TG_F, ref TG_L, ref TG_P, ref TG_E, ref TG_RANK, ref TG_OLDRANK, trigger);

                    TheSubject = TheSubject.Replace("@@R_U@@", R_U);
                    TheSubject = TheSubject.Replace("@@URL@@", R_U + "&Label=" + LabelValue);
                    TheSubject = TheSubject.Replace("@@R_SU@@", R_SU);
                    TheSubject = TheSubject.Replace("@@R_F@@", R_F);
                    TheSubject = TheSubject.Replace("@@R_L@@", R_L);
                    TheSubject = TheSubject.Replace("@@R_E@@", R_E);
                    TheSubject = TheSubject.Replace("@@R_RANK@@", R_RANK);
                    TheSubject = TheSubject.Replace("@@R_OLDRANK@@", R_OLDRANK);
                    TheSubject = TheSubject.Replace("@@C_NAME@@", R_F + " " + R_L);

                    TheSubject = TheSubject.Replace("@@TG_U@@", TG_U);
                    TheSubject = TheSubject.Replace("@@TG_SU@@", TG_SU);
                    TheSubject = TheSubject.Replace("@@TG_F@@", TG_F);
                    TheSubject = TheSubject.Replace("@@TG_L@@", TG_L);
                    TheSubject = TheSubject.Replace("@@TG_E@@", TG_E);
                    TheSubject = TheSubject.Replace("@@TG_RANK@@", TG_RANK);
                    TheSubject = TheSubject.Replace("@@TG_OLDRANK@@", TG_OLDRANK);

                    TheBody = TheBody.Replace("@@R_U@@", R_U);
                    //  TheBody = TheSubject.Replace("@@R_Password@@", strPassword);
                    TheBody = TheBody.Replace("@@URL@@", R_U + "&Label=" + LabelValue);
                    TheBody = TheBody.Replace("@@R_SU@@", R_SU);
                    TheBody = TheBody.Replace("@@R_F@@", R_F);
                    TheBody = TheBody.Replace("@@R_L@@", R_L);
                    TheBody = TheBody.Replace("@@R_E@@", R_E);
                    TheBody = TheBody.Replace("@@R_RANK@@", R_RANK);
                    TheBody = TheBody.Replace("@@R_OLDRANK@@", R_OLDRANK);

                    TheBody = TheBody.Replace("@@TG_U@@", TG_U);
                    TheBody = TheBody.Replace("@@TG_SU@@", TG_SU);
                    TheBody = TheBody.Replace("@@TG_F@@", TG_F);
                    TheBody = TheBody.Replace("@@TG_L@@", TG_L);
                    TheBody = TheBody.Replace("@@TG_E@@", TG_E);
                    TheBody = TheBody.Replace("@@TG_RANK@@", TG_RANK);
                    TheBody = TheBody.Replace("@@TG_OLDRANK@@", TG_OLDRANK);
                    TheBody = TheBody.Replace("@@C_NAME@@", R_F + " " + R_L);

                    var adminEmail = Convert.ToString(ConfigurationManager.AppSettings["AdminFromEmail"]);
                    var QueId = _userService.InsertToEmailQue(adminEmail, R_E, TheSubject, TheBody,true);

                    EmailSendViewModel m = new EmailSendViewModel();
                    m.To = new List<string>();
                    m.To.Add(R_E);
                    m.Subject = TheSubject;
                    m.EmailBody = TheBody;
                    FoxxEmailService emailService = new FoxxEmailService();
                    await emailService.SendEmail(m);
                }
            }
            return 1;
        }

        private async Task<int> GetSponsorsUsersID(long UserId)
        {
            int newUserId = Convert.ToInt32(UserId);
            int[] Sponsers = new int[10];
            Sponsers[0] = _userService.GetSponsorsUsersID(UserId);
            if (Sponsers[0] > 0)
            {
                Sponsers[1] = _userService.GetSponsorsUsersID(Sponsers[0]);
            }
            if (Sponsers[1] > 0)
            {
                Sponsers[2] = _userService.GetSponsorsUsersID(Sponsers[1]);
            }
            if (Sponsers[2] > 0)
            {
                Sponsers[3] = _userService.GetSponsorsUsersID(Sponsers[2]);
            }
            if (Sponsers[3] > 0)
            {
                Sponsers[4] = _userService.GetSponsorsUsersID(Sponsers[3]);
            }
            if (Sponsers[4] > 0)
            {
                Sponsers[5] = _userService.GetSponsorsUsersID(Sponsers[4]);
            }
            if (Sponsers[5] > 0)
            {
                Sponsers[6] = _userService.GetSponsorsUsersID(Sponsers[5]);
            }
            if (Sponsers[6] > 0)
            {
                Sponsers[7] = _userService.GetSponsorsUsersID(Sponsers[6]);
            }
            if (Sponsers[7] > 0)
            {
                Sponsers[8] = _userService.GetSponsorsUsersID(Sponsers[7]);
            }
            if (Sponsers[8] > 0)
            {
                Sponsers[9] = _userService.GetSponsorsUsersID(Sponsers[8]);
            }
            //if (Sponsers[9] > 0)
            //{
            //    Sponsers[10] = _userService.GetSponsorsUsersID(Sponsers[9]);
            //}

            if (Sponsers[0] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[0], 1);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[0], newUserId, 4);
                }
            }
            if (Sponsers[1] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[1], 2);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[1], newUserId, 4);
                }
            }
            if (Sponsers[2] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[2], 3);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[2], newUserId, 4);
                }
            }
            if (Sponsers[3] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[3], 12);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[3], newUserId, 4);
                }
            }
            if (Sponsers[4] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[4], 13);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[4], newUserId, 4);
                }
            }
            if (Sponsers[5] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[5], 14);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[5], newUserId, 4);
                }
            }
            if (Sponsers[6] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[6], 15);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[6], newUserId, 4);
                }
            }
            if (Sponsers[7] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[7], 16);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[7], newUserId, 4);
                }
            }
            if (Sponsers[8] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[8], 17);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[8], newUserId, 4);
                }
            }
            if (Sponsers[9] > 0)
            {
                var IsUserOptOutVal = IsUserOptOut(Sponsers[9], 18);
                if (IsUserOptOutVal == false)
                {
                    await QMailToMember(Sponsers[9], newUserId, 4);
                }
            }
            return 1;
        }

        private static void GetStringValues(ref string R_U, ref string R_SU, ref string R_F, ref string R_L, ref string R_P, ref string R_E, ref string R_RANK, ref string R_OLDRANK, System.Data.DataTable data)
        {
            if (data != null && data.Rows.Count > 0)
            {

                R_U = Convert.ToString(data.Rows[0]["UserName"]);
                R_SU = Convert.ToString(data.Rows[0]["UsersSponsor"]);
                R_F = Convert.ToString(data.Rows[0]["FirstName"]);
                R_L = Convert.ToString(data.Rows[0]["LastName"]);
                R_P = Convert.ToString(data.Rows[0]["Phone"]);
                R_E = Convert.ToString(data.Rows[0]["Email"]);
                R_RANK = Convert.ToString(data.Rows[0]["RankName"]);
                R_OLDRANK = Convert.ToString(data.Rows[0]["OldRankName"]);
            }
        }

        private bool IsUserOptOut(int RecipientID, int OptOutCatID)
        {
            var IsUserOptOut = false;
            var data = _userService.SetSmsCode(1, RecipientID, string.Empty, string.Empty, OptOutCatID);
            if (data != null && data.ValCount > 0)
            {
                IsUserOptOut = true;
            }
            return IsUserOptOut;
        }

        private void SetEmailSubjectAndBody(int EmailID, ref string Subject, ref string Body)
        {
            var email = _userService.GetEmailData(1, EmailID);
            if (email != null)
            {
                Subject = email.Subject;
                Body = email.Body;
                if (!string.IsNullOrEmpty(Body))
                {
                    //   var a = Body.Replace(Body.Replace(Body.Replace(Body.Replace("DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""", ""), "    ", ""), Environment.NewLine, ""), "<!---->", "");
                }
            }
        }
        private void SetEmailSubjectAndBodyByTaskKey(string TaskName, ref string Subject, ref string Body)
        {
            var email = _userService.GetEmailDataByTaskName(1, TaskName);
            if (email != null)
            {
                Subject = email.Subject;
                Body = email.Body;
                if (!string.IsNullOrEmpty(Body))
                {
                    //   var a = Body.Replace(Body.Replace(Body.Replace(Body.Replace("DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""", ""), "    ", ""), Environment.NewLine, ""), "<!---->", "");
                }
            }
        }
        #endregion

        public ActionResult PhoneVerification(string ToNumber)
        {
            UserViewModel user = null;
            if (TempData["PhoneVerificationData"] != null)
            {
                user = TempData.Peek("PhoneVerificationData") as UserViewModel;
            }

            var Id = Convert.ToString(TempData.Peek("UserId"));
            string personalusername = Convert.ToString(TempData.Peek("personalusername"));
            int.TryParse(Id, out int userId);

            if (user != null)
            {
                PhoneVerificationViewModel model = new PhoneVerificationViewModel();
                Random r = new Random();
                var theCode = r.Next(0, 10000).ToString("D5"); ;

                model.UserId = userId;
                model.Code = theCode.ToString();

                var a = _userService.SetTempOptInCode(1, model);
                model.ToNumber = SetPhoneNumber(user.Phone);// "+919812503572"; //user.Phone;
                model.FromNumber = GetPlivoDefaultNumber(model);
                model.Smstext = "Greetings from MoreMito.app. Enter the confirmation code below to complete the SMS opt-in process.  " + theCode;
                var QSmsMessage = _userService.SetTempOptInCode(2, model);
                if (QSmsMessage != null && QSmsMessage.InsertedId > 0)
                {
                    SendSMS(QSmsMessage.InsertedId, model.ToNumber, model.FromNumber, model.Smstext);
                }
                ViewBag.formattedPhoneNumber = model.ToNumber;// SetPhoneNumber(model.ToNumber);
                ViewBag.userid = model.UserId;
                Session["UserIDForPhoneVerification"] = userId;

                var request = ConfigurationManager.AppSettings["WebSitePath"];
                ViewBag.NavigationUrl = request + personalusername;// Convert.ToString(TempData["personalusername"]);
            }
            return View();
        }
        public ActionResult JoinOptions()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Term_conditionAndPolicies_Procedures_FOF()
        {
            ContentViewModel model = new ContentViewModel();

            model.PoliciesAndProcedures = _adminService.GetWebContentById(1, "Pnp_FOF", 0) != null ? _adminService.GetWebContentById(1, "Pnp_FOF", 0).HelpContent : "";
            model.TermsofService = _adminService.GetWebContentById(1, "TOS_FOF", 0) != null ? _adminService.GetWebContentById(1, "TOS_FOF", 0).HelpContent : "";

            return View("TermCondition", model);
        }
        [HttpGet]
        public ActionResult Term_conditionAndPolicies_Procedures_RC()
        {
            ContentViewModel model = new ContentViewModel();

            model.PoliciesAndProcedures = _adminService.GetWebContentById(1, "Pnp_RC", 0) != null ? _adminService.GetWebContentById(1, "Pnp_RC", 0).HelpContent : "";
            model.TermsofService = _adminService.GetWebContentById(1, "TOS_RC", 0) != null ? _adminService.GetWebContentById(1, "TOS_RC", 0).HelpContent : "";

            return View("TermCondition", model);
        }
        public void AddDownlineCounts(long UserId)
        {

            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            DownlineCountsViewModel model = new DownlineCountsViewModel();
            if (UserId > 1)
            {
                long LevelCount = 1;
                long FocusGuy = UserId;
                long FocusGuysSponsor = _adminService.GetSponsorsUsersID(1, FocusGuy, true);
                while (FocusGuysSponsor > 0)
                {
                    model.UsersID = UserId;
                    model.UplineUsersID = FocusGuysSponsor;
                    model.UserIsInUplinesLevelX = LevelCount;
                    baseModel = _commonService.InsertDownlineCounts(1, model);
                    FocusGuy = FocusGuysSponsor;
                    FocusGuysSponsor = _adminService.GetSponsorsUsersID(1, FocusGuy, true);
                    LevelCount += 1;
                }
            }
        }
        public void AddDownlineCountsAfterPlacement(long UserId)
        {

            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            DownlineCountsViewModel model = new DownlineCountsViewModel();
            if (UserId > 1)
            {
                long LevelCount = 1;
                long FocusGuy = UserId;
                long FocusGuysSponsor = _adminService.GetSponsorsUsersID(3, FocusGuy, true);
                while (FocusGuysSponsor > 0)
                {
                    model.UsersID = UserId;
                    model.UplineUsersID = FocusGuysSponsor;
                    model.UserIsInUplinesLevelX = LevelCount;
                    baseModel = _commonService.InsertDownlineCounts(1, model);
                    FocusGuy = FocusGuysSponsor;
                    FocusGuysSponsor = _adminService.GetSponsorsUsersID(3, FocusGuy, true);
                    LevelCount += 1;
                }
            }
        }
        [HttpGet]
        public ActionResult DownLine(int id)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var data = _adminService.GetUplineUsers();
                if (data != null && data.Count > 0)
                {
                    foreach (var item in data)
                    {
                        AddDownlineCounts(item.userId);
                    }
                    status = true;
                    msg = "success";
                }

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> SendTestEmail()
        {
            bool status = false;
            string msg = string.Empty;
            EmailSendViewModel m = new EmailSendViewModel();
            m.To = new List<string>();
            m.EmailBody = "test";
            m.To.Add("shubham_shandil@outlook.com");
            m.Subject = "from foxx";
            FoxxEmailService foxx = new FoxxEmailService();
            await foxx.SendEmail(m);
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);

        }


        private string CleanSMSPhone(string smsNumber)
        {
            try
            {
                return smsNumber;
                //string num = Regex.Replace(smsNumber, "[^\\w\\.@]", "");
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }
        private string GetPlivoDefaultNumber(PhoneVerificationViewModel model)
        {
            string number = string.Empty;
            var data = _userService.SetTempOptInCode(4, model);
            if (data != null)
            {
                number = Convert.ToString(data.InsertedId);
                if (!string.IsNullOrEmpty(number))
                {
                    IncrementPlivoDefaultNumber(number);
                }

            }
            return number;
        }
        private void IncrementPlivoDefaultNumber(string PlivoDefaultNumber)
        {
            PhoneVerificationViewModel model = new PhoneVerificationViewModel();
            model.PlivoDefaultNumber = PlivoDefaultNumber;
            var data = _userService.SetTempOptInCode(5, model);
        }
        private void SendSMS(long? PlivoQueueID, string ToNumber, string FromNumber, string SmsText)
        {
            // FromNumber = "17736496098";
            string AuthId = Convert.ToString(ConfigurationManager.AppSettings["PlivoKeyID"]);
            string AuthToken = Convert.ToString(ConfigurationManager.AppSettings["PlivoKeyValue"]);
            var api = new PlivoApi(AuthId, AuthToken);
            try
            {
                string UUID = string.Empty;
                var response = api.Message.Create(
              src: FromNumber,
               //dst: FromNumber,
               dst: new List<String> { ToNumber },
              text: SmsText
              );
                if (response != null && response.MessageUuid != null && response.MessageUuid.Count > 0)
                {
                    UUID = response.MessageUuid[0];
                    var status = _userService.UpdatePlivoSMSStatus(PlivoQueueID ?? 0, UUID);
                }
            }
            catch (Exception ex)
            {

            }


        }

        protected string SetPhoneNumber(string phoneNumber)
        {
            //string phoneNumber = phoneNumber;
            //NewSMSPhone = phoneNumber;
            //txtSMSPhone.Text = phoneNumber;
            if (phoneNumber.Length == 10)
                phoneNumber = "1" + phoneNumber;
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Insert(1, "(").Insert(1, " ").Insert(6, ")").Insert(7, " ").Insert(11, "-");

            return phoneNumber;
        }
        public ActionResult ConfirmCode(string Code, int UserId)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                PhoneVerificationViewModel model = new PhoneVerificationViewModel();
                model.Code = Code;
                model.UserId = UserId;// SetUserID();
                var data = _userService.SetTempOptInCode(7, model);
                if (data != null && data.InsertedId > 0)
                {
                    var data2 = _userService.SetTempOptInCode(8, model);
                    if (data2 != null)
                    {
                        string NewSMSPhone = data2.ErrorMsag;
                        model.ToNumber = SetPhoneNumber(NewSMSPhone);
                        model.FromNumber = GetPlivoDefaultNumber(model);
                        model.Smstext = "Congratulations! You will now receive mobile messages from MoreMito. Log into your MoreMito back office to control which system messages to receive.";
                        var QSmsMessage = _userService.SetTempOptInCode(2, model);
                        if (QSmsMessage != null && QSmsMessage.InsertedId > 0)
                        {
                            SendSMS(QSmsMessage.InsertedId, model.ToNumber, model.FromNumber, model.Smstext);
                        }
                        status = true;
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCode(string smsNumber, int UserId)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                PhoneVerificationViewModel model1 = new PhoneVerificationViewModel();
                model1.ToNumber = smsNumber;
                model1.UserId = UserId;// SetUserID();
                var data = _userService.SetTempOptInCode(9, model1);


                PhoneVerificationViewModel model = new PhoneVerificationViewModel();
                model.UserId = UserId;
                Random r = new Random();
                var theCode = r.Next(0, 10000).ToString("D5"); ;

                model.Code = theCode.ToString();
                var a = _userService.SetTempOptInCode(1, model);
                model.ToNumber = SetPhoneNumber(smsNumber);
                model.FromNumber = GetPlivoDefaultNumber(model);
                model.Smstext = "Greetings from MoreMito.app. Enter the confirmation code below to complete the SMS opt-in process.  " + theCode;
                var QSmsMessage = _userService.SetTempOptInCode(2, model);
                if (QSmsMessage != null && QSmsMessage.InsertedId > 0)
                {
                    SendSMS(QSmsMessage.InsertedId, model.ToNumber, model.FromNumber, model.Smstext);
                }
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        private int SetUserID()
        {
            string UsersID = string.Empty;

            string Uname = Convert.ToString(Session["UserIDForPhoneVerification"]);
            if (string.IsNullOrEmpty(Convert.ToString(Session["UserIDForPhoneVerification"])))
            {
                var userDetails = _userService.GetUserDetails(9, Uname, 0);
                if (userDetails != null)
                {
                    UsersID = Convert.ToString(userDetails.UserId);
                    Session["UserIDForPhoneVerification"] = UsersID;
                }
            }
            else
            {
                UsersID = Convert.ToString(Session["UserIDForPhoneVerification"]);
            }
            int.TryParse(UsersID, out int id);
            return id;
        }
        public ActionResult GetCountries()
        {
            bool status = false;
            bool HasSponsor = false;
            try
            {
                string userName = Session["SponsorName"] != null ? Session["SponsorName"].ToString() : "";
                if (!string.IsNullOrEmpty(userName))
                {
                    HasSponsor = true;
                }
                var countries = _commonService.GetCountriesAndStates(1, 0);
                status = true;
                return Json(new { status, countries , HasSponsor }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {

            }
            return Json(new { status }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetStates(int CountryId)
        {
            bool status = false;
            try
            {                
                var states = _commonService.GetCountriesAndStates(2, CountryId);
                status = true;
                return Json(new { status, states }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

            }
            return Json(new { status }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public  async  Task<ActionResult> JoinAsGuest(GuestJoinModel model)
        {
            bool status = false;
            int statusCode = -1;
            string ResponseMsg = string.Empty;
            bool HasSponsor = false;
            try
            {
                UserViewModel m = new UserViewModel();

                //   model.GuiId = Guid.NewGuid();
                string userName = Session["SponsorName"] != null ? Session["SponsorName"].ToString() : "";
                
                var userId = _commonService.GetUsersIDbyUserName(9, userName.Trim());
                if (userId == 0)
                {
                    string NewHoldingUser = ConfigurationManager.AppSettings["NewHoldingUserName"];
                    var NewHoldingUserId = _commonService.GetUsersIDbyUserName(9, NewHoldingUser.Trim());
                    m.UserId = NewHoldingUserId;
                    m.SponsorID = Convert.ToInt32(NewHoldingUserId);
                }
                else
                {
                    HasSponsor = true;
                    m.UserId = userId;
                    m.SponsorID = Convert.ToInt32(userId);
                }
               
                if (m.SponsorID == 0)
                {
                    ResponseMsg = "Sponsor User does not found. Contact administrator !";
                    return Json(new { status, statusCode, ResponseMsg }, JsonRequestBehavior.AllowGet);
                }

                var guestUserDetails = _commonService.CheckDuplicateEmailForGuestUser(model.Email);
                if (guestUserDetails !=null)
                {
                    if (guestUserDetails.IsAlreadyARetailUser == 1)
                    {
                        ResponseMsg = "Email address already exists!";
                        status = true;
                        statusCode = 5;
                        return Json(new { status, ResponseMsg, statusCode, guestUserDetails }, JsonRequestBehavior.AllowGet);
                    }
                    else if (guestUserDetails.IsAlreadyARetailUser == 2)
                    {
                        ResponseMsg = "Guest Email address already exists!";
                        //int NopUserId = _userService.GetNopUserId(1, guestUserDetails.UserId, model.Email);

                        //FoxShopTransferModel data = new FoxShopTransferModel
                        //{
                        //    Id = NopUserId,
                        //    Email = model.Email,
                        //    Password = guestUserDetails.Password,
                        //    UserName = guestUserDetails.Username,
                        //    WebsiteSponsorId = m.SponsorID
                        //};

                        //string path = GetGuestUserEncryptionId(data);
                        //statusCode = guestUserDetails.IsAlreadyARetailUser;
                        status = true;
                        statusCode = 5;
                        return Json(new { status, ResponseMsg, statusCode , guestUserDetails }, JsonRequestBehavior.AllowGet);
                    }

                   
                }
                string firstLetter = model.FirstName.Substring(0, 1);
                string secondLetter = model.LastName.Substring(0, 1);

                string AutoUserName = GetNextSmsCode(firstLetter, secondLetter);

             
                m.UserName = AutoUserName;
                m.Password = "12345";
                m.FirstName = model.FirstName;
                m.LastName = model.LastName;
                m.Email = model.Email;
                m.Phone = model.Phone;
                m.Address = model.Address;
                m.City = model.City;
                m.ZipCode = model.Zip;
                m.State = model.StateId;
                m.SignupType = 3;
                m.RetailOnly = true;
                m.IsCustomerAUser = 0;
                
               var response=  _userService.InsertUpdateUsersSignUpData(1, m);

                if (response.InsertedId > 0)
                {
                    //AddDownlineCounts(model.UserId);
                    AddDownlineCounts(response.InsertedId ?? 0); // pass userid of currently registered user instead of his sponsor user
                    try
                    {
                        SetSMSCode(response.InsertedId ?? 0);
                        if (HasSponsor == false)
                        {
                            model.Username = AutoUserName;
                            _userService.AddSponsorInfoToCsj((int)response.InsertedId, model.SponsorName);
                            await SendGuestUserMailToAdmin(model);
                        }
                        
                        await SendEmailsByKey(Convert.ToInt32(response.InsertedId ?? 0), m.Password, "User_Create_Welcome_front_office");
                    }
                    catch (Exception ex)
                    {

                    }

                    int NopUserId = _userService.GetNopUserId(1,(int) response.InsertedId, model.Email);

                    FoxShopTransferModel data = new FoxShopTransferModel
                    {
                        Id = NopUserId,
                        Email = model.Email,
                        Password = guestUserDetails.Password,
                        UserName = guestUserDetails.Username,
                        WebsiteSponsorId = m.SponsorID,
                        IsGuestUser=true
                    };

                    string path = GetGuestUserEncryptionId(data,model.PageType);
                    statusCode = 2;
                    status = true;
                    return Json(new { status, ResponseMsg, statusCode, path }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {

            }
            return Json(new { status, statusCode }, JsonRequestBehavior.AllowGet);
        }

        private string GetGuestUserEncryptionId(FoxShopTransferModel data, string PageType)
        {
            

            string parsedString = JsonConvert.SerializeObject(data);
            var encryptedString = Security.EncryptText(parsedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");
            var parsedhtml = HttpUtility.UrlEncode(encryptedString);

            string targetUrl = PageType == "pet" ? "pet-health-products" : "people-health-products";
            string path = pathValue + targetUrl + "?customerdetail=" + parsedhtml;           
            return path;
        }
    }
}