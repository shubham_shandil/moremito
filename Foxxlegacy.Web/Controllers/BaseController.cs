﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Foxxlegacy.Web.Controllers
{
    public class BaseController : Controller
    {
        string CacheName = String.Empty;
        RequestContext requestContext1 = new RequestContext();
        CacheContext _cacheContext = new CacheContext();
        // GET: Base
        public void InsertCacheUserInfo(LoggedinUserDetails model)
        {
            string CacheName = null;
            CacheName = "FoxxCacheContext_" + HttpContext.Session.SessionID;

            if (model != null)
            {
                CacheContext cacheContext = new CacheContext();
                // LoggedInUser Info
                cacheContext.LoggedInUser.UserId = model.UserId;
                cacheContext.LoggedInUser.UserName = model.UserName;
                cacheContext.LoggedInUser.FirstName = model.FirstName;
                cacheContext.LoggedInUser.Email = model.Email;
                cacheContext.LoggedInUser.LastName = model.LastName;
                cacheContext.LoggedInUser.Password = model.Password;
                cacheContext.LoggedInUser.Address = model.Address;
                cacheContext.LoggedInUser.City = model.City;
                cacheContext.LoggedInUser.State = model.State;
                cacheContext.LoggedInUser.Phone = model.Phone;
                cacheContext.LoggedInUser.Zip = model.Zip;
                cacheContext.LoggedInUser.SignupType = model.SignupType;
                cacheContext.LoggedInUser.NopUserId = model.NopUserId;
                //General Value
                cacheContext.SessionID = HttpContext.Session.SessionID;
                //cacheContext.IP = loginTrailViewModel.IP;
                //cacheContext.MacAddress = loginTrailViewModel.MacAddress;
                //cacheContext.CreatedBy = loginTrailViewModel.CreatedBy;
                //cacheContext.CreatedOn = loginTrailViewModel.CreatedOn;
                //cacheContext.LoginTime = loginTrailViewModel.LoginTime;
                //cacheContext.LogoutTime = loginTrailViewModel.LogoutTime;

                // Populate PrgCache
                HttpContext.Session[CacheName] = cacheContext;
            }
        }

        public LoggedinUserDetails GetCacheUserInfo()
        {
            LoggedinUserDetails model = new LoggedinUserDetails();
            CacheName = "FoxxCacheContext_" + HttpContext.Session.SessionID;
            _cacheContext = (CacheContext)Session[CacheName];

            if (_cacheContext != null)
            {
                model.UserId = _cacheContext.LoggedInUser.UserId;
                model.UserName = _cacheContext.LoggedInUser.UserName;
                model.FirstName = _cacheContext.LoggedInUser.FirstName;
                model.Email = _cacheContext.LoggedInUser.Email;
                model.LastName = _cacheContext.LoggedInUser.LastName;
                model.Password = _cacheContext.LoggedInUser.Password;
                model.Address = _cacheContext.LoggedInUser.Address;
                model.City = _cacheContext.LoggedInUser.City;
                model.State = _cacheContext.LoggedInUser.State;
                model.Phone = _cacheContext.LoggedInUser.Phone;
                model.Zip = _cacheContext.LoggedInUser.Zip;
                model.SignupType = _cacheContext.LoggedInUser.SignupType;
                model.NopUserId = _cacheContext.LoggedInUser.NopUserId;
                model.PlacementMode = true;
            }
            else
            {

            }

            if (model.UserId > 0)
                return model;
            else
                return null;
        }
        protected void ExportToCSV(DataTable dt, string ReportName)
        {
            string mydate = DateTime.Now.Date.ToString("MMddyyyy");
            //Get the data from database into datatable
            Response.Clear();
            Response.Buffer = true;
            // Response.AddHeader("content-disposition", "attachment;filename=" + ReportName + "_" + DateTime.Now + ".csv");
            Response.AddHeader("content-disposition", "attachment;filename=" + ReportName + "_" + mydate + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                //add separator
                sb.Append(dt.Columns[k].ColumnName + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        public string ConvertPartialViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (StringWriter writer = new StringWriter())
            {
                ViewEngineResult vResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext vContext = new ViewContext(this.ControllerContext, vResult.View, ViewData, new TempDataDictionary(), writer);
                vResult.View.Render(vContext, writer);
                return writer.ToString();
            }
        }

        //public ActionResult ExportToExcel<T>(List<T> ModelList)
        //{

        //   // var GetPinCertificateList = _adminService.GetPinCertificateList(4);

        //    DataTable dt = CommonViewModel.ToDataTable<T>( ModelList);
        //    dt.Columns.RemoveAt(0);//Remove column1
        //    dt.Columns.RemoveAt(0);//Remove column2
        //    //gv.DataSource = dt;
        //    //gv.DataBind();



        //    //string mydate = DateTime.Now.Date.ToString("MMddyyyy");
        //    //Response.ClearContent();
        //    //Response.Buffer = true;

        //    //Response.Charset = "";

        //    //Response.AddHeader("content-disposition", "attachment;filename=Foxx_RankPinReport_" + mydate + ".csv");
        //    //// Response.ContentType = "application/ms-excel";
        //    //Response.ContentType = "application/text";

        //    //Response.Charset = "";
        //    //StringWriter objStringWriter = new StringWriter();
        //    //HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);

        //    //gv.RenderControl(objHtmlTextWriter);

        //    //Response.Output.Write(objStringWriter.ToString());
        //    //Response.Flush();
        //    //Response.End();

        //    ExportToCSV(dt, "Foxx_RankPinReport_");

        //    return RedirectToAction("AdminMember");
        //}
    }
}