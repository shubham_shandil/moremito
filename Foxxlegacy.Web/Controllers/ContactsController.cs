﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Services.Contacts;
namespace Foxxlegacy.Web.Controllers
{
    public class ContactsController : BaseController
    {
        #region Fields
        private readonly IContactsService _contactsService;

        #endregion

        #region CTor
        public ContactsController
            (
            IContactsService contactsService

            )
        {
            this._contactsService = contactsService;

        }
        #endregion
        // GET: Contacts
        public ActionResult ContactAdmin()
        {
            ContactUsViewModel model = new ContactUsViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;
                model.ID = 0;

                model.FromUsersID = UserId;
                model.contactUsListViewModel = _contactsService.GetContactUsList(3, model);
                if (TempData.ContainsKey("SUCCESS_MESSAGE"))
                {
                    TempData["SUCCESS_MESSAGE"] = "success";
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult ContactAdmin(ContactUsViewModel model)
        {

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                if (ModelState.IsValid)
                {
                    var loggedInUser = GetCacheUserInfo();
                    long UserId = GetCacheUserInfo().UserId;
                    model.UsersID = 0;// Pending Need to Done
                    model.FromUsersID = UserId;
                    model.IpAddress = "50.209.90.241";//Pending Need to Done
                    model.Origin = "Reps back office";
                    _baseModel = _contactsService.ContactsInsertUpdate(1, model);
                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "success";

                        ModelState.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return RedirectToAction("ContactAdmin");
        }

        [HttpGet]
        public JsonResult GetContactUsById(long? ID)
        {
            ContactUsViewModel model = new ContactUsViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;
                model.ID = Convert.ToInt64(ID);
                model.FromUsersID = UserId;
                model.contactUsListViewModel = _contactsService.GetContactUsList(3, model);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddyourComments(ContactUsViewModel model)
        {
            
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.UsersID = UserId;
                _baseModel = _contactsService.ContactsInsertUpdate(4, model);
                model.contactUsListViewModel = _contactsService.GetContactUsList(5, model);
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "success";

                   
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult GetyourComments(ContactUsViewModel model)
        {
            
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                
                model.contactUsListViewModel = _contactsService.GetContactUsList(5, model);
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "success";


                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ClosethisContact(ContactUsViewModel model)
        {
            //ContactUsViewModel model = new ContactUsViewModel();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.UsersID = UserId;
                _baseModel = _contactsService.ContactsInsertUpdate(6, model);

                if (_baseModel.RowAffected > 0)
                {
                    


                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult MyContacts()
        {
            ContactUsViewModel model = new ContactUsViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;
              
                model.UsersID = UserId;
                model.contactUsListViewModel = _contactsService.GetContactUsList(7, model);
               
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }

        [HttpGet]
        public JsonResult GetMyContactsById(long? ID)
        {
            ContactUsViewModel model = new ContactUsViewModel();

            try
            {

                
                model.ID = Convert.ToInt64(ID);
               
                model.contactUsListViewModel = _contactsService.GetContactUsList(8, model);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult deleteMyContacts(long? ID)
        {
            ContactUsViewModel model = new ContactUsViewModel();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {

                
                model.ID = Convert.ToInt64(ID);

                _baseModel = _contactsService.ContactsInsertUpdate(9, model);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(_baseModel, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult UpdateMyContacts(ContactUsViewModel model)
        {
           
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {

                _baseModel = _contactsService.ContactsInsertUpdate(10, model);
                model.contactUsListViewModel = _contactsService.GetContactUsList(8, model);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}