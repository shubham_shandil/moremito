﻿using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Members;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Helpers;
using Foxxlegacy.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Controllers
{
    public class ProductsController : Controller
    {
        private readonly ICommonService _commonService;
        private readonly IMembersService _membersService;
        public ProductsController(ICommonService commonService, IMembersService memberService)
        {
            this._commonService = commonService;
            this._membersService = memberService;
        }
        // GET: Products
        public ActionResult PeopleHealthProducts()
        {
            //var userDetails = (UserDetailsViewModel)Session["userDetails"];
            //if (userDetails == null)
            //{
            //    return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Products/PeopleHealthProducts" });

            //}
            WebContentModel model = new WebContentModel();
            model.Content = _commonService.GetWebContent("Front_PeopleHealthProducts.aspx_1");
            if(!string.IsNullOrEmpty(model.Content))
            {
              model.Content=  model.Content.Replace("@ViewBag.ControlllerName", "Products");
            }
            ViewBag.ControlllerName1 = "Products";
            ViewBag.ControlllerName2 = "Products";
            return View(model);
        }

        public ActionResult PetHealthProducts()
        {
            //var userDetails = (UserDetailsViewModel)Session["userDetails"];
            //if (userDetails == null)
            //{
            //    return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Products/PetHealthProducts" });

            //}
            ViewBag.ControlllerName = "Products";
            return View();
        }

        public ActionResult Products()
        {
            //var userDetails = (UserDetailsViewModel)Session["userDetails"];
            //if (userDetails == null)
            //{
            //    return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Products/Products" });

            //}
            ViewBag.ControlllerName = "Products";
            return View();
        }
        public ActionResult Detail()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content64303.aspx");
            return View(model);
            
        }
        public ActionResult UltramitoRestore()
        {
            //var userDetails = (UserDetailsViewModel)Session["userDetails"];
            //if (userDetails == null)
            //{
            //    return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Products/UltramitoRestore" });

            //}
            ViewBag.ControlllerName = "Products";
            return View();
        }
        public ActionResult Rejuvenate()
        {
            //var userDetails = (UserDetailsViewModel)Session["userDetails"];
            //if (userDetails == null)
            //{
            //    return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Products/Rejuvenate" });

            //}
            ViewBag.ControlllerName = "Products";
            return View();
        }
        public ActionResult Bundle(string type)
        {
            var userDetails = (UserDetailsViewModel)Session["userDetails"];
            if (userDetails == null)
            {
                return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Products/Bundle" });

            }
            ViewBag.ControlllerName = "Products";
            List<BundelViewModel> bundles = _commonService.GetBundleItems(type == "pet" ? 2 : 1);
            ViewBag.type = type;
            return View(bundles);
        }
        public ActionResult OrderNow(OrderViewModel model)
        {
            string pathValue = ConfigurationManager.AppSettings["nopLogUrl"];
            bool status = false;
            string msg = string.Empty;
            try
            {
                if(model!=null && model.Items !=null && model.Items.Count > 0)
                {
                    string parsedString = JsonConvert.SerializeObject(model);
                    var encryptedString = Security.EncryptText(parsedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");
                    var parseVal = HttpUtility.UrlEncode(encryptedString);
                    status = true;
                    string RedirectUrl = pathValue + "Customer/OrderNow?Id=guest&bundle=" + parseVal;
                    return Json(new { status, msg, RedirectUrl }, JsonRequestBehavior.AllowGet);
                }
               
            }
            catch(Exception ex)
            {
                status = false;
                msg = ex.Message;
            }
           
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UserSignUpInfo()
        {
            bool status = false;
            string msg = string.Empty;
            var UserName =Convert.ToString( Request.RequestContext.HttpContext.Session["UserName"]);
            if (!string.IsNullOrEmpty(UserName))
            {
                status = true;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);

        }
    }
}