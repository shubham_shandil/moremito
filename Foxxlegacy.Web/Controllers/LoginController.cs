﻿using Foxxlegacy.Services.Admin;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Helpers;
using Foxxlegacy.Web.Models;
using Foxxlegacy.Web.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Controllers
{
    public class LoginController : BaseController
    {
        #region Fields
        private readonly IUserService _userService;
        private readonly IAdminService _adminService;
        private readonly ICommonService _commonService;
        private readonly IMembersService _membersService;
        #endregion


        #region CTor
        public LoginController
            (
            IUserService userService,
             IAdminService adminService,
             ICommonService commonService,
            IMembersService membersService
            )
        {
            this._userService = userService;
            this._adminService = adminService;
            this._commonService = commonService;
            this._membersService = membersService;
        }
        #endregion

        string pathValue = ConfigurationManager.AppSettings["nopLogUrl"];
        // GET: Login
        public ActionResult UserLogin()
        {
            UserLoginRequestViewModel userLogin = new UserLoginRequestViewModel();
            return View(userLogin);
        }

        [HttpPost]
        public ActionResult UserLogin(UserLoginRequestViewModel userLogin)
        {
            LoggedinUserDetails userDetails = new LoggedinUserDetails();
            try
            {
                if (ModelState.IsValid)
                {
                    userDetails = _userService.GetLoginUserDetails(1, userLogin);
                    if (userDetails != null)
                    {
                        userDetails.IsAdmin = _userService.IsUserInRole(1, userDetails.UserName, "admin");
                        userDetails.NopUserId = _userService.GetNopUserId(1, (int)userDetails.UserId, userDetails.Email);
                        Session.Add("UserName", userDetails.UserName);
                        Session.Add("Password", userDetails.Password);
                        Session.Add("Address", userDetails.Address);
                        Session.Add("FirstName", userDetails.FirstName);
                        Session.Add("LastName", userDetails.LastName);
                        Session.Add("City", userDetails.City);
                        Session.Add("Email", userDetails.Email);
                        Session.Add("State", userDetails.State);
                        Session.Add("Zip", userDetails.Zip);
                        Session.Add("SignupType", userDetails.SignupType);
                        Session.Add("IsAdmin", userDetails.IsAdmin);

                        InsertCacheUserInfo(userDetails);
                        return RedirectToAction("Dashboard", "Members");
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Invalid Username or Password. Please try again.";
                    }

                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Error occurred : "+ex.Message;
            }



            return View();
        }

        public ActionResult UserLogout()
        {
            Session.Clear();
            Session.RemoveAll();

            return RedirectToAction("UserLogin", "Login");
        }

        [FoxxAuthentication]
        public JsonResult UserCommonData()
        {
            var model = new LoggedinUserDetails();
            var loggedInUser = GetCacheUserInfo();
            if (loggedInUser != null)
            {
                model.UserId = loggedInUser.UserId;
                model.UserName = loggedInUser.UserName;
                model.FirstName = loggedInUser.FirstName;
                model.Email = loggedInUser.Email;
                model.LastName = loggedInUser.LastName;
                model.Password = loggedInUser.Password;
                model.Address = loggedInUser.Address;
                model.City = loggedInUser.City;
                model.State = loggedInUser.State;
                model.Phone = loggedInUser.Phone;
                model.Zip = loggedInUser.Zip;
                model.SignupType = loggedInUser.SignupType;
            }
            if (_userService.IsUserInRole(1, loggedInUser.UserName, "admin") == true)
            {
                model.IsAdmin = true;
            }
            else
            {
                model.IsAdmin = false;
            }
            if (_userService.IsUserInRole(1, loggedInUser.UserName, "Member") == true)
            {
                model.MembershipType = "Paid Representative";
            }
            else if (_userService.IsUserInRole(1, loggedInUser.UserName, "referringcustomer") == true)
            {
                model.MembershipType = "Referring Customer";
            }
            else if (_userService.IsUserInRole(1, loggedInUser.UserName, "friendoffoxx") == true)
            {
                model.MembershipType = "Customer";
            }
            string Name = string.Empty;
            var UserData = _membersService.GetUserInformation(2, loggedInUser.UserId);
            if (UserData != null)
            {
                model.FirstName = UserData.FirstName;
                model.LastName = UserData.LastName;
            }
            return Json(new { data = model }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult AllMenu()
        {
            //var model = new List<MenuViewModel>();
            string Menucontent = null;
            var loggedInUser = GetCacheUserInfo();
            if (loggedInUser != null)
            {
                var UserRole = _userService.GetAllRoles(2, loggedInUser.UserName);
            }
            if (_userService.IsUserInRole(1, loggedInUser.UserName, "Member") == true)
            {
                var ContentData = _adminService.GetWebContentById(3, null, Convert.ToInt32(5));
                Menucontent = ContentData.HelpContent;
            }
            else if (_userService.IsUserInRole(1, loggedInUser.UserName, "referringcustomer") == true)
            {
                var ContentData = _adminService.GetWebContentById(3, null, Convert.ToInt32(83));
                Menucontent = ContentData.HelpContent;
            }
            else if (_userService.IsUserInRole(1, loggedInUser.UserName, "friendoffoxx") == true)
            {
                var ContentData = _adminService.GetWebContentById(3, null, Convert.ToInt32(82));
                Menucontent = ContentData.HelpContent;
            }

            return Json(new { data = Menucontent }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GoToNopComProject()
        {
            var model = new LoggedinUserDetails();
            LoggedinUserDetails loggedInUser = GetCacheUserInfo();
            if (loggedInUser != null)
            {
                model.UserId = loggedInUser.UserId;
                model.UserName = loggedInUser.UserName;
                model.Email = loggedInUser.Email;
                model.Password = loggedInUser.Password;
                model.NopUserId = loggedInUser.NopUserId;
            }

            FoxShopTransferModel data = new FoxShopTransferModel
            {
                Id = model.NopUserId,
                Email = model.Email,
                Password = model.Password,
                UserName = model.UserName,
                WebsiteSponsorId = model.NopUserId
            };

            string parsedString = JsonConvert.SerializeObject(data);

            var encryptedString = Security.EncryptText(parsedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");

            var parsedhtml = HttpUtility.UrlEncode(encryptedString);
            string decryptedString = Security.DecryptText(encryptedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");
            //return Redirect(pathValue + "Customer/LoginWithFoxuser?EmailId=" + model.Email + "&Password=" + model.Password);
            return Redirect(pathValue + "Customer/LoginWithFoxuser?Id=" + parsedhtml);
            //return Redirect("http://182.76.115.146:86/Customer/LoginWithFoxuser?EmailId=" + model.Email + "&Password=" + model.Password);
        }

        public ActionResult LoginWithNopComUser(string EmailId)
        {
            return RedirectToAction("Dashboard", "Members");
            //UserLoginRequestViewModel userLogin = new UserLoginRequestViewModel();
            //if (EmailId != null)
            //{
            //    userLogin.UserName = EmailId;
            //}
            //LoggedinUserDetails userDetails = new LoggedinUserDetails();
            //try
            //{
            //    if (ModelState.IsValid)
            //    {
            //        userDetails = _userService.GetLoginUserDetails(2, userLogin);
            //        if (userDetails != null)
            //        {
            //            Session.Add("UserName", userDetails.UserName);
            //            Session.Add("Password", userDetails.Password);
            //            Session.Add("Address", userDetails.Address);
            //            Session.Add("FirstName", userDetails.FirstName);
            //            Session.Add("LastName", userDetails.LastName);
            //            Session.Add("City", userDetails.City);
            //            Session.Add("Email", userDetails.Email);
            //            Session.Add("State", userDetails.State);
            //            Session.Add("Zip", userDetails.Zip);
            //            Session.Add("SignupType", userDetails.SignupType);

            //            InsertCacheUserInfo(userDetails);
            //            return RedirectToAction("Dashboard", "Members");
            //        }
            //        else
            //        {
            //            TempData["ERROR_MESSAGE"] = "Invalid Username or Password. Please try again.";
            //        }

            //    }
            //}
            //catch (Exception ex)
            //{
            //    TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            //}



            //return RedirectToAction("Home", "Home");
        }




        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            bool IsUser = false;
            long UserId = 0;
            Rankup rankup = new Rankup();
            if (ModelState.IsValid)
            {
                IsUser = _adminService.IsUser(1, model.UserName);
                if (IsUser == true)
                {
                    UserId = _commonService.GetUsersIDbyUserName(9, model.UserName.Trim());
                    await QMailPasswordChangeRequest(UserId, 17, model.UserName);
                    rankup.SendTheMailQue();
                    TempData["emailsent"] = "success";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Please check your username spelling and try again";
                    TempData["emailsent"] = "fail";
                }
            }
            return View();
        }

        public async Task<int> QMailPasswordChangeRequest(long RecipientID, long EmailID, string Username)
        {
            var BaseUrl = Convert.ToString(ConfigurationManager.AppSettings["WebSitePath"]);
            EmailViewModel EmailViewmodel = Common.SetEmailSubjectAndBody(EmailID);
            PasswordChangeRequestsRecordViewModel passwordChangeRequestsRecord = new PasswordChangeRequestsRecordViewModel();
            SetUSUFLEROrViewModel SetUSUFLEROrViewModel = _adminService.SetUSUFLEROr(1, RecipientID);

            var LabelValue = "%Name:" + SetUSUFLEROrViewModel.FirstName + " " + SetUSUFLEROrViewModel.LastName + "%Phone:" + SetUSUFLEROrViewModel.Phone + "%Email:" + SetUSUFLEROrViewModel.Email + "%";
            if (!(string.IsNullOrEmpty(LabelValue) | string.IsNullOrWhiteSpace(LabelValue)))
            {
                byte[] byt = System.Text.Encoding.UTF8.GetBytes(LabelValue);
                LabelValue = Convert.ToBase64String(byt);
            }
            string sGUID;
            sGUID = System.Guid.NewGuid().ToString();

            string TheSubject = "";
            string TheBody = "";
            Guid R_PASSID = Guid.NewGuid();
            TheSubject = EmailViewmodel.Subject;
            TheBody = EmailViewmodel.Body;
            TheSubject.Replace("@@R_U@@", SetUSUFLEROrViewModel.Username);
            TheSubject.Replace("@@URL@@", SetUSUFLEROrViewModel.Username + "&Label=" + LabelValue);
            TheSubject.Replace("@@R_F@@", SetUSUFLEROrViewModel.FirstName);
            TheSubject.Replace("@@R_L@@", SetUSUFLEROrViewModel.LastName);
            TheSubject.Replace("@@C_NAME@@", SetUSUFLEROrViewModel.FirstName + " " + SetUSUFLEROrViewModel.LastName);

            TheBody = TheBody.Replace("@@R_U@@", SetUSUFLEROrViewModel.Username);
            TheBody = TheBody.Replace("@@URL@@", TheBody + "&Label=" + LabelValue);
            TheBody = TheBody.Replace("@@R_SU@@", SetUSUFLEROrViewModel.UsersSponsor);
            TheBody = TheBody.Replace("@@R_F@@", SetUSUFLEROrViewModel.FirstName);
            TheBody = TheBody.Replace("@@R_L@@", SetUSUFLEROrViewModel.LastName);
            TheBody = TheBody.Replace("@@R_E@@", SetUSUFLEROrViewModel.Email);
            TheBody = TheBody.Replace("@@R_RANK@@", SetUSUFLEROrViewModel.Rank);
            TheBody = TheBody.Replace("@@R_OLDRANK@@", SetUSUFLEROrViewModel.OldRank);
            TheBody = TheBody.Replace("@@R_ID@@", RecipientID.ToString());
            TheBody = TheBody.Replace("@@R_PASSID@@", R_PASSID.ToString());
            TheBody = TheBody.Replace("@@C_NAME@@", SetUSUFLEROrViewModel.FirstName + " " + SetUSUFLEROrViewModel.LastName);
            TheBody = TheBody.Replace("@@Base_Url@@", BaseUrl);
            TheBody = TheBody.Replace("@@R_GUID@@", sGUID);

            passwordChangeRequestsRecord.UsersID = RecipientID;
            passwordChangeRequestsRecord.RequestID = R_PASSID.ToString();
            passwordChangeRequestsRecord.FromEmailAddress = ConfigurationManager.AppSettings["AdminFromEmail"];
            passwordChangeRequestsRecord.ToEmailAddress = SetUSUFLEROrViewModel.Email;
            passwordChangeRequestsRecord.Subject = TheSubject;
            passwordChangeRequestsRecord.Body = TheBody;
            passwordChangeRequestsRecord.RecipientID = RecipientID;
            passwordChangeRequestsRecord.sGUID = sGUID;
            _adminService.NewPasswordChangeRequestsRecord(1, passwordChangeRequestsRecord);


            var adminEmail = Convert.ToString(ConfigurationManager.AppSettings["AdminFromEmail"]);

            EmailSendViewModel m = new EmailSendViewModel();
            m.To = new List<string>();
            m.EmailBody = TheBody;
            m.To.Add(passwordChangeRequestsRecord.ToEmailAddress);
            m.Subject = TheSubject;
            m.From = adminEmail;
            FoxxEmailService foxx = new FoxxEmailService();
            await foxx.SendEmail(m);

            return 1;
        }



        public ActionResult ForgotUserName()
        {
            return View();
        }


        [HttpPost]
        public async Task<ActionResult> ForgotUserName(ForgotUserNameViewModel model)
        {
            string Username = "";
            long UserId = 0;
            Rankup rankup = new Rankup();
            if (ModelState.IsValid)
            {
                Username = _commonService.GetUserNameByEmail(18, model.Email);
                if (Username != null)
                {
                    UserId = _commonService.GetUsersIDbyUserName(9, Username);
                    await QMailPasswordChangeRequest(UserId, 21, Username);
                    rankup.SendTheMailQue();
                    TempData["emailsent"] = "success";
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Your email does not exists, Please try with correct one.";
                    TempData["emailsent"] = "fail";
                }
            }
            return View();
        }

        public ActionResult UnAuthorized()
        {
            return View();
        }

        public ActionResult ResetPassword(string Id)
        {
            ResetPassword m = new ResetPassword();
            m.Id = Id;
            return View(m);
        }
        [HttpPost]
        public ActionResult ResetPassword(ResetPassword model)
        {
            int status;
            try
            {
                //throw new ArgumentOutOfRangeException();
                status = _adminService.ResetPassword(model, 2);
                //if (status == 1)
                //{
                //    ModelState.AddModelError("", "You can not reset password because your link has expired or is not valid. Please <a href='/login/forgotpassword'>click here</a> to request a new password reset request.");
                //}
            }
            catch (Exception ex)
            {
                model.ResponseMessage = ex.Message;
                status = -2;
                ModelState.AddModelError("", model.ResponseMessage);
            }
            model.ResponseCode = status;

            return View(model);
        }
    }
}