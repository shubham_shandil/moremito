﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Services.Commission;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Web.Models;
using static Foxxlegacy.Web.Models.StripePaymentGateway;
using System.Web;
using Foxxlegacy.Services.Members;

namespace Foxxlegacy.Web.Controllers
{
    public class WebApiController : ApiController
    {

        #region Fields
        private readonly ICommissionService _commissionService;
        private readonly ICommonService _commonService;
        private readonly IMembersService _membersService;

        #endregion

        #region CTor
        public WebApiController
            (
            ICommissionService commissionService,
            ICommonService commonService,
            IMembersService memberService

            )
        {
            this._commissionService = commissionService;
            this._commonService = commonService;
            this._membersService = memberService;
        }

        #endregion

        [HttpPost]
        [Route("api/Commission/DoCommissionAndRankup")]
        public long AfterOrderAction(DoCommissionViewModel model)
        {
            long Response = 0;
            int UserID = 0, OrderID = 0, UniLiverSponsorID = 0, insertNextUnilevelSponsorIDIn = 0;
            if (model.UserId > 0)
            {
                UserID = (int)_commonService.GetUserIDByNopCustomerID(5, model.UserId);
            }
           
            if (model.CommissionSweptAmount > 0)
            {
                var res = _commonService.InsertCommissionSwept(1, UserID, model.OrderId, model.CommissionSweptAmount ?? 0);
            }
            
            var isCommissionEnabled = _commonService.IsCommissionEnabled();
            if (isCommissionEnabled == true)
            {
                Rankup rankup = new Rankup();


                OrderID = model.OrderId;
                rankup.RunCustomerMetaDataUpline(UserID, OrderID, UniLiverSponsorID, insertNextUnilevelSponsorIDIn);
                rankup.MoreMito_ovcp_OrderVerifyCompPlan(OrderID, UserID, true);

            }


            return Response;
        }

        [HttpPost]
        [Route("api/Commission/DoCommission")]
        public DoCommissionViewModel DoCommission(DoCommissionViewModel model)
        {
            long UserId = 0;
            Rankup rankup = new Rankup();
            if (model == null)
            {
                model = new DoCommissionViewModel();
                model.OrderId = 0;
            }
            if (model.UserId > 0)
            {
                UserId = _commonService.GetUserIDByNopCustomerID(5, model.UserId);
                if (UserId > 0)
                {
                    model.UserId = (Int32)UserId;
                }
                model.Id = _commissionService.UserCommissionDistributionGenerate(1, model);

                //Rankup
                DoRankup(model);


            }
            if (model.Id > 0)
            {
                model.Message = "Success";
            }
            else
            {
                model.Id = 0;
                model.Message = "Oops!!! Something went wrong.";
            }
            return model;

        }


        public string DoRankup(DoCommissionViewModel model)
        {
            string response = "0";
            Rankup rankup = new Rankup();
            int UserID = 0, OrderID = 0, isSponsorship = 0;
            UserID = model.UserId;
            OrderID = model.OrderId;

            response = rankup.RankAdvanceOrderOwner(UserID, OrderID, isSponsorship);
            return response;
        }

        [HttpPost]
        [Route("api/Payment/StripePayment")]
        public ResponseData StripePayment(RecurringPaymentMethod model)
        {
            long response = 0;
            StripePaymentGateway stripePG = new StripePaymentGateway();
            ResponseData responseData = new ResponseData();
            responseData = stripePG.CreateCustomer(model);

            return responseData;
        }
        [HttpPost]
        [Route("api/Payment/CreateCustomerProfile")]
        public long CreateNewCustomerProfile(RecurringPaymentMethod model)
        {
            long response = 0;
            string CustomerId = model.CustomerId;
            StripePaymentGateway stripePG = new StripePaymentGateway();
            var Response = stripePG.CreateNewCustomerProfile(CustomerId, model);
            return response;

        }

        [HttpPost]
        [Route("api/Payment/CreatePayment")]
        public long CreatePaymentMethod(string CustomerId)
        {
            long response = 0;
            StripePaymentGateway stripePG = new StripePaymentGateway();
            response = stripePG.CreatePaymentMethod(CustomerId);


            return response;
        }


        [HttpGet]
        [Route("api/PlivoSmsResponse")]
        public IHttpActionResult PlivoSmsResponse(string From,string MessageIntent,string MessageUUID,string Text,string To)
        {

            PlivoOptInOutModel m = new PlivoOptInOutModel();
            m.From = From;
            m.To = To;          
            m.MessageUUid = MessageUUID;
            m.MessageIntent = MessageIntent;
            m.SmsText = Text;
            m.OptType = "PLIVO";

            _membersService.SavePlivoOptInOut(m);
            return Ok("success");
        }

        [HttpGet]
        [Route("get-user-info")]
        public IHttpActionResult GetUserInfo(string EmailId, int NopUserId)
        {
            var status = false;
            string msg = string.Empty;
            try
            {
                var res = _commonService.GetUserHeaderInfo(EmailId, NopUserId);
                if (res != null)
                {
                    status = true;
                    var obj = new
                    {
                        status,
                        res
                    };
                    return Ok(obj);
                }
                else
                {
                    msg = "No user found !";
                }

            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.Message;
            }
            var objErr = new
            {
                status,
                msg
            };
            return Ok(objErr);
        }
        [HttpGet]
        [Route("get-user-address")]
        public IHttpActionResult GetuserAddress(string Username, string Email, int NopUserId)
        {

            var status = false;
            string msg = string.Empty;
            try
            {
                var res = _commonService.GetUserAddress(Username, Email, NopUserId);
                if (res != null)
                {
                    status = true;
                    var obj = new
                    {
                        status,
                        res
                    };
                    return Ok(obj);
                }
                else
                {
                    msg = "No address found.";
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            var objErr = new
            {
                status,
                msg
            };
            return Ok(objErr);
        }

        [HttpGet]
        [Route("get-moremito-cash")]
        public IHttpActionResult GetMoreMitoCash(int NopUserId)
        {
            var status = false;
            string msg = string.Empty;
            try
            {
                var UserId = _commonService.GetUserIdFromNopUserId(NopUserId);
                var res = _commonService.GetAvailableCommissions(1, UserId);
                if (res != null)
                {
                    var CashAmount = res.TotalAvailable;
                    status = true;
                    var obj = new
                    {
                        status,
                        CashAmount
                    };
                    return Ok(obj);
                }
                else
                {
                    msg = "No Commission found !";
                }

            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.Message;
            }
            var objErr = new
            {
                status,
                msg
            };
            return Ok(objErr);
        }
    }
}
