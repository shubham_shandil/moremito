﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.IO;
using Foxxlegacy.Services.Members;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Services.User;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Contacts;
using System.Globalization;
using Foxxlegacy.Services.Admin;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;
using static Foxxlegacy.ViewModel.ViewModel.CommonViewModel;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using Foxxlegacy.Web.Helpers;
using Foxxlegacy.Web.Models;

namespace Foxxlegacy.Web.Controllers
{
    [FoxxAuthentication]
    [FoxxAuthorization("admin")]
    public class AdminPermissionController : BaseController
    {
        private readonly IAdminService _adminService;
        private readonly IMembersService _membersService;
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;
        private readonly IContactsService _contactsService;
        // GET: AdminPermission
        public AdminPermissionController
          (
          IAdminService adminService, IMembersService membersService, IUserService userService, ICommonService commonService, IContactsService contactsService

          )
        {
            this._adminService = adminService;
            this._membersService = membersService;
            this._userService = userService;
            this._commonService = commonService;
            this._contactsService = contactsService;
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Permission()
        {
            PermissionViewModel model = new PermissionViewModel();
            var loggedInUser = GetCacheUserInfo();
            if (loggedInUser.UserName == "cking" || loggedInUser.UserName == "rbaker")
            {
                model._member = _membersService.PopulateRoleList(2);
            }
            else
            {
                model._member = _membersService.PopulateRoleList(1);
            }
            return View(model);
        }
        public ActionResult RoleDetails(string UserName)
        {
            RoleInfoViewModel model = new RoleInfoViewModel();
            try
            {
                model._roleDetails = _membersService.RoleDetailsList(3, UserName);
                model.UserName = UserName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("~/Views/Admin/_RoleInfoView.cshtml", model);
        }
        [HttpPost]
        public ActionResult UserRoleUpdae(RoleInfoViewModel model)
        {
            try
            {
                long i = 0;
                int modelLen = model._roleDetails.Count;
                DataTable dt = new DataTable();
                dt.Columns.Add("row_idx", typeof(long));
                dt.Columns.Add("permission", typeof(bool));
                dt.Columns.Add("user_name", typeof(string));
                dt.Columns.Add("permission_id", typeof(Guid));
                dt.Columns.Add("role_name", typeof(string));
                dt.Columns.Add("role_id", typeof(Guid));
                dt.Columns.Add("user_id", typeof(Guid));
                foreach (var roleData in model._roleDetails)
                {
                    DataRow row = dt.NewRow();
                    try
                    {
                        if (roleData.PermissionRoleId == Guid.Empty || roleData.PermissionRoleId == null)
                        {
                            row["row_idx"] = 0;
                        }
                        else
                        {
                            row["row_idx"] = 1;
                        }
                        row["permission"] = roleData.Permission;
                        row["user_name"] = model.UserName;
                        row["permission_id"] = roleData.PermissionRoleId;
                        row["role_name"] = roleData.RoleName;
                        row["role_id"] = roleData.MainRoleId;
                        row["user_id"] = roleData.UserId;
                        dt.Rows.Add(row);
                    }
                    catch (Exception)
                    {
                    }
                }
                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                if (dt.Rows.Count > 0)
                {
                    _baseModel = _membersService.InsertUpdateUserRole(5, dt);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Permission");
        }

        public ActionResult AdminReport()
        {
            AdminReportViewModel model = new AdminReportViewModel();
            //_commonService.GetUsersIDbyUserName(9, adminHomeModel.UserName.Trim());
            model.csjEntries = _membersService.GetCSJEntriesList(1);
            model.correspondence = _membersService.GetCorrespondenceList(2);
            model.PinCertificateList = _adminService.GetPinCertificateList(1);
            model.CallList = _adminService.GetCallList(1);
            model.newUsers = _membersService.GetNewUsersList(3);

            model.ItemByState = _adminService.GetItemByStateList(1);

            model.TotalsbyState = _adminService.GetItemByStateList2(2);


            //Items By Date Start Scetion
            model.MinimumDate = DateTime.Now.Date.AddDays(-7);
            model.MaximumDate = DateTime.Today;
            if (TempData.ContainsKey("GetItemsByDate"))
            {

                TempData["ActiveTab"] = "ItemsByDateTab";
                model = (AdminReportViewModel)TempData["GetItemsByDate"];
                model.ItemByDate = _adminService.GetItemByDateList(3, model);
            }
            //Items By Date End Scetion

            //Items By Product Start Scetion
            model.MinimumDateForProduct = DateTime.Now.Date.AddDays(-7);
            model.MaximumDateForProduct = DateTime.Today;
            if (TempData.ContainsKey("GetItemsByProduct"))
            {

                TempData["ActiveTab"] = "ItemsByProductTab";
                model = (AdminReportViewModel)TempData["GetItemsByProduct"];
                model.MinimumDate = model.MinimumDateForProduct;
                model.MaximumDate = model.MaximumDateForProduct;
                var result = _adminService.GetItemByDateList(4, model);
                model.ItemByProduct = Data.StringToObject<List<ItemByProduct>>(Data.ObjectToString(result));

            }

            //Items By Product End Scetion


            // Maps Order Start Scetion
            model.MinimumDateForOrder = DateTime.Now.Date.AddDays(-7);
            model.MaximumDateForOrder = DateTime.Today;
            if (TempData.ContainsKey("GetMapsOrder"))
            {
                model = (AdminReportViewModel)TempData["GetMapsOrder"];
                model.MinimumDate = model.MinimumDateForOrder;
                model.MaximumDate = model.MaximumDateForOrder;
                model.MapsViewModel = _adminService.GetMapsList(1, model);
                model.MapsOrders = Data.StringToObject<List<MapsOrders>>(Data.ObjectToString(model.MapsViewModel));
                TempData["ActiveTab"] = "MapsOrderTab";
            }

            // Maps Order End Scetion


            // Maps Join Start  Scetion
            model.MinimumDateForJoin = DateTime.Now.Date.AddDays(-7);
            model.MaximumDateForJoin = DateTime.Today;
            if (TempData.ContainsKey("GetMapsJoin"))
            {
                model = (AdminReportViewModel)TempData["GetMapsJoin"];
                model.MinimumDate = model.MinimumDateForJoin;
                model.MaximumDate = model.MaximumDateForJoin;
                model.MapsViewModel = _adminService.GetMapsList(2, model);
                model.MapsJoins = Data.StringToObject<List<MapsJoins>>(Data.ObjectToString(model.MapsViewModel));
                TempData["ActiveTab"] = "GetMapsJoin";
            }

            //Maps Join End Scetion

            // Maps Max Level UserCounts Start  Scetion
            model.MaxLevelUserCounts = _adminService.GetUsercountsbymaxlevelList(1, model);
            model.Usercountsbymaxlevel = Data.StringToObject<List<Usercountsbymaxlevel>>(Data.ObjectToString(model.MaxLevelUserCounts));

            if (TempData.ContainsKey("GetMaxLevelUserCountsDeatis"))
            {
                TempData["ActiveTab"] = "MaxLevelUserCountsTab";
                model.Level = Convert.ToInt64(TempData["GetMaxLevelUserCountsDeatis"]);
                model.MaxLevelUserCounts = _adminService.GetUsercountsbymaxlevelList(2, model);
                model.UsercountsbymaxlevelDetails = Data.StringToObject<List<UsercountsbymaxlevelDetails>>(Data.ObjectToString(model.MaxLevelUserCounts));


                //var jsonString = JsonConvert.SerializeObject(model.MaxLevelUserCounts);
                //model.UsercountsbymaxlevelDetails = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UsercountsbymaxlevelDetails>>(jsonString);


            }
            // Maps Max Level UserCounts Start  Scetion

            // Maps SweepReport Start  Scetion
            model.SweepReport = _adminService.GetSweepReportList(1);

            if (TempData.ContainsKey("PaidDeleteSweepReport"))
            {
                TempData["ActiveTab"] = "SweepReportTab";
            }

            // Maps SweepReport End  Scetion



            //AutoShipCounts Start  Scetion
            model.AutoShipCounts = _adminService.GetAutoShipCountsList(1);
            // AutoShipCounts End  Scetion


            //OutOfStockItem Start  Scetion
            model.OutOfStockItem = _adminService.GetOutOfStockItemList(1);
            foreach (var item in model.OutOfStockItem)
            {
                var CountDetails = _adminService.GetOutOfStockItemUserDetails(2, item.Id);
                if (CountDetails != null)
                {
                    item.Count = CountDetails.Count;
                }

            }

            if (TempData.ContainsKey("GetOutOfStockItemUserDetails"))
            {
                TempData["ActiveTab"] = "OutOfStockItemTab";
                long Id = Convert.ToInt64(TempData["GetOutOfStockItemUserDetails"]);
                model.OutOfStockItemUserDetils = _adminService.GetOutOfStockItemUserDetails(2, Id);
                TempData["GetOutOfStockItemUserDetails"] = Id;
                TempData["UserDetailsTab"] = "UserDetailsTab";
            }
            if (TempData.ContainsKey("GetOutOfStockItemOrderDetails"))
            {
                TempData["ActiveTab"] = "OutOfStockItemTab";
                long Id = Convert.ToInt64(TempData["GetOutOfStockItemOrderDetails"]);
                model.OutOfStockItemOrderDetails = _adminService.GetOutOfStockItemOrderDetails(3, Id);
                TempData["OutOfStockItemOrderDetails"] = "OutOfStockItemOrderDetails";

            }
            // OutOfStockItem End  Scetion



            // Contest  Start  Scetion
            ViewBag.GetContestOct2017 = _adminService.GetContestList(1);
            ViewBag.GetContestSep2017 = _adminService.GetContestList(2);
            ViewBag.GetContestAug2017 = _adminService.GetContestList(3);
            ViewBag.GetContestJuly2017 = _adminService.GetContestList(4);
            ViewBag.GetContestJune2017 = _adminService.GetContestList(5);
            ViewBag.GetContestMay2017 = _adminService.GetContestList(6);

            // Contest End  Scetion

            model.ProductList = _commonService.GetAllDropdownlist(10);
            return View("~/Views/Admin/AdminReport.cshtml", model);
        }
        [HttpPost]
        public ActionResult ExportToCSVNewUser(AdminReportViewModel model)
        {
            //string UserList = Request.Form["UserList"];
            DataTable dt = CommonViewModel.ToDataTable<NewUsers>(model.newUsers);
            dt.Columns.RemoveAt(0);
            ExportToCSV(dt, "Foxx_Report_NewUser");
            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult GetUDEmailDetails(string UserName, bool csvCheck)
        {
            UserDownlineViewModel Objmodel = new UserDownlineViewModel();
            try
            {
                long UserId = _commonService.GetUsersIDbyUserName(9, UserName.Trim());
                Objmodel.emails = _membersService.GetUDEmailList(4, UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("~/Views/Admin/_DownLineEmail.cshtml", Objmodel);
        }
        public ActionResult ExportToUDEmail(string UserName)
        {
            UserDownlineViewModel Objmodel = new UserDownlineViewModel();
            long UserId = _commonService.GetUsersIDbyUserName(9, UserName.Trim());
            Objmodel.emails = _membersService.GetUDEmailList(4, UserId);
            if (Objmodel.emails.Count > 0)
            {
                DataTable dt = CommonViewModel.ToDataTable<Emails>(Objmodel.emails);
                dt.Columns.RemoveAt(0);
                ExportToCSV(dt, "Foxx_Report_Email");
            }
            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult GetNewUserData(string UserName, bool csvCheck)
        {
            UserDownlineViewModel Objmodel = new UserDownlineViewModel();
            try
            {
                if (_userService.IsUserInRole(1, UserName, "Member") == false && _userService.IsUserInRole(1, UserName, "ReferringCustomer") == false)
                {
                    Objmodel.ErrorMsg = UserName + " is not a member or referring customer.";
                }
                else
                {
                    long UserId = _commonService.GetUsersIDbyUserName(9, UserName.Trim());
                    Objmodel.newUserData = _membersService.GetNewUserDataList();
                    long DCount = 0, DCountPV = 0;
                    for (int i = 0; i < Objmodel.newUserData.Count; i++)
                    {
                        GetDistirbutorCountsForDateRange((DateTime)Objmodel.newUserData[i].START_DATE, (DateTime)Objmodel.newUserData[i].EndDate, ref DCount, ref DCountPV, UserId);
                        Objmodel.newUserData[i].EndDate = Convert.ToDateTime(Convert.ToString(Objmodel.newUserData[i].EndDate.Date.ToString("yyyy-MM-dd")) + " 23:59:59");
                        Objmodel.newUserData[i].Count = DCount;
                        Objmodel.newUserData[i].WithPV = DCountPV;
                        Objmodel.newUserData[i].PCT = DCountPV > 0 ? Convert.ToString((DCountPV / DCount)) + "%" : "0%";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //if (csvCheck)
            //{
            //    DataTable dt = CommonViewModel.ToDataTable<Emails>(Objmodel.emails);
            //    ExportToCSV(dt, "Foxx_Report_NewUser");
            //}
            return PartialView("~/Views/Admin/_DownLineEmail.cshtml", Objmodel);
        }
        public ActionResult ExportToNewUserData(string UserName)
        {
            UserDownlineViewModel Objmodel = new UserDownlineViewModel();
            long UserId = _commonService.GetUsersIDbyUserName(9, UserName.Trim());
            Objmodel.newUserData = _membersService.GetNewUserDataList();
            long DCount = 0, DCountPV = 0;
            for (int i = 0; i < Objmodel.newUserData.Count; i++)
            {
                GetDistirbutorCountsForDateRange((DateTime)Objmodel.newUserData[i].START_DATE, (DateTime)Objmodel.newUserData[i].EndDate, ref DCount, ref DCountPV, UserId);
                Objmodel.newUserData[i].EndDate = Convert.ToDateTime(Convert.ToString(Objmodel.newUserData[i].EndDate.Date.ToString("yyyy-MM-dd")) + " 23:59:59");
                Objmodel.newUserData[i].Count = DCount;
                Objmodel.newUserData[i].WithPV = DCountPV;
                Objmodel.newUserData[i].PCT = DCountPV > 0 ? Convert.ToString((DCountPV / DCount)) + "%" : "0%";
            }
            if (Objmodel.newUserData.Count > 0)
            {
                DataTable dt = CommonViewModel.ToDataTable<NewUserData>(Objmodel.newUserData);
                dt.Columns.RemoveAt(6);
                dt.Columns.RemoveAt(6);
                dt.Columns.RemoveAt(6);
                ExportToCSV(dt, "Foxx_Report_NewUser");
            }
            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult GetItemPerOrderDetails(string UserName, bool csvCheck)
        {
            UserDownlineViewModel Objmodel = new UserDownlineViewModel();
            try
            {
                if (_userService.IsUserInRole(1, UserName, "Member") == false && _userService.IsUserInRole(1, UserName, "ReferringCustomer") == false)
                {
                    Objmodel.ErrorMsg = UserName + " is not a member or referring customer.";
                }
                else
                {
                    long UserId = _commonService.GetUsersIDbyUserName(9, UserName.Trim());
                    Objmodel.UserId = UserId;
                    Objmodel.TheAverage = _membersService.GetItemPerOrder(8, UserId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("~/Views/Admin/_DownLineEmail.cshtml", Objmodel);
        }
        public bool IsNumber(object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }
        public void GetDistirbutorCountsForDateRange(DateTime StartDate, DateTime EndDate, ref long DCount, ref long DCountPV, long UsersID)
        {
            object retVal1 = _membersService.GetCount(6, StartDate, EndDate, UsersID);
            object retVal2 = _membersService.GetCount(7, StartDate, EndDate, UsersID);

            if (retVal1 != null)
            {
                if (retVal1 != DBNull.Value)
                {
                    if (IsNumber(retVal1))
                        DCount = (long)retVal1;
                }
            }
            if (retVal2 != null)
            {
                if (retVal2 != DBNull.Value)
                {
                    if (IsNumber(retVal2))
                        DCountPV = (long)retVal2;
                }
            }
        }
        public ActionResult Utilities()
        {
            UtilitiesViewModel model = new UtilitiesViewModel();
            model.phoneNotVerify = _membersService.GetPhoneNotVerifyList(1);
            model.optOutReports = _membersService.GetOptOutReportList(2);

            var loggedInUser = GetCacheUserInfo();
            var v = _membersService.IsAdminPlacementAllowed((int)loggedInUser.UserId);
            model.IsPlacementAllowed = v > 0;

            if (TempData.ContainsKey("GetDeleteUserUtility") && TempData.ContainsKey("GetDeleteUserUtilityBySearch"))
            {
                string search = (string)TempData["GetDeleteUserUtilityBySearch"];
                model.DeleteUserUtility = _membersService.GetDeleteUserUtilityList(1, search);
                model.SearchDeleteUserUtility = search;
                TempData["ActiveTab"] = "DeleteUserUtilityTab";
            }

            //-------------------------IncompleteUsers Tab Started Section-------------------------
            model.IncompleteUsers = _membersService.GetIncompleteUsersList(1);
            if (TempData.ContainsKey("DeleteIncompleteUsers"))
            {

                TempData["ActiveTab"] = "IncompleteUsersTab";
            }
            //-------------------------IncompleteUsers Tab End Section-------------------------


            //-------------------------OpenDataBaseConnection Tab Started Section-------------------------
            model.OpenDataBaseConnection = _membersService.GetOpenDataBaseConnectionList(1);
            if (TempData.ContainsKey("KillConnections"))
            {

                TempData["ActiveTab"] = "OpenDataBaseConnectionTab";
            }
            //-------------------------OpenDataBaseConnection Tab End Section-------------------------


            //-------VIE Import Start Section--

            if (TempData.ContainsKey("ActiveTab"))
            {

                TempData["ActiveTab"] = "VIEImportTab";
            }
            //-------VIE Import End Section--

            //-------VIEProcess CommissionTab Start Section--

            if (TempData.ContainsKey("ActiveTab"))
            {

                TempData["ActiveTab"] = "VIEProcessCommissionTab";
            }
            //-------VIEProcess CommissionTab End Section--

            return View("~/Views/Admin/Utilities.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetDeleteUserUtility(UtilitiesViewModel model)
        {
            // UtilitiesViewModel model = new UtilitiesViewModel();
            model.DeleteUserUtility = _membersService.GetDeleteUserUtilityList(1, model.SearchDeleteUserUtility);
            if (model.DeleteUserUtility.Count > 0)
            {
                TempData["GetDeleteUserUtilityBySearch"] = model.SearchDeleteUserUtility;
                TempData["GetDeleteUserUtility"] = "GetDeleteUserUtility";
            }

            return RedirectToAction("Utilities");
        }

        [HttpPost]
        public ActionResult DeleteUserUtility(long Id, string UserName, string SearchBy)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                DeleteUserUtility model = new DeleteUserUtility();
                long CustomerID = _commonService.GetCustomerIDByUserName(16, UserName);
                string AspNetUserID = _commonService.GetAspNetUserID_GUID(17, UserName);
                Id = _commonService.GetUsersIDbyUserName(9, UserName.Trim());

                model.CustomerID = CustomerID;
                model.ID = Id;
                model.UserName = UserName;
                model.AspNetUserID = AspNetUserID;
                _baseModel = _adminService.DeleteUserUtility(2, model);
                TempData["ActiveTab"] = "DeleteUserUtilityTab";
                TempData["GetDeleteUserUtility"] = "GetDeleteUserUtility";
                TempData["GetDeleteUserUtilityBySearch"] = SearchBy;
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return RedirectToAction("Utilities");

        }


        [HttpPost]
        public ActionResult DeleteIncompleteUsers(string UserName)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                IncompleteUsers model = new IncompleteUsers();
                long CustomerID = _commonService.GetCustomerIDByUserName(16, UserName);
                string AspNetUserID = _commonService.GetAspNetUserID_GUID(17, UserName);
                long Id = _commonService.GetUsersIDbyUserName(9, UserName.Trim());

                model.CustomerID = CustomerID;
                model.Id = Id;
                model.UserName = UserName;
                model.AspNetUserID = AspNetUserID;
                _baseModel = _adminService.DeleteIncompleteUsers(2, model);

                TempData["DeleteIncompleteUsers"] = "DeleteIncompleteUsers";

                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return RedirectToAction("Utilities");

        }


        [HttpPost]
        public ActionResult KillConnections(long? SpID)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                OpenDataBaseConnection model = new OpenDataBaseConnection();

                _baseModel = _adminService.KillConnections(2, Convert.ToInt64(SpID));

                TempData["KillConnections"] = "KillConnections";

                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return RedirectToAction("Utilities");

        }




        public ActionResult EmailExport()
        {
            EmailExport model = new EmailExport();
            List<EmailExport> modelList = new List<EmailExport>();
            var GetEmailExportList = _adminService.GetEmailExportList(1);




            foreach (var item in GetEmailExportList)
            {


                model = item;
                model.MembershipType = RoleTypeByUserName(item.UserName);
                if (RoleTypeByUserName(item.UserName).ToLower() == "member")
                {
                    model.MembershipType = "Paid Representative";
                }


                model.Enroller = item.SponsorUsersName;
                model.PersonAbove = model.PlacementUsersName == null ? model.SponsorUsersName : model.PlacementUsersName;

                model.SmsOptOutStatus = model.OptOut_SMSCount == 0 ? "OptIn" : "OptOut";

                modelList.Add(model);
            }
            GetEmailExportList = modelList;
            DataTable dt = CommonViewModel.ToDataTable<EmailExport>(GetEmailExportList);


            dt.Columns["PersonAbove"].ColumnName = "Person Above";
            dt.Columns["SmsOptOutStatus"].ColumnName = "Sms Opt Out Status";
            int columnCount = dt.Columns.Count;

            dt = dt.DefaultView.ToTable(false, "UserName", "FirstName", "LastName", "Phone", "Email", "MembershipType", "Person Above", "Enroller", "Sms", "Sms Opt Out Status").Copy();


            ExportToCSV(dt, "Foxx_EmailExport_");

            return RedirectToAction("AdminReport");
        }
        public string RoleTypeByUserName(string UserName)
        {



            string RoleTypeByUserName = string.Empty;
            if (_userService.IsUserInRole(1, UserName, "member") == true)
            {
                RoleTypeByUserName = "Member";
            }
            else if (_userService.IsUserInRole(1, UserName, "referringcustomer") == true)
            {
                RoleTypeByUserName = "ReferringCustomer";
            }
            else if (_userService.IsUserInRole(1, UserName, "friendoffoxx") == true)
            {
                RoleTypeByUserName = "FriendOfFoxx";
            }

            return RoleTypeByUserName;
        }



        public ActionResult EmailExportNoOrder()
        {
            EmailExportNoOrder model = new EmailExportNoOrder();
            List<EmailExportNoOrder> modelList = new List<EmailExportNoOrder>();
            var GetEmailExportNoOrderList = _adminService.GetEmailExportNoOrderList(2);




            foreach (var item in GetEmailExportNoOrderList)
            {


                model = item;
                model.MembershipType = RoleTypeByUserName(item.UserName);
                if (RoleTypeByUserName(item.UserName).ToLower() == "member")
                {
                    model.MembershipType = "Paid Representative";
                }




                modelList.Add(model);
            }
            GetEmailExportNoOrderList = modelList;
            DataTable dt = CommonViewModel.ToDataTable<EmailExportNoOrder>(GetEmailExportNoOrderList);


            ExportToCSV(dt, "Foxx_EmailExport_NoOrder_");

            return RedirectToAction("AdminReport");
        }

        [HttpPost]
        public ActionResult CustomerEmailsbyOrderCondition(CustomerEmailsbyOrderCondition CustomerEmailsbyOrderCondition)
        {


            var GetCustomerEmailsbyOrderCondition = _adminService.GetCustomerEmailsbyOrderCondition(3, CustomerEmailsbyOrderCondition.ReportType, "");
            string FileName = "EmailExport";
            switch (CustomerEmailsbyOrderCondition.ReportType)
            {
                case 0:
                    FileName = "AllCustomers";
                    break;
                case 1:
                    FileName = "CustomersWithNoOrders";
                    break;
                case 2:
                    FileName = "CustomersWithOrdersButNotInTheLast35Days";
                    break;
                case 3:
                    FileName = "CustomersWithoutRejuvenatePurchase";
                    break;
                case 4:
                    FileName = "CustomersWithoutMindPurchase";
                    break;
                case 5:
                    FileName = "CustomersWithoutPerformPurchase";
                    break;

                default:
                    break;
            }




            DataTable dt = CommonViewModel.ToDataTable<CustomerEmailsbyOrderCondition>(GetCustomerEmailsbyOrderCondition);
            dt = dt.DefaultView.ToTable(false, "UserName", "FirstName", "LastName", "Email", "InfoLink", "BuyLink").Copy();

            ExportToCSV(dt, "Foxx_EmailExport_", FileName);

            return RedirectToAction("AdminReport");
        }


        protected void ExportToCSV(DataTable dt, string ReportName, string FileName)
        {
            string mydate = DateTime.Now.Date.ToString("MMddyyyy");
            //Get the data from database into datatable
            Response.Clear();
            Response.Buffer = true;

            Response.AddHeader("content-disposition", "attachment;filename=Foxx_EmailExport_" + mydate + "_" + FileName + ".csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                //add separator
                sb.Append(dt.Columns[k].ColumnName + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }


        //public FileResult DownloadOptOutPhoneNumber()
        //{
        //    try
        //    {
        //        string path = Server.MapPath("~/SampleFiles/File");
        //        string fileName = Path.GetFileName("OptOutPhoneNumber.csv");
        //        string ext = Path.GetExtension("OptOutPhoneNumber.csv");
        //        string fullPath = Path.Combine(path, fileName);
        //        if (ext == ".csv")
        //        {
        //            return File(fullPath, "text/csv", "OptOutPhoneNumber.csv");
        //        }
        //        else
        //        {
        //            return File(fullPath, "text/csv", "OptOutPhoneNumber.csv");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return File("", "text/csv", "OptOutPhoneNumber.xlsx");
        //}        

        [HttpGet]

        public ActionResult GetItemsByDate(AdminReportViewModel model)
        {

            TempData["ActiveTab"] = "ItemsByDateTab";



            TempData["GetItemsByDate"] = model;
            return RedirectToAction("AdminReport");
        }

        [HttpPost]

        public ActionResult GetItemsByProduct(AdminReportViewModel model)
        {

            TempData["ActiveTab"] = "ItemsByProductTab";
            TempData["GetItemsByProduct"] = model;
            return RedirectToAction("AdminReport");
        }



        [HttpPost]

        public ActionResult GetMapsOrder(AdminReportViewModel model)
        {
            model.UsersId = _commonService.GetUsersIDbyUserName(9, model.UserName.Trim());
            TempData["GetMapsOrder"] = model;
            return RedirectToAction("AdminReport");
        }

        [HttpPost]

        public ActionResult GetMapsJoin(AdminReportViewModel model)
        {
            model.UsersId = _commonService.GetUsersIDbyUserName(9, model.UserName.Trim());
            TempData["GetMapsJoin"] = model;
            return RedirectToAction("AdminReport");
        }


        public ActionResult GetMaxLevelUserCountsDeatis(long Level)
        {

            TempData["GetMaxLevelUserCountsDeatis"] = Level;
            return RedirectToAction("AdminReport");
        }


        [HttpGet]
        public ActionResult PaidSweepReport(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                _baseModel = _adminService.PaidDeleteSweepReport(2, Id);

                TempData["PaidDeleteSweepReport"] = "PaidDeleteSweepReport";

                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return RedirectToAction("AdminReport");

        }

        [HttpPost]
        public ActionResult DeleteSweepReport(long Id)
        {

            try
            {

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                _baseModel = _adminService.PaidDeleteSweepReport(3, Id);

                TempData["PaidDeleteSweepReport"] = "PaidDeleteSweepReport";
                //Make Payment is pending
                if (_baseModel.RowAffected > 0)
                {

                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return RedirectToAction("AdminReport");

        }


        public ActionResult GetOutOfStockItemUserDetails(long Id)
        {

            TempData["GetOutOfStockItemUserDetails"] = Id;

            return RedirectToAction("AdminReport");
        }

        public ActionResult GetOutOfStockItemOrderDetails(long AutoshipDirectivesID, string AutoshipDirectivesItemsItemsID)
        {


            TempData["GetOutOfStockItemUserDetails"] = AutoshipDirectivesItemsItemsID;


            TempData["GetOutOfStockItemOrderDetails"] = AutoshipDirectivesID;
            return RedirectToAction("AdminReport");
        }
        public ActionResult SweepDetailsExportToExcel()
        {
            List<SweepDetailsExportToExcel> sweepDetailsExportToExcel = new List<SweepDetailsExportToExcel>();
            var GetSweepDetailsExportToExcel = _adminService.GetSweepReportList(4);
            var json_serializer = new JavaScriptSerializer();
            var jsonString = json_serializer.Serialize(GetSweepDetailsExportToExcel);
            sweepDetailsExportToExcel = json_serializer.Deserialize<List<SweepDetailsExportToExcel>>(jsonString);
            DataTable dt = CommonViewModel.ToDataTable<SweepDetailsExportToExcel>(sweepDetailsExportToExcel);
            dt.Columns["ExternalAccount"].ColumnName = "External Account";
            dt.Columns["PrepaidCard"].ColumnName = "Prepaid Card";
            dt = dt.DefaultView.ToTable(false, "Identifier", "Profile", "Preference", "External Account", "Prepaid Card", "Payment").Copy();
            ExportToCSV(dt, "Report.xls");
            return RedirectToAction("AdminReport");
        }


        public ActionResult GetRanksData()
        {
            bool status = true;
            string msg = string.Empty;
            try
            {
                List<RanksDataUpdateViewModel> response = _membersService.GetRanksData();
                string data = ConvertPartialViewToString("UpdateRankdata", response);
                status = true;
                return Json(new { status, data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateRankData(List<RanksDataUpdateViewModel> model)
        {
            bool status = true;
            string msg = string.Empty;
            try
            {
                status = _membersService.UpdateRabksData(model);
                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SavePaymentMethod(PaymentMethodsViewModel model)
        {
            bool status = false;
            string msg;
            try
            {
                var data = _membersService.SavePaymentMethod(model);
                status = true;
                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadPaymentMethods()
        {
            bool status = false;
            string msg;
            try
            {
                var data = _membersService.GetPaymentMethods(0);
                string content = ConvertPartialViewToString("_paymentMethodList", data);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeletePaymentMethod(int Id)
        {
            bool status = false;
            string msg;
            try
            {
                var data = _membersService.DeletePaymentMethod(Id);
                string content = ConvertPartialViewToString("_paymentMethodList", data);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUserQualInfo(UserQualModel model)
        {
            bool status = false;
            string msg;
            try
            {
                var data = _membersService.UserQualModel(model);
                status = true;
                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetUserQualInfo()
        {
            bool status = false;
            string msg;
            try
            {
                var data = _membersService.GetUserQual();
                status = true;
                return Json(new { status, data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult GetAdminPlacementTree(string username)
        {
            bool status = false;
            string msg;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                int TopMostUserId = 0;
                if (username == "adminPlacementView")
                {
                    TopMostUserId = 1;
                }
                else
                {
                    long userID = _commonService.GetUsersIDbyUserName(9, username.Trim());
                    if (userID > 0)
                    {
                        TopMostUserId = (int)userID;
                    }
                }
                string content = string.Empty;
                if (TopMostUserId > 0)
                {
                    GenealogyViewModel genealogy = new GenealogyViewModel();
                    genealogy.ReferralData = _membersService.GetReferralNetworkTree(1, TopMostUserId).FirstOrDefault();

                     content = ConvertPartialViewToString("_adminPlacementTreeView", genealogy);
                }
                else
                {
                    content = "No records found !";
                }
               


                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public PartialViewResult FetchReferralNetworkCurrentUserData(int id)
        {
            var model = new GenealogyViewModel();
            if (Request.IsAjaxRequest() && id > 0)
            {
                ReferralUser data = _membersService.GetReferralNetworkTree(1, id).FirstOrDefault();
                if (data != null)
                {
                    model.ReferralDataList.Add(data);
                }
            }
            return PartialView("_adminRefferalTreeView", model.ReferralDataList);
        }
        public ActionResult GetPersonalInformation(long InfoId)
        {
            MiscInfoViewModel misc_model = new MiscInfoViewModel();
            try
            {
                int TopMostUserId = 1;
                var loggedInUser = GetCacheUserInfo();
                string UserName = loggedInUser.UserName;
                misc_model._personalInfo = _membersService.GetPersonalDetails(4, InfoId);
                //long Level = _membersService.GetLevelInformation(2, InfoId, loggedInUser.UserId);
                long Level = _membersService.GetLevelInformation(2, InfoId, (long)TopMostUserId);
                misc_model._questionAnswer = _membersService.GetMQDetails(3, InfoId);
                misc_model._recurringOrder = _membersService.GetRecurringOrderDetails(5, InfoId);
                misc_model._processOrder = _membersService.GetProcessOrderDetails(6, InfoId);
                misc_model._advertisment = _membersService.GetAdvertismentDetails(7, InfoId);

                //var CountData = _userService.GetPlacementUserCount((int)InfoId, (int)loggedInUser.UserId);
                var CountData = _userService.GetAdminPlacementUserCount((int)InfoId, TopMostUserId);
                misc_model._menu.IsPlacementOn = CountData.UsersCount > 0;

                //-------------------Rank History Section----------------------------


                UserName = _commonService.GetUserNamebyUsersID(15, Convert.ToInt64(InfoId));
                TempData["TempDataUserId"] = InfoId;

                if (_userService.IsUserInRole(1, UserName, "member") == true)
                {
                    misc_model.MembershipType = "Paid Representative";
                }
                else if (_userService.IsUserInRole(1, UserName, "referringcustomer") == true)
                {
                    misc_model.MembershipType = "Referring Customer";
                }
                else if (_userService.IsUserInRole(1, UserName, "friendoffoxx") == true)
                {
                    misc_model.MembershipType = "Customer";
                }
                misc_model.HighestRankAchieved = _commonService.RankName(2, _commonService.GetRankID(1, Convert.ToInt64(InfoId)));
                //    misc_model.QTE = getQteNowForMBO(Convert.ToInt64(InfoId));

                var GetJoinDate = _membersService.GetUserInformation(14, Convert.ToInt64(InfoId));
                if (GetJoinDate != null)
                {
                    misc_model.JoinDate = GetJoinDate.JoinDate;
                }
                misc_model._rankHistory = _membersService.GetRankHistoryDetails(8, InfoId);
                //-------------------Rank History Section----------------------------

                //misc_model._marketingActivity = _membersService.GetMarketingActivityDetails(9, InfoId);

               // misc_model._personalInfo.SponserName = UserName;
                misc_model._personalInfo.Level = Level;



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_adminInfoView", misc_model);
        }
        public ActionResult GetOrdersForPlacement(int UserId)
        {
            bool status = false;
            string msg;
            try
            {
                var orders = _commonService.OrdersForPlacement(UserId);
                string content = ConvertPartialViewToString("_userPlacementOrders", orders);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult GetUserDetails(string Username, int CurrentUserId)
        {

            bool status = false;
            string msg;
            try
            {
                var userList = _userService.GetTargetUsersforOrderPlacement(Username);
                if (userList != null)
                {
                    userList = userList.Where(x => x.ID != CurrentUserId).ToList();
                }
                   string content = ConvertPartialViewToString("_adminOrderPlacementUserList", userList);
                status = true;
                //                return Json(userList, JsonRequestBehavior.AllowGet);

                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Updateorderplacement(OrderPlacementModel model)
        {
            bool status = false;
            string msg;
            try
            {
                if (model != null && model.OrderList.Count > 0)
                {
                    var isCommissionEnabled = _commonService.IsCommissionEnabled();
                    foreach (var item in model.OrderList)
                    {
                        var res = _userService.DeleteOrderForPlacement(item.OrderId, model.TargetUserId);

                        if (isCommissionEnabled == true)
                        {
                            int OrderID = 0, UniLiverSponsorID = 0, insertNextUnilevelSponsorIDIn = 0;
                            Rankup rankup = new Rankup();

                            int UserID = Convert.ToInt32(model.TargetUserId);
                            if (item.OrderId > 0 && UserID > 0)
                            {
                                OrderID = item.OrderId;

                                // run active customer meta data  
                                rankup.RunCustomerMetaDataUpline(UserID, OrderID, UniLiverSponsorID, insertNextUnilevelSponsorIDIn);

                                // generate commissions
                                rankup.MoreMito_ovcp_OrderVerifyCompPlan(OrderID, UserID, true); // temporary commented
                            }
                        }
                      
                    }
                    status = true;
                }

                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
    }
}