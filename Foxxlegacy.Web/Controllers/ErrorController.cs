﻿using Foxxlegacy.Services.Admin;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Home;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.Rankup;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Controllers
{
    public class ErrorController : Controller
    {
        #region Fields
        private readonly IUserService _userService;
        private readonly IHomeService _homeService;
        private readonly IMembersService _membersService;
        private readonly ICommonService _commonService;
        private readonly IAdminService _adminService;
        private readonly IRankupService _rankupService;
        #endregion


        public ErrorController(IUserService userService, IHomeService homeService, IMembersService memberService,
            ICommonService commonService, IAdminService adminService, IRankupService rankupService)
        {
            this._userService = userService;
            this._homeService = homeService;
            this._membersService = memberService;
            this._commonService = commonService;
            this._adminService = adminService;
            this._rankupService = rankupService;
        }
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Error()
        {
            // test commit

            // test commit shubham
            string LostUser = string.Empty;
            var LostUserString = HttpContext.Request.QueryString.ToString();
            string RemoteAddress = HttpContext.Request.UserHostAddress;
            if (!string.IsNullOrEmpty(LostUserString))
            {               

                int StartLooking = LostUserString.LastIndexOf("%2f") + 3;                
                LostUser = LostUserString.Substring(StartLooking);
                LostUser = HttpUtility.UrlDecode(LostUser);
                LostUser = LostUser.Trim();

                UserDetailsViewModel userDetails = new UserDetailsViewModel();
                Session["UserName"] = LostUser;

                userDetails = _userService.GetUserDetails(1, LostUser, 0);
                Session["userDetails"] = userDetails;
               
                if (_userService.IsUserInRole(1, LostUser, "Member") == false || _userService.IsUserInRole(1, LostUser, "FriendOfFoxx") == false || _userService.IsUserInRole(1, LostUser, "ReferringCustomer") == false)
                {                   
                    string UserNameFromSmsCode = _userService.UserNameFromSmsCode(LostUser);
                    if (!string.IsNullOrEmpty(UserNameFromSmsCode))
                    {
                        LostUser = UserNameFromSmsCode;
                    }
                }
                
                if (_userService.IsUserInRole(1, LostUser, "Member") == true || _userService.IsUserInRole(1, LostUser, "FriendOfFoxx") == true || _userService.IsUserInRole(1, LostUser, "ReferringCustomer") == true)
                {
                    _userService.New404Log(LostUser, "Found a valid username", RemoteAddress);
                    Session["SponsorName"] = LostUser;
                    return Redirect("/Home/Home?UserName=" + LostUser);
                }
                else
                {
                    _userService.New404Log(LostUser, HttpUtility.UrlDecode(LostUserString), RemoteAddress);
                    Session["SponsorName"] = "";
                    return RedirectToAction("NoSponsor", "Home");
                }               
            }

            return RedirectToAction("Home", "Home");
        }
    }
}