﻿using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Foxxlegacy.Services.Home;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.Admin;
using Foxxlegacy.Services.Rankup;
using System.Configuration;
using System.IO;
using System.Data;
using Foxxlegacy.Web.Helpers;
using Newtonsoft.Json;

namespace Foxxlegacy.Web.Controllers
{
    //old
    public class HomeController : BaseController
    {
        //new -1 -2 -3 -4 -5-6 test comment

        #region Fields
        private readonly IUserService _userService;
        private readonly IHomeService _homeService;
        private readonly IMembersService _membersService;
        private readonly ICommonService _commonService;
        private readonly IAdminService _adminService;
        private readonly IRankupService _rankupService;
        #endregion

        #region CTor
        public HomeController(IUserService userService, IHomeService homeService, IMembersService memberService, 
            ICommonService commonService, IAdminService adminService, IRankupService rankupService)
        {
            this._userService = userService;
            this._homeService = homeService;
            this._membersService = memberService;
            this._commonService = commonService;
            this._adminService = adminService;
            this._rankupService = rankupService;
        }
        #endregion

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        [Route("Home/{UserName}")]
        public ActionResult Home(string UserName)
        {
            UserDetailsViewModel userDetails = new UserDetailsViewModel();
            if (!string.IsNullOrEmpty(UserName))
            {
                Session["UserName"] = UserName;
                userDetails = _userService.GetUserDetails(1, UserName, 0);
                Session["userDetails"] = userDetails;
            }         
          
           
            if (UserName != null && userDetails == null)
            {
                return RedirectToAction("NoSponsor", "Home");
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult NoSponsor(string ReturnUrl)
        {
            UserDetailsViewModel userDetails = new UserDetailsViewModel();
            userDetails.ReturnUrl = ReturnUrl;
            //if (ReturnUrl=="/" || ReturnUrl.Length==0)
            //{
            //    userDetails.ReturnUrl = "/Home/Home";
            //}
            //else
            //{
            //    userDetails.ReturnUrl = ReturnUrl;
            //}          

            return View(userDetails);
        }

        [HttpPost]
        public ActionResult NoSponsor(UserDetailsViewModel userDetails)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Session["SponsorName"] = userDetails.UserName;
                    string UserName = userDetails.UserName, ReturnUrl = userDetails.ReturnUrl;
                    userDetails = _userService.GetUserDetails(1, UserName, 0);
                    if (userDetails != null)
                    {
                        Session["userDetails"] = userDetails;

                        if (ReturnUrl == "/Home/Home" || ReturnUrl == null)
                        {
                            return RedirectToAction("Home", "Home", new { UserName = UserName });
                        }

                        return Redirect(ReturnUrl);
                    }
                    else
                    {
                        TempData["MESSAGE"] = "We can not locate your representative's username. Please check the username above and try again.";

                    }

                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(userDetails);
        }
        public ActionResult Legacy()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ContactUs()
        {
            ContactViewModel ContactUs = new ContactViewModel();
            UserDetailsViewModel userDetails = new UserDetailsViewModel();
            userDetails = (UserDetailsViewModel)Session["userDetails"];
            if (userDetails == null)
            {
                return RedirectToAction("NoSponsor", "Home", new { ReturnUrl = "/Home/ContactUs" });
                //if (userDetails.Name == null && userDetails.Email == null && userDetails.Phone == null)
                //{
                //    return RedirectToAction("NoSponsor", "Home");
                //}
            }
            else
            {
                TempData["SPONSER_NAME"] = userDetails.Name;
            }

            return View(ContactUs);
        }
        [HttpPost]
        public ActionResult ContactUs(ContactViewModel ContactUs)
        {
            try
            {
                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                UserDetailsViewModel userDetails = new UserDetailsViewModel();
                userDetails = (UserDetailsViewModel)Session["userDetails"];
                TempData["SPONSER_NAME"] = userDetails.Name;
                if (ModelState.IsValid)
                {
                    string ContactUserName = "Master";
                    if (Session["SponsorName"] != null)
                        ContactUserName = Session["SponsorName"].ToString();
                    long userID = _commonService.GetUsersIDbyUserName(9, ContactUserName.Trim());
                    ContactUs.UsersID = userID;
                    _baseModel = _homeService.ContactUs(1, ContactUs);

                    if (_baseModel.RowAffected > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Thank you for contacting us.We will reply as soon as possible.";


                        //  Core.QMailToMember(UsersID, UsersID, 29) 'SMS
                        // If Core.IsSMSOptIn(UsersID, 0) = True Then Core.QSMSToMember(UsersID, UsersID, 29, "")
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                    }

                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return View(ContactUs);
        }
        public ActionResult FAQ()
        {
            FAQViewModel model = new FAQViewModel();

            try
            {
                model.Id = 0;         //Default value     
                model.fAQCategoryListViewModel = _homeService.GetFAQCategoryList(1, model);
                int i = 0;

                if (model.fAQCategoryListViewModel.Count > 0)
                {

                    foreach (var item in model.fAQCategoryListViewModel)
                    {

                        model.Id = item.Id;
                        model.fAQCategoryListViewModel[i].FAQList = _homeService.GetFAQList(2, model);

                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }
        public JsonResult FAQById(long? Id)
        {
            FAQViewModel model = new FAQViewModel();

            try
            {
                model.Id = Convert.ToInt64(Id);
                model.fAQCategoryListViewModel = _homeService.GetFAQCategoryList(1, model);
                int i = 0;

                if (model.fAQCategoryListViewModel.Count > 0)
                {
                    foreach (var item in model.fAQCategoryListViewModel)
                    {
                        model.Id = item.Id;
                        model.fAQCategoryListViewModel[i].FAQList = _homeService.GetFAQList(2, model);

                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FAQList()
        {
            FAQViewModel model = new FAQViewModel();

            try
            {
                model.Id = 0;         //Default value     
                model.fAQCategoryListViewModel = _homeService.GetFAQCategoryList(1, model);
                int i = 0;

                if (model.fAQCategoryListViewModel.Count > 0)
                {

                    foreach (var item in model.fAQCategoryListViewModel)
                    {
                        model.Id = item.Id;
                        model.fAQCategoryListViewModel[i].FAQList = _homeService.GetFAQList(2, model);
                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Events()
        {

            EventsViewModel model = new EventsViewModel();

            try
            {
                model.WebContentName = "Members_Content_Events";
                model.eventsListViewModel = _homeService.GetWebContent(1, model);
                if (model.eventsListViewModel != null)
                {
                    model.Content = model.eventsListViewModel.Content;
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }
        public ActionResult Calendar()
        {

            EventsViewModel model = new EventsViewModel();

            try
            {
                model.WebContentName = "Members_Content_CalendarCode";
                model.eventsListViewModel = _homeService.GetWebContent(1, model);
                if (model.eventsListViewModel != null)
                {
                    model.Content = model.eventsListViewModel.Content;
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(model);
        }

        public ActionResult Test()
        {
            Rankup _rankUp = new Rankup();            
            _rankUp.test();
            return View();
        }
        public ActionResult Refresh()
        {
            Rankup Rankup = new Rankup();
            int UserId = 0, RankId=0,OrderId=0, isSponsorship=0;
            var UserDetails = GetCacheUserInfo();
            if(UserDetails!=null)
            {
                 UserId = Convert.ToInt32(UserDetails.UserId);
            }
            OrderId = _commonService.GetOrderIdByUser(3,UserId,RankId);
            if(OrderId>0)
            {
                Rankup.RankAdvanceOrderOwner(UserId,OrderId, isSponsorship);
            }
            //_commonService.UsersQTE(2, UserId, RankId); //remove 
            //RankId = _rankupService.GetRankByUser(1, UserId);
            //if(RankId>0)
            //{
            //    _commonService.UsersQTE(1,UserId,RankId);
            //}
            return View();
        }
        public ActionResult PetHealthProducts()
        {
            return View();
        }
        public ActionResult PeopleElevate()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_PeopleElevate.aspx_1");
                model.peopleElevate.Front_PeopleElevate1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_PeopleElevate.aspx_2");
                model.peopleElevate.Front_PeopleElevate2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult PeopleElevatePerformance()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_PeopleElevatePerformance.aspx_1");
                model.peopleElevatePerformance.Front_PeopleElevatePerformance1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_PeopleElevatePerformance.aspx_2");
                model.peopleElevatePerformance.Front_PeopleElevatePerformance2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult PeopleElevateMind()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_PeopleElevateMind.aspx_1");
                model.peopleElevateMind.Front_PeopleElevateMind1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_PeopleElevateMind.aspx_2");
                model.peopleElevateMind.Front_PeopleElevateMind2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult UltraMitoMobility()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_UltraMitoMobility.aspx_1");
                model.ultraMitoMobility.UltraMitoMobilityProducts1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_UltraMitoMobility.aspx_2");
                model.ultraMitoMobility.UltraMitoMobilityProducts2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult UltraMitoAllergy()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_UltraMitoAllergy.aspx_1");
                model.ultraMitoAllergy.Front_UltraMitoAllergy1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_UltraMitoAllergy.aspx_2");
                model.ultraMitoAllergy.Front_UltraMitoAllergy2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult UltraMitoRecovery()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_UltraMitorecovery.aspx_1");
                model.ultraMitoRecovery.Front_UltraMitorecovery1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_UltraMitorecovery.aspx_2");
                model.ultraMitoRecovery.Front_UltraMitorecovery2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult UltraMitoLife()
        {
            DynamicPagesViewModel model = new DynamicPagesViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                string Front_UltraMitoMobility1 = _membersService.GetWebPagesContent(1, "Front_UltraMitoLife.aspx_1");
                model.ultraMitoLife.Front_UltraMitoLife1 = Front_UltraMitoMobility1;
                string Front_UltraMitoMobility2 = _membersService.GetWebPagesContent(1, "Front_UltraMitoLife.aspx_2");
                model.ultraMitoLife.Front_UltraMitoLife2 = Front_UltraMitoMobility2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult PaymentMethod()
        {
            ContentViewModel model = new ContentViewModel();
            model.PaymentMethods = _adminService.GetWebContentById(1, "Front_PaymentMethods", 0) != null ? _adminService.GetWebContentById(1, "Front_PaymentMethods", 0).HelpContent : "";
            return View(model);
        }

        [HttpGet]
        [Route("ReturnPolicy")]
        public ActionResult ReturnPolicy()
        {
            ContentViewModel model = new ContentViewModel();
            model.ReturnPolicy = _adminService.GetWebContentById(1, "Front_ReturnPolicy", 0) != null ? _adminService.GetWebContentById(1, "Front_ReturnPolicy", 0).HelpContent : "";
            return View(model);
        }

        [HttpGet]
        public ActionResult PrivacyPolicy()
        {
            ContentViewModel model = new ContentViewModel();
            model.PrivacyPolicy = _adminService.GetWebContentById(1, "Front_PrivacyPolicy", 0) != null ? _adminService.GetWebContentById(1, "Front_PrivacyPolicy", 0).HelpContent : "";
            return View(model);
        }



        public ActionResult MyPersonalReferralOrders()
        {
            List<MyPersonalReferralOrdersViewModel> modelList = new List<MyPersonalReferralOrdersViewModel>();

            var loggedInUser = GetCacheUserInfo();
            try
            {
                //loggedInUser.UserId = 6944;
                modelList = _homeService.GetMyPersonalReferralOrdersList(1, loggedInUser.UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(modelList);
        }

        public ActionResult MyPassUpOrders()
        {
            List<MyPassUpOrdersViewModel> modelList = new List<MyPassUpOrdersViewModel>();

            var loggedInUser = GetCacheUserInfo();
            try
            {
                loggedInUser.UserId = 7013;
                modelList = _homeService.GetMyPassUpOrdersList(1, loggedInUser.UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(modelList);
        }

        public ActionResult ImportOrders(int id)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var orders = _commonService.ImportOrders(0);
                if (orders != null && orders.Count > 0)
                {
                    foreach (var model in orders)
                    {
                        long Response = 0;
                        int UserID = 0, OrderID = 0, UniLiverSponsorID = 0, insertNextUnilevelSponsorIDIn = 0;
                        Rankup rankup = new Rankup();
                        if (model.Customerid > 0)
                        {
                            UserID = (int)_commonService.GetUserIDByNopCustomerID(5, model.Customerid);
                        }
                        if (UserID > 0)
                        {
                            // UserID = model.UserId;
                            OrderID = model.OrderId;
                            rankup.RunCustomerMetaDataUpline(UserID, OrderID, UniLiverSponsorID, insertNextUnilevelSponsorIDIn);
                            rankup.MoreMito_ovcp_OrderVerifyCompPlan(OrderID, UserID, true);
                        }
                        
                        
                    }
                }
                status = true;
            }
            catch(Exception ex)
            {
                msg = ex.Message;
            }

            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Thankyou(int NopuserId)
        {
            var res = _commonService.GetUserHeaderInfo("", NopuserId);
            string personalusername = "";
            if (res != null)
            {
                personalusername = res.UserName;
            }
            var request = ConfigurationManager.AppSettings["WebSitePath"];
            ViewBag.NavigationUrl = request + personalusername;
            return View();
        }
       
        public ActionResult ShopifyOrders(HttpPostedFileBase AttachmentFile)
        {
            bool status = false;
            string message = string.Empty;
            string content = string.Empty;
            try
            {
              
                string msg = string.Empty; string type = string.Empty;
            
                if (AttachmentFile != null && AttachmentFile.ContentLength > 0)
                {
                    if (AttachmentFile.FileName.EndsWith("xls") || AttachmentFile.FileName.EndsWith("xlsx"))
                    {
                        var fileName = Path.GetFileName(AttachmentFile.FileName);          //getting only file name(ex-ganesh.jpg)
                        var ext = Path.GetExtension(AttachmentFile.FileName);           //getting the extension(ex-.jpg)
                        string namee = Path.GetFileNameWithoutExtension(fileName);
                        string unique = Guid.NewGuid().ToString();//getting file name without extension
                        string myfile = namee + unique + ext;
                        string pathSave = Path.Combine(Server.MapPath("~/Upload/Import/"), myfile);
                        AttachmentFile.SaveAs(pathSave);

                        DataTable dt = ExcelImport.GetDataTable(namee, pathSave);
                        if (dt.Rows.Count > 0)
                        {
                            
                         
                        }
                        else
                        {
                            TempData["bomError"] = "Unable to Fetch data From Excel Sheet.Try Again Or Contact Your Service Provider.";
                            // return Json(new { msg, type }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
             //   content = ConvertViewToString("_bomList", bomModel);
                status = true;
                message = "Bom Imported successfully.";
                return Json(new { status, message, data = content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
            }
            return Json(new { status, message }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult DeliveryReport()
        {
            List<PlivoJsonModel> resjson = new List<PlivoJsonModel>();
            PlivoLogsModel m = new PlivoLogsModel();
            m.From =Convert.ToString( Request.Form["From"]);
            m.To = Convert.ToString(Request.Form["To"]);
            m.Status = Convert.ToString(Request.Form["Status"]);
            m.MessageUUid = Convert.ToString(Request.Form["MessageUUID"]);

            m.MessageTime = Convert.ToString(Request.Form["MessageTime"]);
            m.QueuedTime = Convert.ToString(Request.Form["QueuedTime"]);
            m.SentTime = Convert.ToString(Request.Form["SentTime"]);
            m.DeliveryReportTime = Convert.ToString(Request.Form["DeliveryReportTime"]);


            foreach (string key in Request.Form.AllKeys)
            {
                string val = Convert.ToString(Request.Form[key]);
                resjson.Add(new PlivoJsonModel
                {
                    Key = key,
                    Values = val
                }) ;

            }
            m.MessageJson = JsonConvert.SerializeObject(resjson);
            
            _membersService.SavePlivoResponseLogs(m);
            //PlivoSmsResponseLogs
            return this.Content("Report Received");
        }
    }
}

