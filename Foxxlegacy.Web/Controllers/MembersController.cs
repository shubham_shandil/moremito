﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using Foxxlegacy.Services.Admin;
using Foxxlegacy.Services.Commission;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.FoxxCash;
using Foxxlegacy.Services.Members;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using Foxxlegacy.Web.Helpers;
using Foxxlegacy.Web.Models;
using Newtonsoft.Json;
using Plivo;
using ZXing;
using ZXing.Common;

namespace Foxxlegacy.Web.Controllers
{
    [FoxxAuthentication]
    public class MembersController : BaseController
    {
        public static long CampaignLinkId = 0;
        #region Fields
        private readonly IMembersService _membersService;
        private readonly ICommonService _commonService;
        private readonly IUserService _userService;
        private readonly IAdminService _adminService;
        private readonly ICommissionService _commissionService;
        private readonly IFoxxCashService _foxxcashService;
        #endregion
        string pathValue = ConfigurationManager.AppSettings["nopLogUrl"];
        #region CTor
        public MembersController
            (
            IMembersService memberService, ICommonService commonService, IUserService userService, IAdminService adminService, ICommissionService commissionService , IFoxxCashService foxxcashService


            )
        {
            this._membersService = memberService;
            this._commonService = commonService;
            this._userService = userService;
            this._adminService = adminService;
            this._commissionService = commissionService;
            this._foxxcashService = foxxcashService;
        }
        #endregion

        // GET: Members10

        internal enum OrderStatus
        {
            Pending = 10,
            Processing = 20,
            Complete = 30,
            Cancelled = 40
        }
        internal enum PaymentStatus
        {
            Pending = 10,
            Authorized = 20,
            Paid = 30,
            PartiallyRefunded = 35,
            Refunded = 40,
            Voided = 50
        }
        internal enum ShippingStatus
        {
            ShippingNotRequired = 10,
            NotYetShipped = 20,
            PartiallyShipped = 25,
            Shipped = 30,
            Delivered = 40
        }
        public ActionResult Dashboard()
        {
            string UserName = GetCacheUserInfo().UserName;
            var BaseUrl = ConfigurationManager.AppSettings["WebSitePath"];
            ViewBag.myWebsite = BaseUrl + UserName;
            DashboardContentModel model = new DashboardContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content67884.aspx");
            return View(model);
        }

        public ActionResult Calendar()
        {
            return RedirectToAction("Calendar", "Home");
            //return View();

        }

        public ActionResult Events()
        {

            return View();
        }

        public ActionResult FAQ()
        {
            return RedirectToAction("FAQ", "Home");
            //return View();
        }

        public ActionResult MyInfo()
        {
            var loggedInUser = GetCacheUserInfo();
            long UserId = GetCacheUserInfo().UserId;
            UserInformationViewModel userInformation = new UserInformationViewModel();
            userInformation = _membersService.GetUserInformation(2, UserId);
            userInformation.CurrentRoleModel = _membersService.GetCurrentRole(1, Convert.ToInt32(UserId), null, null);
            userInformation.RoleList = _membersService.GetRoleList(2, Convert.ToInt32(UserId));
            if (userInformation.RoleList != null && userInformation.CurrentRoleModel != null)
            {
                userInformation.RoleList = userInformation.RoleList.Where(x => x.RoleId != userInformation.CurrentRoleModel.RoleId).ToList();
                if (userInformation.CurrentRoleModel.RoleName == "FriendOfFoxx")
                {
                    userInformation.RoleList = userInformation.RoleList.Where(x => x.RoleName != "Member").ToList();
                }
            }
            // var res = _commonService.GetUserAddress(loggedInUser.UserName, loggedInUser.Email, loggedInUser.NopUserId);
            var res = _commonService.GetUserAddressByUserID((int)UserId);
            if (res != null)
            {
                userInformation.State = res.Statename; // res.CountryName + "/" + res.StateName;
                userInformation.Country = res.countryName;
            }
            return View(userInformation);

        }
        public ActionResult GetMyInfo()
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                userInformation = _membersService.GetUserInformation(2, UserId);
                var m = _commonService.GetUserAddressByUserID((int)UserId);
                if (m != null)
                {
                    userInformation.CountryId = m.CountryId;
                    userInformation.StateId = m.Stateid;
                }
                List<CountryStateModel> countries = _commonService.GetCountriesAndStates(1, 0);
                if (userInformation != null)
                {
                    userInformation.CountryList = countries;
                }
                if (userInformation.CountryId > 0)
                {
                    var states = _commonService.GetCountriesAndStates(2, userInformation.CountryId);
                    if (states != null)
                    {
                        userInformation.StateList = states;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_MyInfoEditView", userInformation);
        }

        [HttpPost]
        public ActionResult UpdateMyInfo(UserInformationViewModel userInformation)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            long UserId = GetCacheUserInfo().UserId;
            userInformation.UserId = UserId;
            _baseModel = _membersService.InsertUpdateUserInformation(1, userInformation);
            if (_baseModel.RowAffected > 0)
            {
                if (userInformation.CountryId > 0 && userInformation.StateId > 0)
                {
                    var dt = _membersService.UpdateUserAddress(userInformation);
                }


                Session["FirstName"] = userInformation.FirstName;
                Session["LastName"] = userInformation.LastName;
                TempData["SUCCESS_MESSAGE"] = "Your information has been updated successfully.";
            }
            else
            {

            }
            return RedirectToAction("MyInfo", "Members");
        }

        public ActionResult ChangeCallStatus(Boolean CallStatus)
        {
            try
            {
                UserInformationViewModel userInformation = new UserInformationViewModel();
                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                userInformation.CourtesyCalls = CallStatus;
                userInformation.UserId = GetCacheUserInfo().UserId;
                _baseModel = _membersService.InsertUpdateUserInformation(2, userInformation);
                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Your Courtesy voice calls status has been changed successfully.";
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return RedirectToAction("MyInfo", "Members");

        }

        [HttpPost]
        public ActionResult UpdateWelcomeTagInfo(UserInformationViewModel userInformation)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            long UserId = GetCacheUserInfo().UserId;
            userInformation.UserId = UserId;
            _baseModel = _membersService.InsertUpdateUserInformation(3, userInformation);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Your welcome tag information has been updated successfully.";
            }
            else
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return RedirectToAction("MyInfo", "Members");
        }
        public ActionResult InsertWelcomeTagInfo(String UserName)
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            long UserId = GetCacheUserInfo().UserId;
            userInformation.UserId = UserId;
            userInformation.WelcomeName = UserName;
            userInformation.WelcomeEmail = loggedInUser.Email;
            userInformation.WelcomePhone = loggedInUser.Phone;
            _baseModel = _membersService.InsertUpdateUserInformation(3, userInformation);
            if (_baseModel.RowAffected > 0)
            {

            }
            return RedirectToAction("MyInfo", "Members");
        }

        public ActionResult DeleteWelcomeTagInfo(long UserId)
        {
            UserInformationViewModel userInformation = new UserInformationViewModel();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            if (UserId == 0)
            {
                UserId = GetCacheUserInfo().UserId;
            }
            userInformation.UserId = UserId;
            _baseModel = _membersService.InsertUpdateUserInformation(4, userInformation);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Your welcome information has been deleted successfully.";
            }
            else
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return RedirectToAction("MyInfo", "Members");
        }


        [HttpPost]
        public ActionResult ChangePassword(UserInformationViewModel userInformation)
        {
            if (ModelState.IsValid)
            {
                ChangePasswordModel model = new ChangePasswordModel();

                SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                userInformation.UserId = UserId;
                model.UserName = loggedInUser.UserName;
                model.NewPassword = userInformation.Password;
                model.ModifiedBy = (int)loggedInUser.UserId;
                if (loggedInUser.Password == userInformation.CurrentPassword)
                {
                    //  _baseModel = _membersService.InsertUpdateUserInformation(5, userInformation);
                    var result = _adminService.ChangePassword(model);
                    //if (_baseModel.RowAffected > 0)
                    if (result > 0)
                    {
                        TempData["SUCCESS_MESSAGE"] = "Your password has been changed successfully.";
                    }
                    else
                    {
                        TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
                    }
                }
                else
                {
                    TempData["ERROR_MESSAGE"] = "Invalid Current Password. Please try again.";
                }



            }
            return RedirectToAction("MyInfo", "Members");
        }

        public ActionResult NotificationSettings()
        {
            List<EmailNotificationSettings> modellist = new List<EmailNotificationSettings>();
            EmailNotificationSettingsViewModel model = new EmailNotificationSettingsViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                model.OptOutCatID = 0;

                model.UsersID = UserId;
                modellist = _membersService.GetEmailNotificationSettings(3, model);
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return View(modellist);
        }

        [HttpGet]
        public JsonResult EmailNotificationSettingsOptOutInsertDelete(EmailNotificationSettingsViewModel objmodel)
        {
            EmailNotificationSettingsViewModel model = new EmailNotificationSettingsViewModel();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                model.OptOutCatID = objmodel.OptOutCatID;

                model.UsersID = UserId;
                if (objmodel.IsOptedIn == "No")
                {
                    _baseModel = _membersService.EmailNotificationSettingsOptOutInsertDelete(2, model);
                }
                else if (objmodel.IsOptedIn == "Yes")
                {
                    _baseModel = _membersService.EmailNotificationSettingsOptOutInsertDelete(1, model);
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(_baseModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SMSNotificationSettings()
        {
            SmsSettingModel m = new SmsSettingModel();

            List<TextNotificationSettings> modellist = new List<TextNotificationSettings>();
            TextNotificationSettingsViewModel model = new TextNotificationSettingsViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                model.OptOutCatID = 0;

                model.UsersID = UserId;
                modellist = _membersService.GetTextNotificationSettings(3, model);

                m = _membersService.SmsSettingInfo((int)UserId);
                var opt = _membersService.BroadcastOptStatus((int)UserId);
                if (opt != null)
                {
                    m.BroadCastOpt = opt;
                }
                m.OptList = modellist;
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            //return View(modellist);
            return View("SmsSettings",m);
        }
        public ActionResult GetCode(string smsNumber)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                 var loggedInUser = GetCacheUserInfo();
                int UserId = (int)GetCacheUserInfo().UserId;
                PhoneVerificationViewModel model1 = new PhoneVerificationViewModel();
                model1.ToNumber = smsNumber;
                model1.UserId = UserId;// SetUserID();
                //var data = _userService.SetTempOptInCode(9, model1);


                PhoneVerificationViewModel model = new PhoneVerificationViewModel();
                model.UserId = UserId;
                Random r = new Random();
                var theCode = r.Next(0, 10000).ToString("D5"); ;

                model.Code = theCode.ToString();
                var a = _userService.SetTempOptInCode(1, model);
                model.ToNumber = smsNumber;// SetPhoneNumber(smsNumber);
                model.FromNumber = GetPlivoDefaultNumber(model);
                model.Smstext = "Greetings from MoreMito.app. Enter the confirmation code below to complete the SMS opt-in process.  " + theCode;
                var QSmsMessage = _userService.SetTempOptInCode(2, model);
                if (QSmsMessage != null && QSmsMessage.InsertedId > 0)
                {
                    SendSMS(QSmsMessage.InsertedId, model.ToNumber, model.FromNumber, model.Smstext);
                }
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ConfirmCode(string Code,string Phone)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                int UserId = (int)GetCacheUserInfo().UserId;
                PhoneVerificationViewModel model = new PhoneVerificationViewModel();
                model.Code = Code;
                model.UserId = UserId;// SetUserID();
                model.ToNumber = Phone;
                var data = _userService.SetTempOptInCode(7, model);
                if (data != null && data.InsertedId > 0)
                {
                   
                    var data2 = _userService.SetTempOptInCode(8, model);
                    if (data2 != null)
                    {
                        string NewSMSPhone = data2.ErrorMsag;
                        model.ToNumber = Phone;// NewSMSPhone;// SetPhoneNumber(NewSMSPhone);
                        model.FromNumber = GetPlivoDefaultNumber(model);
                        model.Smstext = "Congratulations! You will now receive mobile messages from MoreMito. Log into your MoreMito back office to control which system messages to receive.";
                        var QSmsMessage = _userService.SetTempOptInCode(2, model);
                        if (QSmsMessage != null && QSmsMessage.InsertedId > 0)
                        {
                            SendSMS(QSmsMessage.InsertedId, model.ToNumber, model.FromNumber, model.Smstext);
                        }
                        status = true;
                    }
                    var optIn = _userService.SetTempOptInCode(9, model);
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        private void SendSMS(long? PlivoQueueID, string ToNumber, string FromNumber, string SmsText)
        {
            // FromNumber = "17736496098";
            string AuthId = Convert.ToString(ConfigurationManager.AppSettings["PlivoKeyID"]);
            string AuthToken = Convert.ToString(ConfigurationManager.AppSettings["PlivoKeyValue"]);
            var api = new PlivoApi(AuthId, AuthToken);
            try
            {
                string UUID = string.Empty;
                var response = api.Message.Create(
              src: FromNumber,
               //dst: FromNumber,
               dst: new List<String> { ToNumber },
              text: SmsText
              );
                if (response != null && response.MessageUuid != null && response.MessageUuid.Count > 0)
                {
                    UUID = response.MessageUuid[0];
                    var status = _userService.UpdatePlivoSMSStatus(PlivoQueueID ?? 0, UUID);
                }
            }
            catch (Exception ex)
            {

            }


        }
        private string GetPlivoDefaultNumber(PhoneVerificationViewModel model)
        {
            string number = string.Empty;
            var data = _userService.SetTempOptInCode(4, model);
            if (data != null)
            {
                number = Convert.ToString(data.InsertedId);
                if (!string.IsNullOrEmpty(number))
                {
                    IncrementPlivoDefaultNumber(number);
                }

            }
            return number;
        }
        private void IncrementPlivoDefaultNumber(string PlivoDefaultNumber)
        {
            PhoneVerificationViewModel model = new PhoneVerificationViewModel();
            model.PlivoDefaultNumber = PlivoDefaultNumber;
            var data = _userService.SetTempOptInCode(5, model);
        }
        protected string SetPhoneNumber(string phoneNumber)
        {
            //string phoneNumber = phoneNumber;
            //NewSMSPhone = phoneNumber;
            //txtSMSPhone.Text = phoneNumber;
            if (phoneNumber.Length == 10)
                phoneNumber = "1" + phoneNumber;
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Insert(1, "(").Insert(1, " ").Insert(6, ")").Insert(7, " ").Insert(11, "-");

            return phoneNumber;
        }
        [HttpGet]
        public JsonResult TextNotificationSettingsOptOutInsertDelete(TextNotificationSettingsViewModel objmodel)
        {
            TextNotificationSettingsViewModel model = new TextNotificationSettingsViewModel();
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;
                model.OptOutCatID = objmodel.OptOutCatID;

                model.UsersID = UserId;
                if (objmodel.IsOptedIn == "No")
                {
                    _baseModel = _membersService.TextNotificationSettingsOptOutInsertDelete(2, model);
                }
                else if (objmodel.IsOptedIn == "Yes")
                {
                    _baseModel = _membersService.TextNotificationSettingsOptOutInsertDelete(1, model);
                }

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return Json(_baseModel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MyPersonals()
        {
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            List<PersonalInformationViewModel> obj_List = new List<PersonalInformationViewModel>();
            obj_List = _membersService.GetPersonalInformation(1, UserName);
            return View(obj_List);
        }
        public ActionResult MyRep()
        {
            var loggedInUser = GetCacheUserInfo();
            long UserId = GetCacheUserInfo().UserId;
            RepresentativeInformationViewModel _SponserInformation = new RepresentativeInformationViewModel();
            _SponserInformation = _membersService.GetRepresentativeInformation(1, UserId, false);

            long sponserUserId = _SponserInformation.SponsorUsersID;

            RepresentativeInformationViewModel _repsInformation = new RepresentativeInformationViewModel();
            _repsInformation = _membersService.GetRepresentativeInformation(2, _SponserInformation.SponsorUsersID, false);
            if (_repsInformation == null)
            {
                _repsInformation = new RepresentativeInformationViewModel();
            }
            _repsInformation.Name = string.Concat(loggedInUser.FirstName, " ", loggedInUser.LastName);
            _repsInformation.UserName = loggedInUser.UserName;

            //long sponser = PopulateInfo(sponserId);
            //PopulateInfo(sponserUserId, UserId);

            return View(_repsInformation);
        }
        public void PopulateInfo(long Id, long UserId)
        {
            long Sponser = Id;
            long SponSpon = _membersService.GetRepresentativeInformation(1, Id, false).SponsorUsersID;
            long SponSponSpon = 0;
            if (SponSpon > 0)
            {
                SponSponSpon = _membersService.GetRepresentativeInformation(1, SponSpon, false).SponsorUsersID;
            }

            List<long> UserIDList = new List<long>();
            if (Sponser > 25)
            {
                UserIDList.Add(Sponser);
            }
            if (Sponser > 25)
            {
                UserIDList.Add(SponSpon);
            }
            if (Sponser > 25)
            {
                UserIDList.Add(SponSponSpon);
            }

            long nextSilver = getNextUplineSilver(UserId);

            bool NeedToFindSilver2 = true;
            bool NeedToFindSilver3 = true;

            if (nextSilver >= 25)
            {
                switch (GetRankID(nextSilver))
                {
                    case 4:
                        {
                            if (nextSilver > 25)
                                UserIDList.Add(nextSilver);
                            break;
                        }

                    case 5:
                        {
                            if (nextSilver > 25)
                                UserIDList.Add(nextSilver);
                            NeedToFindSilver2 = false;
                            break;
                        }

                    case object _ when GetRankID(nextSilver) > 5:
                        {
                            if (nextSilver > 25)
                                UserIDList.Add(nextSilver);
                            NeedToFindSilver2 = false;
                            NeedToFindSilver3 = false;
                            break;
                        }
                }
                while (NeedToFindSilver2 == true)
                {
                    nextSilver = getNextUplineSilver(nextSilver);
                    switch (GetRankID(nextSilver))
                    {
                        case 5:
                            {
                                if (nextSilver > 25)
                                    UserIDList.Add(nextSilver);
                                NeedToFindSilver2 = false;
                                break;
                            }

                        case object _ when GetRankID(nextSilver) > 5:
                            {
                                if (nextSilver > 25)
                                    UserIDList.Add(nextSilver);
                                NeedToFindSilver2 = false;
                                NeedToFindSilver3 = false;
                                break;
                            }
                    }
                }
                while (NeedToFindSilver3 == true)
                {
                    nextSilver = getNextUplineSilver(nextSilver);
                    switch (GetRankID(nextSilver))
                    {
                        case object _ when GetRankID(nextSilver) > 5:
                            {
                                if (nextSilver > 25)
                                    UserIDList.Add(nextSilver);
                                NeedToFindSilver3 = false;
                                break;
                            }
                    }
                }
            }

            long CustomerID = _membersService.GetRepresentativeInformation(2, UserId, false).CustomerID;
            long G1 = 0;
            long G2 = 0;
            long G3 = 0;
            CustomerDetailsViewModel _custModel = new CustomerDetailsViewModel();
            _custModel = _membersService.GetCustomerDetails(1, CustomerID);
            if (_custModel.G1ID > 25)
                UserIDList.Add(G1);
            if (_custModel.G2ID > 25)
                UserIDList.Add(G2);
            if (_custModel.G3ID > 25)
                UserIDList.Add(G3);
            int P1 = 0;
            int P2 = 0;
            int P3 = 0;
            if (_custModel.P1ID > 25)
                UserIDList.Add(P1);
            if (_custModel.P2ID > 25)
                UserIDList.Add(G2);
            if (G3 > 25)
                UserIDList.Add(G3);
            // deduplicate list
            if (UserIDList.Count > 1)
                UserIDList = UserIDList.Distinct().ToList();
        }
        public long getNextUplineSilver(long UserID)
        {
            long getNextUplineSilver = 26;
            long EvalGuy = _membersService.GetRepresentativeInformation(1, UserID, false).SponsorUsersID;
            while (EvalGuy > 25)
            {
                if (GetRankID(EvalGuy) >= 4)
                {
                    getNextUplineSilver = EvalGuy;
                    break;
                }
                else
                    return EvalGuy = _membersService.GetRepresentativeInformation(1, EvalGuy, false).SponsorUsersID;
            }
            return getNextUplineSilver;
        }
        public long GetRankID(long UsersID)
        {
            long GetRankID = 0;
            GetRankID = _membersService.GetRepresentativeInformation(2, UsersID, false).RankID;
            return GetRankID;
        }
        public ActionResult Genealogy()
        {
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            GenealogyViewModel genealogy = new GenealogyViewModel();
            genealogy.ReferralData = _membersService.GetReferralNetworkTree(1, loggedInUser.UserId).FirstOrDefault();

            //if(genealogy.ReferralData is object)
            //{
            //    genealogy.ReferralData.IsQualified = IsUserQualified((int)genealogy.ReferralData.Id);
            //    genealogy.ReferralData.IsEmail = IsUserEmailOpt((int)genealogy.ReferralData.Id);
            //    genealogy.ReferralData.IsSms = IsUserSmsOpt((int)genealogy.ReferralData.Id);
            //    var MainRankOpt = UserRankOpt((int)genealogy.ReferralData.Id);
            //    if ((MainRankOpt.SponsorUsersID == MainRankOpt.UniLevelSponsorID) && (Convert.ToDateTime(MainRankOpt.JoinDate) > System.DateTime.Now.AddMonths(-1)))
            //    {
            //        genealogy.ReferralData.IsPersonalMovable = true;
            //        genealogy.ReferralData.IsRankShow = true;
            //        genealogy.ReferralData.RankText = MainRankOpt.RankName;
            //    }
            //    else if ((MainRankOpt.SponsorUsersID == MainRankOpt.UniLevelSponsorID) && (Convert.ToDateTime(MainRankOpt.JoinDate) < System.DateTime.Now.AddMonths(-1)))
            //    {
            //        genealogy.ReferralData.IsPersonalPermanent = true;
            //        genealogy.ReferralData.IsRankShow = true;
            //        genealogy.ReferralData.RankText = MainRankOpt.RankName;
            //    }
            //    else if ((MainRankOpt.SponsorUsersID != MainRankOpt.UniLevelSponsorID) && (Convert.ToDateTime(MainRankOpt.JoinDate) > System.DateTime.Now.AddMonths(-1)))
            //    {
            //        genealogy.ReferralData.IsNonPersonalMovable = true;
            //        genealogy.ReferralData.IsRankShow = true;
            //        genealogy.ReferralData.RankText = MainRankOpt.RankName;
            //    }
            //    else if ((MainRankOpt.SponsorUsersID != MainRankOpt.UniLevelSponsorID) && (Convert.ToDateTime(MainRankOpt.JoinDate) < System.DateTime.Now.AddMonths(-1)))
            //    {
            //        genealogy.ReferralData.IsNonPersonalPermanent = true;
            //        genealogy.ReferralData.IsRankShow = true;
            //        genealogy.ReferralData.RankText = MainRankOpt.RankName;
            //    }
            //}           

            return View(genealogy);

        }

        [HttpGet]
        public PartialViewResult FetchReferralNetworkUserData(int id)
        {
            var model = new GenealogyViewModel();
            if (Request.IsAjaxRequest() && id > 0)
            {
                model.ReferralDataList = _membersService.GetReferralNetworkTree(2, id);
            }
            return PartialView("_ReferralTreeView", model.ReferralDataList);
        }
        [HttpGet]
        public PartialViewResult FetchReferralNetworkCurrentUserData(int id)
        {
            var model = new GenealogyViewModel();
            if (Request.IsAjaxRequest() && id > 0)
            {
                ReferralUser data = _membersService.GetReferralNetworkTree(1, id).FirstOrDefault();
                if (data != null)
                {
                    model.ReferralDataList.Add(data);
                }
            }
            return PartialView("_ReferralTreeView", model.ReferralDataList);
        }


        public ActionResult GenealogySearch(string keySearch)
        {
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            GenealogySearchViewModel model = new GenealogySearchViewModel();
            if (!string.IsNullOrEmpty(keySearch))
            {
                model.genealogyList = _membersService.GetGenealogyList(1, loggedInUser.UserId, keySearch);
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult GenealogySearch(GenealogySearchViewModel model)
        {
            return RedirectToAction("GenealogySearch", "Members", new { keySearch = model.keySearch });
        }
        public ActionResult ReportsSilver1Star()
        {
            var loggedInUser = GetCacheUserInfo();
            SilverStarReport model = new SilverStarReport();
            model.silverStar = _membersService.GetSilverIDList(1, loggedInUser.UserId);
            for (int i = 0; i < model.silverStar.Count; i++)
            {
                long nCount = _membersService.GetSilverCount(2, model.silverStar[i].Id, "1Star");
                bool Check = _membersService.GetSilverCheck(4, model.silverStar[i].Id, "1Star");
                if (Check == true)
                {
                    nCount += 1;
                }
                model.silverStar[i].SilverStarCount = nCount;
                model.silverStar[i].CheckCount = Check;
            }

            return View(model);
        }
        public ActionResult ReportsSilver2Star()
        {
            var loggedInUser = GetCacheUserInfo();
            SilverStarReport model = new SilverStarReport();
            model.silverStar = _membersService.GetSilverIDList(1, loggedInUser.UserId);
            for (int i = 0; i < model.silverStar.Count; i++)
            {
                long nCount = _membersService.GetSilverCount(3, model.silverStar[i].Id, "2Star");
                bool Check = _membersService.GetSilverCheck(5, model.silverStar[i].Id, "2Star");
                if (Check == true)
                {
                    nCount += 1;
                }
                model.silverStar[i].SilverStarCount = nCount;
                model.silverStar[i].CheckCount = Check;
            }

            return View(model);
        }
        public ActionResult TeamCustomerCounts()
        {
            var loggedInUser = GetCacheUserInfo();
            long nCount = 0; long MyLegsTCV = 0; long MyCount = 0; long MyOrder = 0;
            TeamCustomerViewModel model = new TeamCustomerViewModel();
            model.teamCustomer = _membersService.GetTeamCustomersIDList(1, loggedInUser.UserId);
            for (int i = 0; i < model.teamCustomer.Count; i++)
            {
                nCount = _membersService.GetTeamCustomersCount(2, model.teamCustomer[i].Id);
                model.teamCustomer[i].Total = nCount;
                model.teamCustomer[i].S2 = nCount > 9 ? 9 : nCount;
                model.teamCustomer[i].S3 = nCount > 45 ? 45 : nCount;
                model.teamCustomer[i].G1 = nCount > 75 ? 75 : nCount;
                model.teamCustomer[i].G2 = nCount > 150 ? 150 : nCount;
                model.teamCustomer[i].G3 = nCount > 300 ? 300 : nCount;
                model.teamCustomer[i].P1 = nCount > 750 ? 750 : nCount;
                model.teamCustomer[i].P2 = nCount > 1500 ? 1500 : nCount;
                model.teamCustomer[i].P3 = nCount > 2250 ? 2250 : nCount;
                MyLegsTCV += nCount;
            }
            MyCount = _membersService.GetTeamCustomersCount(2, loggedInUser.UserId);
            model.PersRefTC = MyCount - MyLegsTCV;
            model.TotalRefNetworkTC = MyLegsTCV;
            model.MyCustomers = MyCount;
            model.TotalTC = MyCount;
            model.legs = _membersService.GetLegsDetails(3, loggedInUser.UserId);
            model.LegsCount = model.legs.Count();
            model.MyTV = _membersService.GetMyTV(4, loggedInUser.UserId);
            model.MyPersTV = _membersService.GetMyPersSponTV(5, loggedInUser.UserId);
            if (model.MyTV >= model.MyTV + 20)
            {
                MyOrder = 1;
            }
            model.LegsCount = MyOrder == 0 ? model.LegsCount - 1 : model.LegsCount;
            model.LegsCount = model.LegsCount < 0 ? 0 : model.LegsCount;

            if (MyOrder == 0)
            {
                if (model.legs.Count() == 0)
                {
                    model.QualOrder = "You do not have a personal order to use as your required Qualifying Customer.";
                }
                if (model.legs.Count() > 0)
                {
                    model.QualOrder = "You do not have a personal order to use as your required Qualifying Customer. <br />Therefore, one of your active legs is being substituted instead. This has reduced your Active Leg count by 1.";
                }
            }
            else
            {
                if (model.legs.Count() == 0)
                {
                    model.QualOrder = "Your personal order being used as your required Qualifying Customer.";
                }
                else
                {
                    model.QualOrder = string.Empty;
                }
            }
            return View(model);
        }
        public ActionResult RankHistory()
        {
            RankHistoryViewModel model = new RankHistoryViewModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                model.webContentData = _membersService.GetWebContentData(1, "Members_RankHistory_Content");
                model.rankPromotion = _membersService.GetRankPromotion(2, loggedInUser.UserId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }
        public ActionResult MyContacts()
        {

            return View();
        }
        public ActionResult ContactAdmin()
        {

            return View();
        }

        public ActionResult FoxxCashIssued()
        {

            return View();
        }

        public ActionResult MyFoxxCash()
        {

            return View();
        }
        public ActionResult MyMoreMitoCash()
        {

            return View();
        }
        public ActionResult GetMyCash()
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                int UserId = (int)loggedInUser.UserId;
                var res = _commonService.GetAvailableCommissions(1, UserId);
                status = true;
                return Json(new { status, res }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FoxxCashPending()
        {

            return View();
        }

        public ActionResult Sweeper()
        {

            return View();
        }
        public ActionResult SweeperHistory()
        {

            return View();
        }

        public ActionResult TransferFC()
        {

            return View();
        }

        public ActionResult TransferFCHistory()
        {

            return View();
        }

        public ActionResult MyFoxxCashRC()
        {

            return View();
        }

        public ActionResult FoxxCashDetails()
        {

            return View();
        }
        public ActionResult Resources(long? Id)
        {
            ResourcesViewModel model = new ResourcesViewModel();
            model.resources = _membersService.GetResourcesList(1);
            model.activeId = Id == null ? model.resources[0].Id : (long)Id;
            model.filesResources = _membersService.GetFileResourcesList(2, model.activeId);
            return View(model);
        }
        public ActionResult MobileApp()
        {
            MobileApp model = new MobileApp();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                model.mobileContentData = _membersService.GetMobileContentData(1, "Members_Content_MobileApp");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        public ActionResult MobileAppRecipientData()
        {
            MobileApp model = new MobileApp();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                model.mobileContentData = _membersService.GetMobileContentData(1, "Members_Content_MobileApp_RecipientData");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);

        }

        public ActionResult FoxxNetwork()
        {

            MobileApp model = new MobileApp();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                model.mobileContentData = _membersService.GetMobileContentData(1, "Members_Content_FoxxNetwork.com");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        public ActionResult TMRIS()
        {
            MobileApp model = new MobileApp();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                model.mobileContentData = _membersService.GetMobileContentData(1, "Members_Content_TextMessageRequestInfoSystem");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        public ActionResult MyLeads()
        {
            MyLeadsViewModel model = new MyLeadsViewModel();
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            model.leadsContentValue = _membersService.GetLeadsContentData(1, "Members_Content_MyLeads");
            return View(model);
        }
        public ActionResult MyOrders()
        {
            Orders model = new Orders();
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            model.myOrders = _membersService.GetMyOrdersList(1, loggedInUser.UserId);
            foreach (var order in model.myOrders)
            {
                order.Status = Enum.GetName(typeof(OrderStatus), order.OrderStatusId);
            }
            return View(model);

        }
        public ActionResult OrdersFromPersonals()
        {
            var loggedInUser = GetCacheUserInfo();
            var Orders = _membersService.MyReferralOrders(1, (int)loggedInUser.UserId);
            return View(Orders);
        }
        public ActionResult MyOrdersDetails(long OrderId)
        {
            OrderDetailsModel model = new OrderDetailsModel();
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            model.myOrders = _membersService.GetMyOrdersList(1, loggedInUser.UserId).Where(x => x.OrderId == Convert.ToString(OrderId)).FirstOrDefault();
            model.Items = _membersService.GetMyOrdersDetailsList(2, OrderId);
            model.OrderStatus = Enum.GetName(typeof(OrderStatus), model.myOrders.OrderStatusId);
            model.ShippingStatus = Enum.GetName(typeof(ShippingStatus), model.myOrders.ShippingStatusId);
            model.PaymentMethodStatus = Enum.GetName(typeof(PaymentStatus), model.myOrders.PaymentStatusId);
            return View(model);
        }
        public ActionResult OrderDetail(long OrderId, int OrderOwnerId)
        {
            OrderDetailsModel model = new OrderDetailsModel();

            model.myOrders = _membersService.GetMyOrdersList(1, (long)OrderOwnerId).Where(x => x.OrderId == Convert.ToString(OrderId)).FirstOrDefault();
            model.Items = _membersService.GetMyOrdersDetailsList(2, OrderId);
            model.OrderStatus = Enum.GetName(typeof(OrderStatus), model.myOrders.OrderStatusId);
            model.ShippingStatus = Enum.GetName(typeof(ShippingStatus), model.myOrders.ShippingStatusId);
            model.PaymentMethodStatus = Enum.GetName(typeof(PaymentStatus), model.myOrders.PaymentStatusId);
            return PartialView("_orderDetail", model);
        }
        public ActionResult AdLinks(long? Id)
        {
            AdLinksViewModel model = new AdLinksViewModel();
            var loggedInUser = GetCacheUserInfo();
            string UserName = GetCacheUserInfo().UserName;
            model.CampaignList = _membersService.GetCampaignList(2, loggedInUser.UserId);
            if (Id != null)
            {
                model.CampaignId = Convert.ToString(Id);
                model.usersCampaignPages = _membersService.GetUserWiseCampaignPageList(5, (long)Id);
            }
            CampaignLinkId = model.CampaignId == null ? Convert.ToInt64(model.CampaignList[0].Value) : Convert.ToInt64(model.CampaignId);

            return View(model);
        }
        [HttpPost]
        public ActionResult AdLinks(AdLinksViewModel Objmodel, string btnType)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string UserName = GetCacheUserInfo().UserName;
                var objCampaing = new Campaign()
                {
                    UserId = loggedInUser.UserId,
                    CampaignName = Objmodel.campaign.CampaignName
                };
                switch (btnType)
                {
                    case "Create Campaign":
                        _baseModel = _membersService.InsertUpdateCamoaign(1, objCampaing);
                        break;
                    case "Delete Campaign":
                        objCampaing.UserId = Convert.ToInt64(Objmodel.CampaignId);
                        _baseModel = _membersService.InsertUpdateCamoaign(3, objCampaing);
                        break;
                    default:
                        break;
                }

                if (_baseModel.RowAffected > 0)
                {
                    TempData["SUCCESS_MESSAGE"] = "Campaign has been inserted successfully.";
                }
            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            return RedirectToAction("AdLinks", "Members");
        }

        [HttpPost]
        public ActionResult UpdateCampaign(AdLinksViewModel customer)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _membersService.InsertUpdateTagCamoaign(6, customer);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Campaign has been updated successfully.";
            }
            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult DeleteCampaign(AdLinksViewModel customer)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            _baseModel = _membersService.InsertUpdateTagCamoaign(7, customer);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Campaign has been deleted successfully.";
            }
            return new EmptyResult();
            //return RedirectToAction("AdLinks", "Members", new { Id = Convert.ToInt64(customer.CampaignId) });
        }

        [HttpPost]
        public ActionResult AddCampaign(AdLinksViewModel customer)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            customer.campaign.UserId = loggedInUser.UserId;
            _baseModel = _membersService.InsertUpdateTagCamoaign(8, customer);
            if (_baseModel.RowAffected > 0)
            {
                TempData["SUCCESS_MESSAGE"] = "Campaign has been deleted successfully.";
            }
            return new EmptyResult();
            //return RedirectToAction("AdLinks", "Members", new { Id = Convert.ToInt64(customer.CampaignId) });
        }
        public ActionResult CampaignInfo()
        {
            AdLinksViewModel model = new AdLinksViewModel();
            try
            {
                model.campaignPages = _membersService.GetCampaignPageList(4);
                model.CampId = CampaignLinkId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_CampaignInfoView", model);
        }

        public ActionResult QRCampaignInfo(string Guid, string LinkName, string CampId)
        {
            QRCodeModel model = new QRCodeModel();
            string qrCodeURL = "https://localhost:44388/Members/Code?ID=" + Guid;
            try
            {
                model.QRCodeImagePath = GenerateQRCode(qrCodeURL);
                model.QRCodeText = qrCodeURL;
                model.CampaignId = CampId;
                string notes = "The Written URL will not function without first Clicking the ‘Edit’ button on the previous page in order to create a name for the Link Name field. It is this Link Name that people will type into the form to access your link";
                if (string.IsNullOrEmpty(LinkName) || LinkName.ToLower().Trim() == "customize link name")
                {
                    model.Notes = notes;
                }
                else
                {
                    model.Notes = LinkName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_qrInfoView", model);
        }
        public ActionResult Code(string ID)
        {
            PageRedirection model = new PageRedirection();
            model = _membersService.GetPageRedirection(9, ID);
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            if (model.Id > 0)
            {
                _baseModel = _membersService.UpdateRedirectionHit(10, (long)model.Id);
            }
            return Redirect(model.PageUrl);
        }
        public ActionResult AccessRedirection(SearchCampaignURL model)
        {
            PageRedirection objmodel = new PageRedirection();
            objmodel = _membersService.GetPageSearchRedirection(11, model);
            return Redirect(objmodel.PageUrl);
        }
        public ActionResult Upgrade()
        {

            return View();
        }

        public ActionResult RecurringOrders()
        {
            var loggedInUser = GetCacheUserInfo();
            int UserId = (Int32)loggedInUser.UserId;

            RecurringViewModel recurringViewModel = new RecurringViewModel();
            recurringViewModel.AutoshipList = _membersService.GetAutoshipList(1, UserId);

            return View(recurringViewModel);
        }


        public ActionResult NewAD(int AdId, string CustomerPaymentID, string CustomerAddressId)
        {
            RecurringOrderViewModel recurringOrderViewModel = new RecurringOrderViewModel();
            CreateCustomerProfile ccp = new CreateCustomerProfile();
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            var loggedInUser = GetCacheUserInfo();
            int UserId = (Int32)loggedInUser.UserId;


            if (AdId == 0)
            {
                recurringOrderViewModel.UserId = UserId;
                recurringOrderViewModel.Status = true;

                _model = _membersService.InsertUpdateAutoship(3, recurringOrderViewModel);
                if (_model.InsertedId > 0)
                {
                    AdId = (Int32)_model.InsertedId;
                    return RedirectToAction("NewAD", new { AdId = AdId });
                }
            }
            if (CustomerPaymentID == null)
            {
                if (Session["CustomerPaymentID"] != null)
                {
                    CustomerPaymentID = Session["CustomerPaymentID"].ToString();
                }
            }
            if (CustomerAddressId == null)
            {
                if (Session["CustomerAddressId"] != null)
                {
                    CustomerAddressId = Session["CustomerAddressId"].ToString();
                }
            }
            recurringOrderViewModel.AdId = AdId;
            recurringOrderViewModel = _membersService.GetRecurringOrder(6, recurringOrderViewModel);
            recurringOrderViewModel.ProductListFromNopCom = _membersService.GetProductListFromNopCom(8);
            recurringOrderViewModel.ProductListFoxx = _membersService.GetProductListFoxx(2, AdId);
            if (recurringOrderViewModel.ProductListFoxx.Count > 0)
            {
                ViewBag.Total = recurringOrderViewModel.ProductListFoxx.Sum(x => x.Price);
            }
            else
            {
                ViewBag.Total = 0;
            }

            recurringOrderViewModel.UserId = UserId;
            recurringOrderViewModel.StateList = _commonService.GetAllDropdownlist(8);
            recurringOrderViewModel.AutoshipPeriodList = _commonService.GetAllDropdownlist(9);
            var CustomerProfile = _membersService.GetCustomerProfileId(9, recurringOrderViewModel);
            if (CustomerProfile != null)
            {

                recurringOrderViewModel.CustomerProfileId = CustomerProfile.CustomerProfileId;
            }
            else
            {
                recurringOrderViewModel.CustomerProfileId = "0";
            }

            recurringOrderViewModel.NickNamePaymentMethodList = _membersService.GetAllDropdownlistForPaymentAddressById(4, recurringOrderViewModel.CustomerProfileId);
            recurringOrderViewModel.NickNameShipingAddressList = _membersService.GetAllDropdownlistForPaymentAddressById(5, recurringOrderViewModel.CustomerProfileId);
            if (CustomerPaymentID != null)
            {
                var rr = ccp.GetCustomerProfile(recurringOrderViewModel.CustomerProfileId);
                var response2 = ccp.GetCustomerPaymentProfile(recurringOrderViewModel.CustomerProfileId, CustomerPaymentID);
                if (response2 != null)
                {
                    recurringOrderViewModel.PaymentMethod = response2;
                    recurringOrderViewModel.CustomerPaymentID = CustomerPaymentID;
                    var Name = recurringOrderViewModel.NickNamePaymentMethodList.Where(m => m.Value == CustomerPaymentID).First();
                    {
                        recurringOrderViewModel.PaymentMethod.NickName = Name.Text;
                    }
                }
            }
            if (CustomerAddressId != null)
            {
                var response = ccp.GetCustomerShippingAddressRun(recurringOrderViewModel.CustomerProfileId, CustomerAddressId);
                if (response != null)
                {
                    recurringOrderViewModel.ShippingAddress = response;
                    recurringOrderViewModel.CustomerAddressId = CustomerAddressId;
                    var Name = recurringOrderViewModel.NickNameShipingAddressList.Where(m => m.Value == CustomerAddressId).First();
                    {
                        recurringOrderViewModel.ShippingAddress.NickName = Name.Text;
                    }
                }
            }
            CreateCustomerProfile mm = new CreateCustomerProfile();
            RecurringPaymentMethod model = new RecurringPaymentMethod();
            // mm.CreateNewCustomerProfile("4Pr3w2FE", "45S8QyZ44R96x5md", "chandan.mandal1@met-technologies.com", model);
            //  mm.GetCustomerProfile("4Pr3w2FE", "45S8QyZ44R96x5md", "900622555");
            return View(recurringOrderViewModel);
        }
        [HttpPost]
        public ActionResult AddAutoshipFrequency(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            //************************
            string ddlFreqPeriod = model.AutoshipPeriodId.ToString();
            string StartingOn = model.StartingOn;// "20/07/2020";
            int Quantity = 1;
            var FreqInt = model.OnceEvery;// DateTime.Now.Day; 
            AdminEditorViewModel ModelObj = new AdminEditorViewModel();
            // Dim FreqInt As New DateInterval

            DateTime originalDate = Convert.ToDateTime(StartingOn);
            long Qty = Quantity;
            if (ddlFreqPeriod == "2")
            {
                FreqInt = (FreqInt * 7);
            }
            string dt = Convert.ToDateTime(StartingOn).ToString("MM-dd-yyyy");
            var NextDate = dt.Replace("-", "/");
            List<FreqTableList> ListViewFreqChart = new List<FreqTableList>();

            for (int i = 0; i < Quantity; i++)
            {
                FreqTableList Freqmodel = new FreqTableList();
                Freqmodel.ID = i;
                Freqmodel.Ordinal = i + 1;
                Freqmodel.OrderDate = NextDate;
                ListViewFreqChart.Add(Freqmodel);
                if (ddlFreqPeriod == "2" || ddlFreqPeriod == "1")
                {
                    NextDate = originalDate.AddDays(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddDays(Convert.ToInt32(FreqInt));
                }
                else if (ddlFreqPeriod == "3")
                {
                    NextDate = originalDate.AddMonths(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddMonths(Convert.ToInt32(FreqInt));
                }
            }
            ModelObj.FreqTableList = ListViewFreqChart;


            //*******************************************************
            model.NextOrderDate = NextDate;
            _model = _membersService.InsertUpdateAutoship(10, model);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }
        [HttpPost]
        public JsonResult PopulateAutoshipFrequencyTable(string OnceEvery, string AutoshipPeriodId, string StartingOn)
        {
            string ddlFreqPeriod = AutoshipPeriodId.ToString();
            //string StartingOn = StartingOn;// "20/07/2020";
            int Quantity = 10;
            var FreqInt = Convert.ToInt32(OnceEvery);// DateTime.Now.Day; 
            AdminEditorViewModel ModelObj = new AdminEditorViewModel();
            // Dim FreqInt As New DateInterval

            DateTime originalDate = Convert.ToDateTime(StartingOn);
            long Qty = Quantity;
            if (ddlFreqPeriod == "2")
            {
                FreqInt = (FreqInt * 7);
            }
            string dt = Convert.ToDateTime(StartingOn).ToString("MM-dd-yyyy");
            var NextDate = dt.Replace("-", "/");
            List<FreqTableList> ListViewFreqChart = new List<FreqTableList>();

            for (int i = 0; i < Quantity; i++)
            {
                FreqTableList Freqmodel = new FreqTableList();
                Freqmodel.ID = i;
                Freqmodel.Ordinal = i + 1;
                Freqmodel.OrderDate = NextDate;
                ListViewFreqChart.Add(Freqmodel);
                if (ddlFreqPeriod == "2" || ddlFreqPeriod == "1")
                {
                    NextDate = originalDate.AddDays(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddDays(Convert.ToInt32(FreqInt));
                }
                else if (ddlFreqPeriod == "3")
                {
                    NextDate = originalDate.AddMonths(FreqInt).ToString("MM-dd-yyyy").Replace("-", "/");
                    originalDate = originalDate.AddMonths(Convert.ToInt32(FreqInt));
                }
            }
            ModelObj.FreqTableList = ListViewFreqChart;


            //*******************************************************
            return Json(ListViewFreqChart);
            // return Json(new { data = ListViewFreqChart }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RemoveProductItem(int Id, int AdId)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            ProductListFromNopCom model1 = new ProductListFromNopCom();
            model1.Id = Id;
            _model = _membersService.InsertUpdateAutoshipDirectivesItems(3, 0, model1);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAD", new { AdId = AdId });
        }
        public ActionResult ChangeAutoshipStatus(int AdId, Boolean Status)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            RecurringOrderViewModel recurringOrderModel = new RecurringOrderViewModel();
            recurringOrderModel.AdId = AdId;
            recurringOrderModel.Status = Status;
            _model = _membersService.InsertUpdateAutoship(4, recurringOrderModel);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAD", new { AdId = AdId });
        }
        public ActionResult ChangeUseFoxxCash(int AdId, Boolean Status)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            RecurringOrderViewModel recurringOrderModel = new RecurringOrderViewModel();
            recurringOrderModel.AdId = AdId;
            recurringOrderModel.UseFoxxCash = Status;
            recurringOrderModel.Status = Status;
            _model = _membersService.InsertUpdateAutoship(7, recurringOrderModel);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAD", new { AdId = AdId });
        }
        [HttpPost]
        public ActionResult ChangeNickName(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            _model = _membersService.InsertUpdateAutoship(5, model);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult ChangeProductQty(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            ProductListFromNopCom model1 = new ProductListFromNopCom();
            model1.Qty = model.Qty;
            model1.Id = model.Id;
            _model = _membersService.InsertUpdateAutoshipDirectivesItems(4, model.AdId, model1);
            if (_model.RowAffected > 0)
            {
                // AdId = (Int32)_model.InsertedId;
            }
            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult AddProductsToAutoship(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            ProductListFromNopCom model1 = new ProductListFromNopCom();
            foreach (var Item in model.ProductListFromNopCom)
            {
                if (Item.IsCheck)
                {
                    model1 = Item;
                    model1.Qty = 1;
                    _model = _membersService.InsertUpdateAutoshipDirectivesItems(1, model.AdId, model1);
                }
            }


            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult AddPaymentMethod(RecurringOrderViewModel model)
        {
            CreateCustomerProfile ccp = new CreateCustomerProfile();
            SqlResponseBaseModel _model = new SqlResponseBaseModel();
            StripePaymentGateway spg = new StripePaymentGateway();

            createCustomerProfileResponse response = new createCustomerProfileResponse();
            string CustomerId = "CustomerId-" + model.AdId.ToString();
            string customerProfileId = null;
            string PaymentProfileId = null;
            string ShippingAddressId = null;
            string StripeCustomerId = null;
            string StripeAddressId = null;
            string StripePaymentMethodId = null;


            var CustomerResp = spg.CreateCustomer(model.PaymentMethod);
            if (CustomerResp != null)
            {
                StripeCustomerId = CustomerResp.CustomerId;
                StripePaymentMethodId = CustomerResp.PaymentMethodId;
            }
            var ResponseData = _membersService.InsertUpdateAutoshipPaymentMethod(1, model.AdId, model.PaymentMethod);
            //if(ResponseData!=null)
            //{
            //    StripeCustomerId=ResponseData.I
            //}
            //customerProfileId = CustomerResp.Id;
            //PaymentProfileId = response.customerPaymentProfileIdList[0];
            //ShippingAddressId = response.customerShippingAddressIdList[0];
            // Session["customerProfileId"] = customerProfileId;
            // _model = _membersService.InsertUpdateAnetPaymentShipping(3, 0, customerProfileId, PaymentProfileId, model.PaymentMethod.NickName, false, null, false, model.UserId);


            //var val = spg.CreateNewCustomerProfile(CustomerId, model.PaymentMethod);

            if (model.CustomerProfileId == "0")
            {
                response = ccp.CreateNewCustomerProfile(CustomerId, model.PaymentMethod);
                if (response != null)
                {
                    if (response.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response.messages.message != null)
                        {
                            customerProfileId = response.customerProfileId;
                            PaymentProfileId = response.customerPaymentProfileIdList[0];
                            ShippingAddressId = response.customerShippingAddressIdList[0];
                            Session["customerProfileId"] = customerProfileId;

                            _model = _membersService.InsertUpdateAnetPaymentShipping(3, 0, customerProfileId, PaymentProfileId, model.PaymentMethod.NickName, false, null, false, model.UserId, StripeCustomerId, StripeAddressId);
                        }
                    }
                }
            }
            else
            {
                customerProfileId = model.CustomerProfileId;
                var response1 = ccp.CreateCustomerPaymentProfile(customerProfileId, model.PaymentMethod);
                if (response1 != null)
                {
                    if (response1.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response1.messages.message != null)
                        {
                            PaymentProfileId = response1.customerPaymentProfileId;
                            Session["customerProfileId"] = customerProfileId;

                            _model = _membersService.InsertUpdateAnetPaymentShipping(1, 0, customerProfileId, PaymentProfileId, model.PaymentMethod.NickName, false, null, false, model.UserId, StripeCustomerId, StripeAddressId);

                        }
                    }
                }
            }



            //if (_model.RowAffected > 0)
            //{
            //    // AdId = (Int32)_model.InsertedId;
            //}



            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }


        [HttpPost]
        public ActionResult EditPaymentMethod(RecurringOrderViewModel model)
        {
            CreateCustomerProfile ccp = new CreateCustomerProfile();
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            string CustomerId = "CustomerId-" + model.AdId.ToString();
            string customerProfileId = model.CustomerProfileId;
            string PaymentProfileId = model.CustomerPaymentID;
            string ShippingAddressId = null;
            if (model.CustomerProfileId != "0")
            {
                var ResponseData = _membersService.InsertUpdateAutoshipPaymentMethod(3, model.AdId, model.PaymentMethod);
                var response = ccp.UpdateCustomerPaymentProfile(customerProfileId, PaymentProfileId, model.PaymentMethod);
                if (response != null)
                {
                    if (response.messages.resultCode == messageTypeEnum.Ok)
                    {
                        if (response.messages.message != null)
                        {

                        }
                    }
                }
            }

            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult AddAddressDetails(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            CreateCustomerProfile ccp = new CreateCustomerProfile();
            string customerProfileId = model.CustomerProfileId;// Session["customerProfileId"].ToString(); //"900622555";
            string customerAddressId = null;
            string StripeCustomerId = null;
            string StripeAddressId = null;
            //var response = ccp.CreateCustomerShippingAddress(customerProfileId,model.ShippingAddress);
            _membersService.InsertUpdateAutoshipShipingAddress(2, model.AdId, model.ShippingAddress);
            //if (response.customerAddressId!=null)
            //{
            // customerAddressId = response.customerAddressId;
            _model = _membersService.InsertUpdateAnetPaymentShipping(2, 0, customerProfileId, null, model.ShippingAddress.NickName, false, customerAddressId, false, model.UserId, StripeCustomerId, StripeAddressId);
            //}
            //_model = _membersService.InsertUpdateAutoshipPaymentMethod(1, model.AdId, model1);
            //if (_model.RowAffected > 0)
            //{
            //    // AdId = (Int32)_model.InsertedId;
            //}



            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }

        [HttpPost]
        public ActionResult EditAddressDetails(RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _model = new SqlResponseBaseModel();

            CreateCustomerProfile ccp = new CreateCustomerProfile();
            string customerProfileId = model.CustomerProfileId;// Session["customerProfileId"].ToString(); //"900622555";
            string customerAddressId = model.CustomerAddressId;
            _membersService.InsertUpdateAutoshipShipingAddress(4, model.AdId, model.ShippingAddress);
            var response = ccp.UpdateCustomerShippingAddress(customerProfileId, customerAddressId, model.ShippingAddress);
            if (response != null)
            {

            }
            //_model = _membersService.InsertUpdateAutoshipPaymentMethod(1, model.AdId, model1);
            //if (_model.RowAffected > 0)
            //{
            //    // AdId = (Int32)_model.InsertedId;
            //}



            return RedirectToAction("NewAD", new { AdId = model.AdId });
        }

        public ActionResult ChangePaymentNickName(string CustomerPaymentID, int AdId)
        {
            Session["CustomerPaymentID"] = CustomerPaymentID;
            return RedirectToAction("NewAD", new { AdId = AdId, CustomerPaymentID = CustomerPaymentID });
        }
        public ActionResult ChangeAddressNickName(string CustomerAddressID, int AdId)
        {
            string CustomerPaymentID = null;
            if (Session["CustomerPaymentID"] != null)
            {
                CustomerPaymentID = Session["CustomerPaymentID"].ToString();
            }
            Session["CustomerAddressID"] = CustomerAddressID;
            return RedirectToAction("NewAD", new { AdId = AdId, CustomerPaymentID = CustomerPaymentID, CustomerAddressID = CustomerAddressID });
        }

        public ActionResult NewAD1()
        {

            return View();
        }

        public ActionResult GetPersonalInformation(long InfoId)
        {
            MiscInfoViewModel misc_model = new MiscInfoViewModel();
            try
            {
                //var loggedInUser = GetCacheUserInfo();
                //string UserName = GetCacheUserInfo().UserName;
                //misc_model._personalInfo = _membersService.GetPersonalDetails(4, InfoId);
                //long Level = _membersService.GetLevelInformation(2, InfoId, loggedInUser.UserId);
                //misc_model._questionAnswer = _membersService.GetMQDetails(3, InfoId);
                //misc_model._recurringOrder = _membersService.GetRecurringOrderDetails(5, InfoId);
                //misc_model._processOrder = _membersService.GetProcessOrderDetails(6, InfoId);
                //misc_model._advertisment = _membersService.GetAdvertismentDetails(7, InfoId);
                //misc_model._rankHistory = _membersService.GetRankHistoryDetails(8, InfoId);
                //misc_model._marketingActivity = _membersService.GetMarketingActivityDetails(9, InfoId);
                ////misc_model._mobileAppActivity = _membersService.GetMobileAppActivityDetails(10, InfoId);
                //misc_model._personalInfo.SponserName = UserName;
                //misc_model._personalInfo.Level = Level;


                var loggedInUser = GetCacheUserInfo();
                string UserName = loggedInUser.UserName;
                misc_model._personalInfo = _membersService.GetPersonalDetails(4, InfoId);
                long Level = _membersService.GetLevelInformation(2, InfoId, loggedInUser.UserId);
                misc_model._questionAnswer = _membersService.GetMQDetails(3, InfoId);
                misc_model._recurringOrder = _membersService.GetRecurringOrderDetails(5, InfoId);
                misc_model._processOrder = _membersService.GetProcessOrderDetails(6, InfoId);
                misc_model._advertisment = _membersService.GetAdvertismentDetails(7, InfoId);

                var CountData = _userService.GetPlacementUserCount((int)InfoId, (int)loggedInUser.UserId);
                misc_model._menu.IsPlacementOn = CountData.UsersCount > 0;

                //Temp Logic 
                //misc_model._menu.IsPlacementOn = true;

                //-------------------Rank History Section----------------------------


                UserName = _commonService.GetUserNamebyUsersID(15, Convert.ToInt64(InfoId));
                TempData["TempDataUserId"] = InfoId;

                if (_userService.IsUserInRole(1, UserName, "member") == true)
                {
                    misc_model.MembershipType = "Paid Representative";
                }
                else if (_userService.IsUserInRole(1, UserName, "referringcustomer") == true)
                {
                    misc_model.MembershipType = "Referring Customer";
                }
                else if (_userService.IsUserInRole(1, UserName, "friendoffoxx") == true)
                {
                    misc_model.MembershipType = "Customer";
                }
                misc_model.HighestRankAchieved = _commonService.HighestRank((int)InfoId);// _commonService.RankName(2, _commonService.GetRankID(1, Convert.ToInt64(InfoId)));
                misc_model.QTE = getQteNowForMBO(Convert.ToInt64(InfoId));

                var GetJoinDate = _membersService.GetUserInformation(14, Convert.ToInt64(InfoId));
                if (GetJoinDate != null)
                {
                    misc_model.JoinDate = GetJoinDate.JoinDate;
                }
                misc_model._rankHistory = _membersService.GetRankHistoryDetails(8, InfoId);
                //-------------------Rank History Section----------------------------






                misc_model._marketingActivity = _membersService.GetMarketingActivityDetails(9, InfoId);

                //   misc_model._personalInfo.SponserName = UserName; // commented by Shubham on 15-05-2022
                misc_model._personalInfo.Level = Level;


                misc_model._mobileAppActivity.RecipientsLast72Hours = RecipientsOrActivityCountInTime(InfoId, "72H", "recipient").ToString();
                misc_model._mobileAppActivity.RecipientsLast7Days = RecipientsOrActivityCountInTime(InfoId, "7day", "recipient").ToString();
                misc_model._mobileAppActivity.Recipients8to14day = RecipientsOrActivityCountInTime(InfoId, "8to14day", "recipient").ToString();
                misc_model._mobileAppActivity.Recipinets15to21day = RecipientsOrActivityCountInTime(InfoId, "15to21day", "recipient").ToString();
                misc_model._mobileAppActivity.Recipients22to28day = RecipientsOrActivityCountInTime(InfoId, "22to28day", "recipient").ToString();
                misc_model._mobileAppActivity.RecipientsLife = RecipientsOrActivityCountInTime(InfoId, "", "recipient").ToString();

                misc_model._mobileAppActivity.Activity72h = RecipientsOrActivityCountInTime(InfoId, "72H", "activity").ToString();
                misc_model._mobileAppActivity.Activity7day = RecipientsOrActivityCountInTime(InfoId, "7day", "activity").ToString();
                misc_model._mobileAppActivity.Activity8to14day = RecipientsOrActivityCountInTime(InfoId, "8to14day", "activity").ToString();
                misc_model._mobileAppActivity.Activity15to21day = RecipientsOrActivityCountInTime(InfoId, "15to21day", "activity").ToString();
                misc_model._mobileAppActivity.Activity22to28day = RecipientsOrActivityCountInTime(InfoId, "22to28day", "activity").ToString();
                misc_model._mobileAppActivity.ActivityLife = RecipientsOrActivityCountInTime(InfoId, "", "activity").ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PartialView("_MyInfoView", misc_model);
        }

        public ActionResult Help(string KeyName, string Return)
        {
            //KeyName = "Members_Help_Genealogy";
            string KeyName2 = "Members_Help_Salutation";
            HelpViewModel helpModel = new HelpViewModel();
            if (Return != null)
            {
                helpModel.Return = Return;
            }
            var ContentData = _membersService.GetWebContent(1, KeyName);
            if (ContentData != null)
            {
                helpModel.HelpContent = ContentData.HelpContent;
            }
            var ContentData2 = _membersService.GetWebContent(1, KeyName2);
            if (ContentData2 != null)
            {
                helpModel.HelpContent2 = ContentData2.HelpContent;
            }

            return View(helpModel);
        }

        private string GenerateQRCode(string qrcodeText)
        {
            string folderPath = "~/Images/";
            string imagePath = "~/Images/QrCode.jpg";
            // If the directory doesn't exist then create it.
            if (!Directory.Exists(Server.MapPath(folderPath)))
            {
                Directory.CreateDirectory(Server.MapPath(folderPath));
            }

            var barcodeWriter = new BarcodeWriter();
            barcodeWriter.Format = BarcodeFormat.QR_CODE;
            barcodeWriter.Options = new EncodingOptions() { Height = 150, Width = 150, Margin = 0 };
            var result = barcodeWriter.Write(qrcodeText);

            string barcodePath = Server.MapPath(imagePath);
            var barcodeBitmap = new Bitmap(result);
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(barcodePath, FileMode.Create, FileAccess.ReadWrite))
                {
                    barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }
            }
            return imagePath;
        }


        public string getQteNowForMBO(long PlacementUserID)
        {
            string getQteNowForMBO = "NA";
            UsersQTENowforMBOViewModel modelDetails = new UsersQTENowforMBOViewModel();

            var model = _commonService.GetQteNowForMBODetails(2, PlacementUserID);

            if (model != null)
            {
                if (model.HasQteForNowRecord == false)
                {
                    // SetQteNowForMBO(UsersID)//Need to Modify
                }
            }
            modelDetails = _commonService.GetQteNowForMBODetails(1, PlacementUserID);
            if (modelDetails != null)
            {

                if (modelDetails.QteRankID > 0)
                {


                    getQteNowForMBO = _commonService.RankName(2, modelDetails.QteRankID) + "as of " + modelDetails.AsOfDate;
                }

            }

            return getQteNowForMBO;
        }


        public long RecipientsOrActivityCountInTime(long UsersID, string TimeFrame, string RecipientOrActivity)
        {
            long TheCount = 0;
            string TimeClause = "";
            string TableName = "MobileAppRecipient";
            TableName = RecipientOrActivity.ToLower() == "activity" ? "MobileAppRecipientActivity" : "MobileAppRecipient";
            switch (TimeFrame.ToLower())
            {


                case "72h":
                    TimeClause = "AND (" + TableName + ".CreateDate >= DATEADD(hh, - 72, GETDATE()))";
                    break;
                case "7day":
                    TimeClause = "AND (" + TableName + ".CreateDate >= DATEADD(d, - 7, GETDATE()))";
                    break;
                case "8to14day":
                    TimeClause = "AND (" + TableName + ".CreateDate between DATEADD(d, - 8, GETDATE()) AND  DATEADD(d, - 14, GETDATE()))";
                    break;
                case "15to21day":
                    TimeClause = "AND (" + TableName + ".CreateDate between DATEADD(d, - 15, GETDATE()) AND  DATEADD(d, - 21, GETDATE()))";
                    break;
                case "22to28day":
                    TimeClause = "AND (" + TableName + ".CreateDate between DATEADD(d, - 22, GETDATE()) AND  DATEADD(d, - 28, GETDATE()))";
                    break;
                default:
                    TimeClause = "";
                    break;


            }

            if (RecipientOrActivity.ToLower() == "activity")
            {
                TheCount = _membersService.GetMobileAppActivityDetails(11, UsersID, TimeClause);
            }
            else
            {
                TheCount = _membersService.GetMobileAppActivityDetails(10, UsersID, TimeClause);
            }

            return TheCount;
        }

        [HttpGet]
        public JsonResult GetUserDetails(string u, int MoveUserId)
        {
            if (Request.IsAjaxRequest() && !string.IsNullOrEmpty(u))
            {
                var model = new BroadcastEmailUserRequest()
                {
                    CategoryId = 0,
                    UserName = u
                };
                var loggedInUser = GetCacheUserInfo();
                int userid = Convert.ToInt32(loggedInUser.UserId);
                var userList = _userService.GetTargetUsersforPlacement(userid, u, MoveUserId);//  _userService.GetUserDetailsList(1, model);
                return Json(userList, JsonRequestBehavior.AllowGet);
            }
            return Json("Access Denied", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateUserPlacement(UpdateUserPlacementRequestViewModel request)
        {
            if (Request.IsAjaxRequest())
            {
                string _msg = string.Empty;
                try
                {
                    if (request.SponsorUserId > 0 && request.UserId > 0)
                    {
                        JoinController joinController = new JoinController(_membersService, _commonService, _userService, _adminService);
                        var RowAffected = _userService.UpdateUserPlacement(request);
                        if (RowAffected > 0)
                        {
                            var isCommissionEnabled = _commonService.IsCommissionEnabled();
                            List<TargetUserNameViewModel> users = new List<TargetUserNameViewModel>();

                            List<TargetUserNameViewModel> downlineUsers = _userService.FindAllusersInUplineDownline(request.UserId, 1);
                            if (downlineUsers != null && downlineUsers.Count > 0)
                            {
                                users.AddRange(downlineUsers);
                            }

                            foreach (var item in users)
                            {
                                // delete downline count
                                var delete = _userService.DeleteDownlineCount(1, Convert.ToInt32(item.ID), 0);
                                // re- generate downline count
                                joinController.AddDownlineCountsAfterPlacement(item.ID);
                                // fetch all orders of a user
                                var orderList = _userService.GetAllOrdersByUserId(Convert.ToInt32(item.ID));
                               
                                if (isCommissionEnabled == true)
                                {
                                    foreach (var o in orderList)
                                    {
                                        int OrderID = 0, UniLiverSponsorID = 0, insertNextUnilevelSponsorIDIn = 0;
                                        Rankup rankup = new Rankup();

                                        int UserID = Convert.ToInt32(item.ID);
                                        if (o.OrderId > 0)
                                        {
                                            OrderID = o.OrderId;
                                            //delete active customer meta data
                                            var del = _userService.DeleteDownlineCount(2, Convert.ToInt32(item.ID), OrderID);

                                            // run active customer meta data  
                                            rankup.RunCustomerMetaDataUpline(UserID, OrderID, UniLiverSponsorID, insertNextUnilevelSponsorIDIn);

                                            rankup.RankAdvanceOrderOwner(UserID, OrderID, 0);
                                          //  rankup.RankAdvanceZeroToOne(UserID, OrderID);
                                            rankup.RankAdvanceUplineUsers(UserID, OrderID);

                                        }

                                    }
                                }
                                

                            }
                            if (isCommissionEnabled == true)
                            {
                                //_userService.DownRankUplineReCheck(request.SponsorUserId);
                            }
                            
                            _msg = "Placement has been done successfully.";
                        }
                        else
                        {
                            _msg = "Opps!!! Something went wrong.";
                        }
                    }
                    else
                    {
                        _msg = "Invalid Sponsor User or Mapping User.";
                    }
                }
                catch (Exception ex)
                {
                    _msg = "Error occured :" + ex.Message;
                }


                return Json(_msg);
            }
            return Json("Unauthorized Access");
        }

        [HttpGet]
        public JsonResult IsAdminUser()
        {
            bool status;
            string CurrentRank = string.Empty;
            string HighestRank = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                string IsAdmin = Convert.ToString(Session["IsAdmin"]);
                bool.TryParse(IsAdmin, out status);
                HighestRank = _commonService.HighestRank((int)loggedInUser.UserId) ;// _commonService.RankName(2, _commonService.GetRankID(1, Convert.ToInt64(loggedInUser.UserId)));
                CurrentRank = _userService.GetCurrentRankByUserId(Convert.ToInt32(loggedInUser.UserId)); ;
            }
            catch (Exception ex)
            {
                //throw ex;
                status = false;
            }
            return Json(new { status, HighestRank, CurrentRank }, JsonRequestBehavior.AllowGet);
        }

        #region ServiceCommonFunction
        private bool IsUserQualified(int UserId)
        {
            bool _response = false;
            if (UserId > 0)
            {
                var UserData = _userService.GetUserRankQualifiedDataDetails(UserId);
                if (UserData is object)
                {
                    if (UserData.RankID > 0 && Convert.ToDateTime(UserData.ExpDate) > DateTime.Now)
                    {
                        _response = true;
                    }
                    else if (UserData.RankID < 1 && Convert.ToDateTime(UserData.ExpDate) < DateTime.Now)
                    {
                        _response = true;
                    }
                }
            }
            return _response;
        }

        private bool IsUserEmailOpt(int UserId)
        {
            bool _response = false;
            if (UserId > 0)
            {
                var UserData = _userService.GetUserRankEmailOptDataDetails(UserId);
                if (UserData is object)
                    _response = UserData.EmailOpt;
            }
            return _response;
        }

        private bool IsUserSmsOpt(int UserId)
        {
            bool _response = false;
            if (UserId > 0)
            {
                var UserData = _userService.GetUserRankSmsOptDataDetails(UserId);
                if (UserData is object)
                    _response = UserData.SMSOptIn;
            }
            return _response;
        }

        private UserRankOptDataViewModel UserRankOpt(int UserId)
        {
            var _response = new UserRankOptDataViewModel();
            if (UserId > 0)
            {
                _response = _userService.GetUserRankOptDataDetails(UserId);
            }
            return _response;
        }
        #endregion

        public ActionResult RankManagementReport(int id)
        {
            var data = _membersService.RankManagementReport(id);
            return View(data);
        }

        public ActionResult PeopleHealthProducts()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _commonService.GetWebContent("Front_PeopleHealthProducts.aspx_1");
            if (!string.IsNullOrEmpty(model.Content))
            {
                model.Content = model.Content.Replace("@ViewBag.ControlllerName", "Members");
            }

            return View(model);
        }
        public ActionResult PetHealthProducts()
        {
            ViewBag.ControlllerName = "Members";
            return View();
        }
        public ActionResult UltramitoRestore()
        {
            ViewBag.ControlllerName = "Members";
            return View();
        }
        public ActionResult Rejuvenate()
        {
            ViewBag.ControlllerName = "Members";
            return View();
        }
        public ActionResult Bundle(string type)
        {
            //ViewBag.type = type;
            //ViewBag.ControlllerName = "Members";
            //var loggedInUser = GetCacheUserInfo();
            //long UserId = GetCacheUserInfo().UserId;
            //ViewBag.HasAutoShipOrder = _membersService.HasAnAutoShipOrder(Convert.ToInt32(UserId));
            //List<BundelViewModel> bundles = _commonService.GetBundleItems(type == "pet" ? 2 : 1);
            //return View(bundles);

            var model = new LoggedinUserDetails();
            LoggedinUserDetails loggedInUser = GetCacheUserInfo();
            if (loggedInUser != null)
            {
                model.UserId = loggedInUser.UserId;
                model.UserName = loggedInUser.UserName;
                model.Email = loggedInUser.Email;
                model.Password = loggedInUser.Password;
                model.NopUserId = loggedInUser.NopUserId;
            }

            FoxShopTransferModel data = new FoxShopTransferModel
            {
                Id = model.NopUserId,
                Email = model.Email,
                Password = model.Password,
                UserName = model.UserName,
                WebsiteSponsorId = model.NopUserId,
                IsGuestUser = false
            };


            string parsedString = JsonConvert.SerializeObject(data);

            var encryptedString = Security.EncryptText(parsedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");

            var parsedhtml = HttpUtility.UrlEncode(encryptedString);
            string decryptedString = Security.DecryptText(encryptedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");

            string targetUrl = type == "pet" ? "pet-health-products" : "people-health-products";
            string url = pathValue + targetUrl + "?customerdetail=" + parsedhtml;
            return Redirect(url);
            // return View("UnderConstruction");
        }
        public ActionResult AllCategories()
        {
            ViewBag.ControlllerName = "Members";
            return View();
        }
        public ActionResult GetChangeRoleContent(string ContentKey, string MessageKey)
        {
            string content = string.Empty;
            string Message = string.Empty;
            try
            {
                var model = new HelpViewModel();
                var ContentData = _adminService.GetWebContentById(1, ContentKey, 0);
                if (ContentData != null)
                {
                    model.HelpContent = ContentData.HelpContent;
                    content = ConvertPartialViewToString("_changeUserRole", model);
                    var MsgData = _adminService.GetWebContentById(1, MessageKey, 0);
                    if (MsgData != null)
                    {
                        Message = MsgData.HelpContent;
                    }

                }
            }
            catch (Exception ex)
            {

            }

            return Json(new { data = content, Message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangeUserRole(string TargetRole, Guid CurrentRoleId, Guid NewRoleId)
        {
            bool status = false;
            int responseCode = 0;
            string msg = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                long UserId = GetCacheUserInfo().UserId;

                if (TargetRole == "Member")
                {

                    var st = _membersService.HasQualifyingOrderForRoleChange(Convert.ToInt32(UserId));
                    if (st == false)
                    {
                        string Message = string.Empty;
                        status = true;
                        responseCode = 1;
                        var MsgData = _adminService.GetWebContentById(1, "Member_ChangeRole_RC_to_PR_ErrorMsg", 0);
                        if (MsgData != null)
                        {
                            Message = MsgData.HelpContent;
                        }
                        return Json(new { status, responseCode, Message }, JsonRequestBehavior.AllowGet);
                    }

                }
                var d = _membersService.GetCurrentRole(3, Convert.ToInt32(UserId), CurrentRoleId, NewRoleId);
                if (d != null && d.ResponsCode > 0)
                {
                    responseCode = 2;
                }
                status = true;
                return Json(new { status, responseCode }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OrderNow(OrderViewModel model)
        {
            //"https://localhost:44369/Customer/OrderNow?Id=guest&bundle=oTaSQ3bInbPfSdFdWphzbQmX5QGiDmX4XdCuqkYlknZT3Ya3b8Ky2bEsn1Lvu1e43WR5XB%2bEDhKkzFztkQ7aYkf7G99CIviV3lJyO4diDIetTQgx2CTA5VwiTwgoo0bxCxYnyuKiYeruZQBg1w6wCQ%3d%3d

            // https://localhost:44369/Customer/OrderNow?Id=n1DrQHcuUmtu%2bH3koiFgOmCVtsCB8HJivil4RP4ROfaOPxzWiE4OVdy9sEjndC%2f3KT9Dqpgnv5pitZ0x1GN%2bN7mRkCP04rauaVSCWMKU%2fcl9CQxenF%2f1Rfjmvr7d2UZHxn4U0tVBQSoKCbiWKUkGWSYN5WrpROvjoAqrACSGugN6dfmknVzaIg7IE7w6aqMu6dU0ah26l8%2fKk%2bbBMk1jYqMEhVdnVtJdDD4YWeoUdGGAgy8CE5oUeH0BUClfb0AWF30EGboL9WHZTdAtV%2ftYisKAnYuqYsiHTTFpVt7JHNIsMmICGPzXuBVSm7fUfmNYYfxyvJk3IHkVbedyKjL1w%2beUEXyl29j8Jw4pYgHxLnTu5tMbifOWr8mYUhA3Ml1uyX6H3PS%2bfHtHUxviRw5q4w%3d%3d&bundle=5EF%2fnnioyqKSb6RnLy6Da%2bjgEM3T7nkkacd5oS%2flAlVgrfPUQkEP%2fp%2fg7PQ62hMHvnJWaZMOEm%2bRqDPTBd912lrOhSxT77w%2bAOdxLSndnnQP6QLK02h7HFcUIBGQCKpH8QUhuXN18Z5c%2fgCRUm6YydE8zpOTvZniVV%2fgYB5g8z0k18Ik%2b4yjBdZHs80QFj5N1g26xv2cp8zDuh0cNttftTWi3d0YW%2bwatyX9hsCUdj1y3TrskoqhKaeQw4r9jXQjUorkgELitCII1tltTjjBWXStrmJWCem05xYwg0xl1bkdjSUUeGyurZV75wdhlWIMmHBMbwEYlp93MvHICiIAaY%2bE6KHe8%2fqe6rYG26W3FXdFp2cyX8QcE0RvYzkNmyVB%2fVTZcco%2bL0KGBrJF2uE9XVdHPwS22D7ifY7n2lBKvcTYPY3%2bu6Wg4P26JdIkQ4zp2eBVQT2UFYXTjA%2bINLFvHO7W49GBjSA3dXwUf%2fIdonVwcQQl3PAMwc2d4lVIty9PKzS62v6QAixJF6cAHDoT2tyTHH7PbZTlG0%2bcIuRLZ6wMhOp9GhJv%2bFS12rj9gUg61kNvEpDALVToGMCt7xdj5t5oj2s36hMyCERlfaL2aN4tzbbixyBq9potdPGkLSpSj4KK1lW12454Tx6t2IOVPMsc3mfvPGPBL0WW8KUfZgeRG3Mho60bNIzsAvVea1JUXubxPPfewh3NED%2fIJZawAlpxflTKpE6r5QnrSm8iMBDujPb9ZRh3puRJYPTpXtzvKsauOCo9K%2b4QIE1VCcHqUKjr1tzzmQG8V5Hbf9%2bH3Is%3d


            string pathValue = ConfigurationManager.AppSettings["nopLogUrl"];
            bool status = false;
            string msg = string.Empty;
            try
            {
                if (model != null && model.Items != null && model.Items.Count > 0)
                {
                    var login = new LoggedinUserDetails();
                    LoggedinUserDetails loggedInUser = GetCacheUserInfo();
                    if (loggedInUser != null)
                    {
                        login.UserId = loggedInUser.UserId;
                        login.UserName = loggedInUser.UserName;
                        login.Email = loggedInUser.Email;
                        login.Password = loggedInUser.Password;
                    }

                    string loginString = JsonConvert.SerializeObject(loggedInUser);

                    var encryptedLoginString = Security.EncryptText(loginString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");
                    var parseLoginVal = HttpUtility.UrlEncode(encryptedLoginString);

                    string parsedString = JsonConvert.SerializeObject(model);
                    var encryptedString = Security.EncryptText(parsedString, "KCD4y1om9ZWH1xoSxzFqpUGW8FZ");
                    var parseVal = HttpUtility.UrlEncode(encryptedString);

                    status = true;
                    string RedirectUrl = pathValue + "Customer/OrderNow?Id=" + parseLoginVal + "&bundle=" + parseVal;
                    return Json(new { status, msg, RedirectUrl }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.Message;
            }

            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadUserPaymentMethods()
        {
            bool status = false;
            string msg;
            try
            {
                long UserId = GetCacheUserInfo().UserId;
                var data = _membersService.GetUserPaymentMethods(Convert.ToInt32(UserId));
                string content = ConvertPartialViewToString("_paymentMethodChooseList", data);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadPaymentMethods(int Id)
        {
            bool status = false;
            string msg;
            try
            {
                List<PaymentMethodsViewModel> data = _membersService.GetPaymentMethods(Id);
                if (data != null && data.Count > 0)
                {
                    var d = data.First();
                    status = true;
                    return Json(new { status, data = d }, JsonRequestBehavior.AllowGet);

                }

                return Json(new { status, data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SavePaymentMethod(UserPaymentMethod method)
        {
            bool status = false;
            string msg;
            try
            {
                int UserId = Convert.ToInt32(GetCacheUserInfo().UserId);
                var res = _membersService.SavePaymentMethod(UserId, method);
                return Json(new { status }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadUserPaymentMethodById(int MethodId)
        {
            bool status = false;
            string msg;
            try
            {
                int UserId = Convert.ToInt32(GetCacheUserInfo().UserId);
                var data = _membersService.GetUserPaymentMethods(UserId, MethodId);
                //  var content = ConvertPartialViewToString("_userPaymentMethods", res);
                return Json(new { status, data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadUserCurrentPaymentMethodById(int MethodId)
        {
            bool status = false;
            string msg;
            try
            {
                int UserId = Convert.ToInt32(GetCacheUserInfo().UserId);
                var res = _membersService.GetUserPaymentMethods(UserId, MethodId);
                var content = ConvertPartialViewToString("_userPaymentMethods", res);
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Testimonials()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content36254.aspx");
            return View(model);
        }
        public ActionResult EmailTemplates()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content67884.aspx");
            return View(model);
        }
        public ActionResult SmsTemplates()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content36387.aspx");
            return View(model);
        }
        public ActionResult TextTemplates()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content43246.aspx");
            return View(model);
        }
        public ActionResult CommissionVideo()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content58505.aspx");
            return View(model);
        }
        public ActionResult HealthWellness()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content89095.aspx");
            return View(model);
        }
        public ActionResult PandP()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Members_Content_PandP");
            return View(model);
        }
        public ActionResult CommissionPayOut()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Content34392.aspx");
            return View(model);
        }
        public ActionResult StartCheckList()
        {
            WebContentModel model = new WebContentModel();
            model.Content = _membersService.GetWebPagesContent(1, "Members_Content_GettingStarted");
            return View(model);
        }
        public ActionResult GetMyRankHistory()
        {
          

            bool status = false;
            string msg;
            try
            {
                long UserId = GetCacheUserInfo().UserId;
                List<RankPromotion> ranks = _membersService.GetRankPromotion(2, UserId);
                string content = ConvertPartialViewToString("_rankHistoryView", ranks);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Import()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetCommisions(GetCommissionModel model)
        {
            AjaxResponseViewModel response = new Models.AjaxResponseViewModel();
            try
            {
                long UserId = GetCacheUserInfo().UserId;
                model.UserId = UserId;
                var data = _commissionService.GetMyCommision(model);
                response.data = ConvertPartialViewToString("_myPersonalCommissions", data);
                response.Status = true;
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return new JsonResult { Data = response, MaxJsonLength = Int32.MaxValue, JsonRequestBehavior = JsonRequestBehavior.AllowGet };

        }
        public ActionResult GetCommisionsByOrderId(int OrderId)
        {
            string msg = string.Empty;
            string content = string.Empty;
            GetCommissionModel model = new GetCommissionModel();

            bool status;
            try
            {
                long UserId = GetCacheUserInfo().UserId;
                model.UserId = UserId;
                model.OrderNo = OrderId;
                // model.UserId = UserId;
                model.SearchType = 1;//_myAdminCommisions
                var data = _commissionService.GetCommision(model);
                content = ConvertPartialViewToString("_myCommissionInfo", data);
                status = true;
                return Json(new { status, msg, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                status = false;
                msg = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CompensationPlan()
        {
            return View();
        }

        public ActionResult MyCompensation()
        {
            MyFoxxCashViewModel model = new MyFoxxCashViewModel();

            try
            {

                long UserId = GetCacheUserInfo().UserId;

                model.myFoxxCashViewModelList = _foxxcashService.GetTotals(1, UserId);

                if (model.myFoxxCashViewModelList.Count > 0)
                {
                    if (model.myFoxxCashViewModelList[0].RetVal1 > 0)
                    {
                        model.Earned = model.myFoxxCashViewModelList[0].RetVal1;
                    }
                    if (model.myFoxxCashViewModelList[0].RetVal2 > 0)
                    {
                        model.Swept = model.myFoxxCashViewModelList[0].RetVal2;

                    }
                    model.Balance = (model.Earned - model.Swept);
                }

                //  var data = _commissionService.GetCommision(model);
                // response.data = ConvertPartialViewToString("_commissionView", data);

            }
            catch (Exception ex)
            {
                TempData["ERROR_MESSAGE"] = "Opps something went wrong!";
            }
            //return View(model);
            return View();
        }
        public ActionResult TransferMC()
        {
            return View();
        }
        public ActionResult SearchUser(string Username)
        {
            bool status = false;
            string msg = string.Empty;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                int UserId = (int)loggedInUser.UserId;

                List<TargetUserNameViewModel> res = _commonService.GetUserByUsername(1, Username);
                string content = ConvertPartialViewToString("_transferUserList", res);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TransferCash(TransferCashViewModel model)
        {
            bool status = false;
            string msg = string.Empty;
            int statusNo = 0;
            try
            {
                var loggedInUser = GetCacheUserInfo();
                int UserId = (int)loggedInUser.UserId;

              

                CommissionSUmmaryModel res = _commonService.GetAvailableCommissions(1, UserId);
                if(res != null)
                {
                    if (res.TotalAvailable < model.Amount)
                    {
                        statusNo = 2;
                        return Json(new { status, statusNo }, JsonRequestBehavior.AllowGet);
                    }
                }
                
                model.CurrentUserId = UserId;
                status = _commissionService.InsertTransferCommissions(model);
                statusNo = 1;
                return Json(new { status, statusNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg, statusNo }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TransferHistory()
        {
            return View();
        }
        public ActionResult GetTransferHistory(TransferCashViewModel model)
        {
            bool status = false;
            string msg = string.Empty;
            
            try
            {
                var loggedInUser = GetCacheUserInfo();
                int UserId = (int)loggedInUser.UserId;

                var m = _commonService.TransferHistory(1, UserId);

                string content = ConvertPartialViewToString("_transferHistory", m);
                status = true;
                return Json(new { status, content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult OptOutInUser(string Phone, string OptType, string Text)
        {
            bool status = false;
            string msg = string.Empty;
            int statusCode = 0;
            try
            {
                PlivoOptInOutModel m = new PlivoOptInOutModel();
                m.From = Phone;
                m.To = "moremito";
                m.MessageUUid = "moremito";
                m.MessageIntent = OptType;
                m.SmsText = Text;
                m.OptType = "MOREMITO";



                _membersService.SavePlivoOptInOut(m);
                statusCode = 200;

                status = true;
                return Json(new { status, statusCode }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return Json(new { status, msg }, JsonRequestBehavior.AllowGet);
        }

    }



}

