using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Foxxlegacy.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Foxxlegacy.Web
{
    //[AdminSessionExpireFilter]
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Autofac Dependency Modules Registration
            var builder = new ContainerBuilder();

            //Registering all Services
            builder.RegisterModule(new DependencyRegistrar());

            //After building the Container, lets resolve the container with IDependencyResolver, so calling DependencyResolver.SetResolver method
            var container = builder.Build();

            // Mvc Resolver
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            // Web Api Resolver
            var configuration = GlobalConfiguration.Configuration;
            configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        protected void Session_Start(object src, EventArgs e)
        {
            if (Context.Session != null)
            {
                //if (Context.Session.IsNewSession)//|| Context.Session.Count==0)
                //{
                //    string sCookieHeader = Request.Headers["Cookie"];
                //    if ((null != sCookieHeader) )
                //    {
                //        //if (Request.IsAuthenticated)
                //        FormsAuthentication.SignOut();
                //        Response.Redirect("/Login/Login");
                //    }
                //}
            }

        }
        protected void Session_End(object sender, EventArgs e)
        {

            //HttpContext ctx = HttpContext.Current;
            //ctx.Response.Redirect(@"~/Login/Login");
            //var routeData = new RouteData();
            //routeData.Values["controller"] = "Home";
            //routeData.Values["action"] = "Index";
            //Response.StatusCode = 500;

            //Controller controller = new LoginController();
            //var rc = new RequestContext(new HttpContextWrapper(Context), routeData);
            //controller.Execute(rc);

            //  Session.Clear();


        }
        protected void Application_EndRequest()
        {

            //var context = new HttpContextWrapper(Context);

            //// If we're an ajax request, and doing a 302, then we actually need to do a 401                  

            //if (Context.Response.StatusCode == 302 && context.Request.IsAjaxRequest())
            //{
            //    Context.Response.Clear(); Context.Response.StatusCode = 401;

            //}

        }
    }
}
