﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;

namespace Foxxlegacy.Web.Helpers
{
    public class ExcelImport
    {
        private static string a = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1;TypeGuessRows=0;MaxScanRows=0;ImportMixedTypes=Text\"";
        private static string b = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1;TypeGuessRows=0;MaxScanRows=0;ImportMixedTypes=Text;\"";

        public static DataTable GetDataTable(string strFileName, string filePath)
        {
            DataTable datatable = new DataTable();
            string extension = Path.GetExtension(filePath);
            string conStr, sheetName;
            conStr = string.Empty;
            switch (extension)
            {
                case ".xls": //Excel 97-03
                    conStr = string.Format(a, filePath);
                    break;

                case ".xlsx": //Excel 07
                    conStr = string.Format(b, filePath);
                    break;
            }

            using (OleDbConnection con = new OleDbConnection(conStr))
            {
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = con;
                    con.Close();
                    con.Open();
                    DataTable dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                    con.Close();
                }
            }
            using (OleDbConnection con = new OleDbConnection(conStr))
            {
                using (OleDbCommand cmd = new OleDbCommand())
                {
                    using (OleDbDataAdapter oda = new OleDbDataAdapter())
                    {
                        cmd.CommandText = "SELECT * From [" + sheetName + "]";
                        cmd.Connection = con;
                        con.Open();
                        oda.SelectCommand = cmd;
                        oda.Fill(datatable);
                        con.Close();
                        return datatable;
                    }
                }
            }
        }
    }
}