﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Foxxlegacy.Web.Helpers
{
    public class FoxxAuthorization: AuthorizeAttribute
    {
        private readonly string[] allowedroles;
        public FoxxAuthorization(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            var userId = Convert.ToString(httpContext.Session["UserName"]);
            if (!string.IsNullOrEmpty(userId))
            {
                var isAdmin = Convert.ToBoolean(httpContext.Session["IsAdmin"]);
                if (isAdmin == true)
                {
                    return true;
                }
            }


            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
               new RouteValueDictionary
               {
                    { "controller", "Login" },
                    { "action", "UnAuthorized" }
               });
        }
    }
}