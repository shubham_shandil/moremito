﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Foxxlegacy.Web.Models
{
    public class CreateCustomerProfile
    {
        string ApiLoginID = ConfigurationManager.AppSettings["AuthNetApiLoginID"];
        string ApiTransactionKey = ConfigurationManager.AppSettings["AuthNetApiTransactionKey"];

        public createCustomerProfileResponse CreateNewCustomerProfile( string CustomerId, RecurringPaymentMethod model)
        {
            //Console.WriteLine("Create Customer Profile Sample");

            // set whether to use the sandbox environment, or production enviornment
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var creditCard = new creditCardType
            {
                cardNumber = model.CCNo,
                expirationDate = model.ExpDate,
                cardCode = model.CVV
            };

            var bankAccount = new bankAccountType
            {
                accountNumber = "231323342",
                routingNumber = "000000224",
                accountType = bankAccountTypeEnum.checking,
                echeckType = echeckTypeEnum.WEB,
                nameOnAccount = "test",
                bankName = "Bank Of America"
            };

            // standard api call to retrieve response
            paymentType cc = new paymentType { Item = creditCard };
            paymentType echeck = new paymentType { Item = bankAccount };

            List<customerPaymentProfileType> paymentProfileList = new List<customerPaymentProfileType>();
            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;

            customerPaymentProfileType echeckPaymentProfile = new customerPaymentProfileType();
            echeckPaymentProfile.payment = echeck;

            paymentProfileList.Add(ccPaymentProfile);
            paymentProfileList.Add(echeckPaymentProfile);

            List<customerAddressType> addressInfoList = new List<customerAddressType>();
            customerAddressType homeAddress = new customerAddressType();
            homeAddress.address = model.Address;
            homeAddress.city = model.City;
            homeAddress.zip = model.Zip;
            homeAddress.firstName = model.FirstName;
            homeAddress.lastName = model.LastName;
            homeAddress.phoneNumber = model.Phone;

            customerAddressType officeAddress = new customerAddressType();
            officeAddress.address = "1200 148th AVE NE";
            officeAddress.city = "NorthBend";
            officeAddress.zip = "92101";
            officeAddress.firstName = "Chandan";
            officeAddress.lastName = "Mandal";
            officeAddress.phoneNumber = "9874563211";

            addressInfoList.Add(homeAddress);
            addressInfoList.Add(officeAddress);


            customerProfileType customerProfile = new customerProfileType();
            customerProfile.merchantCustomerId = CustomerId;
            customerProfile.email = model.Email;
            customerProfile.paymentProfiles = paymentProfileList.ToArray();
            customerProfile.shipToList = addressInfoList.ToArray();
            

            var request = new createCustomerProfileRequest { profile = customerProfile, validationMode = validationModeEnum.none };

            // instantiate the controller that will call the service
            var controller = new createCustomerProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            createCustomerProfileResponse response = controller.GetApiResponse();

            // validate response 
          

            return response;
        }

        public createCustomerPaymentProfileResponse CreateCustomerPaymentProfile(string customerProfileId, RecurringPaymentMethod model)
        {
            //Console.WriteLine("Create Customer Payment Profile Sample");

            // set whether to use the sandbox environment, or production enviornment
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;

            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };
            var creditCard = new creditCardType
            {
                cardNumber = model.CCNo,
                expirationDate = model.ExpDate,
                cardCode = model.CVV

            };

            //var bankAccount = new bankAccountType
            //{
            //    accountNumber = "01245524300",
            //    routingNumber = "000000205",
            //    accountType = bankAccountTypeEnum.checking,
            //    echeckType = echeckTypeEnum.WEB,
            //    nameOnAccount = "test",
            //    bankName = "Bank Of America"
            //};

           // paymentType echeck = new paymentType { Item = bankAccount };
            paymentType cc = new paymentType { Item = creditCard };

            var billTo = new customerAddressType
            {
                firstName = model.FirstName,
                lastName = model.LastName,
                address= model.Address,
                city= model.City,
                state= model.State,
                company= model.CompanyName,
                phoneNumber= model.Phone,
                email= model.Email,
                zip= model.Zip
            };
            //customerPaymentProfileType echeckPaymentProfile = new customerPaymentProfileType();
            //echeckPaymentProfile.payment = cc;
            //echeckPaymentProfile.billTo = billTo;

            customerPaymentProfileType ccPaymentProfile = new customerPaymentProfileType();
            ccPaymentProfile.payment = cc;
            ccPaymentProfile.billTo = billTo;

            var request = new createCustomerPaymentProfileRequest
            {
                customerProfileId = customerProfileId,
                paymentProfile = ccPaymentProfile,
                validationMode = validationModeEnum.none
            };

            // instantiate the controller that will call the service
            var controller = new createCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            createCustomerPaymentProfileResponse response = controller.GetApiResponse();

            // validate response 
            if (response != null)
            {
                if (response.messages.resultCode == messageTypeEnum.Ok)
                {
                    if (response.messages.message != null)
                    {
                        Console.WriteLine("Success! Customer Payment Profile ID: " + response.customerPaymentProfileId);
                    }
                }
                else
                {
                    Console.WriteLine("Customer Payment Profile Creation Failed.");
                    Console.WriteLine("Error Code: " + response.messages.message[0].code);
                    Console.WriteLine("Error message: " + response.messages.message[0].text);
                    if (response.messages.message[0].code == "E00039")
                    {
                        Console.WriteLine("Duplicate Payment Profile ID: " + response.customerPaymentProfileId);
                    }
                }
            }
            else
            {
                if (controller.GetErrorResponse().messages.message.Length > 0)
                {
                    Console.WriteLine("Customer Payment Profile Creation Failed.");
                    Console.WriteLine("Error Code: " + response.messages.message[0].code);
                    Console.WriteLine("Error message: " + response.messages.message[0].text);
                }
                else
                {
                    Console.WriteLine("Null Response.");
                }
            }

            return response;

        }

        public ANetApiResponse GetCustomerProfile(string customerProfileId)
        {
            Console.WriteLine("Get Customer Profile sample");

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var request = new getCustomerProfileRequest();
            request.customerProfileId = customerProfileId;

            // instantiate the controller that will call the service
            var controller = new getCustomerProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            //if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            //{
            //    Console.WriteLine(response.messages.message[0].text);
            //    Console.WriteLine("Customer Profile Id: " + response.profile.customerProfileId);

            //    if (response.subscriptionIds != null && response.subscriptionIds.Length > 0)
            //    {
            //        Console.WriteLine("List of subscriptions : ");
            //        for (int i = 0; i < response.subscriptionIds.Length; i++)
            //            Console.WriteLine(response.subscriptionIds[i]);
            //    }

            //}
            //else if (response != null)
            //{
            //    Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
            //                      response.messages.message[0].text);
            //}

            return response;

        }

        public RecurringPaymentMethod GetCustomerPaymentProfile(string customerProfileId,string customerPaymentProfileId)
        {
            //Console.WriteLine("Get Customer Payment Profile sample");
            RecurringPaymentMethod Rpm = new RecurringPaymentMethod();
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var request = new getCustomerPaymentProfileRequest();
            request.customerProfileId = customerProfileId;
            request.customerPaymentProfileId = customerPaymentProfileId;

            // Set this optional property to true to return an unmasked expiration date
            //request.unmaskExpirationDateSpecified = true;
            //request.unmaskExpirationDate = true;


            // instantiate the controller that will call the service
            var controller = new getCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                
              
                if (response.paymentProfile.payment.Item is creditCardMaskedType)
                {
                  
                    if(response.paymentProfile.billTo!=null)
                    {
                        Rpm.FirstName = response.paymentProfile.billTo.firstName;
                        Rpm.LastName = response.paymentProfile.billTo.lastName;
                        Rpm.Address = response.paymentProfile.billTo.address;
                        Rpm.City = response.paymentProfile.billTo.city;
                        Rpm.State = response.paymentProfile.billTo.state;
                        Rpm.Zip = response.paymentProfile.billTo.zip;
                        Rpm.CompanyName = response.paymentProfile.billTo.company;
                        Rpm.Phone = response.paymentProfile.billTo.phoneNumber;
                        Rpm.Email = response.paymentProfile.billTo.email;
                    }                   
                    
                    Rpm.CCNo= (response.paymentProfile.payment.Item as creditCardMaskedType).cardNumber;
                    Rpm.ExpDate= (response.paymentProfile.payment.Item as creditCardMaskedType).expirationDate;
                    Rpm.Type= (response.paymentProfile.payment.Item as creditCardMaskedType).cardType;
                   

                    //Console.WriteLine("Customer Payment Profile Last 4: " + (response.paymentProfile.payment.Item as creditCardMaskedType).cardNumber);
                    //Console.WriteLine("Customer Payment Profile Expiration Date: " + (response.paymentProfile.payment.Item as creditCardMaskedType).expirationDate);

                    //if (response.paymentProfile.subscriptionIds != null && response.paymentProfile.subscriptionIds.Length > 0)
                    //{
                    //    Console.WriteLine("List of subscriptions : ");
                    //    for (int i = 0; i < response.paymentProfile.subscriptionIds.Length; i++)
                    //        Console.WriteLine(response.paymentProfile.subscriptionIds[i]);
                    //}
                }
            }
            //else if (response != null)
            //{
            //    Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
            //                      response.messages.message[0].text);
            //}

            return Rpm;
        }

        public createCustomerShippingAddressResponse  CreateCustomerShippingAddress(string customerProfileId, RecurringShippingAddress model)
        {
           // Console.WriteLine("CreateCustomerShippingAddress Sample");
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            customerAddressType officeAddress = new customerAddressType();
            officeAddress.firstName = model.FirstName;
            officeAddress.lastName = model.LastName;
            officeAddress.address = model.Address;
            officeAddress.city = model.City;
            officeAddress.zip = model.Zip;
            officeAddress.state = model.State;
            officeAddress.company = model.CompanyName;
            officeAddress.email = model.Email;
            officeAddress.phoneNumber = model.Phone;
            officeAddress.country = model.Country;


            var request = new createCustomerShippingAddressRequest
            {
                customerProfileId = customerProfileId,
                address = officeAddress,
            };

            //Prepare Request
            var controller = new createCustomerShippingAddressController(request);
            controller.Execute();

            //Send Request to EndPoint
            createCustomerShippingAddressResponse response = controller.GetApiResponse();
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                if (response != null && response.messages.message != null)
                {
                    Console.WriteLine("Success, customerAddressId : " + response.customerAddressId);
                }
            }
            else if (response != null)
            {
                Console.WriteLine("Error: " + response.messages.message[0].code + "  " + response.messages.message[0].text);
            }
           
            return response;
        }


        public RecurringShippingAddress GetCustomerShippingAddressRun(string customerProfileId,string customerAddressId)
        {
            //Console.WriteLine("Get Customer Shipping Address sample");
            RecurringShippingAddress Rsa = new RecurringShippingAddress();
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            var request = new getCustomerShippingAddressRequest();
            request.customerProfileId = customerProfileId;
            request.customerAddressId = customerAddressId;

            // instantiate the controller that will call the service
            var controller = new getCustomerShippingAddressController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
               
                Rsa.FirstName = response.address.firstName;
                Rsa.LastName = response.address.lastName;
                Rsa.Address = response.address.address;
                Rsa.City = response.address.city;
                Rsa.State = response.address.state;
                Rsa.Zip = response.address.zip;
                Rsa.CompanyName = response.address.company;
                Rsa.Email = response.address.email;
                Rsa.Phone = response.address.phoneNumber;
                Rsa.Country = response.address.country;

                //Console.WriteLine(response.messages.message[0].text);
                //if (response.subscriptionIds != null && response.subscriptionIds.Length > 0)
                //{
                //    Console.WriteLine("List of subscriptions : ");
                //    for (int i = 0; i < response.subscriptionIds.Length; i++)
                //        Console.WriteLine(response.subscriptionIds[i]);
                //}

            }
            else if (response != null)
            {
                Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                                  response.messages.message[0].text);
            }

            return Rsa;
        }


        public  ANetApiResponse UpdateCustomerShippingAddress(string customerProfileID, string customerAddressId, RecurringShippingAddress model)
        {
           // Console.WriteLine("Update customer shipping address sample");

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };

            //var creditCard = new creditCardType
            //{
            //    cardNumber = model.,
            //    expirationDate = "1028"
            //};

            //var paymentType = new paymentType { Item = creditCard };

            var address = new customerAddressExType
            {
                firstName = model.FirstName,
                lastName = model.LastName,
                address = model.Address,
                city = model.City,
                state = model.State,
                zip = model.Zip,
                country = model.Country,
                phoneNumber = model.Phone,
                company = model.CompanyName,
                customerAddressId = customerAddressId
            };

            var request = new updateCustomerShippingAddressRequest();
            request.customerProfileId = customerProfileID;
            request.address = address;


            // instantiate the controller that will call the service
            var controller = new updateCustomerShippingAddressController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                Console.WriteLine(response.messages.message[0].text);
            }
            else if (response != null)
            {
                Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                                  response.messages.message[0].text);
            }

            return response;
        }

        public  ANetApiResponse UpdateCustomerPaymentProfile(string customerProfileId, string customerPaymentProfileId, RecurringPaymentMethod model)
        {
            //Console.WriteLine("Update Customer payment profile sample");

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
            // define the merchant information (authentication / transaction id)
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = ApiLoginID,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = ApiTransactionKey,
            };
            
            creditCardType creditCard = new creditCardType();           
            //creditCard.cardCode = model.CVV;
            //if (model.CCNo != null)
            //{
            //    string CCNo = model.CCNo.Substring(0, 4);
            //    if(CCNo!= "XXXX")
            //    {
            //        creditCard.cardNumber = model.CCNo;
            //    }
            //}
            //if (model.ExpDate!= "XXXX")
            //{
            //    creditCard.expirationDate = model.ExpDate;
            //}
            creditCard.cardNumber = model.CCNo;
            creditCard.expirationDate = model.ExpDate;
            //===========================================================================
            // NOTE:  For updating just the address, not the credit card/payment data 
            //        you can pass the masked values returned from 
            //        GetCustomerPaymentProfile or GetCustomerProfile
            //        E.g.
            //                * literal values shown below
            //===========================================================================
            /*var creditCard = new creditCardType
            {
                cardNumber = "XXXX1111",
                expirationDate = "XXXX"
            };*/

            var paymentType = new paymentType { Item = creditCard };

            var paymentProfile = new customerPaymentProfileExType
            {
                billTo = new customerAddressType
                {
                    // change information as required for billing
                    firstName = model.FirstName,
                    lastName = model.LastName,
                    address = model.Address,
                    city = model.City,
                    state = model.State,
                    zip = model.Zip,                    
                    phoneNumber = model.Phone
                },
                payment = paymentType,
                customerPaymentProfileId = customerPaymentProfileId
            };

            var request = new updateCustomerPaymentProfileRequest();
            request.customerProfileId = customerProfileId;
            request.paymentProfile = paymentProfile;
            request.validationMode = validationModeEnum.none;


            // instantiate the controller that will call the service
            var controller = new updateCustomerPaymentProfileController(request);
            controller.Execute();

            // get the response from the service (errors contained if any)
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok)
            {
                Console.WriteLine(response.messages.message[0].text);
            }
            else if (response != null)
            {
                Console.WriteLine("Error: " + response.messages.message[0].code + "  " +
                                  response.messages.message[0].text);
            }

            return response;
        }

    }
}