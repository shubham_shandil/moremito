﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Web.Models
{
    public class AdminEmailsDataValidationManagement
    {

    }

    public class AdminBroadcastEmailDataValidation
    {
        [Range(1, long.MaxValue, ErrorMessage = "Choose Email Sending Type.")]
        public long EmailSendingTypeId { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "Choose Email Template.")]
        public long EmailTemplateId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name field is mandatory.")]
        public string SpecificUserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name field is mandatory.")]
        public string DownlineUserName { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "Choose User Rank.")]
        public long UserRankId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Subject field is mandatory.")]
        public string EmailSubject { get; set; }

        [AllowHtml]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Body field is mandatory.")]
        public string EmailBody { get; set; }



        public string SearchUserNameOrPhone { get; set; }

        public string ScheduleTime { get; set; }

        public List<SelectListItem> EmailSendingTypeList { get; set; }
        public List<SelectListItem> EmailTemplateTypeList { get; set; }
        public List<SelectListItem> UserRankList { get; set; }
        public List<AdminScheduleBroadcastViewModel> ScheduleList { get; set; }

        public List<GetBroadcastEmailList> GetBroadcastEmailList { get; set; }


        public BroadcastReportsViewModel BroadcastReportsViewModel { get; set; }

        public List<SentEmailReport> SentEmailReport { get; set; }
        public AdminBroadcastEmailDataValidation()
        {
            EmailSendingTypeList = new List<SelectListItem>();
            EmailTemplateTypeList = new List<SelectListItem>();
            UserRankList = new List<SelectListItem>();
            ScheduleList = new List<AdminScheduleBroadcastViewModel>();
            GetBroadcastEmailList = new List<GetBroadcastEmailList>();
            BroadcastReportsViewModel = new BroadcastReportsViewModel();
            SentEmailReport = new List<SentEmailReport>();
        }
    }

    public class AdminBroadcastSmsDataValidation
    {
        [Range(1, long.MaxValue, ErrorMessage = "Choose SMS Sending Type.")]
        public long SmsSendingTypeId { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "Choose SMS Template.")]
        public long SmsTemplateId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name field is mandatory.")]
        public string SpecificUserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name field is mandatory.")]
        public string DownlineUserName { get; set; }

        [Range(1, long.MaxValue, ErrorMessage = "Choose User Rank.")]
        public long UserRankId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Sms Subject field is mandatory.")]
        public string SmsSubject { get; set; }

        [AllowHtml]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Sms Body field is mandatory.")]
        public string SmsBody { get; set; }


        public string SearchUserNameOrPhone { get; set; }

        public string ScheduleTime { get; set; }
        public List<SelectListItem> SmsSendingTypeList { get; set; }
        public List<SelectListItem> SmsTemplateTypeList { get; set; }
        public List<SelectListItem> UserRankList { get; set; }
        public List<AdminScheduleBroadcastViewModel> ScheduleList { get; set; }

        public List<GetBroadcastSMSList> GetBroadcastSMSList { get; set; }
        public BroadcastReportsViewModel BroadcastReportsViewModel { get; set; }

        public List<SentEmailReport> SentEmailReport { get; set; }
        public AdminBroadcastSmsDataValidation()
        {
            SmsSendingTypeList = new List<SelectListItem>();
            SmsTemplateTypeList = new List<SelectListItem>();
            UserRankList = new List<SelectListItem>();
            ScheduleList = new List<AdminScheduleBroadcastViewModel>();
            GetBroadcastSMSList = new List<GetBroadcastSMSList>();
            BroadcastReportsViewModel = new BroadcastReportsViewModel();
            SentEmailReport = new List<SentEmailReport>();
        }
    }


   
}