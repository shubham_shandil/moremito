﻿using Foxxlegacy.Services.Commission;
using Foxxlegacy.Services.Common;
using Foxxlegacy.Services.Rankup;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Configuration;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Models
{
    public class Rankup
    {
        readonly string GoldCommissionUserName = ConfigurationManager.AppSettings["GoldCommissionUserName"];
        readonly string PoolCommissionUserName = ConfigurationManager.AppSettings["PoolCommissionUserName"];

        #region Fields
        //private readonly IRankupService _rankupService;

        string smtpHost = WebConfigurationManager.AppSettings["EmailsmtpHost"];
        string UserName = WebConfigurationManager.AppSettings["EmailUserName"];
        string Password = WebConfigurationManager.AppSettings["EmailPassword"];
        int Port = Convert.ToInt32(WebConfigurationManager.AppSettings["EmailPort"]);
        Boolean EnableSsl = Convert.ToBoolean(WebConfigurationManager.AppSettings["EmailEnableSsl"]);
        #endregion

        #region CTor
        //public Rankup
        //    (
        //    IRankupService rankupService

        //    )
        //{
        //    this._rankupService = rankupService;
        //}

        //public Rankup(IRankupService rankupService)
        //{
        //    this._rankupService = rankupService;
        //}

        public Rankup()
        {
        }


        #endregion
        public long test()
        {
            var _rankupService1 = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            long a = 0;
            RanksDataViewModel RanksData = new RanksDataViewModel();
            RanksData = _rankupService1.GetRanksData(5, 4);

            return a;
        }
        public string RankAdvanceOrderOwner(int UserID, int OrderID, int isSponsorship)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            string Response = null;
            String RankCheckStatusID = "0", SendingLevel = "0";
            int CheckForLevel = 0;
            int CompareCountLegs = 0, PerLegCappedCustomerCount = 0, CappedCustomerCount = 0, LegsWithRank3Rank4 = 0,
                      CheckRankLegTotal = 0, NoOfRankingLeg = 0;
            RanksDataViewModel RanksData = new RanksDataViewModel();
            int RankId = _rankupService.GetRankByUser(1, UserID);
            if (RankId == 12 || RankId == 0) //Users with rank 0 will be updated to rank 1 from passCheckup Method
            {
                return Response;
            }
            if (UserID == 1)
            {
                return Response;
            }
            CheckForLevel = RankId;

            //if (CheckForLevel == 1)
            //{
            //    insertUsersQualifyingOrders(UserID, OrderID, RankId);
            //}

            CheckForLevel = CheckForLevel + 1;
            SendingLevel = CheckForLevel.ToString();
            if (CheckForLevel == 4)
            {
                SendingLevel = "4a";
            }
            //if (SendingLevel == "1")
            //{
            //    RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
            //    if (RankCheckStatusID == "0")
            //    {
            //        //' that means it is failed
            //        return Response;
            //    }
            //}
            if (SendingLevel == "2" || RankCheckStatusID == "2")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "3" || RankCheckStatusID == "3")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "4a" || RankCheckStatusID == "4")
            {
                RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    SendingLevel = "4b";
                    RankCheckStatusID = RankCheckPeople(UserID, OrderID, SendingLevel);
                    if (RankCheckStatusID == "0")
                    {
                        return Response;
                    }

                }
            }
            if (SendingLevel == "5" || RankCheckStatusID == "5")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "6" || RankCheckStatusID == "6")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }

            if (SendingLevel == "7" || RankCheckStatusID == "7")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "8" || RankCheckStatusID == "8")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "9" || RankCheckStatusID == "9")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "10" || RankCheckStatusID == "10")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }
            if (SendingLevel == "11" || RankCheckStatusID == "11")
            {
                RanksData = _rankupService.GetRanksData(5, Convert.ToInt32(SendingLevel));
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                    PerLegCappedCustomerCount = RanksData.PerLegCappedCustomerCount;
                    CappedCustomerCount = RanksData.CappedCustomerCount;
                    LegsWithRank3Rank4 = RanksData.LegsWithRank3Rank4;
                    CheckRankLegTotal = RanksData.CheckRankLegTotal;
                    NoOfRankingLeg = RanksData.NoOfRankingLeg;
                }
                int RankCheckStatus = CheckStep_Five_To_Eleven(UserID, OrderID, PerLegCappedCustomerCount, CappedCustomerCount,
                    LegsWithRank3Rank4, CheckRankLegTotal, NoOfRankingLeg, Convert.ToInt32(SendingLevel));

                RankCheckStatusID = RankCheckStatus.ToString();
                if (RankCheckStatusID == "0")
                {
                    //' that means it is failed
                    return Response;
                }
            }

            if (isSponsorship == 0)
            {
                AssignGold_On_OrderOwner(OrderID);
            }



            return Response;
        }

        public int RankAdvanceUplineUsers(int UserID, int OrderID)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int response = 0;
            response = _rankupService.RankUpUplineUsers(1, UserID, OrderID);
            return response;
        }

        public long insertUsersQualifyingOrders(int UserID, int OrderID, int RankID, string RankType = null)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int response = 0;
            response = _rankupService.InsertUsersQualifyingOrders(2, UserID, OrderID, RankID);

            return response;
        }

        public string RankCheckPeople(int UserId, int OrderID, string CurrLevel)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int ActiveLegCount = 0, RankID = 0, CompareCountLegs = 0;
            string response = "0";
            RanksDataViewModel RanksData = new RanksDataViewModel();
            if (CurrLevel == "1" || CurrLevel == "2" || CurrLevel == "3" || CurrLevel == "4a")
            {
                ActiveLegCount = _rankupService.InsertUsersQualifyingOrders(3, UserId, OrderID, RankID);
            }
            else if (CurrLevel == "4b")
            {
                //ActiveLegCount = _rankupService.InsertUsersQualifyingOrders(4, UserId, OrderID, RankID);
                ActiveLegCount = _rankupService.InsertUsersQualifyingOrders(12, UserId, OrderID, RankID);
            }

            if (CurrLevel == "1")
            {
                RanksData = _rankupService.GetRanksData(5, 1);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount >= CompareCountLegs)
                {
                    ActiveLegCount = Convert.ToInt32(CurrLevel) + 1;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "2")
            {
                RanksData = _rankupService.GetRanksData(5, 2);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount >= CompareCountLegs)
                {
                    ActiveLegCount = Convert.ToInt32(CurrLevel) + 1;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "3")
            {
                RanksData = _rankupService.GetRanksData(5, 3);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                if (ActiveLegCount >= CompareCountLegs)
                {
                    ActiveLegCount = Convert.ToInt32(CurrLevel) + 1;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "4a")
            {
                RanksData = _rankupService.GetRanksData(5, 4);
                if (RanksData != null)
                {
                    CompareCountLegs = RanksData.CompareCountLegs;
                }
                // CompareCountLegs = _rankupService.GetRanksData(5, 2);
                //if (ActiveLegCount >= 11)
                if (ActiveLegCount >= CompareCountLegs)
                {
                    ActiveLegCount = 4;
                }
                else
                {
                    ActiveLegCount = 0;
                }
            }
            if (CurrLevel == "4b")
            {
                //RanksData = _rankupService.GetRanksData(5, 4);
                //if (RanksData != null)
                //{
                //    CompareCountLegs = RanksData.CompareCountLegs;
                //}
                //if (ActiveLegCount >= CompareCountLegs)
                //{
                //    ActiveLegCount = 4;
                //}
                //else
                //{
                //    ActiveLegCount = 0;
                //}

                if (ActiveLegCount > 0)
                {
                    ActiveLegCount = 4;
                }
            }

            DateTime QTE_ExpDate = System.DateTime.Now,
         QTE_FailDate = System.DateTime.Now,
         FailureMessagedDate = System.DateTime.Now,
         QTE_DemotionDate = System.DateTime.Now,
         SetQualTillDate = System.DateTime.Now;

            string RankType = string.Empty;

            if (CurrLevel == "4a" || CurrLevel == "4b")
            {
                RankType = CurrLevel;
                CurrLevel = "4";
            }
            if (ActiveLegCount == 0)
            {

                _rankupService.InsertUsersQTERank(1, UserId, Convert.ToInt32(CurrLevel), QTE_ExpDate, QTE_FailDate, FailureMessagedDate, QTE_DemotionDate);
            }
            else
            {
                //if (CurrLevel == "4a" && ActiveLegCount == 4)
                //{
                //    CurrLevel = "5";
                //}

                //else if (CurrLevel == "4b" && ActiveLegCount == 4)
                //{
                //    CurrLevel = "5";
                //}

                insertUsersQualifyingOrders(UserId, OrderID, Convert.ToInt32(CurrLevel), RankType);
            }

            response = ActiveLegCount.ToString();
            return response;

        }

        public int CheckStep_Five_To_Eleven(int UserID, int OrderID, int PerLegCappedCustomerCount, int CappedCustomerCount, int LegsWithRank3Rank4, int CheckRankLegTotal, int NoOfRankingLeg, int SendingLevel)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            bool IsClearRank = false;
            var model = new CheckStepFiveToElevenRequestViewModel
            {
                UserId = UserID,
                PerLegCappedCustomerCount = PerLegCappedCustomerCount,
                LegsWithRank3Rank4 = LegsWithRank3Rank4,
                CheckRankLegTotal = CheckRankLegTotal
            };
            var CountData = _rankupService.CheckStepFiveToEleven(1, model);

            if (CountData.Count > 0)
            {
                foreach (var userCount in CountData)
                {
                    if (userCount.CappedCustomerCount >= CappedCustomerCount && userCount.UsersInLegCount >= NoOfRankingLeg)
                    {
                        IsClearRank = true;
                    }
                }
            }

            if (IsClearRank)
            {
                int insertedId = _rankupService.InsertUsersQualifyingOrders(2, UserID, OrderID, SendingLevel);
                return SendingLevel + 1;
            }
            else
            {
                return 0;
            }
        }

        public void AssignGold_On_OrderOwner(int OrderId)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            if (OrderId > 0)
            {
                var OrderRequestModel = new OrderUserIdRequestViewModel { OrderId = OrderId };
                int UserId = _rankupService.GetOrderUserId(OrderRequestModel);

                if (UserId > 0)
                {
                    int UnilevelSponsorIDFirst = SetGoldOne_On_UsersID(UserId, 6);
                    if (UnilevelSponsorIDFirst > 0)
                    {
                        Update_User_Codes(UserId, "Gold1", UnilevelSponsorIDFirst);
                        if (UserId > 0)
                        {
                            int UnilevelSponsorID2A = SetGoldOne_On_UsersID(UserId, 7);
                            if (UnilevelSponsorID2A > 0)
                            {
                                Update_User_Codes(UserId, "Gold2A", UnilevelSponsorID2A);
                                if (UnilevelSponsorID2A > 0)
                                {
                                    int UnilevelSponsorID2B = SetGoldOne_On_UsersID(UnilevelSponsorID2A, 7);
                                    Update_User_Codes(UserId, "Gold2B", UnilevelSponsorID2B);
                                }
                            }
                        }
                    }
                }


            }
        }

        public int SetGoldOne_On_UsersID(int UserId, int CheckRankID)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int ReturnUniLevelSponsorID = 0;
            try
            {
                var model = new GoldRankupSponsorIdReuestViewModel { UserId = UserId, CheckRankId = CheckRankID };
                ReturnUniLevelSponsorID = _rankupService.GetGoldRankupSponsorId(model);
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return ReturnUniLevelSponsorID;
        }

        public void Update_User_Codes(int UsersID, string GoldLevel, int UnilevelSponsorID)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            var model = new UpdateGoldRankUserReuestViewModel { UserId = UsersID, GoldLevel = GoldLevel, SponsorID = UnilevelSponsorID };
            _rankupService.UpdateGoldUserRankup(model);
        }

        public int RunCustomerMetaDataUpline(int UserID, int OrderID, int UniLiverSponsorID, int insertNextUnilevelSponsorIDIn)
        {
            int Response = 0, RetailOnly = 0, NextUnilevelSponsorID = 0, UniLevelSponsorID = 0, pcCount = 0, insertNextUnilevelSponsorID = 0;
            string PersonalCustomer = null;
            bool PersonalCustomerBool = false;
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            if (UniLiverSponsorID == 0)
            {
                UserDetails = _userService.GetUserDetails(19, "", UserID);
            }
            else
            {
                UserDetails = _userService.GetUserDetails(19, "", UniLiverSponsorID);
            }
            if (UserDetails != null)
            {
                //RetailOnly = UserDetails.RetailOnly;
                //NextUnilevelSponsorID = UserDetails.UniLevelSponsorID;

                if (UniLiverSponsorID == 0)
                {
                    UniLevelSponsorID = UserID;
                    PersonalCustomer = UserDetails.RetailOnly.ToString();
                    NextUnilevelSponsorID = UserDetails.UniLevelSponsorID;
                }
                else if (UserID == UniLiverSponsorID)
                {
                    UniLevelSponsorID = UserDetails.UniLevelSponsorID;
                    PersonalCustomer = GetRetailOnlySecondRow(insertNextUnilevelSponsorIDIn);
                    NextUnilevelSponsorID = GetNextUnilevelSponsorID(UserDetails.UniLevelSponsorID);
                }
                else
                {
                    UniLevelSponsorID = UserDetails.UniLevelSponsorID;
                    PersonalCustomer = UserDetails.RetailOnly.ToString();
                    NextUnilevelSponsorID = GetNextUnilevelSponsorID(UserDetails.UniLevelSponsorID);
                }

                if (PersonalCustomer == "True")
                {
                    PersonalCustomer = "False";
                    PersonalCustomerBool = false;
                }
                else
                {
                    PersonalCustomer = "True";
                    PersonalCustomerBool = true;
                }



                pcCount = _userService.ActiveCustomerMetaData(2, UserID, OrderID, UniLevelSponsorID, Convert.ToBoolean(PersonalCustomerBool), insertNextUnilevelSponsorIDIn);
                if (pcCount > 0)
                {
                    PersonalCustomer = "0";
                    PersonalCustomerBool = false;
                }
                if (NextUnilevelSponsorID == 0)
                {
                    insertNextUnilevelSponsorID = 1;
                }
                else
                {
                    insertNextUnilevelSponsorID = NextUnilevelSponsorID;
                }
                //Insert Customer MetaData
                int RowCount = _userService.ActiveCustomerMetaData(1, UserID, OrderID, UniLevelSponsorID, Convert.ToBoolean(PersonalCustomerBool), insertNextUnilevelSponsorID);
                if (NextUnilevelSponsorID > 0)
                {
                    RunCustomerMetaDataUpline(UserID, OrderID, UniLevelSponsorID, insertNextUnilevelSponsorID);
                }

            }


            return Response;
        }
        public string GetRetailOnlySecondRow(int UserID)
        {
            string response = "0";
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            UserDetails = _userService.GetUserDetails(8, "", UserID);
            if (UserDetails != null)
            {
                response = UserDetails.RetailOnly.ToString();
            }
            return response;
        }
        public int GetNextUnilevelSponsorID(int UniLevelSponsorID)
        {
            int Response = 0;
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            UserDetails = _userService.GetUserDetails(8, "", UniLevelSponsorID);
            if (UserDetails != null)
            {
                Response = UserDetails.UniLevelSponsorID;
            }
            return Response;
        }

        public int MoreMito_ovcp_OrderVerifyCompPlan(int OrderId, int UserId, bool JoinOrder)
        {
            int Response = 0;
            int isSponsorship = 0;
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.OrderId = OrderId;
            model.UserId = UserId;
            UpdateOrdersStatus(OrderId, "Comp Plan");
            //DoGrandfathering(OrderId, UserId);
            RankAdvanceOrderOwner(UserId, OrderId, isSponsorship);
       //     RankAdvanceZeroToOne(UserId, OrderId);
            RankAdvanceUplineUsers(UserId, OrderId);
            DoCommission(model);
            UpdateOrdersStatus(OrderId, "Pending Shipping");
            UpdateOrdersVerifyAndCompPlan(OrderId, true);
            return Response;
        }

        public void RankAdvanceZeroToOne(int userId, int orderId)
        {
            PassUpCheck(userId, orderId, true);
        }
        private int UpdateRankZeroToOne(int userId, int orderId)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int response = 0;
            response = _rankupService.RankUpZeroToOneUsers(1, userId, orderId, 0);
            return response;
        }

        public long DoCommission(DoCommissionViewModel model)
        {

            long UserId = 0;
            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            Rankup rankup = new Rankup();
            if (model == null)
            {
                model = new DoCommissionViewModel();
                model.OrderId = 0;
            }
            if (model.UserId > 0)
            {
                //UserId = _commonService.GetUserIDByNopCustomerID(5, model.UserId);
                //if (UserId > 0)
                //{
                //    model.UserId = (Int32)UserId;
                //}
             
                //model.Id = _commissionService.UserCommissionDistributionGenerateRank(1, model);
                PassUpCheck(model.UserId, model.OrderId, false);
                model.Id = _commissionService.UserCommissionDistributionGenerate(1, model);
                SilverCommission(model.UserId, model.OrderId);
                GoldCommission(model.UserId, model.OrderId);
                PlatinumCommission(model.UserId, model.OrderId);
                PoolCommission(model.UserId, model.OrderId);
                Retail(model.OrderId);


            }

            return UserId;

        }

        public void UpdateOrdersStatus(int OrderId, string Status)
        {
            //  int Pending = 10, Processing = 20, Complete = 30, Cancelled = 40;
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            int Response = _userService.ActiveCustomerMetaData(3, 0, OrderId, 0, Convert.ToBoolean(0), 0);


        }

        public void DoGrandfathering(int OrderId, int UserId)
        {
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));

            int RetVal = 0, RetVal2 = 0, Val = 0;
            RetVal = _userService.ActiveCustomerMetaData(4, UserId, OrderId, 0, Convert.ToBoolean(0), 0);
            if (RetVal > 0)
            {
                Val = _userService.ActiveCustomerMetaData(5, UserId, OrderId, 0, Convert.ToBoolean(0), 0);
            }

            RetVal2 = _userService.ActiveCustomerMetaData(6, UserId, OrderId, 0, Convert.ToBoolean(0), 0);
            if (RetVal2 > 0)
            {
                Val = _userService.ActiveCustomerMetaData(7, UserId, OrderId, 0, Convert.ToBoolean(0), 0);
            }
        }

        public void UpdateOrdersVerifyAndCompPlan(int OrderId, bool Status)
        {
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            int Response = _userService.ActiveCustomerMetaData(3, 0, OrderId, 0, Convert.ToBoolean(0), 0);

        }
        public void SendTheMailQue()
        {
            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            EmailViewModel model = new EmailViewModel();
            KillOldEmailQue();
            int EmailQueID = 0;
            //Get email details
            model = _commonService.SetEmailSubjectAndBody(2, 0);
            if (model != null)
            {
                SendIT(model, EmailQueID);
            }
            EmailSentFlag(EmailQueID);

        }
        public void KillOldEmailQue()
        {
            UserDetailsViewModel UserDetails = new UserDetailsViewModel();
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            int Response = _userService.ActiveCustomerMetaData(8, 0, 0, 0, Convert.ToBoolean(0), 0);

        }

        public int SendIT(EmailViewModel model, int EmailQueID)
        {
            int Response = 0;

            //var apiKey = Environment.GetEnvironmentVariable("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");

            //var client = new SendGridClient(apiKey);
            //var from = new EmailAddress(model.FromEmailAddress, "Example User");
            //var subject = model.Subject;
            //var to = new EmailAddress(model.ToEmailAddress, "Example User");
            //var plainTextContent = "and easy to do anywhere, even with C#";
            var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            //var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            //var response =  client.SendEmailAsync(msg);

            using (MailMessage message = new MailMessage())
            {

                message.To.Add(new MailAddress(model.ToEmailAddress.Trim().ToString()));




                message.Subject = model.Subject;
                message.Body = model.Body;
                message.IsBodyHtml = true;
                message.From = new MailAddress(model.FromEmailAddress);

                SmtpClient client = new SmtpClient();
                client.Host = smtpHost;
                client.Port = Port;
                client.EnableSsl = EnableSsl;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(UserName, Password);
                client.Timeout = 100000;
                try
                {
                    client.Send(message);

                }
                catch (Exception ex)
                {
                    // logger.ErrorException("Email Error: " + ex.ToString(), ex);
                }
            }
            return Response;
        }

        public void EmailSentFlag(int EmailQueID)
        {
            var _userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
            int Response = _userService.ActiveCustomerMetaData(9, EmailQueID, 0, 0, Convert.ToBoolean(0), 0);

        }

        public void PassUpCheck(int UserId, int orderId, bool IsZeroToOneQualify)
        {

            //int orderId = 101; // Used a dummy value for order id
            OrderOwner orderOwner = new OrderOwner(); // write logic to determine the order owner            
            int MaxToQualify, QualCount = 0; // intialize the variables
            orderOwner.OwnerId = UserId; //2017

            if (OrderOwner_Is_A__RC_PR(orderOwner) == true)
            {
                QualifyThisRep(orderOwner, orderId, IsZeroToOneQualify);   // Qualify the order owner 
                MaxToQualify = 1;                      // OrderOwner was a RC-PR, at most one more qualify
            }
            else
            {
                MaxToQualify = 2;           // OrderOwner was a Customer up to 2 qualify
            }
            OrderOwner NextOrderOwner = orderOwner;
            while (QualCount < MaxToQualify) // loop through
            {
                var foundGuy = FindTheNextUpLine_RC_PR_Above(NextOrderOwner);
                if (IsThisUserQualified(foundGuy, orderOwner.OwnerId) == true)
                {
                    if (CurrentlyQualified_ByThisOrderOwner(foundGuy, orderOwner) == true)
                    {
                        QualifyThisRep(foundGuy, orderId, IsZeroToOneQualify);
                        QualCount++;
                    }                   
                    else
                    {
                        QualCount++;
                    }
                }
                else
                {
                    QualifyThisRep(foundGuy, orderId, IsZeroToOneQualify);
                    QualCount++;
                }
                NextOrderOwner = foundGuy;
            }

        }

        public bool OrderOwner_Is_A__RC_PR(OrderOwner orderOwner)
        {
            // write logic here to determine if FoundGuy is a RC-PR  or not  
            //if user is RC - PR then return true
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int Response = _rankupService.IsOrderOwnerRC_PR(6, orderOwner.OwnerId);
            if (Response > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// Qualify the order owner
        /// </summary>
        /// <param name="foundGuy"></param>
        /// <param name="orderId"></param>
        public void QualifyThisRep(OrderOwner foundGuy, int orderId, bool IsZeroToOneQualify)
        {
            if (IsZeroToOneQualify == true)
            {
                UpdateRankZeroToOne(foundGuy.OwnerId, orderId);

            }
            else
            {
                //int UserID = 0, OrderID = 0, RankID = 0;
                int UserID = 0, OrderID = orderId, RankID = 1;
                DateTime expirationDate = DateTime.Now.AddDays(35);
                //insertUsersQualifyingOrders(foundGuy.OwnerId, OrderID, RankID);
                QualifyUsers(foundGuy.OwnerId, OrderID, RankID);
                //store the data in table
                // Use order id and expiration date to write  a new qualification’s record for the indicated rep (FoundGuy)
            }

        }
        private int QualifyUsers(int UserID, int OrderID, int RankID, string RankType = null)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int response = 0;
            response = _rankupService.InsertUsersQualifyingOrders(13, UserID, OrderID, RankID);

            return response;
        }
        /// <summary>
        /// Find next up line RC PR 
        /// </summary>
        /// <param name="orderOwner"></param>
        /// <returns></returns>
        public OrderOwner FindTheNextUpLine_RC_PR_Above(OrderOwner orderOwner)
        {
            OrderOwner personAbove = new OrderOwner();// write logic here -	Searches up line beginning with the “person above” the indicated rep for the next RC-PR
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int Response = _rankupService.FindTheNextUpLineRC_PR_Above(9, orderOwner.OwnerId);
            if (Response > 0)
            {
                personAbove.OwnerId = Response;
            }
            else
            {
                personAbove.OwnerId = 0;
            }
            return personAbove;
        }
        /// <summary>
        /// Method to determine rep qualification
        /// </summary>
        /// <param name="foundGuy"></param>
        /// <returns></returns>
        public bool IsThisRepQualified(OrderOwner foundGuy)
        {
            //var expirationDate = DateTime.Today.Date.AddDays(1); //date greater than today
            //write logic here that checks to see if the indicated rep has a qualification record with an expiration date greater than today
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int Response = _rankupService.IsThisRepQualified(7, foundGuy.OwnerId);
            if (Response > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// A Boolean functions that checks to see if the indicated rep has a qualification record with an expiration date greater than today
        /// </summary>
        /// <param name="foundGuy"></param>
        /// <returns></returns>
        public bool IsThisUserQualified(OrderOwner foundGuy, int OrderOwnderId)
        {
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int Response = _rankupService.IsThisUserQualified(1, foundGuy.OwnerId, OrderOwnderId);

            bool status;
            if (Response > 0)
            {
                status = true;
            }
            else
            {
                status = false;
            }
            return status;
        }

        /// <summary>
        /// Determine theat FoundGuy is currently qualified with an order from owner
        /// </summary>
        /// <param name="foundGuy"></param>
        /// <param name="orderOwner"></param>
        /// <returns></returns>
        public bool CurrentlyQualified_ByThisOrderOwner(OrderOwner foundGuy, OrderOwner orderOwner)
        {
            // write logic here that checks to see if the indicated rep, FoundGuy is currently qualified with an order 
            //from owner (OrderOwner) of the order currently being processed
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            //int Response = _rankupService.IsThisRepQualified(8, foundGuy.OwnerId);
            int Response = _rankupService.IsThisUserQualified(2, foundGuy.OwnerId,orderOwner.OwnerId);
            if (Response > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /// <summary>
        /// Method to find next up line qualified RC PR
        /// </summary>
        /// <param name="foundGuy"></param>
        /// <returns></returns>
        public OrderOwner FindTheNextupLine_Qualified_RC_PR_Above(OrderOwner foundGuy)
        {
            //var expirationDate = DateTime.Today.Date.AddDays(1); //date greater than today
            OrderOwner orderOwner = new OrderOwner();// write logic here -	Searches up line beginning with the “person above” the indicated rep 
                                                     //for the next RC-PR that is currently qualified with a qualification record with an expiration date greater than today
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int Response = _rankupService.FindTheNextUpLineRC_PR_Above(10, orderOwner.OwnerId);
            if (Response > 0)
            {
                orderOwner.OwnerId = Response;
            }
            else
            {
                orderOwner.OwnerId = 0;
            }
            return orderOwner;
        }

        public class OrderOwner
        {
            public int OwnerId { get; set; }
            public string OwnerName { get; set; }
        }

        //Rank Coom

        public int SilverCommission(int OwnerId, int OrderId)
        {
            int RankId = 0, FoundGuyUserId = 0, Next_Upline_2_Star = 0;
            string CommissionType = null;

            //Pay the S1 Commission
            RankId = 3;  // *Silver
            CommissionType = "S1";
            FoundGuyUserId = FindTheNextUpLine_RC_PR(OwnerId, RankId, OwnerId);
            if (IsUser_RC(FoundGuyUserId) == true)
            {
                PayThisGuy_MoreMitoCash(CommissionType, FoundGuyUserId, RankId, OrderId, OwnerId);
            }
            else
            {
                PayThisGuy_Cash(CommissionType, FoundGuyUserId, RankId, OrderId, OwnerId);
            }

            //Pay the S2-A commission
            RankId = 4;  // **Silver
            CommissionType = "S2-A";
            FoundGuyUserId = FindTheNextUpLine_RC_PR(OwnerId, RankId, OwnerId);
            if (IsUser_RC(FoundGuyUserId) == true)
            {
                PayThisGuy_MoreMitoCash(CommissionType, FoundGuyUserId, RankId, OrderId, OwnerId);
            }
            else
            {
                PayThisGuy_Cash(CommissionType, FoundGuyUserId, RankId, OrderId, OwnerId);
            }

            //Pay the S2-B commission
            RankId = 4;  // **Silver
            CommissionType = "S2-B";
            Next_Upline_2_Star = FindTheNextUpLine_RC_PR(FoundGuyUserId, RankId, OwnerId);
            //if (Next_Upline_2_Star == 0) // applied incorrectly
            //{
            //    Next_Upline_2_Star = FindTheNextUpLine_RC_PR(OwnerId, RankId);
            //}

            //  if (IsUser_RC(FoundGuyUserId) == true)
            if (IsUser_RC(Next_Upline_2_Star) == true)
            {
                PayThisGuy_MoreMitoCash(CommissionType, Next_Upline_2_Star, RankId, OrderId, OwnerId);
            }
            else
            {
                PayThisGuy_Cash(CommissionType, Next_Upline_2_Star, RankId, OrderId, OwnerId);
            }

            //Pay the S3 Commission
            RankId = 5;  // **Silver
            CommissionType = "S3";
            FoundGuyUserId = FindTheNextUpLine_RC_PR(OwnerId, RankId,OwnerId);
            if (IsUser_RC(FoundGuyUserId) == true)
            {
                PayThisGuy_MoreMitoCash(CommissionType, FoundGuyUserId, RankId, OrderId, OwnerId);
            }
            else
            {
                PayThisGuy_Cash(CommissionType, FoundGuyUserId, RankId, OrderId, OwnerId);
            }

            return 0;
        }

        public int GoldCommission(int OwnerId, int OrderId)
        {
            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            long GoldCommissionUserID = _commonService.GetUsersIDbyUserName(9, GoldCommissionUserName);

            int GoldUserID = Convert.ToInt32(GoldCommissionUserID);
            int RankId = 0, PayGuyId = 0; string CommissionType = null;
            RankId = 6;  // *Gold
            CommissionType = "G1";
            PayGuyId = GoldUserID;// 3024; //Rodger
            PayThisGuy_CashForGold(CommissionType, PayGuyId, RankId, OrderId,OwnerId);


            RankId = 7;  // **Gold
            CommissionType = "G2-A";
            PayGuyId = GoldUserID;// 3024; //Rodger
            PayThisGuy_CashForGold(CommissionType, PayGuyId, RankId, OrderId,OwnerId);

            RankId = 7;  // **Gold
            CommissionType = "G2-B";
            PayGuyId = GoldUserID;// 3024; //Rodger
            PayThisGuy_CashForGold(CommissionType, PayGuyId, RankId, OrderId,OwnerId);

            RankId = 8;  // ***Gold
            CommissionType = "G3";
            PayGuyId = GoldUserID;// 3024; //Rodger
            PayThisGuy_CashForGold(CommissionType, PayGuyId, RankId, OrderId,OwnerId);


            return 0;
        }

        public int PlatinumCommission(int OwnerId, int OrderId)
        {
            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            long GoldCommissionUserID = _commonService.GetUsersIDbyUserName(9, GoldCommissionUserName);

            int PlatinumUserID = Convert.ToInt32(GoldCommissionUserID);
            //  int.TryParse(GoldCommissionUserID, out int PlatinumUserID);
            int RankId = 0, PayGuyId = 0; string CommissionType = null;
            RankId = 9;  // *Platinum
            CommissionType = "P1";
            PayGuyId = PlatinumUserID;// 3024; //Rodger
            PayThisGuy_CashForPlatinum(CommissionType, PayGuyId, RankId, OrderId,OwnerId);

            RankId = 10;  // *Platinum
            CommissionType = "P2";
            PayGuyId = PlatinumUserID;// 3024; //Rodger
            PayThisGuy_CashForPlatinum(CommissionType, PayGuyId, RankId, OrderId,OwnerId);


            RankId = 11;  // *Platinum
            CommissionType = "P3";
            PayGuyId = PlatinumUserID;// 3024; //Rodger
            PayThisGuy_CashForPlatinum(CommissionType, PayGuyId, RankId, OrderId,OwnerId);


            return 0;
        }


        public int PoolCommission(int OwnerId, int OrderId)
        {
            string CommissionType = null;

            CommissionType = "PoolCommission";
            PayThisGuy_CashForPool(CommissionType, OrderId, OwnerId);

            return 0;
        }

        public int Retail(int OrderId)
        {
            string CommissionType = null;

            CommissionType = "Retail";
            PayRetail(OrderId, CommissionType);
            return 0;
        }

        //Pay Pool
        //if autoshihp -no retail
        //  not retail 
        //Pay 5$ retail sell com per item if not discounted autoship 


        public int FindTheNextUpLine_RC_PR(int OwnerId, int RankId,int OriginalOrderOwnerId)
        {
            DoCommissionViewModel model = new DoCommissionViewModel();// write logic here -	Searches up line beginning with the “person above” the indicated rep for the next RC-PR
            model.UserId = OwnerId;
            model.RankId = RankId;
            model.OrginalOrderOwnerId = OriginalOrderOwnerId;
            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.UserCommissionDistributionGenerateRank(2, model);

            return Response;
        }

        public bool IsUser_RC(int UserId)
        {
            //if user is RC then return true
            var _rankupService = (IRankupService)DependencyResolver.Current.GetService(typeof(IRankupService));
            int Response = _rankupService.IsOrderOwnerRC_PR(11, UserId);
            if (Response > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public int PayThisGuy_MoreMitoCash(string CommissionType, int FoundGuyUserId, int RankId, int OrderId, int OrderOwnerId)
        {
            //Insert MoreMito Cash...
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.UserId = FoundGuyUserId;
            model.CommissionType = CommissionType;
            model.RankId = RankId;
            model.OrderId = OrderId;
            model.OrginalOrderOwnerId = OrderOwnerId;

            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.PayMoreMitoCash(3, model);

            return Response;
        }

        public int PayThisGuy_Cash(string CommissionType, int FoundGuyUserId, int RankId, int OrderId,int OrderOwnerId)
        {
            //Insert Cash...
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.UserId = FoundGuyUserId;
            model.CommissionType = CommissionType;
            model.RankId = RankId;
            model.OrderId = OrderId;
            model.OrginalOrderOwnerId = OrderOwnerId;

            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.PayCash(4, model);

            return Response;
        }

        public int PayThisGuy_CashForGold(string CommissionType, int FoundGuyUserId, int RankId, int OrderId, int OrderOwnerId)
        {
            //Insert Cash...
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.UserId = FoundGuyUserId;
            model.CommissionType = CommissionType;
            model.RankId = RankId;
            model.OrderId = OrderId;
            model.OrginalOrderOwnerId = OrderOwnerId;
            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.PayCash(6, model);

            return Response;
        }

        public int PayThisGuy_CashForPlatinum(string CommissionType, int FoundGuyUserId, int RankId, int OrderId, int OrderOwnerId)
        {
            //Insert Cash...
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.UserId = FoundGuyUserId;
            model.CommissionType = CommissionType;
            model.RankId = RankId;
            model.OrderId = OrderId;
            model.OrginalOrderOwnerId = OrderOwnerId;
            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.PayCash(7, model);

            return Response;
        }

        public int PayThisGuy_CashForPool(string CommissionType, int OrderId, int ownerId)
        {

            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            long PoolCommissionUserID = _commonService.GetUsersIDbyUserName(9, PoolCommissionUserName);

            int PoolUserID = Convert.ToInt32(PoolCommissionUserID);
            //Insert Cash...
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.CommissionType = CommissionType;
            model.OrderId = OrderId;
            model.UserId = PoolUserID;// 3025;
            model.OrginalOrderOwnerId = ownerId;
            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.PayCash(5, model);

            return Response;
        }

        public int PayRetail(int OrderId, string CommissionType)
        {
            DoCommissionViewModel model = new DoCommissionViewModel();
            model.CommissionType = CommissionType;
            model.OrderId = OrderId;
            var _commissionService = (ICommissionService)DependencyResolver.Current.GetService(typeof(ICommissionService));
            int Response = _commissionService.PayCash(8, model);

            return Response;

        }

    }
}