﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Stripe.Infrastructure;
using Stripe;
using System.Configuration;

namespace Foxxlegacy.Web.Models
{
    public class StripePaymentGateway
    {
        string StripeApiKey = ConfigurationManager.AppSettings["StripeApiKey"];

        public long StripePay()
        {
            long Status = 0;
            //Stripe.CreditCardOptions card = new Stripe.CreditCardOptions();
            StripeConfiguration.ApiKey = StripeApiKey;

            var options = new TokenCreateOptions
            {
                Card = new TokenCardOptions
                {
                    Number = "4242424242424242",
                    ExpMonth = 7,
                    ExpYear = 2022,
                    Cvc = "314",
                },
            };
            var service = new TokenService();
            service.Create(options);

            return Status;
        }

        public ResponseData CreateCustomer(RecurringPaymentMethod model)
        {
            long Status = 0;
            StripeConfiguration.ApiKey = StripeApiKey;
            var AddressData = new AddressOptions
            {
                City = model.City,
                Line1= model.Address,
                PostalCode= model.Zip,
                State= model.State
            };

            long ExpMonth = 0, ExpYear = 0;
            if (model.ExpDate != null)
            {
                string[] expMonthYear = model.ExpDate.Split('/');
                ExpMonth =Convert.ToInt64(expMonthYear[0].ToString());
                ExpYear =Convert.ToInt64(expMonthYear[1].ToString());
            }
            //var aa = new ShippingOptions
            //{

            //};

          
          

            var options = new CustomerCreateOptions
            {
                Description = "My First Test Customer (created for API docs)",
                Email=model.Email,
                Name = model.FirstName + " " + model.LastName,
                Phone =model.Phone,
                Address= AddressData,

            };
            var service = new CustomerService();
            var response=  service.Create(options);

            var optionsCard = new TokenCreateOptions
            {
                Card = new TokenCardOptions
                {
                    Number = model.CCNo,
                    ExpMonth = ExpMonth,
                    ExpYear = ExpYear,
                    Cvc = model.CVV,
                }


            };
            var serviceCard = new TokenService();           
            var CardResponse= serviceCard.Create(optionsCard);

            var optionsPaymentMethod = new PaymentMethodCreateOptions
            {
                Type = "card",
                Card = new PaymentMethodCardOptions
                {
                    Number = model.CCNo,
                    ExpMonth = ExpMonth,
                    ExpYear = ExpYear,
                    Cvc = model.CVV,
                }
            };
            //var rp=new reque
            var servicePaymentMethod = new PaymentMethodService();
            //service.Attach.CustomerId = "".ToString();
            //service.Attach(CustomerId,new PaymentMethodAttachOptions { } ,new RequestOptions{ApiKey= StripeApiKey });
            var paymentMet = servicePaymentMethod.Create(optionsPaymentMethod);

            // CreatePaymentMethod(response.Id);

            var ResponseData = new ResponseData
            {
                CustomerId = response.Id,
                PaymentMethodId = paymentMet.Id

            };

            return ResponseData;
        }
       public class ResponseData
        {
            public string CustomerId;
            public string PaymentMethodId;
            public string AddressId;
        }
        public Stripe.Customer CreateNewCustomerProfile(string CustomerId, RecurringPaymentMethod model)
        {
            //Console.WriteLine("Create Customer Profile Sample");
            Stripe.StripeConfiguration.SetApiKey(StripeApiKey);
            
            //Assign Card to Token Object and create Token  
            //Stripe.TokenCreateOptions token = new Stripe.TokenCreateOptions();
            //token.Card = card;
            //Stripe.TokenService serviceToken = new Stripe.TokenService();
            //Stripe.Token newToken = serviceToken.Create(token);


            //Create Charge Object with details of Charge  
            //var options = new Stripe.ChargeCreateOptions
            //{
            //    Amount = Convert.ToInt32(tParams.Amount),
            //    Currency = tParams.CurrencyId == 1 ? "ILS" : "USD",
            //    ReceiptEmail = tParams.Buyer_Email,
            //    CustomerId = stripeCustomer.Id,
            //    Description = Convert.ToString(tParams.TransactionId), //Optional  
            //};
            //and Create Method of this object is doing the payment execution.  
           // var service = new Stripe.ChargeService();
           // Stripe.Charge charge = service.Create(options); // This will do the Payment

            //Create Customer Object and Register it on Stripe  
            Stripe.CustomerCreateOptions myCustomer = new Stripe.CustomerCreateOptions();
            myCustomer.Email = model.Email;
            myCustomer.Name = model.FirstName+" "+model.LastName;
            //myCustomer.Address.City = model.City;
            //myCustomer.Address.PostalCode = model.Zip;
            //myCustomer.Address.State = model.State;
            myCustomer.Phone = model.Phone;
            var customerService = new Stripe.CustomerService();
            
            Stripe.Customer stripeCustomer = customerService.Create(myCustomer);

            // validate response 


            return stripeCustomer;
        }

        public void GetCustomerDetails()
        {
            StripeConfiguration.ApiKey = StripeApiKey;

            var service = new CustomerService();
          var CustomerDetails=  service.Get("cus_AJ6mUWjfoelHlJ");
        }

        public long CreatePaymentMethod(string CustomerId)
        {
            long response = 0;
            var service1 = new CustomerService();
            var CustomerDetails = service1.Get(CustomerId);
            StripeConfiguration.ApiKey = StripeApiKey;

          //var billing=new  PaymentMethodBillingDetails
          //{
          //    Address="",
          //}
            var cu = new Customer
            {
                Id = CustomerId
            };
            var options = new PaymentMethodCreateOptions
            {
                Type = "card",
                Card = new PaymentMethodCardOptions
                {
                    Number = "4242424242424242",
                    ExpMonth = 7,
                    ExpYear = 2022,
                    Cvc = "314",
                }
            };
            //var rp=new reque
            var service = new PaymentMethodService();
            //service.Attach.CustomerId = "".ToString();
            //service.Attach(CustomerId,new PaymentMethodAttachOptions { } ,new RequestOptions{ApiKey= StripeApiKey });
           var paymentMet=  service.Create(options);

            //******************
            // Set your secret key. Remember to switch to your live secret key in production.
            // See your keys here: https://dashboard.stripe.com/apikeys
            StripeConfiguration.ApiKey = StripeApiKey;

            try
            {
                var service2 = new PaymentIntentService();
                var options2 = new PaymentIntentCreateOptions
                {
                    Amount = 1099,
                    Currency = "inr",
                    Customer = CustomerId,
                    PaymentMethod = paymentMet.Id,
                    Confirm = true,
                    OffSession = true,
                };
                service2.Create(options2);
            }
            catch (StripeException e)
            {
                switch (e.StripeError.Error)
                {
                    case "card_error":
                        // Error code will be authentication_required if authentication is needed
                        Console.WriteLine("Error code: " + e.StripeError.Code);
                        var paymentIntentId = e.StripeError.PaymentIntent.Id;
                        var service2 = new PaymentIntentService();
                        var paymentIntent = service2.Get(paymentIntentId);

                        Console.WriteLine(paymentIntent.Id);
                        break;
                    default:
                        break;
                }
            }

            return response;
        }

    }
}