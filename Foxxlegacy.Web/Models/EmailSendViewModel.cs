﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Foxxlegacy.Web.Models
{
    public class EmailSendViewModel
    {
        public List<string> To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
    }
}