﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Web.Models
{
    public class UserManagementModel
    {

    }

    public class Users
    {
        #region Fields
        private readonly IUserService _userService;
        #endregion
        public Users(IUserService userService)
        {
            _userService = userService;
        }

        public long TotalCount(BroadcastEmailUserRequest req)
        {
            long count = 0;
            try
            {
                var list = _userService.GetUserDetailsList(1, req);
                count = list.Count;
            }
            catch (Exception ex)
            {
                _ = ex;
                throw;
            }
            return count;
        }

        public List<UserPersonalInformationViewModel> TotalList(BroadcastEmailUserRequest req)
        {
            var list = new List<UserPersonalInformationViewModel>();
            try
            {
                list = _userService.GetUserDetailsList(1, req);
            }
            catch (Exception ex)
            {
                _ = ex;
                throw;
            }
            return list;
        }


    }
}