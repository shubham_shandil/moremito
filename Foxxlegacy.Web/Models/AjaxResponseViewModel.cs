﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Foxxlegacy.Web.Models
{
    public class AjaxResponseViewModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object data { get; set; }
    }
    public class BroadCastResponseViewModel
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object data { get; set; }
        public int Count { get; set; }
    }
    
}