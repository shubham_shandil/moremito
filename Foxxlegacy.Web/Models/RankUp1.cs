﻿using Foxxlegacy.Services.Rankup;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Foxxlegacy.Web.Models
{
    public class RankUp1
    {
        #region Fields
        private readonly IRankupService _rankupService;


        #endregion

        #region CTor
        public RankUp1(IRankupService rankupService)
        {
            this._rankupService = rankupService;
        }
        #endregion

        public int CheckStep_Five_To_Eleven(int UserID, int OrderID, int PerLegCappedCustomerCount, int CappedCustomerCount, int LegsWithRank3Rank4, int CheckRankLegTotal, int NoOfRankingLeg, int SendingLevel)
        {
            bool IsClearRank = false;
            var model = new CheckStepFiveToElevenRequestViewModel
            {
                UserId = UserID,
                PerLegCappedCustomerCount = PerLegCappedCustomerCount,
                LegsWithRank3Rank4 = LegsWithRank3Rank4,
                CheckRankLegTotal = CheckRankLegTotal
            };
            var CountData = _rankupService.CheckStepFiveToEleven(1, model);

            if(CountData.Count > 0)
            {
                foreach(var userCount in CountData)
                {
                    if(userCount.CappedCustomerCount >= CappedCustomerCount && userCount.UsersInLegCount >= NoOfRankingLeg)
                    {
                        IsClearRank = true;
                    }
                }
            }

            if (IsClearRank)
            {
                int insertedId = _rankupService.InsertUsersQualifyingOrders(2, UserID, OrderID, SendingLevel);
                return SendingLevel + 1;
            }
            else
            {
                return 0;
            }
        }

        public void AssignGold_On_OrderOwner(int OrderId)
        {
            if(OrderId > 0)
            {
                var OrderRequestModel = new OrderUserIdRequestViewModel { OrderId = OrderId };
                int UserId = _rankupService.GetOrderUserId(OrderRequestModel);

                if(UserId > 0)
                {
                    int UnilevelSponsorIDFirst = SetGoldOne_On_UsersID(UserId, 6);
                    if (UnilevelSponsorIDFirst > 0)
                    {
                        Update_User_Codes(UserId, "Gold1", UnilevelSponsorIDFirst);
                        if (UserId > 0)
                        {
                            int UnilevelSponsorID2A = SetGoldOne_On_UsersID(UserId, 7);
                            if (UnilevelSponsorID2A > 0)
                            {
                                Update_User_Codes(UserId, "Gold2A", UnilevelSponsorID2A);
                                if (UnilevelSponsorID2A > 0)
                                {
                                    int UnilevelSponsorID2B = SetGoldOne_On_UsersID(UnilevelSponsorID2A, 7);
                                    Update_User_Codes(UserId, "Gold2B", UnilevelSponsorID2B);
                                }
                            }
                        }
                    }
                }

                
            }
        }        

        public int SetGoldOne_On_UsersID(int UserId, int CheckRankID)
        {
            int ReturnUniLevelSponsorID = 0;
            try
            {
                var model = new GoldRankupSponsorIdReuestViewModel { UserId = UserId, CheckRankId = CheckRankID };
                ReturnUniLevelSponsorID = _rankupService.GetGoldRankupSponsorId(model);
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return ReturnUniLevelSponsorID;
        }

        public void Update_User_Codes(int UsersID, string GoldLevel, int UnilevelSponsorID)
        {
            var model = new UpdateGoldRankUserReuestViewModel { UserId = UsersID, GoldLevel = GoldLevel, SponsorID = UnilevelSponsorID };
            _rankupService.UpdateGoldUserRankup(model);
        }


    }
}