﻿using Foxxlegacy.Services.User;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Web;
using System.Web.Services;
using Foxxlegacy.Services.Common;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Models
{

    public class Common
    {
        #region Fields
        private readonly IUserService _userService;
        private readonly ICommonService _commonService;

        #endregion

       
        #region CTor
        public Common
            (
            IUserService userService, 
            ICommonService commonService

            )
        {
            this._userService = userService;            
            this._commonService = commonService;
            

        }
        #endregion

        [WebMethod]
        public static UserDetailsViewModel GetUserDetailsData()
        {

            var modelObj = new UserDetailsViewModel();
            UserService hh = new UserService();
            string UserName = null;
            string s = (string)HttpContext.Current.Session["UserName"];
            if (HttpContext.Current.Session["userDetails"] != null)
            {
                modelObj = (UserDetailsViewModel)HttpContext.Current.Session["userDetails"];
            }

            //modelObj.Email = "abc";
            //modelObj.Name = "Name";
            //modelObj.Phone = "9874563210";

            return modelObj;
        }

        public static EmailViewModel SetEmailSubjectAndBody(long EmailID)
        {
            EmailViewModel model = new EmailViewModel();
            
            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            model = _commonService.SetEmailSubjectAndBody(1, EmailID);

            return model;
        }

        public static SqlResponseBaseModel InsertToEmailQue(EmailViewModel model)
        {
            SqlResponseBaseModel Basemodel = new SqlResponseBaseModel();
            var _commonService = (ICommonService)DependencyResolver.Current.GetService(typeof(ICommonService));
            Basemodel = _commonService.InsertToEmailQue(1, model);

            return Basemodel;
        }

    }

   
}