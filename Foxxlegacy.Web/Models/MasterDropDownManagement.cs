﻿using Foxxlegacy.Services.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Foxxlegacy.Web.Models
{
    public static class DropDown
    {
        public static List<SelectListItem> GetEmailSendingProfileTypes()
        {
            var Profiles = new List<SelectListItem>();
            Profiles.Add(new SelectListItem { Value = "1", Text = "Send To All" });
            Profiles.Add(new SelectListItem { Value = "2", Text = "Send To UserName" });
            Profiles.Add(new SelectListItem { Value = "3", Text = "Send To Downline" });
            Profiles.Add(new SelectListItem { Value = "4", Text = "Send To Rank And Above" });
            Profiles.Add(new SelectListItem { Value = "5", Text = "Send To Only Rank" });
            Profiles.Add(new SelectListItem { Value = "7", Text = "PR & RC Only" });
            Profiles.Add(new SelectListItem { Value = "8", Text = "General SMS Opt-Out Only" });
            Profiles.Add(new SelectListItem { Value = "9", Text = "SMS Not Verify User Only" });
            Profiles.Add(new SelectListItem { Value = "10", Text = "Send To Referring Customer" });
            Profiles.Add(new SelectListItem { Value = "11", Text = "Send To Customer" });
            return Profiles;
        }

        public static List<SelectListItem> GetSmsSendingProfileTypes()
        {
            var Profiles = new List<SelectListItem>();
            Profiles.Add(new SelectListItem { Value = "1", Text = "Send To All" });
            Profiles.Add(new SelectListItem { Value = "2", Text = "Send To UserName" });
            Profiles.Add(new SelectListItem { Value = "3", Text = "Send To Downline" });
            Profiles.Add(new SelectListItem { Value = "4", Text = "Send To Rank And Above" });
            Profiles.Add(new SelectListItem { Value = "5", Text = "Send To Only Rank" });
            Profiles.Add(new SelectListItem { Value = "7", Text = "PR & RC Only" });
            Profiles.Add(new SelectListItem { Value = "8", Text = "General SMS Opt-Out Only" });
            Profiles.Add(new SelectListItem { Value = "9", Text = "SMS Not Verify User Only" });
            Profiles.Add(new SelectListItem { Value = "10", Text = "Send To Referring Customer" });
            Profiles.Add(new SelectListItem { Value = "11", Text = "Send To Customer" });
            return Profiles;
        }

        public static List<SelectListItem> GetEmailTemplateType()
        {
            var list = new List<SelectListItem>();
            try
            {
                //var datalist = new AdminService().GetEmailTaskNameList(4,0);
                //list.Add(new SelectListItem { Value = Convert.ToString(0), Text = "Please select a template" });
                //foreach (var item in datalist)
                //{
                //    list.Add(new SelectListItem { Value = Convert.ToString(item.EmailId), Text = item.Task });
                //}
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return list;
        }
    }
}