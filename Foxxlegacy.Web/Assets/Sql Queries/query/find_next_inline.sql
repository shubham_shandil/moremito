 DECLARE @Id int = 0,  @UniLevelSponsorId int = 0, @CommissionPercentage decimal(19,2) = 0, @UserRole varchar(32) = '',@RankIdLocal int=0, @RankId int=5
 
 SELECT 
		 @Id = uplineUser.ID
		,@RankIdLocal = uplineUser.RankID
		,@UniLevelSponsorId = uplineUser.UnilevelSponsorID
		,@CommissionPercentage = r.MaxCommissionPercentageDistribute
		,@UserRole = ISNULL(ur.LoweredRoleName, '')
	    FROM 
		dbo.users AS u WITH(NOLOCK)
		INNER JOIN dbo.users AS uplineUser WITH(NOLOCK)
			ON uplineUser.ID = u.UnilevelSponsorID
		INNER JOIN dbo.ranks AS r WITH(NOLOCK)
			ON r.ID = uplineUser.RankID
		INNER JOIN dbo.aspnet_UsersInRoles AS rm WITH(NOLOCK) 
		ON rm.UserId = u.AspNetUserID
		INNER JOIN dbo.aspnet_Roles as ur WITH(NOLOCK) 
			ON ur.RoleId = rm.RoleId
	    WHERE
		U.ID = 4069
		AND LOWER(ur.LoweredRoleName) IN ('member', 'referringcustomer')

SELECT @id,@RankIdLocal as RankIdLocal,@UniLevelSponsorId,@CommissionPercentage,@UserRole

if(@RankId<=@RankIdLocal)
		begin
			print 'b'
			select @Id as RowAffected
		end




		return

		SELECT count(uplineUser.ID)
			FROM 
				dbo.users AS u WITH(NOLOCK)
				INNER JOIN dbo.users AS uplineUser WITH(NOLOCK)
					ON uplineUser.ID = u.UnilevelSponsorID
				--INNER JOIN dbo.ranks AS r WITH(NOLOCK)
				--	ON r.ID = uplineUser.RankID
				INNER JOIN dbo.aspnet_UsersInRoles AS rm WITH(NOLOCK) -- Role Mapping
					ON rm.UserId = u.AspNetUserID
				INNER JOIN dbo.aspnet_Roles as ur WITH(NOLOCK) -- User Role
					ON ur.RoleId = rm.RoleId
			WHERE
				U.ID = 4058
				AND LOWER(ur.LoweredRoleName) IN ('referringcustomer')