SET IDENTITY_INSERT [moremito].[dbo].[Users] ON 
insert into [moremito].[dbo].[users]
(id ,[AspNetUserID] ,[RetailOnly] ,[UserName] ,[SponsorUsersID] ,[UnilevelSponsorID]  ,[RankID] ,[FirstName] ,[LastName] ,[Email] ,[Phone] ,[PhoneType] ,[SMSOptIn] ,[SMSOptInDontRemindMe] ,[SMSPhone] ,[SmsCode]    ,[JoinDate] ,[AgreePP] ,[AdminBlockSweep] )

select 
	   a.[ID]
      ,a.[AspNetUserID]
      ,isnull(a.[RetailOnly],0)
      ,a.[UserName]
      ,a.[SponsorUsersID]
      ,a.[UnilevelSponsorID] 
      ,a.[RankID]
      ,a.[FirstName]
      ,a.[LastName]
      ,a.[Email]
      ,a.[Phone]
      ,a.[PhoneType]
      ,a.[SMSOptIn]
      ,isnull(a.[SMSOptInDontRemindMe],0)
      ,a.[SMSPhone]
      ,a.[SmsCode]   
      ,a.[JoinDate]
      ,isnull(a.[AgreePP],0)
      ,isnull(a.[AdminBlockSweep],0)
      
      
    

 from [moremito_dev].[dbo].[users] a

left join [moremito].[dbo].[users] b on a.id=b.id

where b.id is null