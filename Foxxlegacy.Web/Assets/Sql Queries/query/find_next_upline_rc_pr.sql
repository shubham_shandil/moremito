declare  @ActionId int = null
declare	@UserId bigint =4077--4082-- 4094
declare	@OrderId bigint = 12306
declare	@RankId bigint = 4--3
declare	@CommissionType nvarchar(50) = null

DECLARE @Id int = 0,  @UniLevelSponsorId int = 0, @CommissionPercentage decimal(19,2) = 0, @UserRole varchar(32) = ''
		DECLARE @UplinkUserRank TABLE(Id int, RankId int, SponsorId int, Commission decimal(19,2), [Role] varchar(32))

		 declare @RankIdLocal int =0
		 SELECT 
		 @Id = uplineUser.ID
		,@RankIdLocal = uplineUser.RankID
		,@UniLevelSponsorId = uplineUser.UnilevelSponsorID
		,@CommissionPercentage = r.MaxCommissionPercentageDistribute
		,@UserRole = ISNULL(ur.LoweredRoleName, '')
	    FROM 
		dbo.users AS u WITH(NOLOCK)
		INNER JOIN dbo.users AS uplineUser WITH(NOLOCK)
			ON uplineUser.ID = u.UnilevelSponsorID
		INNER JOIN dbo.ranks AS r WITH(NOLOCK)
			ON r.ID = uplineUser.RankID
		INNER JOIN dbo.aspnet_UsersInRoles AS rm WITH(NOLOCK) -- Role Mapping
		ON rm.UserId = u.AspNetUserID
		INNER JOIN dbo.aspnet_Roles as ur WITH(NOLOCK) -- User Role
			ON ur.RoleId = rm.RoleId
	    WHERE
		U.ID = @UserId
		AND LOWER(ur.LoweredRoleName) IN ('member', 'referringcustomer')
		--AND r.id NOT IN (0,12)
		--select @RankIdLocal,@Id,@RankId

		select @RankId as rankId,@RankIdLocal as localRank,@Id as nextid

		if(@RankId<=@RankIdLocal)
		begin
			print 'b'
			select @Id as RowAffected
		end
		else
		begin
		 print 'a'
		--COMMIT TRANSACTION T	   
		end

		