SET IDENTITY_INSERT [Moremito_dev].[dbo].[UsersAddresses] ON 
insert into [Moremito_dev].[dbo].[UsersAddresses] ([ID]
      ,[UsersID]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Country])
SELECT  [ID]
      ,[UsersID]
      ,[Address1]
      ,[Address2]
      ,[City]
      ,[State]
      ,[Zip]
      ,[Country]
  FROM [Moremito_live_apr7].[dbo].[UsersAddresses]