SET IDENTITY_INSERT  [foxShop_dev].[dbo].[Order] ON 

insert into [foxShop_dev].[dbo].[Order]([Id]      ,[CustomOrderNumber],
BillingAddressId,CustomerId,[OrderGuid],[StoreId],[PickupInStore],[OrderStatusId],[ShippingStatusId],[PaymentStatusId],[PaymentMethodSystemName],
[CustomerCurrencyCode],[CurrencyRate],[CustomerTaxDisplayTypeId],[OrderSubtotalInclTax] ,[OrderSubtotalExclTax],[OrderTotal] ,[CustomerLanguageId]
,[AffiliateId],[CustomerIp], [AllowStoringCreditCardNumber] ,[CreatedOnUtc] ,[IsRecurringOrder],OrderSubTotalDiscountInclTax,OrderSubTotalDiscountExclTax,
OrderShippingInclTax,OrderShippingExclTax,PaymentMethodAdditionalFeeInclTax,PaymentMethodAdditionalFeeExclTax,OrderTax,OrderDiscount,RefundedAmount,Deleted,[PaidDateUtc]
	  )

SELECT 
 distinct a.id,a.id, c.id,nopuserid=(select top 1 nopuserid from  [moremito_dev].[dbo].[nopconnectfox] where foxuserid=a.usersid )
 ,newid(),1,0,0,10,10,'Payments.CashOnDelivery','USD',1,10,0,0,0,1,0,'205.253.127.28',0,getdate(),0,0,0,0,0,0,0,0,0,0,0,a.orderdate
--b.*,a.usersid
FROM [Moremito].[dbo].[orders] a
inner join [foxshop_dev].[dbo].[address] c on a.id=c.CustomAttributes
--inner join [moremito_dev].[dbo].[nopconnectfox] b on a.usersid=b.foxuserid
  where a.orderdate > '2021-11-01'
