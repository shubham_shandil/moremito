

 insert into [foxShop_dev].[dbo].[OrderItem](
      [OrderId]      ,[ProductId]      ,[OrderItemGuid]      ,[Quantity]      ,[UnitPriceInclTax]      ,[UnitPriceExclTax]      ,[PriceInclTax]      ,[PriceExclTax]
      ,[DiscountAmountInclTax]      ,[DiscountAmountExclTax]      ,[OriginalProductCost]   ,[DownloadCount],IsDownloadActivated,attributesxml
      )
 select 
 orderid=(select id from [foxShop_dev].[dbo].[Order] where CheckoutAttributesXml ='shopify' and customvaluesxml=a.mytrimmedcolumn)
 ,[productid],NEWID(),a.qty,b.price,b.price,isnull(a.QTY,0)* isnull(b.PRICE,0),isnull(a.QTY,0)* isnull(b.PRICE,0),0,0,0,0,0,mytrimmedcolumn
 
 from shopify_order_detail_new a
 inner join  [foxshop_dev].[dbo].[product] b on a.[productid]=b.id
