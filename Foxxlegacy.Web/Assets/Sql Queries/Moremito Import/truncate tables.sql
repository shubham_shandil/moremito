use [moremito_dev]
return

update [Moremito_dev].[dbo].[users] set CourtesyCalls=1


update [Moremito_dev].[dbo].[users] set rankid=12 where username  in ('cking','jking','rodger','admindale','moremito')

update [Moremito_dev].[dbo].[users] set rankid=0 where username not  in ('cking','jking','rodger','admindale','moremito')

insert into  [moremito_dev].[dbo].[aspnet_UsersInRoles] values('C6146EAF-056D-4398-9842-240E8E2331A3','9C6581B9-891F-4276-ADAD-57F0CBBBABCD')
insert into  [moremito_dev].[dbo].[aspnet_UsersInRoles] values('C6146EAF-056D-4398-9842-240E8E2331A3','B9229908-7EED-447D-811A-DFC6F5A930CD')
insert into  [moremito_dev].[dbo].[aspnet_UsersInRoles] values('13ABDC5C-F4CA-4D9F-988D-8577ACDBE291','B9229908-7EED-447D-811A-DFC6F5A930CD')

-- set role of pool to RC
update aspnet_UsersInRoles set roleid ='B9229908-7EED-447D-811A-DFC6F5A930CD' where userid='5551828A-6C53-43BC-8E55-5D8A3BB078F8' and roleid ='CE092BFE-6F2D-4A45-BA6E-9250D6D2CC63'  

select * from aspnet_roles where roleid in ('9C6581B9-891F-4276-ADAD-57F0CBBBABCD','B9229908-7EED-447D-811A-DFC6F5A930CD')

truncate table users

truncate table UsersSignUpData

truncate table [Moremito_dev].[dbo].[UsersAddresses]

truncate table [Moremito_dev].[dbo].[WelcomeTag]

truncate table  [moremito_dev].[dbo].[aspnet_Users]

delete from  [moremito_dev].[dbo].[aspnet_Users_old]
delete from [moremito_dev].[dbo].[backup_aspnet_Roles]

delete from [Moremito_dev].[dbo].[aspnet_Applications]


truncate table [Moremito_dev].[dbo].[aspnet_UsersInRoles]

truncate table  [Moremito_dev].[dbo].[DownlineCounts]

truncate table  [Moremito_dev].[dbo].[customer]

select * from [foxShop_dev].[dbo].[Customer]

truncate table [Moremito_dev].[dbo].[NopConnectFox]

select * from  [foxShop_dev].[dbo].[GenericAttribute]

delete from [foxShop_dev].[dbo].[Ordernote]

delete from [foxShop_dev].[dbo].[Orderitem]

delete from [foxShop_dev].[dbo].[Order]
