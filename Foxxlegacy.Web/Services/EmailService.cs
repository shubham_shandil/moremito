﻿using Foxxlegacy.Web.Models;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Foxxlegacy.Web.Services
{
    public class FoxxEmailService
    {
        string SendGridKeyID = string.Empty;
        public FoxxEmailService()
        {
            SendGridKeyID = Convert.ToString(ConfigurationManager.AppSettings["SendGridKeyID"]);
        }
        public async Task<Response> SendEmail(EmailSendViewModel model)
        {
            var client = new SendGridClient(SendGridKeyID);
            var from =Convert.ToString(ConfigurationManager.AppSettings["AdminFromEmail"]);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(from),
                Subject = model.Subject,
                HtmlContent = model.EmailBody
            };
            
            foreach (var item in model.To)
            {
                msg.AddTo(new EmailAddress(item));

            }
            Response response = await client.SendEmailAsync(msg);
            return response;
        }
    }
}