﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using Foxxlegacy.Core;
using System.Reflection;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;

namespace Foxxlegacy.Web.Infrastructure
{
    public class DependencyRegistrar : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder);

            // Registering Repository
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(Assembly.Load("Foxxlegacy.Core"))
                    .Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces().InstancePerLifetimeScope();

            // Registering DB Context
            builder.Register<IDbContext>(c => new FoxxlegacyAppContext()).InstancePerLifetimeScope();

            //Registering Services
            builder.RegisterAssemblyTypes(Assembly.Load("Foxxlegacy.Services"))
                .Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces().InstancePerLifetimeScope();

            // Register Mvc Controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly).InstancePerLifetimeScope();

            // Register Web Api Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
        }
    }
}