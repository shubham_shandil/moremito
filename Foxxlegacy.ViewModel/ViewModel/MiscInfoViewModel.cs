﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class MiscInfoViewModel
    {
        public QuestionAnswer _questionAnswer { get; set; }
        public PersonalInfo _personalInfo { get; set; }
        public RecurringOrder _recurringOrder { get; set; }
        public List<ProcessOrder> _processOrder { get; set; }
        public List<Advertisment> _advertisment { get; set; }
        public List<RankHistory> _rankHistory { get; set; }
        public List<MarketingActivity> _marketingActivity { get; set; }
        public MobileAppActivity _mobileAppActivity { get; set; }
        public MenuActivity _menu { get; set; }

        public string MembershipType { get; set; }

        public string HighestRankAchieved { get; set; }

        public string QTE { get; set; }

        public string JoinDate { get; set; }
        public MiscInfoViewModel()
        {
            _questionAnswer = new QuestionAnswer();
            _personalInfo = new PersonalInfo();
            _recurringOrder = new RecurringOrder();
            _processOrder = new List<ProcessOrder>();
            _advertisment = new List<Advertisment>();
            _rankHistory = new List<RankHistory>();
            _marketingActivity = new List<MarketingActivity>();
            _mobileAppActivity = new MobileAppActivity();
            _menu = new MenuActivity();
        }
    }
    public class QuestionAnswer
    {
        public string Q1 { get; set; }
        public string Answer1 { get; set; }
        public string Q2 { get; set; }
        public string Answer2 { get; set; }
    }
    public class PersonalInfo
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public long Placement { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime JoinDate { get; set; }
        public string SponserName { get; set; }
        public long Level { get; set; }
    }
    public class RecurringOrder
    {
        public string NickName { get; set; }
        public string NextOrderDate { get; set; }
        public string ShippingInfo { get; set; }
        public long CV { get; set; }
        public string PaymentInfo { get; set; }
        public string Active { get; set; }
    }
    public class ProcessOrder
    {
        public long Id { get; set; }
        public long OrderNo { get; set; }
        public string OrderDate { get; set; }
        public string CV { get; set; }
        public string FulfillmentStatus { get; set; }
        public string CVExpireDate { get; set; }
        public string RSC { get; set; }
    }
    public class Advertisment
    {
        public string CampaignName { get; set; }
        public string LinkName { get; set; }
        public long Hits { get; set; }
        public string PageName { get; set; }
    }
    public class RankHistory
    {
        public string History { get; set; }
    }

    public class MarketingActivity
    {
        public string UniqueHits { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string FQDN { get; set; }
        public string Method { get; set; }
        public string Description { get; set; }
    }
    public class MobileAppActivity
    {
        public string RecipientsLast72Hours { get; set; }
        public string RecipientsLast7Days { get; set; }
        public string RecipientsTweWeekDaysAgo { get; set; }
        public string RecipientsThreeWeekDaysAgo { get; set; }
        public string RecipientsFourWeekDaysAgo { get; set; }
        public string RecipientsLifetime { get; set; }
        public string MobileLast72Hours { get; set; }
        public string MobileLast7Days { get; set; }
        public string MobileTweWeekDaysAgo { get; set; }
        public string MobileThreeWeekDaysAgo { get; set; }
        public string MobileFourWeekDaysAgo { get; set; }
        public string MobileLifetime { get; set; }

        public string Recipients8to14day { get; set; }

        public string Recipinets15to21day { get; set; }

        public string Recipients22to28day { get; set; }

        public string RecipientsLife { get; set; }

        public string Activity72h { get; set; }
        public string Activity7day { get; set; }

        public string Activity8to14day { get; set; }

        public string Activity15to21day { get; set; }

        public string Activity22to28day { get; set; }


        public string ActivityLife { get; set; }

    }

    public class MenuActivity
    {
        public bool IsPlacementOn { get; set; }
    }

}
