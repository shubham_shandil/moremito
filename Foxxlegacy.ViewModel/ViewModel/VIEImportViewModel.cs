﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class VIEImportViewModel
    {
        public long IndexNo { get; set; }
        public string EnrollmentDate { get; set; }

        public string RecordLocatorNumber { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CreditScoreClass { get; set; }
        public string ProductCode { get; set; }
        public string ProductTerm { get; set; }
        public string MemberStatus { get; set; }
        public string SponsorStatus { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string AddressPostalCode { get; set; }
        public string OVCPStatus { get; set; }


    }


    public enum VIEImportColumnList
    {
        EnrollmentDate = 1,
        RecordLocatorNumber = 2,
        FirstName = 3,
        LastName = 4,
        CreditScoreClass = 5,
        ProductCode = 6,
        ProductTerm = 7,
        MemberStatus = 8,
        SponsorStatus = 9,
        StreetAddress = 10,
        City = 11,
        State = 12,
        AddressPostalCode = 13,
        OVCPStatus = 14


    }
}
