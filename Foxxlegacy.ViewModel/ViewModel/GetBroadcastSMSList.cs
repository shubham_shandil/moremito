﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class GetBroadcastSMSList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }

        public string ToNumber { get; set; }

        public string Date { get; set; }

        public string SMSText { get; set; }
        public bool CheckId { get; set; }


        public string FromNumber { get; set; }
        public long UserID { get; set; }
    }
}
