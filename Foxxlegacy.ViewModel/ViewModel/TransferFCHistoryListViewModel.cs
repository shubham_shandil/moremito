﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class TransferFCHistoryListViewModel
    {
        public string CreateDate { get; set; }
        public string DateAvailable { get; set; }
        public bool FromLevel { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public long ID { get; set; }


        public string WebContentName { get; set; }
        public string Content { get; set; }

        public decimal TotalReceived { get; set; }

        public decimal TotalSent { get; set; }


       

        public List<TransferFCHistoryListViewModel> transferFCHistoryListViewModel { get; set; }

        public List<TransferFCHistoryListViewModel2> transferFCHistoryListViewModel2 { get; set; }

        public EventsViewModel eventsViewModel { get; set; }
        public EventsListViewModel eventsListViewModel = new EventsListViewModel();

        public List<FoxxCashSentListViewModel> foxxCashSentListViewModel { get; set; }

        public List<FoxxCashReceivedListViewModel> foxxCashReceivedListViewModel { get; set; }

        public TransferFCHistoryListViewModel()

        {
            transferFCHistoryListViewModel = new List<TransferFCHistoryListViewModel>();
            eventsViewModel = new EventsViewModel();
            eventsListViewModel = new EventsListViewModel();
            transferFCHistoryListViewModel2 = new List<TransferFCHistoryListViewModel2>();
            foxxCashSentListViewModel = new List<FoxxCashSentListViewModel>();
            foxxCashReceivedListViewModel = new List<FoxxCashReceivedListViewModel>();
        }
    }

    public class TransferFCHistoryListViewModel2
    {
        public long CommissionsSweptId { get; set; }

        public string CommissionsSweptCreateDate { get; set; }

        public decimal CommissionsSweptAmount { get; set; }

        public string CommissionsSweptDescription { get; set; }
    }
}
