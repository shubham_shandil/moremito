﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   
    public class BundelViewModel
    {
        public int BundleId { get; set; }
        public string BundleName { get; set; }
        public int BundleDisplayOrder { get; set; }
        public List<BundleItems> bundleItems { get; set; }
    }
    public class BundleItems
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ShortDescription { get; set; }
        public decimal Price { get; set; }
        public int ItemDisplayOrder { get; set; }
    }
    public class BundleAllModel
    {
        public int BundleId { get; set; }
        public string BundleName { get; set; }
        public int BundleDisplayOrder { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public string ShortDescription { get; set; }
        public decimal Price { get; set; }
        public int ItemDisplayOrder { get; set; }
    }
    public class OrderViewModel
    {
        public decimal? OrderTotal { get; set; }
       public List<OrderItems> Items { get; set; }
    }
    public class OrderItems
    {
        public int ItemId { get; set; }
        public decimal Price { get; set; }
        public decimal Qty { get; set; }
        public decimal Total { get; set; }
        public bool AutoShip { get; set; }
    }
    public class ImportOrderModel
    {
        public int OrderId { get; set; }
        public int Customerid { get; set; }
    }
    public class ImportOrderForPlacementModel
    {
        public int OrderId { get; set; }
        public int Customerid { get; set; }
        public DateTime? OrderDate { get; set; }
        public decimal? OrderTotal { get; set; }
        public bool? IsRecurringOrder { get; set; }
    }
    public class UserHeaderInfoModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
    public class UserAddressInfoModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string UserAddress { get; set; }
        public string City { get; set; }
        public string StateName { get; set; }
        public string StateAbbr { get; set; }
        public string CountryName { get; set; }
        public string CountryAbbr { get; set; }
        public string Zip { get; set; }
    }
    public class CommissionSUmmaryModel
    {
        public decimal TotalOrderCashCommission { get; set; }
        public decimal TotalOrderCash { get; set; }
        public decimal TotalEarnedCommission { get; set; }
        public decimal TotalOrderCommission { get; set; }
        public decimal MoreCashReceived { get; set; }
        public decimal TotalSwept { get; set; }
        public decimal TotalAvailable { get; set; }
        public decimal MoreCash { get; set; }
        public decimal MoreCommission { get; set; }
        public decimal TotalSweptOnPurchase { get; set; }
        public decimal TotalSweptOnTransfer { get; set; }
    }
}
