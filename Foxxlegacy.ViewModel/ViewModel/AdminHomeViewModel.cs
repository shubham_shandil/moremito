﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class AdminHomeViewModel
    {
        public List<MemberInfo> MemberInfo { get; set; }
        public UserNamePassword UserNamePassword { get; set; }
        public IEnumerable<SelectListItem> SearchByList { get; set; }

        public string UserName { get; set; }
        public string SearchBy { get; set; }
        public string OrderNo { get; set; }
        public long ActiveTab { get; set; }
        public string OldUserName { get; set; }
        public string NewUserName { get; set; }

        public string TabName { get; set; }
        public string OldSponsor { get; set; }
        public string NewSponsor { get; set; }

        public List<OrderCallListViewModel> OrderCallListViewModel { get; set; }

        public List<OrderCallListViewModel> DeclineCallListViewModel { get; set; }

        public List<PinCertificateListViewModel> PinCertificateListViewModel { get; set; }

        public long PinsToSendCount { get; set; }

        public List<CallListViewModel> CallListViewModel { get; set; }

        public long CallToMakeCount { get; set; }

        public string AdminHomeContent{get;set;}

        public long UserCount { get; set; }

        public long EmailCount { get; set; }

        public List<string> LinkErrorsList { get; set; }

        public long MysteryNumber { get; set; }
        public AdminHomeViewModel()
        {
            OrderCallListViewModel = new List<OrderCallListViewModel>();
            DeclineCallListViewModel = new List<OrderCallListViewModel>();
            PinCertificateListViewModel = new List<PinCertificateListViewModel>();
            CallListViewModel = new List<CallListViewModel>();
            LinkErrorsList = new List<string>();
        }
    }

    public class MemberInfo
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
    }

    public class UserNamePassword
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
