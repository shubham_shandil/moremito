﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class UsersQTENowforMBOViewModel
    {
        public long QteRankID { get; set; }

        public string AsOfDate { get; set; }

        public bool HasQteForNowRecord { get; set; }
    }
}
