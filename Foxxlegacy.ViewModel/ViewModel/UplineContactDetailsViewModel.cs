﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UplineContactDetailsViewModel
    {

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Rank { get; set; }

        public string Level { get; set; }
    }
    public class TestModel
    {
        public int id { get; set; }
        public  int userId { get; set; }
        public string username { get; set; }
        public int unilevelsponsorid { get; set; }
    }

}
