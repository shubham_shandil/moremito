﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class MessagesListViewModel
    {
        public long ID { get; set; }
        public long UserId { get; set; }
        public string Origin { get; set; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string Subject { get; set; }
        public string Details { get; set; }
        public string Notes { get; set; }
       

        public bool Archived { get; set; }
        public string EventDate { get; set; }

        
    }
}
