﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class PasswordChangeRequestsRecordViewModel
    {
        public long UsersID { get; set; }
        public string RequestID { get; set; }

        public string FromEmailAddress { get; set; }

        public string ToEmailAddress { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }


        public long RecipientID { get; set; }

        public string sGUID { get; set; }
    }
}
