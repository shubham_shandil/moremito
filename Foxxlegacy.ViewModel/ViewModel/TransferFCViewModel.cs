﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class TransferFCViewModel
    {
        public decimal Balance { get; set; }

        public decimal Earned { get; set; }

        public decimal Swept { get; set; }

        public bool HasCommissions { get; set; }
        public string Content { get; set; }

        public string Search { get; set; }

        public long UsersID { get; set; }

        public string RecipientsUsername { get; set; }

        public string RecipientName { get; set; }

        public string RecipientEmail { get; set; }

        public string RecipientPhone { get; set; }

        public long RecipientUserID { get; set; }

        public bool SelfUser { get; set; }

        public bool HasW9 { get; set; }
        public bool AdminBlockSweep { get; set; }

        public decimal Amount { get; set; }
        public string CommissionsSweptDescription { get; set; }

        public string CommissionsDescription { get; set; }
        public long FromLevel { get; set; }
        public List<TransferFCListViewModel> transferFCListViewModel { get; set; }
        public List<MyFoxxCashListViewModel> myFoxxCashViewModelList { get; set; }
        public EventsListViewModel eventsListViewModel = new EventsListViewModel();

        public EventsViewModel eventsViewModel { get; set; }

        public UserDetailsViewModel userDetailsViewModel { get; set; }

        public UserViewModel userViewModel { get; set; }

        public CommissionsSweptViewModel commissionsSweptViewModel { get; set; }

        public CommissionsViewModel commissionsViewModel { get; set; }

        public CommissionsListViewModel commissionsListViewModel { get; set; }

        public TransferFCViewModel()
        {
            transferFCListViewModel = new List<TransferFCListViewModel>();
            myFoxxCashViewModelList = new List<MyFoxxCashListViewModel>();
            eventsListViewModel = new EventsListViewModel();
            eventsViewModel = new EventsViewModel();
            userDetailsViewModel = new UserDetailsViewModel();
            userViewModel = new UserViewModel();
            commissionsSweptViewModel = new CommissionsSweptViewModel();
            commissionsViewModel = new CommissionsViewModel();
            commissionsListViewModel = new CommissionsListViewModel();
        }
    }
}
