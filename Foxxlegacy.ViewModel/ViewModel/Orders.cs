﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class Orders
    {
        public List<MyOrders> myOrders { get; set; }
        public string GrandOrderTotal { get; set; }

        public string SearchBy { get; set; }
        public Orders()
        {
            myOrders = new List<MyOrders>();
        }
    }
    public class MyOrders
    {
        public string OrderId { get; set; }
        public string ProductName { get; set; }
        public long ProductQty { get; set; }
        public string ProductImage { get; set; }
        public decimal OrderTotal { get; set; }
        public DateTime CreatedOn { get; set; }
        public decimal SubTotal { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string ZipPostalCode { get; set; }
        public long OrderStatusId { get; set; }
        public long PaymentStatusId { get; set; }
        public long ShippingStatusId { get; set; }
        public string PaymentMethodSystemName { get; set; }
        public string Status { get; set; }


        public long CustomerId { get; set; }


        public long NoOfOrder { get; set; }


    }

    public class MyReferralOrders
    {
        public List<MyReferralOrdersList> OrderList { get; set; }
    }
    public class MyReferralOrdersList
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int OrderId { get; set; }
        public decimal OrderTotal { get; set; } 
        public DateTime CreatedOn { get; set; }
        public DateTime ExpDate { get; set; }
    }

}
