﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class AdminEditorViewModel
    {
        public Int32 Id { get; set; }
        public long ActiveTab { get; set; }
        public string ContentKeyName { get; set; }
        public string EditorContent { get; set; }

        public Int32 DeeplinkId { get; set; }
        public string PageName { get; set; }
        public string Url { get; set; }
        public Int32 Order { get; set; }
        public Boolean SelectForQuickLinks { get; set; }

        public string Task { get; set; }
        public string CatsName { get; set; }

        public Int32 SmsId { get; set; }
        public Int32 Quantity { get; set; }
        public Int32 OnceEvery { get; set; }
        public string StartingOn { get; set; }
        public string Time { get; set; }

        public string ddlFreqPeriod { get; set; }
        public EmailTemplateDetails emailTemplateDetails { get; set; }
        public ResourcesItems resourcesItems { get; set; }
        public List<ResourcesItems> resourcesItemsList { get; set; }
        public Faqs FaqsItems { get; set; }
        public List<Faqs> FaqsItemsList { get; set; }
        public List<FAQCategories> FAQCategoriesList { get; set; }
        public List<SmsEvent> SmsEventList { get; set; }
        public SmsEvent SmsEventDetails { get; set; }
        public List<SmsEventMessage> SmsEventMsgList { get; set; }


        public List<HelpViewModel> ContentKeyList { get; set; }
        public List<DeeplinkDetails> DeeplinkDetailsList { get; set; }
        public List<EmailTemplateDetails> EmailTaskList { get; set; }
        public List<CatsDetails> CatsDetailsList { get; set; }
        public List<CatsDetails> FaqCatsDetailsList { get; set; }

        public List<FreqTableList> FreqTableList { get; set; }
    }

    public class FAQCategories
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }


    }
    public class DeeplinkDetails
    {
        public Int32 DeeplinkId { get; set; }
        public string PageName { get; set; }
        public string Url { get; set; }
        public Int32 Order { get; set; }
        public Boolean SelectForQuickLinks { get; set; }
    }

    public class EmailTemplateDetails
    {
        public Int32 EmailId { get; set; }
        public string Task { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string SmsBody { get; set; }
    }

    public class CatsDetails
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public decimal CatOrder { get; set; }

    }

    public class ResourcesItems
    {
        public Int32 Id { get; set; }
        public Int32 CatsId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string FilePath { get; set; }
        public double DisplayOrder { get; set; }


    }

    public class Faqs
    {
        public Int32 Id { get; set; }
        public Int32 Category { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public double DisplayOrder { get; set; }
        public int[] SelectedTeaIds { get; set; }

    }

    public class SmsEvent
    {
        public Int32 Id { get; set; }
        public string EventName { get; set; }
        public string NewEventName { get; set; }
        public string Keyword { get; set; }
        public string ConfirmOptIn { get; set; }
        public string ConfirmOptOut { get; set; }


    }

    public class SmsEventMessage
    {
        public Int32 Id { get; set; }
        public string Msg { get; set; }
        public string SendTime { get; set; }


    }

    public class FreqTableList
    {
        public long ID { get; set; }
        public long Ordinal { get; set; }

        public string OrderDate { get; set; }
    }


}
