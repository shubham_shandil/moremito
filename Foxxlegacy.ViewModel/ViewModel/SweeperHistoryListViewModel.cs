﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class SweeperHistoryListViewModel
    {
        public string CreateDate { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public bool Paid { get; set; }
        public string PaidDate { get; set; }

        public List<SweeperHistoryListViewModel> sweeperHistoryListViewModel { get; set; }

        public SweeperHistoryListViewModel()

        {
            sweeperHistoryListViewModel = new List<SweeperHistoryListViewModel>();
        }
    }
}
