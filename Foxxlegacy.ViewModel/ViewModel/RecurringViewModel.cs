﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class RecurringViewModel
    {

        public List<Autoship> AutoshipList { get; set; }
        public List<RecurringOrderHistory> RecurringOrderHistoryList { get; set; }
            
    }

    public class Autoship
    {
        public Int32 Id { get; set; }
        public string Active { get; set; }
        public string NickName { get; set; }
        public string NextOrderDate { get; set; }
        public string LastStatus { get; set; }
        public string UseFoxxCash { get; set; }
    }

    public class RecurringOrderHistory
    {
        public Int32 Id { get; set; }
        public string OrderNo { get; set; }
        public string OrderDate { get; set; }
        public string Status { get; set; }
        public Int32 Customerid { get; set; }
    }
}
