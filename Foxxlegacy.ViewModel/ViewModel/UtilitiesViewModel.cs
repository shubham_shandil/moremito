﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UtilitiesViewModel
    {
        public List<PhoneNotVerify> phoneNotVerify { get; set; }
        public List<OptOutReport> optOutReports { get; set; }

        public List<DeleteUserUtility> DeleteUserUtility { get; set; }


        public string SearchDeleteUserUtility { get; set; }
        public bool IsPlacementAllowed { get; set; }
        public List<IncompleteUsers> IncompleteUsers { get; set; }


        public List<OpenDataBaseConnection> OpenDataBaseConnection { get; set; }


        public ImportExcelViewModel importExcel { get; set; }
        public UtilitiesViewModel()
        {
            phoneNotVerify = new List<PhoneNotVerify>();
            optOutReports = new List<OptOutReport>();
            DeleteUserUtility = new List<DeleteUserUtility>();
            OpenDataBaseConnection = new List<OpenDataBaseConnection>();
            importExcel = new ImportExcelViewModel();
        }
    }
    public class OptOutReport
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public class PhoneNotVerify
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }


    public class DeleteUserUtility
    {
        public long ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string JoinDate { get; set; }

        public string AspNetUserID { get; set; }

        public long CustomerID { get; set; }
    }

    public class IncompleteUsers
    {
        public string UserId { get; set; }
        public long Id { get; set; }
        public string UserName { get; set; }
        public string AspNetUserID { get; set; }
        public long CustomerID { get; set; }
    }


    public class OpenDataBaseConnection
    {
        public long Spid { get; set; }
        public string Status { get; set; }
        public string Login { get; set; }
        public string hostname { get; set; }
        public long BlkBy { get; set; }

        public string DBName { get; set; }
        public string Command { get; set; }
        public long CPUTime { get; set; }


        public long DiskIO { get; set; }
        public string LastBatch { get; set; }
        public string ProgramName { get; set; }
    }
    public class RanksDataUpdateViewModel
    {
        public int ID { get; set; }
        public string RankName { get; set; }
        public Int32 CompareCountLegs { get; set; }
        public Int32 PerLegCappedCustomerCount { get; set; }
        public Int32 CappedCustomerCount { get; set; }
        public Int32 LegsWithRank3Rank4 { get; set; }
        public Int32 CheckRankLegTotal { get; set; }
        public Int32 NoOfRankingLeg { get; set; }
        public Int32? Display1 { get; set; }
        public Int32? Display2 { get; set; }
        public Int32? Display3 { get; set; }
        public Int32? Display4 { get; set; }
    }
    public class PaymentMethodsViewModel
    {
        public int Id { get; set; }
        public string paymentName { get; set; }
        public string tb_field1 { get; set; }
        public string tb_field2 { get; set; }
        public string tb_field3 { get; set; }
        public string tb_field4 { get; set; }
        public string tb_field5 { get; set; }
        public string tb_field6 { get; set; }
        public bool paymentName_req { get; set; }
        public bool field1_req { get; set; }
        public bool field2_req { get; set; }
        public bool field3_req { get; set; }
        public bool field4_req { get; set; }
        public bool field5_req { get; set; }
        public bool field6_req { get; set; }
        public bool Active { get; set; }
    }

    public class UserPaymentMethod
    {
        public int UserProfileId { get; set; }
        public int PaymentMethodId { get; set; }
        public string tb_field1 { get; set; }
        public string tb_field2 { get; set; }
        public string tb_field3 { get; set; }
        public string tb_field4 { get; set; }
        public string tb_field5 { get; set; }
        public string tb_field6 { get; set; }
    }
    public class UserPaymentMethodListModel
    {
        public int UserProfileId { get; set; }
        public int PaymentMethodId { get; set; }
        public string Name { get; set; }
        public string Field1 { get; set; }
        public string Field2 { get; set; }
        public string Field3 { get; set; }
        public string Field4 { get; set; }
        public string Field5 { get; set; }
        public string Field6 { get; set; }
        public bool Active { get; set; }
        public bool Verified { get; set; }
    }
    public class UserQualModel
    {
        public int Days { get; set; }
        public int UserQualOrderExpiryInDays { get; set; }
    }

}
