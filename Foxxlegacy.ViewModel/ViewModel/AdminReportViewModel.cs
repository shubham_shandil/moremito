﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class AdminReportViewModel
    {

        [Required(ErrorMessage = "UserName is Required")]
        public string UserName { get; set; }
        public List<CSJEntries> csjEntries { get; set; }
        public List<Correspondence> correspondence { get; set; }
        public List<PinCertificateListViewModel> PinCertificateList { get; set; }
        public List<CallListViewModel> CallList { get; set; }
        public List<NewUsers> newUsers { get; set; }

        public List<EmailExport> EmailExport { get; set; }

        public CustomerEmailsbyOrderCondition CustomerEmailsbyOrderCondition { get; set; }


        public List<ItemByState> ItemByState { get; set; }

        public List<TotalsbyState> TotalsbyState { get; set; }


        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]

        [Required(ErrorMessage = "From Date Required")]
        public DateTime MinimumDate { get; set; }

        public DateTime MaximumDate { get; set; }

        [Required(ErrorMessage = "From Date Required")]
        public DateTime MinimumDateForProduct { get; set; }

        [Required(ErrorMessage = "To Date Required")]
        public DateTime MaximumDateForProduct { get; set; }

        public List<ItemByDate> ItemByDate { get; set; }

        public List<ItemByProduct> ItemByProduct { get; set; }


        [Required(ErrorMessage = "Select a Product ")]
        public long ProductId { get; set; }

        public IEnumerable<SelectListItem> ProductList { get; set; }

        public List<MapsViewModel> MapsViewModel { get; set; }

        public List<MapsOrders> MapsOrders { get; set; }

        public List<MapsJoins> MapsJoins { get; set; }

        public long UsersId { get; set; }


        [Required(ErrorMessage = "From Date Required")]
        public DateTime MinimumDateForOrder { get; set; }

        [Required(ErrorMessage = "To Date Required")]
        public DateTime MaximumDateForOrder { get; set; }

        [Required(ErrorMessage = "From Date Required")]
        public DateTime MinimumDateForJoin { get; set; }

        [Required(ErrorMessage = "To Date Required")]
        public DateTime MaximumDateForJoin { get; set; }

        public long Level { get; set; }

        public List<MaxLevelUserCounts> MaxLevelUserCounts { get; set; }

        public List<Usercountsbymaxlevel> Usercountsbymaxlevel { get; set; }

        public List<UsercountsbymaxlevelDetails> UsercountsbymaxlevelDetails { get; set; }


        public List<SweepReport> SweepReport { get; set; }


        public List<AutoShipCounts> AutoShipCounts { get; set; }


        public List<OutOfStockItem> OutOfStockItem { get; set; }

        public List<OutOfStockItemUserDetils> OutOfStockItemUserDetils { get; set; }

        public List<OutOfStockItemOrderDetails> OutOfStockItemOrderDetails { get; set; }
        public AdminReportViewModel()
        {
            csjEntries = new List<CSJEntries>();
            correspondence = new List<Correspondence>();
            PinCertificateList = new List<PinCertificateListViewModel>();
            CallList = new List<CallListViewModel>();
            newUsers = new List<NewUsers>();
            EmailExport = new List<EmailExport>();
            CustomerEmailsbyOrderCondition = new CustomerEmailsbyOrderCondition();
            ItemByState = new List<ItemByState>();
            TotalsbyState = new List<TotalsbyState>();
            ItemByDate = new List<ItemByDate>();
            MapsViewModel = new List<MapsViewModel>();
            ItemByProduct = new List<ItemByProduct>();
            MapsOrders = new List<MapsOrders>();
            MapsJoins = new List<MapsJoins>();
            MaxLevelUserCounts = new List<MaxLevelUserCounts>();
            Usercountsbymaxlevel = new List<Usercountsbymaxlevel>();
            UsercountsbymaxlevelDetails = new List<UsercountsbymaxlevelDetails>();
            SweepReport = new List<SweepReport>();
            AutoShipCounts = new List<AutoShipCounts>();
            OutOfStockItem = new List<OutOfStockItem>();
            OutOfStockItemUserDetils = new List<OutOfStockItemUserDetils>();
            OutOfStockItemOrderDetails = new List<OutOfStockItemOrderDetails>();
        }
    }
    public class CSJEntries
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }
        public DateTime? JournalEntryDate { get; set; }
        public DateTime? LastModDate { get; set; }
        public string LastModAdmin { get; set; }
    }
    public class Correspondence
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string Origin { get; set; }
        public string Subject { get; set; }
        public DateTime EventDate { get; set; }
        public DateTime AdminReviewNeededDate { get; set; }
        public string UserName { get; set; }
        public bool CSRArchived { get; set; }
    }
    public class NewUsers
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public DateTime JoinDate { get; set; }
        public string SignupType { get; set; }
        public string Sponsor { get; set; }
        public string SponSpon { get; set; }
    }


    public class EmailExport
    {
        public long UserId { get; set; }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }

        public string Email { get; set; }
        public long SponsorUsersID { get; set; }
        public string SponsorUsersName { get; set; }
        public string PlacementUsersName { get; set; }


        public long OptOut_SMSCount { get; set; }

        public string MembershipType { get; set; }


        public string PersonAbove { get; set; }

        public string Enroller { get; set; }

        public string Sms { get; set; }




        public string SmsOptOutStatus { get; set; }
    }


    public class EmailExportNoOrder
    {

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string MembershipType { get; set; }
    }


    public class CustomerEmailsbyOrderCondition
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string InfoLink { get; set; }
        public string BuyLink { get; set; }

        public long ReportType { get; set; }
    }


    public class ItemByState
    {
        public long TheCountByItems { get; set; }

        public string ShipstateByItems { get; set; }

        public string Name { get; set; }

        public string Itemno { get; set; }




    }

    public class TotalsbyState
    {
        public long TheCountByState { get; set; }

        public string ShipstateByState { get; set; }
    }

    public class ItemByDate
    {
        public long TheCountByItems { get; set; }

        public string Name { get; set; }

        public string Itemno { get; set; }

        public long ProductQuantity { get; set; }


    }

    public class ItemByProduct
    {
        public long TheCountByItems { get; set; }

        public string Name { get; set; }

        public string Itemno { get; set; }

        public long ProductQuantity { get; set; }


    }
    public class MapsViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string MapType { get; set; }



    }

    public class MapsOrders
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string MapType { get; set; }



    }
    public class MapsJoins
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string MapType { get; set; }



    }


    public class MaxLevelUserCounts
    {
        public long NumberOfUsersWhich { get; set; }
        public long HaveUsersAsDeepAsLevel { get; set; }
        public string UserName { get; set; }
        public string MaxLevel { get; set; }

        public long UserId { get; set; }
    }

    public class Usercountsbymaxlevel
    {
        public long NumberOfUsersWhich { get; set; }
        public long HaveUsersAsDeepAsLevel { get; set; }
    }
    public class UsercountsbymaxlevelDetails
    {
        public string UserName { get; set; }
        public string MaxLevel { get; set; }

        public long UserId { get; set; }
    }


    public class SweepReport
    {

        public long Id { get; set; }
        public long UserId { get; set; }

        public string AccountNumber { get; set; }

        public string ExtraID { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public decimal Amount { get; set; }

        public string CreateDate { get; set; }

        public string Description { get; set; }


        #region Export to Excell Properties


        public long Identifier { get; set; }

        public string Profile { get; set; }
        public string ExternalAccount { get; set; }
        public string Preference { get; set; }

        public string PrepaidCard { get; set; }

        public decimal Payment { get; set; }



        #endregion
    }

    public class AutoShipCounts
    {
        public string Name { get; set; }

        public long SumofQtyonAutoship { get; set; }
    }


    public class OutOfStockItem
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public string itemno { get; set; }

        public long Qty { get; set; }

        public long Count { get; set; }
    }

    public class OutOfStockItemUserDetils
    {
        public long UserId { get; set; }
        public string UserName { get; set; }

        public string FullName { get; set; }

        public long AutoshipDirectivesID { get; set; }


    }


    public class OutOfStockItemOrderDetails
    {
        public long Id { get; set; }
        public string ItemName { get; set; }

        public long ItemQuantity { get; set; }

        public long Qty { get; set; }
        public decimal Price { get; set; }


    }

    public class SweepDetailsExportToExcel

    {
        public long Identifier { get; set; }

        public string Profile { get; set; }
        public string ExternalAccount { get; set; }
        public string Preference { get; set; }

        public string PrepaidCard { get; set; }

        public string Payment { get; set; }
    }


    public class Contest
    {
        public long SponsorUsersID { get; set; }

        public long TheCount { get; set; }

        public string UserName { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }
      
    }
}
