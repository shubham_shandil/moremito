﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class MyPassUpOrdersViewModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }

        public string Orderno { get; set; }

        public string Orderdate { get; set; }

        public string Status { get; set; }

        public string OrderOwner { get; set; }

        public string OrderCV { get; set; }
        public string ExpDate { get; set; }
        public bool PassUp { get; set; }
        public string RSC { get; set; }
    }
}
