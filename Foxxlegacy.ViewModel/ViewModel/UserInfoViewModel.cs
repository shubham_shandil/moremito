﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserInfoViewModel
    {
        public long ID { get; set; }

        public UserPersonalInformationViewModel userPersonalInformationViewModel { get; set; }

        public UserInfoViewModel()
        {
            userPersonalInformationViewModel = new UserPersonalInformationViewModel();
        }
    }
}
