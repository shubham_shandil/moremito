﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserRoleListViewModel
    {
        public string RoleName { get; set; }
        public string RoleId { get; set; }

        public string UserId { get; set; }

        public string UserName { get; set; }
        public bool Check { get; set; }

    }

    public class UserRoleDetailsListViewModel

    {
        public long ID { get; set; }//USERID
        public List<UserRoleListViewModel> userRoleListViewModel { get; set; }        

        public List<bool> IsUserInRoleList { get; set; }

        public List<string> RoleNameList { get; set; }

        public List<string> RoleIdList { get; set; }

        public List<string> UserIdList { get; set; }

        public List<string> UserNameList { get; set; }

        public string UserName { get; set; }
        public UserRoleDetailsListViewModel()
        {
            userRoleListViewModel = new List<UserRoleListViewModel>();
            IsUserInRoleList = new List<bool>();
            RoleNameList = new List<string>();
            RoleIdList = new List<string>();
            UserIdList = new List<string>();
            UserNameList = new List<string>();
        }
    }
}
