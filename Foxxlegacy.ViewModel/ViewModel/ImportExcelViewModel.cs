﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ImportExcelViewModel
    {

        [Required(ErrorMessage = "Please select file")]
        //[FileExt(Allow = ".xls,.xlsx", ErrorMessage = "Please upload only .xlsx or .xls file.")]

        [FileExt(ErrorMessage = "Please upload only .xlsx or .xls file.")]
        public HttpPostedFileBase file { get; set; }
    }
}
