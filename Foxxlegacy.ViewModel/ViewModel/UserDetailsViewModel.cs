﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserDetailsViewModel
    {
        public long UserId { get; set; }
        [Required(ErrorMessage = "Please enter user name.")]
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ReturnUrl { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }



        public long RecipientUserID { get; set; }

        public bool AdminBlockSweep { get; set; }

        public long HasW9 { get; set; }

        public long UserCount { get; set; }

        public long EmailCount { get; set; }
        public int UniLevelSponsorID { get; set; }
        public bool RetailOnly { get; set; }
    }
}
