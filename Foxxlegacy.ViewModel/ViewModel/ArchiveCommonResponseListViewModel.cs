﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ArchiveCommonResponseListViewModel
    {
        public long ID { get; set; }

        public string EventDate { get; set; }

        public string Origin { get; set; }

        public string Name { get; set; }

        public long FromUsersID { get; set; }

        public string subject { get; set; }

        public string UserName { get; set; }

        public long DialogueCount { get; set; }
    }
}
