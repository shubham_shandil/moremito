﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class MyLeadsViewModel
    {
        public LeadsContentData leadsContentValue { get; set; }        
        public MyLeadsViewModel()
        {
            leadsContentValue = new LeadsContentData();
        }
    }
    public class LeadsContentData
    {
        public string LeadsContentValue { get; set; }
    }
}
