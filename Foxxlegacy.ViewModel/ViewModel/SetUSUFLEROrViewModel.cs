﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class SetUSUFLEROrViewModel
    {
        public string Username { get; set; }

        public string UsersSponsor { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Rank { get; set; }

        public string OldRank { get; set; }
    }
}
