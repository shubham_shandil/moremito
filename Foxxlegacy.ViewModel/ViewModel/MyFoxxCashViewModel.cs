﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class MyFoxxCashViewModel
    {


        public decimal Balance { get; set; }

        public decimal Earned { get; set; }

        public decimal Swept { get; set; }

        public List<MyFoxxCashListViewModel> myFoxxCashViewModelList { get; set; }

        public MyFoxxCashViewModel()
        {
            myFoxxCashViewModelList = new List<MyFoxxCashListViewModel>();
        }
    }

    public class MyFoxxCashListViewModel
    {
        public decimal RetVal1 { get; set; }
        public decimal RetVal2 { get; set; }
        public decimal RetVal3 { get; set; }
    }
    public class TransferCashViewModel
    {
        public int TargetUserId { get; set; }
        public decimal Amount { get; set; }
        public int CurrentUserId { get; set; }
        public string TransferType { get; set; }
        public string Message { get; set; }
    }
    public class TransferHistoryModel
    {
        public List<CashRecievedModel> RecievedList { get; set; }
        public List<CashRecievedModel> TransferList { get; set; }
        public decimal TotalReceived { get; set; }
        public decimal TotalTransfer { get; set; }
    }
    public class CashRecievedModel
    {
        public string Username { get; set; }
        public string UName { get; set; }
        public decimal Transferamount { get; set; }
        public DateTime Transferdate { get; set; }
        public string Message { get; set; }
    }
}
