﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class StatusViewModel
    {
        public long error_code { get; set; }
        public string message { get; set; }
    }
}
