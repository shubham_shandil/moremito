﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class GrandFathersListViewModel
    {
        public long Id { get; set; }

        public long UsersID { get; set; }
        public string CreateDate { get; set; }
        public string ExpireDate { get; set; }
        public string Reason { get; set; }
       
        public string RankName { get; set; }
    }
}
