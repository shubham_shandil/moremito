﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ContentViewModel
    {
        public string PoliciesAndProcedures { get; set; }

        public string TermsofService { get; set; }


        public string PaymentMethods { get; set; }


        public string ReturnPolicy { get; set; }

        public string PrivacyPolicy { get; set; }
    }
}
