﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ContactUsViewModel
    {
        [Required(ErrorMessage = "Please enter  name.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please enter email address.")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public string EmailId { get; set; }
        [Required(ErrorMessage = "Please enter phone.")]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Please enter a valid Phone Numbe")]
        public string Phone { get; set; }
        [Required(ErrorMessage = "Please enter sublect.")]
        public string Subject { get; set; }
        [Required(ErrorMessage = "Please enter subject.")]
        public string Message { get; set; }


        public long ID { get; set; }
        public long UsersID { get; set; }
        public long FromUsersID { get; set; }
        public string Origin { get; set; }
        public string IpAddress { get; set; }
        public string Notes { get; set; }

        public string Dialogue { get; set; }
        public string CreateDate { get; set; }

        public bool IsAdmin { get; set; }

        public string EventDate { get; set; }

        public string Details { get; set; }
       
        public List<ContactUsListViewModel> contactUsListViewModel { get; set; }
        public ContactUsListViewModel AddyourCommentsViewModel { get; set; }
        public ContactUsViewModel()
        {
            contactUsListViewModel = new List<ContactUsListViewModel>();
            AddyourCommentsViewModel = new ContactUsListViewModel();
        }


    }
}
