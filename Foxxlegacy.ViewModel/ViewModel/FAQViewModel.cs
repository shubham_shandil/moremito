﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class FAQViewModel
    {
        public long Id { get; set; }
        public string CatName { get; set; }

       

        public List<FAQCategoryListViewModel> fAQCategoryListViewModel = new List<FAQCategoryListViewModel>();

        public FAQViewModel()
        {
            
            fAQCategoryListViewModel = new List<FAQCategoryListViewModel>();
        }
    }
    public class FAQCategoryListViewModel
    {
        public long Id { get; set; }
        public string CatName { get; set; }
        public List<FAQListViewModel> FAQList { get; set; }

        public FAQCategoryListViewModel()
        {
            FAQList = new List<FAQListViewModel>();

        }
    }
}
