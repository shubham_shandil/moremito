﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class TreeOrganicPlacementViewModel
    {
        public MainList mainList { get; set; }
        public List<SubMainList> subMainList { get; set; }
        public List<ChildMainList> childMainList { get; set; }
        public TreeOrganicPlacementViewModel()
        {
            mainList = new MainList();
            subMainList = new List<SubMainList>();
            childMainList = new List<ChildMainList>();
        }
    }

    //public class MainList
    //{
    //    public long Id { get; set; }
    //    public string UserName { get; set; }
    //    public string Text { get; set; }
    //    public long ChildNodeCount { get; set; }
    //}
    //public class SubMainList
    //{
    //    public long Id { get; set; }
    //    public string UserName { get; set; }
    //    public string Text { get; set; }
    //    public long ChildNodeCount { get; set; }
    //}
    //public class ChildMainList
    //{
    //    public long Id { get; set; }
    //    public string UserName { get; set; }
    //    public string Text { get; set; }
    //    public long ChildNodeCount { get; set; }
    //    public long SponsorUsersID { get; set; }
    //}
}
