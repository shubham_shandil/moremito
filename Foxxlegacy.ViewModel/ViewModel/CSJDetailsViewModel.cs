﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class CSJDetailsViewModel
    {
        public long UserId { get; set; }

        public string Comments { get; set; }
        public string UserName { get; set; }
        public string JournalEntryDate { get; set; }

        public string LastModDate { get; set; }

        public string LastModAdmin { get; set; }
        public int AdminId { get; set; }

        public string MyRecord { get; set; }

        public string JournalHistoricalRecords { get; set; }

       
    }
    public class NotFoundModel
    {
        public int ID { get; set; }
        public string LostUser { get; set; }
        public string Description { get; set; }
        public string EventDate { get; set; }
        public string IpAddress { get; set; }
    }

   
}
