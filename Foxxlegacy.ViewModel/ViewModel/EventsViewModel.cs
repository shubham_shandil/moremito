﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class EventsViewModel
    {
        public string WebContentName { get; set; }
        public string Content { get; set; }
        public EventsListViewModel eventsListViewModel = new EventsListViewModel();
        public EventsViewModel()
        {
            eventsListViewModel = new EventsListViewModel();
        }
    }
}
