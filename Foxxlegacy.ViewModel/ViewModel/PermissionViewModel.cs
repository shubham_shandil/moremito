﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class PermissionViewModel
    {
        public List<Member> _member { get; set; }
        public PermissionViewModel()
        {
            _member = new List<Member>();
        }
    }
    public class Member
    {        
        public string LoweredUserName { get; set; }
        public string Email { get; set; }
    }
}
