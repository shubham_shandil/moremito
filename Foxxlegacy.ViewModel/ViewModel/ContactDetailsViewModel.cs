﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class ContactDetailsViewModel
    {
        public long ContactID { get; set; }
        public long UsersID { get; set; }
        public string Dialogue { get; set; }

        public long DialogueId { get; set; }

        public string Notes { get; set; }

        public IEnumerable<SelectListItem> DialogueList { get; set; }

        public List<ContactNotesListViewModel> contactNotesListViewModel { get; set; }
        public ContactDetailsViewModel()
        {
            contactNotesListViewModel = new List<ContactNotesListViewModel>();

        }
    }

   
}
