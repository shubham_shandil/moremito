﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class CommissionsSweptViewModel
    {
        public long UsersID { get; set; }
        public DateTime CreateDate { get; set; }
        public long CommissionProfilesID { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public bool Paid { get; set; }
        public DateTime PaidDate { get; set; }


    }
}
