﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class RecurringOrderViewModel
    {

        public Int32 AdId { get; set; }
        public Int32 UserId { get; set; }
        public Boolean Status { get; set; }
        public string NickName { get; set; }
        public Boolean UseFoxxCash { get; set; }
        public Int32 PaymentMethodId { get; set; }
        public Int32 Id { get; set; }
        public Int32 Qty { get; set; }
        public string CustomerProfileId { get; set; }
        public string CustomerPaymentID { get; set; }
        public string CustomerAddressId { get; set; }
        public IEnumerable<SelectListItem> StateList { get; set; }
        public IEnumerable<SelectListItem> AutoshipPeriodList { get; set; }
        public List<SelectListItem> AddressList { get; set; }
        public List<ProductListFromNopCom> ProductListFromNopCom { get; set; }
        public List<ProductListFoxx> ProductListFoxx { get; set; }
        public IEnumerable<SelectListItem> NickNamePaymentMethodList { get; set; }
        public IEnumerable<SelectListItem> NickNameShipingAddressList { get; set; }
        public RecurringPaymentMethod PaymentMethod { get; set; }
        public RecurringShippingAddress ShippingAddress { get; set; }
        public int OnceEvery { get; set; }
        public string StartingOn { get; set; }
        public string NextOrderDate { get; set; }
        public int AutoshipPeriodId { get; set; }

    }

    public class ProductListFromNopCom
    {
        public Int32 Id { get; set; }
        public Boolean IsCheck { get; set; }
        public Int32 ProductId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Int32 Qty { get; set; }
        public Boolean Taxable { get; set; }
    }

    public class ProductListFoxx
    {
        public Int32 Id { get; set; }
        public Int32 ProductId { get; set; }
        public Int32 Qty { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public Boolean Taxable { get; set; }
    }

    public class RecurringPaymentMethod
    {
        public Int32 Id { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string CCNo { get; set; }     
        public string ExpDate { get; set; }
        public string Type { get; set; }
        public string RoutingNumber { get; set; }
        public string CVV { get; set; }
        public string CustomerId { get; set; }

    }
    public class RecurringShippingAddress
    {
        public Int32 Id { get; set; }
        public string NickName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Country { get; set; }
     }
}
