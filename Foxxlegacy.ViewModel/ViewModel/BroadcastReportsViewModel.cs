﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class BroadcastReportsViewModel
    {
        public long UnSendEmail { get; set; }
        public long SendEmail { get; set; }
        public long UnSendSms { get; set; }
        public long SendSms { get; set; }
    }
}
