﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class DynamicPagesViewModel
    {
        public PetHealth petHealth { get; set; }
        public PeopleElevate peopleElevate { get; set; }
        public PeopleElevatePerformance peopleElevatePerformance { get; set; }
        public PeopleElevateMind peopleElevateMind { get; set; }
        public UltraMitoMobility ultraMitoMobility { get; set; }
        public UltraMitoAllergy ultraMitoAllergy { get; set; }
        public UltraMitoRecovery ultraMitoRecovery { get; set; }
        public UltraMitoLife ultraMitoLife { get; set; }
        public DynamicPagesViewModel()
        {
            petHealth = new PetHealth();
            peopleElevate = new PeopleElevate();
            peopleElevatePerformance = new PeopleElevatePerformance();
            peopleElevateMind = new PeopleElevateMind();
            ultraMitoMobility = new UltraMitoMobility();
            ultraMitoAllergy = new UltraMitoAllergy();
            ultraMitoRecovery = new UltraMitoRecovery();
            ultraMitoLife = new UltraMitoLife();
        }
    }
    public class PetHealth
    {
        public string PetHealthProducts { get; set; }         
    }
    public class PeopleElevate
    {
        public string Front_PeopleElevate1 { get; set; }
        public string Front_PeopleElevate2 { get; set; }
    }
    public class PeopleElevatePerformance
    {
        public string Front_PeopleElevatePerformance1 { get; set; }
        public string Front_PeopleElevatePerformance2 { get; set; }
    }
    public class PeopleElevateMind
    {
        public string Front_PeopleElevateMind1 { get; set; }
        public string Front_PeopleElevateMind2 { get; set; }
    }
    public class UltraMitoMobility
    {
        public string UltraMitoMobilityProducts1 { get; set; }
        public string UltraMitoMobilityProducts2 { get; set; }
    }
    public class UltraMitoAllergy
    {
        public string Front_UltraMitoAllergy1 { get; set; }
        public string Front_UltraMitoAllergy2 { get; set; }
    }
    public class UltraMitoRecovery
    {
        public string Front_UltraMitorecovery1 { get; set; }
        public string Front_UltraMitorecovery2 { get; set; }
    }
    public class UltraMitoLife
    {
        public string Front_UltraMitoLife1 { get; set; }
        public string Front_UltraMitoLife2 { get; set; }
    }
}
