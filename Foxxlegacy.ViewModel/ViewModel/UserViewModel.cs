﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserViewModel
    {
        public long UserId { get; set; }
        public Int32 NewUserId { get; set; }
        public Int32 OrderID { get; set; }
        public Int32 SponsorID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "User Name field is required.")]
        public string UserName { get; set; }
       
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Email field is required.")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Customer Phone field is required.")]
        [RegularExpression(@"^([0-9]{10})$", ErrorMessage = "Please enter a valid Phone Numbe")]
        public string Phone { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "First Name field is required.")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Last Name field is required.")]
        public string LastName { get; set; }
        public long RecipientUserID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Address field is required.")]
        public string Address { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "City field is required.")]
        public string City   { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Country field is required.")]
        public int CountryId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "State field is required.")]
        public string State { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ZipCode field is required.")]
        public string ZipCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password field is required.")]
        public string Password { get; set; }
        public Int32 SignupType { get; set; }
        public Guid GuiId { get; set; }
        public IEnumerable<SelectListItem> StateList { get; set; }
        public IEnumerable<SelectListItem> StateNewList { get; set; }


        public string FullName { get; set; }

        public string Content { get; set; }

        public string SignUpBelowURL { get; set; }


        public string cbAgree { get; set; }

        public int IsCustomerAUser { get; set; }


        public bool CheckboxTermCondition { get; set; }
        public bool? RetailOnly { get; set; }
    }

    public class UserRankViewModel
    {
        public int Id { get; set; }
        public string RankName { get; set; }
    }
    public class GuestJoinModel
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateId { get; set; }
        public int CountryId { get; set; }
        public string SponsorName { get; set; }
        public string Zip { get; set; }
        public string PageType { get; set; }
        public string CountryStateName { get; set; }
    }
    public class GuestResponseModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int IsAlreadyARetailUser { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
    }
    public class CountryStateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbr { get; set; }
    }
    public class CountryStateIdModel
    {
        public int CountryId { get; set; }
        public int Stateid { get; set; }
        public string Statename { get; set; }
        public string StateAbbr { get; set; }
        public string countryName { get; set; }
    }
}
