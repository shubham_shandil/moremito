﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class InserUpdateResponseViewModel
    {

        public Int32 Id { get; set; }
        public string Message { get; set; }
    }
}
