﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class AdminMEditorsViewModel
    {

        public BrandsViewModel BrandsViewModel { get; set; }
        public List<BrandsListViewModel> BrandsListViewModel { get; set; }


        public ProductsViewModel ProductsViewModel { get; set; }

        public IEnumerable<SelectListItem> BrandList { get; set; }

        public IEnumerable<SelectListItem> BrandsforthisProductdList { get; set; }

        public List<ProductsListViewModel> ProductsListViewModel { get; set; }

        public IEnumerable<SelectListItem> ProductList { get; set; }


        public SiteViewModel SiteViewModel { get; set; }


        public List<SiteListViewModel> SiteListViewModel { get; set; }

        public IEnumerable<SelectListItem> BrandsforthisSiteList { get; set; }



        public MethodsViewModel MethodsViewModel { get; set; }

        public List<MethodsListViewModel> MethodsListViewModel { get; set; }

        public MarketingResourcesViewModel ResourcesViewModel { get; set; }

        public List<MarketingResourcesListViewModel> ResourcesListViewModel { get; set; }


        public IEnumerable<SelectListItem> SiteList { get; set; }
        public IEnumerable<SelectListItem> MethodList { get; set; }


        public QRDataForMarketingResource QRDataForResource { get; set; }

        public List<QRDataListForMarketingResource> QRDataListForResource { get; set; }


        public MarketingResourceVideoPages MarketingResourceVideoPages { get; set; }


        public List<MarketingResourceListVideoPages> MarketingResourceListVideoPages { get; set; }

        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public AdminMEditorsViewModel()
        {
            BrandsViewModel = new BrandsViewModel();
            BrandsListViewModel = new List<BrandsListViewModel>();
            ProductsViewModel = new ProductsViewModel();
            ProductsListViewModel = new List<ProductsListViewModel>();
            SiteViewModel = new SiteViewModel();
            SiteListViewModel = new List<SiteListViewModel>();
            MethodsViewModel = new MethodsViewModel();
            MethodsListViewModel = new List<MethodsListViewModel>();
            ResourcesViewModel = new MarketingResourcesViewModel();
            ResourcesListViewModel = new List<MarketingResourcesListViewModel>();
            QRDataForResource = new QRDataForMarketingResource();
            QRDataListForResource = new List<QRDataListForMarketingResource>();
            MarketingResourceVideoPages = new MarketingResourceVideoPages();
            MarketingResourceListVideoPages = new List<MarketingResourceListVideoPages>();
        }
    }

    public class BrandsViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal AppDisplayOrder { get; set; }
        [Required]
        public string AppImgUrl { get; set; }

        [Required]
        public string EditName { get; set; }
    }


    public class ProductsViewModel
    {
        public long ID { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Select a Brand")]
        public long MBrandID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Display Order is Required")]
        public decimal AppDisplayOrder { get; set; }
        [Required]
        public string AppImgUrl { get; set; }

        public long MBrandID2 { get; set; }

        public string ProductName { get; set; }

        public int[] SelectedMBrandID { get; set; }
    }


    public class ProductsListViewModel
    {
        public long ID { get; set; }


        public long MBrandID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public decimal AppDisplayOrder { get; set; }

        public string AppImgUrl { get; set; }


    }


    public class SiteListViewModel
    {
        public long ID { get; set; }
        public long ProductID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string FQDN { get; set; }

        public string RootDomainName { get; set; }


    }

    public class SiteViewModel
    {
        public long ID { get; set; }
        public long ProductID { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Description  is Required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Fully Qualified Domain Name  is Required")]
        public string FQDN { get; set; }

        [Required(ErrorMessage = "Root Domain  is Required")]
        public string RootDomainName { get; set; }

        [Required(ErrorMessage = "Select a Product")]
        public long ProductID2 { get; set; }

        public string SiteName { get; set; }

        public int[] SelectedProductID { get; set; }
    }


    public class MethodsViewModel
    {
        public long ID { get; set; }


        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }


        [Required(ErrorMessage = "Name is Required")]
        public string EditName { get; set; }

        [Required(ErrorMessage = "Description  is Required")]
        public string Description { get; set; }


    }

    public class MethodsListViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }


    }


    public class MarketingResourcesViewModel
    {
        public long ID { get; set; }

        [Required(ErrorMessage = "Select a Site")]
        public long SitesID { get; set; }

        [Required(ErrorMessage = "Select a Method")]
        public long MethodsID { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        public string Description { get; set; }



        [Required(ErrorMessage = "Url code is Required")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Destination is Required")]
        public string DestinationURL { get; set; }

        [Required(ErrorMessage = "App image url is Required")]
        public string AppImgUrl { get; set; }

        [Required(ErrorMessage = "App sms message is Required")]
        public string AppSMSMessage { get; set; }

        [Required(ErrorMessage = "App Email Subject is Required")]
        public string AppEmailSubject { get; set; }

        [Required(ErrorMessage = "App Email Body is Required")]
        public string AppEmailBody { get; set; }


        public decimal AppDisplayOrder { get; set; }

        [Required(ErrorMessage = "PDF Source is Required")]
        public string PDFSource { get; set; }

        public bool PdfHasForm { get; set; }

        [Required(ErrorMessage = "VideoCode is Required")]
        public string VideoCode { get; set; }

        [Required(ErrorMessage = "Preview URL is Required")]
        public string PreviewURL { get; set; }
    }

    public class MarketingResourcesListViewModel
    {
        public long ID { get; set; }


        public long SitesID { get; set; }


        public long MethodsID { get; set; }


        public string Name { get; set; }


        public string Description { get; set; }




        public string Code { get; set; }


        public string DestinationURL { get; set; }


        public string AppImgUrl { get; set; }


        public string AppSMSMessage { get; set; }


        public string AppEmailSubject { get; set; }


        public string AppEmailBody { get; set; }


        public decimal AppDisplayOrder { get; set; }


        public string PDFSource { get; set; }

        public bool PdfHasForm { get; set; }


        public string VideoCode { get; set; }


        public string PreviewURL { get; set; }
    }


    public class QRDataForMarketingResource
    {
        public long QRDataForResourceId { get; set; }
        public long SourcesID { get; set; }

        [Required(ErrorMessage = "URL is Required")]
        public string URL { get; set; }

        [Required(ErrorMessage = "Pdf Page is Required")]
        public long PdfPage { get; set; }
        public long Xpx { get; set; }
        public long Ypx { get; set; }
        public string Color { get; set; }
        public string BackColor { get; set; }
        public long VersionSize { get; set; }
        public long Spacing { get; set; }

        [Required(ErrorMessage = "URL is Required")]
        public string EditURL { get; set; }
    }


    public class QRDataListForMarketingResource
    {
        public long QRDataForResourceId { get; set; }
        public long SourcesID { get; set; }
        public string URL { get; set; }

        public long PdfPage { get; set; }
        public long Xpx { get; set; }
        public long Ypx { get; set; }
        public string Color { get; set; }
        public string BackColor { get; set; }
        public long VersionSize { get; set; }
        public long Spacing { get; set; }
    }


    public class MarketingResourceVideoPages
    {
        public long ID { get; set; }


        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Category is Required")]
        public string Category { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        public string Description { get; set; }


        public string VideoHTML { get; set; }
        public string BodyHTML { get; set; }
        [Required(ErrorMessage = "Info Button Url is Required")]
        public string InfoButtonURL { get; set; }
        [Required(ErrorMessage = "Buy Button Url is Required")]
        public string BuyButtonURL { get; set; }

        public bool Active { get; set; }

        public string CategoryDropdown { get; set; }


    }


    public class MarketingResourceListVideoPages
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public string Category { get; set; }
        public string Description { get; set; }
        public string VideoHTML { get; set; }
        public string BodyHTML { get; set; }
        public string InfoButtonURL { get; set; }
        public string BuyButtonURL { get; set; }
        public bool Active { get; set; }

    }
}
