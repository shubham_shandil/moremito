﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class TextNotificationSettings
    {
        public long OptOutCatID { get; set; }
        public string Description { get; set; }
        public string OptOutCatName { get; set; }

        public string IsOptedIn { get; set; }
    }
    public class SmsSettingModel
    {
        public string Phone { get; set; }
        public string FormattedPhone { get; set; }
        public bool? IsBroadCastOptOut { get; set; }
        public PlivoOptInOutReportModel BroadCastOpt { get; set; }
        public List<TextNotificationSettings> list { get; set; }
      public  List<TextNotificationSettings> OptList { get; set; }
    }
}
