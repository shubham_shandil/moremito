﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class MobileApp
    {
        public MobileContentData mobileContentData { get; set; }
        public MobileApp()
        {
            mobileContentData = new MobileContentData();
        }
    }
    public class MobileContentData
    {
        public string WebContentValue { get; set; }
    }
   
}
