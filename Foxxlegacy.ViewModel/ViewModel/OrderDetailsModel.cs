﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{

    public class OrderDetailsModel
    {

        public OrderDetailsModel()
        {
            TaxRates = new List<TaxRate>();
            GiftCards = new List<GiftCard>();
            Items = new List<OrderItemModel>();
            OrderNotes = new List<OrderNote>();
            Shipments = new List<ShipmentBriefModel>();
            myOrders = new MyOrders();
            BillingAddress = new AddressModel();
            ShippingAddress = new AddressModel();
            PickupAddress = new AddressModel();

            CustomValues = new Dictionary<string, object>();
        }
        public long Id { get; set; }
        public bool PrintMode { get; set; }
        public bool PdfInvoiceDisabled { get; set; }

        public string CustomOrderNumber { get; set; }

        public DateTime CreatedOn { get; set; }

        public string OrderStatus { get; set; }

        public bool IsReOrderAllowed { get; set; }

        public bool IsReturnRequestAllowed { get; set; }

        public bool IsShippable { get; set; }
        public bool PickupInStore { get; set; }
        public AddressModel PickupAddress { get; set; }
        public string ShippingStatus { get; set; }
        public AddressModel ShippingAddress { get; set; }
        public string ShippingMethod { get; set; }
        public IList<ShipmentBriefModel> Shipments { get; set; }

        public AddressModel BillingAddress { get; set; }

        public string VatNumber { get; set; }

        public string PaymentMethod { get; set; }
        public string PaymentMethodStatus { get; set; }
        public bool CanRePostProcessPayment { get; set; }
        public Dictionary<string, object> CustomValues { get; set; }

        public string OrderSubtotal { get; set; }
        public string OrderSubTotalDiscount { get; set; }
        public string OrderShipping { get; set; }
        public string PaymentMethodAdditionalFee { get; set; }
        public string CheckoutAttributeInfo { get; set; }

        public bool PricesIncludeTax { get; set; }
        public bool DisplayTaxShippingInfo { get; set; }
        public string Tax { get; set; }
        public IList<TaxRate> TaxRates { get; set; }
        public bool DisplayTax { get; set; }
        public bool DisplayTaxRates { get; set; }

        public string OrderTotalDiscount { get; set; }
        public int RedeemedRewardPoints { get; set; }
        public string RedeemedRewardPointsAmount { get; set; }
        public decimal OrderTotal { get; set; }

        public IList<GiftCard> GiftCards { get; set; }

        public bool ShowSku { get; set; }
        public IList<OrderItemModel> Items { get; set; }
        public MyOrders myOrders { get; set; }
        public IList<OrderNote> OrderNotes { get; set; }

        public bool ShowVendorName { get; set; }
    }

    public class OrderItemModel : BaseModel
    {
        public Guid OrderItemGuid { get; set; }
        public string Sku { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductSeName { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public int Quantity { get; set; }
        public string AttributeInfo { get; set; }
        public string RentalInfo { get; set; }
        public string VendorName { get; set; }
        //downloadable product properties
        public int DownloadId { get; set; }
        public int LicenseId { get; set; }
    }

    public class TaxRate : BaseModel
    {
        public string Rate { get; set; }
        public string Value { get; set; }
    }

    public class GiftCard : BaseModel
    {
        public string CouponCode { get; set; }
        public string Amount { get; set; }
    }

    public class OrderNote: BaseModel
    {
        public bool HasDownload { get; set; }
        public string Note { get; set; }
        public DateTime CreatedOn { get; set; }
    }

    public class ShipmentBriefModel : BaseModel
    {
        public string TrackingNumber { get; set; }
        public DateTime? ShippedDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
    }

    public class AddressModel : BaseModel
    {
        
        public string FirstName { get; set; }
       
        public string LastName { get; set; }
        
        public string Email { get; set; }


        public bool CompanyEnabled { get; set; }
        public bool CompanyRequired { get; set; }
        
        public string Company { get; set; }

        public bool CountryEnabled { get; set; }
       
        public int? CountryId { get; set; }
       
        public string CountryName { get; set; }

        public bool StateProvinceEnabled { get; set; }
        
        public int? StateProvinceId { get; set; }
        
        public string StateProvinceName { get; set; }

        public bool CountyEnabled { get; set; }
        public bool CountyRequired { get; set; }
       
        public string County { get; set; }

        public bool CityEnabled { get; set; }
        public bool CityRequired { get; set; }
       
        public string City { get; set; }

        public bool StreetAddressEnabled { get; set; }
        public bool StreetAddressRequired { get; set; }
        
        public string Address1 { get; set; }

        public bool StreetAddress2Enabled { get; set; }
        public bool StreetAddress2Required { get; set; }
       
        public string Address2 { get; set; }

        public bool ZipPostalCodeEnabled { get; set; }
        public bool ZipPostalCodeRequired { get; set; }
        
        public string ZipPostalCode { get; set; }

        public bool PhoneEnabled { get; set; }
        public bool PhoneRequired { get; set; }
        
        public string PhoneNumber { get; set; }

        public bool FaxEnabled { get; set; }
        public bool FaxRequired { get; set; }
        
        public string FaxNumber { get; set; }
    }
}


