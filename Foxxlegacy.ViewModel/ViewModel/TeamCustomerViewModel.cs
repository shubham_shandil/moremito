﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class TeamCustomerViewModel
    {
        public List<TeamCustomer> teamCustomer { get; set; }
        public long PersRefTC { get; set; }
        public long TotalRefNetworkTC { get; set; }
        public long MyCustomers { get; set; }
        public long TotalTC { get; set; }
        public long ActualLegs { get; set; }
        public long LegsCount { get; set; }
        public long MyTV { get; set; }
        public long MyPersTV { get; set; }
        public string QualOrder { get; set; }
        public List<LegsForMBO> legs { get; set; }
        public TeamCustomerViewModel()
        {
            teamCustomer = new List<TeamCustomer>();
            legs = new List<LegsForMBO>();
        }
    }
    public class TeamCustomer
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public long S2 { get; set; }
        public long S3 { get; set; }
        public long G1 { get; set; }
        public long G2 { get; set; }
        public long G3 { get; set; }
        public long P1 { get; set; }
        public long P2 { get; set; }
        public long P3 { get; set; }
        public long Total { get; set; }
        
    }
    public class LegsForMBO
    {
        public long Id { get; set; }
        public long Qty { get; set; }
    }
}
