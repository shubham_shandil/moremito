﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ResourcesViewModel
    {
        public List<Resources> resources { get; set; }
        public List<FilesResources> filesResources { get; set; }
        public long activeId { get; set; }
        public ResourcesViewModel()
        {
            resources = new List<Resources>();
            filesResources = new List<FilesResources>();
        }
    }
    public class Resources
    {
        public long Id { get; set; }        
        public string Name { get; set; }
        public string CatOrder { get; set; }
        public long TheCount { get; set; }
    }
    public class FilesResources
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
