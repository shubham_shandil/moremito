﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class RankUpManagement
    {

    }

    public class CheckStepFiveToElevenRequestViewModel
    {
        public int UserId { get; set; }
        public int PerLegCappedCustomerCount { get; set; }
        public int LegsWithRank3Rank4 { get; set; }
        public int CheckRankLegTotal { get; set; }
    }

    public class CheckStepFiveToElevenResponseViewModel
    {
        public int SponsorId { get; set; }
        public int CappedCustomerCount { get; set; }
        public int UsersInLegCount { get; set; }
    }

    public class CappedCustomerCountRequestViewModel
    {
        public int UserId { get; set; }
        public int PerLegCappedCustomerCount { get; set; }
    }

    public class TWORank3UsersInLegCountRequestViewModel
    {
        public int UserId { get; set; }
        public int LegsWithRank3Rank4 { get; set; }
        public int CheckRankLegTotal { get; set; }
    }

    public class GoldRankupSponsorIdReuestViewModel
    {
        public int UserId { get; set; }
        public int CheckRankId { get; set; }
    }

    public class UpdateGoldRankUserReuestViewModel
    {
        public int UserId { get; set; }
        public string GoldLevel { get; set; }
        public int SponsorID { get; set; }
    }

    public class OrderUserIdRequestViewModel
    {
        public int OrderId { get; set; }
    }
    public class RankManagementViewModel
    {
        public MyRankReportViewModel myRankData { get; set; }
        public List<RanksNameViewModel> ranksNames { get; set; }
        public List<NextRanksViewModel> nextRanks { get; set; }
        public List<RanksNameViewModel> Silver_1Star { get; set; }
        public List<RanksNameViewModel> Silver_2star { get; set; }
        public List<RanksNameViewModel> Silver_1_2_Star { get; set; }
    }
    public class MyRankReportViewModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public int RankId { get; set; }
        public string RankName { get; set; }
        public Int64 Customers { get; set; }
        public Int64 LegsWithActiveCustomers { get; set; }
        public string QualifiedBy { get; set; }
        public DateTime? ExpiringOn { get; set; }
        public Int64 Silver_1Star { get; set; }
        public Int64 Silver_2Star { get; set; }
        public Int64 Silver_1_2Star { get; set; }
        public string S1_bg_color { get; set; }
        public string S2_bg_color { get; set; }
        public string S3_bg_color { get; set; }

    }
    public class RanksNameViewModel
    {
        public int unilevelsponsorid { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
    }
    public class NextRanksViewModel
    {
        public int ID { get; set; }
        public string RankName { get; set; }
        public Int32 CompareCountLegs { get; set; }
        public Int32 PerLegCappedCustomerCount { get; set; }
        public Int32 CappedCustomerCount { get; set; }
        public Int32 LegsWithRank3Rank4 { get; set; }
        public Int32 CheckRankLegTotal { get; set; }
        public Int32 NoOfRankingLeg { get; set; }
        public Int32? Display1 { get; set; }
        public Int32? Display2 { get; set; }
        public Int32? Display3 { get; set; }
        public Int32? Display4 { get; set; }
        public string S1_bg_color { get; set; }
        public string S2_bg_color { get; set; }
        public string S3_bg_color { get; set; }
    }

}
