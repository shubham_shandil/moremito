﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ArchiveCommonResponseViewModel
    {
        public long ID { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Reply text is required")]
        public string ReplyText { get; set; }

        [Required(ErrorMessage = "Display Order is required")]
        public string DisplayOrder { get; set; }
        public List<ArchiveCommonResponseListViewModel> archiveCommonResponseListViewModel { get; set; }

        public List<CommonResponseList> commonResponseList { get; set; }

        public ArchiveCommonResponseViewModel()
        {
            archiveCommonResponseListViewModel = new List<ArchiveCommonResponseListViewModel>();
            commonResponseList = new List<CommonResponseList>();
        }
    }
    public class CommonResponseList
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public string ReplyText { get; set; }


        public long DisplayOrder { get; set; }
    }
}
