﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class ContactNotesListViewModel
    {
        public long ID { get; set; }
        public string CreateDate { get; set; }
        public string Notes { get; set; }
        public string UserName { get; set; }
    }
}
