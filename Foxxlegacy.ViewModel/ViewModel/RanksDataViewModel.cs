﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class RanksDataViewModel
    {
        public Int32 CompareCountLegs { get; set; }
        public Int32 PerLegCappedCustomerCount { get; set; }
        public Int32 CappedCustomerCount { get; set; }
        public Int32 LegsWithRank3Rank4 { get; set; }
        public Int32 CheckRankLegTotal { get; set; }
        public Int32 NoOfRankingLeg { get; set; }
    }
}
