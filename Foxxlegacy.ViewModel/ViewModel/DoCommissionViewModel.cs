﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class DoCommissionViewModel
    {

        public Int32 Id { get; set; }
        public string Message { get; set; }
        public Int32 UserId { get; set; }
        public Int32 OrderId { get; set; }
        public Int32 RankId { get; set; }
        public string CommissionType { get; set; }
        public Int32 OrginalOrderOwnerId { get; set; }
        public decimal? CommissionSweptAmount { get; set; }
    }

    public class CommissionDistributionResponseViewModel
    {
        public int RowAffected { get; set; }
    }
    public class AdminCommissionViewModel
    {
        public List<ViewCommissionModel> _commissionList { get; set; }
        public List<ActiveCustomerMetaDataViewModel> _activeCustomerMetaData { get; set; }
        public List<UserQualifyMetaDataViewModel> _userQualify { get; set; }
        public List<RankHistoryMetaViewModel> _rankHistory { get; set; }
    }
    public class ActiveCustomerMetaDataViewModel
    {
        public int? UniLevelSponsorID { get; set; }
        public bool? PersonalCustomer { get; set; }
        public int? NextUnilevelSponsorID { get; set; }
        public DateTime? ExpDate { get; set; }
        public string SponsorName { get; set; }
        public string NextSponsorName { get; set; }
      
    }
    public class UserQualifyMetaDataViewModel
    {
        public string username { get; set; }
        public string UName { get; set; }
        public DateTime? dateofentry { get; set; }
        public DateTime? QualTillDate { get; set; }
        public bool? Active { get; set; }
    }
    public class RankHistoryMetaViewModel
    {
        public string username { get; set; }
        public string UName { get; set; }
        public string RankName { get; set; }
        public DateTime? promodate { get; set; }
        public string ranktype { get; set; }
        public int? gfid { get; set; }
    }
    public class ViewCommissionModel
    {
        public int ID { get; set; }
        public int? UserId { get; set; }
        public string UserRankId { get; set; }
        public int? OrderId { get; set; }
        public decimal? OrderAmount { get; set; }
        public decimal? OrderTotalCommissionAmount { get; set; }
        public decimal? CommissionPercentage { get; set; }
        public decimal? CommissionAmount { get; set; }
        public string CommissionLevel { get; set; }
        public string CommissionType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UserName { get; set; }
        public string RecordType { get; set; }
        public string OrderOwner { get; set; }
    }

    public class GetCommissionModel
    {
        public long UserId { get; set; }
        public int SearchType { get; set; }
        public int OrderNo { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int SortBy { get; set; }
    }
    public class UserWiseCommissionModel
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int? UserRankId { get; set; }
        public decimal? OrderTotalCommissionAmount { get; set; }        
        public decimal? Amount { get; set; }      
        public int OrderCount { get; set; }
        public string RoleName { get; set; }

    }
}
