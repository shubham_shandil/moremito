﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class RoleInfoViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<RoleDetails> _roleDetails { get; set; }
        public RoleInfoViewModel()
        {
            _roleDetails = new List<RoleDetails>();
        }
    }
    public class RoleDetails
    {
        public Guid PermissionRoleId { get; set; }
        public bool Permission { get; set; }
        public string RoleName { get; set; }
        public Guid MainRoleId { get; set; }
        public Guid UserId { get; set; }
    }

}
