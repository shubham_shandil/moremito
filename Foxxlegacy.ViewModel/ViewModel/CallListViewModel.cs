﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class CallListViewModel
    {
        public long LogID { get; set; }
        public long UserId { get; set; }
        public string UserName { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string RankName { get; set; }

        public string PromoDate { get; set; }
        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string Country { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string CSR { get; set; }
    }
}
