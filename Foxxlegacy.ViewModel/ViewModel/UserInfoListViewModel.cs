﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserInfoListViewModel
    {
        public UserPersonalInformationViewModel userPersonalInformationViewModel { get; set; }

        public UserInfoListViewModel()
        {
            userPersonalInformationViewModel = new UserPersonalInformationViewModel();
        }
    }

    public class UserPersonalInformationViewModel
    {
        public long ID { get; set; }
        public long CustomerID { get; set; }
        public string UserName { get; set; }
        public string SmsCode { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public long RankID { get; set; }

        public string Rank { get; set; }
    }
    public class TargetUserNameViewModel
    {
        public long ID { get; set; }
        public long CustomerID { get; set; }
        public string UserName { get; set; }
        public string SmsCode { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public long RankID { get; set; }

        public string Rank { get; set; }
        public bool IsInDownline { get; set; }
    }
    public class UserOrdersViewModel
    {
        public int OrderId { get; set; }
    }
    public class OrderPlacementModel
    {
        public int TargetUserId { get; set; }
        public int OwnerUserId { get; set; }
        public List<UserOrdersViewModel> OrderList { get; set; }
    }
}
