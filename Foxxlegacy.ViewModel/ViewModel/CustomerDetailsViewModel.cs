﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class CustomerDetailsViewModel
    {
        public long P1ID { get; set; }
        public long P2ID { get; set; }
        public long P3ID { get; set; }
        public long G1ID { get; set; }
        public long G2ID { get; set; }
        public long G3ID { get; set; }
    }
}
