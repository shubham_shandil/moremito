﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class GenealogyViewModel
    {
        public ReferralUser ReferralData { get; set; }
        public List<ReferralUser> ReferralDataList { get; set; }
        public GenealogyViewModel()
        {
            ReferralData = new ReferralUser();
            ReferralDataList = new List<ReferralUser>();
        }
    }

    public class ReferralUser
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public long ChildNodeCount { get; set; }
        public bool IsQualified { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsRankShow { get; set; }
        public bool IsPersonalMovable { get; set; }
        public bool IsPersonalPermanent { get; set; }
        public bool IsNonPersonalMovable { get; set; }
        public bool IsNonPersonalPermanent { get; set; }
        public string RankText { get; set; }
        public bool IsRecurringOrder { get; set; }
    }
    public class MainList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public long ChildNodeCount { get; set; }
        public bool IsQualified { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsRankShow { get; set; }
        public bool IsPersonalMovable { get; set; }
        public bool IsPersonalPermanent { get; set; }
        public bool IsNonPersonalMovable { get; set; }
        public bool IsNonPersonalPermanent { get; set; }
        public string RankText{ get; set; }
        public bool IsRecurringOrder { get; set; }
    }
    public class SubMainList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public long ChildNodeCount { get; set; }
        public bool IsQualified { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsRankShow { get; set; }
        public bool IsPersonalMovable { get; set; }
        public bool IsPersonalPermanent { get; set; }
        public bool IsNonPersonalMovable { get; set; }
        public bool IsNonPersonalPermanent { get; set; }
        public string RankText { get; set; }
    }
    public class ChildMainList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public long ChildNodeCount { get; set; }
        public long SponsorUsersID { get; set; }
        public bool IsQualified { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsRankShow { get; set; }
        public bool IsPersonalMovable { get; set; }
        public bool IsPersonalPermanent { get; set; }
        public bool IsNonPersonalMovable { get; set; }
        public bool IsNonPersonalPermanent { get; set; }
        public string RankText { get; set; }
    }
}
