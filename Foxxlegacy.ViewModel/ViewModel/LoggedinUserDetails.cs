﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class LoggedinUserDetails
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SignupType { get; set; }
        public bool PlacementMode { get; set; }
        public bool IsAdmin { get; set; }
        public string MembershipType { get; set; }
        public int NopUserId { get; set; }
       
    }

    public class CacheContext
    {
        public CacheContext()
        {
            LoggedInUser = new LoggedinUserDetails();
            //InvoiceSettings = new InvoiceSettingsViewModel();
        }
        public LoggedinUserDetails LoggedInUser;
        //public InvoiceSettingsViewModel InvoiceSettings;
        public string MenuName;
        public string SessionID;
        public string IP { get; set; }
        public string MacAddress { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime LogoutTime { get; set; }


    }
    public class FoxShopTransferModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int WebsiteSponsorId { get; set; }
        public bool IsGuestUser { get; set; }
    }
}
