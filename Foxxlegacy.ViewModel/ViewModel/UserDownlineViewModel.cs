﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserDownlineViewModel
    {
        public string ErrorMsg { get; set; }
        public string TheAverage { get; set; }
        public long UserId { get; set; }
        public List<Emails> emails { get; set; }
        public List<NewUserData> newUserData { get; set; }
        public UserDownlineViewModel()
        {
            emails = new List<Emails>();
            newUserData = new List<NewUserData>();
        }
    }
    public class Emails
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }

    public class NewUserData
    {
        public long Week { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long Count { get; set; }
        public long WithPV { get; set; }
        public string PCT { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime END_DATE { get; set; }
        public long WEEKNO { get; set; }
    }
}
