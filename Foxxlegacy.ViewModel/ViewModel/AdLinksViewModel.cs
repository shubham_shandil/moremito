﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class AdLinksViewModel
    {
        public Campaign campaign { get; set; }
        public string CampaignId { get; set; }
        public List<SelectListItem> CampaignList { get; set; }
        public List<CampaignPage> campaignPages { get; set; }
        public List<UsersCampaignPage> usersCampaignPages { get; set; }
        public long CampId { get; set; }
        public string CampName { get; set; }
        public long PageId { get; set; }
        public AdLinksViewModel()
        {
            campaign = new Campaign();
            CampaignList = new List<SelectListItem>();
            campaignPages = new List<CampaignPage>();
            usersCampaignPages = new List<UsersCampaignPage>();
        }
    }
    public class Campaign
    {
        public long UserId { get; set; }
        public string CampaignName { get; set; }
    }

    public class CampaignPage
    {
        public long Id { get; set; }
        public string PageName { get; set; }
        public string PageUrl{ get; set; }
    }

    public class UsersCampaignPage
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long Hits { get; set; }
        public string PageName { get; set; }
        public Guid LinkGUID { get; set; }
    }
    public class QRCodeModel
    {
        public string QRCodeText { get; set; }
        public string QRCodeImagePath { get; set; }
        public string Notes { get; set; }
        public string CampaignId { get; set; }
    }

    public class PageRedirection
    {
        public long Id { get; set; }
        public string PageUrl { get; set; }
        public string UserName { get; set; }
    }
    public class SearchCampaignURL
    {
        public long CampaignId { get; set; }
        public string Name { get; set; }
    }
}
