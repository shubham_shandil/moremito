﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class SqlResponseBaseModel
    {
        public long? InsertedId { get; set; }
        public long? RowAffected { get; set; }
        public long? ErrorCode { get; set; }
        public string ErrorMsag { get; set; }
        public string FileCurrentPath { get; set; }
    }
}
