﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class PlacementTooListlViewModel
    {
        public long PlacementUserID { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public class PlacementToolViewModel
    {
        public long UserId { get; set; }

        public long PlacementUserID { get; set; }
    }
}
