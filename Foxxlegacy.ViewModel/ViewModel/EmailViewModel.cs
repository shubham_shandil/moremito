﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class EmailViewModel
    {
        public long EmailID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string FromEmailAddress { get; set; }
        public string ToEmailAddress { get; set; }
        //public string EmailID { get; set; }
        //public string EmailID { get; set; }
    }
}
