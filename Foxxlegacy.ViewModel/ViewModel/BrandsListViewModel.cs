﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class BrandsListViewModel
    {
        public long ID { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public decimal AppDisplayOrder { get; set; }
        public string AppImgUrl { get; set; }
    }
}
