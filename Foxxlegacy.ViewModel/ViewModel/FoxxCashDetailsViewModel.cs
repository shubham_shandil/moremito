﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class FoxxCashDetailsViewModel
    {
        public long UsersID { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }

        public string DetailsList { get; set; }

        public decimal TotalEarned { get; set; }
        public string Content { get; set; }
        public List<FoxxCashDetailsListViewModel> foxxCashDetailsListViewModel { get; set; }

        public List<FoxxCashDetailsListViewModel2> foxxCashDetailsListViewModel2 { get; set; }

        public List<FoxxCashDetailsListViewModel3> foxxCashDetailsListViewModel3 { get; set; }

        public List<FoxxCashDetailsListViewModel4> foxxCashDetailsListViewModel4 { get; set; }
        public List<FoxxCashDetailsListViewModel5> foxxCashDetailsListViewModel5 { get; set; }

        public EventsListViewModel eventsListViewModel = new EventsListViewModel();
        public EventsViewModel eventsViewModel { get; set; }

        public FoxxCashDetailsViewModel()

        {
            foxxCashDetailsListViewModel = new List<FoxxCashDetailsListViewModel>();
            foxxCashDetailsListViewModel2 = new List<FoxxCashDetailsListViewModel2>();
            foxxCashDetailsListViewModel3 = new List<FoxxCashDetailsListViewModel3>();
            foxxCashDetailsListViewModel4 = new List<FoxxCashDetailsListViewModel4>();
            foxxCashDetailsListViewModel5 = new List<FoxxCashDetailsListViewModel5>();
            eventsListViewModel = new EventsListViewModel();
            eventsViewModel = new EventsViewModel();
        }

    }
}
