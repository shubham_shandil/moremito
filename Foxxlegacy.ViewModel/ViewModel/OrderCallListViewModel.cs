﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class OrderCallListViewModel
    {
        public long UsersID { get; set; }
        public string CustomerUserName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public long OrdersId { get; set; }
        public long Orderno { get; set; }

        public string Orderdate { get; set; }

        public string status { get; set; }

        public string state { get; set; }

        public string CSR { get; set; }
    }
}
