﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserInformationViewModel
    {

        public long UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string SMSCode { get; set; }
        public Boolean CourtesyCalls { get; set; }

        public string WelcomeName { get; set; }
        public string WelcomeEmail { get; set; }
        public string WelcomePhone { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Current Password field is required.")]

        public string CurrentPassword { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password field is required.")]
        //[RegularExpression(@"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}", ErrorMessage = "The password has to be minimum 8 characters, maximum 12 characters and must consists of at least one number and one uppercase letter.")]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Confirm Password field is required.")]
        [Compare("Password", ErrorMessage = "Password and Confirm Password does not match.")]
        public string ConfirmPassword { get; set; }

        public long ID { get; set; }//UserId

        public long RankID { get; set; }
        public string Rank { get; set; }
        public long UsersAddressesID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string TreeContentOrganicPlacement { get; set; }
        public List<PromotionRanksViewModel> promotionRanksViewModel { get; set; }
        public List<StateListViewModel> stateListViewModel { get; set; }
        public List<PromotionRanksViewModel> RankDropDownList { get; set; }
        public List<GrandFathersListViewModel> grandFathersListViewModel { get; set; }
        public UserRoleDetailsListViewModel userRoleDetailsListViewModel { get; set; }
        public List<PersonalsListViewModel> personalsListViewModel { get; set; }
        public List<MessagesListViewModel> messagesListViewModel { get; set; }
        public List<ContactDetailsListViewModel> contactDetailsListViewModel { get; set; }
        public HistoryofContactsViewModel historyofContactsViewModel { get; set; }
        public ContactDetailsViewModel contactDetailsViewModel { get; set; }
        public SponsorContactDetailsViewModel sponsorContactDetailsViewModel { get; set; }
        public List<UplineContactDetailsViewModel> UplineContactDetailsViewModelList { get; set; }
        public TreeOrganicPlacementViewModel treeOrganicViewModel { get; set; }
        public TreeOrganicPlacementViewModel treePlacementViewModel { get; set; }
        public TreeOrganicPlacementViewModel treeOrganicPlacementViewModel { get; set; }
        public CSJDetailsViewModel cSJDetailsViewModel { get; set; }
        public string SearchBy { get; set; }
        public string Value { get; set; }

        //public long ActiveTab { get; set; }

        public string ActiveTab { get; set; }



        public List<PlacementTooListlViewModel> placementTooListlViewModel { get; set; }

        public List<RankPromotion> rankPromotion { get; set; }//For Rank history

        public string MembershipType { get; set; }
        public string HighestRankAchieved { get; set; }

        public string QTE { get; set; }

        public string GetCurrentRank { get; set; }

        public TeamCustomerViewModel TeamCustomerViewModel { get; set; }

        public string JoinDate { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }

        public List<AdminOrders> adminOrders { get; set; }
        public CommissionSummary commissionSummary { get; set; }
        public List<Autoship> AutoshipList { get; set; }
        public List<RecurringOrderHistory> RecurringOrderHistoryList { get; set; }
        public RankManagementViewModel RankManagement { get; set; }
        public CurrentRoleModel CurrentRoleModel { get; set; }
        public List<CurrentRoleModel> RoleList { get; set; }
        public List<CountryStateModel> CountryList { get; set; }
        public List<CountryStateModel> StateList { get; set; }
        public UserInformationViewModel()
        {
            promotionRanksViewModel = new List<PromotionRanksViewModel>();
            stateListViewModel = new List<StateListViewModel>();
            RankDropDownList = new List<PromotionRanksViewModel>();


            grandFathersListViewModel = new List<GrandFathersListViewModel>();
            userRoleDetailsListViewModel = new UserRoleDetailsListViewModel();

            personalsListViewModel = new List<PersonalsListViewModel>();
            messagesListViewModel = new List<MessagesListViewModel>();
            contactDetailsListViewModel = new List<ContactDetailsListViewModel>();
            historyofContactsViewModel = new HistoryofContactsViewModel();
            contactDetailsViewModel = new ContactDetailsViewModel();
            sponsorContactDetailsViewModel = new SponsorContactDetailsViewModel();
            UplineContactDetailsViewModelList = new List<UplineContactDetailsViewModel>();
            treeOrganicViewModel = new TreeOrganicPlacementViewModel();

            treePlacementViewModel = new TreeOrganicPlacementViewModel();
            treeOrganicPlacementViewModel = new TreeOrganicPlacementViewModel();
            cSJDetailsViewModel = new CSJDetailsViewModel();
            placementTooListlViewModel = new List<PlacementTooListlViewModel>();
            rankPromotion = new List<RankPromotion>();
            adminOrders = new List<AdminOrders>();
            commissionSummary = new CommissionSummary();
            AutoshipList = new List<Autoship>();
            RecurringOrderHistoryList = new List<RecurringOrderHistory>();
            RankManagement = new RankManagementViewModel();
            CurrentRoleModel = new CurrentRoleModel();
            RoleList = new List<CurrentRoleModel>();
            CountryList = new List<CountryStateModel>();
            StateList = new List<CountryStateModel>();
        }
    }


    public class UserQualifiedRankDataViewModel
    {
        public int ID { get; set; }
        public int RankID { get; set; }
        public string ExpDate { get; set; }
    }

    public class UserRankEmailOptDataViewModel
    {
        public bool EmailOpt { get; set; }
    }

    public class UserRankSmsOptDataViewModel
    {
        public bool SMSOptIn { get; set; }
    }

    public class UserRankOptDataViewModel
    {
        public int RankID { get; set; }
        public int SponsorUsersID { get; set; }
        public int UniLevelSponsorID { get; set; }
        public string JoinDate { get; set; }
        public string RankName { get; set; }
    }

    public class PlaceUserCountViewModel
    {
        public int UsersCount { get; set; }
    }

    public class UpdateUserPlacementRequestViewModel
    {
        public int SponsorUserId { get; set; }
        public int UserId { get; set; }
    }

    public class UpdateUserPlacementResponseViewModel
    {
        public int AffectedRow { get; set; }
    }
   

    public class AdminOrders
    {
        public long Id { get; set; }

        public string orderno { get; set; }
        public string orderdate { get; set; }

        public string status { get; set; }

        public bool PassUp { get; set; }

        public string Qty { get; set; }

        public string expDate { get; set; }

        public string OrderOwner { get; set; }
    }

    public class CommissionSummary
    {
        public decimal Pending { get; set; }
        public decimal Earned { get; set; }
        public decimal Swept { get; set; }
        public decimal SummarySweepNow { get; set; }
        public decimal Summarybalance { get; set; }
    }
    public class CommissionEarnedMode
    {
        public decimal amount { get; set; }
        public List<CommissionEarnedDescriptionModel> commissionList { get; set; }
    }
    public class CommissionEarnedDescriptionModel
    {
        public decimal total { get; set; }
        public string TheYear { get; set; }
        public string TheMonth { get; set; }
        public string Type { get; set; }
        public int TheMonthInt { get; set; }
    }
    public class CommissionDetailByMonthYearModel
    {
        public decimal SumTotal { get; set; }
        public int OrderID { get; set; }
        public string DateAvailable { get; set; }
        public string Description { get; set; }
        public string CreateDate { get; set; }
        public string CommissionType { get; set; }
    }
    public class CommissionPaidModel
    {
        public string CreateDate { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
        public bool Paid { get; set; }
        public string PaidDate { get; set; }
    }
    public class AddNewCsj
    {
        public int UserId { get; set; }
        public int AdminId { get; set; }
        public string Comment { get; set; }
    }
    public class CurrentRoleModel
    {
        public int ResponsCode { get; set; }
        public Guid? RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
       
    }
    public class RoleListModel
    {
        public Guid? RoleId { get; set; }
        public string RoleName { get; set; }
    }
    public class ChangePasswordModel
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public int ModifiedBy { get; set; }
    }
    public class SendSmsModel
    {
        public string Phone { get; set; }
        public string Content { get; set; }
    }
}

