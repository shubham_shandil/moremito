﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Your User Name is Required")]
        public string UserName { get; set; }
    }

    public class ForgotUserNameViewModel
    {
        [Required(ErrorMessage = "Your Email is Required")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Please enter a valid email address")]
        public string Email { get; set; }
    }
    public class ResetPassword
    {
        [Required(ErrorMessage ="Password change code missing!")]
        public string Id { get; set; }

        [Required(ErrorMessage = "Enter New password !")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Minimum 3 characters required")]
        public string NewPassword { get; set; }
        

        [Compare("NewPassword", ErrorMessage = "Confirm password doesn't match, Type again !")]
        public string ConfirmNewPassword { get; set; }
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}
