﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class ContactDetailsListViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }

        public string Subject { get; set; }

        public string Details { get; set; }
        public string EventDate { get; set; }
        public bool OwnerAnswered { get; set; }

        public string Dialogue { get; set; }
        public string CreateDate { get; set; }

        public bool IsAdmin { get; set; }

        public string Origin { get; set; }

        public string Phone { get; set; }
        public string EmailId { get; set; }

        public string Notes { get; set; }

        public string UserName { get; set; }
    }
}
