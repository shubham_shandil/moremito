﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class GenealogySearchViewModel
    {
        public List<GenealogyList> genealogyList { get; set; }
        public string keySearch { get; set; }
        public GenealogySearchViewModel()
        {
            genealogyList = new List<GenealogyList>();
        }
    }
    public class GenealogyList
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public long InMyLevel { get; set; }
        public string SponsorUserName { get; set; }
    }
}
