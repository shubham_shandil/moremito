﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class GrandFathersViewModel
    {
        public long UsersID { get; set; }
        public long GFRankID { get; set; }
        public string ExpireDate { get; set; }
        public string Reason { get; set; }
        public string PreviousRank { get; set; }


        public long RankID { get; set; }

        public string Comments { get; set; }
        public string UserName { get; set; }

        public long GrandFatheringID { get; set; }

        public UserInformationViewModel userInformationViewModel { get; set; }

       // public List<GrandFathersListViewModel> grandFathersListViewModel { get; set; }

        public GrandFathersViewModel grandFathersViewModel { get; set; }
        public GrandFathersViewModel()
        {
            userInformationViewModel = new UserInformationViewModel();
           // grandFathersListViewModel = new List<GrandFathersListViewModel>();
            //grandFathersViewModel = new GrandFathersViewModel();
        }

    }
}
