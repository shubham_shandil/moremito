﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class VIEProcessCommissionViewModel
    {
        public long SponsorID { get; set; }
        public long Id { get; set; }
        public long OrderNo { get; set; }

        public long Customerid { get; set; }

        public long Itemid { get; set; }

        public decimal Price { get; set; }

        public string Cartid { get; set; }


        public long nextorder { get; set; }

        public bool IsOrderOvcp { get; set; }
    }

    public class ItemModel
    {
        public decimal Price { get; set; }
        public long ItemID { get; set; }

    }
}
