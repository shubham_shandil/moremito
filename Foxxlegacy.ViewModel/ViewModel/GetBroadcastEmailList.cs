﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class GetBroadcastEmailList
    {
        public long Id { get; set; }
        public string ToEmail { get; set; }     
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public bool CheckId { get; set; }


       
        public long UserID { get; set; }

        public string Date { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }

        public string FromEmail { get; set; }

        public long CustomerID { get; set; }

        public string SendByAdmin { get; set; }

        public string GroupType { get; set; }
    }


    public class SentEmailReport
    {
        public long Id { get; set; }

        public string UserName { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
        public string Date { get; set; }

        public string ToNumber { get; set; }


        public string EmailSubject { get; set; }



    }
}
