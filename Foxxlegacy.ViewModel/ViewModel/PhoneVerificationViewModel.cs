﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class PhoneVerificationViewModel
    {
        public int UserId { get;set; }
        public string Code { get; set; }
        public string ToNumber { get; set; }
        public string FromNumber { get; set; }
        public string Smstext { get; set; }
        public string UUID { get; set; }
        public int PlivoQueueID { get; set; }
        public string PlivoDefaultNumber { get; set; }
    }
    public class SetSmsCodeViewModel
    {
        public string SmsCode { get; set; }
        public string SmsCode1 { get; set; }
        public int ValCount { get; set; }
    }
    public class SetEmailViewModel
    {
        public string Subject { get;set; }
        public string Body { get; set; }
    }
    public class PlivoResponseModel
    {
        public string ApiId { get; set; }
        public string Message { get; set; }
        public string MessageUuid { get; set; }
        public string StatusCode { get; set; }
        public string Username { get; set; }
        public string InvalidNumber { get; set; }
        public string InputJson { get; set; }
        public string SmsContent { get; set; }
       // public List<string> MessageUuid { get; set; }
        
        public string Alias { get; set; }
        public string TotalCount { get; set; }
      //  public List<string> invalid_number { get; set; }
      public bool? IsBroadcast { get; set; }
        public int? MaxStatus { get; set; }

    }
    public class PlivoLogsModel
    {
        public string MessageUUid { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string Status { get; set; }
        public string MessageJson { get; set; }
        public string MessageTime { get; set; }
        public string QueuedTime { get; set; }
        public string SentTime { get; set; }
        public string DeliveryReportTime { get; set; }
        
    }
    public class PlivoJsonModel
    {
        public string Key { get; set; }
        public string Values { get; set; }
    }
    public class PlivoOptInOutModel
    {
        public string MessageUUid { get; set; }
        public string To { get; set; }
        public string From { get; set; }
        public string MessageIntent { get; set; }
        public string SmsText { get; set; }
        public string OptType { get; set; }
    }
    public class PlivoBroadCastInputModel
    {
        public int FilterType { get; set; }
        public string Content { get; set; }
        public List<BroadCastUsersModel> phoneList { get; set; }
    }
    public class BroadcastViewModel
    {
        public List<BroadCastUsersModel> userList { get; set; }
    }
    public class BroadCastUsersModel
    {
        public int Id { get; set; }
        public string username { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string smsphone { get; set; }
    }
    public class ApiIdModel
    {
        public int id { get; set; }
        public string apiid { get; set; }
        public string createddate { get; set; }
    }
    public class SmsReportModel
    {
        public int TotalPhoneCount { get; set; }
        public int TotalInvalid { get; set; }
        public int TotalQueuedBefore { get; set; }
        public int TotalQueued { get; set; }
        public int TotalSent { get; set; }
        public int TotalDelivered { get; set; }
    }
    public class SmsReportDashboardModel
    {
        public SmsReportModel list { get; set; }
    }
    public class PlivoOptInOutReportModel
    {
        public int Id { get; set; }
        public string phone { get; set; }
        public string From { get; set; }
        public string MessageIntent { get; set; }
        public string SmsText { get; set; }
        public DateTime? createdon { get; set; }
        public string username { get; set; }
        public string OptType { get; set; }
        public bool? IsOptOut { get; set; }
        
    }
}
