﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class FAQListViewModel
    {
        public long Id { get; set; }
        public string CatName{ get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
       
    }
}
