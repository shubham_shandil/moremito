﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class PersonalInformationViewModel
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public long Placement { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public DateTime JoinDate { get; set; }
        public string SponserName { get; set; }
        public long Level { get; set; }
        public string RankName { get; set; }
        public string PlacementUser { get; set; }
        
    }
}
