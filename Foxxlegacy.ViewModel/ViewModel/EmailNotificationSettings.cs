﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class EmailNotificationSettings
    {
        public long OptOutCatID { get; set; }
        public string Description { get; set; }
        public string OptOutCatName { get; set; }

        public string IsOptedIn { get; set; }
    }
}
