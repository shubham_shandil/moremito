﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class SilverStarReport
    {
        public List<SilverStar> silverStar { get; set; }
        public SilverStarReport()
        {
            silverStar = new List<SilverStar>();
        }
    }
    public class SilverStar
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public long SilverStarCount { get; set; }
        public bool CheckCount { get; set; }
    }   
}
