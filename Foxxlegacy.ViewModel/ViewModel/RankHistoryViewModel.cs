﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class RankHistoryViewModel
    {
        public WebContentData webContentData { get; set; }
        public List<RankPromotion> rankPromotion { get; set; }
        public RankHistoryViewModel()
        {
            webContentData = new WebContentData();
            rankPromotion = new List<RankPromotion>();
        }
    }
    public class WebContentData
    {
        public string WebContentValue { get; set; }
    }
    public class RankPromotion
    {
        public string RankName { get; set; }
        public string OrderNo { get; set; }
        public string PromoDate { get; set; }

        public string GetCurrentRank { get; set; }
    }
}