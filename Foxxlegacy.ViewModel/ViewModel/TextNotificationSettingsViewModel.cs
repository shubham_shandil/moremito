﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class TextNotificationSettingsViewModel
    {
        public long OptOutCatID { get; set; }
        public long UsersID { get; set; }

        public string IsOptedIn { get; set; }
    }
}
