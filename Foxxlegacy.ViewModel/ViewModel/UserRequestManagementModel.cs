﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class UserRequestManagementModel
    {
    }

    public class BroadcastEmailUserRequest
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public int CategoryId { get; set; }
        public int RankId { get; set; }
    }
}
