﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
    public class HistoryofContactsListViewModel
    {
        public long ID { get; set; }

        public string Subject { get; set; }
        public string Details { get; set; }
        public string Notes { get; set; }

        public string EventDate { get; set; }
    }

    public class DialogueList

    {
        public string UserName { get; set; }
        public string Dialogue { get; set; }
        public string CreateDate { get; set; }
    }

    public class HistoryofContactsViewModel
    {

        public List<HistoryofContactsListViewModel> historyofContactsListViewModel { get; set; }
        public List<DialogueList> dialogueList { get; set; }

        public HistoryofContactsViewModel()
        {
            historyofContactsListViewModel = new List<HistoryofContactsListViewModel>();
            dialogueList = new List<DialogueList>();

        }
    }
}
