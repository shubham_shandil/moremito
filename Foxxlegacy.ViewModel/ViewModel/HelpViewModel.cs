﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class HelpViewModel
    {

        public Int32 Id { get; set; }
        public string WebContentName { get; set; }
        public string HelpContent { get; set; }
        public string HelpContent2 { get; set; }
        public string Return { get; set; }
    }
    
}
