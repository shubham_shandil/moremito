﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.ViewModel.ViewModel
{
   public class CommissionsViewModel
    {
        public long UsersID { get; set; }
      
        public long CommissionTypesID { get; set; }
        public string Type { get; set; }
        public DateTime DateAvailable { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }

        public long OrderID { get; set; }

        public long FromLevel { get; set; }

        public long RecipientRankID { get; set; }

        public long RecipientUserID { get; set; }
        public bool Taxable { get; set; }

        public long FromCommissionsSweptID { get; set; }

        public long RecipientCommissionRecordID { get; set; }

       
    }
}
