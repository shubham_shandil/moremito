﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.Common
{
    public class CommonService : ICommonService
    {
        private readonly IDbContext _dbContext;
        public CommonService()
        {
        }

        #region CTor
        public CommonService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }




        #endregion


        public List<StateListViewModel> GetStateList(int ActionId)
        {
            List<StateListViewModel> modelList = new List<StateListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                modelList = _dbContext.ExecuteStoredProcedure<StateListViewModel>("exec SP_State @ActionId", _ActionId).ToList();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public SqlResponseBaseModel InsertToEmailQue(int ActionId, EmailViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _FromEmailAddress = new SqlParameter();
                _FromEmailAddress.Direction = System.Data.ParameterDirection.Input;
                _FromEmailAddress.DbType = System.Data.DbType.String;
                _FromEmailAddress.ParameterName = "@FromEmailAddress";
                _FromEmailAddress.Value = model.FromEmailAddress ?? (object)DBNull.Value;

                SqlParameter _ToEmailAddress = new SqlParameter();
                _ToEmailAddress.Direction = System.Data.ParameterDirection.Input;
                _ToEmailAddress.DbType = System.Data.DbType.String;
                _ToEmailAddress.ParameterName = "@ToEmailAddress";
                _ToEmailAddress.Value = model.ToEmailAddress ?? (object)DBNull.Value;

                SqlParameter _Subject = new SqlParameter();
                _Subject.Direction = System.Data.ParameterDirection.Input;
                _Subject.DbType = System.Data.DbType.String;
                _Subject.ParameterName = "@Subject";
                _Subject.Value = model.Subject ?? (object)DBNull.Value;

                SqlParameter _Body = new SqlParameter();
                _Body.Direction = System.Data.ParameterDirection.Input;
                _Body.DbType = System.Data.DbType.String;
                _Body.ParameterName = "@Body";
                _Body.Value = model.Body ?? (object)DBNull.Value;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_InsertToEmailQue @ActionId,@FromEmailAddress,@ToEmailAddress,@Subject,@Body", _ActionId, _FromEmailAddress, _ToEmailAddress, _Subject, _Body).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public EmailViewModel SetEmailSubjectAndBody(int ActionId, long EmailID)
        {

            EmailViewModel model = new EmailViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@EmailID",
                    Value = EmailID
                });

                model = _dbContext.ExecuteStoredProcedure<EmailViewModel>("exec dbo.SP_SetEmailSubjectAndBody @ActionId,@EmailID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return model;

        }

        public IEnumerable<SelectListItem> GetAllDropdownlist(long ActionId)
        {
            List<SelectListItem> model = new List<SelectListItem>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                model = _dbContext.ExecuteStoredProcedure<SelectListItem>("exec SP_GetAllDropdownlistData @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<CountryStateModel> GetCountriesAndStates(int ActionId, int CountryId)
        {
            List<CountryStateModel> models = new List<CountryStateModel>();

            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@countryId",
                Value = CountryId
            });
           

            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetCountryStates", parameters);
            if (dt != null && dt.Rows.Count>0)
            {
                models = dt.ConvertDataTableToList<CountryStateModel>();
            }
            return models;
        }
        public CountryStateIdModel GetUserAddressByUserID(int UserID)
        {
            CountryStateIdModel models = new CountryStateIdModel();

            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@userId",
                Value = UserID
            });
           


            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetUserAddressByUserid", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                var d = dt.ConvertDataTableToList<CountryStateIdModel>();
                if (d != null)
                {
                    models = d.First();
                }
            }
            return models;
        }
        public long GetRankID(int ActionId, long UsersID)
        {
            long RankID = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;
                RankID = _dbContext.ExecuteStoredProcedure<long>("exec SP_RankDetails @ActionId,@UsersID", _ActionId, _UsersID).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RankID;
        }

        public string RankName(int ActionId, long RankID)
        {
            string RankName = string.Empty;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = 0;

                SqlParameter _RankID = new SqlParameter();
                _RankID.Direction = System.Data.ParameterDirection.Input;
                _RankID.DbType = System.Data.DbType.Int64;
                _RankID.ParameterName = "@RankID";
                _RankID.Value = RankID;
                RankName = _dbContext.ExecuteStoredProcedure<string>("exec SP_RankDetails @ActionId,@UsersID,@RankID", _ActionId, _UsersID, _RankID).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RankName;
        }
        public string HighestRank(int UserId)
        {
            string RankName = string.Empty;
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@userId",
                Value = UserId
            });
           
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetHighestRanks", parameters);
            if (dt != null && dt.Rows.Count>0)
            {
                RankName= Convert.ToString(dt.Rows[0][1]);
              
            }
            return RankName;
        }


        public SqlResponseBaseModel InsertDownlineCounts(int ActionId, DownlineCountsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = model.UsersID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UplineUsersID",
                    Value = model.UplineUsersID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserIsInUplinesLevelX",
                    Value = model.UserIsInUplinesLevelX
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_DownlineCounts @ActionId," +
                    "@UsersID,@UplineUsersID,@UserIsInUplinesLevelX", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public UsersQTENowforMBOViewModel GetQteNowForMBODetails(int ActionId, long PlacementUserID)
        {
            UsersQTENowforMBOViewModel model = new UsersQTENowforMBOViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@PlacementUserID",
                    Value = PlacementUserID
                });


                model = _dbContext.ExecuteStoredProcedure<UsersQTENowforMBOViewModel>("exec SP_GetQteNowForMBO @ActionId," +
                    "@PlacementUserID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public List<long> GetTeamCustomersCount(int ActionId, long Id)
        {

            List<long> nCount = new List<long>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                nCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_team_customer @ACTION,@ID", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nCount;
        }

        public SqlResponseBaseModel RemoveDownlineCounts(int ActionId, DownlineCountsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = model.UsersID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UplineUsersID",
                    Value = model.UplineUsersID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserIsInUplinesLevelX",
                    Value = model.UserIsInUplinesLevelX
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_DownlineCounts @ActionId," +
                    "@UsersID,@UplineUsersID,@UserIsInUplinesLevelX", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public long GetUsersIDbyUserName(int ActionId, string UserName)
        {
            long GetUsersIDbyUserName = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName;
                GetUsersIDbyUserName = _dbContext.ExecuteStoredProcedure<long>("exec SP_Get_UserDetails @ActionId,@UserName", _ActionId, _UserName).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetUsersIDbyUserName;
        }

        public bool CheckDuplicateUserName(string UserName)
        {
            bool status = false;
            string sql = "select count(*) from users where username='" + UserName + "'";
            var res = _dbContext.ExecuteSqlDataTable(sql, null);
            if(res!=null && res.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(res.Rows[0][0]) ,out int count);
                status = count > 0;
            }
            if (status == false)
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@actionId",
                    Value = 1
                });
                parameters[1] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@username",
                    Value = UserName
                });
                DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("CheckDuplicateNopUserName", parameters);
                if (dt != null)
                {
                    int.TryParse(Convert.ToString(dt.Rows[0][0]), out int count);
                    status = count > 0;
                }
            }

            return status;
        }
        public bool CheckDuplicateEmail(string Email)
        {
            bool status = false;
            string sql = "select count(*) from users where email='" + Email + "'";
            var res = _dbContext.ExecuteSqlDataTable(sql, null);
            if (res != null && res.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(res.Rows[0][0]), out int count);
                status = count > 0;
            }
            if (status == false)
            {
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@actionId",
                    Value = 1
                });
                parameters[1] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@username",
                    Value = Email
                });
                DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("CheckDuplicateNopUserName", parameters);
                if (dt != null)
                {
                    int.TryParse(Convert.ToString(dt.Rows[0][0]), out int count);
                    status = count > 0;
                }
            }

            return status;
        }
        public GuestResponseModel CheckDuplicateEmailForGuestUser(string Email)
        {
            GuestResponseModel m = new GuestResponseModel();
           
            string sql = "select FirstName+' '+LastName as Name,retailonly,username,id,Userpassword=(select password from userssignupdata where username=a.username) from users a  where email='" + Email + "'";
            var res = _dbContext.ExecuteSqlDataTable(sql, null);
            if (res != null && res.Rows.Count > 0)
            {
                bool.TryParse(Convert.ToString(res.Rows[0]["retailonly"]), out bool retailOnly);
                m.Username = Convert.ToString(res.Rows[0]["username"]);
                m.Name = Convert.ToString(res.Rows[0]["Name"]);
                m.Password = Convert.ToString(res.Rows[0]["Userpassword"]);
                m.IsAlreadyARetailUser = retailOnly==true?2:1;
                int.TryParse(Convert.ToString(res.Rows[0]["id"]), out int UserId);
                m.UserId = UserId;
            }

            return m;
        }

        public long GetCustomerIDByUserID(int ActionId, long UserID)
        {
            long GetCustomerIDByUserID = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _CustomerId = new SqlParameter();
                _CustomerId.Direction = System.Data.ParameterDirection.Input;
                _CustomerId.DbType = System.Data.DbType.Int64;
                _CustomerId.ParameterName = "@CUSTOMER_ID";
                _CustomerId.Value = 0;

                SqlParameter _UserID = new SqlParameter();
                _UserID.Direction = System.Data.ParameterDirection.Input;
                _UserID.DbType = System.Data.DbType.String;
                _UserID.ParameterName = "@UserID";
                _UserID.Value = UserID;



                GetCustomerIDByUserID = _dbContext.ExecuteStoredProcedure<long>("exec sp_customer_details @ActionId,@CUSTOMER_ID,@UserID", _ActionId, _CustomerId, _UserID).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetCustomerIDByUserID;
        }

        public PlatinumCodesListViewModel GetUsersPlatinumCodes(int ActionId, long UsersID)
        {
            PlatinumCodesListViewModel modelList = new PlatinumCodesListViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = ""
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserId",
                    Value = UsersID
                });

                modelList = _dbContext.ExecuteStoredProcedure<PlatinumCodesListViewModel>("exec SP_Get_UserDetails @ActionId," +
                    "@UserName,@UserId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public SqlResponseBaseModel CleanUpOrder0QtePromoData(int ActionId, long UserID)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = ""
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserId",
                    Value = UserID
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Get_UserDetails @ActionId," +
                    "@UserName,@UserId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public long GetQteDataRecord(int ActionId, long UsersID, long OrderID)
        {
            long GetQteDataRecord = 0;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = UsersID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@OrderID",
                    Value = OrderID
                });

                GetQteDataRecord = _dbContext.ExecuteStoredProcedure<long>("exec SP_UsersQTEPromoData @ActionId," +
                    "@UsersID,@OrderID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return GetQteDataRecord;
        }

        public long GetQteMetaValue(int ActionId, long DataID, string QteMetric)
        {
            long QteMetaValue = 0;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@OrderID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@QteMetric",
                    Value = QteMetric ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@DataID",
                    Value = DataID
                });
                QteMetaValue = _dbContext.ExecuteStoredProcedure<long>("exec SP_UsersQTEPromoData @ActionId," +
                    "@UsersID,@OrderID,@QteMetric,@DataID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return QteMetaValue;
        }

        public SqlResponseBaseModel SetQteMetaValue(int ActionId, long DataID, string QteMetric, long MetricValue)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@OrderID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@QteMetric",
                    Value = QteMetric ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@DataID",
                    Value = DataID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@MetricValue",
                    Value = MetricValue
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_UsersQTEPromoData @ActionId," +
                    "@UsersID,@OrderID,@QteMetric,@DataID,@MetricValue", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return baseModel;
        }


        public long GetOrderNOFromOrderID(int ActionId, long OrdersID)
        {
            long GetOrderNOFromOrderID = 0;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@OrderID",
                    Value = OrdersID
                });


                GetOrderNOFromOrderID = _dbContext.ExecuteStoredProcedure<long>("exec sp_OrderCallList @ActionId,@OrderID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return GetOrderNOFromOrderID;

        }


        public SqlResponseBaseModel SentPin(int ActionId, long ID)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = ID;


                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_PinList @ActionId,@ID", _ActionId, _Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return baseModel;
        }

        public long PinsToSendCount(int ActionId)
        {
            long PinsToSendCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                PinsToSendCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_PinList @ActionId", _ActionId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PinsToSendCount;
        }

        public string GetUserNamebyUsersID(int ActionId, long UsersID)
        {
            string GetUserNamebyUsersID = string.Empty;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = null ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UsersID;

                GetUserNamebyUsersID = _dbContext.ExecuteStoredProcedure<string>("exec SP_Get_UserDetails @ActionId,@UserName,@UserId", _ActionId, _UserName, _UserId).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetUserNamebyUsersID;
        }

        public long GetCustomerIDByUserName(int ActionId, string UserName)
        {
            long GetCustomerIDByUserName = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName ?? (Object)DBNull.Value;


                GetCustomerIDByUserName = _dbContext.ExecuteStoredProcedure<long>("exec SP_Get_UserDetails @ActionId,@UserName", _ActionId, _UserName).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetCustomerIDByUserName;
        }

        public string GetAspNetUserID_GUID(int ActionId, string UserName)
        {
            string GetAspNetUserID_GUID = string.Empty;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName ?? (Object)DBNull.Value;




                GetAspNetUserID_GUID = _dbContext.ExecuteStoredProcedure<string>("exec SP_Get_UserDetails @ActionId,@UserName", _ActionId, _UserName).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetAspNetUserID_GUID;
        }

        public int UsersQTE(int ActionId, int UserId, int RankId)
        {
            int Id = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                SqlParameter _RankId = new SqlParameter();
                _RankId.Direction = System.Data.ParameterDirection.Input;
                _RankId.DbType = System.Data.DbType.Int32;
                _RankId.ParameterName = "@RankId";
                _RankId.Value = RankId;


                Id = _dbContext.ExecuteStoredProcedure<int>("exec SP_Refresh @ActionId,@UserId,@RankId", _ActionId, _UserId, _RankId).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Id;
        }

        public int GetOrderIdByUser(int ActionId, int UserId, int RankId)
        {
            int Id = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                SqlParameter _RankId = new SqlParameter();
                _RankId.Direction = System.Data.ParameterDirection.Input;
                _RankId.DbType = System.Data.DbType.Int32;
                _RankId.ParameterName = "@RankId";
                _RankId.Value = RankId;


                Id = _dbContext.ExecuteStoredProcedure<int>("exec SP_Refresh @ActionId,@UserId,@RankId", _ActionId, _UserId, _RankId).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Id;
        }

        public long GetUserIDByNopCustomerID(int ActionId, long UserID)
        {
            long GetCustomerIDByUserID = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _CustomerId = new SqlParameter();
                _CustomerId.Direction = System.Data.ParameterDirection.Input;
                _CustomerId.DbType = System.Data.DbType.Int64;
                _CustomerId.ParameterName = "@CUSTOMER_ID";
                _CustomerId.Value = 0;

                SqlParameter _UserID = new SqlParameter();
                _UserID.Direction = System.Data.ParameterDirection.Input;
                _UserID.DbType = System.Data.DbType.String;
                _UserID.ParameterName = "@UserID";
                _UserID.Value = UserID;



                GetCustomerIDByUserID = _dbContext.ExecuteStoredProcedure<long>("exec sp_customer_details @ActionId,@CUSTOMER_ID,@UserID", _ActionId, _CustomerId, _UserID).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetCustomerIDByUserID;
        }
        public bool IsCommissionEnabled()
        {
            bool status = false;
            string sql = "select count(*) from CommissionSettings where CommissionEnabled=1";
            var dt = _dbContext.ExecuteSqlDataTable(sql);
            if(dt!=null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out int count);
                status = count > 0;
            }
            return status;
        }
        public string GetUserNameByEmail(long ActionId, string Email)
        {
            string UserName = "";
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = Email;
                UserName = _dbContext.ExecuteStoredProcedure<string>("exec SP_Get_UserDetails @ActionId,@UserName", _ActionId, _UserName).FirstOrDefault();



            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserName;
        }
        public string GetWebContent(string ContentName)
        {
            string Content = string.Empty;
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@ContentName",
                Value = ContentName
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("SP_GetWebContent", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                Content = Convert.ToString(dt.Rows[0][0]);
            }

            return Content;
        }
        public bool InsertCommissionSwept(int actionId,int UserId, int OrderId,decimal Amount)
        {
            SqlParameter[] parameters = new SqlParameter[4];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = actionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@userId",
                Value = UserId
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Decimal,
                ParameterName = "@amount",
                Value = Amount
            });
            parameters[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@OrderId",
                Value = OrderId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("InsertSweptCommission", parameters);
            return true;
        }
        public TransferHistoryModel TransferHistory(int ActionId,int UserId)
        {
            TransferHistoryModel m = new TransferHistoryModel();
            m.RecievedList= GetTransferRecieveList(ActionId, UserId);
            if (m.RecievedList != null)
            {
                m.TotalReceived = m.RecievedList.Sum(x => x.Transferamount);
            }
            m.TransferList = GetTransferRecieveList(2, UserId);
            if(m.TransferList != null)
            {
                m.TotalTransfer = m.TransferList.Sum(x => x.Transferamount);
            }
            return m;
        }

        private List<CashRecievedModel> GetTransferRecieveList(int ActionId, int UserId)
        {
            List<CashRecievedModel> m = new List<CashRecievedModel>();
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@userId",
                Value = UserId
            });

            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("TransferHistory", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                m = dt.ConvertDataTableToList<CashRecievedModel>();
                // m.RecievedList = list;
            }
            return m;
        }

        public List<BundelViewModel> GetBundleItems(int PageType)
        {
            List<BundelViewModel> bundels = new List<BundelViewModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@pageType",
                Value = PageType
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_GetBundle", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                List<BundleAllModel> items = new List<BundleAllModel>();
                foreach (DataRow item in dt.Rows)
                {
                    string BundleId = Convert.ToString(item["BundleId"]);
                    string BundleName = Convert.ToString(item["BundleName"]);
                    string BundleDisplayOrder = Convert.ToString(item["BundleDisplayOrder"]);
                    string ItemId = Convert.ToString(item["ItemId"]);
                    string ItemName = Convert.ToString(item["ItemName"]);
                    string ShortDescription = Convert.ToString(item["ShortDescription"]);
                    string Price = Convert.ToString(item["Price"]);
                    string ItemDisplayOrder = Convert.ToString(item["ItemDisplayOrder"]);

                    int.TryParse(BundleId, out int BundleIdVal);
                    int.TryParse(BundleDisplayOrder, out int BundleDisplayOrderVal);
                    int.TryParse(ItemId, out int ItemIdVal);
                    decimal.TryParse(Price, out decimal PriceVal);
                    int.TryParse(ItemDisplayOrder, out int ItemDisplayOrderVal);

                    items.Add(new BundleAllModel
                    {
                        BundleId = BundleIdVal,
                        BundleDisplayOrder = BundleDisplayOrderVal,
                        BundleName = BundleName,
                        ItemDisplayOrder = ItemDisplayOrderVal,
                        ItemId = ItemIdVal,
                        ItemName = ItemName,
                        Price = PriceVal,
                        ShortDescription = ShortDescription
                    });

                    
                }

                if (items != null && items.Count > 0)
                {
                    IEnumerable<BundleAllModel> v = items.GroupBy(x => x.BundleId).Select(x => x.First());
                    bundels = v.Select(x => new BundelViewModel
                    {
                        BundleId = x.BundleId,
                        BundleName = x.BundleName,
                        BundleDisplayOrder = x.BundleDisplayOrder
                    }).OrderBy(x => x.BundleDisplayOrder).Distinct().ToList();
                    if (bundels != null && bundels.Count > 0)
                    {
                        foreach (var bundle in bundels)
                        {
                            bundle.bundleItems = items.Where(x => x.BundleId == bundle.BundleId).Select(x => new BundleItems
                            {
                                ItemId = x.ItemId,
                                ItemDisplayOrder = x.ItemDisplayOrder,
                                ItemName = x.ItemName,
                                Price = x.Price,
                                ShortDescription = x.ShortDescription
                            }).OrderBy(x => x.ItemDisplayOrder).ToList();
                        }
                    }
                }
            }

            return bundels;
        }
        public List<ImportOrderModel> ImportOrders(int id)
        {
            List<ImportOrderModel> orders = new List<ImportOrderModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@id",
                Value = 0
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_ImportOrderCommissions", parameters);
            if(dt!=null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    ImportOrderModel m = new ImportOrderModel();
                    m.OrderId = Convert.ToInt32(item["OrderId"]);
                    m.Customerid = Convert.ToInt32(item["customerid"]);

                    orders.Add(m);
                }
            }
            
            return orders;
        }
        public List<ImportOrderForPlacementModel> OrdersForPlacement(int UserId)
        {
            List<ImportOrderForPlacementModel> orderList = new List<ImportOrderForPlacementModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@userId",
                Value = UserId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetOrdersForPlacement", parameters);
            if(dt!=null && dt.Rows.Count > 0)
            {
                orderList = dt.ConvertDataTableToList<ImportOrderForPlacementModel>();
            }
            return orderList;
        }
        public UserHeaderInfoModel GetUserHeaderInfo(string EmailId,int NopUserId)
        {
            UserHeaderInfoModel m =null;
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = 1
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@emailId",
                Value = EmailId
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@NopUserId",
                Value = NopUserId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetUserHeaderInfo", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                var res = dt.ConvertDataTableToList<UserHeaderInfoModel>();
                if(res!=null && res.Count > 0)
                {
                    m = new UserHeaderInfoModel();
                    m = res.First();
                }
            }
                return m;
        }
        public UserAddressInfoModel GetUserAddress(string Username,string Email,int NopUserId)
        {
            UserAddressInfoModel m =null;
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@username",
                Value = Username
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@email",
                Value = Email
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@NopUserId",
                Value = NopUserId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetUserAddress", parameters);
            if(dt!=null && dt.Rows.Count > 0)
            {
                var r = dt.ConvertDataTableToList<UserAddressInfoModel>();
                if (r != null && r.Count>0)
                {
                    m = r.First();
                }
            }
            return m;
        }
        public CommissionSUmmaryModel GetAvailableCommissions(int ActionId,int UserId)
        {
            CommissionSUmmaryModel commission = new CommissionSUmmaryModel();
           
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UsersID",
                Value = UserId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetAvailbleCommissions", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                var m = dt.ConvertDataTableToList<CommissionSUmmaryModel>();
                if (m != null)
                {
                    commission = m.First();
                }
            }
            return commission;
        }
        public List<TargetUserNameViewModel> GetUserByUsername(int ActionId,string UserName)
        {
            List<TargetUserNameViewModel> m = new List<TargetUserNameViewModel>();
            string sql = "select ID,UserName,firstname,lastname,email,Phone from users where username like '" + "%"+ UserName +"%"+ "'";
            DataTable dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {

                    int.TryParse(Convert.ToString(item["ID"]), out int ID);
                    
                    string Email = Convert.ToString(item["Email"]);
                    string UserNameNew = Convert.ToString(item["UserName"]);
                    string Firstname = Convert.ToString(item["Firstname"]);
                    string Lastname = Convert.ToString(item["Lastname"]);
                    string Phone = Convert.ToString(item["Phone"]);
                  
                    m.Add(new TargetUserNameViewModel
                    {                       
                        Email = Email,
                        FirstName = Firstname,
                        ID = ID,
                        
                        Phone = Phone,
                        UserName = UserNameNew,
                        LastName = Lastname

                    });
                }
            }
            return m;
        }
        public int GetUserIdFromNopUserId( int NopUserId)
        {
            int id = 0;
            SqlParameter[] parameter = new SqlParameter[1];

            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@NopUserId",
                Value = NopUserId
            };
           

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetUserIdFromNopUserId", parameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out id);
            }

            return id;
        }
    }
   
}
