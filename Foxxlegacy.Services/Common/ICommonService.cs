﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Foxxlegacy.ViewModel.ViewModel;
namespace Foxxlegacy.Services.Common
{
   public interface ICommonService
    {
        EmailViewModel SetEmailSubjectAndBody(int ActionId, long EmailID);
        SqlResponseBaseModel InsertToEmailQue(int ActionId, EmailViewModel model);

         List<StateListViewModel>  GetStateList(int ActionId);

        IEnumerable<SelectListItem> GetAllDropdownlist(long ActionId);

        long GetRankID(int ActionId, long UsersID);

        string RankName(int ActionId, long RankID);
        string HighestRank(int UserId);

        SqlResponseBaseModel InsertDownlineCounts(int ActionId, DownlineCountsViewModel model);

        UsersQTENowforMBOViewModel GetQteNowForMBODetails(int ActionId, long PlacementUserID);
        List<long> GetTeamCustomersCount(int ActionId, long Id);

        SqlResponseBaseModel RemoveDownlineCounts(int ActionId, DownlineCountsViewModel model);

        long GetUsersIDbyUserName(int ActionId, string UserName);
        bool CheckDuplicateUserName(string UserName);
        bool CheckDuplicateEmail(string Email);
        GuestResponseModel CheckDuplicateEmailForGuestUser(string Email);
        long GetCustomerIDByUserID(int ActionId, long UserID);

        PlatinumCodesListViewModel GetUsersPlatinumCodes(int ActionId,long UsersID);

        SqlResponseBaseModel CleanUpOrder0QtePromoData(int ActionId, long UserID);

        long GetQteDataRecord(int ActionId, long UsersID ,long OrderID);

        long GetQteMetaValue(int ActionId, long DataID, string QteMetric);

        SqlResponseBaseModel SetQteMetaValue(int ActionId,long DataID , string QteMetric, long MetricValue);

        long GetOrderNOFromOrderID(int ActionId, long OrdersID);

        SqlResponseBaseModel SentPin(int ActionId, long ID);

        long PinsToSendCount(int ActionId);


        string GetUserNamebyUsersID(int ActionId, long UsersID);

        long GetCustomerIDByUserName(int ActionId, string UserName);

        string GetAspNetUserID_GUID(int ActionId, string UserName);

        int UsersQTE(int ActionId, int UserId, int RankId);
        int GetOrderIdByUser(int ActionId, int UserId, int RankId);
        long GetUserIDByNopCustomerID(int ActionId, long UserID);
        bool IsCommissionEnabled();
        string GetUserNameByEmail(long ActionId, string Email);
        string GetWebContent(string ContentName);
        List<BundelViewModel> GetBundleItems(int PageType);
        List<ImportOrderModel> ImportOrders(int id);
        List<ImportOrderForPlacementModel> OrdersForPlacement(int UserId);
        UserHeaderInfoModel GetUserHeaderInfo(string EmailId, int NopUserId);
        UserAddressInfoModel GetUserAddress(string Username, string Email, int NopUserId);
        List<CountryStateModel> GetCountriesAndStates(int ActionId, int CountryId);
        CommissionSUmmaryModel GetAvailableCommissions(int ActionId, int UserId);
        int GetUserIdFromNopUserId(int NopUserId);
        bool InsertCommissionSwept(int actionId, int UserId, int OrderId, decimal Amount);
        CountryStateIdModel GetUserAddressByUserID(int UserID);
        List<TargetUserNameViewModel> GetUserByUsername(int ActionId, string UserName);
        TransferHistoryModel TransferHistory(int ActionId, int UserId);
    }
}
