﻿using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.User
{
    public class UserService : IUserService
    {

        private readonly IDbContext _dbContext;

        public UserService()
        {
        }

        #region CTor
        public UserService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        #endregion


        public UserDetailsViewModel GetUserDetails(int ActionId, string UserName, long UserId)
        {
            UserDetailsViewModel model = new UserDetailsViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;



                model = _dbContext.ExecuteStoredProcedure<UserDetailsViewModel>("exec SP_Get_UserDetails @ActionId,@UserName,@UserId", _ActionId, _UserName, _UserId).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public LoggedinUserDetails GetLoginUserDetails(int ActionId, UserLoginRequestViewModel userLogin)
        {
            LoggedinUserDetails model = new LoggedinUserDetails();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = userLogin.UserName ?? (Object)DBNull.Value;

                SqlParameter _Password = new SqlParameter();
                _Password.Direction = System.Data.ParameterDirection.Input;
                _Password.DbType = System.Data.DbType.String;
                _Password.ParameterName = "@Password";
                _Password.Value = userLogin.Password ?? (Object)DBNull.Value;




                model = _dbContext.ExecuteStoredProcedure<LoggedinUserDetails>("exec SP_Get_LoginUserDetails @ActionId,@UserName,@Password", _ActionId, _UserName, _Password).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public UserDetailsViewModel GetUserDetailsbyUsersID(int ActionId, UserViewModel modelobj)
        {
            UserDetailsViewModel model = new UserDetailsViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = modelobj.UserName ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = modelobj.UserId;



                model = _dbContext.ExecuteStoredProcedure<UserDetailsViewModel>("exec SP_Get_UserDetails @ActionId,@UserName,@UserId", _ActionId, _UserName, _UserId).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public UserInfoListViewModel GetUserInfo(int ActionId, UserInfoViewModel model)
        {
            UserInfoListViewModel modelList = new UserInfoListViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ID";
                _ID.Value = model.ID;



                modelList = _dbContext.ExecuteStoredProcedure<UserInfoListViewModel>("exec SP_UserInfo @ActionId,@ID", _ActionId, _ID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public List<PromotionRanksViewModel> GetAllPromotionRanks(int ActionId, long RankID)
        {
            List<PromotionRanksViewModel> modelList = new List<PromotionRanksViewModel>(); ;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@RankID";
                _ID.Value = RankID;



                modelList = _dbContext.ExecuteStoredProcedure<PromotionRanksViewModel>("exec SP_GetAllPromotionRanks @ActionId,@RankID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public bool IsUserInRole(int ActionId, string userName, string roleName)
        {
            bool IsUserInRole = false;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _userName = new SqlParameter();
                _userName.Direction = System.Data.ParameterDirection.Input;
                _userName.DbType = System.Data.DbType.String;
                _userName.ParameterName = "@userName";
                _userName.Value = userName ?? (object)DBNull.Value;

                SqlParameter _roleName = new SqlParameter();
                _roleName.Direction = System.Data.ParameterDirection.Input;
                _roleName.DbType = System.Data.DbType.String;
                _roleName.ParameterName = "@roleName";
                _roleName.Value = roleName ?? (object)DBNull.Value;

                IsUserInRole = _dbContext.ExecuteStoredProcedure<bool>("exec SP_UserRole @ActionId,@userName,@roleName", _ActionId, _userName, _roleName).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsUserInRole;
        }
        public int GetNopUserId(int ActionId, int UserId, string Email)
        {
            int id = 0;
            SqlParameter[] parameter = new SqlParameter[3];

            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@actionid",
                Value = ActionId
            };
            parameter[1] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@userId",
                Value = UserId
            };
            parameter[2] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@email",
                Value = Email
            };


            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetNopUserId", parameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out id);
            }

            return id;
        }


        public List<UserRoleListViewModel> GetAllRoles(int ActionId, string userName)
        {
            List<UserRoleListViewModel> UserRoleListViewModel = new List<UserRoleListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _userName = new SqlParameter();
                _userName.Direction = System.Data.ParameterDirection.Input;
                _userName.DbType = System.Data.DbType.String;
                _userName.ParameterName = "@userName";
                _userName.Value = userName ?? (object)DBNull.Value;



                UserRoleListViewModel = _dbContext.ExecuteStoredProcedure<UserRoleListViewModel>("exec SP_UserRole @ActionId,@userName", _ActionId, _userName).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserRoleListViewModel;
        }

        public SqlResponseBaseModel AddRemoveUserRole(int ActionId, string UserId, string RoleId)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _userName = new SqlParameter();
                _userName.Direction = System.Data.ParameterDirection.Input;
                _userName.DbType = System.Data.DbType.String;
                _userName.ParameterName = "@userName";
                _userName.Value = "";


                SqlParameter _roleName = new SqlParameter();
                _roleName.Direction = System.Data.ParameterDirection.Input;
                _roleName.DbType = System.Data.DbType.String;

                _roleName.ParameterName = "@roleName";
                _roleName.Value = "";

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.String;
                _UsersID.ParameterName = "@UserId";
                _UsersID.Value = UserId;

                SqlParameter _RoleId = new SqlParameter();
                _RoleId.Direction = System.Data.ParameterDirection.Input;
                _RoleId.DbType = System.Data.DbType.String;
                _RoleId.ParameterName = "@RoleId";
                _RoleId.Value = RoleId;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_UserRole @ActionId,@userName,@roleName,@UserId,@RoleId", _ActionId, _userName, _roleName, _UsersID, _RoleId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public List<PersonalsListViewModel> GetPersonalsList(int ActionId, string userName)
        {
            List<PersonalsListViewModel> modelList = new List<PersonalsListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _userName = new SqlParameter();
                _userName.Direction = System.Data.ParameterDirection.Input;
                _userName.DbType = System.Data.DbType.String;
                _userName.ParameterName = "@userName";
                _userName.Value = userName ?? (object)DBNull.Value;



                modelList = _dbContext.ExecuteStoredProcedure<PersonalsListViewModel>("exec SP_Personals @ActionId,@userName", _ActionId, _userName).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public List<MessagesListViewModel> GetMessagesList(int ActionId, long UserId)
        {
            List<MessagesListViewModel> modelList = new List<MessagesListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;



                modelList = _dbContext.ExecuteStoredProcedure<MessagesListViewModel>("exec SP_Messages @ActionId,@UserId", _ActionId, _UserId).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public List<HistoryofContactsListViewModel> GetHistoryofContactsList(int ActionId, long ContactID)
        {
            List<HistoryofContactsListViewModel> modelList = new List<HistoryofContactsListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = 0;



                SqlParameter _ContactID = new SqlParameter();
                _ContactID.Direction = System.Data.ParameterDirection.Input;
                _ContactID.DbType = System.Data.DbType.Int64;
                _ContactID.ParameterName = "@ContactID";
                _ContactID.Value = ContactID;

                modelList = _dbContext.ExecuteStoredProcedure<HistoryofContactsListViewModel>("exec SP_Messages @ActionId,@UserId,@ContactID", _ActionId, _UserId, _ContactID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public List<DialogueList> GetHistoryofContactsDialogueList(int ActionId, long ContactID)
        {
            List<DialogueList> modelList = new List<DialogueList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = 0;



                SqlParameter _ContactID = new SqlParameter();
                _ContactID.Direction = System.Data.ParameterDirection.Input;
                _ContactID.DbType = System.Data.DbType.Int64;
                _ContactID.ParameterName = "@ContactID";
                _ContactID.Value = ContactID;

                modelList = _dbContext.ExecuteStoredProcedure<DialogueList>("exec SP_Messages @ActionId,@UserId,@ContactID", _ActionId, _UserId, _ContactID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public SqlResponseBaseModel InsertUpdateUsersSignUpData(long ActionId, UserViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = model.NewUserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@OrderID",
                    Value = model.OrderID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@SponsorID",
                    Value = model.SponsorID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = model.UserName.Trim() ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Password",
                    Value = model.Password ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FirstName",
                    Value = model.FirstName ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@LastName",
                    Value = model.LastName ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Email",
                    Value = model.Email ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Phone",
                    Value = model.Phone ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Address",
                    Value = model.Address ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@City",
                    Value = model.City ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@State",
                    Value = model.State ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Zip",
                    Value = model.ZipCode ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@SignupType",
                    Value = model.SignupType
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@IsCustomerAUser",
                    Value = model.IsCustomerAUser
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@retailOnly",
                    Value = model.RetailOnly ?? (Object)DBNull.Value
                });
                //parameter.Add(new SqlParameter
                //{
                //    Direction = System.Data.ParameterDirection.Input,
                //    DbType = System.Data.DbType.String,
                //    ParameterName = "@CustomerGuid",
                //    Value = model.GuiId.ToString()
                //});


                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_UsersSignUpData @ActionId," +
                    "@UserId,@OrderID,@SponsorID, @UserName, @Password, @FirstName, @LastName, @Email, @Phone, @Address, @City, @State,@Zip,@SignupType,@IsCustomerAUser,@retailOnly", parameter.ToArray()).FirstOrDefault();


            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public SetSmsCodeViewModel SetSmsCode(int ActionId, long UserId, string UserName, string SmsCode, int? val = null)
        {
            SetSmsCodeViewModel model = new SetSmsCodeViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionID",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userId",
                    Value = UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SmsCode",
                    Value = SmsCode ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = UserName ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@val",
                    Value = val ?? (Object)DBNull.Value
                });
                model = _dbContext.ExecuteStoredProcedure<SetSmsCodeViewModel>("exec Sp_SetSMSCode @actionID, @userId, @SmsCode, @UserName,@val", parameter.ToArray()).FirstOrDefault();


            }
            catch (Exception ex)
            {

            }
            return model;
        }
        public void AddSponsorInfoToCsj(int userId, string SponsorName)
        {
            string sql = "insert into csj(usersid,entrydate,adminid,comments) values(" + userId + ",'" + DateTime.Now + "'," + userId + ",'" + SponsorName + "')";
            _dbContext.ExecuteInsertQuery(sql, null);
        }
        public SetEmailViewModel GetEmailData(int ActionId, int EmailId)
        {
            SetEmailViewModel model = new SetEmailViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@emailId",
                    Value = EmailId
                });
                model = _dbContext.ExecuteStoredProcedure<SetEmailViewModel>("exec Sp_Email @actionId, @emailId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {

            }
            return model;
        }
        public SetEmailViewModel GetEmailDataByTaskName(int ActionId, string TaskName)
        {
            SetEmailViewModel model = new SetEmailViewModel();

            List<object> parameter = new List<object>();
            parameter.Add(new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameter.Add(new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@task",
                Value = TaskName
            });
            model = _dbContext.ExecuteStoredProcedure<SetEmailViewModel>("exec Sp_Email_by_task @actionId, @task", parameter.ToArray()).FirstOrDefault();


            return model;
        }

        public DataTable SetUSUFLEROr(long userId)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT Users.UserName, Users_1.UserName AS UsersSponsor, Users.FirstName, Users.LastName, Users.Phone, Users.Email, Ranks.RankName, Ranks_1.RankName AS OldRankName FROM Users LEFT OUTER JOIN Users AS Users_1 ON Users.SponsorUsersID = Users_1.ID INNER JOIN Ranks ON Users.RankID = Ranks.ID  LEFT OUTER JOIN Ranks AS Ranks_1 ON Users.RankID - 1 = Ranks_1.ID WHERE (Users.ID = " + userId + ")";
                dt = _dbContext.ExecuteSqlDataTable(sql, null);
            }
            catch (Exception ex)
            {

            }

            return dt;
        }
        public int GetSponsorsUsersID(long userId)
        {
            int SponsorId = 0;
            try
            {
                string sql = "Select SponsorUsersID from Users where (ID = " + userId + ")";
                DataTable dt = new DataTable();
                dt = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    string id = Convert.ToString(dt.Rows[0]["SponsorUsersID"]);
                    int.TryParse(id, out SponsorId);
                }
            }
            catch (Exception ex)
            {

            }
            return SponsorId;
        }
        public int InsertToEmailQue(string FromAddress, string ToAddress, string Subject, string Body, bool IsSent)
        {
            try
            {
                //  var a =Replace(Replace(strin, "'", "''"), ChrW(0), "")
                Body = Body.Replace("'", "''");
                SqlParameter[] parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FromAddress",
                    Value = FromAddress
                };

                parameters[1] = new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToAddress",
                    Value = ToAddress
                };
                parameters[2] = new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Subject",
                    Value = Subject
                };
                parameters[3] = new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Body",
                    Value = Body
                };
                parameters[4] = new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@Sent",
                    Value = IsSent
                };

                string sql = "Insert into EmailQue ([FromEmailAddress],[ToEmailAddress],[Subject],[Body],[Sent]) values (@FromAddress,@ToAddress,@Subject,@Body,@Sent)";
                var data = _dbContext.ExecuteInsertQuery(sql, parameters);
            }
            catch (Exception ex)
            {

            }

            return 0;
        }
        public SqlResponseBaseModel SetTempOptInCode(int ActionId, PhoneVerificationViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userId",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@smsCode",
                    Value = model.Code ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToNumber",
                    Value = model.ToNumber ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FromNumber",
                    Value = model.FromNumber ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SmsText",
                    Value = model.Smstext ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UUID",
                    Value = model.UUID ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@PlivoQueueID",
                    Value = model.PlivoQueueID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@PlivoDefaultNumber",
                    Value = model.PlivoDefaultNumber ?? (Object)DBNull.Value
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec Sp_PhoneVerification @ActionId," +
                   "@UserId,@smsCode,@ToNumber, @FromNumber, @SmsText, @UUID, @PlivoQueueID, @PlivoDefaultNumber", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {

            }
            return _baseModel;
        }
        public bool UpdatePlivoSMSStatus(long PlivoQueueID, string UUID)
        {
            bool status = false;
            string sql = "Update PlivoQueue set UUID = '" + UUID + "' where ID=" + PlivoQueueID + " ";
            if (!string.IsNullOrEmpty(sql))
            {
                status = _dbContext.ExecuteInsertQuery(sql, null);
            }
            return status;
        }
        public bool SMSOptIN(int UserID)
        {
            bool status = false;
            string sql = "Update Users set SMSOptIn = 1 WHERE ID=" + UserID + " ";
            if (!string.IsNullOrEmpty(sql))
            {
                status = _dbContext.ExecuteInsertQuery(sql, null);
            }
            string sql1 = "Delete from SMS_TempOptInCode WHERE UsersID=" + UserID + " ";
            if (!string.IsNullOrEmpty(sql1))
            {
                status = _dbContext.ExecuteInsertQuery(sql1, null);
            }
            return status;
        }
        public List<UserPersonalInformationViewModel> GetUserDetailsList(long ActionId, BroadcastEmailUserRequest model)
        {
            var list = new List<UserPersonalInformationViewModel>();
            try
            {
                var Parameter = new List<object>();
                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlValue = model.UserId
                });

                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Size = 500,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserName",
                    SqlValue = model.UserName ?? string.Empty
                });

                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CategoryId",
                    SqlValue = model.CategoryId
                });

                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@RankId",
                    SqlValue = model.RankId
                });

                /* exec dbo.sp_get_user_list @Actionid = 3, @UserId = 0, @UserName = '', @CategoryId = 0, @RankId='' */
                list = _dbContext.ExecuteStoredProcedure<UserPersonalInformationViewModel>("exec sp_get_user_list @Actionid, @UserId, @UserName, @CategoryId, @RankId", Parameter.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return list;
        }
        public List<TargetUserNameViewModel> GetTargetUsersforPlacement(int UserId, string TargetUserName, int MoveUserId)
        {
            List<TargetUserNameViewModel> m = new List<TargetUserNameViewModel>();
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@currentUserId",
                Value = UserId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@UserName",
                Value = TargetUserName
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MoveUserId",
                Value = MoveUserId
            });


            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetTargetUsernamesForPlacement", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {

                    int.TryParse(Convert.ToString(item["ID"]), out int ID);
                    int.TryParse(Convert.ToString(item["CustomerID"]), out int CustomerID);
                    string Email = Convert.ToString(item["Email"]);
                    string UserName = Convert.ToString(item["UserName"]);
                    string Firstname = Convert.ToString(item["Firstname"]);
                    string Lastname = Convert.ToString(item["Lastname"]);
                    string Phone = Convert.ToString(item["Phone"]);
                    bool IsInDownline = Convert.ToBoolean(item["IsInDownline"]);
                    m.Add(new TargetUserNameViewModel
                    {
                        CustomerID = CustomerID,
                        Email = Email,
                        FirstName = Firstname,
                        ID = ID,
                        IsInDownline = IsInDownline,
                        Phone = Phone,
                        UserName = UserName,
                        LastName = Lastname

                    });
                }
            }

            return m;
        }
        public List<TargetUserNameViewModel> GetTargetUsersforOrderPlacement(string TargetUserName)
        {
            List<TargetUserNameViewModel> m = new List<TargetUserNameViewModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@UserName",
                Value = TargetUserName
            });



            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetUsersByUserName", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                m = dt.ConvertDataTableToList<TargetUserNameViewModel>();
            }

            return m;
        }
        public List<UserRankViewModel> GetUserRankList(long ActionId)
        {
            var list = new List<UserRankViewModel>();
            try
            {
                var Parameter = new List<object>();
                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });


                /* exec SP_RankDetails 3 */
                list = _dbContext.ExecuteStoredProcedure<UserRankViewModel>("exec SP_RankDetails @Actionid", Parameter.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return list;
        }

        public UserQualifiedRankDataViewModel GetUserRankQualifiedDataDetails(int UserId)
        {
            var RankData = new UserQualifiedRankDataViewModel();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 1
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec SP_Rankup @ActionId=5, @UserId = 1 */
                RankData = _dbContext.ExecuteStoredProcedure<UserQualifiedRankDataViewModel>("exec SP_Rankup @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RankData;
        }

        public UserRankEmailOptDataViewModel GetUserRankEmailOptDataDetails(int UserId)
        {
            var RankData = new UserRankEmailOptDataViewModel();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 1
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_fetch_user_details @ActionId = 1, @UserId = 1 */
                RankData = _dbContext.ExecuteStoredProcedure<UserRankEmailOptDataViewModel>("exec sp_fetch_user_details @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RankData;
        }
        public UserRankSmsOptDataViewModel GetUserRankSmsOptDataDetails(int UserId)
        {
            var RankData = new UserRankSmsOptDataViewModel();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 2
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_fetch_user_details @ActionId = 1, @UserId = 1 */
                RankData = _dbContext.ExecuteStoredProcedure<UserRankSmsOptDataViewModel>("exec sp_fetch_user_details @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RankData;
        }
        public UserRankOptDataViewModel GetUserRankOptDataDetails(int UserId)
        {
            var RankData = new UserRankOptDataViewModel();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 3
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_fetch_user_details @ActionId = 1, @UserId = 1 */
                RankData = _dbContext.ExecuteStoredProcedure<UserRankOptDataViewModel>("exec sp_fetch_user_details @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RankData;
        }
        public PlaceUserCountViewModel GetPlacementUserCount(int UserId, int SponsorUserId)
        {
            var Countdata = new PlaceUserCountViewModel();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 4
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@SponsorUsersID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = SponsorUserId
                });

                /* exec sp_fetch_user_details @ActionId = 4, @UserId = 6, @SponsorUsersID = 1 */
                Countdata = _dbContext.ExecuteStoredProcedure<PlaceUserCountViewModel>("exec sp_fetch_user_details @ActionId, @UserId, @SponsorUsersID", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Countdata;
        }
        public PlaceUserCountViewModel GetAdminPlacementUserCount(int UserId, int SponsorUserId)
        {
            var Countdata = new PlaceUserCountViewModel();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 1
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@SponsorUsersID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = SponsorUserId
                });

                /* exec sp_fetch_user_details @ActionId = 4, @UserId = 6, @SponsorUsersID = 1 */
                Countdata = _dbContext.ExecuteStoredProcedure<PlaceUserCountViewModel>("exec GetAdminPlacementUserCount @ActionId, @UserId, @SponsorUsersID", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Countdata;
        }

        public int UpdateUserPlacement(UpdateUserPlacementRequestViewModel model)
        {
            int RowCount = 0;

            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 5
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@SponsorUsersID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.SponsorUserId
                });

                /* exec sp_fetch_user_details @ActionId = 5, @UserId = 4279, @SponsorUsersID = 1 */
                var ResponseData = _dbContext.ExecuteStoredProcedure<UpdateUserPlacementResponseViewModel>("exec sp_fetch_user_details @ActionId, @UserId, @SponsorUsersID", parameter.ToArray()).FirstOrDefault();
                if (ResponseData.AffectedRow > 0)
                    RowCount = ResponseData.AffectedRow;
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RowCount;
        }

        public bool DeleteOrderForPlacement(int OrderId, int TargetUserId)
        {
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@orderId",
                Value = OrderId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@TargetUserId",
                Value = TargetUserId
            });

            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("DeletePlacementOrder", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {

            }
            return true;
        }

        public List<TargetUserNameViewModel> FindAllusersInUplineDownline(int UserId, int SearchType)
        {
            string SpName = SearchType == 0 ? "findallusersinupline" : "findallusersindownline";
            List<TargetUserNameViewModel> m = new List<TargetUserNameViewModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = UserId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable(SpName, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                m = dt.ConvertDataTableToList<TargetUserNameViewModel>();
            }
            return m;
        }
        public List<UserOrdersViewModel> GetAllOrdersByUserId(int UserId)
        {
            List<UserOrdersViewModel> orders = new List<UserOrdersViewModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = UserId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetAllOrdersByOrderId", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                orders = dt.ConvertDataTableToList<UserOrdersViewModel>();
            }
            return orders;
        }
        public void DownRankUplineReCheck(int DonatingSponsorId)
        {
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = 1
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@id",
                Value = DonatingSponsorId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("DownRankSponsorUser", parameters);
        }
        public bool DeleteDownlineCount(int ActionId, int UserId, int OrderId)
        {
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = UserId
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@orderID",
                Value = OrderId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("DeleteDownlinCountsByUserId", parameters);

            return true;
        }
        public int ActiveCustomerMetaData(int Actionid, int UserId, int OrderId, int UniLevelSponsorID, bool PersonalCustomer, int insertNextUnilevelSponsorID)
        {
            int RowCount = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@OrderId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = OrderId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UniLevelSponsorID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UniLevelSponsorID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@PersonalCustomer",
                    SqlDbType = System.Data.SqlDbType.Bit,
                    Value = PersonalCustomer
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@insertNextUnilevelSponsorID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = insertNextUnilevelSponsorID
                });

                /* exec sp_fetch_user_details @ActionId = 5, @UserId = 4279, @SponsorUsersID = 1 */
                var ResponseData = _dbContext.ExecuteStoredProcedure<int>("exec SP_MetaDataUpline " +
                    "@ActionId, @UserId,@OrderID,@UniLevelSponsorID,@PersonalCustomer,@insertNextUnilevelSponsorID", parameter.ToArray()).FirstOrDefault();
                if (ResponseData > 0)
                    RowCount = ResponseData;
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return RowCount;
        }

        public string UserNameFromSmsCode(string SmsCode)
        {
            string UserName = string.Empty;
            try
            {
                SqlParameter _smsCode = new SqlParameter();
                _smsCode.Direction = System.Data.ParameterDirection.Input;
                _smsCode.DbType = System.Data.DbType.String;
                _smsCode.ParameterName = "@smsCode";
                _smsCode.Value = SmsCode;

                UserName = _dbContext.ExecuteStoredProcedure<string>("exec SP_UserNameFromSmsCode @smsCode", _smsCode).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserName;
        }
        public void New404Log(string LostUser, string Description, string REMOTE_ADDR)
        {
            if (LostUser != "robots.txt" && LostUser != "autodiscover.xml")
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@LostUser",
                    Value = LostUser
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = Description
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@IPAddress",
                    Value = REMOTE_ADDR
                });

                var aa = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_New404Log @LostUser,@Description,@IPAddress", parameter.ToArray());
            }
        }

        public string GetPassword(int ActionId, long UserId)
        {
            string _password = string.Empty;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                _password = _dbContext.ExecuteStoredProcedure<string>("exec SP_Messages @ActionId,@UserId", _ActionId, _UserId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _password;
        }

        public bool HasW9(int UserId)
        {
            bool status = false;
            string sql = "Select Count(ID) from w9 WHERE (UsersID=" + UserId + ")";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                string val = Convert.ToString(dt.Rows[0][0]);
                int.TryParse(val, out int values);
                status = values > 0;
            }
            return status;
        }
        public CommissionEarnedMode GetTotalEarned(int UserId)
        {
            CommissionEarnedMode commission = new CommissionEarnedMode();
            commission.commissionList = new List<CommissionEarnedDescriptionModel>();
            decimal Amount = 0;
            string sql = "Select Sum(Amount) as total from Commissions where UsersID =" + UserId + " AND (Active =1)";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                string val = Convert.ToString(dt.Rows[0][0]);
                decimal.TryParse(val, out Amount);
                commission.amount = Amount;

            }
            //SELECT SUM(Amount) AS total, DATEPART(year, DateAvailable) AS TheYear FROM Commissions WHERE (GETDATE() &gt; DateAvailable) AND (UsersID =@UsersID) AND (Active =1) GROUP BY DATEPART(year, DateAvailable) ORDER BY TheYear DESC
            string sql2 = "SELECT SUM(Amount) AS total, DATEPART(year, DateAvailable) AS TheYear FROM Commissions WHERE (UsersID =" + UserId + ") AND (Active =1) GROUP BY DATEPART(year, DateAvailable) ORDER BY TheYear DESC";
            var dt2 = _dbContext.ExecuteSqlDataTable(sql2, null);
            if (dt2 != null && dt2.Rows.Count > 0)
            {
                foreach (DataRow item in dt2.Rows)
                {
                    string total = Convert.ToString(item[0]);
                    string TheYear = Convert.ToString(item[1]);
                    decimal.TryParse(total, out Amount);
                    // int.TryParse(TheYear, out int year);
                    commission.commissionList.Add(new CommissionEarnedDescriptionModel { total = Amount, TheYear = TheYear, Type = "Year", TheMonthInt = 0 });
                }
            }
            return commission;

        }
        public List<CommissionEarnedDescriptionModel> GetCommissionByYear(int userId, int Year)
        {
            List<CommissionEarnedDescriptionModel> models = new List<CommissionEarnedDescriptionModel>();
            //SELECT SUM(Amount) AS total, DATENAME(month, DateAvailable) AS TheMonth, DATEPART(year, DateAvailable) AS TheYear, DATEPART(month, DateAvailable) AS TheMonthINT FROM Commissions WHERE (GETDATE() &gt; DateAvailable) AND (UsersID = @UsersID) AND (DATEPART(year, DateAvailable) = @GridView4Year) AND (Active = 1) GROUP BY DATENAME(month, DateAvailable), DATEPART(month, DateAvailable), DATEPART(year, DateAvailable) ORDER BY TheMonthINT DESC
            string sql = "SELECT SUM(Amount) AS total, DATENAME(month, DateAvailable) AS TheMonth, DATEPART(year, DateAvailable) AS TheYear, DATEPART(month, DateAvailable) AS TheMonthINT FROM Commissions WHERE (GETDATE() > DateAvailable) AND (UsersID = " + userId + ") AND (DATEPART(year, DateAvailable) = " + Year + ") AND (Active = 1) GROUP BY DATENAME(month, DateAvailable), DATEPART(month, DateAvailable), DATEPART(year, DateAvailable) ORDER BY TheMonthINT DESC";
            var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt2 != null && dt2.Rows.Count > 0)
            {
                foreach (DataRow item in dt2.Rows)
                {
                    string total = Convert.ToString(item[0]);
                    string theMonth = Convert.ToString(item[1]);
                    string TheYear = Convert.ToString(item[2]);
                    string month = Convert.ToString(item[3]);
                    decimal.TryParse(total, out decimal Amount);
                    int.TryParse(month, out int TheMonth);
                    models.Add(new CommissionEarnedDescriptionModel { total = Amount, TheYear = TheYear, TheMonth = theMonth, Type = "Month", TheMonthInt = TheMonth });
                }
            }
            return models;
        }

        public List<CommissionEarnedDescriptionModel> GetCommissionByMonth(int userId, int Year, int Month)
        {
            List<CommissionEarnedDescriptionModel> models = new List<CommissionEarnedDescriptionModel>();
            //SELECT SUM(Commissions.Amount) AS total, CommissionTypes.CommissionName FROM Commissions INNER JOIN CommissionTypes ON Commissions.CommissionTypesID = CommissionTypes.ID WHERE (GETDATE() > Commissions.DateAvailable) AND (Commissions.UsersID = @UsersID) AND (Commissions.Active = 1) AND (DATEPART(year, Commissions.DateAvailable) = @GridView4Year) AND (DATEPART(month, Commissions.DateAvailable) = @GridView5Month) GROUP BY CommissionTypes.CommissionName ORDER BY CommissionTypes.CommissionName DESC

            string sql = "SELECT SUM(Commissions.Amount) AS total, CommissionTypes.CommissionName FROM Commissions INNER JOIN CommissionTypes ON Commissions.CommissionTypesID = CommissionTypes.ID WHERE (GETDATE() > Commissions.DateAvailable) AND (Commissions.UsersID = " + userId + ") AND (Commissions.Active = 1) AND (DATEPART(year, Commissions.DateAvailable) = " + Year + ") AND (DATEPART(month, Commissions.DateAvailable) = " + Month + ") GROUP BY CommissionTypes.CommissionName ORDER BY CommissionTypes.CommissionName DESC";
            var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt2 != null && dt2.Rows.Count > 0)
            {
                foreach (DataRow item in dt2.Rows)
                {
                    string total = Convert.ToString(item[0]);
                    string TheYear = Convert.ToString(item[1]);
                    decimal.TryParse(total, out decimal Amount);

                    models.Add(new CommissionEarnedDescriptionModel { total = Amount, TheYear = TheYear, Type = "Comm", TheMonthInt = 0 });
                }
            }
            return models;
        }
        public List<CommissionDetailByMonthYearModel> GetCommissionDetailsByMonthYear(int userId, int Year, int Month)
        {
            List<CommissionDetailByMonthYearModel> m = new List<CommissionDetailByMonthYearModel>();
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@UsersID",
                Value = userId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@theYear",
                Value = Year
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@theMonth",
                Value = Month
            });

            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("Get_CommissionDetailsByMonthYear", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    decimal.TryParse(Convert.ToString(item["SumTotal"]), out decimal sum);
                    int.TryParse(Convert.ToString(item["OrderID"]), out int _orderId); ;
                    string _DateAvailable = Convert.ToString(item["DateAvailable"]);
                    string _Description = Convert.ToString(item["Description"]);
                    string commissionType = Convert.ToString(item["CommissionType"]);

                    m.Add(new CommissionDetailByMonthYearModel
                    {
                        SumTotal = sum,
                        OrderID = _orderId,
                        DateAvailable = _DateAvailable,
                        Description = _Description,
                        CommissionType = commissionType
                    });
                }
            }
            return m;
        }
        public List<CommissionDetailByMonthYearModel> GetPendingCommissions(int userId)
        {
            List<CommissionDetailByMonthYearModel> m = new List<CommissionDetailByMonthYearModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@UsersID",
                Value = userId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("Get_CommissionsPending", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    decimal.TryParse(Convert.ToString(item["SumTotal"]), out decimal sum);
                    int.TryParse(Convert.ToString(item["OrderID"]), out int _orderId); ;
                    string _DateAvailable = Convert.ToString(item["DateAvailable"]);
                    string _Description = Convert.ToString(item["Description"]);
                    string _CreateDate = Convert.ToString(item["CreateDate"]);

                    m.Add(new CommissionDetailByMonthYearModel
                    {
                        SumTotal = sum,
                        OrderID = _orderId,
                        DateAvailable = _DateAvailable,
                        Description = _Description,
                        CreateDate = _CreateDate
                    });
                }
            }
            return m;
        }
        public List<UplineContactDetailsViewModel> GetUplineUsers(int userId)
        {
            List<UplineContactDetailsViewModel> model = new List<UplineContactDetailsViewModel>();
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userId",
                    Value = userId
                });
                DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("SP_GetUplineUsersByUserId", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {

                        string Name = Convert.ToString(item["Name"]);
                        string Email = Convert.ToString(item["Email"]);
                        string Phone = Convert.ToString(item["Phone"]);
                        string rankname = Convert.ToString(item["rankname"]);

                        model.Add(new UplineContactDetailsViewModel
                        {
                            Email = Email,
                            Name = Name,
                            Phone = Phone,
                            Rank = rankname
                        });


                    }
                }
            }
            catch (Exception ex) { }


            return model;
        }
        public string GetCurrentRankByUserId(int userId)
        {
            string RankName = string.Empty;
            List<UplineContactDetailsViewModel> model = new List<UplineContactDetailsViewModel>();
            try
            {
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userId",
                    Value = userId
                });
                DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("SP_GetCurrentRankByUserID", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RankName = Convert.ToString(dt.Rows[0]["rankname"]);
                }
            }
            catch (Exception ex) { }


            return RankName;
        }
        public List<CommissionPaidModel> GetPaidCommissions(int userId)
        {
            List<CommissionPaidModel> m = new List<CommissionPaidModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@UsersID",
                Value = userId
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_CommissionsPaid", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    decimal.TryParse(Convert.ToString(item["Amount"]), out decimal sum);
                    string _paidDate = Convert.ToString(item["PaidDate"]);
                    string _Description = Convert.ToString(item["Description"]);
                    string _CreateDate = Convert.ToString(item["CreateDate"]);
                    Boolean.TryParse(Convert.ToString(item["Paid"]), out bool isPaid);

                    m.Add(new CommissionPaidModel
                    {
                        Amount = sum,
                        PaidDate = _paidDate,
                        Description = _Description,
                        CreateDate = _CreateDate,
                        Paid = isPaid
                    });
                }
            }
            return m;
        }
        public int CheckUserSMSOptOut(int UsersID)
        {
            int count = 0;
            try
            {
                string sql = "select count(*) from OptOut_SMS where UsersID= " + UsersID + "";
                var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    string val = Convert.ToString(dt2.Rows[0][0]);
                    int.TryParse(val, out count);
                }
            }
            catch (Exception ex)
            {

            }

            return count;
        }
        public int CheckUserSMSOptOutGeneralCount(int UsersID)
        {
            int count = 0;
            try
            {
                string sql = "select count(*) from OptOut_SMS where UsersID= " + UsersID + " And OptOut_CatsID=0";
                var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    string val = Convert.ToString(dt2.Rows[0][0]);
                    int.TryParse(val, out count);
                }
            }
            catch (Exception ex)
            {

            }

            return count;
        }
        public int CheckUserEmailOptOut(int UsersID)
        {
            int count = 0;
            try
            {
                string sql = "select count(*) from OptOut_Emails where UsersID= " + UsersID + "";
                var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    string val = Convert.ToString(dt2.Rows[0][0]);
                    int.TryParse(val, out count);
                }
            }
            catch (Exception ex)
            {

            }

            return count;
        }

        public int CheckUserEmailOptOutGeneralCount(int UsersID)
        {
            int count = 0;
            try
            {
                string sql = "select count(*) from OptOut_Emails where UsersID= " + UsersID + " And OptOut_CatsID=0";
                var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    string val = Convert.ToString(dt2.Rows[0][0]);
                    int.TryParse(val, out count);
                }
            }
            catch (Exception ex)
            {

            }

            return count;
        }
        public (bool, bool) CheckSMSOptIn(int UsersID)
        {

            bool smsOptIn = false;
            bool mobileVerified = false;
            try
            {
                string sql = "select SMSOptIn,MobileVerified from Users where ID = (" + UsersID + ")";
                var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    string val = Convert.ToString(dt2.Rows[0][0]);
                    bool.TryParse(val, out smsOptIn);
                    string val2 = Convert.ToString(dt2.Rows[0][1]);
                    bool.TryParse(val2, out mobileVerified);
                }
            }
            catch (Exception ex)
            {

            }

            return (smsOptIn, mobileVerified);
        }
        public bool CheckEmailOptIn(int UsersID)
        {

            bool emailOptIn = false;

            try
            {
                string sql = "select EmailOptIn from Users where ID = (" + UsersID + ")";
                var dt2 = _dbContext.ExecuteSqlDataTable(sql, null);
                if (dt2 != null && dt2.Rows.Count > 0)
                {
                    string val = Convert.ToString(dt2.Rows[0][0]);
                    bool.TryParse(val, out emailOptIn);

                }
            }
            catch (Exception ex)
            {

            }

            return emailOptIn;
        }

        public bool KillOptOut(int UserId, string EmailOrSMS)
        {
            string TableName = EmailOrSMS == "email" ? "OptOut_Emails" : "OptOut_SMS";
            string sql = "Delete from " + TableName + " where UsersID=" + UserId + "";
            var data = _dbContext.ExecuteInsertQuery(sql, null);
            return true;
        }
        public bool ToggleEmailSmSOptOutIn(int UserId, string EmailOrSMS, int status)
        {
            string colName = EmailOrSMS == "email" ? "EmailOptIn" : "SMSOptIn";
            string sql = "update users set " + colName + " = " + status + " where id =" + UserId;
            var data = _dbContext.ExecuteInsertQuery(sql, null);
            return true;
        }
        public bool OptOutFromEmail(int UserId, string EmailOrSMS)
        {
            KillOptOut(UserId, EmailOrSMS);
            string sql = "Select ID from OptOut_Cats";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null)
            {
                foreach (DataRow item in dt.Rows)
                {
                    int.TryParse(Convert.ToString(item["ID"]), out int id);
                    OptUserOut(UserId, EmailOrSMS, id);
                }
            }
            return true;
        }
        public bool OptUserOut(int UsersID, string EmailOrSMS, int OptOutCatID)
        {
            string TableName = EmailOrSMS == "email" ? "OptOut_Emails" : "OptOut_SMS";
            string sql = "INSERT INTO " + TableName + " (UsersID,OptOut_CatsID) VALUES(" + UsersID + "," + OptOutCatID + ")";
            var data = _dbContext.ExecuteInsertQuery(sql);
            return true;
        }
        public bool OptOutofNonSystem(string Phone)
        {

            string sql = "Delete from SMS_EventRecipients where (RecipientPhone='" + Phone + "')";
            string sql2 = "update users set SMSOptIn =0 where SMSPhone='" + Phone + "'";
            var data = _dbContext.ExecuteInsertQuery(sql);
            var data2 = _dbContext.ExecuteInsertQuery(sql2);
            return true;
        }
    }
}
