﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Foxxlegacy.Services.User
{
    public interface IUserService
    {

        UserDetailsViewModel GetUserDetails(int ActionId, string UserName, long UserId);
        LoggedinUserDetails GetLoginUserDetails(int ActionId, UserLoginRequestViewModel userLogin);

        UserDetailsViewModel GetUserDetailsbyUsersID(int ActionId, UserViewModel model);
        UserInfoListViewModel GetUserInfo(int ActionId, UserInfoViewModel model);

        List<PromotionRanksViewModel> GetAllPromotionRanks(int ActionId, long RankID);

        Boolean IsUserInRole(int ActionId, string userName, string roleName);
        int GetNopUserId(int ActionId, int UserId, string Email);

        List<UserRoleListViewModel> GetAllRoles(int ActionId, string userName);

        SqlResponseBaseModel AddRemoveUserRole(int ActionId, string UserId, string RoleId);

        List<PersonalsListViewModel> GetPersonalsList(int ActionId, string userName);

        List<MessagesListViewModel> GetMessagesList(int ActionId, long UserId);

        List<HistoryofContactsListViewModel> GetHistoryofContactsList(int ActionId, long ContactID);

        List<DialogueList> GetHistoryofContactsDialogueList(int ActionId, long ContactID);
        SqlResponseBaseModel InsertUpdateUsersSignUpData(long ActionId, UserViewModel model);
        SetSmsCodeViewModel SetSmsCode(int ActionId, long UserId, string UserName, string SmsCode, int? val = null);
        SetEmailViewModel GetEmailData(int ActionId, int EmailId);
        SetEmailViewModel GetEmailDataByTaskName(int ActionId, string TaskName);
        DataTable SetUSUFLEROr(long userId);
        int GetSponsorsUsersID(long userId);
        int InsertToEmailQue(string FromAddress, string ToAddress, string Subject, string Body, bool IsSent);
        SqlResponseBaseModel SetTempOptInCode(int ActionId, PhoneVerificationViewModel model);
        bool UpdatePlivoSMSStatus(long PlivoQueueID, string UUID);
        bool SMSOptIN(int UserID);

        List<UserPersonalInformationViewModel> GetUserDetailsList(long ActionId, BroadcastEmailUserRequest model);
        List<TargetUserNameViewModel> GetTargetUsersforPlacement(int UserId, string TargetUserName, int MoveUserId);
        List<TargetUserNameViewModel> GetTargetUsersforOrderPlacement(string TargetUserName);
        List<UserRankViewModel> GetUserRankList(long ActionId);

        UserQualifiedRankDataViewModel GetUserRankQualifiedDataDetails(int UserId);
        UserRankEmailOptDataViewModel GetUserRankEmailOptDataDetails(int UserId);
        UserRankSmsOptDataViewModel GetUserRankSmsOptDataDetails(int UserId);
        UserRankOptDataViewModel GetUserRankOptDataDetails(int UserId);
        PlaceUserCountViewModel GetPlacementUserCount(int UserId, int SponsorUserId);
        PlaceUserCountViewModel GetAdminPlacementUserCount(int UserId, int SponsorUserId);
        int UpdateUserPlacement(UpdateUserPlacementRequestViewModel model);
        bool DeleteOrderForPlacement(int OrderId, int TargetUserId);
        List<TargetUserNameViewModel> FindAllusersInUplineDownline(int UserId, int SearchType);
        List<UserOrdersViewModel> GetAllOrdersByUserId(int UserId);
        void DownRankUplineReCheck(int DonatingSponsorId);
        bool DeleteDownlineCount(int ActionId, int UserId, int OrderId);
        
        int ActiveCustomerMetaData(int Actionid, int UserId, int OrderId, int UniLevelSponsorID, bool PersonalCustomer, int insertNextUnilevelSponsorID);
        string UserNameFromSmsCode(string SmsCode);
        void New404Log(string LostUser, string Description, string REMOTE_ADDR);
        bool HasW9(int UserId);
        CommissionEarnedMode GetTotalEarned(int UserId);
        List<CommissionEarnedDescriptionModel> GetCommissionByYear(int userId, int Year);
        List<CommissionEarnedDescriptionModel> GetCommissionByMonth(int userId, int Year, int Month);
        List<CommissionDetailByMonthYearModel> GetCommissionDetailsByMonthYear(int userId, int Year, int Month);
        List<CommissionDetailByMonthYearModel> GetPendingCommissions(int userId);
        List<UplineContactDetailsViewModel> GetUplineUsers(int userId);
        List<CommissionPaidModel> GetPaidCommissions(int userId);
        string GetCurrentRankByUserId(int userId);
        string GetPassword(int ActionId, long UserId);
        int CheckUserEmailOptOut(int UsersID);
        int CheckUserEmailOptOutGeneralCount(int UsersID);
        int CheckUserSMSOptOut(int UsersID);
        int CheckUserSMSOptOutGeneralCount(int UsersID);
        (bool,bool) CheckSMSOptIn(int UsersID);
        bool CheckEmailOptIn(int UsersID);
        bool OptOutFromEmail(int UserId, string EmailOrSMS);
        bool KillOptOut(int UserId, string EmailOrSMS);
        bool OptUserOut(int UsersID, string EmailOrSMS, int OptOutCatID);
        bool OptOutofNonSystem(string Phone);
        bool ToggleEmailSmSOptOutIn(int UserId, string EmailOrSMS, int status);
        void AddSponsorInfoToCsj(int userId, string SponsorName);
    }
}
