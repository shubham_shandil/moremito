﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.Home
{
    public class HomeService:IHomeService
    {
        private readonly IDbContext _dbContext;

        public HomeService()
        {
        }

        #region CTor
        public HomeService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

       
        #endregion


        public List<FAQCategoryListViewModel> GetFAQCategoryList(int ActionId, FAQViewModel model)
        {
            List<FAQCategoryListViewModel> objmodelist = new List<FAQCategoryListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ID";
                _ID.Value = model.Id;

               



                objmodelist = _dbContext.ExecuteStoredProcedure<FAQCategoryListViewModel>("exec SP_FAQ @ActionId,@ID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FAQListViewModel> GetFAQList(int ActionId, FAQViewModel model)
        {
            List<FAQListViewModel> objmodelist = new List<FAQListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ID";
                _ID.Value = model.Id;





                objmodelist = _dbContext.ExecuteStoredProcedure<FAQListViewModel>("exec SP_FAQ @ActionId,@ID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public EventsListViewModel GetWebContent(int ActionId, EventsViewModel model)
        {
            EventsListViewModel objmodelist = new EventsListViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _WebContentName = new SqlParameter();
                _WebContentName.Direction = System.Data.ParameterDirection.Input;
                _WebContentName.DbType = System.Data.DbType.String;
                _WebContentName.ParameterName = "@WebContentName";
                _WebContentName.Value = model.WebContentName;





                objmodelist = _dbContext.ExecuteStoredProcedure<EventsListViewModel>("exec SP_GetWebContent @ActionId,@WebContentName", _ActionId, _WebContentName).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel ContactUs(int ActionId, ContactViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ID";
                _ID.Value = model.ID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;

               

                SqlParameter _Origin = new SqlParameter();
                _Origin.Direction = System.Data.ParameterDirection.Input;
                _Origin.DbType = System.Data.DbType.String;
                _Origin.ParameterName = "@Origin";
                _Origin.Value = model.Origin ?? (object)DBNull.Value;

                SqlParameter _Name = new SqlParameter();
                _Name.Direction = System.Data.ParameterDirection.Input;
                _Name.DbType = System.Data.DbType.String;
                _Name.ParameterName = "@Name";
                _Name.Value = model.Name ?? (object)DBNull.Value;

                SqlParameter _EmailId = new SqlParameter();
                _EmailId.Direction = System.Data.ParameterDirection.Input;
                _EmailId.DbType = System.Data.DbType.String;
                _EmailId.ParameterName = "@Email";
                _EmailId.Value = model.EmailId ?? (object)DBNull.Value;

                SqlParameter _Phone = new SqlParameter();
                _Phone.Direction = System.Data.ParameterDirection.Input;
                _Phone.DbType = System.Data.DbType.String;
                _Phone.ParameterName = "@Phone";
                _Phone.Value = model.Phone ?? (object)DBNull.Value;


                SqlParameter _Subject = new SqlParameter();
                _Subject.Direction = System.Data.ParameterDirection.Input;
                _Subject.DbType = System.Data.DbType.String;
                _Subject.ParameterName = "@Subject";
                _Subject.Value = model.Subject ?? (object)DBNull.Value;

                SqlParameter _Message = new SqlParameter();
                _Message.Direction = System.Data.ParameterDirection.Input;
                _Message.DbType = System.Data.DbType.String;
                _Message.ParameterName = "@Details";
                _Message.Value = model.Message ?? (object)DBNull.Value;

             

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_ContactUs @ActionId,@ID,@UsersID,@Origin,@Name,@Email,@Phone,@Subject,@Details", _ActionId, _ID, _UsersID, _Origin, _Name, _EmailId, _Phone, _Subject, _Message).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public List<MyPersonalReferralOrdersViewModel> GetMyPersonalReferralOrdersList(int ActionId, long UsersID)
        {
            List<MyPersonalReferralOrdersViewModel> objmodelist = new List<MyPersonalReferralOrdersViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@UsersID";
                _ID.Value = UsersID;





                objmodelist = _dbContext.ExecuteStoredProcedure<MyPersonalReferralOrdersViewModel>("exec SP_RMyPersonalReferralOrders @ActionId,@UsersID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<MyPassUpOrdersViewModel> GetMyPassUpOrdersList(int ActionId, long UsersID)
        {
            List<MyPassUpOrdersViewModel> objmodelist = new List<MyPassUpOrdersViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@UsersID";
                _ID.Value = UsersID;



                objmodelist = _dbContext.ExecuteStoredProcedure<MyPassUpOrdersViewModel>("exec SP_MyPassUpOrders @ActionId,@UsersID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
    }
}
