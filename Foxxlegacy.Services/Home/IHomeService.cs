﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.Home
{
    public interface IHomeService
    {

        List<FAQCategoryListViewModel> GetFAQCategoryList(int ActionId, FAQViewModel model);
        List<FAQListViewModel> GetFAQList(int ActionId, FAQViewModel model);

        EventsListViewModel GetWebContent(int ActionId, EventsViewModel model);

        SqlResponseBaseModel ContactUs(int ActionId, ContactViewModel model);


        List<MyPersonalReferralOrdersViewModel> GetMyPersonalReferralOrdersList(int ActionId, long UsersID);

        List<MyPassUpOrdersViewModel> GetMyPassUpOrdersList(int ActionId, long UsersID);
    }
}
