﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.Admin
{
    public interface IAdminService
    {
        List<MemberInfo> GetMemberInformation(int ActionId, string UserName, string SearchBy, string OrderNo);
        SqlResponseBaseModel ChangeUserName(int ActionId, long UserId, string UserName, string SearchBy, string OrderNo, string NewUserName);
        List<HelpViewModel> GetWebContentKeyName(int ActionId);

        HelpViewModel GetWebContentById(int ActionId, string KeyName, int Id);
        SqlResponseBaseModel UpdateWebContentById(int ActionId, string KeyName, int Id, string ContentValue);

        SqlResponseBaseModel GrandFathersInsertUpdate(int ActionId, GrandFathersViewModel model);

        SqlResponseBaseModel UpdateUserRank(int ActionId, GrandFathersViewModel model);

        List<GrandFathersListViewModel> GetGrandFathersList(int ActionId, GrandFathersViewModel model);
        List<DeeplinkDetails> GetDeeplinkDetailsList(int ActionId, AdminEditorViewModel model);
        SqlResponseBaseModel InsertUpdateDeeplinkDetails(int ActionId, AdminEditorViewModel model);
        DeeplinkDetails GetDeeplinkDetails(int ActionId, AdminEditorViewModel model);
        List<EmailTemplateDetails> GetEmailTaskNameList(int ActionId, int EmailId);
        SqlResponseBaseModel InsertUpdateEmailTemplateDetails(int ActionId, EmailTemplateDetails model);

        long GetSponsorsUsersID(int ActionId, long SelectedUsersID, bool PlacementMode);

        UplineContactDetailsViewModel GetWelcomeTagLiterals(int ActionId, string Username, long UsersID, long SelectedUserID);
        MainList GetMainTree(int ActionId, long Id, long Placement);
        MainList GetReferralNetworkTree(int ActionId, long Id);
        List<SubMainList> GetSubMainTree(int ActionId, long Id, long Placement);
        List<ChildMainList> GetChildTree(int ActionId, long Id);

        CSJDetailsViewModel GetCSJDetails(int ActionId, long UserId);

        SqlResponseBaseModel UpdateCSJ(int ActionId, CSJDetailsViewModel model);

        string GetCSJJournalHistoricalRecords(int ActionId, long UserId);

        List<PlacementTooListlViewModel> GetPlacementToolList(int ActionId, string UserName, long UserId, string SearchBy, string Value);
        List<CatsDetails> GetCatsDetailsList(int ActionId, int Id);
        SqlResponseBaseModel InsertUpdateCatsDetails(int ActionId, CatsDetails model);

        SqlResponseBaseModel InsertUpdatePlacementTool(int ActionId, PlacementToolViewModel model);

        List<ResourcesItems> GetResourcesItemsList(int ActionId, int Id);

        SqlResponseBaseModel ChangeSponsor(int ActionId, long UserID, long NewSponsor);
        SqlResponseBaseModel InsertResourcesItems(int ActionId, ResourcesItems model);
        List<Faqs> GetFaqsItemsList(int ActionId, int Id);
        List<FAQCategories> GetFAQCategoriesList(int ActionId);
        SqlResponseBaseModel InsertFaqItems(int ActionId, Faqs model);

        List<OrderCallListViewModel> GetOrderCallList(int ActionId);

        SqlResponseBaseModel UpdateOrderCallList(int ActionId, long OrderID);


        SqlResponseBaseModel ClaimOrder(int ActionId, long CsrUserID, long OrderID);

        List<PinCertificateListViewModel> GetPinCertificateList(int ActionId);

        List<SmsEvent> GetSmsEventList(int ActionId, int Id);
        List<SmsEventMessage> GetSmsEventMsgList(int ActionId, int Id);


        List<CallListViewModel> GetCallList(int ActionId);



        long CallToMakeCount(int ActionId);

        SqlResponseBaseModel UpdateCalled(int ActionId, long ID);

        SqlResponseBaseModel ClaimThisCall(int ActionId, long ID, long UserID);
        SqlResponseBaseModel InsertUpdateSmsEvent(int ActionId, SmsEvent model);
        SqlResponseBaseModel InsertUpdateSmsEventMessage(int ActionId, SmsEventMessage model);

        List<string> GetLinkErrors(int ActionId);

        SqlResponseBaseModel ClearLog(int ActionId);

        SqlResponseBaseModel CleanupPlivo(int ActionId);

        long GetMysteryNumber(int ActionId);

        List<BrandsListViewModel> GetBrandList(int ActionId);

        SqlResponseBaseModel CreateUpdateDeleteBrand(int ActionId, BrandsViewModel model);

        BrandsViewModel GetBrandListById(int ActionId, long Id);


        SqlResponseBaseModel CreateUpdateDeleteProduct(int ActionId, ProductsViewModel model);

        List<ProductsListViewModel> GetProductsList(int ActionId, long MBrandID);

        ProductsViewModel GetProductListById(int ActionId, long MBrandID);

        ProductsViewModel GetProductSingleListById(int ActionId, long Id);
        List<SiteListViewModel> GetSiteList(int ActionId, long ProductId);
        SqlResponseBaseModel CreateUpdateDeleteSite(int ActionId, SiteViewModel model);

        SiteViewModel GetSiteListById(int ActionId, long Id);


        SqlResponseBaseModel CreateUpdateDeleteMethods(int ActionId, MethodsViewModel model);


        List<MethodsListViewModel> GetMethodsList(int ActionId);

        MethodsViewModel GetMethodsListById(int ActionId, long Id);

        SqlResponseBaseModel CreateUpdateDeleteResources(int ActionId, MarketingResourcesViewModel model);

        List<MarketingResourcesListViewModel> GetResourcesList(int ActionId, long SitesID);

        MarketingResourcesViewModel GetResourcesListById(int ActionId, long Id);

        Boolean IsProductInBrand(int ActionId, long Id, long BrandId);


        SqlResponseBaseModel CreateUpdateDeleteResourcesQrCode(int ActionId, QRDataForMarketingResource model);


        List<QRDataListForMarketingResource> GetResourcesQrCodeList(int ActionId, long SourcesID);
        QRDataForMarketingResource GetResourcesQrCodeListById(int ActionId, long Id);


        SqlResponseBaseModel CreateUpdateDeleteMarketingResourcesVideoPages(int ActionId, MarketingResourceVideoPages model);

        List<MarketingResourceListVideoPages> GetProductVideoPagesList(int ActionId, string Category);

        MarketingResourceVideoPages GetProductVideoPagesListById(int ActionId, long Id);

        SqlResponseBaseModel DeleteUserUtility(int ActionId, DeleteUserUtility model);

        SqlResponseBaseModel DeleteIncompleteUsers(int ActionId, IncompleteUsers model);

        SqlResponseBaseModel KillConnections(int ActionId, long SpID);

        List<EmailExport> GetEmailExportList(int ActionId);

        List<EmailExportNoOrder> GetEmailExportNoOrderList(int ActionId);

        List<CustomerEmailsbyOrderCondition> GetCustomerEmailsbyOrderCondition(int ActionId, long ReportType, string UserName);


        SqlResponseBaseModel VIEImportExcel(int ActionId, DataTable dt);


        List<VIEProcessCommissionViewModel> GetCustomerIdList(int ActionId);


        SqlResponseBaseModel CreateOrder(int ActionId, VIEProcessCommissionViewModel model);

        List<ItemModel> GetItemDetails(int ActionId, long ItemID);

        bool IsOrderOvcp(int ActionId, long Id);

        long GetNextOrderNo(int ActionId);

        List<ItemByState> GetItemByStateList(int ActionId);

        List<TotalsbyState> GetItemByStateList2(int ActionId);

        List<ItemByDate> GetItemByDateList(int ActionId, AdminReportViewModel model);

        List<MapsViewModel> GetMapsList(int ActionId, AdminReportViewModel model);

        List<MaxLevelUserCounts> GetUsercountsbymaxlevelList(int ActionId, AdminReportViewModel model);


        List<SweepReport> GetSweepReportList(int ActionId);


        SqlResponseBaseModel PaidDeleteSweepReport(int ActionId, long Id);


        List<AutoShipCounts> GetAutoShipCountsList(int ActionId);


        List<OutOfStockItem> GetOutOfStockItemList(int ActionId);

        List<OutOfStockItemUserDetils> GetOutOfStockItemUserDetails(int ActionId,long ItemsID);

        List<OutOfStockItemOrderDetails> GetOutOfStockItemOrderDetails(int ActionId, long AutoshipDirectivesID);

        List<Contest> GetContestList(int ActionId);

        SqlResponseBaseModel InsertAdminBroadcastEmail(int ActionId, DataTable table);

        List<AdminScheduleBroadcastViewModel> GetAdminScheduleBroadcastList(long ActionId);

        List<SmsTempleteInfo> GetSmsTempletes(long ActionId, int Id);

        SqlResponseBaseModel InsertAdminBroadcastSms(int ActionId, DataTable table);

        long BroadcastSMSUserCount(int ActionId, string SmsText,string ToNumber);

        long BroadcastSMSmessagesCount(int ActionId, string SearchText);


        List<GetBroadcastSMSList> GetBroadcastSMSGridList(long ActionId, string SearchText);

        SqlResponseBaseModel DeleteAdminBroadcastSms(int ActionId, long Id,string ScheduleTime);

        GetBroadcastSMSList GetBroadcastSMSList(long ActionId, long Id);


        SqlResponseBaseModel SendBroadcastSMS(int ActionId, GetBroadcastSMSList model);

        long BroadcastEmailmessagesCount(int ActionId, string SearchText);

        List<GetBroadcastEmailList> GetBroadcastEmailGridList(long ActionId, string SearchText);

        SqlResponseBaseModel DeleteAdminBroadcastEmail(int ActionId, long Id, string ScheduleTime);


        GetBroadcastEmailList GetBroadcastEmailList(long ActionId, long Id);

        SqlResponseBaseModel SendBroadcastEmail(int ActionId, GetBroadcastEmailList model);



        bool IsUser(long ActionId, string UserName);

        SetUSUFLEROrViewModel SetUSUFLEROr(long ActionId, long UsersID);

        SqlResponseBaseModel NewPasswordChangeRequestsRecord(long ActionId, PasswordChangeRequestsRecordViewModel model);


        BroadcastReportsViewModel GetBroadcastReportsPendingEmailSmsData(long ActionId);

        SqlResponseBaseModel ClearBroadcastReportsEmailSms(long ActionId);


        List<SentEmailReport> SentEmailReportList(long ActionId);

        List<SentEmailReport> BroadcastReportsEmailSmsReportList(long ActionId,string qtype);
        List<TestModel> GetUplineUsers();
        List<CSJDetailsViewModel> GetNewCsj(int ActionId, long UserId);
        List<NotFoundModel> GetNotFoundLogs();
        bool ClearNotFoundLogs();
        int ChangePassword(ChangePasswordModel model);
        int ResetPassword(ResetPassword model, int ActionId);
    }
}
