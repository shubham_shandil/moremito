﻿using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.Admin
{
    public class AdminService : IAdminService
    {

        private readonly IDbContext _dbContext;

        #region CTor
        public AdminService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        #endregion

        public List<MemberInfo> GetMemberInformation(int ActionId, string UserName, string SearchBy, string OrderNo)
        {
            List<MemberInfo> model = new List<MemberInfo>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = 0;

                SqlParameter _SearchBy = new SqlParameter();
                _SearchBy.Direction = System.Data.ParameterDirection.Input;
                _SearchBy.DbType = System.Data.DbType.String;
                _SearchBy.ParameterName = "@SearchBy";
                _SearchBy.Value = SearchBy ?? (Object)DBNull.Value;

                SqlParameter _OrderNo = new SqlParameter();
                _OrderNo.Direction = System.Data.ParameterDirection.Input;
                _OrderNo.DbType = System.Data.DbType.String;
                _OrderNo.ParameterName = "@OrderNo";
                _OrderNo.Value = OrderNo ?? (Object)DBNull.Value;

                model = _dbContext.ExecuteStoredProcedure<MemberInfo>("exec SP_Get_UserDetails @ActionId,@UserName,@UserId,@SearchBy,@OrderNo", _ActionId, _UserName, _UserId, _SearchBy, _OrderNo).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public SqlResponseBaseModel ChangeUserName(int ActionId, long UserId, string UserName, string SearchBy, string OrderNo, string NewUserName)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                SqlParameter _SearchBy = new SqlParameter();
                _SearchBy.Direction = System.Data.ParameterDirection.Input;
                _SearchBy.DbType = System.Data.DbType.String;
                _SearchBy.ParameterName = "@SearchBy";
                _SearchBy.Value = SearchBy ?? (Object)DBNull.Value;

                SqlParameter _OrderNo = new SqlParameter();
                _OrderNo.Direction = System.Data.ParameterDirection.Input;
                _OrderNo.DbType = System.Data.DbType.String;
                _OrderNo.ParameterName = "@OrderNo";
                _OrderNo.Value = OrderNo ?? (Object)DBNull.Value;

                SqlParameter _NewUserName = new SqlParameter();
                _NewUserName.Direction = System.Data.ParameterDirection.Input;
                _NewUserName.DbType = System.Data.DbType.String;
                _NewUserName.ParameterName = "@NewUserName";
                _NewUserName.Value = NewUserName ?? (Object)DBNull.Value;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Get_UserDetails @ActionId,@UserName,@UserId,@SearchBy,@OrderNo,@NewUserName", _ActionId, _UserName, _UserId, _SearchBy, _OrderNo, _NewUserName).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public List<HelpViewModel> GetWebContentKeyName(int ActionId)
        {
            List<HelpViewModel> objmodelist = new List<HelpViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _KeyName = new SqlParameter();
                _KeyName.Direction = System.Data.ParameterDirection.Input;
                _KeyName.DbType = System.Data.DbType.String;
                _KeyName.ParameterName = "@KeyName";
                _KeyName.Value = null ?? string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<HelpViewModel>("exec SP_Get_WebContent @ActionId,@KeyName", _ActionId, _KeyName).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<EmailTemplateDetails> GetEmailTaskNameList(int ActionId, int EmailId)
        {
            List<EmailTemplateDetails> objmodelist = new List<EmailTemplateDetails>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _EmailId = new SqlParameter();
                _EmailId.Direction = System.Data.ParameterDirection.Input;
                _EmailId.DbType = System.Data.DbType.Int32;
                _EmailId.ParameterName = "@EmailId";
                _EmailId.Value = EmailId;

                objmodelist = _dbContext.ExecuteStoredProcedure<EmailTemplateDetails>("exec SP_EmailTemplate @ActionId,@EmailId", _ActionId, _EmailId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }


        public HelpViewModel GetWebContentById(int ActionId, string KeyName, int Id)
        {
            HelpViewModel objmodelist = new HelpViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _KeyName = new SqlParameter();
                _KeyName.Direction = System.Data.ParameterDirection.Input;
                _KeyName.DbType = System.Data.DbType.String;
                _KeyName.ParameterName = "@KeyName";
                _KeyName.Value = KeyName ?? string.Empty;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                objmodelist = _dbContext.ExecuteStoredProcedure<HelpViewModel>("exec SP_Get_WebContent @ActionId,@KeyName,@Id", _ActionId, _KeyName, _Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel UpdateWebContentById(int ActionId, string KeyName, int Id, string ContentValue)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _KeyName = new SqlParameter();
                _KeyName.Direction = System.Data.ParameterDirection.Input;
                _KeyName.DbType = System.Data.DbType.String;
                _KeyName.ParameterName = "@KeyName";
                _KeyName.Value = KeyName ?? string.Empty;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                SqlParameter _ContentValue = new SqlParameter();
                _ContentValue.Direction = System.Data.ParameterDirection.Input;
                _ContentValue.DbType = System.Data.DbType.String;
                _ContentValue.ParameterName = "@ContentValue";
                _ContentValue.Value = ContentValue ?? string.Empty;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Get_WebContent @ActionId,@KeyName,@Id,@ContentValue", _ActionId, _KeyName, _Id, _ContentValue).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public SqlResponseBaseModel GrandFathersInsertUpdate(int ActionId, GrandFathersViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UsersID";
                _UserId.Value = model.UsersID;


                SqlParameter _RankID = new SqlParameter();
                _RankID.Direction = System.Data.ParameterDirection.Input;
                _RankID.DbType = System.Data.DbType.Int64;
                _RankID.ParameterName = "@RankID";
                _RankID.Value = model.RankID;

                SqlParameter _GFRankID = new SqlParameter();
                _GFRankID.Direction = System.Data.ParameterDirection.Input;
                _GFRankID.DbType = System.Data.DbType.Int64;
                _GFRankID.ParameterName = "@GFRankID";
                _GFRankID.Value = model.GFRankID;

                SqlParameter _ExpireDate = new SqlParameter();
                _ExpireDate.Direction = System.Data.ParameterDirection.Input;
                _ExpireDate.DbType = System.Data.DbType.String;
                _ExpireDate.ParameterName = "@ExpireDate";
                _ExpireDate.Value = model.ExpireDate ?? (Object)DBNull.Value;

                SqlParameter _Reason = new SqlParameter();
                _Reason.Direction = System.Data.ParameterDirection.Input;
                _Reason.DbType = System.Data.DbType.String;
                _Reason.ParameterName = "@Reason";
                _Reason.Value = model.Reason ?? (Object)DBNull.Value;


                SqlParameter _PreviousRank = new SqlParameter();
                _PreviousRank.Direction = System.Data.ParameterDirection.Input;
                _PreviousRank.DbType = System.Data.DbType.String;
                _PreviousRank.ParameterName = "@PreviousRank";
                _PreviousRank.Value = model.PreviousRank ?? (Object)DBNull.Value;

                SqlParameter _Comments = new SqlParameter();
                _Comments.Direction = System.Data.ParameterDirection.Input;
                _Comments.DbType = System.Data.DbType.String;
                _Comments.ParameterName = "@Comments";
                _Comments.Value = model.Comments ?? (Object)DBNull.Value;


                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = model.UserName ?? (Object)DBNull.Value;


                SqlParameter _GrandFatheringID = new SqlParameter();
                _GrandFatheringID.Direction = System.Data.ParameterDirection.Input;
                _GrandFatheringID.DbType = System.Data.DbType.Int64;
                _GrandFatheringID.ParameterName = "@GrandFatheringID";
                _GrandFatheringID.Value = model.GrandFatheringID;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_GrandFathers @ActionId,@UsersID,@RankID,@GFRankID,@ExpireDate,@Reason,@PreviousRank,@Comments,@UserName,@GrandFatheringID", _ActionId, _UserId, _RankID, _GFRankID, _ExpireDate, _Reason, _PreviousRank, _Comments, _UserName, _GrandFatheringID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public SqlResponseBaseModel UpdateUserRank(int ActionId, GrandFathersViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UsersID";
                _UserId.Value = model.UsersID;


                SqlParameter _RankID = new SqlParameter();
                _RankID.Direction = System.Data.ParameterDirection.Input;
                _RankID.DbType = System.Data.DbType.Int64;
                _RankID.ParameterName = "@RankID";
                _RankID.Value = model.RankID;



                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_GrandFathers @ActionId,@UsersID,@RankID", _ActionId, _UserId, _RankID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public List<GrandFathersListViewModel> GetGrandFathersList(int ActionId, GrandFathersViewModel model)
        {
            List<GrandFathersListViewModel> modelList = new List<GrandFathersListViewModel>();
            try
            {
                SqlParameter[] parameter = new SqlParameter[2];

                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                parameter[0] = _ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UsersID";
                _UserId.Value = model.UsersID;



                parameter[1] = _UserId;


             //   modelList = _dbContext.ExecuteStoredProcedure<GrandFathersListViewModel>("exec SP_GrandFathers @ActionId,@UsersID", _ActionId, _UserId).ToList();

                var dt = _dbContext.ExecuteStoredProcedureDataTable("SP_GrandFathers", parameter);
                if (dt != null)
                {
                    modelList = dt.ConvertDataTableToList<GrandFathersListViewModel>();
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public List<DeeplinkDetails> GetDeeplinkDetailsList(int ActionId, AdminEditorViewModel model)
        {
            List<DeeplinkDetails> objmodelist = new List<DeeplinkDetails>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _DeeplinkId = new SqlParameter();
                _DeeplinkId.Direction = System.Data.ParameterDirection.Input;
                _DeeplinkId.DbType = System.Data.DbType.Int32;
                _DeeplinkId.ParameterName = "@DeeplinkId";
                _DeeplinkId.Value = model.DeeplinkId;

                objmodelist = _dbContext.ExecuteStoredProcedure<DeeplinkDetails>("exec SP_DeeplinkDetails @ActionId,@DeeplinkId", _ActionId, _DeeplinkId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertUpdateDeeplinkDetails(int ActionId, AdminEditorViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _DeeplinkId = new SqlParameter();
                _DeeplinkId.Direction = System.Data.ParameterDirection.Input;
                _DeeplinkId.DbType = System.Data.DbType.Int32;
                _DeeplinkId.ParameterName = "@DeeplinkId";
                _DeeplinkId.Value = model.DeeplinkId;



                SqlParameter _PageName = new SqlParameter();
                _PageName.Direction = System.Data.ParameterDirection.Input;
                _PageName.DbType = System.Data.DbType.String;
                _PageName.ParameterName = "@PageName";
                _PageName.Value = model.PageName ?? string.Empty;

                SqlParameter _Url = new SqlParameter();
                _Url.Direction = System.Data.ParameterDirection.Input;
                _Url.DbType = System.Data.DbType.String;
                _Url.ParameterName = "@PageUrl";
                _Url.Value = model.Url ?? string.Empty;

                SqlParameter _Order = new SqlParameter();
                _Order.Direction = System.Data.ParameterDirection.Input;
                _Order.DbType = System.Data.DbType.Int32;
                _Order.ParameterName = "@Order";
                _Order.Value = model.Order;

                SqlParameter _SelectForQuickLinks = new SqlParameter();
                _SelectForQuickLinks.Direction = System.Data.ParameterDirection.Input;
                _SelectForQuickLinks.DbType = System.Data.DbType.Boolean;
                _SelectForQuickLinks.ParameterName = "@SelectForQuickLinks";
                _SelectForQuickLinks.Value = model.SelectForQuickLinks;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_DeeplinkDetails @ActionId,@DeeplinkId,@PageName,@PageUrl,@Order,@SelectForQuickLinks",
                    _ActionId, _DeeplinkId, _PageName, _Url, _Order, _SelectForQuickLinks).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public DeeplinkDetails GetDeeplinkDetails(int ActionId, AdminEditorViewModel model)
        {
            DeeplinkDetails objmodelist = new DeeplinkDetails();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _DeeplinkId = new SqlParameter();
                _DeeplinkId.Direction = System.Data.ParameterDirection.Input;
                _DeeplinkId.DbType = System.Data.DbType.Int32;
                _DeeplinkId.ParameterName = "@DeeplinkId";
                _DeeplinkId.Value = model.DeeplinkId;

                objmodelist = _dbContext.ExecuteStoredProcedure<DeeplinkDetails>("exec SP_DeeplinkDetails @ActionId,@DeeplinkId", _ActionId, _DeeplinkId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertUpdateEmailTemplateDetails(int ActionId, EmailTemplateDetails model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _EmailId = new SqlParameter();
                _EmailId.Direction = System.Data.ParameterDirection.Input;
                _EmailId.DbType = System.Data.DbType.Int32;
                _EmailId.ParameterName = "@EmailId";
                _EmailId.Value = model.EmailId;

                SqlParameter _Task = new SqlParameter();
                _Task.Direction = System.Data.ParameterDirection.Input;
                _Task.DbType = System.Data.DbType.String;
                _Task.ParameterName = "@Task";
                _Task.Value = model.Task ?? string.Empty;

                SqlParameter _Subject = new SqlParameter();
                _Subject.Direction = System.Data.ParameterDirection.Input;
                _Subject.DbType = System.Data.DbType.String;
                _Subject.ParameterName = "@Subject";
                _Subject.Value = model.Subject ?? string.Empty;

                SqlParameter _Body = new SqlParameter();
                _Body.Direction = System.Data.ParameterDirection.Input;
                _Body.DbType = System.Data.DbType.String;
                _Body.ParameterName = "@Body";
                _Body.Value = model.Body ?? string.Empty;

                SqlParameter _SmsBody = new SqlParameter();
                _SmsBody.Direction = System.Data.ParameterDirection.Input;
                _SmsBody.DbType = System.Data.DbType.String;
                _SmsBody.ParameterName = "@SmsBody";
                _SmsBody.Value = model.SmsBody ?? string.Empty;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_EmailTemplate @ActionId,@EmailId,@Task,@Subject,@Body,@SmsBody",
                    _ActionId, _EmailId, _Task, _Subject, _Body, _SmsBody).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }


        public long GetSponsorsUsersID(int ActionId, long SelectedUsersID, bool PlacementMode)
        {
            long GetSponsorsUsersID = 0;

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _SelectedUsersID = new SqlParameter();
                _SelectedUsersID.Direction = System.Data.ParameterDirection.Input;
                _SelectedUsersID.DbType = System.Data.DbType.Int64;
                _SelectedUsersID.ParameterName = "@UsersID";
                _SelectedUsersID.Value = SelectedUsersID;

                GetSponsorsUsersID = _dbContext.ExecuteStoredProcedure<long>("exec SP_GetSponsors @ActionId,@UsersID", _ActionId, _SelectedUsersID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetSponsorsUsersID;

        }

        public List<TestModel> GetUplineUsers()
        {
            List<TestModel> data = new List<TestModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@id";
                _ActionId.Value = 1;

                data = new List<TestModel>();
                data = _dbContext.ExecuteStoredProcedure<TestModel>("exec GetUsersForDownlineScript @id", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return data;
        }

        public UplineContactDetailsViewModel GetWelcomeTagLiterals(int ActionId, string Username, long UsersID, long SelectedUserID)
        {
            UplineContactDetailsViewModel modelList = new UplineContactDetailsViewModel();

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Username = new SqlParameter();
                _Username.Direction = System.Data.ParameterDirection.Input;
                _Username.DbType = System.Data.DbType.String;
                _Username.ParameterName = "@Username";
                _Username.Value = Username;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;


                SqlParameter _SelectedUsersID = new SqlParameter();
                _SelectedUsersID.Direction = System.Data.ParameterDirection.Input;
                _SelectedUsersID.DbType = System.Data.DbType.Int64;
                _SelectedUsersID.ParameterName = "@SelectedUserID";
                _SelectedUsersID.Value = SelectedUserID;

                modelList = _dbContext.ExecuteStoredProcedure<UplineContactDetailsViewModel>("exec SP_GetWelcomeTagLiterals @ActionId,@Username,@UsersID,@SelectedUserID", _ActionId, _Username, _UsersID, _SelectedUsersID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }


            return modelList;
        }


        public MainList GetMainTree(int ActionId, long Id, long Placement)
        {
            MainList model = new MainList();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = Id;

                SqlParameter _Placement = new SqlParameter();
                _Placement.Direction = System.Data.ParameterDirection.Input;
                _Placement.DbType = System.Data.DbType.Int64;
                _Placement.ParameterName = "@Placement";
                _Placement.Value = Placement;

                model = _dbContext.ExecuteStoredProcedure<MainList>("exec sp_TreeView @ACTION,@ID,@Placement", _ActionId, _InfoId, _Placement).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public MainList GetReferralNetworkTree(int ActionId, long Id)
        {
            var model = new MainList();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = Id;

                /* exec dbo.sp_get_referral_network_users @ActionId=1, @UserId=6 */
                model = _dbContext.ExecuteStoredProcedure<MainList>("exec sp_get_referral_network_users @ActionId,@UserId", _ActionId, _UserId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return model;
        }
        public List<SubMainList> GetSubMainTree(int ActionId, long Id, long Placement)
        {
            List<SubMainList> model = new List<SubMainList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = Id;

                SqlParameter _Placement = new SqlParameter();
                _Placement.Direction = System.Data.ParameterDirection.Input;
                _Placement.DbType = System.Data.DbType.Int64;
                _Placement.ParameterName = "@Placement";
                _Placement.Value = Placement;
                model = _dbContext.ExecuteStoredProcedure<SubMainList>("exec sp_TreeView @ACTION,@ID,@Placement", _ActionId, _InfoId, _Placement).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<ChildMainList> GetChildTree(int ActionId, long Id)
        {
            List<ChildMainList> model = new List<ChildMainList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = Id;



                model = _dbContext.ExecuteStoredProcedure<ChildMainList>("exec sp_TreeView @ACTION,@ID", _ActionId, _InfoId).ToList();
                if (model == null)
                {
                    model = new List<ChildMainList>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public CSJDetailsViewModel GetCSJDetails(int ActionId, long UserId)
        {
            CSJDetailsViewModel model = new CSJDetailsViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserId",
                    Value = UserId
                });


                model = _dbContext.ExecuteStoredProcedure<CSJDetailsViewModel>("exec SP_CSJ @ActionId," +
                    "@UserId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel UpdateCSJ(int ActionId, CSJDetailsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserId",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Comments",
                    Value = model.Comments ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@adminId",
                    Value = model.AdminId
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_CSJ @ActionId," +
                    "@UserId,@Comments,@adminId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public string GetCSJJournalHistoricalRecords(int ActionId, long UserId)
        {
            string JournalHistoricalRecords = string.Empty;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserId",
                    Value = UserId
                });


                JournalHistoricalRecords = _dbContext.ExecuteStoredProcedure<string>("exec SP_CSJ @ActionId," +
                    "@UserId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return JournalHistoricalRecords;
        }

        public List<PlacementTooListlViewModel> GetPlacementToolList(int ActionId, string UserName, long UserId, string SearchBy, string Value)
        {
            List<PlacementTooListlViewModel> modelList = new List<PlacementTooListlViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                SqlParameter _SearchBy = new SqlParameter();
                _SearchBy.Direction = System.Data.ParameterDirection.Input;
                _SearchBy.DbType = System.Data.DbType.String;
                _SearchBy.ParameterName = "@SearchBy";
                _SearchBy.Value = SearchBy ?? (Object)DBNull.Value;

                SqlParameter _Value = new SqlParameter();
                _Value.Direction = System.Data.ParameterDirection.Input;
                _Value.DbType = System.Data.DbType.String;
                _Value.ParameterName = "@Value";
                _Value.Value = Value ?? (Object)DBNull.Value;


                modelList = _dbContext.ExecuteStoredProcedure<PlacementTooListlViewModel>("exec SP_PlacementTool @ActionId,@UserName,@UserId,@SearchBy,@Value", _ActionId, _UserName, _UserId, _SearchBy, _Value).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return modelList;
        }

        public List<CatsDetails> GetCatsDetailsList(int ActionId, int Id)
        {
            List<CatsDetails> objmodelist = new List<CatsDetails>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                objmodelist = _dbContext.ExecuteStoredProcedure<CatsDetails>("exec SP_CatsDetails @ActionId,@Id", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertUpdateCatsDetails(int ActionId, CatsDetails model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@CatOrder",
                    Value = model.CatOrder
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_CatsDetails @ActionId," +
                    "@Id,@Name,@CatOrder", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public SqlResponseBaseModel InsertUpdatePlacementTool(int ActionId, PlacementToolViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = "";

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = model.UserId;

                SqlParameter _SearchBy = new SqlParameter();
                _SearchBy.Direction = System.Data.ParameterDirection.Input;
                _SearchBy.DbType = System.Data.DbType.String;
                _SearchBy.ParameterName = "@SearchBy";
                _SearchBy.Value = "";

                SqlParameter _Value = new SqlParameter();
                _Value.Direction = System.Data.ParameterDirection.Input;
                _Value.DbType = System.Data.DbType.String;
                _Value.ParameterName = "@Value";
                _Value.Value = "";
                SqlParameter _PlacementUserID = new SqlParameter();
                _PlacementUserID.Direction = System.Data.ParameterDirection.Input;
                _PlacementUserID.DbType = System.Data.DbType.Int64;
                _PlacementUserID.ParameterName = "@PlacementUserID";
                _PlacementUserID.Value = model.PlacementUserID;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_PlacementTool @ActionId,@UserName,@UserId,@SearchBy,@Value,@PlacementUserID", _ActionId, _UserName, _UserId, _SearchBy, _Value, _PlacementUserID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public List<ResourcesItems> GetResourcesItemsList(int ActionId, int Id)
        {
            List<ResourcesItems> objmodelist = new List<ResourcesItems>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                objmodelist = _dbContext.ExecuteStoredProcedure<ResourcesItems>("exec SP_ResourcesItemsDetails @ActionId,@Id", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel ChangeSponsor(int ActionId, long UserID, long NewSponsor)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UserID;

                SqlParameter _PlacementMode = new SqlParameter();
                _PlacementMode.Direction = System.Data.ParameterDirection.Input;
                _PlacementMode.DbType = System.Data.DbType.Boolean;
                _PlacementMode.ParameterName = "PlacementMode";
                _PlacementMode.Value = 0;

                SqlParameter _SponsorUsersID = new SqlParameter();
                _SponsorUsersID.Direction = System.Data.ParameterDirection.Input;
                _SponsorUsersID.DbType = System.Data.DbType.Int64;
                _SponsorUsersID.ParameterName = "@SponsorUsersID";
                _SponsorUsersID.Value = NewSponsor;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_GetSponsors @ActionId,@UsersID,@PlacementMode,@SponsorUsersID", _ActionId, _UsersID, _PlacementMode, _SponsorUsersID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public SqlResponseBaseModel InsertResourcesItems(int ActionId, ResourcesItems model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = model.Id;

                SqlParameter _CatsId = new SqlParameter();
                _CatsId.Direction = System.Data.ParameterDirection.Input;
                _CatsId.DbType = System.Data.DbType.Int32;
                _CatsId.ParameterName = "@CatsId";
                _CatsId.Value = model.CatsId;

                SqlParameter _Name = new SqlParameter();
                _Name.Direction = System.Data.ParameterDirection.Input;
                _Name.DbType = System.Data.DbType.String;
                _Name.ParameterName = "@Name";
                _Name.Value = model.Name ?? (Object)DBNull.Value;

                SqlParameter _Description = new SqlParameter();
                _Description.Direction = System.Data.ParameterDirection.Input;
                _Description.DbType = System.Data.DbType.String;
                _Description.ParameterName = "@Description";
                _Description.Value = model.Description ?? (Object)DBNull.Value;

                SqlParameter _FilePath = new SqlParameter();
                _FilePath.Direction = System.Data.ParameterDirection.Input;
                _FilePath.DbType = System.Data.DbType.String;
                _FilePath.ParameterName = "@FilePath";
                _FilePath.Value = model.FilePath ?? (Object)DBNull.Value;

                SqlParameter _DisplayOrder = new SqlParameter();
                _DisplayOrder.Direction = System.Data.ParameterDirection.Input;
                _DisplayOrder.DbType = System.Data.DbType.Decimal;
                _DisplayOrder.ParameterName = "@DisplayOrder";
                _DisplayOrder.Value = model.DisplayOrder;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_ResourcesItemsDetails @ActionId,@Id,@CatsId,@Name,@Description,@FilePath,@DisplayOrder",
                    _ActionId, _Id, _CatsId, _Name, _Description, _FilePath, _DisplayOrder).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public List<Faqs> GetFaqsItemsList(int ActionId, int Id)
        {
            List<Faqs> objmodelist = new List<Faqs>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                objmodelist = _dbContext.ExecuteStoredProcedure<Faqs>("exec SP_FaqsDetails @ActionId,@Id", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FAQCategories> GetFAQCategoriesList(int ActionId)
        {
            List<FAQCategories> objmodelist = new List<FAQCategories>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = 0;

                objmodelist = _dbContext.ExecuteStoredProcedure<FAQCategories>("exec SP_FaqsDetails @ActionId,@Id", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertFaqItems(int ActionId, Faqs model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = model.Id;

                SqlParameter _CatsId = new SqlParameter();
                _CatsId.Direction = System.Data.ParameterDirection.Input;
                _CatsId.DbType = System.Data.DbType.Int32;
                _CatsId.ParameterName = "@Category";
                _CatsId.Value = model.Category;

                SqlParameter _Question = new SqlParameter();
                _Question.Direction = System.Data.ParameterDirection.Input;
                _Question.DbType = System.Data.DbType.String;
                _Question.ParameterName = "@Question";
                _Question.Value = model.Question ?? (Object)DBNull.Value;

                SqlParameter _Answer = new SqlParameter();
                _Answer.Direction = System.Data.ParameterDirection.Input;
                _Answer.DbType = System.Data.DbType.String;
                _Answer.ParameterName = "@Answer";
                _Answer.Value = model.Answer ?? (Object)DBNull.Value;



                SqlParameter _DisplayOrder = new SqlParameter();
                _DisplayOrder.Direction = System.Data.ParameterDirection.Input;
                _DisplayOrder.DbType = System.Data.DbType.Decimal;
                _DisplayOrder.ParameterName = "@DisplayOrder";
                _DisplayOrder.Value = model.DisplayOrder;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_FaqsDetails @ActionId,@Id,@Category,@Question,@Answer,@DisplayOrder",
                    _ActionId, _Id, _CatsId, _Question, _Answer, _DisplayOrder).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public List<OrderCallListViewModel> GetOrderCallList(int ActionId)
        {
            List<OrderCallListViewModel> objmodelist = new List<OrderCallListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                objmodelist = _dbContext.ExecuteStoredProcedure<OrderCallListViewModel>("exec sp_OrderCallList @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel UpdateOrderCallList(int ActionId, long OrderID)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int64;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = OrderID;


                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_OrderCallList @ActionId,@OrderID", _ActionId, _OrderID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return baseModel;
        }


        public SqlResponseBaseModel ClaimOrder(int ActionId, long CsrUserID, long OrderID)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int64;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = OrderID;

                SqlParameter _UserID = new SqlParameter();
                _UserID.Direction = System.Data.ParameterDirection.Input;
                _UserID.DbType = System.Data.DbType.Int64;
                _UserID.ParameterName = "@UserID";
                _UserID.Value = 0;


                SqlParameter _CsrUserID = new SqlParameter();
                _CsrUserID.Direction = System.Data.ParameterDirection.Input;
                _CsrUserID.DbType = System.Data.DbType.Int64;
                _CsrUserID.ParameterName = "@CsrUserID";
                _CsrUserID.Value = CsrUserID;

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_OrderCallList @ActionId,@OrderID,@UserID,@CsrUserID", _ActionId, _OrderID, _UserID, _CsrUserID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return baseModel;
        }

        public List<SmsEvent> GetSmsEventList(int ActionId, int Id)
        {
            List<SmsEvent> objmodelist = new List<SmsEvent>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                objmodelist = _dbContext.ExecuteStoredProcedure<SmsEvent>("exec SP_SmsEditorDetails @ActionId,@Id", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<SmsEventMessage> GetSmsEventMsgList(int ActionId, int Id)
        {
            List<SmsEventMessage> objmodelist = new List<SmsEventMessage>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = Id;

                objmodelist = _dbContext.ExecuteStoredProcedure<SmsEventMessage>("exec SP_SmsEditorDetails @ActionId,@Id", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }


        public List<PinCertificateListViewModel> GetPinCertificateList(int ActionId)
        {
            List<PinCertificateListViewModel> objmodelist = new List<PinCertificateListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                objmodelist = _dbContext.ExecuteStoredProcedure<PinCertificateListViewModel>("exec sp_PinList @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }


        public List<CallListViewModel> GetCallList(int ActionId)
        {
            List<CallListViewModel> objmodelist = new List<CallListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                objmodelist = _dbContext.ExecuteStoredProcedure<CallListViewModel>("exec SP_CallList @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public long CallToMakeCount(int ActionId)
        {
            long PinsToSendCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                PinsToSendCount = _dbContext.ExecuteStoredProcedure<long>("exec SP_CallList @ActionId", _ActionId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PinsToSendCount;
        }


        public SqlResponseBaseModel UpdateCalled(int ActionId, long ID)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = ID;


                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_CallList @ActionId,@ID", _ActionId, _Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return baseModel;
        }

        public SqlResponseBaseModel ClaimThisCall(int ActionId, long ID, long UserID)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = ID;

                SqlParameter _UserID = new SqlParameter();
                _UserID.Direction = System.Data.ParameterDirection.Input;
                _UserID.DbType = System.Data.DbType.Int64;
                _UserID.ParameterName = "@UserID";
                _UserID.Value = UserID;

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_CallList @ActionId,@ID,@UserID", _ActionId, _Id, _UserID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return baseModel;
        }


        public SqlResponseBaseModel InsertUpdateSmsEvent(int ActionId, SmsEvent model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = model.Id;

                SqlParameter _EventName = new SqlParameter();
                _EventName.Direction = System.Data.ParameterDirection.Input;
                _EventName.DbType = System.Data.DbType.String;
                _EventName.ParameterName = "@EventName";
                _EventName.Value = model.EventName ?? (object)DBNull.Value;


                SqlParameter _Keyword = new SqlParameter();
                _Keyword.Direction = System.Data.ParameterDirection.Input;
                _Keyword.DbType = System.Data.DbType.String;
                _Keyword.ParameterName = "@Keyword";
                _Keyword.Value = model.Keyword ?? (object)DBNull.Value;

                SqlParameter _ConfirmOptIn = new SqlParameter();
                _ConfirmOptIn.Direction = System.Data.ParameterDirection.Input;
                _ConfirmOptIn.DbType = System.Data.DbType.String;
                _ConfirmOptIn.ParameterName = "@ConfirmOptIn";
                _ConfirmOptIn.Value = model.ConfirmOptIn ?? (object)DBNull.Value;

                SqlParameter _ConfirmOptOut = new SqlParameter();
                _ConfirmOptOut.Direction = System.Data.ParameterDirection.Input;
                _ConfirmOptOut.DbType = System.Data.DbType.String;
                _ConfirmOptOut.ParameterName = "@ConfirmOptOut";
                _ConfirmOptOut.Value = model.ConfirmOptOut ?? (object)DBNull.Value;

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_SmsEditorDetails @ActionId,@Id,@EventName,@Keyword,@ConfirmOptIn,@ConfirmOptOut",
                    _ActionId, _Id, _EventName, _Keyword, _ConfirmOptIn, _ConfirmOptOut).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return baseModel;
        }


        public SqlResponseBaseModel InsertUpdateSmsEventMessage(int ActionId, SmsEventMessage model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();

            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = model.Id;

                SqlParameter _EventName = new SqlParameter();
                _EventName.Direction = System.Data.ParameterDirection.Input;
                _EventName.DbType = System.Data.DbType.String;
                _EventName.ParameterName = "@EventName";
                _EventName.Value = null ?? (object)DBNull.Value;


                SqlParameter _Keyword = new SqlParameter();
                _Keyword.Direction = System.Data.ParameterDirection.Input;
                _Keyword.DbType = System.Data.DbType.String;
                _Keyword.ParameterName = "@Keyword";
                _Keyword.Value = null ?? (object)DBNull.Value;

                SqlParameter _ConfirmOptIn = new SqlParameter();
                _ConfirmOptIn.Direction = System.Data.ParameterDirection.Input;
                _ConfirmOptIn.DbType = System.Data.DbType.String;
                _ConfirmOptIn.ParameterName = "@ConfirmOptIn";
                _ConfirmOptIn.Value = null ?? (object)DBNull.Value;

                SqlParameter _ConfirmOptOut = new SqlParameter();
                _ConfirmOptOut.Direction = System.Data.ParameterDirection.Input;
                _ConfirmOptOut.DbType = System.Data.DbType.String;
                _ConfirmOptOut.ParameterName = "@ConfirmOptOut";
                _ConfirmOptOut.Value = null ?? (object)DBNull.Value;

                SqlParameter _Msg = new SqlParameter();
                _Msg.Direction = System.Data.ParameterDirection.Input;
                _Msg.DbType = System.Data.DbType.String;
                _Msg.ParameterName = "@Msg";
                _Msg.Value = model.Msg ?? (object)DBNull.Value;

                SqlParameter _SendTime = new SqlParameter();
                _SendTime.Direction = System.Data.ParameterDirection.Input;
                _SendTime.DbType = System.Data.DbType.String;
                _SendTime.ParameterName = "@SendTime";
                _SendTime.Value = model.SendTime ?? (object)DBNull.Value;

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_SmsEditorDetails @ActionId,@Id,@EventName,@Keyword,@ConfirmOptIn,@ConfirmOptOut,@Msg,@SendTime",
                    _ActionId, _Id, _EventName, _Keyword, _ConfirmOptIn, _ConfirmOptOut, _Msg, _SendTime).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return baseModel;
        }

        public List<string> GetLinkErrors(int ActionId)
        {
            List<string> GetLinkErrors = new List<string>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                GetLinkErrors = _dbContext.ExecuteStoredProcedure<string>("exec usp_Common @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetLinkErrors;
        }

        public SqlResponseBaseModel ClearLog(int ActionId)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec usp_Common @ActionId", _ActionId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return baseModel;
        }

        public SqlResponseBaseModel CleanupPlivo(int ActionId)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec usp_Common @ActionId", _ActionId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return baseModel;
        }

        public long GetMysteryNumber(int ActionId)
        {
            long GetMysteryNumber = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                GetMysteryNumber = _dbContext.ExecuteStoredProcedure<long>("exec usp_Common @ActionId", _ActionId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GetMysteryNumber;
        }

        public List<BrandsListViewModel> GetBrandList(int ActionId)
        {
            List<BrandsListViewModel> objmodelist = new List<BrandsListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                objmodelist = _dbContext.ExecuteStoredProcedure<BrandsListViewModel>("exec SP_Brands @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel CreateUpdateDeleteBrand(int ActionId, BrandsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = model.Description ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = model.AppDisplayOrder
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = model.AppImgUrl ?? (object)DBNull.Value
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Brands @ActionId,@ID,@Name,@Description,@AppDisplayOrder," +
                    "@AppImgUrl", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public BrandsViewModel GetBrandListById(int ActionId, long Id)
        {
            BrandsViewModel model = new BrandsViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<BrandsViewModel>("exec SP_Brands @ActionId,@ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel CreateUpdateDeleteProduct(int ActionId, ProductsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = model.Description ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = model.AppDisplayOrder
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = model.AppImgUrl ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_brandid",
                    Value = model.MBrandID
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Products @ActionId,@ID,@Name,@Description,@AppDisplayOrder," +
                    "@AppImgUrl,@m_brandid", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public List<ProductsListViewModel> GetProductsList(int ActionId, long MBrandID)
        {
            List<ProductsListViewModel> objmodelist = new List<ProductsListViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_brandid",
                    Value = MBrandID
                });
                objmodelist = _dbContext.ExecuteStoredProcedure<ProductsListViewModel>("exec SP_Products @ActionId,@ID,@Name,@Description,@AppDisplayOrder," +
                    "@AppImgUrl,@m_brandid", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodelist;
        }

        public ProductsViewModel GetProductListById(int ActionId, long MBrandID)
        {
            ProductsViewModel model = new ProductsViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_brandid",
                    Value = MBrandID
                });
                model = _dbContext.ExecuteStoredProcedure<ProductsViewModel>("exec SP_Products @ActionId,@ID,@Name,@Description,@AppDisplayOrder," +
                    "@AppImgUrl,@m_brandid", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public bool IsProductInBrand(int ActionId, long Id, long BrandId)
        {
            bool IsProductInBrand = false;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_brandid",
                    Value = BrandId
                });
                IsProductInBrand = _dbContext.ExecuteStoredProcedure<bool>("exec SP_Products @ActionId,@ID,@Name,@Description,@AppDisplayOrder," +
                    "@AppImgUrl,@m_brandid", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return IsProductInBrand;
        }
        public List<SiteListViewModel> GetSiteList(int ActionId, long ProductId)
        {
            List<SiteListViewModel> objmodelist = new List<SiteListViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FQDN",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@RootDomainName",
                    Value = string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_ProductID",
                    Value = ProductId
                });
                objmodelist = _dbContext.ExecuteStoredProcedure<SiteListViewModel>("exec SP_Site @ActionId,@ID,@Name,@Description,@FQDN," +
                    "@RootDomainName,@m_ProductID", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodelist;
        }

        public SqlResponseBaseModel CreateUpdateDeleteSite(int ActionId, SiteViewModel model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = model.Description ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FQDN",
                    Value = model.FQDN ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@RootDomainName",
                    Value = model.RootDomainName ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_ProductID",
                    Value = model.ProductID
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Site @ActionId,@ID,@Name,@Description,@FQDN," +
                    "@RootDomainName,@m_ProductID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public SiteViewModel GetSiteListById(int ActionId, long Id)
        {
            SiteViewModel objmodel = new SiteViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FQDN",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@RootDomainName",
                    Value = string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_ProductID",
                    Value = 0
                });
                objmodel = _dbContext.ExecuteStoredProcedure<SiteViewModel>("exec SP_Site @ActionId,@ID,@Name,@Description,@FQDN," +
                    "@RootDomainName,@m_ProductID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodel;
        }

        public SqlResponseBaseModel CreateUpdateDeleteMethods(int ActionId, MethodsViewModel model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = model.Description ?? (object)DBNull.Value
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Methods @ActionId,@ID,@Name,@Description", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<MethodsListViewModel> GetMethodsList(int ActionId)
        {
            List<MethodsListViewModel> objmodelist = new List<MethodsListViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });


                objmodelist = _dbContext.ExecuteStoredProcedure<MethodsListViewModel>("exec SP_Methods @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodelist;
        }

        public MethodsViewModel GetMethodsListById(int ActionId, long Id)
        {
            MethodsViewModel model = new MethodsViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<MethodsViewModel>("exec SP_Methods @ActionId,@ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel CreateUpdateDeleteResources(int ActionId, MarketingResourcesViewModel model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@M_MethodsID",
                    Value = model.MethodsID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@M_SitesID",
                    Value = model.SitesID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Code",
                    Value = model.Code ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = model.Description ?? (object)DBNull.Value
                });





                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@DestinationURL",
                    Value = model.DestinationURL ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = model.AppImgUrl ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppSMSMessage",
                    Value = model.AppSMSMessage ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppEmailSubject",
                    Value = model.AppEmailSubject ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppEmailBody",
                    Value = model.AppEmailBody ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = model.AppDisplayOrder
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@PDFSource",
                    Value = model.PDFSource ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@PdfHasForm",
                    Value = model.PdfHasForm
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@VideoCode",
                    Value = model.VideoCode ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@PreviewURL",
                    Value = model.PreviewURL ?? (object)DBNull.Value
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_MarketingResources @ActionId,@ID,@M_MethodsID,@M_SitesID,@Code," +
                    "@Name,@Description,@DestinationURL,@AppImgUrl,@AppSMSMessage,@AppEmailSubject,@AppEmailBody,@AppDisplayOrder,@PDFSource,@PdfHasForm,@VideoCode,@PreviewURL", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<MarketingResourcesListViewModel> GetResourcesList(int ActionId, long SitesID)
        {
            List<MarketingResourcesListViewModel> objmodelist = new List<MarketingResourcesListViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@M_MethodsID",
                    Value = 0
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@M_SitesID",
                    Value = SitesID
                });
                objmodelist = _dbContext.ExecuteStoredProcedure<MarketingResourcesListViewModel>("exec SP_MarketingResources @ActionId,@ID,@M_MethodsID,@M_SitesID", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodelist;
        }

        public MarketingResourcesViewModel GetResourcesListById(int ActionId, long Id)
        {
            MarketingResourcesViewModel model = new MarketingResourcesViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<MarketingResourcesViewModel>("exec SP_MarketingResources @ActionId,@ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public ProductsViewModel GetProductSingleListById(int ActionId, long Id)
        {
            ProductsViewModel model = new ProductsViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@AppDisplayOrder",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AppImgUrl",
                    Value = string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@m_brandid",
                    Value = 0
                });
                model = _dbContext.ExecuteStoredProcedure<ProductsViewModel>("exec SP_Products @ActionId,@ID,@Name,@Description,@AppDisplayOrder," +
                    "@AppImgUrl,@m_brandid", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel CreateUpdateDeleteResourcesQrCode(int ActionId, QRDataForMarketingResource model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.QRDataForResourceId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@M_SourcesID",
                    Value = model.SourcesID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@URL",
                    Value = model.URL ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@PdfPage",
                    Value = model.PdfPage
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Xpx",
                    Value = model.Xpx
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Ypx",
                    Value = model.Ypx
                });



                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Color",
                    Value = model.Color ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@BackColor",
                    Value = model.BackColor ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@VersionSize",
                    Value = model.VersionSize
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Spacing",
                    Value = model.Spacing
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_MarketingResources_QrData @ActionId,@ID,@M_SourcesID,@URL,@PdfPage," +
                    "@Xpx,@Ypx,@Color,@BackColor,@VersionSize,@Spacing", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<QRDataListForMarketingResource> GetResourcesQrCodeList(int ActionId, long SourcesID)
        {
            List<QRDataListForMarketingResource> objmodelist = new List<QRDataListForMarketingResource>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@M_SourcesID",
                    Value = SourcesID
                });



                objmodelist = _dbContext.ExecuteStoredProcedure<QRDataListForMarketingResource>("exec SP_MarketingResources_QrData @ActionId,@ID,@M_SourcesID", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodelist;
        }

        public QRDataForMarketingResource GetResourcesQrCodeListById(int ActionId, long Id)
        {
            QRDataForMarketingResource model = new QRDataForMarketingResource();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<QRDataForMarketingResource>("exec SP_MarketingResources_QrData @ActionId,@ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel CreateUpdateDeleteMarketingResourcesVideoPages(int ActionId, MarketingResourceVideoPages model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Category",
                    Value = model.Category ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Description",
                    Value = model.Description ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@VideoHtml",
                    Value = model.VideoHTML ?? (object)DBNull.Value
                });



                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@BodyHtml",
                    Value = model.BodyHTML ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@InfoButtonURL",
                    Value = model.InfoButtonURL ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@BuyButtonURL",
                    Value = model.BuyButtonURL ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@Active",
                    Value = model.Active
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_MarketingResources_VideoPages @ActionId,@ID,@Category,@Name,@Description," +
                    "@VideoHtml,@BodyHtml,@InfoButtonURL,@BuyButtonURL,@Active", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<MarketingResourceListVideoPages> GetProductVideoPagesList(int ActionId, string Category)
        {
            List<MarketingResourceListVideoPages> objmodelist = new List<MarketingResourceListVideoPages>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Category",
                    Value = Category
                });



                objmodelist = _dbContext.ExecuteStoredProcedure<MarketingResourceListVideoPages>("exec SP_MarketingResources_VideoPages @ActionId,@ID,@Category", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return objmodelist;
        }

        public MarketingResourceVideoPages GetProductVideoPagesListById(int ActionId, long Id)
        {
            MarketingResourceVideoPages model = new MarketingResourceVideoPages();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<MarketingResourceVideoPages>("exec SP_MarketingResources_VideoPages @ActionId,@ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel DeleteUserUtility(int ActionId, DeleteUserUtility model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.ID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Search",
                    Value = ""
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = model.UserName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AspNetUserID",
                    Value = model.AspNetUserID ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@CustomerID",
                    Value = model.CustomerID
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_DeleteUserUtility @ActionId,@ID,@Search,@UserName,@AspNetUserID,@CustomerID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public SqlResponseBaseModel DeleteIncompleteUsers(int ActionId, IncompleteUsers model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.Id
                });


                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = model.UserName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@AspNetUserID",
                    Value = model.AspNetUserID ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@CustomerID",
                    Value = model.CustomerID
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_IncompleteUsers @ActionId,@ID,@UserName,@AspNetUserID,@CustomerID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public SqlResponseBaseModel KillConnections(int ActionId, long SpID)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@SpID",
                    Value = SpID
                });




                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_OpenDataBaseConnection @ActionId,@SpID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<EmailExport> GetEmailExportList(int ActionId)
        {
            List<EmailExport> objmodelist = new List<EmailExport>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                objmodelist = _dbContext.ExecuteStoredProcedure<EmailExport>("exec SP_Reports_Emails @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<EmailExportNoOrder> GetEmailExportNoOrderList(int ActionId)
        {
            List<EmailExportNoOrder> objmodelist = new List<EmailExportNoOrder>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;




                objmodelist = _dbContext.ExecuteStoredProcedure<EmailExportNoOrder>("exec SP_Reports_Emails @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<CustomerEmailsbyOrderCondition> GetCustomerEmailsbyOrderCondition(int ActionId, long ReportType, string UserName)
        {
            List<CustomerEmailsbyOrderCondition> objmodelist = new List<CustomerEmailsbyOrderCondition>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ReportType = new SqlParameter();
                _ReportType.Direction = System.Data.ParameterDirection.Input;
                _ReportType.DbType = System.Data.DbType.Int64;
                _ReportType.ParameterName = "@ReportType";
                _ReportType.Value = ReportType;


                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName;


                objmodelist = _dbContext.ExecuteStoredProcedure<CustomerEmailsbyOrderCondition>("exec SP_Reports_Emails @ActionId,@ReportType,@UserName", _ActionId, _ReportType, _UserName).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel VIEImportExcel(int ActionId, DataTable dt)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });



                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    SqlDbType = System.Data.SqlDbType.Structured,
                    ParameterName = "@VIEImportTable",
                    Value = dt,
                    TypeName = "dbo.TT_VIEImport"
                });



                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_VIEImportExcel @ActionId," +
                "@VIEImportTable", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public List<VIEProcessCommissionViewModel> GetCustomerIdList(int ActionId)
        {
            List<VIEProcessCommissionViewModel> modelList = new List<VIEProcessCommissionViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });


                modelList = _dbContext.ExecuteStoredProcedure<VIEProcessCommissionViewModel>("exec dbo.SP_VIEProcessCommission @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public SqlResponseBaseModel CreateOrder(int ActionId, VIEProcessCommissionViewModel model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@itemid",
                    Value = model.Itemid
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@orderno",
                    Value = model.OrderNo
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@customerid",
                    Value = model.Customerid
                });


                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@Price",
                    Value = model.Price
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@cartid",
                    Value = model.Cartid
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_VIEProcessCommission @ActionId,@ID,@itemid,@orderno,@customerid,@Price,@cartid", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<ItemModel> GetItemDetails(int ActionId, long ItemID)
        {
            List<ItemModel> modelList = new List<ItemModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@itemid",
                    Value = ItemID
                });

                modelList = _dbContext.ExecuteStoredProcedure<ItemModel>("exec dbo.SP_VIEProcessCommission @ActionId,@ID,@itemid", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public bool IsOrderOvcp(int ActionId, long Id)
        {
            bool IsOrderOvcp = false;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ID",
                    Value = Id
                });


                IsOrderOvcp = _dbContext.ExecuteStoredProcedure<bool>("exec dbo.SP_VIEProcessCommission @ActionId,@ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return IsOrderOvcp;
        }

        public long GetNextOrderNo(int ActionId)
        {
            long GetNextOrderNo = 0;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });



                GetNextOrderNo = _dbContext.ExecuteStoredProcedure<long>("exec dbo.SP_VIEProcessCommission @ActionId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return GetNextOrderNo;
        }

        public List<ItemByState> GetItemByStateList(int ActionId)
        {
            List<ItemByState> modelList = new List<ItemByState>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });


                modelList = _dbContext.ExecuteStoredProcedure<ItemByState>("exec dbo.SP_Sales @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }


        public List<TotalsbyState> GetItemByStateList2(int ActionId)
        {
            List<TotalsbyState> modelList = new List<TotalsbyState>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });


                modelList = _dbContext.ExecuteStoredProcedure<TotalsbyState>("exec dbo.SP_Sales @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<ItemByDate> GetItemByDateList(int ActionId, AdminReportViewModel model)
        {
            List<ItemByDate> modelList = new List<ItemByDate>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.DateTime,
                    ParameterName = "@MinimumDate",
                    Value = model.MinimumDate
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.DateTime,
                    ParameterName = "@MaximumDate",
                    Value = model.MaximumDate
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ProductId",
                    Value = model.ProductId
                });
                modelList = _dbContext.ExecuteStoredProcedure<ItemByDate>("exec dbo.SP_Sales @ActionId,@MinimumDate,@MaximumDate,@ProductId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<MapsViewModel> GetMapsList(int ActionId, AdminReportViewModel model)
        {
            List<MapsViewModel> modelList = new List<MapsViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.DateTime,
                    ParameterName = "@MinimumDate",
                    Value = model.MinimumDate
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.DateTime,
                    ParameterName = "@MaximumDate",
                    Value = model.MaximumDate
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = model.UsersId
                });
                modelList = _dbContext.ExecuteStoredProcedure<MapsViewModel>("exec dbo.SP_Maps @ActionId,@MinimumDate,@MaximumDate,@UsersID", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<MaxLevelUserCounts> GetUsercountsbymaxlevelList(int ActionId, AdminReportViewModel model)
        {
            List<MaxLevelUserCounts> modelList = new List<MaxLevelUserCounts>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Level",
                    Value = model.Level
                });
                modelList = _dbContext.ExecuteStoredProcedure<MaxLevelUserCounts>("exec dbo.SP_UserCounts_Max_level @ActionId,@Level", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<SweepReport> GetSweepReportList(int ActionId)
        {
            List<SweepReport> modelList = new List<SweepReport>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                modelList = _dbContext.ExecuteStoredProcedure<SweepReport>("exec dbo.SP_SweepReport @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public SqlResponseBaseModel PaidDeleteSweepReport(int ActionId, long Id)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = Id
                });


                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_SweepReport @ActionId,@Id", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return baseModel;
        }

        public List<AutoShipCounts> GetAutoShipCountsList(int ActionId)
        {
            List<AutoShipCounts> modelList = new List<AutoShipCounts>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                modelList = _dbContext.ExecuteStoredProcedure<AutoShipCounts>("exec dbo.SP_AutoShipCounts @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<OutOfStockItem> GetOutOfStockItemList(int ActionId)
        {
            List<OutOfStockItem> modelList = new List<OutOfStockItem>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                modelList = _dbContext.ExecuteStoredProcedure<OutOfStockItem>("exec dbo.SP_OutOfStockItem @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<OutOfStockItemUserDetils> GetOutOfStockItemUserDetails(int ActionId, long ItemsID)
        {
            List<OutOfStockItemUserDetils> modelList = new List<OutOfStockItemUserDetils>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ItemsID",
                    Value = ItemsID
                });
                modelList = _dbContext.ExecuteStoredProcedure<OutOfStockItemUserDetils>("exec dbo.SP_OutOfStockItem @ActionId,@ItemsID", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<OutOfStockItemOrderDetails> GetOutOfStockItemOrderDetails(int ActionId, long AutoshipDirectivesID)
        {
            List<OutOfStockItemOrderDetails> modelList = new List<OutOfStockItemOrderDetails>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ItemsID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@AutoshipDirectivesID",
                    Value = AutoshipDirectivesID
                });
                modelList = _dbContext.ExecuteStoredProcedure<OutOfStockItemOrderDetails>("exec dbo.SP_OutOfStockItem @ActionId,@ItemsID,@AutoshipDirectivesID", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public List<Contest> GetContestList(int ActionId)
        {
            List<Contest> modelList = new List<Contest>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                modelList = _dbContext.ExecuteStoredProcedure<Contest>("exec dbo.SP_Contest @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return modelList;
        }

        public SqlResponseBaseModel InsertAdminBroadcastEmail(int ActionId, DataTable table)
        {
            var response = new SqlResponseBaseModel();
            try
            {
                var Parameter = new List<object>();
                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Parameter.Add(new SqlParameter
                {
                    TypeName = "TT_BroadcastingEmailInsertData",
                    SqlDbType = SqlDbType.Structured,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@BroadcastEmailTable",
                    SqlValue = table
                });

                /* exec sp_insert_broadcast_email @Actionid, @BroadcastEmailTable */
                response = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_insert_broadcast_email @Actionid, @BroadcastEmailTable", Parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return response;
        }

        public List<AdminScheduleBroadcastViewModel> GetAdminScheduleBroadcastList(long ActionId)
        {
            var list = new List<AdminScheduleBroadcastViewModel>();
            try
            {
                var Parameter = new List<object>();
                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                /* exec sp_insert_broadcast_email @Actionid, @BroadcastEmailTable */
                list = _dbContext.ExecuteStoredProcedure<AdminScheduleBroadcastViewModel>("exec sp_schedule_broadcast_list @Actionid", Parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return list;
        }

        public List<SmsTempleteInfo> GetSmsTempletes(long ActionId, int Id)
        {
            var list = new List<SmsTempleteInfo>();
            try
            {
                var Parameter = new List<object>();
                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Id",
                    SqlValue = Id
                });

                /* exec sp_sms_template @ActionId = 1, @Id = 1 */
                list = _dbContext.ExecuteStoredProcedure<SmsTempleteInfo>("exec sp_sms_template @Actionid, @Id", Parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return list;
        }

        public SqlResponseBaseModel InsertAdminBroadcastSms(int ActionId, DataTable table)
        {
            var response = new SqlResponseBaseModel();
            try
            {
                var Parameter = new List<object>();
                Parameter.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Parameter.Add(new SqlParameter
                {
                    TypeName = "TT_BroadcastingSmsInsertData",
                    SqlDbType = SqlDbType.Structured,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@BroadcastSmsTable",
                    SqlValue = table
                });

                /* exec sp_insert_broadcast_sms @Actionid, @BroadcastEmailTable */
                response = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_insert_broadcast_sms @Actionid, @BroadcastSmsTable", Parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return response;
        }

        public long BroadcastSMSUserCount(int ActionId, string SmsText, string ToNumber)
        {
            long UserCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _SmsText = new SqlParameter();
                _SmsText.Direction = System.Data.ParameterDirection.Input;
                _SmsText.DbType = System.Data.DbType.String;
                _SmsText.ParameterName = "@SmsText";
                _SmsText.Value = SmsText ?? (Object)DBNull.Value;

                SqlParameter _ToNumber = new SqlParameter();
                _ToNumber.Direction = System.Data.ParameterDirection.Input;
                _ToNumber.DbType = System.Data.DbType.String;
                _ToNumber.ParameterName = "@ToNumber";
                _ToNumber.Value = ToNumber ?? (Object)DBNull.Value;


                UserCount = _dbContext.ExecuteStoredProcedure<long>("exec SP_BroadCastSms @ActionId,@SmsText,@ToNumber", _ActionId, _SmsText, _ToNumber).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserCount;
        }

        public long BroadcastSMSmessagesCount(int ActionId, string SearchText)
        {
            long UserCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _SmsText = new SqlParameter();
                _SmsText.Direction = System.Data.ParameterDirection.Input;
                _SmsText.DbType = System.Data.DbType.String;
                _SmsText.ParameterName = "@SmsText";
                _SmsText.Value = null ?? (Object)DBNull.Value;

                SqlParameter _ToNumber = new SqlParameter();
                _ToNumber.Direction = System.Data.ParameterDirection.Input;
                _ToNumber.DbType = System.Data.DbType.String;
                _ToNumber.ParameterName = "@ToNumber";
                _ToNumber.Value = null ?? (Object)DBNull.Value;

                SqlParameter _SearchText = new SqlParameter();
                _SearchText.Direction = System.Data.ParameterDirection.Input;
                _SearchText.DbType = System.Data.DbType.String;
                _SearchText.ParameterName = "@SearchText";
                _SearchText.Value = SearchText ?? (Object)DBNull.Value;

                UserCount = _dbContext.ExecuteStoredProcedure<long>("exec SP_BroadCastSms @ActionId,@SmsText,@ToNumber,@SearchText", _ActionId, _SmsText, _ToNumber, _SearchText).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserCount;
        }

        public List<GetBroadcastSMSList> GetBroadcastSMSGridList(long ActionId, string SearchText)
        {
            List<GetBroadcastSMSList> model = new List<GetBroadcastSMSList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _SmsText = new SqlParameter();
                _SmsText.Direction = System.Data.ParameterDirection.Input;
                _SmsText.DbType = System.Data.DbType.String;
                _SmsText.ParameterName = "@SmsText";
                _SmsText.Value = null ?? (Object)DBNull.Value;

                SqlParameter _ToNumber = new SqlParameter();
                _ToNumber.Direction = System.Data.ParameterDirection.Input;
                _ToNumber.DbType = System.Data.DbType.String;
                _ToNumber.ParameterName = "@ToNumber";
                _ToNumber.Value = null ?? (Object)DBNull.Value;

                SqlParameter _SearchText = new SqlParameter();
                _SearchText.Direction = System.Data.ParameterDirection.Input;
                _SearchText.DbType = System.Data.DbType.String;
                _SearchText.ParameterName = "@SearchText";
                _SearchText.Value = SearchText ?? (Object)DBNull.Value;

                model = _dbContext.ExecuteStoredProcedure<GetBroadcastSMSList>("exec SP_BroadCastSms @ActionId,@SmsText,@ToNumber,@SearchText", _ActionId, _SmsText, _ToNumber, _SearchText).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public SqlResponseBaseModel DeleteAdminBroadcastSms(int ActionId, long Id, string ScheduleTime)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SmsText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToNumber",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SearchText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ScheduleTime",
                    Value = ScheduleTime ?? (Object)DBNull.Value
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_BroadCastSms @ActionId,@SmsText,@ToNumber,@SearchText,@Id,@ScheduleTime", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return baseModel;
        }

        public GetBroadcastSMSList GetBroadcastSMSList(long ActionId, long Id)
        {
            GetBroadcastSMSList model = new GetBroadcastSMSList();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SmsText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToNumber",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SearchText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<GetBroadcastSMSList>("exec dbo.SP_BroadCastSms @ActionId,@SmsText,@ToNumber,@SearchText,@Id", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel SendBroadcastSMS(int ActionId, GetBroadcastSMSList model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SmsText",
                    Value = model.SMSText ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToNumber",
                    Value = model.ToNumber ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SearchText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ScheduleTime",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FromNumber",
                    Value = model.FromNumber ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserID",
                    Value = model.UserID
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_BroadCastSms @ActionId,@SmsText,@ToNumber,@SearchText,@Id,@ScheduleTime,@FromNumber,@UserID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return baseModel;
        }

        public long BroadcastEmailmessagesCount(int ActionId, string SearchText)
        {
            long UserCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _SearchText = new SqlParameter();
                _SearchText.Direction = System.Data.ParameterDirection.Input;
                _SearchText.DbType = System.Data.DbType.String;
                _SearchText.ParameterName = "@SearchText";
                _SearchText.Value = SearchText ?? (Object)DBNull.Value;

                UserCount = _dbContext.ExecuteStoredProcedure<long>("exec SP_BroadCastEmail @ActionId,@SearchText", _ActionId, _SearchText).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return UserCount;
        }

        public List<GetBroadcastEmailList> GetBroadcastEmailGridList(long ActionId, string SearchText)
        {
            List<GetBroadcastEmailList> model = new List<GetBroadcastEmailList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _SearchText = new SqlParameter();
                _SearchText.Direction = System.Data.ParameterDirection.Input;
                _SearchText.DbType = System.Data.DbType.String;
                _SearchText.ParameterName = "@SearchText";
                _SearchText.Value = SearchText ?? (Object)DBNull.Value;

                model = _dbContext.ExecuteStoredProcedure<GetBroadcastEmailList>("exec SP_BroadCastEmail @ActionId,@SearchText", _ActionId, _SearchText).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public SqlResponseBaseModel DeleteAdminBroadcastEmail(int ActionId, long Id, string ScheduleTime)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });


                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SearchText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ScheduleTime",
                    Value = ScheduleTime ?? (Object)DBNull.Value
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_BroadCastEmail @ActionId,@SearchText,@Id,@ScheduleTime", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return baseModel;
        }

        public GetBroadcastEmailList GetBroadcastEmailList(long ActionId, long Id)
        {
            GetBroadcastEmailList model = new GetBroadcastEmailList();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SearchText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = Id
                });

                model = _dbContext.ExecuteStoredProcedure<GetBroadcastEmailList>("exec dbo.SP_BroadCastEmail @ActionId,@SearchText,@Id", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }

        public SqlResponseBaseModel SendBroadcastEmail(int ActionId, GetBroadcastEmailList model)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SearchText",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ScheduleTime",
                    Value = null ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToEmail",
                    Value = model.ToEmail ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FromEmail",
                    Value = model.FromEmail ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@EmailSubject",
                    Value = model.EmailSubject ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@EmailBody",
                    Value = model.EmailBody ?? (Object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserID",
                    Value = model.UserID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@CustomerID",
                    Value = model.CustomerID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SendByAdmin",
                    Value = model.SendByAdmin ?? (Object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@GroupType",
                    Value = model.GroupType ?? (Object)DBNull.Value
                });
                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_BroadCastEmail @ActionId,@SearchText,@Id,@ScheduleTime,@ToEmail,@FromEmail,@EmailSubject,@EmailBody,@UserID,@CustomerID,@SendByAdmin,@GroupType", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return baseModel;
        }

        public bool IsUser(long ActionId, string UserName)
        {
            bool IsUser = false;
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = UserName ?? (Object)DBNull.Value
                });

                IsUser = _dbContext.ExecuteStoredProcedure<bool>("exec dbo.SP_ForgotPassword_UserName @ActionId,@UserName", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return IsUser;
        }

        public SetUSUFLEROrViewModel SetUSUFLEROr(long ActionId, long UsersID)
        {
            SetUSUFLEROrViewModel model = new SetUSUFLEROrViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = UsersID
                });

                model = _dbContext.ExecuteStoredProcedure<SetUSUFLEROrViewModel>("exec dbo.SP_SetUSUFLEROr @ActionId,@UsersID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return model;
        }

        public SqlResponseBaseModel NewPasswordChangeRequestsRecord(long ActionId, PasswordChangeRequestsRecordViewModel model)
        {
            SqlResponseBaseModel Basemodel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = model.UsersID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@RequestID",
                    Value = model.RequestID ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FromEmailAddress",
                    Value = model.FromEmailAddress ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ToEmailAddress",
                    Value = model.ToEmailAddress ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Subject",
                    Value = model.Subject ?? (object)DBNull.Value
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Body",
                    Value = model.Body ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@RecipientID",
                    Value = model.RecipientID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@sGUID",
                    Value = model.sGUID ?? (object)DBNull.Value
                });
                Basemodel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_NewPasswordChangeRequestsRecord @ActionId,@UsersID,@RequestID,@FromEmailAddress,@ToEmailAddress,@Subject,@Body,@RecipientID,@sGUID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return Basemodel;
        }



        public BroadcastReportsViewModel GetBroadcastReportsPendingEmailSmsData(long ActionId)
        {
            BroadcastReportsViewModel model = new BroadcastReportsViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                model = _dbContext.ExecuteStoredProcedure<BroadcastReportsViewModel>("exec dbo.SP_BroadcastReports @ActionId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return model;
        }

        public SqlResponseBaseModel ClearBroadcastReportsEmailSms(long ActionId)
        {
            SqlResponseBaseModel baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_BroadcastReports @ActionId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return baseModel;
        }

        public List<SentEmailReport> SentEmailReportList(long ActionId)
        {
            List<SentEmailReport> model = new List<SentEmailReport>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });


                model = _dbContext.ExecuteStoredProcedure<SentEmailReport>("exec dbo.SP_SentEmailReport @ActionId", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return model;
        }

        public List<SentEmailReport> BroadcastReportsEmailSmsReportList(long ActionId, string qtype)
        {
            List<SentEmailReport> model = new List<SentEmailReport>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@qtype",
                    Value = qtype ?? (object)DBNull.Value
                });
                model = _dbContext.ExecuteStoredProcedure<SentEmailReport>("exec dbo.SP_BroadcastReports @ActionId,@qtype", parameter.ToArray()).ToList();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }

            return model;
        }

        public List<CSJDetailsViewModel> GetNewCsj(int ActionId, long UserId)
        {
            List<CSJDetailsViewModel> model = new List<CSJDetailsViewModel>();
            try
            {
                SqlParameter[] parameters = new SqlParameter[4];
                parameters[0] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameters[1] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserID",
                    Value = UserId
                });
                parameters[2] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@Comments",
                    Value = (object)DBNull.Value
                });
                parameters[3] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@adminId",
                    Value = 0
                });


                var dt = _dbContext.ExecuteStoredProcedureDataTable("SP_CSJ", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        int.TryParse(Convert.ToString(item["ID"]), out int id);
                        string date = Convert.ToString(item["JournalEntryDate"]);
                        string admin = Convert.ToString(item["LastModAdmin"]);
                        string comment = Convert.ToString(item["Comments"]);

                        model.Add(new CSJDetailsViewModel
                        {
                            Comments = comment,
                            JournalEntryDate = date,
                            LastModAdmin = admin

                        });
                    }
                }

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return model;
        }
        public List<NotFoundModel> GetNotFoundLogs()
        {
            List<NotFoundModel> models = new List<NotFoundModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@id",
                Value = 0
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_GetNotFoundLogs", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    int.TryParse(Convert.ToString(item["ID"]), out int id);
                    string lostuser = Convert.ToString(item["lostuser"]);
                    string description = Convert.ToString(item["description"]);
                    string EventDate = Convert.ToString(item["EventDate"]);
                    string ipaddress = Convert.ToString(item["ipaddress"]);

                    models.Add(new NotFoundModel
                    {
                        LostUser = lostuser,
                        Description = description,
                        EventDate = EventDate,
                        IpAddress = ipaddress

                    });
                }
            }

            return models;
        }
        public int ResetPassword(ResetPassword model, int ActionId)
        {
            int status = 0;
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@id",
                Value = model.Id ?? (Object)DBNull.Value
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@newPassword",
                Value = model.NewPassword
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("ResetPassword", parameters);
            if(dt !=null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out status);
            } 
            return status;
        }
        public bool ClearNotFoundLogs()
        {

            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@id",
                Value = 0
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_ClearNotFoundLogs", parameters);


            return true;
        }
        public int ChangePassword(ChangePasswordModel model)
        {
            int status = 0;
            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@username",
                Value = model.UserName
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@newPassword",
                Value = model.NewPassword
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@modifyBy",
                Value = model.ModifiedBy
            });

            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("ChangeUserPassword", parameters);
            if (dt != null)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out status);
            }
            return status;
        }
    }
}
