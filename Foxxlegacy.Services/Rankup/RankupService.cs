﻿using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.Rankup
{
    public class RankupService : IRankupService
    {

        private readonly IDbContext _dbContext;

        public RankupService()
        {
        }

        #region CTor
        public RankupService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        #endregion
        
        public int GetRankByUser(int ActionId, int UserId)
        {
            int RankId = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;



                RankId = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId,@UserId", _ActionId, _UserId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RankId;
        }

        public int InsertUsersQualifyingOrders(int ActionId, int UserId, int OrderID, int RankID,string RankType =null)
        {
            int RankId = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int32;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = OrderID;

                SqlParameter _RankID = new SqlParameter();
                _RankID.Direction = System.Data.ParameterDirection.Input;
                _RankID.DbType = System.Data.DbType.Int32;
                _RankID.ParameterName = "@RankID";
                _RankID.Value = RankID;

                SqlParameter _RankType = new SqlParameter();
                _RankType.Direction = System.Data.ParameterDirection.Input;
                _RankType.DbType = System.Data.DbType.String;
                _RankType.ParameterName = "@RankType";
                _RankType.Value = RankType==null? (object)DBNull.Value : RankType;


                RankId = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId,@UserId,@OrderID,@RankID,@RankType", _ActionId, _UserId, _OrderID, _RankID, _RankType).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RankId;
        }

        public RanksDataViewModel GetRanksData(int ActionId, int RankId)
        {
            RanksDataViewModel model = new RanksDataViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = 0;

                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int32;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = 0;

                SqlParameter _RankID = new SqlParameter();
                _RankID.Direction = System.Data.ParameterDirection.Input;
                _RankID.DbType = System.Data.DbType.Int32;
                _RankID.ParameterName = "@RankID";
                _RankID.Value = RankId;


                model = _dbContext.ExecuteStoredProcedure<RanksDataViewModel>("exec SP_Rankup @ActionId,@UserId,@OrderID,@RankID", _ActionId, _UserId, _OrderID, _RankID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public int InsertUsersQTERank(int ActionId, int UserId, int CurrLevel, DateTime QTE_ExpDate, DateTime QTE_FailDate, DateTime FailureMessagedDate, DateTime QTE_DemotionDate)
        {
            int CompareCountLegs = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                SqlParameter _CurrLevel = new SqlParameter();
                _CurrLevel.Direction = System.Data.ParameterDirection.Input;
                _CurrLevel.DbType = System.Data.DbType.Int32;
                _CurrLevel.ParameterName = "@CurrLevel";
                _CurrLevel.Value = CurrLevel;

                SqlParameter _QTE_ExpDate = new SqlParameter();
                _QTE_ExpDate.Direction = System.Data.ParameterDirection.Input;
                _QTE_ExpDate.DbType = System.Data.DbType.DateTime;
                _QTE_ExpDate.ParameterName = "@QTE_ExpDate";
                _QTE_ExpDate.Value = QTE_ExpDate;

                SqlParameter _QTE_FailDate = new SqlParameter();
                _QTE_FailDate.Direction = System.Data.ParameterDirection.Input;
                _QTE_FailDate.DbType = System.Data.DbType.DateTime;
                _QTE_FailDate.ParameterName = "@QTE_FailDate";
                _QTE_FailDate.Value = QTE_FailDate;

                SqlParameter _FailureMessagedDate = new SqlParameter();
                _FailureMessagedDate.Direction = System.Data.ParameterDirection.Input;
                _FailureMessagedDate.DbType = System.Data.DbType.DateTime;
                _FailureMessagedDate.ParameterName = "@FailureMessagedDate";
                _FailureMessagedDate.Value = FailureMessagedDate;

                SqlParameter _QTE_DemotionDate = new SqlParameter();
                _QTE_DemotionDate.Direction = System.Data.ParameterDirection.Input;
                _QTE_DemotionDate.DbType = System.Data.DbType.DateTime;
                _QTE_DemotionDate.ParameterName = "@QTE_DemotionDate";
                _QTE_DemotionDate.Value = QTE_DemotionDate;


                CompareCountLegs = _dbContext.ExecuteStoredProcedure<int>("exec SP_UsersQTE_atRank_Rankup @ActionId,@UserId,@CurrLevel,@QTE_ExpDate,@QTE_FailDate,@FailureMessagedDate,@QTE_DemotionDate",
                    _ActionId, _UserId, _CurrLevel, _QTE_ExpDate, _QTE_FailDate, _FailureMessagedDate, _QTE_DemotionDate).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return CompareCountLegs;
        }

        public List<CheckStepFiveToElevenResponseViewModel> CheckStepFiveToEleven(int ActionId, CheckStepFiveToElevenRequestViewModel model)
        {
            var list = new List<CheckStepFiveToElevenResponseViewModel>();
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@PerLegCappedCustomerCount",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.PerLegCappedCustomerCount
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@LegsWithRank3Rank4",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.LegsWithRank3Rank4
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CheckRankLegTotal",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.CheckRankLegTotal
                });

                /* exec sp_rankup_execution @Actionid = 1,@UserId = '',@PerLegCappedCustomerCount = '',@LegsWithRank3Rank4 = '',@CheckRankLegTotal = '' */
                list = _dbContext.ExecuteStoredProcedure<CheckStepFiveToElevenResponseViewModel>("sp_rankup_execution @Actionid,@UserId,@PerLegCappedCustomerCount,@LegsWithRank3Rank4,@CheckRankLegTotal", parameter.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return list;
        }

        public int CappedCustomerCount(int ActionId, CappedCustomerCountRequestViewModel model)
        {
            int Count = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@PerLegCappedCustomerCount",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.PerLegCappedCustomerCount
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@LegsWithRank3Rank4",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 0
                });

                /* exec sp_rankup_execution @Actionid = 1,@UserId = '',@PerLegCappedCustomerCount = '',@LegsWithRank3Rank4 = '',@CheckRankLegTotal = '' */
                Count = _dbContext.ExecuteStoredProcedure<int>("sp_rankup_execution @Actionid,@UserId,@PerLegCappedCustomerCount,@LegsWithRank3Rank4,@CheckRankLegTotal", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Count;
        }
        public int TWORank3UsersInLegCount(int ActionId, TWORank3UsersInLegCountRequestViewModel model)
        {
            int Count = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Value = ActionId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@PerLegCappedCustomerCount",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 0
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@LegsWithRank3Rank4",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.LegsWithRank3Rank4
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CheckRankLegTotal",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.CheckRankLegTotal
                });

                /* exec sp_rankup_execution @Actionid = 1,@UserId = '',@PerLegCappedCustomerCount = '',@LegsWithRank3Rank4 = '',@CheckRankLegTotal = '' */
                Count = _dbContext.ExecuteStoredProcedure<int>("sp_rankup_execution @Actionid,@UserId,@PerLegCappedCustomerCount,@LegsWithRank3Rank4,@CheckRankLegTotal", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Count;
        }

        public int GetGoldRankupSponsorId(GoldRankupSponsorIdReuestViewModel model)
        {
            int SponsorId = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 1
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CheckRankId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.CheckRankId
                });

                /* exec sp_user_gold_rankup_execution @ActionId = 1, @UserId = 1, @CheckRankId = 0 */
                SponsorId = _dbContext.ExecuteStoredProcedure<int>("exec sp_user_gold_rankup_execution @ActionId, @UserId, @CheckRankId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return SponsorId;
        }
        public int UpdateGoldUserRankup(UpdateGoldRankUserReuestViewModel model)
        {
            int UpdateUserCodeResponse = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 1
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.UserId
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CheckRankId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 0
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@GoldLevel",
                    SqlDbType = System.Data.SqlDbType.Char,
                    Value = model.GoldLevel
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@SponsorID",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.SponsorID
                });

                /* exec sp_user_gold_rankup_execution @ActionId = 2, @UserId = 1, @CheckRankId = 0, @GoldLevel='', @SponsorID = 0 */
                UpdateUserCodeResponse = _dbContext.ExecuteStoredProcedure<int>("exec sp_user_gold_rankup_execution @ActionId, @UserId, @CheckRankId,@GoldLevel,@SponsorID", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return UpdateUserCodeResponse;
        }
        public int GetOrderUserId(OrderUserIdRequestViewModel model)
        {
            int UserId = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = 1
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@OrderId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = model.OrderId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                UserId = _dbContext.ExecuteStoredProcedure<int>("exec sp_get_order_user @ActionId, @OrderId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return UserId;
        }

        public int IsOrderOwnerRC_PR(int Actionid,int UserId)
        {
            int Response = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                Response = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Response;
        }

        public int IsThisRepQualified(int Actionid, int UserId)
        {
            int Response = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                Response = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Response;
        }
        public int IsThisUserQualified(int Actionid, int UserId,int OwnerId)
        {
            int Response = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderOwnerId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = OwnerId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                Response = _dbContext.ExecuteStoredProcedure<int>("exec UserQual @ActionId, @UserId,@orderOwnerId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Response;
        }

        public int FindTheNextUpLineRC_PR_Above(int Actionid, int UserId)
        {
            int Response = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                Response = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Response;
        }

        public int CurrentlyQualified_ByThisOrderOwner(int Actionid, int UserId)
        {
            int Response = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                Response = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Response;
        }

        public int FindTheNextupLineQualifiedRC_PR_Above(int Actionid, int UserId)
        {
            int Response = 0;
            try
            {
                var parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = Actionid
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlDbType = System.Data.SqlDbType.Int,
                    Value = UserId
                });


                /* exec sp_get_order_user @ActionId = 1, @OrderId = 1 */
                Response = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup @ActionId, @UserId", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return Response;
        }

        public int RankUpUplineUsers(int ActionId, int UserId, int OrderID)
        {
            int RankId = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@actionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@userID";
                _UserId.Value = UserId;

                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int32;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = OrderID;              


                RankId = _dbContext.ExecuteStoredProcedure<int>("exec SP_Rankup_UplineUsers @actionId,@userID,@OrderID", _ActionId, _UserId, _OrderID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RankId;
           
        }
        public int RankUpZeroToOneUsers(int ActionId, int UserId, int OrderID,int RankId)
        {
            
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@actionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int32;
                _UserId.ParameterName = "@userID";
                _UserId.Value = UserId;

                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int32;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = OrderID;

                SqlParameter _RankId = new SqlParameter();
                _RankId.Direction = System.Data.ParameterDirection.Input;
                _RankId.DbType = System.Data.DbType.Int32;
                _RankId.ParameterName = "@RankId";
                _RankId.Value = RankId;


                var res = _dbContext.ExecuteStoredProcedure<int>("exec RankAdvanceZeroToOne @actionId,@userID,@OrderID,@RankId", _ActionId, _UserId, _OrderID,_RankId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RankId;

        }
    }
}
