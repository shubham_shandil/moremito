﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.Rankup
{
   public interface IRankupService
    {

        int GetRankByUser(int ActionId, int UserId);
        int InsertUsersQualifyingOrders(int ActionId, int UserId, int OrderID, int RankID,string RankType=null);
        RanksDataViewModel GetRanksData(int ActionId, int RankId);
        int InsertUsersQTERank(int ActionId, int UserId, int CurrLevel, DateTime QTE_ExpDate, DateTime QTE_FailDate, DateTime FailureMessagedDate, DateTime QTE_DemotionDate);
        List<CheckStepFiveToElevenResponseViewModel> CheckStepFiveToEleven(int ActionId, CheckStepFiveToElevenRequestViewModel model);
        int CappedCustomerCount(int ActionId, CappedCustomerCountRequestViewModel model);
        int TWORank3UsersInLegCount(int ActionId, TWORank3UsersInLegCountRequestViewModel model);
        int GetGoldRankupSponsorId(GoldRankupSponsorIdReuestViewModel model);
        int UpdateGoldUserRankup(UpdateGoldRankUserReuestViewModel model);
        int GetOrderUserId(OrderUserIdRequestViewModel model);
        int IsOrderOwnerRC_PR(int Actionid, int UserId);
        int IsThisRepQualified(int Actionid, int UserId);
        int IsThisUserQualified(int Actionid, int UserId, int OwnerId);
        int FindTheNextUpLineRC_PR_Above(int Actionid, int UserId);
        int CurrentlyQualified_ByThisOrderOwner(int Actionid, int UserId);
        int FindTheNextupLineQualifiedRC_PR_Above(int Actionid, int UserId);
        int RankUpUplineUsers(int ActionId, int UserId, int OrderID);
        int RankUpZeroToOneUsers(int ActionId, int UserId, int OrderID, int RankId);

    }
}
