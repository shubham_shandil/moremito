﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.FoxxCash
{

    public class FoxxCashService : IFoxxCashService
    {
        private readonly IDbContext _dbContext;

        public FoxxCashService()
        {
        }

        #region CTor
        public FoxxCashService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }


        #endregion

        public List<MyFoxxCashListViewModel> GetTotals(int ActionId, long UsersID)
        {
            List<MyFoxxCashListViewModel> objmodelist = new List<MyFoxxCashListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;






                objmodelist = _dbContext.ExecuteStoredProcedure<MyFoxxCashListViewModel>("exec SP_FoxxCash @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public List<SweeperHistoryListViewModel> GetSweeperHistory(int ActionId, long UsersID)
        {
            List<SweeperHistoryListViewModel> objmodelist = new List<SweeperHistoryListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;






                objmodelist = _dbContext.ExecuteStoredProcedure<SweeperHistoryListViewModel>("exec SP_SweeperHistory @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<TransferFCHistoryListViewModel> GetTransferFCHistory(int ActionId, long UsersID)
        {
            List<TransferFCHistoryListViewModel> objmodelist = new List<TransferFCHistoryListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;


                objmodelist = _dbContext.ExecuteStoredProcedure<TransferFCHistoryListViewModel>("exec SP_TransferFCHistory @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashReceivedListViewModel> GetTotalFoxxCashReceived(int ActionId, long UsersID)
        {
            List<FoxxCashReceivedListViewModel> objmodelist = new List<FoxxCashReceivedListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;


                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashReceivedListViewModel>("exec SP_TransferFCHistory @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashSentListViewModel> GetTotalFoxxCashSent(int ActionId, long UsersID)
        {
            List<FoxxCashSentListViewModel> objmodelist = new List<FoxxCashSentListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;


                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashSentListViewModel>("exec SP_TransferFCHistory @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<TransferFCHistoryListViewModel2> GetTransferFCHistory2(int ActionId, long UsersID)
        {
            List<TransferFCHistoryListViewModel2> objmodelist = new List<TransferFCHistoryListViewModel2>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;


                objmodelist = _dbContext.ExecuteStoredProcedure<TransferFCHistoryListViewModel2>("exec SP_TransferFCHistory @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashDetailsListViewModel> FoxxCashDetails1(int ActionId, FoxxCashDetailsViewModel model)
        {
            List<FoxxCashDetailsListViewModel> objmodelist = new List<FoxxCashDetailsListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value =model.UsersID;


                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashDetailsListViewModel>("exec SP_FoxxCashDetails @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashDetailsListViewModel2> FoxxCashDetails2(int ActionId, FoxxCashDetailsViewModel model)
        {
            List<FoxxCashDetailsListViewModel2> objmodelist = new List<FoxxCashDetailsListViewModel2>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _Year = new SqlParameter();
                _Year.Direction = System.Data.ParameterDirection.Input;
                _Year.DbType = System.Data.DbType.String;
                _Year.ParameterName = "@Year";
                _Year.Value = model.Year;

                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashDetailsListViewModel2>("exec SP_FoxxCashDetails @ActionId,@UsersID,@Year", _ActionId, _UsersID, _Year).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashDetailsListViewModel3> FoxxCashDetails3(int ActionId, FoxxCashDetailsViewModel model)
        {
            List<FoxxCashDetailsListViewModel3> objmodelist = new List<FoxxCashDetailsListViewModel3>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _Year = new SqlParameter();
                _Year.Direction = System.Data.ParameterDirection.Input;
                _Year.DbType = System.Data.DbType.String;
                _Year.ParameterName = "@Year";
                _Year.Value = model.Year;


                SqlParameter _Month = new SqlParameter();
                _Month.Direction = System.Data.ParameterDirection.Input;
                _Month.DbType = System.Data.DbType.String;
                _Month.ParameterName = "@Month";
                _Month.Value = model.Month;

                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashDetailsListViewModel3>("exec SP_FoxxCashDetails @ActionId,@UsersID,@Year,@Month", _ActionId, _UsersID, _Year, _Month).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashDetailsListViewModel4> FoxxCashDetails4(int ActionId, FoxxCashDetailsViewModel model)
        {
            List<FoxxCashDetailsListViewModel4> objmodelist = new List<FoxxCashDetailsListViewModel4>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _Year = new SqlParameter();
                _Year.Direction = System.Data.ParameterDirection.Input;
                _Year.DbType = System.Data.DbType.String;
                _Year.ParameterName = "@Year";
                _Year.Value = model.Year;


                SqlParameter _Month = new SqlParameter();
                _Month.Direction = System.Data.ParameterDirection.Input;
                _Month.DbType = System.Data.DbType.String;
                _Month.ParameterName = "@Month";
                _Month.Value = model.Month;

                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashDetailsListViewModel4>("exec SP_FoxxCashDetails @ActionId,@UsersID,@Year,@Month", _ActionId, _UsersID, _Year, _Month).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<FoxxCashDetailsListViewModel5> FoxxCashDetails5(int ActionId, FoxxCashDetailsViewModel model)
        {
            List<FoxxCashDetailsListViewModel5> objmodelist = new List<FoxxCashDetailsListViewModel5>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _Year = new SqlParameter();
                _Year.Direction = System.Data.ParameterDirection.Input;
                _Year.DbType = System.Data.DbType.String;
                _Year.ParameterName = "@Year";
                _Year.Value = model.Year;


                SqlParameter _Month = new SqlParameter();
                _Month.Direction = System.Data.ParameterDirection.Input;
                _Month.DbType = System.Data.DbType.String;
                _Month.ParameterName = "@Month";
                _Month.Value = model.Month;

                objmodelist = _dbContext.ExecuteStoredProcedure<FoxxCashDetailsListViewModel5>("exec SP_FoxxCashDetails @ActionId,@UsersID,@Year,@Month", _ActionId, _UsersID, _Year, _Month).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<TransferFCListViewModel> TransferFC(int ActionId, TransferFCViewModel model)
        {
            List<TransferFCListViewModel> objmodelist = new List<TransferFCListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _Search = new SqlParameter();
                _Search.Direction = System.Data.ParameterDirection.Input;
                _Search.DbType = System.Data.DbType.String;
                _Search.ParameterName = "@Search";
                _Search.Value = model.Search;

                objmodelist = _dbContext.ExecuteStoredProcedure<TransferFCListViewModel>("exec sp_TransferFC @ActionId,@UsersID,@Search", _ActionId, _UsersID, _Search).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel NewCommissionsSweptRecord(int ActionId, CommissionsSweptViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _CommissionProfilesID = new SqlParameter();
                _CommissionProfilesID.Direction = System.Data.ParameterDirection.Input;
                _CommissionProfilesID.DbType = System.Data.DbType.Int64;
                _CommissionProfilesID.ParameterName = "@CommissionProfilesID";
                _CommissionProfilesID.Value = model.CommissionProfilesID;

                SqlParameter _Amount = new SqlParameter();
                _Amount.Direction = System.Data.ParameterDirection.Input;
                _Amount.DbType = System.Data.DbType.Decimal;
                _Amount.ParameterName = "@Amount";
                _Amount.Value = model.Amount;


                SqlParameter _Description = new SqlParameter();
                _Description.Direction = System.Data.ParameterDirection.Input;
                _Description.DbType = System.Data.DbType.String;
                _Description.ParameterName = "@Description";
                _Description.Value = model.Description??(object)DBNull.Value;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_CommissionsSwept @ActionId,@UsersID,@CommissionProfilesID,@Amount,@Description", _ActionId, _UsersID, _CommissionProfilesID,_Amount,_Description).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public List<CommissionsSweptListViewModel> GetCommissionsSweptRecord(int ActionId, CommissionsSweptViewModel model)
        {
            List<CommissionsSweptListViewModel> objmodelist = new List<CommissionsSweptListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


               

                objmodelist = _dbContext.ExecuteStoredProcedure<CommissionsSweptListViewModel>("exec SP_CommissionsSwept @ActionId,@UsersID", _ActionId, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel NewCommissionsRecord(int ActionId, CommissionsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _CommissionTypesID = new SqlParameter();
                _CommissionTypesID.Direction = System.Data.ParameterDirection.Input;
                _CommissionTypesID.DbType = System.Data.DbType.Int64;
                _CommissionTypesID.ParameterName = "@CommissionTypesID";
                _CommissionTypesID.Value = model.CommissionTypesID;


                SqlParameter _Type = new SqlParameter();
                _Type.Direction = System.Data.ParameterDirection.Input;
                _Type.DbType = System.Data.DbType.String;
                _Type.ParameterName = "@Type";
                _Type.Value = model.Type ?? (object)DBNull.Value;

                SqlParameter _Amount = new SqlParameter();
                _Amount.Direction = System.Data.ParameterDirection.Input;
                _Amount.DbType = System.Data.DbType.Decimal;
                _Amount.ParameterName = "@Amount";
                _Amount.Value = model.Amount;


                SqlParameter _Description = new SqlParameter();
                _Description.Direction = System.Data.ParameterDirection.Input;
                _Description.DbType = System.Data.DbType.String;
                _Description.ParameterName = "@Description";
                _Description.Value = model.Description ?? (object)DBNull.Value;


                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int64;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = model.OrderID;


                SqlParameter _FromLevel = new SqlParameter();
                _FromLevel.Direction = System.Data.ParameterDirection.Input;
                _FromLevel.DbType = System.Data.DbType.Int64;
                _FromLevel.ParameterName = "@FromLevel";
                _FromLevel.Value = model.FromLevel;

                SqlParameter _RecipientRankID = new SqlParameter();
                _RecipientRankID.Direction = System.Data.ParameterDirection.Input;
                _RecipientRankID.DbType = System.Data.DbType.Int64;
                _RecipientRankID.ParameterName = "@RecipientRankID";
                _RecipientRankID.Value = model.RecipientRankID;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Commissions @ActionId,@UsersID,@CommissionTypesID,@Type,@Amount,@Description,@OrderID,@FromLevel,@RecipientRankID", _ActionId, _UsersID, _CommissionTypesID,_Type, _Amount, _Description,_OrderID,_FromLevel,_RecipientRankID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public CommissionsListViewModel setCommissionCodeAndName(int ActionId, CommissionsViewModel model)
        {
            CommissionsListViewModel objmodelist = new CommissionsListViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;

                SqlParameter _CommissionTypesID = new SqlParameter();
                _CommissionTypesID.Direction = System.Data.ParameterDirection.Input;
                _CommissionTypesID.DbType = System.Data.DbType.Int64;
                _CommissionTypesID.ParameterName = "@CommissionTypesID";
                _CommissionTypesID.Value = model.CommissionTypesID;


                objmodelist = _dbContext.ExecuteStoredProcedure<CommissionsListViewModel>("exec SP_Commissions @ActionId,@UsersID,@CommissionTypesID", _ActionId, _UsersID,_CommissionTypesID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public long GetRecipientCommissionID(int ActionId, CommissionsViewModel model)
        {
            long RecipientUserID = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                RecipientUserID = _dbContext.ExecuteStoredProcedure<long>("exec SP_Commissions @ActionId,@UsersID", _ActionId, _UsersID).FirstOrDefault();

               


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RecipientUserID;
        }

        public SqlResponseBaseModel NewFoxxCashTransferLog(int ActionId, CommissionsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _CommissionTypesID = new SqlParameter();
                _CommissionTypesID.Direction = System.Data.ParameterDirection.Input;
                _CommissionTypesID.DbType = System.Data.DbType.Int64;
                _CommissionTypesID.ParameterName = "@CommissionTypesID";
                _CommissionTypesID.Value = model.CommissionTypesID;


                SqlParameter _Type = new SqlParameter();
                _Type.Direction = System.Data.ParameterDirection.Input;
                _Type.DbType = System.Data.DbType.String;
                _Type.ParameterName = "@Type";
                _Type.Value = model.Type;

                SqlParameter _Amount = new SqlParameter();
                _Amount.Direction = System.Data.ParameterDirection.Input;
                _Amount.DbType = System.Data.DbType.Decimal;
                _Amount.ParameterName = "@Amount";
                _Amount.Value = model.Amount;


                SqlParameter _Description = new SqlParameter();
                _Description.Direction = System.Data.ParameterDirection.Input;
                _Description.DbType = System.Data.DbType.String;
                _Description.ParameterName = "@Description";
                _Description.Value = model.Description ?? (object)DBNull.Value;


                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int64;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = model.OrderID;


                SqlParameter _FromLevel = new SqlParameter();
                _FromLevel.Direction = System.Data.ParameterDirection.Input;
                _FromLevel.DbType = System.Data.DbType.Int64;
                _FromLevel.ParameterName = "@FromLevel";
                _FromLevel.Value = model.FromLevel;

                SqlParameter _RecipientRankID = new SqlParameter();
                _RecipientRankID.Direction = System.Data.ParameterDirection.Input;
                _RecipientRankID.DbType = System.Data.DbType.Int64;
                _RecipientRankID.ParameterName = "@RecipientRankID";
                _RecipientRankID.Value = model.RecipientRankID;

                SqlParameter _RecipientUserID = new SqlParameter();
                _RecipientUserID.Direction = System.Data.ParameterDirection.Input;
                _RecipientUserID.DbType = System.Data.DbType.Int64;
                _RecipientUserID.ParameterName = "@RecipientUserID";
                _RecipientUserID.Value = model.RecipientUserID;

                SqlParameter _FromCommissionsSweptID = new SqlParameter();
                _FromCommissionsSweptID.Direction = System.Data.ParameterDirection.Input;
                _FromCommissionsSweptID.DbType = System.Data.DbType.Int64;
                _FromCommissionsSweptID.ParameterName = "@FromCommissionsSweptID";
                _FromCommissionsSweptID.Value = model.FromCommissionsSweptID;

                SqlParameter _RecipientCommissionRecordID = new SqlParameter();
                _RecipientCommissionRecordID.Direction = System.Data.ParameterDirection.Input;
                _RecipientCommissionRecordID.DbType = System.Data.DbType.Int64;
                _RecipientCommissionRecordID.ParameterName = "@RecipientCommissionRecordID";
                _RecipientCommissionRecordID.Value = model.RecipientCommissionRecordID;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_Commissions @ActionId,@UsersID,@CommissionTypesID,@Type,@Amount,@Description,@OrderID,@FromLevel,@RecipientRankID,@RecipientUserID,@FromCommissionsSweptID,@RecipientCommissionRecordID", _ActionId, _UsersID, _CommissionTypesID, _Type, _Amount, _Description, _OrderID, _FromLevel, _RecipientRankID, _RecipientUserID, _FromCommissionsSweptID, _RecipientCommissionRecordID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _baseModel;
        }

        public long GetDownlineUsersLevel(int ActionId, CommissionsViewModel model)
        {
            long UserIsInUplinesLevelX = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;



                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _CommissionTypesID = new SqlParameter();
                _CommissionTypesID.Direction = System.Data.ParameterDirection.Input;
                _CommissionTypesID.DbType = System.Data.DbType.Int64;
                _CommissionTypesID.ParameterName = "@CommissionTypesID";
                _CommissionTypesID.Value = model.CommissionTypesID;


                SqlParameter _Type = new SqlParameter();
                _Type.Direction = System.Data.ParameterDirection.Input;
                _Type.DbType = System.Data.DbType.String;
                _Type.ParameterName = "@Type";
                _Type.Value = model.Type;

                SqlParameter _Amount = new SqlParameter();
                _Amount.Direction = System.Data.ParameterDirection.Input;
                _Amount.DbType = System.Data.DbType.Decimal;
                _Amount.ParameterName = "@Amount";
                _Amount.Value = model.Amount;


                SqlParameter _Description = new SqlParameter();
                _Description.Direction = System.Data.ParameterDirection.Input;
                _Description.DbType = System.Data.DbType.String;
                _Description.ParameterName = "@Description";
                _Description.Value = model.Description ?? (object)DBNull.Value;


                SqlParameter _OrderID = new SqlParameter();
                _OrderID.Direction = System.Data.ParameterDirection.Input;
                _OrderID.DbType = System.Data.DbType.Int64;
                _OrderID.ParameterName = "@OrderID";
                _OrderID.Value = model.OrderID;


                SqlParameter _FromLevel = new SqlParameter();
                _FromLevel.Direction = System.Data.ParameterDirection.Input;
                _FromLevel.DbType = System.Data.DbType.Int64;
                _FromLevel.ParameterName = "@FromLevel";
                _FromLevel.Value = model.FromLevel;

                SqlParameter _RecipientRankID = new SqlParameter();
                _RecipientRankID.Direction = System.Data.ParameterDirection.Input;
                _RecipientRankID.DbType = System.Data.DbType.Int64;
                _RecipientRankID.ParameterName = "@RecipientRankID";
                _RecipientRankID.Value = model.RecipientRankID;

                SqlParameter _RecipientUserID = new SqlParameter();
                _RecipientUserID.Direction = System.Data.ParameterDirection.Input;
                _RecipientUserID.DbType = System.Data.DbType.Int64;
                _RecipientUserID.ParameterName = "@RecipientUserID";
                _RecipientUserID.Value = model.RecipientUserID;



                UserIsInUplinesLevelX = _dbContext.ExecuteStoredProcedure<long>("exec SP_Commissions @ActionId,@UsersID,@CommissionTypesID,@Type,@Amount,@Description,@OrderID,@FromLevel,@RecipientRankID,@RecipientUserID", _ActionId, _UsersID, _CommissionTypesID, _Type, _Amount, _Description, _OrderID, _FromLevel, _RecipientRankID, _RecipientUserID).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }

            return UserIsInUplinesLevelX;
        }
        public decimal GetCommissionSummaryData(int ActionId, long UsersID)
        {
            decimal Value = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                 SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = UsersID;


                Value = _dbContext.ExecuteStoredProcedure<decimal>("exec SP_Commissions @ActionId,@UsersID", _ActionId, _UsersID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Value;
        }
    }
}
