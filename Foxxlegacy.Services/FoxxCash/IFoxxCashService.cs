﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.FoxxCash
{
   public interface IFoxxCashService
    {
        List<MyFoxxCashListViewModel> GetTotals(int ActionId, long UsersID);

        List<SweeperHistoryListViewModel> GetSweeperHistory(int ActionId, long UsersID);
        List<TransferFCHistoryListViewModel> GetTransferFCHistory(int ActionId, long UsersID);

        List<FoxxCashReceivedListViewModel> GetTotalFoxxCashReceived(int ActionId, long UsersID);

        List<FoxxCashSentListViewModel> GetTotalFoxxCashSent(int ActionId, long UsersID);
        List<TransferFCHistoryListViewModel2> GetTransferFCHistory2(int ActionId, long UsersID);

        List<FoxxCashDetailsListViewModel> FoxxCashDetails1(int ActionId, FoxxCashDetailsViewModel model);
        List<FoxxCashDetailsListViewModel2> FoxxCashDetails2(int ActionId, FoxxCashDetailsViewModel model);
        List<FoxxCashDetailsListViewModel3> FoxxCashDetails3(int ActionId, FoxxCashDetailsViewModel model);
        List<FoxxCashDetailsListViewModel4> FoxxCashDetails4(int ActionId, FoxxCashDetailsViewModel model);
        List<FoxxCashDetailsListViewModel5> FoxxCashDetails5(int ActionId, FoxxCashDetailsViewModel model);
        List<TransferFCListViewModel> TransferFC(int ActionId, TransferFCViewModel model);
        SqlResponseBaseModel NewCommissionsSweptRecord(int ActionId, CommissionsSweptViewModel model);

        List<CommissionsSweptListViewModel> GetCommissionsSweptRecord(int ActionId, CommissionsSweptViewModel model);

        SqlResponseBaseModel NewCommissionsRecord(int ActionId, CommissionsViewModel model);

        CommissionsListViewModel setCommissionCodeAndName(int ActionId, CommissionsViewModel model);

       long  GetRecipientCommissionID(int ActionId, CommissionsViewModel model);

        SqlResponseBaseModel NewFoxxCashTransferLog(int ActionId, CommissionsViewModel model);

        long GetDownlineUsersLevel(int ActionId, CommissionsViewModel model);
        decimal GetCommissionSummaryData(int ActionId, long UsersID);
    }
}
