﻿using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.Commission
{
    public class CommissionService : ICommissionService
    {
        private readonly IDbContext _dbContext;

        #region CTor
        public CommissionService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        #endregion

        public int UserCommissionDistributionGenerate(int ActionId, DoCommissionViewModel model)
        {
            int InsertedRowCount = 0;
            try
            {
                var Param = new List<object>();

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlValue = model.UserId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@OrderId",
                    SqlValue = model.OrderId
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderOwnerId",
                    SqlValue = model.UserId
                });

                /* exec sp_generate_user_order_commission @ActionId = 1, @UserId = 3000, @OrderId = 1 */
                var response = _dbContext.ExecuteStoredProcedure<CommissionDistributionResponseViewModel>("exec sp_generate_user_order_commission @ActionId, @UserId, @OrderId,@orderOwnerId", Param.ToArray()).FirstOrDefault();

                if (response != null)
                    InsertedRowCount = response.RowAffected;
            }
            catch (Exception ex)
            {
                _ = ex;
            }

            return InsertedRowCount;
        }
        public int UserCommissionDistributionGenerateRank(int ActionId, DoCommissionViewModel model)
        {
            int InsertedRowCount = 0;
            try
            {
                var Param = new List<object>();

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlValue = model.UserId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@OrderId",
                    SqlValue = model.OrderId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@RankId",
                    SqlValue = model.RankId
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CommissionType",
                    SqlValue = model.CommissionType ?? (object)DBNull.Value
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderOwnerId",
                    SqlValue = model.OrginalOrderOwnerId 
                });

                /* exec sp_generate_user_order_commission @ActionId = 1, @UserId = 3000, @OrderId = 1 */
                var response = _dbContext.ExecuteStoredProcedure<CommissionDistributionResponseViewModel>("exec sp_generate_rank_user_order_commission  @ActionId, @UserId, @OrderId,@RankId,@CommissionType,@orderOwnerId", Param.ToArray()).FirstOrDefault();
                if (response != null)
                    InsertedRowCount = response.RowAffected;
            }
            catch (Exception ex)
            {
                _ = ex;
            }

            return InsertedRowCount;
        }

        public int PayMoreMitoCash(int ActionId, DoCommissionViewModel model)
        {
            int InsertedRowCount = 0;
            try
            {
                var Param = new List<object>();

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlValue = model.UserId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@OrderId",
                    SqlValue = model.OrderId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@RankId",
                    SqlValue = model.RankId
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.NVarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CommissionType",
                    SqlValue = model.CommissionType ?? (object)DBNull.Value
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderOwnerId",
                    SqlValue = model.OrginalOrderOwnerId 
                });

                /* exec sp_generate_user_order_commission @ActionId = 1, @UserId = 3000, @OrderId = 1 */
                var response = _dbContext.ExecuteStoredProcedure<CommissionDistributionResponseViewModel>("exec sp_generate_rank_user_order_commission  @ActionId, @UserId, @OrderId,@RankId,@CommissionType,@orderOwnerId", Param.ToArray()).FirstOrDefault();
                if (response != null)
                    InsertedRowCount = response.RowAffected;
            }
            catch (Exception ex)
            {
                _ = ex;
            }

            return InsertedRowCount;
        }

        public int PayCash(int ActionId, DoCommissionViewModel model)
        {
            int InsertedRowCount = 0;
            try
            {
                var Param = new List<object>();

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@Actionid",
                    SqlValue = ActionId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@UserId",
                    SqlValue = model.UserId
                });

                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@OrderId",
                    SqlValue = model.OrderId
                });


                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@RankId",
                    SqlValue = model.RankId
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.NVarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@CommissionType",
                    SqlValue = model.CommissionType ?? (object)DBNull.Value
                });
                Param.Add(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderOwnerId",
                    SqlValue = model.OrginalOrderOwnerId 
                });

                /* exec sp_generate_user_order_commission @ActionId = 1, @UserId = 3000, @OrderId = 1 */
                var response = _dbContext.ExecuteStoredProcedure<CommissionDistributionResponseViewModel>("exec sp_generate_rank_user_order_commission  @ActionId, @UserId, @OrderId,@RankId,@CommissionType,@orderOwnerId", Param.ToArray()).FirstOrDefault();
                if (response != null)
                    InsertedRowCount = response.RowAffected;
            }
            catch (Exception ex)
            {
                _ = ex;
            }

            return InsertedRowCount;
        }

        public List<ViewCommissionModel> GetCommision(GetCommissionModel model)
        {
            var a = String.Format(CultureInfo.InvariantCulture, "{0:M-d-yyyy}", DateTime.Now);

            var response = new List<ViewCommissionModel>();
            try
            {
                var Param = new SqlParameter[5];
                Param[0]=(new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@searchType",
                    SqlValue = model.SearchType
                });
                Param[1] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderNo",
                    SqlValue = model.OrderNo
                });
                Param[2] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@fromDate",
                    SqlValue =model.FromDate==null?DateTime.Now.Date.ToString("yyyy-MM-dd") : model.FromDate.Value.ToString("yyyy-MM-dd")
                });
                Param[3] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@toDate",
                    SqlValue =model.ToDate==null?DateTime.Now.Date.ToString("yyyy-MM-dd") : model.ToDate.Value.ToString("yyyy-MM-dd")
                });
                Param[4] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@userID",
                    SqlValue = model.UserId
                });
                //var dt = _dbContext.ExecuteStoredProcedure<ViewCommissionModel>("exec Sp_GetCommission @searchType,@orderNo,@fromDate,@toDate,@userID", Param.ToArray()).ToList();

                var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_GetCommission", Param);
                if(dt!=null && dt.Rows.Count > 0)
                {
                    response = dt.ConvertDataTableToList<ViewCommissionModel>();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        public AdminCommissionViewModel GetOrderMetaData(GetCommissionModel model)
        {
            AdminCommissionViewModel m = new AdminCommissionViewModel();
            m._commissionList = GetCommision(model);

            var ActiveCustomerMetaData = GetActiveCustomerMetaData(1, model.OrderNo);
            if(ActiveCustomerMetaData !=null && ActiveCustomerMetaData.Rows.Count > 0)
            {
                m._activeCustomerMetaData = ActiveCustomerMetaData.ConvertDataTableToList<ActiveCustomerMetaDataViewModel>();
            }

            var UserQualify = GetActiveCustomerMetaData(2, model.OrderNo);
            if (UserQualify != null && UserQualify.Rows.Count > 0)
            {
                m._userQualify = UserQualify.ConvertDataTableToList<UserQualifyMetaDataViewModel>();
            }

            var RankHistory = GetActiveCustomerMetaData(3, model.OrderNo);
            if (RankHistory != null && RankHistory.Rows.Count > 0)
            {
                m._rankHistory = RankHistory.ConvertDataTableToList<RankHistoryMetaViewModel>();
            }

            return m;
        }
        public DataTable GetActiveCustomerMetaData(int ActionId,int OrderId)
        {
            var Param = new SqlParameter[2];
            Param[0] = (new SqlParameter
            {
                SqlDbType = System.Data.SqlDbType.BigInt,
                Direction = System.Data.ParameterDirection.Input,
                ParameterName = "@actionId",
                SqlValue = ActionId
            });
            Param[1] = (new SqlParameter
            {
                SqlDbType = System.Data.SqlDbType.BigInt,
                Direction = System.Data.ParameterDirection.Input,
                ParameterName = "@orderId",
                SqlValue = OrderId
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetOrderMetaData", Param);
            return dt;
        }
        public List<ViewCommissionModel> GetMyCommision(GetCommissionModel model)
        {
            

            var response = new List<ViewCommissionModel>();
            try
            {
                var Param = new SqlParameter[6];
                Param[0] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@searchType",
                    SqlValue = model.SearchType
                });
                Param[1] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.BigInt,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@orderNo",
                    SqlValue = model.OrderNo
                });
                Param[2] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@fromDate",
                    SqlValue = model.FromDate == null ? DateTime.Now.Date.ToString("yyyy-MM-dd") : model.FromDate.Value.ToString("yyyy-MM-dd")
                });
                Param[3] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.VarChar,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@toDate",
                    SqlValue = model.ToDate == null ? DateTime.Now.Date.ToString("yyyy-MM-dd") : model.ToDate.Value.ToString("yyyy-MM-dd")
                });
                Param[4] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@userID",
                    SqlValue = model.UserId
                });
                Param[5] = (new SqlParameter
                {
                    SqlDbType = System.Data.SqlDbType.Int,
                    Direction = System.Data.ParameterDirection.Input,
                    ParameterName = "@sortBy",
                    SqlValue = model.SortBy
                });
                //var dt = _dbContext.ExecuteStoredProcedure<ViewCommissionModel>("exec Sp_GetCommission @searchType,@orderNo,@fromDate,@toDate,@userID", Param.ToArray()).ToList();

                var dt = _dbContext.ExecuteStoredProcedureDataTable("GetMyCommissions", Param);
                if (dt != null && dt.Rows.Count > 0)
                {
                    response = dt.ConvertDataTableToList<ViewCommissionModel>();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        public List<UserWiseCommissionModel>  GetUserWiseCommissions(int Type)
        {
            List<UserWiseCommissionModel> list = new List<UserWiseCommissionModel>();
            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@type",
                Value = Type
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetUserWiseCommissions", parameters);
            if(dt!=null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    UserWiseCommissionModel m = new UserWiseCommissionModel();
                    m.ID = Convert.ToInt32(item["id"]);
                    m.Name = Convert.ToString(item["firstname"]) + " " + Convert.ToString(item["lastname"]);
                    m.UserName = Convert.ToString(item["username"]);
                    m.RoleName = Convert.ToString(item["RoleName"]);
                    m.Email = Convert.ToString(item["email"]);
                    m.OrderTotalCommissionAmount = Convert.ToDecimal(item["ordertotalcommissionamount"]);
                    m.Amount = Convert.ToDecimal(item["amount"]);
                    m.OrderCount = Convert.ToInt32(item["OrderCount"]);
                    list.Add(m);
                }
            }
            return list;
        }
        public bool InsertTransferCommissions(TransferCashViewModel model)
        {
            bool status = false;
            SqlParameter[] parameters = new SqlParameter[5];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@transferBy",
                Value = model.CurrentUserId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@transferTo",
                Value = model.TargetUserId
            });
            parameters[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Decimal,
                ParameterName = "@transferAmount",
                Value = model.Amount
            });
            parameters[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@transferType",
                Value = model.TransferType
            });
            parameters[4] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@description",
                Value = model.Message
            });
            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("InsertTransferCash", parameters);
            if (dt != null)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out int RowsCount);
                status = RowsCount > 0;
                if (RowsCount == 0)
                {
                    string err = Convert.ToString(dt.Rows[0]["ErrorMessage"]);
                    throw new Exception(err);
                }
            }
            return status;
        }
    }
}
