﻿using Foxxlegacy.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Services.Commission
{
    public interface ICommissionService
    {

        int UserCommissionDistributionGenerate(int ActionId, DoCommissionViewModel model);
        int UserCommissionDistributionGenerateRank(int ActionId, DoCommissionViewModel model);
        int PayMoreMitoCash(int ActionId, DoCommissionViewModel model);
        int PayCash(int ActionId, DoCommissionViewModel model);
        List<ViewCommissionModel> GetCommision(GetCommissionModel model);
        List<ViewCommissionModel> GetMyCommision(GetCommissionModel model);
        List<UserWiseCommissionModel> GetUserWiseCommissions(int Type);
        AdminCommissionViewModel GetOrderMetaData(GetCommissionModel model);
        bool InsertTransferCommissions(TransferCashViewModel model);
    }
}
