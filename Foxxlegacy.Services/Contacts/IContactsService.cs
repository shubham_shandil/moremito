﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.Contacts
{
    public interface IContactsService
    {
        SqlResponseBaseModel ContactsInsertUpdate(int ActionId, ContactUsViewModel model);

        List<ContactUsListViewModel> GetContactUsList(int ActionId, ContactUsViewModel model);

        List<ContactDetailsListViewModel> ContactDetails(int ActionId, ContactDetailsViewModel model);

    
        SqlResponseBaseModel RecordDialogueContactDetails(int ActionId, ContactDetailsViewModel model);

        List<ContactNotesListViewModel> GetContactNotesList(int ActionId, ContactDetailsViewModel model);
        List<ArchiveCommonResponseListViewModel> GetArchiveCommonResponseList(int ActionId, ArchiveCommonResponseViewModel model);

        SqlResponseBaseModel CreateNewCommonResponseNow(int ActionId, ArchiveCommonResponseViewModel model);

        List<CommonResponseList> GetcommonResponseList(int ActionId, ArchiveCommonResponseViewModel model);

    }
}
