﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;

namespace Foxxlegacy.Services.Contacts
{
    public class ContactsService : IContactsService
    {
        private readonly IDbContext _dbContext;

        public ContactsService()
        {
        }

        #region CTor
        public ContactsService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }

        public List<ContactDetailsListViewModel> ContactDetails(int ActionId, ContactDetailsViewModel model)
        {
            List<ContactDetailsListViewModel> objmodelist = new List<ContactDetailsListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ContactID";
                _ID.Value = model.ContactID;

                


                objmodelist = _dbContext.ExecuteStoredProcedure<ContactDetailsListViewModel>("exec sp_ContactDetails @ActionId,@ContactID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        #endregion
        public SqlResponseBaseModel ContactsInsertUpdate(int ActionId, ContactUsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ID";
                _ID.Value = model.ID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;

                SqlParameter _FromUsersID = new SqlParameter();
                _FromUsersID.Direction = System.Data.ParameterDirection.Input;
                _FromUsersID.DbType = System.Data.DbType.Int64;
                _FromUsersID.ParameterName = "@FromUsersID";
                _FromUsersID.Value = model.FromUsersID;

                SqlParameter _Origin = new SqlParameter();
                _Origin.Direction = System.Data.ParameterDirection.Input;
                _Origin.DbType = System.Data.DbType.String;
                _Origin.ParameterName = "@Origin";
                _Origin.Value = model.Origin ?? (object)DBNull.Value;

                SqlParameter _Name = new SqlParameter();
                _Name.Direction = System.Data.ParameterDirection.Input;
                _Name.DbType = System.Data.DbType.String;
                _Name.ParameterName = "@Name";
                _Name.Value = model.Name ?? (object)DBNull.Value;

                SqlParameter _EmailId = new SqlParameter();
                _EmailId.Direction = System.Data.ParameterDirection.Input;
                _EmailId.DbType = System.Data.DbType.String;
                _EmailId.ParameterName = "@EmailId";
                _EmailId.Value = model.EmailId ?? (object)DBNull.Value;

                SqlParameter _Phone = new SqlParameter();
                _Phone.Direction = System.Data.ParameterDirection.Input;
                _Phone.DbType = System.Data.DbType.String;
                _Phone.ParameterName = "@Phone";
                _Phone.Value = model.Phone ?? (object)DBNull.Value;


                SqlParameter _Subject = new SqlParameter();
                _Subject.Direction = System.Data.ParameterDirection.Input;
                _Subject.DbType = System.Data.DbType.String;
                _Subject.ParameterName = "@Subject";
                _Subject.Value = model.Subject ?? (object)DBNull.Value;

                SqlParameter _Message = new SqlParameter();
                _Message.Direction = System.Data.ParameterDirection.Input;
                _Message.DbType = System.Data.DbType.String;
                _Message.ParameterName = "@Message";
                _Message.Value = model.Message ?? (object)DBNull.Value;

                SqlParameter _IpAddress = new SqlParameter();
                _IpAddress.Direction = System.Data.ParameterDirection.Input;
                _IpAddress.DbType = System.Data.DbType.String;
                _IpAddress.ParameterName = "@IpAddress";
                _IpAddress.Value = model.IpAddress ?? (object)DBNull.Value;


                SqlParameter _Notes = new SqlParameter();
                _Notes.Direction = System.Data.ParameterDirection.Input;
                _Notes.DbType = System.Data.DbType.String;
                _Notes.ParameterName = "@Notes";
                _Notes.Value = model.Notes ?? (object)DBNull.Value;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_ContactAdmin @ActionId,@ID,@UsersID,@FromUsersID,@Origin,@Name,@EmailId,@Phone,@Subject,@Message,@IpAddress,@Notes", _ActionId, _ID, _UsersID, _FromUsersID, _Origin, _Name, _EmailId, _Phone, _Subject, _Message, _IpAddress, _Notes).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

       

        public List<ContactNotesListViewModel> GetContactNotesList(int ActionId, ContactDetailsViewModel model)
        {
            List<ContactNotesListViewModel> objmodelist = new List<ContactNotesListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ContactID";
                _ID.Value = model.ContactID;




                objmodelist = _dbContext.ExecuteStoredProcedure<ContactNotesListViewModel>("exec sp_ContactDetails @ActionId,@ContactID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<ContactUsListViewModel> GetContactUsList(int ActionId, ContactUsViewModel model)
        {
            List<ContactUsListViewModel> objmodelist = new List<ContactUsListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ID";
                _ID.Value = model.ID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;


                SqlParameter _FromUsersID = new SqlParameter();
                _FromUsersID.Direction = System.Data.ParameterDirection.Input;
                _FromUsersID.DbType = System.Data.DbType.Int64;
                _FromUsersID.ParameterName = "@FromUsersID";
                _FromUsersID.Value = model.FromUsersID;



                objmodelist = _dbContext.ExecuteStoredProcedure<ContactUsListViewModel>("exec sp_ContactAdmin @ActionId,@ID,@UsersID,@FromUsersID", _ActionId, _ID, _UsersID, _FromUsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel RecordDialogueContactDetails(int ActionId, ContactDetailsViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ContactID";
                _ID.Value = model.ContactID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;



                SqlParameter _Dialogue = new SqlParameter();
                _Dialogue.Direction = System.Data.ParameterDirection.Input;
                _Dialogue.DbType = System.Data.DbType.String;
                _Dialogue.ParameterName = "@Dialogue";
                _Dialogue.Value = model.Dialogue ?? (object)DBNull.Value;


                SqlParameter _Notes = new SqlParameter();
                _Notes.Direction = System.Data.ParameterDirection.Input;
                _Notes.DbType = System.Data.DbType.String;
                _Notes.ParameterName = "@Notes";
                _Notes.Value = model.Notes?? (object)DBNull.Value;

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_RecordDialogueContactDetails @ActionId,@ContactID,@UsersID,@Dialogue,@Notes", _ActionId, _ID, _UsersID,_Dialogue, _Notes).FirstOrDefault();

            }
            catch (Exception ex)
            {
               // _baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }


        public List<ArchiveCommonResponseListViewModel> GetArchiveCommonResponseList(int ActionId, ArchiveCommonResponseViewModel model)
        {
            List<ArchiveCommonResponseListViewModel> objmodelist = new List<ArchiveCommonResponseListViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ContactID";
                _ID.Value = 0;




                objmodelist = _dbContext.ExecuteStoredProcedure<ArchiveCommonResponseListViewModel>("exec sp_ContactDetails @ActionId,@ContactID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel CreateNewCommonResponseNow(int ActionId, ArchiveCommonResponseViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ContactID",
                    Value = model.ID
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Dialogue",
                    Value = ""
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Notes",
                    Value = ""
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Name",
                    Value = model.Name ?? (object)DBNull.Value
                }) ;
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ReplyText",
                    Value = model.ReplyText ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@DisplayOrder",
                    Value = model.DisplayOrder 
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec SP_RecordDialogueContactDetails @actionId," +
                    "@ContactID,@UsersID,@Dialogue,@Notes,@Name,@ReplyText,@DisplayOrder", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public List<CommonResponseList> GetcommonResponseList(int ActionId, ArchiveCommonResponseViewModel model)
        {
            List<CommonResponseList> objmodelist = new List<CommonResponseList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _ID = new SqlParameter();
                _ID.Direction = System.Data.ParameterDirection.Input;
                _ID.DbType = System.Data.DbType.Int64;
                _ID.ParameterName = "@ContactID";
                _ID.Value =model.ID;




                objmodelist = _dbContext.ExecuteStoredProcedure<CommonResponseList>("exec sp_ContactDetails @ActionId,@ContactID", _ActionId, _ID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
    }
}
