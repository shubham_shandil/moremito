﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Foxxlegacy.Core;
using Foxxlegacy.ViewModel.ViewModel;
using System.Web.Mvc;
using System.Data;

namespace Foxxlegacy.Services.Members
{
    public class MembersService : IMembersService
    {
        private readonly IDbContext _dbContext;

        public MembersService()
        {
        }

        #region CTor
        public MembersService(IDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        #endregion
        public List<EmailNotificationSettings> GetEmailNotificationSettings(int ActionId, EmailNotificationSettingsViewModel model)
        {
            List<EmailNotificationSettings> objmodelist = new List<EmailNotificationSettings>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@OptOutCatID";
                _Id.Value = model.OptOutCatID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;



                objmodelist = _dbContext.ExecuteStoredProcedure<EmailNotificationSettings>("exec sp_EmailNotificationSettings @ActionId,@OptOutCatID,@UsersID", _ActionId, _Id, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel EmailNotificationSettingsOptOutInsertDelete(int ActionId, EmailNotificationSettingsViewModel model)
        {

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@OptOutCatID";
                _Id.Value = model.OptOutCatID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;



                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_EmailNotificationSettings @ActionId,@OptOutCatID,@UsersID", _ActionId, _Id, _UsersID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public UserInformationViewModel GetUserInformation(int ActionId, long UserId)
        {
            UserInformationViewModel model = new UserInformationViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = null ?? (Object)DBNull.Value;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = UserId;

                model = _dbContext.ExecuteStoredProcedure<UserInformationViewModel>("exec SP_Get_UserDetails @ActionId,@UserName,@UserId", _ActionId, _UserName, _UserId).FirstOrDefault();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public RepresentativeInformationViewModel GetRepresentativeInformation(int ActionId, long UserId, bool mode)
        {
            RepresentativeInformationViewModel model = new RepresentativeInformationViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@USER_ID";
                _UserId.Value = UserId;

                SqlParameter _mode = new SqlParameter();
                _mode.Direction = System.Data.ParameterDirection.Input;
                _mode.DbType = System.Data.DbType.Boolean;
                _mode.ParameterName = "@PLACEMENT_MODE";
                _mode.Value = mode;

                model = _dbContext.ExecuteStoredProcedure<RepresentativeInformationViewModel>("exec sp_support_network @ACTION,@USER_ID,@PLACEMENT_MODE", _ActionId, _UserId, _mode).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public long GetLevelInformation(int ActionId, long InfoId, long Id)
        {
            long Level = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UplineId = new SqlParameter();
                _UplineId.Direction = System.Data.ParameterDirection.Input;
                _UplineId.DbType = System.Data.DbType.Int64;
                _UplineId.ParameterName = "@UPLINE_USERS_ID";
                _UplineId.Value = Id;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@USER_NAME";
                _UserName.Value = string.Empty;

                Level = _dbContext.ExecuteStoredProcedure<long>("exec sp_personal_details @ACTION,@USER_NAME,@ID,@UPLINE_USERS_ID", _ActionId, _UserName, _InfoId, _UplineId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Level;
        }
        public CustomerDetailsViewModel GetCustomerDetails(int ActionId, long CustomerId)
        {
            CustomerDetailsViewModel model = new CustomerDetailsViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _CustomerId = new SqlParameter();
                _CustomerId.Direction = System.Data.ParameterDirection.Input;
                _CustomerId.DbType = System.Data.DbType.Int64;
                _CustomerId.ParameterName = "@CUSTOMER_ID";
                _CustomerId.Value = CustomerId;

                model = _dbContext.ExecuteStoredProcedure<CustomerDetailsViewModel>("exec sp_customer_details @ACTION,@CUSTOMER_ID", _ActionId, _CustomerId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public QuestionAnswer GetMQDetails(int ActionId, long InfoId)
        {
            QuestionAnswer model = new QuestionAnswer();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@USER_NAME";
                _UserName.Value = string.Empty;

                model = _dbContext.ExecuteStoredProcedure<QuestionAnswer>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UserName, _InfoId).FirstOrDefault();
                if (model == null)
                {
                    model = new QuestionAnswer();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public PersonalInfo GetPersonalDetails(int ActionId, long InfoId)
        {
            PersonalInfo objmodelist = new PersonalInfo();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<PersonalInfo>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UsersName, _InfoId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public RecurringOrder GetRecurringOrderDetails(int ActionId, long InfoId)
        {
            RecurringOrder objmodelist = new RecurringOrder();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<RecurringOrder>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UsersName, _InfoId).FirstOrDefault();

                if (objmodelist == null)
                {
                    objmodelist = new RecurringOrder();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public List<ProcessOrder> GetProcessOrderDetails(int ActionId, long InfoId)
        {
            List<ProcessOrder> objmodelist = new List<ProcessOrder>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<ProcessOrder>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UsersName, _InfoId).ToList();
                if (objmodelist == null)
                {
                    objmodelist = new List<ProcessOrder>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<Advertisment> GetAdvertismentDetails(int ActionId, long InfoId)
        {
            List<Advertisment> objmodelist = new List<Advertisment>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<Advertisment>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UsersName, _InfoId).ToList();
                if (objmodelist == null)
                {
                    objmodelist = new List<Advertisment>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<RankHistory> GetRankHistoryDetails(int ActionId, long InfoId)
        {
            List<RankHistory> objmodelist = new List<RankHistory>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<RankHistory>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UsersName, _InfoId).ToList();
                if (objmodelist == null)
                {
                    objmodelist = new List<RankHistory>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public List<MarketingActivity> GetMarketingActivityDetails(int ActionId, long InfoId)
        {
            List<MarketingActivity> objmodelist = new List<MarketingActivity>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;

                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<MarketingActivity>("exec sp_personal_details @ACTION,@USER_NAME,@ID", _ActionId, _UsersName, _InfoId).ToList();
                if (objmodelist == null)
                {
                    objmodelist = new List<MarketingActivity>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public long GetMobileAppActivityDetails(int ActionId, long InfoId, string TimeClause)
        {
            long TheCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;



                SqlParameter _UsersName = new SqlParameter();
                _UsersName.Direction = System.Data.ParameterDirection.Input;
                _UsersName.DbType = System.Data.DbType.String;
                _UsersName.ParameterName = "@USER_NAME";
                _UsersName.Value = string.Empty;

                SqlParameter _InfoId = new SqlParameter();
                _InfoId.Direction = System.Data.ParameterDirection.Input;
                _InfoId.DbType = System.Data.DbType.Int64;
                _InfoId.ParameterName = "@ID";
                _InfoId.Value = InfoId;


                SqlParameter _UPLINE_USERS_ID = new SqlParameter();
                _UPLINE_USERS_ID.Direction = System.Data.ParameterDirection.Input;
                _UPLINE_USERS_ID.DbType = System.Data.DbType.Int64;
                _UPLINE_USERS_ID.ParameterName = "@UPLINE_USERS_ID";
                _UPLINE_USERS_ID.Value = 0;


                SqlParameter _TimeClause = new SqlParameter();
                _TimeClause.Direction = System.Data.ParameterDirection.Input;
                _TimeClause.DbType = System.Data.DbType.String;
                _TimeClause.ParameterName = "@TimeClause";
                _TimeClause.Value = TimeClause ?? (object)DBNull.Value;

                TheCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_personal_details @ACTION,@USER_NAME,@ID,@UPLINE_USERS_ID,@TimeClause", _ActionId, _UsersName, _InfoId, _UPLINE_USERS_ID, _TimeClause).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TheCount;
        }

        public List<PersonalInformationViewModel> GetPersonalInformation(int ActionId, string UserName)
        {
            List<PersonalInformationViewModel> objmodelist = new List<PersonalInformationViewModel>();
            try
            {               

                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@ACTION",
                    Value = ActionId
                });
                parameters[1] = (new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@USER_NAME",
                    Value = UserName
                });

                //objmodelist = _dbContext.ExecuteStoredProcedure<PersonalInformationViewModel>("exec sp_personal_details @ACTION,@USER_NAME", _ActionId, _UsersName).ToList();
                DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("sp_personal_details", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objmodelist = dt.ConvertDataTableToList<PersonalInformationViewModel>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<TextNotificationSettings> GetTextNotificationSettings(int ActionId, TextNotificationSettingsViewModel model)
        {
            List<TextNotificationSettings> objmodelist = new List<TextNotificationSettings>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@OptOutCatID";
                _Id.Value = model.OptOutCatID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;



                objmodelist = _dbContext.ExecuteStoredProcedure<TextNotificationSettings>("exec sp_TextNotificationSettings @ActionId,@OptOutCatID,@UsersID", _ActionId, _Id, _UsersID).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public SmsSettingModel SmsSettingInfo(int UserId)
        {
            SmsSettingModel model = new SmsSettingModel();

            string sql = "select Phone,SMSPhone,RIGHT(REPLACE(REPLACE(REPLACE( REPLACE(phone , '(', '') , '-', ''), ')', ''), ' ', ''),10) AS FormattedPhone from users where id=" + UserId;
            var dt=_dbContext.ExecuteSqlDataTable(sql,null);
            if(dt!=null && dt.Rows.Count > 0)
            {
                var phone= Convert.ToString(dt.Rows[0]["Phone"]);
                var FormattedPhone = Convert.ToString(dt.Rows[0]["FormattedPhone"]);
                model.Phone = phone;// SetPhoneNumber(phone);
                model.FormattedPhone = FormattedPhone;
            }
            return model;
        }
        public PlivoOptInOutReportModel BroadcastOptStatus(int UserId)
        {
            PlivoOptInOutReportModel p = new PlivoOptInOutReportModel();
            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@userId",
                Value = UserId
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetBroadCastOptStatus", parameter);
            if (dt != null)
            {
               var m = dt.ConvertDataTableToList<PlivoOptInOutReportModel>();
                if (m != null && m.Count > 0)
                {
                    p = m.First();
                }


            }
            return p;
        }
        protected string SetPhoneNumber(string phoneNumber)
        {
            //string phoneNumber = phoneNumber;
            //NewSMSPhone = phoneNumber;
            //txtSMSPhone.Text = phoneNumber;
            if (phoneNumber.Length == 10)
                phoneNumber = "1" + phoneNumber;
            if (!string.IsNullOrEmpty(phoneNumber))
                phoneNumber = phoneNumber.Insert(1, "(").Insert(1, " ").Insert(6, ")").Insert(7, " ").Insert(11, "-");

            return phoneNumber;
        }
        public SqlResponseBaseModel TextNotificationSettingsOptOutInsertDelete(int ActionId, TextNotificationSettingsViewModel model)
        {

            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@OptOutCatID";
                _Id.Value = model.OptOutCatID;

                SqlParameter _UsersID = new SqlParameter();
                _UsersID.Direction = System.Data.ParameterDirection.Input;
                _UsersID.DbType = System.Data.DbType.Int64;
                _UsersID.ParameterName = "@UsersID";
                _UsersID.Value = model.UsersID;



                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec sp_TextNotificationSettings @ActionId,@OptOutCatID,@UsersID", _ActionId, _Id, _UsersID).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public SqlResponseBaseModel InsertUpdateUserInformation(long ActionId, UserInformationViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UserId",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = model.UserName ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FirstName",
                    Value = model.FirstName ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@LastName",
                    Value = model.LastName ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Address",
                    Value = model.Address ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@City",
                    Value = model.City ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Country",
                    Value = model.Country ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Email",
                    Value = model.Email ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Phone",
                    Value = model.Phone ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@SMSCode",
                    Value = model.SMSCode ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@State",
                    Value = model.State ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Zip",
                    Value = model.Zip ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@CourtesyCalls",
                    Value = model.CourtesyCalls
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@WelcomeName",
                    Value = model.WelcomeName ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@WelcomeEmail",
                    Value = model.WelcomeEmail ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@WelcomePhone",
                    Value = model.WelcomePhone ?? string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@RankID",
                    Value = model.RankID
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Address1",
                    Value = model.Address1 ?? string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Address2",
                    Value = model.Address2 ?? string.Empty
                });

                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@UsersAddressesID",
                    Value = model.UsersAddressesID
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_InsertUpdateUserInformation @ActionId," +
                    "@UserId,@UserName,@FirstName,@LastName,@Address,@City,@Country,@Email,@Phone,@SMSCode,@State,@Zip,@CourtesyCalls," +
                    "@WelcomeName,@WelcomeEmail,@WelcomePhone,@RankID,@Address1,@Address2,@UsersAddressesID", parameter.ToArray()).FirstOrDefault();


            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public bool UpdateUserAddress(UserInformationViewModel model)
        {
            SqlParameter[] parameter = new SqlParameter[6];
            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = model.UserId
            };
            parameter[1] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Address",
                Value = model.Address ?? string.Empty
            };
            parameter[2] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@City",
                Value = model.City ?? string.Empty
            };
            parameter[3] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@State",
                Value = model.StateId 
            };
            parameter[4] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Zip",
                Value = model.Zip ?? string.Empty
            };
            parameter[5] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Country",
                Value = model.CountryId
            };
            var a = _dbContext.ExecuteStoredProcedureDataTable("UpdateUserAddress", parameter);
            return true;
        }
        public int GetGrandFatherRank(int UserId)
        {
            int rankid = 0;
            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = UserId
            };
            var dt = _dbContext.ExecuteStoredProcedureDataTable("CheckGrandfatherRank", parameter);
            if (dt != null && dt.Rows.Count>0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out rankid);
            }
            return rankid;
        }
        public List<ReferralUser> GetReferralNetworkTree(int ActionId, long Id)
        {
            var model = new List<ReferralUser>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UserId";
                _UserId.Value = Id;

                /* exec dbo.sp_get_referral_network_users @ActionId=1, @UserId=6 */
                model = _dbContext.ExecuteStoredProcedure<ReferralUser>("exec sp_get_referral_network_users @ActionId,@UserId", _ActionId, _UserId).ToList();

            }
            catch (Exception ex)
            {
                _ = ex;
            }
            return model;
        }

        public WebContentData GetWebContentData(int ActionId, string content)
        {
            WebContentData model = new WebContentData();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Content = new SqlParameter();
                _Content.Direction = System.Data.ParameterDirection.Input;
                _Content.DbType = System.Data.DbType.String;
                _Content.ParameterName = "@CONTENT";
                _Content.Value = content;

                model = _dbContext.ExecuteStoredProcedure<WebContentData>("exec sp_rank_history @ACTION,@CONTENT", _ActionId, _Content).FirstOrDefault();
                if (model == null)
                {
                    model = new WebContentData();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<RankPromotion> GetRankPromotion(int ActionId, long Id)
        {
            List<RankPromotion> model = new List<RankPromotion>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Content = new SqlParameter();
                _Content.Direction = System.Data.ParameterDirection.Input;
                _Content.DbType = System.Data.DbType.String;
                _Content.ParameterName = "@CONTENT";
                _Content.Value = string.Empty;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@ID";
                _UserId.Value = Id;

                model = _dbContext.ExecuteStoredProcedure<RankPromotion>("exec sp_rank_history @ACTION,@CONTENT,@ID", _ActionId, _Content, _UserId).ToList();
                if (model == null)
                {
                    model = new List<RankPromotion>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public MobileContentData GetMobileContentData(int ActionId, string content)
        {
            MobileContentData model = new MobileContentData();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Content = new SqlParameter();
                _Content.Direction = System.Data.ParameterDirection.Input;
                _Content.DbType = System.Data.DbType.String;
                _Content.ParameterName = "@CONTENT";
                _Content.Value = content;

                model = _dbContext.ExecuteStoredProcedure<MobileContentData>("exec sp_rank_history @ACTION,@CONTENT", _ActionId, _Content).FirstOrDefault();
                if (model == null)
                {
                    model = new MobileContentData();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<GenealogyList> GetGenealogyList(int ActionId, long Id, string search)
        {
            List<GenealogyList> model = new List<GenealogyList>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Search = new SqlParameter();
                _Search.Direction = System.Data.ParameterDirection.Input;
                _Search.DbType = System.Data.DbType.String;
                _Search.ParameterName = "@SEARCHKEY";
                _Search.Value = search;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                model = _dbContext.ExecuteStoredProcedure<GenealogyList>("exec sp_genealogy_search @ACTION,@SEARCHKEY,@ID", _ActionId, _Search, _Id).ToList();
                if (model == null)
                {
                    model = new List<GenealogyList>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<SilverStar> GetSilverIDList(int ActionId, long Id)
        {
            List<SilverStar> model = new List<SilverStar>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                model = _dbContext.ExecuteStoredProcedure<SilverStar>("exec sp_silver_start_report @ACTION,@ID", _ActionId, _Id).ToList();
                if (model == null)
                {
                    model = new List<SilverStar>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public long GetSilverCount(int ActionId, long Id, string type)
        {
            long nCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                SqlParameter _Type = new SqlParameter();
                _Type.Direction = System.Data.ParameterDirection.Input;
                _Type.DbType = System.Data.DbType.String;
                _Type.ParameterName = "@TYPE";
                _Type.Value = string.Empty;

                nCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_silver_start_report @ACTION,@ID,@TYPE", _ActionId, _Id, _Type).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nCount;
        }
        public bool GetSilverCheck(int ActionId, long Id, string type)
        {
            bool Check = false;
            long Count = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                SqlParameter _Type = new SqlParameter();
                _Type.Direction = System.Data.ParameterDirection.Input;
                _Type.DbType = System.Data.DbType.String;
                _Type.ParameterName = "@TYPE";
                _Type.Value = string.Empty;

                Count = _dbContext.ExecuteStoredProcedure<long>("exec sp_silver_start_report @ACTION,@ID,@TYPE", _ActionId, _Id, _Type).FirstOrDefault();

                if (Count > 0)
                {
                    Check = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Check;
        }
        public List<TeamCustomer> GetTeamCustomersIDList(int ActionId, long Id)
        {
            List<TeamCustomer> model = new List<TeamCustomer>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                model = _dbContext.ExecuteStoredProcedure<TeamCustomer>("exec sp_team_customer @ACTION,@ID", _ActionId, _Id).ToList();
                if (model == null)
                {
                    model = new List<TeamCustomer>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public long GetTeamCustomersCount(int ActionId, long Id)
        {
            long nCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                nCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_team_customer @ACTION,@ID", _ActionId, _Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nCount;
        }
        public List<LegsForMBO> GetLegsDetails(int ActionId, long Id)
        {
            List<LegsForMBO> model = new List<LegsForMBO>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                model = _dbContext.ExecuteStoredProcedure<LegsForMBO>("exec sp_team_customer @ACTION,@ID", _ActionId, _Id).ToList();
                if (model == null)
                {
                    model = new List<LegsForMBO>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public long GetMyTV(int ActionId, long Id)
        {
            long nCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                nCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_team_customer @ACTION,@ID", _ActionId, _Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nCount;
        }
        public long GetMyPersSponTV(int ActionId, long Id)
        {
            long nCount = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                nCount = _dbContext.ExecuteStoredProcedure<long>("exec sp_team_customer @ACTION,@ID", _ActionId, _Id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return nCount;
        }

        public HelpViewModel GetWebContent(int ActionId, string KeyName)
        {
            HelpViewModel objmodelist = new HelpViewModel();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _KeyName = new SqlParameter();
                _KeyName.Direction = System.Data.ParameterDirection.Input;
                _KeyName.DbType = System.Data.DbType.String;
                _KeyName.ParameterName = "@KeyName";
                _KeyName.Value = KeyName ?? string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<HelpViewModel>("exec SP_Get_WebContent @ActionId,@KeyName", _ActionId, _KeyName).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<HelpViewModel> GetWebContentKeyName(int ActionId)
        {
            List<HelpViewModel> objmodelist = new List<HelpViewModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _KeyName = new SqlParameter();
                _KeyName.Direction = System.Data.ParameterDirection.Input;
                _KeyName.DbType = System.Data.DbType.String;
                _KeyName.ParameterName = "@KeyName";
                _KeyName.Value = null ?? string.Empty;

                objmodelist = _dbContext.ExecuteStoredProcedure<HelpViewModel>("exec SP_Get_WebContent @ActionId,@KeyName", _ActionId, _KeyName).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public LeadsContentData GetLeadsContentData(int ActionId, string content)
        {
            LeadsContentData model = new LeadsContentData();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Content = new SqlParameter();
                _Content.Direction = System.Data.ParameterDirection.Input;
                _Content.DbType = System.Data.DbType.String;
                _Content.ParameterName = "@CONTENT";
                _Content.Value = content;

                model = _dbContext.ExecuteStoredProcedure<LeadsContentData>("exec sp_leads @ACTION,@CONTENT", _ActionId, _Content).FirstOrDefault();
                if (model == null)
                {
                    model = new LeadsContentData();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<Resources> GetResourcesList(int ActionId)
        {
            List<Resources> model = new List<Resources>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;


                model = _dbContext.ExecuteStoredProcedure<Resources>("exec sp_resources @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<Resources>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<FilesResources> GetFileResourcesList(int ActionId, long Id)
        {
            List<FilesResources> model = new List<FilesResources>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@ID";
                _Id.Value = Id;

                model = _dbContext.ExecuteStoredProcedure<FilesResources>("exec sp_resources @ACTION,@ID", _ActionId, _Id).ToList();
                if (model == null)
                {
                    model = new List<FilesResources>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public SqlResponseBaseModel InsertUpdateCamoaign(long ActionId, Campaign model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@USER_ID",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CAMPAIGN_NAME",
                    Value = model.CampaignName ?? (object)DBNull.Value
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.sp_ad_link @ActionId," +
                    "@USER_ID,@CAMPAIGN_NAME", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public List<SelectListItem> GetCampaignList(int ActionId, long UserId)
        {
            List<SelectListItem> objmodelist = new List<SelectListItem>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;


                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = UserId;

                objmodelist = _dbContext.ExecuteStoredProcedure<SelectListItem>("exec sp_ad_link @ActionId,@USER_ID", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<CampaignPage> GetCampaignPageList(int ActionId)
        {
            List<CampaignPage> objmodelist = new List<CampaignPage>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                objmodelist = _dbContext.ExecuteStoredProcedure<CampaignPage>("exec sp_ad_link @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public List<UsersCampaignPage> GetUserWiseCampaignPageList(int ActionId, long CampaignId)
        {
            List<UsersCampaignPage> objmodelist = new List<UsersCampaignPage>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = 0;

                SqlParameter _Name = new SqlParameter();
                _Name.Direction = System.Data.ParameterDirection.Input;
                _Name.DbType = System.Data.DbType.String;
                _Name.ParameterName = "@CAMPAIGN_NAME";
                _Name.Value = string.Empty;

                SqlParameter _CampaignId = new SqlParameter();
                _CampaignId.Direction = System.Data.ParameterDirection.Input;
                _CampaignId.DbType = System.Data.DbType.Int64;
                _CampaignId.ParameterName = "@CAMPAIGN_ID";
                _CampaignId.Value = CampaignId;

                objmodelist = _dbContext.ExecuteStoredProcedure<UsersCampaignPage>("exec sp_ad_link @ActionId,@USER_ID,@CAMPAIGN_NAME,@CAMPAIGN_ID", _ActionId, _Id, _Name, _CampaignId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public SqlResponseBaseModel InsertUpdateTagCamoaign(long ActionId, AdLinksViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@USER_ID",
                    Value = model.campaign.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CAMPAIGN_NAME",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@CAMPAIGN_ID",
                    Value = model.CampId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@LINK_ID",
                    Value = model.CampId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NAME",
                    Value = model.CampName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@LINK_PAGE_ID",
                    Value = model.PageId
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.sp_ad_link @ActionId," +
                    "@USER_ID,@CAMPAIGN_NAME,@CAMPAIGN_ID,@LINK_ID,@NAME,@LINK_PAGE_ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public PageRedirection GetPageRedirection(int ActionId, string GuidId)
        {
            PageRedirection objmodelist = new PageRedirection();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = 0;

                SqlParameter _CampaignName = new SqlParameter();
                _CampaignName.Direction = System.Data.ParameterDirection.Input;
                _CampaignName.DbType = System.Data.DbType.String;
                _CampaignName.ParameterName = "@CAMPAIGN_NAME";
                _CampaignName.Value = string.Empty;

                SqlParameter _CampaignId = new SqlParameter();
                _CampaignId.Direction = System.Data.ParameterDirection.Input;
                _CampaignId.DbType = System.Data.DbType.Int64;
                _CampaignId.ParameterName = "@CAMPAIGN_ID";
                _CampaignId.Value = 0;

                SqlParameter _LinkId = new SqlParameter();
                _LinkId.Direction = System.Data.ParameterDirection.Input;
                _LinkId.DbType = System.Data.DbType.Int64;
                _LinkId.ParameterName = "@LINK_ID";
                _LinkId.Value = 0;

                SqlParameter _Name = new SqlParameter();
                _Name.Direction = System.Data.ParameterDirection.Input;
                _Name.DbType = System.Data.DbType.String;
                _Name.ParameterName = "@NAME";
                _Name.Value = string.Empty;

                SqlParameter _LinkPageId = new SqlParameter();
                _LinkPageId.Direction = System.Data.ParameterDirection.Input;
                _LinkPageId.DbType = System.Data.DbType.Int64;
                _LinkPageId.ParameterName = "@LINK_PAGE_ID";
                _LinkPageId.Value = 0;

                SqlParameter _GuidId = new SqlParameter();
                _GuidId.Direction = System.Data.ParameterDirection.Input;
                _GuidId.DbType = System.Data.DbType.String;
                _GuidId.ParameterName = "@GUID_ID";
                _GuidId.Value = GuidId;

                objmodelist = _dbContext.ExecuteStoredProcedure<PageRedirection>("exec sp_ad_link @ActionId,@USER_ID,@CAMPAIGN_NAME,@CAMPAIGN_ID" +
                    ",@LINK_ID,@NAME,@LINK_PAGE_ID,@GUID_ID", _ActionId, _Id, _CampaignName, _CampaignId, _LinkId, _Name, _LinkPageId, _GuidId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel UpdateRedirectionHit(long ActionId, long LinkId)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@USER_ID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CAMPAIGN_NAME",
                    Value = string.Empty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@CAMPAIGN_ID",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@LINK_ID",
                    Value = LinkId
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.sp_ad_link @ActionId," +
                    "@USER_ID,@CAMPAIGN_NAME,@CAMPAIGN_ID,@LINK_ID", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public PageRedirection GetPageSearchRedirection(int ActionId, SearchCampaignURL model)
        {
            PageRedirection objmodelist = new PageRedirection();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = 0;

                SqlParameter _CampaignName = new SqlParameter();
                _CampaignName.Direction = System.Data.ParameterDirection.Input;
                _CampaignName.DbType = System.Data.DbType.String;
                _CampaignName.ParameterName = "@CAMPAIGN_NAME";
                _CampaignName.Value = string.Empty;

                SqlParameter _CampaignId = new SqlParameter();
                _CampaignId.Direction = System.Data.ParameterDirection.Input;
                _CampaignId.DbType = System.Data.DbType.Int64;
                _CampaignId.ParameterName = "@CAMPAIGN_ID";
                _CampaignId.Value = model.CampaignId;

                SqlParameter _LinkId = new SqlParameter();
                _LinkId.Direction = System.Data.ParameterDirection.Input;
                _LinkId.DbType = System.Data.DbType.Int64;
                _LinkId.ParameterName = "@LINK_ID";
                _LinkId.Value = 0;

                SqlParameter _Name = new SqlParameter();
                _Name.Direction = System.Data.ParameterDirection.Input;
                _Name.DbType = System.Data.DbType.String;
                _Name.ParameterName = "@NAME";
                _Name.Value = model.Name;

                objmodelist = _dbContext.ExecuteStoredProcedure<PageRedirection>("exec sp_ad_link @ActionId,@USER_ID,@CAMPAIGN_NAME,@CAMPAIGN_ID" +
                    ",@LINK_ID,@NAME", _ActionId, _Id, _CampaignName, _CampaignId, _LinkId, _Name).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<Member> PopulateRoleList(int ActionId)
        {
            List<Member> objmodelist = new List<Member>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                objmodelist = _dbContext.ExecuteStoredProcedure<Member>("exec sp_permisssion @ActionId", _ActionId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public List<RoleDetails> RoleDetailsList(int ActionId, string UserName)
        {
            List<RoleDetails> objmodelist = new List<RoleDetails>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _UserName = new SqlParameter();
                _UserName.Direction = System.Data.ParameterDirection.Input;
                _UserName.DbType = System.Data.DbType.String;
                _UserName.ParameterName = "@UserName";
                _UserName.Value = UserName;

                objmodelist = _dbContext.ExecuteStoredProcedure<RoleDetails>("exec sp_permisssion @ActionId,@UserName", _ActionId, _UserName).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public SqlResponseBaseModel InsertUpdateUserRole(long ActionId, DataTable dt)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@UserName",
                    Value = Convert.ToString(dt.Rows[0][2])
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    SqlDbType = System.Data.SqlDbType.Structured,
                    ParameterName = "@RoleTable",
                    Value = dt,
                    TypeName = "dbo.TT_USER_ROLE_DETAILS"
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.sp_permisssion_privillage @ActionId,@UserName," +
                    "@RoleTable", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public string GetWebPagesContent(int ActionId, string content)
        {
            string WebContent = string.Empty;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _Content = new SqlParameter();
                _Content.Direction = System.Data.ParameterDirection.Input;
                _Content.DbType = System.Data.DbType.String;
                _Content.ParameterName = "@CONTENT";
                _Content.Value = content;

                WebContent = _dbContext.ExecuteStoredProcedure<string>("exec sp_web_content @ACTION,@CONTENT", _ActionId, _Content).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return WebContent;
        }
        public List<CSJEntries> GetCSJEntriesList(int ActionId)
        {
            List<CSJEntries> model = new List<CSJEntries>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                model = _dbContext.ExecuteStoredProcedure<CSJEntries>("exec sp_report @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<CSJEntries>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<Correspondence> GetCorrespondenceList(int ActionId)
        {
            List<Correspondence> model = new List<Correspondence>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                model = _dbContext.ExecuteStoredProcedure<Correspondence>("exec sp_report @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<Correspondence>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<NewUsers> GetNewUsersList(int ActionId)
        {
            List<NewUsers> model = new List<NewUsers>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                model = _dbContext.ExecuteStoredProcedure<NewUsers>("exec sp_report @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<NewUsers>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<Emails> GetUDEmailList(int ActionId, long UserId)
        {
            List<Emails> model = new List<Emails>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UsersID";
                _UserId.Value = UserId;

                model = _dbContext.ExecuteStoredProcedure<Emails>("exec sp_report @ACTION,@UsersID", _ActionId, _UserId).ToList();
                if (model == null)
                {
                    model = new List<Emails>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<NewUserData> GetNewUserDataList()
        {
            List<NewUserData> model = new List<NewUserData>();
            try
            {
                model = _dbContext.ExecuteStoredProcedure<NewUserData>("exec sp_CK_WeeksSinceStart").ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public long GetCount(int ActionId, DateTime StartDate, DateTime EndDate, long UserId)
        {
            long val = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UsersID";
                _UserId.Value = UserId;

                SqlParameter _StartDate = new SqlParameter();
                _StartDate.Direction = System.Data.ParameterDirection.Input;
                _StartDate.DbType = System.Data.DbType.DateTime;
                _StartDate.ParameterName = "@StartDate";
                _StartDate.Value = StartDate;

                SqlParameter _EndDate = new SqlParameter();
                _EndDate.Direction = System.Data.ParameterDirection.Input;
                _EndDate.DbType = System.Data.DbType.DateTime;
                _EndDate.ParameterName = "@EndDate";
                _EndDate.Value = EndDate;

                val = _dbContext.ExecuteStoredProcedure<long>("exec sp_report @ACTION,@UsersID,@StartDate,@EndDate", _ActionId, _UserId, _StartDate, _EndDate).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return val;
        }
        public string GetItemPerOrder(int ActionId, long UserId)
        {
            string val = string.Empty;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                SqlParameter _UserId = new SqlParameter();
                _UserId.Direction = System.Data.ParameterDirection.Input;
                _UserId.DbType = System.Data.DbType.Int64;
                _UserId.ParameterName = "@UsersID";
                _UserId.Value = UserId;

                val = _dbContext.ExecuteStoredProcedure<string>("exec sp_report @ACTION,@UsersID", _ActionId, _UserId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return val;
        }
        public List<PhoneNotVerify> GetPhoneNotVerifyList(int ActionId)
        {
            List<PhoneNotVerify> model = new List<PhoneNotVerify>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                model = _dbContext.ExecuteStoredProcedure<PhoneNotVerify>("exec sp_utilities @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<PhoneNotVerify>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public List<OptOutReport> GetOptOutReportList(int ActionId)
        {
            List<OptOutReport> model = new List<OptOutReport>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;

                model = _dbContext.ExecuteStoredProcedure<OptOutReport>("exec sp_utilities @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<OptOutReport>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }
        public int IsAdminPlacementAllowed(int UserId)
        {
            int val = 0;
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@userid";
                _ActionId.Value = UserId;

                val = _dbContext.ExecuteStoredProcedure<int>("exec GetAdminUserForPlacement @userid", _ActionId).FirstOrDefault();
             
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return val;
        }

        public List<DeleteUserUtility> GetDeleteUserUtilityList(int ActionId, string Search)
        {
            List<DeleteUserUtility> model = new List<DeleteUserUtility>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;
                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@ID";
                _Id.Value = 0;

                SqlParameter _Search = new SqlParameter();
                _Search.Direction = System.Data.ParameterDirection.Input;
                _Search.DbType = System.Data.DbType.String;
                _Search.ParameterName = "@Search";
                _Search.Value = Search ?? (object)DBNull.Value;

                model = _dbContext.ExecuteStoredProcedure<DeleteUserUtility>("exec SP_DeleteUserUtility @ACTION,@ID,@Search", _ActionId, _Id, _Search).ToList();
                if (model == null)
                {
                    model = new List<DeleteUserUtility>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<IncompleteUsers> GetIncompleteUsersList(int ActionId)
        {
            List<IncompleteUsers> model = new List<IncompleteUsers>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;


                model = _dbContext.ExecuteStoredProcedure<IncompleteUsers>("exec SP_IncompleteUsers @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<IncompleteUsers>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<OpenDataBaseConnection> GetOpenDataBaseConnectionList(int ActionId)
        {
            List<OpenDataBaseConnection> model = new List<OpenDataBaseConnection>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ACTION";
                _ActionId.Value = ActionId;


                model = _dbContext.ExecuteStoredProcedure<OpenDataBaseConnection>("exec SP_OpenDataBaseConnection @ACTION", _ActionId).ToList();
                if (model == null)
                {
                    model = new List<OpenDataBaseConnection>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<MyOrders> GetMyOrdersList(int ActionId, long UserId)
        {
            List<MyOrders> objmodelist = new List<MyOrders>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = UserId;

                objmodelist = _dbContext.ExecuteStoredProcedure<MyOrders>("exec sp_orders @ActionId,@USER_ID", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public MyReferralOrders MyReferralOrders(int ActionId, int UserId)
        {
            MyReferralOrders m = new MyReferralOrders();
            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = ActionId
            });
            parameters[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@userId",
                Value = UserId
            });

            DataTable dt = _dbContext.ExecuteStoredProcedureDataTable("GetOrdersFromPersonals", parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                m.OrderList = dt.ConvertDataTableToList<MyReferralOrdersList>();
            }
            return m;
        }
        public List<OrderItemModel> GetMyOrdersDetailsList(int ActionId, long OrderId)
        {
            List<OrderItemModel> objmodelist = new List<OrderItemModel>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = 0;

                SqlParameter _OrderId = new SqlParameter();
                _OrderId.Direction = System.Data.ParameterDirection.Input;
                _OrderId.DbType = System.Data.DbType.Int64;
                _OrderId.ParameterName = "@OrderId";
                _OrderId.Value = OrderId;

                objmodelist = _dbContext.ExecuteStoredProcedure<OrderItemModel>("exec sp_orders @ActionId,@USER_ID,@OrderId", _ActionId, _Id, _OrderId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<Autoship> GetAutoshipList(int ActionId, int UserId)
        {
            List<Autoship> objmodelist = new List<Autoship>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@UserId";
                _Id.Value = UserId;

                objmodelist = _dbContext.ExecuteStoredProcedure<Autoship>("exec SP_AutoshipDetails @ActionId,@UserId", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertUpdateAutoship(long ActionId, RecurringOrderViewModel model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@StatusId",
                    Value = model.Status

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = model.AdId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = model.NickName ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NextOrderDate",
                    Value = model.NextOrderDate ?? (Object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Interval",
                    Value = model.OnceEvery
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AutoshipPeriodId",
                    Value = model.AutoshipPeriodId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@StartingOnDate",
                    Value = model.StartingOn ?? (Object)DBNull.Value
                });
                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_AutoshipDetails @ActionId,@UserId," +
                    "@StatusId,@AdId,@NickName,@NextOrderDate,@Interval,@AutoshipPeriodId,@StartingOnDate", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public RecurringOrderViewModel GetRecurringOrder(int ActionId, RecurringOrderViewModel model)
        {
            RecurringOrderViewModel objmodelist = new RecurringOrderViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@StatusId",
                    Value = model.Status

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = model.AdId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = model.NickName ?? (Object)DBNull.Value
                });
                objmodelist = _dbContext.ExecuteStoredProcedure<RecurringOrderViewModel>("exec dbo.SP_AutoshipDetails @ActionId,@UserId," +
                    "@StatusId,@AdId,@NickName", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<ProductListFromNopCom> GetProductListFromNopCom(int ActionId)
        {
            List<ProductListFromNopCom> objmodelist = new List<ProductListFromNopCom>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@UserId";
                _Id.Value = 0;

                objmodelist = _dbContext.ExecuteStoredProcedure<ProductListFromNopCom>("exec SP_AutoshipDetails @ActionId,@UserId", _ActionId, _Id).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertUpdateAutoshipDirectivesItems(long ActionId, Int32 AdId, ProductListFromNopCom model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = AdId

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@ItemsID",
                    Value = model.ProductId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Qty",
                    Value = model.Qty
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Decimal,
                    ParameterName = "@Price",
                    Value = model.Price
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@Taxable",
                    Value = model.Taxable
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_AutoshipDirectivesItems @ActionId," +
                    "@Id,@AdId,@ItemsID,@Qty,@Price,@Taxable", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public List<ProductListFoxx> GetProductListFoxx(int ActionId, int AdId)
        {
            List<ProductListFoxx> objmodelist = new List<ProductListFoxx>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int32;
                _Id.ParameterName = "@Id";
                _Id.Value = 0;

                SqlParameter _AdId = new SqlParameter();
                _AdId.Direction = System.Data.ParameterDirection.Input;
                _AdId.DbType = System.Data.DbType.Int32;
                _AdId.ParameterName = "@AdId";
                _AdId.Value = AdId;

                objmodelist = _dbContext.ExecuteStoredProcedure<ProductListFoxx>("exec SP_AutoshipDirectivesItems @ActionId,@Id,@AdID", _ActionId, _Id, _AdId).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public SqlResponseBaseModel InsertUpdateAutoshipPaymentMethod(long ActionId, Int32 AdId, RecurringPaymentMethod model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = AdId

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = model.NickName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FirstName",
                    Value = model.FirstName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@LastName",
                    Value = model.LastName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CompanyName",
                    Value = model.CompanyName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Address",
                    Value = model.Address ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@City",
                    Value = model.City ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@State",
                    Value = model.State ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Zip",
                    Value = model.Zip ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Phone",
                    Value = model.Phone ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Email",
                    Value = model.Email ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CCNo",
                    Value = model.CCNo ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ExpDate",
                    Value = model.ExpDate ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Type",
                    Value = model.Type ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@RoutingNumber",
                    Value = model.RoutingNumber ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CVV",
                    Value = model.CVV ?? (object)DBNull.Value
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_AutoshipAddress @ActionId," +
                    "@Id,@AdId,@NickName,@FirstName,@LastName,@CompanyName,@Address,@City,@State,@Zip,@Phone,@Email,@CCNo,@ExpDate,@Type,@RoutingNumber,@CVV", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }
        public SqlResponseBaseModel InsertUpdateAutoshipShipingAddress(long ActionId, Int32 AdId, RecurringShippingAddress model)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = AdId

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = model.NickName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@FirstName",
                    Value = model.FirstName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@LastName",
                    Value = model.LastName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CompanyName",
                    Value = model.CompanyName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Address",
                    Value = model.Address ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@City",
                    Value = model.City ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@State",
                    Value = model.State ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Zip",
                    Value = model.Zip ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Phone",
                    Value = model.Phone ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Email",
                    Value = model.Email ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CCNo",
                    Value = null ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@ExpDate",
                    Value = null ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Type",
                    Value = null ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@RoutingNumber",
                    Value = null ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CVV",
                    Value = null ?? (object)DBNull.Value
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_AutoshipAddress @ActionId," +
                    "@Id,@AdId,@NickName,@FirstName,@LastName,@CompanyName,@Address,@City,@State,@Zip,@Phone,@Email,@CCNo,@ExpDate,@Type,@RoutingNumber,@CVV", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public RecurringPaymentMethod GetAutoshipPaymentMethod(long ActionId, Int32 AdId, RecurringPaymentMethod model)
        {
            RecurringPaymentMethod _baseModel = new RecurringPaymentMethod();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Id",
                    Value = model.Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = AdId

                });


                _baseModel = _dbContext.ExecuteStoredProcedure<RecurringPaymentMethod>("exec dbo.SP_AutoshipAddress @ActionId," +
                    "@Id,@AdId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public SqlResponseBaseModel InsertUpdateAnetPaymentShipping(long ActionId, Int32 Id, string CustomerProfileID, string CustomerPaymentID, string NickName, Boolean DefaultPaymentAddress, string CustomerAddressId, Boolean DefaultShippingAddress, Int32 UserId, string StripeCustomerId, string StripeAddressId)
        {
            SqlResponseBaseModel _baseModel = new SqlResponseBaseModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Id",
                    Value = Id
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CustomerProfileID",
                    Value = CustomerProfileID ?? (object)DBNull.Value

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CustomerPaymentID",
                    Value = CustomerPaymentID ?? (object)DBNull.Value

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = NickName ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@DefaultPaymentAddress",
                    Value = DefaultPaymentAddress
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CustomerAddressId",
                    Value = CustomerAddressId ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@DefaultShippingAddress",
                    Value = DefaultShippingAddress
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Stripe_CustomerId",
                    Value = StripeCustomerId ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@Stripe_AddressId",
                    Value = StripeAddressId ?? (object)DBNull.Value
                });

                _baseModel = _dbContext.ExecuteStoredProcedure<SqlResponseBaseModel>("exec dbo.SP_AnetPaymentShipping @ActionId," +
                    "@Id,@CustomerProfileID,@CustomerPaymentID,@NickName,@DefaultShippingAddress,@CustomerAddressId,@DefaultShippingAddress,@UserId,@Stripe_CustomerId,@Stripe_AddressId", parameter.ToArray()).FirstOrDefault();

            }
            catch (Exception ex)
            {
                //_baseModel.ErrorCode = (long)ex.GetExceptionCode();
            }
            return _baseModel;
        }

        public RecurringOrderViewModel GetCustomerProfileId(int ActionId, RecurringOrderViewModel model)
        {
            RecurringOrderViewModel objmodelist = new RecurringOrderViewModel();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = model.UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@StatusId",
                    Value = model.Status

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@AdId",
                    Value = model.AdId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = model.NickName ?? (Object)DBNull.Value
                });
                objmodelist = _dbContext.ExecuteStoredProcedure<RecurringOrderViewModel>("exec dbo.SP_AutoshipDetails @ActionId,@UserId," +
                    "@StatusId,@AdId,@NickName", parameter.ToArray()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public IEnumerable<SelectListItem> GetAllDropdownlistForPaymentAddressById(long ActionId, string CustomerProfileID)
        {
            List<SelectListItem> model = new List<SelectListItem>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@Id",
                    Value = 0
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CustomerProfileID",
                    Value = CustomerProfileID ?? (object)DBNull.Value

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CustomerPaymentID",
                    Value = null ?? (object)DBNull.Value

                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@NickName",
                    Value = null ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@DefaultPaymentAddress",
                    Value = false
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.String,
                    ParameterName = "@CustomerAddressId",
                    Value = null ?? (object)DBNull.Value
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Boolean,
                    ParameterName = "@DefaultShippingAddress",
                    Value = false
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = 0
                });


                model = _dbContext.ExecuteStoredProcedure<SelectListItem>("exec dbo.SP_AnetPaymentShipping @ActionId," +
                    "@Id,@CustomerProfileID,@CustomerPaymentID,@NickName,@DefaultShippingAddress,@CustomerAddressId,@DefaultShippingAddress,@UserId", parameter.ToArray()).ToList();




            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public List<MyOrders> GetOrdersList(int ActionId, long Id, string SearchBy)
        {
            List<MyOrders> objmodelist = new List<MyOrders>();
            try
            {
                SqlParameter _ActionId = new SqlParameter();
                _ActionId.Direction = System.Data.ParameterDirection.Input;
                _ActionId.DbType = System.Data.DbType.Int32;
                _ActionId.ParameterName = "@ActionId";
                _ActionId.Value = ActionId;

                SqlParameter _Id = new SqlParameter();
                _Id.Direction = System.Data.ParameterDirection.Input;
                _Id.DbType = System.Data.DbType.Int64;
                _Id.ParameterName = "@USER_ID";
                _Id.Value = Id;


                SqlParameter _OrderId = new SqlParameter();
                _OrderId.Direction = System.Data.ParameterDirection.Input;
                _OrderId.DbType = System.Data.DbType.Int64;
                _OrderId.ParameterName = "@OrderId";
                _OrderId.Value = 0;
                SqlParameter _SearchBy = new SqlParameter();
                _SearchBy.Direction = System.Data.ParameterDirection.Input;
                _SearchBy.DbType = System.Data.DbType.String;
                _SearchBy.ParameterName = "@SearchBy";
                _SearchBy.Value = SearchBy ?? (object)DBNull.Value;

                objmodelist = _dbContext.ExecuteStoredProcedure<MyOrders>("exec sp_orders @ActionId,@USER_ID,@OrderId,@SearchBy", _ActionId, _Id, _OrderId, _SearchBy).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }

        public List<RanksDataUpdateViewModel> GetRanksData()
        {
            List<RanksDataUpdateViewModel> _ranks = new List<RanksDataUpdateViewModel>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@id",
                    Value = 1
                });

                _ranks = _dbContext.ExecuteStoredProcedure<RanksDataUpdateViewModel>("exec GetRanksData @id", parameter.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _ranks;
        }

        public bool UpdateRabksData(List<RanksDataUpdateViewModel> model)
        {
            bool status = false;
            string sql = string.Empty;
            foreach (var item in model)
            {
                sql += "update Ranks set CompareCountLegs =" + item.CompareCountLegs + ", PerLegCappedCustomerCount=" + item.PerLegCappedCustomerCount + ",CappedCustomerCount=" + item.CappedCustomerCount + ",LegsWithRank3Rank4=" + item.LegsWithRank3Rank4 + ",CheckRankLegTotal=" + item.CheckRankLegTotal + ",NoOfRankingLeg=" + item.NoOfRankingLeg + ",Display1=" + item.Display1 + ",Display2=" + item.Display2 + ",Display3=" + item.Display3 + ",Display4=" + item.Display4 + " where id=" + item.ID + ";";
            }
            if (!string.IsNullOrEmpty(sql))
            {
                status = _dbContext.ExecuteInsertQuery(sql, null);
            }
            return status;
        }

        public List<AdminOrders> GetAdminOrders(int ActionId, long Id)
        {
            List<AdminOrders> _lst = new List<AdminOrders>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@id",
                    Value = Id
                });

                _lst = _dbContext.ExecuteStoredProcedure<AdminOrders>("exec GetAdminOrders @ActionId,@id", parameter.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _lst;
        }

        public RankManagementViewModel RankManagementReport(int UserId)
        {
            RankManagementViewModel model = new RankManagementViewModel();
            model.myRankData = new MyRankReportViewModel();
            model.nextRanks = new List<NextRanksViewModel>();

            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = 1
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userID",
                    Value = UserId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@rankID",
                    Value = 0
                });

                MyRankReportViewModel myRank = _dbContext.ExecuteStoredProcedure<MyRankReportViewModel>("exec Sp_RankManagementReport @actionId,@userID,@rankID", parameter.ToArray()).FirstOrDefault();
                if (myRank != null)
                {
                    model.myRankData = myRank;
                }

                List<object> parameter1 = new List<object>();
                parameter1.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = 2
                });
                parameter1.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userID",
                    Value = UserId
                });
                parameter1.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@rankID",
                    Value = 0
                });

                List<RanksNameViewModel> rankNames = _dbContext.ExecuteStoredProcedure<RanksNameViewModel>("exec Sp_RankManagementReport @actionId,@userID,@rankID", parameter1.ToArray()).ToList();
                if (rankNames != null)
                {
                    model.ranksNames = rankNames;
                }

                List<object> parameter2 = new List<object>();
                parameter2.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = 3
                });
                parameter2.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userID",
                    Value = UserId
                });

                parameter2.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@rankID",
                    Value = 0
                });

                List<NextRanksViewModel> nextRanks = _dbContext.ExecuteStoredProcedure<NextRanksViewModel>("exec Sp_RankManagementReport @actionId,@userID,@rankID", parameter2.ToArray()).ToList();
                if (nextRanks != null)
                {
                    model.nextRanks = nextRanks;
                }

                List<object> parameter3 = new List<object>();
                parameter3.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = 4
                });
                parameter3.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userID",
                    Value = UserId
                });
                parameter3.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@rankID",
                    Value = 3
                });

                List<RanksNameViewModel> silver_1star = _dbContext.ExecuteStoredProcedure<RanksNameViewModel>("exec Sp_RankManagementReport @actionId,@userID,@rankID", parameter3.ToArray()).ToList();
                if (silver_1star != null)
                {
                    model.Silver_1Star = silver_1star;
                }

                List<object> parameter4 = new List<object>();
                parameter4.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = 5
                });
                parameter4.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userID",
                    Value = UserId
                });
                parameter4.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@rankID",
                    Value = 4
                });

                List<RanksNameViewModel> silver_2star = _dbContext.ExecuteStoredProcedure<RanksNameViewModel>("exec Sp_RankManagementReport @actionId,@userID,@rankID", parameter4.ToArray()).ToList();
                if (silver_2star != null)
                {
                    model.Silver_2star = silver_2star;
                }

                List<object> parameter5 = new List<object>();
                parameter5.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@actionId",
                    Value = 6
                });
                parameter5.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@userID",
                    Value = UserId
                });
                parameter5.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@rankID",
                    Value = 3
                });

                List<RanksNameViewModel> silver_1_2star = _dbContext.ExecuteStoredProcedure<RanksNameViewModel>("exec Sp_RankManagementReport @actionId,@userID,@rankID", parameter5.ToArray()).ToList();
                if (silver_1_2star != null)
                {
                    model.Silver_1_2_Star = silver_1_2star;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        public int HasAnAutoShipOrder(int UserId)
        {
            int counter = 0;
            SqlParameter[] parameter = new SqlParameter[1];

            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@userId",
                Value = UserId
            };

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_GetUserRecurringOrderCount", parameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                string count = Convert.ToString(dt.Rows[0][0]);
                int.TryParse(count, out counter);
            }

            return counter;
        }
        public bool HasQualifyingOrderForRoleChange(int UserId)
        {
            bool status = false;
            string sql = "SELECT count(*)  FROM UsersQualifyingOrders where usersid =" + UserId + " and active=1";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                var val = Convert.ToString(dt.Rows[0][0]);
                int.TryParse(val, out int counter);
                status = counter > 0;
            }
            return status;
        }
        public CurrentRoleModel GetCurrentRole(int ActionId, int UserId, Guid? CurrentRole, Guid? NewRole)
        {
            CurrentRoleModel model = null;
            SqlParameter[] parameter = new SqlParameter[4];

            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@actionid",
                Value = ActionId
            };
            parameter[1] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@userId",
                Value = UserId
            };
            parameter[2] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Guid,
                ParameterName = "@currentRoleId",
                Value = CurrentRole == null ? (Object)DBNull.Value : CurrentRole
            };
            parameter[3] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Guid,
                ParameterName = "@newRoleId",
                Value = NewRole == null ? (Object)DBNull.Value : NewRole
            };

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_ChangeUserRole", parameter);
            if (dt != null)
            {
                model = new CurrentRoleModel();
                string id = Convert.ToString(dt.Rows[0][0]);
                if (ActionId == 1 || ActionId == 2)
                {
                    model.RoleName = Convert.ToString(dt.Rows[0][1]);
                    model.Description = Convert.ToString(dt.Rows[0]["Description"]);
                    Guid roleid = Guid.Empty;
                    Guid.TryParse(id, out roleid);
                    model.RoleId = roleid;
                }
                else
                {
                    int.TryParse(id, out int code);
                    model.ResponsCode = code;
                }

            }

            return model;
        }
        public List<CurrentRoleModel> GetRoleList(int ActionId, int UserId)
        {
            List<CurrentRoleModel> model = new List<CurrentRoleModel>();
            SqlParameter[] parameter = new SqlParameter[2];

            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@actionid",
                Value = ActionId
            };
            parameter[1] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@userId",
                Value = UserId
            };

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_ChangeUserRole", parameter);
            if (dt != null)
            {
                foreach (DataRow item in dt.Rows)
                {
                    string id = Convert.ToString(item[0]);
                    string RoleName = Convert.ToString(item[1]);
                    string RoleDescription = Convert.ToString(item[2]);
                    Guid roleid;
                    Guid.TryParse(id, out roleid);

                    model.Add(new CurrentRoleModel
                    {
                        RoleId = roleid,
                        RoleName = RoleName,
                        Description = RoleDescription
                    });
                }

            }

            return model;
        }

        public List<RecurringOrderHistory> GetRecurringOrderHistory(int ActionId, int UserId)
        {
            List<RecurringOrderHistory> objmodelist = new List<RecurringOrderHistory>();
            try
            {
                List<object> parameter = new List<object>();
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int64,
                    ParameterName = "@ActionId",
                    Value = ActionId
                });
                parameter.Add(new SqlParameter
                {
                    Direction = System.Data.ParameterDirection.Input,
                    DbType = System.Data.DbType.Int32,
                    ParameterName = "@UserId",
                    Value = UserId
                });

                objmodelist = _dbContext.ExecuteStoredProcedure<RecurringOrderHistory>("exec dbo.SP_AutoshipDetails @ActionId,@UserId"
                    , parameter.ToArray()).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objmodelist;
        }
        public bool SavePaymentMethod(PaymentMethodsViewModel model)
        {
            string sql = string.Empty;
            if (model.Id == 0)
            {
                sql = "INSERT INTO CommissionPaymentTypes([Name],[Field1],[Field1Required] ,[Field2],[Field2Required],[Field3],[Field3Required],[Field4],[Field4Required],[Field5],[Field5Required],[Field6],[Field6Required],[Active]) values(@Name,@Field1,@Field1Required ,@Field2,@Field2Required,@Field3,@Field3Required,@Field4,@Field4Required,@Field5,@Field5Required,@Field6,@Field6Required,@Active)";
            }
            else
            {
                sql = "UPDATE CommissionPaymentTypes SET Name=@Name, Field1=@Field1, Field1Required=@Field1Required,Field2=@Field2,Field2Required=@Field2Required,Field3=@Field3,Field3Required=@Field3Required,Field4=@Field4,Field4Required=@Field4Required,Field5=@Field5,Field5Required=@Field5Required,Field6=@Field6,Field6Required=@Field6Required WHERE ID =" + model.Id;
            }
            SqlParameter[] parameter = new SqlParameter[14];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Name",
                Value = model.paymentName
            });

            parameter[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field1",
                Value = model.tb_field1 ?? (Object)DBNull.Value
            });
            parameter[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Field1Required",
                Value = model.field1_req
            });
            parameter[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field2",
                Value = model.tb_field2 ?? (Object)DBNull.Value
            });
            parameter[4] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Field2Required",
                Value = model.field2_req
            });
            parameter[5] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field3",
                Value = model.tb_field3 ?? (Object)DBNull.Value
            });
            parameter[6] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Field3Required",
                Value = model.field3_req
            });
            parameter[7] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field4",
                Value = model.tb_field4 ?? (Object)DBNull.Value
            });
            parameter[8] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Field4Required",
                Value = model.field4_req
            });
            parameter[9] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field5",
                Value = model.tb_field5 ?? (Object)DBNull.Value
            });
            parameter[10] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Field5Required",
                Value = model.field5_req
            });
            parameter[11] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field6",
                Value = model.tb_field6 ?? (Object)DBNull.Value
            });
            parameter[12] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Field6Required",
                Value = model.field6_req
            });
            parameter[13] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Active",
                Value = true
            });


            var res = _dbContext.ExecuteInsertQuery(sql, parameter.ToArray());

            return true;
        }

        public bool UserQualModel(UserQualModel model)
        {
            int id = 0;
            string sql = "select top 1 * from adminsettings order by id desc";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if(dt!=null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out id);
            }
            string saveSql = "";
            if (id == 0)
            {
                saveSql = "insert into adminsettings(expiryindays,UserQualOrderExpiryInDays) values (" + model.Days+","+model.UserQualOrderExpiryInDays+")";
            }
            else
            {
                saveSql = "update adminsettings set expiryindays="+model.Days+ ",UserQualOrderExpiryInDays="+model.UserQualOrderExpiryInDays+" where id=" + id;
            }
            var res = _dbContext.ExecuteInsertQuery(saveSql);
            return true;
        }
        public bool SavePlivoSMSResponse(PlivoResponseModel m,DataTable MessageUidTable,DataTable InvalidNumberTable)
        {
          //  string sql = "INSERT INTO PlivoSmsResponse (ApiId,[Message],MessageUuid,StatusCode,Username,InvalidNumber,InputJson,CreatedDate) VALUES (@ApiId,@Message,@MessageUuid,@StatusCode,@Username,@InvalidNumber,@InputJson,@CreatedDate)";
            SqlParameter[] parameter = new SqlParameter[13];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@ApiId",
                Value = m.ApiId
            });

            parameter[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Message",
                Value = m.Message
            });
            parameter[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MessageUuid",
                Value = m.MessageUuid
            });
            parameter[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@StatusCode",
                Value = m.StatusCode
            });
            parameter[4] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Username",
                Value = m.Username ?? (Object)DBNull.Value
            });
            parameter[5] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@InvalidNumber",
                Value = m.InvalidNumber ?? (Object)DBNull.Value
            });
            parameter[6] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@InputJson",
                Value = m.InputJson
            });
            parameter[7] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@SmsContent",
                Value = m.SmsContent
            });
            parameter[8] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@TotalCount",
                Value = m.TotalCount
            });
            parameter[9] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@IsBroadCast",
                Value = m.IsBroadcast??false
            });
            parameter[10] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@statusNo",
                Value = m.MaxStatus??0
            });
            parameter[11] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Structured,
                ParameterName = "@uidList",
                Value =MessageUidTable
            });
            parameter[12] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                SqlDbType = SqlDbType.Structured,
                ParameterName = "@invalidList",
                Value = InvalidNumberTable
            });

            //var res = _dbContext.ExecuteInsertQuery(sql, parameter.ToArray());
            //var r = _dbContext.ExecuteSqlDataTable("SavePlivoSmsResponse", parameter);
            var a = _dbContext.ExecuteStoredProcedureDataTable("SavePlivoSmsResponse",parameter);
            return true;
        }
        public void SavePlivoResponseLogs(PlivoLogsModel m)
        {
            SqlParameter[] parameter = new SqlParameter[9];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MessageUUID",
                Value = m.MessageUUid
            });

            parameter[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@FromNumber",
                Value = m.From
            });
            parameter[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@ToNumber",
                Value = m.To
            });
            parameter[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@SmsStatus",
                Value = m.Status
            });
            parameter[4] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MessageJson",
                Value = m.MessageJson
            });
            parameter[5] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MessageTime",
                Value = m.MessageTime ?? (Object)DBNull.Value
            });
            parameter[6] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@QueuedTime",
                Value = m.QueuedTime ?? (Object)DBNull.Value
            });
            parameter[7] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@SentTime",
                Value = m.SentTime ?? (Object)DBNull.Value
            });
            parameter[8] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@DeliveryReportTime",
                Value = m.DeliveryReportTime ?? (Object)DBNull.Value
            });
        
            var a = _dbContext.ExecuteStoredProcedureDataTable("SavePlivoSmsResponseLogs", parameter);
        }
        public void SavePlivoOptInOut(PlivoOptInOutModel m)
        {
            SqlParameter[] parameter = new SqlParameter[6];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Phone",
                Value = m.To
            });

            parameter[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@FromNumber",
                Value = m.From
            });
            parameter[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MessageIntent",
                Value = m.MessageIntent
            });
            parameter[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@MessageUUID",
                Value = m.MessageUUid
            });
            parameter[4] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@SmsText",
                Value = m.SmsText
            });
            parameter[5] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@optType",
                Value = m.OptType
            });
           
            var a = _dbContext.ExecuteStoredProcedureDataTable("SaveSmsOptInOut", parameter);
        }
        public BroadcastViewModel GetUsersForSMSBroadcasting(PlivoBroadCastInputModel m)
        {
            BroadcastViewModel model = new BroadcastViewModel();
            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@filterType",
                Value = m.FilterType
            });          

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetUsersForSMSBroadCast", parameter);
            if (dt != null)
            {
                model.userList = dt.ConvertDataTableToList<BroadCastUsersModel>();
            }
            return model;
        }
        public List<ApiIdModel> GetSmsApiI ()
        {
            List<ApiIdModel> m = new List<ApiIdModel>();
            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = 1
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetAllSmsDates", parameter);
            if (dt != null)
            {
                m = dt.ConvertDataTableToList<ApiIdModel>();
            }
            return m;
        }
        public SmsReportDashboardModel GetSmsReport(int id)
        {
            SmsReportDashboardModel m = new SmsReportDashboardModel();
        
            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@id",
                Value = id
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetSmsReport", parameter);
            if (dt != null)
            {
               var list = dt.ConvertDataTableToList<SmsReportModel>();
                if (list != null)
                {
                    m.list = list.First();
                }
                
            }
            return m;
        }
        public List<PlivoOptInOutReportModel> GetOptSmsReport(int id)
        {
            List<PlivoOptInOutReportModel> m = new List<PlivoOptInOutReportModel>();

            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@actionId",
                Value = id
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetPlivoOptInOut", parameter);
            if (dt != null)
            {
                m = dt.ConvertDataTableToList<PlivoOptInOutReportModel>();
                

            }
            return m;
        }
        public UserQualModel GetUserQual()
        {
            UserQualModel m = new UserQualModel();
            string sql = "select top 1 * from adminsettings order by id desc";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0]["expiryindays"]), out int id);
                int.TryParse(Convert.ToString(dt.Rows[0]["UserQualOrderExpiryInDays"]), out int UserQualOrderExpiryInDays);
                m.Days = id;
                m.UserQualOrderExpiryInDays = UserQualOrderExpiryInDays;
            }
            return m;
        }
        public bool IsOptInOut(string Phone,string OptType)
        {
            bool status = false;

            string sql = "select count(*) from PlivoSmsOptInOut where active=1 and MessageIntent ='" + OptType + "' and right(FromNumber,10)=right('" + Phone + "',10)";
            var dt = _dbContext.ExecuteSqlDataTable(sql,null);
            if(dt!=null && dt.Rows.Count > 0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out int count);
                status = count > 0;
            }
            return status;
        }
        public string GetUsersFromPhone(string Phone)
        {
            string names = string.Empty;
            //GetUserNamesFromPhone
            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@phone",
                Value = Phone
            };
            var dt = _dbContext.ExecuteStoredProcedureDataTable("GetUserNamesFromPhone", parameter);
            if (dt != null)
            {
                var list = dt.AsEnumerable().Select(r => r["Usernames"].ToString());
                names = string.Join(",  ", list);
            }
            return names;
        }
        public List<PaymentMethodsViewModel> GetPaymentMethods(int Userid)
        {
            List<PaymentMethodsViewModel> payments = new List<PaymentMethodsViewModel>();

            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@id",
                Value = Userid
            };

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_GetAllPaymentMethods", parameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    PaymentMethodsViewModel p = new PaymentMethodsViewModel();
                    int.TryParse(Convert.ToString(item["Id"]), out int id);
                    p.Id = id;
                    p.paymentName = Convert.ToString(item["Name"]);
                    p.tb_field1 = Convert.ToString(item["Field1"]);
                    p.tb_field2 = Convert.ToString(item["Field2"]);
                    p.tb_field3 = Convert.ToString(item["Field3"]);
                    p.tb_field4 = Convert.ToString(item["Field4"]);
                    p.tb_field5 = Convert.ToString(item["Field5"]);
                    p.tb_field6 = Convert.ToString(item["Field6"]);
                    p.field1_req = Convert.ToBoolean(item["Field1Required"]);
                    p.field2_req = Convert.ToBoolean(item["Field2Required"]);
                    p.field3_req = Convert.ToBoolean(item["Field3Required"]);
                    p.field4_req = Convert.ToBoolean(item["Field4Required"]);
                    p.field5_req = Convert.ToBoolean(item["Field5Required"]);
                    p.field6_req = Convert.ToBoolean(item["Field6Required"]);
                    p.Active = Convert.ToBoolean(item["Active"]);

                    payments.Add(p);
                }
            }

            return payments;
        }
        public List<PaymentMethodsViewModel> GetUserPaymentMethods(int UserId)
        {
            List<PaymentMethodsViewModel> payments = new List<PaymentMethodsViewModel>();

            SqlParameter[] parameter = new SqlParameter[1];
            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@UserId",
                Value = UserId
            };

            var dt = _dbContext.ExecuteStoredProcedureDataTable("Sp_GetAllUserPaymentMethods", parameter);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    PaymentMethodsViewModel p = new PaymentMethodsViewModel();
                    int.TryParse(Convert.ToString(item["Id"]), out int id);
                    p.Id = id;
                    p.paymentName = Convert.ToString(item["Name"]);
                    p.tb_field1 = Convert.ToString(item["Field1"]);
                    p.tb_field2 = Convert.ToString(item["Field2"]);
                    p.tb_field3 = Convert.ToString(item["Field3"]);
                    p.tb_field4 = Convert.ToString(item["Field4"]);
                    p.tb_field5 = Convert.ToString(item["Field5"]);
                    p.tb_field6 = Convert.ToString(item["Field6"]);
                    p.field1_req = Convert.ToBoolean(item["Field1Required"]);
                    p.field2_req = Convert.ToBoolean(item["Field2Required"]);
                    p.field3_req = Convert.ToBoolean(item["Field3Required"]);
                    p.field4_req = Convert.ToBoolean(item["Field4Required"]);
                    p.field5_req = Convert.ToBoolean(item["Field5Required"]);
                    p.field6_req = Convert.ToBoolean(item["Field6Required"]);
                    p.Active = Convert.ToBoolean(item["Active"]);

                    payments.Add(p);
                }
            }

            return payments;
        }
        public bool DeletePaymentMethod(int Id)
        {
            string sql = "update CommissionPaymentTypes set active=0 where id=" + Id;
            var res = _dbContext.ExecuteInsertQuery(sql, null);
            return true;
        }
        public List<UserPaymentMethodListModel> GetUserPaymentMethods(int UserId, int UserProfileId)
        {
            List<UserPaymentMethodListModel> models = new List<UserPaymentMethodListModel>();
            SqlParameter[] parameter = new SqlParameter[2];
            parameter[0] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@UserId",
                Value = UserId
            };
            parameter[1] = new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int64,
                ParameterName = "@methodId",
                Value = UserProfileId
            };

            var dt = _dbContext.ExecuteStoredProcedureDataTable("SP_GetUserPaymentMethods", parameter);
            if(dt!=null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    UserPaymentMethodListModel p = new UserPaymentMethodListModel();
                    int.TryParse(Convert.ToString(item["UserProfileId"]), out int UserProfileIdVal);
                    int.TryParse(Convert.ToString(item["PaymentMethodId"]), out int PaymentMethodId);

                    p.UserProfileId = UserProfileIdVal;
                    p.PaymentMethodId = PaymentMethodId;
                    p.Name = Convert.ToString(item["Name"]);
                    p.Field1 = Convert.ToString(item["Field1"]);
                    p.Field2 = Convert.ToString(item["Field2"]);
                    p.Field3 = Convert.ToString(item["Field3"]);
                    p.Field4 = Convert.ToString(item["Field4"]);
                    p.Field5 = Convert.ToString(item["Field5"]);
                    p.Field6 = Convert.ToString(item["Field6"]);
                    p.Active= Convert.ToBoolean(Convert.ToString( item["Active"]));
                    p.Verified = Convert.ToBoolean(item["Verified"]);

                    models.Add(p);
                }
            }

            return models;
        }
        public bool SavePaymentMethod(int UserId, UserPaymentMethod model)
        {
            string sql = string.Empty;
            if (model.UserProfileId == 0)
            {
                sql = "INSERT INTO UserCommissionProfiles(UserId, PaymentMethodId, Field1, Field2, Field3, Field4, Field5, Field6, Active, Verified) VALUES ( @UserId  , @PaymentMethodId  , @Field1  , @Field2  , @Field3  , @Field4  , @Field5  , @Field6  , @Active  , @Verified )";
            }
            else
            {
                sql = "UPDATE UserCommissionProfiles SET UserId=@UserId, PaymentMethodId=@PaymentMethodId, Field1=@Field1, Field2=@Field2, Field3=@Field3, Field4=@Field4, Field5=@Field5, Field6=@Field6, Active=@Active, Verified=@Verified where id= " + model.UserProfileId;
            }
            SqlParameter[] parameter = new SqlParameter[10];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = UserId
            });

            parameter[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@PaymentMethodId",
                Value = model.PaymentMethodId
            });
            parameter[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field1",
                Value = model.tb_field1 ?? (Object)DBNull.Value
            });
            parameter[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field2",
                Value = model.tb_field2 ?? (Object)DBNull.Value
            });
            parameter[4] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field3",
                Value = model.tb_field3 ?? (Object)DBNull.Value
            });
            parameter[5] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field4",
                Value = model.tb_field4 ?? (Object)DBNull.Value
            });
            parameter[6] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field5",
                Value = model.tb_field5 ?? (Object)DBNull.Value
            });
            parameter[7] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@Field6",
                Value = model.tb_field6 ?? (Object)DBNull.Value
            });
            parameter[8] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Active",
                Value = true
            });
            parameter[9] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Boolean,
                ParameterName = "@Verified",
                Value = true
            });

            var res = _dbContext.ExecuteInsertQuery(sql, parameter);

            return true;
        }
        public List<string> GetAllPlivoNumbers()
        {
            List<string> list = new List<string>();
            string sql = "SELECT DEFAULTNUMBER FROM plivosetup ORDER BY SENDCOUNT ASC";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null)
            {
                foreach (DataRow item in dt.Rows)
                {
                    string phone = Convert.ToString(item["DEFAULTNUMBER"]);
                    list.Add(phone);
                }
            }
            return list;
        }
        public int MaxBroadCastNo()
        {
            int no = 1;
            string sql = "select max(isnull(statusno,0))+1 as LastStausNo from PlivoSmsResponse";
            var dt = _dbContext.ExecuteSqlDataTable(sql, null);
            if (dt != null && dt.Rows.Count>0)
            {
                int.TryParse(Convert.ToString(dt.Rows[0][0]), out no);
            }
            return no;
        }
        public void LogException(int UserId, Exception ex)
        {
            SqlParameter[] parameter = new SqlParameter[4];
            parameter[0] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.Int32,
                ParameterName = "@UserId",
                Value = UserId 
            });

            parameter[1] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@msg",
                Value = ex.Message ?? (Object)DBNull.Value
            });
            parameter[2] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@innerMsg",
                Value =( ex.InnerException != null ? ex.InnerException.Message : ex.Message)?? (Object)DBNull.Value
            });
            parameter[3] = (new SqlParameter
            {
                Direction = System.Data.ParameterDirection.Input,
                DbType = System.Data.DbType.String,
                ParameterName = "@stacktrace",
                Value = ex.StackTrace ?? (Object)DBNull.Value
            });

            var dt = _dbContext.ExecuteStoredProcedureDataTable("LogException", parameter.ToArray());

        }
    }
}
