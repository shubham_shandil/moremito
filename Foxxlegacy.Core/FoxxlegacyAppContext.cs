﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxxlegacy.Core
{
   public class FoxxlegacyAppContext : DbContext, IDbContext
    {

        #region CTor
        public FoxxlegacyAppContext()
            : base("DefaultConnection")
        {

        }


        #endregion

        #region All DB SET
       // public DbSet<DomainCompany> Company { get; set; }
       


        #endregion


        #region Model Builders

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
           // modelBuilder.Configurations.Add(new DomainCompanyMapping());
            
        }

        #endregion

        #region Special Methods
        public IList<T> ExecuteStoredProcedure<T>(string query, params object[] parameters)
        {
            try
            {
                return this.Database.SqlQuery<T>(query, parameters).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ExecuteStoredProcedureDataTable(string storedProcedureName, params SqlParameter[] arrParam)
        {
            DBConnect connaction = new DBConnect();
            DataTable dt = new DataTable();
            try
            {
                dt = connaction.ExecuteDataTable(storedProcedureName, arrParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable ExecuteSqlDataTable(string SqlQuery, params SqlParameter[] arrParam)
        {
            DBConnect connaction = new DBConnect();
            DataTable dt = new DataTable();
            try
            {
                dt = connaction.ExecuteSqlQuery(SqlQuery, arrParam);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public bool ExecuteInsertQuery(string SqlQuery, params SqlParameter[] arrParam)
        {
            bool status = false;
           
            DBConnect connaction = new DBConnect();
            try
            {
                int rowsAffected = connaction.InsertData(SqlQuery, arrParam);
                status = rowsAffected > 0;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return status;
        }

        #endregion
    }
}
